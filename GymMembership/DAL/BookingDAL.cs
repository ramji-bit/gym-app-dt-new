﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections;
using GymMembership.DTO;
using System.Web.Services;
using System.Windows.Forms;

namespace GymMembership.DAL
{
    public class BookingDAL
    {
        //GymAppWebService.GymAppWebServices Services = new GymAppWebService.GymAppWebServices();
        
        public DataSet get_BookingMembersDetails(string MemberLastName)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@MemberLastName", MemberLastName);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                DataSet ds = sql.ExecuteProcedure("BookingMembersDetails", ht);
                
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet get_BookingResourceTrainer()
        {
            try
            {
                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                DataSet ds;

                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                if (Comman.Constants._OfflineFlag)
                {
                    Hashtable ht = new Hashtable();
                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@SiteID", _SiteID);

                    ds = sql.ExecuteProcedure("getBookingResourceTrainer", ht);
                }
                else
                {
                    ds = new DataSet();
                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("Trainer", typeof(string));
                    dtResult.Columns.Add("StaffID", typeof(int));
                    dtResult.Columns.Add("StaffRoleId", typeof(int));

                    var result = from StaffLoginRows1 in Comman.Constants._DisConnectedALLTables.Tables["Staff"].AsEnumerable()
                                 join LoginRows2 in Comman.Constants._DisConnectedALLTables.Tables["Login"].AsEnumerable()
                                 on StaffLoginRows1.Field<int>("LoginID") equals LoginRows2.Field<int>("LoginID")
                                 where (LoginRows2.Field<int>("CompanyID") == _CompanyID &&
                                 LoginRows2.Field<int>("siteID") == _SiteID &&
                                 StaffLoginRows1.Field<int>("staffroleID") == 2)

                                 select new
                                 {
                                     Trainer = StaffLoginRows1.Field<string>("FirstName") + " " + StaffLoginRows1.Field<string>("LastName"),
                                     StaffID = StaffLoginRows1.Field<int>("StaffId"),
                                     StaffRoleID = StaffLoginRows1.Field<int>("StaffRoleId")
                                 };

                    foreach (var v in result)
                    {
                        DataRow dr = dtResult.NewRow();
                        dr["Trainer"] = v.Trainer;
                        dr["StaffID"] = v.StaffID;
                        dr["StaffRoleId"] = v.StaffRoleID;
                        dtResult.Rows.Add(dr);
                    }
                    ds.Tables.Add(dtResult);
                }
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet get_Facilities()
        {
            try
            {
                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                DataSet ds;

                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                if (Comman.Constants._OfflineFlag)
                {
                    Hashtable ht = new Hashtable();
                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@SiteID", _SiteID);
                    ds = sql.ExecuteProcedure("getFacilities", ht);
                }
                else
                {
                    ds = new DataSet();
                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("FacilityID", typeof(int));
                    dtResult.Columns.Add("FacilityName", typeof(string));

                    var result = from Facilities in Comman.Constants._DisConnectedALLTables.Tables["Facilities"].AsEnumerable()
                                 where (Facilities.Field<int>("CompanyID") == _CompanyID && Facilities.Field<int>("SiteID") == _SiteID
                                 && Facilities.Field<int>("FacilityStatus") == 1)

                                 select Facilities;

                    foreach (var v in result)
                    {
                        DataRow dr = dtResult.NewRow();
                        dr["FacilityID"] = v.Field<int>("FacilityID");
                        dr["FacilityName"] = v.Field<string>("FacilityName").ToString().ToUpper();
                        dtResult.Rows.Add(dr);
                    }
                    ds.Tables.Add(dtResult);
                }
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet getFacilitySlots(int _facilityID)
        {
            try
            {
                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                DataSet ds = null;

                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                if (Comman.Constants._OfflineFlag)
                {
                    Hashtable ht = new Hashtable();
                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@SiteID", _SiteID);
                    ht.Add("@FacilityID", _facilityID);
                    ds = sql.ExecuteProcedure("getFacilitySlots", ht);
                }
                
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet get_ResourceTypes()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetResourceTypes", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public int addBooking(BookingTransDTO _bookingDTO)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", _bookingDTO.CompanyID);
                ht.Add("@SiteID", _bookingDTO.SiteID);
                ht.Add("@ResourceTypeName", _bookingDTO.ResourceTypename);
                ht.Add("@ResourceTypeID", _bookingDTO.ResourceTypeID);
                ht.Add("@FirstName", _bookingDTO.FirstName);
                ht.Add("@LastName", _bookingDTO.LastName);
                ht.Add("@Phone", _bookingDTO.Phone);
                ht.Add("@Date", _bookingDTO.Date);
                ht.Add("@Time", _bookingDTO.Time);
                ht.Add("@RoomOrTrainer", _bookingDTO.RoomOrTrainer);
                ht.Add("@MemberID", _bookingDTO.MemberID);
                ht.Add("@BookingStatus", _bookingDTO.BookingStatus);
                ht.Add("@WaitListNo", _bookingDTO.WaitListNo);
                ht.Add("@FacilityID", _bookingDTO.FacilityID);
                ht.Add("@SessionNumber", _bookingDTO.SessionNumber);

                DataSet ds = sql.ExecuteProcedure("AddBookingDetails", ht);
                if (ds != null)
                {
                    if (ds.Tables[0].DefaultView.Count > 0)
                    {
                        return Convert.ToInt32(ds.Tables[0].DefaultView[0]["Result"].ToString());
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }
        public DataSet getBooking(int _CompanyID, int _SiteID, int _ResourceTypeID, DateTime _Date, int _SlotID = 0, int _FacilityID = 0)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                ht.Add("@ResourceTypeID", _ResourceTypeID);
                ht.Add("@Date", _Date);
                ht.Add("@SlotID", _SlotID);
                ht.Add("@FacilityID", _FacilityID);

                DataSet ds = sql.ExecuteProcedure("getBookingDetails", ht);
                if (ds != null)
                {
                    if (ds.Tables[0].DefaultView.Count > 0)
                    {
                        return ds;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
    }
}
