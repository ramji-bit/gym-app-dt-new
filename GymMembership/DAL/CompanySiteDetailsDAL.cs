﻿using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System;
using System.Collections;

namespace GymMembership.DAL
{
    class CompanySiteDetailsDAL
    {
        public DataSet get_OrgCompanyName()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                DataSet ds = sql.ExecuteProcedure("GetCompanyNames",ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet get_SitebyCompanyName(string _CompanyName)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyName", _CompanyName);
                DataSet ds = sql.ExecuteProcedure("GetSitebyCompanyName", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
    }
}
