﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections;
using System.Net.Mail;

using GymMembership.DTO;

namespace GymMembership.DAL
{
    public class UserDetailsDAL
    {
        public DataSet get_UserLogin(string UserName, string Password, string CompanyName)
        {
            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return null;

            Hashtable ht = new Hashtable();
            ht.Add("@Username", UserName);
            ht.Add("@Password", Password);
            ht.Add("@CompanyName", CompanyName);
            DataSet ds = sql.ExecuteProcedure("ValidateUser_new", ht);
            return ds;
        }
        public DataSet Get_UserCompanySiteDetails(string UserName, string Password, string CompanyName)
        {
            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return null;

            Hashtable ht = new Hashtable();
            ht.Add("@Username", UserName);
            ht.Add("@Password", Password);
            ht.Add("@CompanyName", CompanyName);
            DataSet ds = sql.ExecuteProcedure("GetUserCompanySiteStaffDetails", ht);
            return ds;
        }
        public DataSet CheckIfUserAlreadyExists(string CompanyName, string SiteName , string UserName)
        {
            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return null;

            Hashtable ht = new Hashtable();
            ht.Add("@CompanyName", CompanyName);
            ht.Add("@SiteName", SiteName);
            ht.Add("@Username", UserName);
            DataSet ds = sql.ExecuteProcedure("CheckIfUserExists", ht);
            return ds;
        }

        public DataSet OrgNameExistsinDB (string _CompanyName)
        {
            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return null;

            Hashtable ht = new Hashtable();
            ht.Add("@CompanyName", _CompanyName);
            DataSet ds = sql.ExecuteProcedure("CheckIfCompanyExists", ht);
            return ds;
        }

        public DataSet AddNEWLoginAndStaffDetails(UserLoginDTO _UserLoginDTO)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                //Login Table
                ht.Add("@UserName", _UserLoginDTO.UserName);
                ht.Add("@Password", _UserLoginDTO.Password);
                ht.Add("@UserRole", _UserLoginDTO.UserRole);
                ht.Add("@UserStatus", _UserLoginDTO.UserStatus);
                ht.Add("@CompanyName", _UserLoginDTO.CompanyName);
                ht.Add("@SiteName", _UserLoginDTO.SiteName);

                //Staff Table
                ht.Add("@FirstName", _UserLoginDTO.FirstName);
                ht.Add("@LastName", _UserLoginDTO.LastName);
                ht.Add("@DateOfBirth", _UserLoginDTO.DateOfBirth);
                ht.Add("@Gender", _UserLoginDTO.Gender);
                ht.Add("@Street", _UserLoginDTO.Street);
                ht.Add("@Suburb", _UserLoginDTO.Suburb);
                ht.Add("@PostalCode", _UserLoginDTO.PostalCode);
                ht.Add("@HomePhone", _UserLoginDTO.HomePhone);
                ht.Add("@MobilePhone", _UserLoginDTO.MobilePhone);
                ht.Add("@IsMember", _UserLoginDTO.IsMember);
                ht.Add("@MemberID", _UserLoginDTO.MemberID);
                ht.Add("@StaffRoleID", _UserLoginDTO.StaffRoleID);
                ht.Add("@StaffPic", _UserLoginDTO.StaffPic);
                ht.Add("@isAppTimeOutNeverExpire", _UserLoginDTO.isAppTimeOutNeverExpire);
                ht.Add("@EmailID", _UserLoginDTO.EmailID);

                return sql.ExecuteProcedure("AddLoginAndStaffDetails", ht);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public int UpdateLoginAndStaffDetails(UserLoginDTO _UserLoginDTO)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                //Login Table
                ht.Add("@LoginID", _UserLoginDTO.LoginID);
                ht.Add("@UserName", _UserLoginDTO.UserName);
                ht.Add("@Password", _UserLoginDTO.Password);
                ht.Add("@UserRole", _UserLoginDTO.UserRole);
                ht.Add("@UserStatus", _UserLoginDTO.UserStatus);
                ht.Add("@CompanyName", _UserLoginDTO.CompanyName);
                ht.Add("@SiteName", _UserLoginDTO.SiteName);

                //Staff Table
                ht.Add("@StaffID", _UserLoginDTO.StaffID);
                ht.Add("@FirstName", _UserLoginDTO.FirstName);
                ht.Add("@LastName", _UserLoginDTO.LastName);
                ht.Add("@DateOfBirth", _UserLoginDTO.DateOfBirth);
                ht.Add("@Gender", _UserLoginDTO.Gender);
                ht.Add("@Street", _UserLoginDTO.Street);
                ht.Add("@Suburb", _UserLoginDTO.Suburb);
                ht.Add("@PostalCode", _UserLoginDTO.PostalCode);
                ht.Add("@HomePhone", _UserLoginDTO.HomePhone);
                ht.Add("@MobilePhone", _UserLoginDTO.MobilePhone);
                ht.Add("@IsMember", _UserLoginDTO.IsMember);
                ht.Add("@MemberID", _UserLoginDTO.MemberID);
                ht.Add("@StaffRoleID", _UserLoginDTO.StaffRoleID);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                //ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                ht.Add("@SiteID", _UserLoginDTO.SiteID);
                ht.Add("@StaffPic", _UserLoginDTO.StaffPic);
                ht.Add("@isAppTimeOutNeverExpire", _UserLoginDTO.isAppTimeOutNeverExpire);
                ht.Add("@EmailID", _UserLoginDTO.EmailID);

                int result = sql.ExecuteQuery("UpdateLoginAndStaffDetails", ht);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public DataSet GetOrgNameFromDB()
        {
            SqlHelper sql = new SqlHelper();
            //if (sql._ConnOpen == 0) return null;
            if (sql._ConnOpen == 0) return null;

            Hashtable ht = new Hashtable();
            DataSet ds = sql.ExecuteProcedure("GetCompanyNames", ht);
            return ds;
        }
        public DataSet AddNewOrganizationName(string _OrgName)
        {
            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return null;

            Hashtable ht = new Hashtable();
            ht.Add("@CompanyName", _OrgName);
            DataSet ds = sql.ExecuteProcedure("AddNewOrganizationName", ht);
            return ds;
        }
        public int UA_AddDefaultAccessRightsOnLoginCreation(int LoginID, int AccessRights, int CompanyID, int SiteID)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", CompanyID);
                ht.Add("@LoginID", LoginID);
                ht.Add("@AccessStatus", AccessRights);
                ht.Add("@SiteID", SiteID);//Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                int result = sql.ExecuteQuery("UA_AddDefaultAccessRightsOnLoginCreation", ht);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
