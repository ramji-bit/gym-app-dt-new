﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections;
using GymMembership.DTO;
using System.Web.Services;
using System.Windows.Forms;

namespace GymMembership.DAL
{
    public class TaskDAL
    {
        //GymAppWebService.GymAppWebServices Services = new GymAppWebService.GymAppWebServices();
        public DataSet get_TaskByMemberId(int MemberId,int TaskId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();

                ht.Add("@MemberID", MemberId);
                ht.Add("@TaskId", TaskId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetTaskByMemberId", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet get_TaskByProspectId(int MemberId, int TaskId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();

                ht.Add("@MemberID", MemberId);
                ht.Add("@TaskId", TaskId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetTaskByProspectId", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet get_TasKType(int TaskTypeId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();

                ht.Add("@TaskTypeId", TaskTypeId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetTasKType", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }

        }

        public int add_TaskToStaff(int MemberId, int TaskType, string TaskDecription, DateTime TaskAssgnDt,int StaffAssgnTo, int TasKStatus)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();

                ht.Add("@MemberId", MemberId);
                ht.Add("@TaskType", TaskType);
                ht.Add("@TaskDecription", TaskDecription);
                ht.Add("@TaskAssgnDt", TaskAssgnDt);
                ht.Add("@StaffAssgnTo", StaffAssgnTo);
                ht.Add("@TaskStatus", TasKStatus);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                int result = sql.ExecuteQuery("AddTaskToStaff", ht);
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }

        }
        public int add_ProspectTaskToStaff(int ProspectID, int TaskType, string TaskDecription, DateTime TaskAssgnDt, int StaffAssgnTo, int TasKStatus)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();

                ht.Add("@ProspectID", ProspectID);
                ht.Add("@TaskType", TaskType);
                ht.Add("@TaskDecription", TaskDecription);
                ht.Add("@TaskAssgnDt", TaskAssgnDt);
                ht.Add("@StaffAssgnTo", StaffAssgnTo);
                ht.Add("@TaskStatus", TasKStatus);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                int result = sql.ExecuteQuery("AddProspectTaskToStaff", ht);
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }

        }
        public DataSet get_TaskByStaffId(int TaskId,int StaffId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();

                ht.Add("@TaskTypeId", TaskId);
                ht.Add("@StaffId", StaffId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetTaskByStaff", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }

        }

        public int add_StaffTask(int TaskId,int MemberId, int StaffId,int TaskStaus, DateTime TaskExpireDate, string StaffActionTaken)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@TaskId", TaskId);
                ht.Add("@MemberId", MemberId);
                ht.Add("@StaffId", StaffId);
                ht.Add("@Status", TaskStaus);
                ht.Add("@ExpiredDt", TaskExpireDate);
                ht.Add("@StaffActionTaken", StaffActionTaken);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                int result = sql.ExecuteQuery("AddStaffTask", ht);
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }

        public int add_ProspectStaffTask(int TaskId, int MemberId, int StaffId, int TaskStaus, DateTime TaskExpireDate, string StaffActionTaken)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@TaskId", TaskId);
                ht.Add("@MemberId", MemberId);
                ht.Add("@StaffId", StaffId);
                ht.Add("@Status", TaskStaus);
                ht.Add("@ExpiredDt", TaskExpireDate);
                ht.Add("@StaffActionTaken", StaffActionTaken);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                int result = sql.ExecuteQuery("AddProspectStaffTask", ht);

                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }

        public int change_StaffTask(int TaskId, int ChangedStaffId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@TaskId", TaskId);
                ht.Add("@AssignedStaff", ChangedStaffId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                int result = sql.ExecuteQuery("ChangeStaffTask", ht);
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }


        public DataSet get_TaskHistoryByMember(int StaffId, int MemberId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();

                ht.Add("@staffId", StaffId);
                ht.Add("@MemberId", MemberId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetTaskHistory", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }

        }

        public DataSet get_TaskHistoryByProspect(int StaffId, int MemberId, int _TaskID)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();

                ht.Add("@staffId", StaffId);
                ht.Add("@MemberId", MemberId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                ht.Add("@TaskID", _TaskID);

                DataSet ds = sql.ExecuteProcedure("GetProspectTaskHistory", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }

        }
        public DataSet get_StaffIdByUserName(string UserName)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();

                ht.Add("@UserName", UserName);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetStaffIdByUserName", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }

        }

    }
}
