﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;
using System.Configuration;

namespace GymMembership.DAL
{
    public class PointOfSaleDAL
    {
        public DataSet getPOSMemberDetails(string MemberLastName)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                Hashtable ht = new Hashtable();
                ht.Add("@MemberLastName", MemberLastName);
                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                DataSet ds = sql.ExecuteProcedure("MakeASaleMemberDetails", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public int SavePOSInvoiceDetails(DataTable addInvoiceDetails)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                int result = 0;
                for (int i = 0; i < addInvoiceDetails.Rows.Count; i++)
                {
                    Hashtable ht = new Hashtable();

                    ht.Add("@InvoiceNo", Convert.ToInt32(addInvoiceDetails.Rows[i]["InvoiceNo"].ToString()));
                    ht.Add("@MemberId", Convert.ToInt32(addInvoiceDetails.Rows[i]["MemberNo"].ToString()));
                    ht.Add("@InvoiceDetails", addInvoiceDetails.Rows[i]["InvoiceDetails"].ToString());
                    ht.Add("@InvoiceDetailsAmt", Convert.ToDecimal(addInvoiceDetails.Rows[i]["Amount"].ToString()));
                    ht.Add("@InvoiceStatus", addInvoiceDetails.Rows[i]["Status"].ToString());
                    ht.Add("@ProdSale", Convert.ToChar(addInvoiceDetails.Rows[i]["ProdSale"].ToString()));
                    ht.Add("@ProdID", Convert.ToInt32(addInvoiceDetails.Rows[i]["ProdID"].ToString()));
                    ht.Add("@ProdQty",Convert.ToInt32(addInvoiceDetails.Rows[i]["ProdQty"].ToString()));
                    ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"]));
                    ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"]));
                    
                    result = sql.ExecuteQuery("SavePOSInvoiceDetails", ht);

                    //IF PRODUCT SALE; UPDATE INVENTORY
                    if ((Convert.ToInt32(addInvoiceDetails.Rows[i]["ProdID"].ToString()) != 0) && (Convert.ToInt32(addInvoiceDetails.Rows[i]["ProdQty"].ToString()) != 0))
                    {
                        if (Convert.ToChar(addInvoiceDetails.Rows[i]["ProdSale"].ToString()) == 'Y')
                        {
                            //UPDATE INVENTORY
                            ht.Clear();

                            ht.Add("@ProductID", Convert.ToInt32(addInvoiceDetails.Rows[i]["ProdID"].ToString()));
                            ht.Add("@Quantity", Convert.ToInt32(addInvoiceDetails.Rows[i]["ProdQty"].ToString()));
                            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"]));
                            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"]));

                            sql.ExecuteQuery("Update_Product_Inventory_Sale", ht);
                        }
                    }                  
                }

                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }

        public int raise_POS_Invoice (
                                        DateTime InvoiceDate, int InvoiceNo, decimal InvoiceAmount, decimal AmountPaid, 
                                        decimal OutstandingAmt, int MemberId, string InvoiceStatus, string FieldNameType, 
                                        decimal _Cash = 0, decimal _Card = 0, decimal _Change = 0
                                        , decimal _InvAmtExcDiscount = 0m, int _discountPercent = 0
                                        , decimal _discountDollar = 0m
                                     )
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();

                ht.Add("@InvoiceDate", InvoiceDate);
                ht.Add("@InvoiceNo", InvoiceNo);
                ht.Add("@InvoiceAmt", InvoiceAmount);
                ht.Add("@AmountPaid", AmountPaid);
                ht.Add("@AmountOutstanding", OutstandingAmt);
                ht.Add("@MemberId", MemberId);
                ht.Add("@InvoiceStatus", InvoiceStatus);
                ht.Add("@FieldNameType", FieldNameType);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                ht.Add("@UserName", ConfigurationManager.AppSettings["LoggedInUser"].ToString());
                ht.Add("@Cash", _Cash);
                ht.Add("@Card", _Card);
                ht.Add("@Change", _Change);
                ht.Add("@InvAmtExcDiscount", _InvAmtExcDiscount);
                ht.Add("@DiscountPercent", _discountPercent);
                ht.Add("@DiscountDollar", _discountDollar);

                int result = sql.ExecuteQuery("RaiseInvoice", ht);
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }
    }
}
