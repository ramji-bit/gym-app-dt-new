﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections;
using GymMembership.DTO;
using System.Web.Services;
using System.Windows.Forms;
using GymMembership.Comman;

namespace GymMembership.DAL
{
    
    class SettingsDAL
    {
        public DataSet get_MembershipGroupById(int GroupId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@GroupId", GroupId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetMembershipGroupById", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet get_MemberNumber()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                DataSet ds = sql.ExecuteProcedure("GetMemberNo", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public int add_ProgrammeGroups(int GroupsID, string GroupName, string Abbreviation, int GroupMembershipCard, int GroupStatus)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@GroupsID", GroupsID);
                ht.Add("@GroupsName", GroupName);
                ht.Add("@Abbreviation", Abbreviation);
                ht.Add("@GroupMembershipCard", GroupMembershipCard);
                ht.Add("@GroupStatus", GroupStatus);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));

                int result = sql.ExecuteQuery("SaveProgrammeGroup", ht);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }

        }

        public int addNew_ProgrammeGroups(string GroupName, string Abbreviation, int GroupMembershipCard, int GroupStatus)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@GroupsName", GroupName);
                ht.Add("@Abbreviation", Abbreviation);
                ht.Add("@GroupMembershipCard", GroupMembershipCard);
                ht.Add("@GroupStatus", GroupStatus);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));

                int result = sql.ExecuteQuery("SaveNewProgrammeGroup", ht);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }

        }

        public int saveEdit_ProgrammeDetails(int ProgramId, string ProgrammeName, decimal ProgrammePrice, int ProgrammeLength, int ProgrammeDuration, int NoOfVisits, decimal SignUpFee, string Conditions, int ProgrammeStatus, DateTime ProgrammeSdate, bool _isUpFront = false, bool _isNeverExpire = false)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@ProgrammeID", ProgramId);
                ht.Add("@ProgrammeName", ProgrammeName);
                ht.Add("@ProgrammePrice", ProgrammePrice);
                ht.Add("@ProgrammeLength", ProgrammeLength);
                ht.Add("@ProgrammeDuration", ProgrammeDuration);
                ht.Add("@NoOfVisits", NoOfVisits);
                ht.Add("@SignUpFee", SignUpFee);
                ht.Add("@Conditions", Conditions);
                ht.Add("@ProgrammeStatus", ProgrammeStatus);
                ht.Add("@ProgrammeSdate", ProgrammeSdate);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@isUpFront", _isUpFront);
                ht.Add("@isNeverExpire", _isNeverExpire);

                int result = sql.ExecuteQuery("SaveEditProgrammeDetails", ht);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }

        }

        public int saveNew_ProgrammeDetails(int ProgrammeGroupId, string ProgrammeName, decimal ProgrammePrice, int ProgrammeLength, int ProgrammeDuration, int NoOfVisits, decimal SignUpFee, string Conditions, int ProgrammeStatus, DateTime ProgrammeSdate, bool _isUpFront = false, bool _isNeverExpire = false)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@ProgrammeGroupId", ProgrammeGroupId);
                ht.Add("@ProgrammeName", ProgrammeName);
                ht.Add("@ProgrammePrice", ProgrammePrice);
                ht.Add("@ProgrammeLength", ProgrammeLength);
                ht.Add("@ProgrammeDuration", ProgrammeDuration);
                ht.Add("@NoOfVisits", NoOfVisits);
                ht.Add("@SignUpFee", SignUpFee);
                ht.Add("@Conditions", Conditions);
                ht.Add("@ProgrammeStatus", ProgrammeStatus);
                ht.Add("@ProgrammeSdate", ProgrammeSdate);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@isUpFront", _isUpFront);
                ht.Add("@isNeverExpire", _isNeverExpire);

                int result = sql.ExecuteQuery("SaveNewProgrammeDetails", ht);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }

        }

        public int saveNew_productType(string ProductType)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@ProductTypeName", ProductType);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                //ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                int result = sql.ExecuteQuery("SaveNewProductType", ht);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }

        }

        public int saveEdit_productType(int ProductTypeId, string ProductTypeName, int Active = 1)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@ProductTypeId", ProductTypeId);
                ht.Add("@ProductTypeName", ProductTypeName);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@Active", Active);

                int result = sql.ExecuteQuery("SaveEditProductType", ht);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }

        }

        public int saveEdit_ProductDetails(int ProductId, string ProductName, decimal ProductCostPrice, decimal ProductSellingPrice, int Active = 1, int ReOrderQty = 0, string alertEmail = "" )
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@ProductID", ProductId);
                ht.Add("@ProductName", ProductName);
                ht.Add("@ProductCostPrice", ProductCostPrice);
                ht.Add("@ProductSalePrice", ProductSellingPrice);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@Active", Active);
                ht.Add("@ReOrderQty", ReOrderQty);
                ht.Add("@alertEmail", alertEmail);

                int result = sql.ExecuteQuery("SaveEditProductDetails", ht);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }

        }
        
        public int saveNew_ProductDetails(int ProductTypeId, string ProductName, decimal ProductCostPrice, decimal ProductSellingPrice, int ReOrderQty = 0, string alertEmail = "")
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@ProductTypeID", ProductTypeId);
                ht.Add("@ProductName", ProductName);
                ht.Add("@ProductCostPrice", ProductCostPrice);
                ht.Add("@ProductSalePrice", ProductSellingPrice);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                //ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                ht.Add("@ReOrderQty", ReOrderQty);
                ht.Add("@alertEmail", alertEmail);


                int result = sql.ExecuteQuery("SaveNewProductDetails", ht);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }

        }

        public DataSet getSupplierName()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                DataSet ds = sql.ExecuteProcedure("GetSupplierName", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet get_SupplierDetails(int SupplierId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@SupplierId", SupplierId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetSupplierDetailsById", ht);
                //DataSet ds = Services.get_ProgrammeDetails(ProgramId);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }


        public int saveEdit_SupplierDetails(int SupplierId, string SupplierName, string MobileNo, string PhoneNo, string FaxNo, string Email, string Website, string ContactPerson, string Notes)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@SupplierId", SupplierId);
                ht.Add("@SupplierName", SupplierName);
                ht.Add("@MobileNo", MobileNo);
                ht.Add("@PhoneNo", PhoneNo);
                ht.Add("@FaxNo", FaxNo);
                ht.Add("@Email", Email);
                ht.Add("@Website", Website);
                ht.Add("@ContactPerson", ContactPerson);
                ht.Add("@Notes", Notes);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));

                int result = sql.ExecuteQuery("SaveEditSupplierDetails", ht);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }

        }

        public int saveNew_SupplierDetails(string SupplierName, string MobileNo, string PhoneNo, string FaxNo, string Email, string Website, string ContactPerson, string Notes)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@SupplierName", SupplierName);
                ht.Add("@MobileNo", MobileNo);
                ht.Add("@PhoneNo", PhoneNo);
                ht.Add("@FaxNo", FaxNo);
                ht.Add("@Email", Email);
                ht.Add("@Website", Website);
                ht.Add("@ContactPerson", ContactPerson);
                ht.Add("@Notes", Notes);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));

                int result = sql.ExecuteQuery("SaveNewSupplierDetails", ht);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }

        }

        public DataSet getResourceType()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetResourceTypesByID", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet get_ResourceById(int ResourceTypeId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@ResourceTypeId", ResourceTypeId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetResourcesById", ht);

                //DataSet ds = Services.get_ProgrammeDetails(ProgramId);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet get_ResourceFacilityId(int FacilityId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@FacilityId", FacilityId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetResourcesByFacilityId", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public int saveNew_ResourcesDetails(BookingResourceDTO _bookingResourceDTO)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@FacilityName", _bookingResourceDTO._FacilityName);
                ht.Add("@FacilityStatus", _bookingResourceDTO._FacilityStatus);
                ht.Add("@ResourceTypeID", _bookingResourceDTO._ResourceTypeID);
                ht.Add("@FacilityId", _bookingResourceDTO._FacilityID);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                ht.Add("@MaxMembers", _bookingResourceDTO._MaxMembers);
                ht.Add("@MaxWaitList", _bookingResourceDTO._MaxWaitList);
                ht.Add("@isUpdate", _bookingResourceDTO._isUpdate);
                ht.Add("@NumTimeSlots", _bookingResourceDTO._NumTimeSlots);
                ht.Add("@ChargePerSlot", _bookingResourceDTO._ChargePerSlot);
                ht.Add("@FacilitySlots", _bookingResourceDTO._dtSlots);

                int result = sql.ExecuteQuery("SaveEditNewResource_NEW", ht);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }

        }

        public int saveEdit_ResourcesDetails(string _FacilityName, int _FacilityStatus, int _FacilityId, int _MaxMembers = 0, int _MaxWaitList = 0)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@FacilityName", _FacilityName);
                ht.Add("@FacilityStatus", _FacilityStatus);
                ht.Add("@FacilityId", _FacilityId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                ht.Add("@MaxMembers", _MaxMembers);
                ht.Add("@MaxWaitList", _MaxWaitList);

                int result = sql.ExecuteQuery("SaveEditResource", ht);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public DataSet GetActiveUserNamesUAdmin()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                DataSet ds = sql.ExecuteProcedure("GetActiveUserNamesUAdmin", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet GetAllContactTypes()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                DataSet ds = sql.ExecuteProcedure("GetAllContactTypes", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet GetAccessRightForUser(int _CompanyID, int _SiteID, int _LoginID)
        {
            try
            {
                DataSet _dsUserRights;
                if (Constants._OfflineFlag)
                {
                    SqlHelper sql = new SqlHelper();
                    if (sql._ConnOpen == 0) return null;

                    Hashtable ht = new Hashtable();
                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@LoginID", _LoginID);
                    ht.Add("@SiteID", _SiteID);
                    _dsUserRights = sql.ExecuteProcedure("GetAccessRightForUser", ht);
                }
                else
                {
                    _dsUserRights = new DataSet(); //ADD Tables in Order dtResult1, dtResult2, etc

                    DataTable dtResult1 = new DataTable();
                    dtResult1.Columns.Add("ModuleName", typeof(string));
                    dtResult1.Columns.Add("ModuleID", typeof(int));
                    dtResult1.Columns.Add("Active", typeof(int));

                    var result1 = from UserAccessRights in Constants._DisConnectedALLTables.Tables["GymApp_UserAccessRights"].AsEnumerable()
                                  join Modules in Constants._DisConnectedALLTables.Tables["GymApp_Modules"].AsEnumerable()
                                  on UserAccessRights.Field<int>("ModuleID") equals Modules.Field<int>("ModuleID")
                                  join SubModules in Constants._DisConnectedALLTables.Tables["GymApp_SubModules"].AsEnumerable()
                                  on UserAccessRights.Field<int>("SubModuleID") equals SubModules.Field<int>("SubModuleID")
                                  where (UserAccessRights.Field<int>("CompanyID") == _CompanyID &&
                                  UserAccessRights.Field<int>("LoginID") == _LoginID &&
                                  UserAccessRights.Field<int>("SiteID") == _SiteID)
                                  orderby Modules.Field<int>("ModuleID") ascending
                                  orderby UserAccessRights.Field<bool>("AccessStatus") descending

                                  select new
                                  {
                                      ModuleName = Modules.Field<string>("ModuleName"),
                                      ModuleID = Modules.Field<int>("ModuleID"),
                                      Active = UserAccessRights.Field<bool>("AccessStatus")
                                  };

                    int _moduleID = 0;
                    foreach (var v in result1)
                    {
                        if (_moduleID != v.ModuleID)
                        {
                            DataRow dr = dtResult1.NewRow();
                            dr["ModuleName"] = v.ModuleName;
                            dr["ModuleID"] = v.ModuleID;
                            dr["Active"] = v.Active;
                            dtResult1.Rows.Add(dr);
                        }
                        _moduleID = v.ModuleID;
                    };

                    DataTable dtResult2 = new DataTable();
                    dtResult2.Columns.Add("LoginID", typeof(int));
                    dtResult2.Columns.Add("CompanyID", typeof(int));
                    dtResult2.Columns.Add("ModuleName", typeof(string));
                    dtResult2.Columns.Add("ModuleID", typeof(int));
                    dtResult2.Columns.Add("SubModuleID", typeof(int));
                    dtResult2.Columns.Add("SubModuleName", typeof(string));
                    dtResult2.Columns.Add("Active", typeof(int));

                    var result2 = from UserAccessRights in Constants._DisConnectedALLTables.Tables["GymApp_UserAccessRights"].AsEnumerable()
                                  join Modules in Constants._DisConnectedALLTables.Tables["GymApp_Modules"].AsEnumerable()
                                  on UserAccessRights.Field<int>("ModuleID") equals Modules.Field<int>("ModuleID")
                                  join SubModules in Constants._DisConnectedALLTables.Tables["GymApp_SubModules"].AsEnumerable()
                                  on UserAccessRights.Field<int>("SubModuleID") equals SubModules.Field<int>("SubModuleID")
                                  where (UserAccessRights.Field<int>("CompanyID") == _CompanyID &&
                                  UserAccessRights.Field<int>("LoginID") == _LoginID &&
                                  UserAccessRights.Field<int>("SiteID") == _SiteID)
                                  orderby UserAccessRights.Field<int>("ModuleID")

                                  select new
                                  {
                                      LoginID = UserAccessRights.Field<int>("LoginID")
                                        ,
                                      CompanyID = UserAccessRights.Field<int>("CompanyID")
                                        ,
                                      ModuleID = UserAccessRights.Field<int>("ModuleID")
                                        ,
                                      ModuleName = Modules.Field<string>("ModuleName")
                                        ,
                                      SubModuleID = UserAccessRights.Field<int>("SubModuleID")
                                        ,
                                      SubModuleName = SubModules.Field<string>("SubModuleName")
                                        ,
                                      Active = UserAccessRights.Field<bool>("AccessStatus")
                                  };

                    foreach (var v in result2)
                    {
                        DataRow dr = dtResult2.NewRow();
                        dr["LoginID"] = v.LoginID;
                        dr["CompanyID"] = v.CompanyID;
                        dr["ModuleName"] = v.ModuleName;
                        dr["ModuleID"] = v.ModuleID;
                        dr["SubModuleID"] = v.SubModuleID;
                        dr["SubModuleName"] = v.SubModuleName;
                        dr["Active"] = v.Active;
                        dtResult2.Rows.Add(dr);
                    };

                    _dsUserRights.Tables.Add(dtResult1);
                    _dsUserRights.Tables.Add(dtResult2);
                }
                return _dsUserRights;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
    }
}
