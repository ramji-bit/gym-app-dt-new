﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections;
using System.Web.Services;
using System.Windows.Forms;
using System.Globalization;
using GymMembership.DTO;
using GymMembership.Comman;

namespace GymMembership.DAL
{
    public class MemberDetailsDAL
    {
        //GymWebServicesReference.GymAppWebServicesSoapClient Services = new GymWebServicesReference.GymAppWebServicesSoapClient();
        //GymAppWebService.GymAppWebServices Services = new GymAppWebService.GymAppWebServices();


        //public int add_MemberDetails(string Fname, string Lname, DateTime DOB, int Gender, string Street, string Subrub, string City, string PostalCode, string HomePhone)
        public int add_MemberDetails(MemberDetailsDTO memberDetailsDTO, int _Update = 0)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                int result;
                if (Comman.Constants._OfflineFlag)
                {
                    Hashtable ht = new Hashtable();

                    ht.Add("@Fname", memberDetailsDTO.FirstName);
                    ht.Add("@Lname", memberDetailsDTO.LastName);
                    ht.Add("@dob", memberDetailsDTO.DateofBirth);
                    ht.Add("@Gender", memberDetailsDTO.Gender);
                    ht.Add("@Street", memberDetailsDTO.Street);
                    ht.Add("@Suburb", memberDetailsDTO.Suburb);
                    ht.Add("@City", memberDetailsDTO.City);
                    ht.Add("@PostalCode", memberDetailsDTO.PostalCode);
                    ht.Add("@HomePhone", memberDetailsDTO.HomePhone);
                    ht.Add("@MobilePhone", memberDetailsDTO.MobilePhone);
                    ht.Add("@Notes", memberDetailsDTO.Notes);
                    ht.Add("@CardNumber", memberDetailsDTO.CardNo);
                    ht.Add("@WorkPhone", memberDetailsDTO.WorkPhone);
                    ht.Add("@Occupation", memberDetailsDTO.Occupation);
                    ht.Add("@Organisation", memberDetailsDTO.Organisation);
                    ht.Add("@InvolvementType", memberDetailsDTO.InvolvementType);
                    ht.Add("@PersonalTrainer", memberDetailsDTO.PersonalTrainer);
                    ht.Add("@EmailId", memberDetailsDTO.EmailId);
                    ht.Add("@MemberNo", memberDetailsDTO.MemberId);
                    //RAMJI 24/MAR/2016
                    ht.Add("@CompanyID", memberDetailsDTO.CompanyID);
                    //RAMJI 24/MAR/2016

                    //RAMJI 24/MAR/2016
                    ht.Add("@MemberProfilePhotoPath", memberDetailsDTO.MemberProfilePhotoPath);
                    //RAMJI 24/MAR/2016

                    //RAMJI 09/APR/2016
                    ht.Add("@Update", _Update);
                    //RAMJI 09/APR/2016

                    //RAMJI 08/APR/2016
                    if (memberDetailsDTO.MemberPic == null)
                    {
                        ht.Add("@MemberPic", DBNull.Value);
                    }
                    else
                    {
                        ht.Add("@MemberPic", memberDetailsDTO.MemberPic);
                    }
                    //RAMJI 08/APR/2016
                    ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                    ht.Add("@appVersion", memberDetailsDTO.appVersion);

                    if (memberDetailsDTO.cardReplicate == 1)
                    {
                        ht.Add("@cardReplicate", 1);
                    }
                    ht.Add("@technoGymPermanentToken", memberDetailsDTO.technoGymPermanentToken);
                    ht.Add("@technoGymEnv", memberDetailsDTO.technoGymEnv);

                    result = sql.ExecuteQuery("SaveMemberDetails", ht);
                }
                else
                {
                    if (_Update == 0)
                    {
                        DataRow dR= Comman.Constants._DisConnectedALLTables.Tables["MemberDetails"].NewRow();
                        //dR["MemberID"]
                        dR["FirstName"] = memberDetailsDTO.FirstName.ToUpper();
                        dR["LastName"] = memberDetailsDTO.LastName.ToUpper();
                        dR["DateofBirth"] = memberDetailsDTO.DateofBirth;
                        dR["Gender"] = memberDetailsDTO.Gender;
                        dR["Street"] = memberDetailsDTO.Street.ToUpper();
                        dR["Suburb"] = memberDetailsDTO.Suburb.ToUpper();
                        dR["City"] = memberDetailsDTO.City.ToUpper();
                        dR["PostalCode"] = memberDetailsDTO.PostalCode;
                        dR["HomePhone"] = memberDetailsDTO.HomePhone;
                        dR["MobilePhone"] = memberDetailsDTO.MobilePhone;
                        dR["Notes"] = memberDetailsDTO.Notes.ToUpper();
                        dR["CardNumber"] = memberDetailsDTO.CardNo;
                        dR["WorkPhone"] = memberDetailsDTO.WorkPhone;
                        dR["Occupation"] = memberDetailsDTO.Occupation.ToUpper();
                        dR["Organisation"] = memberDetailsDTO.Organisation.ToUpper();
                        dR["InvolvementType"] = memberDetailsDTO.InvolvementType;
                        dR["PersonalTrainer"] = memberDetailsDTO.PersonalTrainer;
                        dR["EmailAddress"] = memberDetailsDTO.EmailId.ToUpper();
                        dR["MemberNo"] = memberDetailsDTO.MemberId;
                        dR["CompanyId"] = memberDetailsDTO.CompanyID;
                        dR["MemberProfilePhotoPath"] = memberDetailsDTO.MemberProfilePhotoPath;
                        dR["MemberPic"] = memberDetailsDTO.MemberPic;
                        dR["SiteID"] = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                        dR["CreatedDate"] = DateTime.Now;
                        dR["ActiveMember"] = 1;
      
                        Comman.Constants._DisConnectedALLTables.Tables["MemberDetails"].Rows.Add(dR);
                        Comman.Constants._DisConnectedALLTables.Tables["MemberDetails"].AcceptChanges();
                    }
                    else
                    {
                        Comman.Constants._DisConnectedALLTables.Tables["MemberDetails"].DefaultView.RowFilter = "MemberNo = " + memberDetailsDTO.MemberId;
                        foreach (DataRowView dR in Comman.Constants._DisConnectedALLTables.Tables["MemberDetails"].DefaultView)
                        {
                            dR["MemberPic"] = memberDetailsDTO.MemberPic;
                            dR["FirstName"] = memberDetailsDTO.FirstName;
                            dR["LastName"] = memberDetailsDTO.LastName;
                            dR["DateofBirth"] = memberDetailsDTO.DateofBirth;
                            dR["Gender"] = memberDetailsDTO.Gender;
                            dR["Street"] = memberDetailsDTO.Street;
                            dR["Suburb"] = memberDetailsDTO.Suburb;
                            dR["City"] = memberDetailsDTO.City;
                            dR["PostalCode"] = memberDetailsDTO.PostalCode;
                            dR["HomePhone"] = memberDetailsDTO.HomePhone;
                            dR["MobilePhone"] = memberDetailsDTO.MobilePhone;
                            dR["Notes"] = memberDetailsDTO.Notes;
                            dR["CardNumber"] = memberDetailsDTO.CardNo;
                            dR["WorkPhone"] = memberDetailsDTO.WorkPhone;
                            dR["Occupation"] = memberDetailsDTO.Occupation;
                            dR["Organisation"] = memberDetailsDTO.Organisation;
                            dR["InvolvementType"] = memberDetailsDTO.InvolvementType;
                            dR["PersonalTrainer"] = memberDetailsDTO.PersonalTrainer;
                            dR["EmailAddress"] = memberDetailsDTO.EmailId;
                            dR["MemberNo"] = memberDetailsDTO.MemberId;
                            dR["CompanyId"] = memberDetailsDTO.CompanyID;
                            dR["MemberProfilePhotoPath"] = memberDetailsDTO.MemberProfilePhotoPath;
                            dR["MemberPic"] = memberDetailsDTO.MemberPic;
                            dR["SiteID"] = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                            dR["ActiveMember"] = 1;
                        }
                        Comman.Constants._DisConnectedALLTables.Tables["MemberDetails"].AcceptChanges();
                        Comman.Constants._DisConnectedALLTables.Tables["MemberDetails"].DefaultView.RowFilter = "";
                        Comman.Constants._DisConnectedALLTables.AcceptChanges();
                    }
                    return 1;
                }
                return result;
            }
            catch (Exception)
            {
                return 0;
            }

        }
        public DataSet get_PersonalTrainer()
        {
            try
            {
                //DataSet ds = Services.get_PersonalTrainer();
                try
                {
                    SqlHelper sql = new SqlHelper();
                    if (sql._ConnOpen == 0) return null;

                    int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                    DataSet ds;
                    if (Constants._OfflineFlag)
                    {
                        Hashtable ht = new Hashtable();
                        ht.Add("@CompanyID", _CompanyID);
                        ht.Add("@SiteID", _SiteID);
                        ds = sql.ExecuteProcedure("GetPersonalTrainer", ht);
                    }
                    else
                    {
                        ds = new DataSet();
                        DataTable dtResult = new DataTable();
                        dtResult.Columns.Add("TrainerName", typeof(string));
                        dtResult.Columns.Add("StaffID", typeof(int));
                        dtResult.Columns.Add("StaffRoleId", typeof(int));

                        var result = from StaffLoginRows1 in Constants._DisConnectedALLTables.Tables["Staff"].AsEnumerable()
                                     join LoginRows2 in Constants._DisConnectedALLTables.Tables["Login"].AsEnumerable()
                                     on StaffLoginRows1.Field<int>("LoginID") equals LoginRows2.Field<int>("LoginID")
                                     where (LoginRows2.Field<int>("CompanyID") == _CompanyID &&
                                     LoginRows2.Field<int>("siteID") == _SiteID &&
                                     StaffLoginRows1.Field<int>("staffroleID") == 2)

                                     select new
                                     {
                                         TrainerName = StaffLoginRows1.Field<string>("FirstName") + " " + StaffLoginRows1.Field<string>("LastName")
                                         , StaffID = StaffLoginRows1.Field<int>("StaffId")
                                         , StaffRoleID = StaffLoginRows1.Field<int>("StaffRoleId")
                                     };

                        foreach (var v in result)
                        {
                            DataRow dr = dtResult.NewRow();
                            dr["TrainerName"] = v.TrainerName;
                            dr["StaffID"] = v.StaffID;
                            dr["StaffRoleId"] = v.StaffRoleID;
                            dtResult.Rows.Add(dr);
                        }
                        ds.Tables.Add(dtResult);
                    }
                    return ds;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message).ToString();
                    return null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet get_PersonalTrainer_TaskFilter(int _Flag = 0)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                DataSet ds;
                if (Constants._OfflineFlag)
                {
                    Hashtable ht = new Hashtable();
                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@SiteID", _SiteID);
                    ht.Add("@Flag", _Flag);

                    ds = sql.ExecuteProcedure("GetPersonalTrainer_TaskFilter", ht);
                }
                else
                {
                    ds = null;
                }
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet get_AllActiveStaff()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                DataSet ds;
                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                ds = sql.ExecuteProcedure("GetAllActiveStaff", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet CheckIfDuplicateCardNo(int _CompanyID, int _SiteID, string _CardNo, string _MemberNo)
        {
            try
            {
                DataSet ds;
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                if (Constants._OfflineFlag)
                {
                    Hashtable ht = new Hashtable();
                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@SiteID", _SiteID);
                    ht.Add("@CardNo", _CardNo);
                    ht.Add("@MemberNo", _MemberNo);

                    ds = sql.ExecuteProcedure("CheckIfDuplicateCardNo", ht);
                }
                else
                {
                    ds = null;
                }
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet get_InvolvementType()
        {
            try
            {
                DataSet ds;

                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                if (Constants._OfflineFlag)
                { 
                    Hashtable ht = new Hashtable();
                    ht.Add("@CompanyID", _CompanyID);

                    ds = sql.ExecuteProcedure("GetInvolvementType", ht);
                }
                else
                {
                    ds = new DataSet();
                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("InvolvementTypeID", typeof(int));
                    dtResult.Columns.Add("InvolvementTypeName", typeof(string));

                    var result = from InvolvementType in Constants._DisConnectedALLTables.Tables["InvolvementType"].AsEnumerable()
                                 where (InvolvementType.Field<int>("CompanyID") == _CompanyID)

                                 select InvolvementType;

                    foreach (var v in result)
                    {
                        DataRow dr = dtResult.NewRow();
                        dr["InvolvementTypeID"] = v.Field<int>("InvolvementTypeID");
                        dr["InvolvementTypeName"] = v.Field<string>("InvolvementTypeName");
                        dtResult.Rows.Add(dr);
                    }
                    ds.Tables.Add(dtResult);
                }
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet get_ProgramGroup()
        {
            try
            {
                DataSet ds;

                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                if (Constants._OfflineFlag)
                { 
                    Hashtable ht = new Hashtable();
                    ht.Add("@CompanyID", _CompanyID);

                    ds = sql.ExecuteProcedure("GetALLProgrammeGroup", ht);
                }
                else
                {
                    ds = new DataSet();
                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("GroupsID", typeof(int));
                    dtResult.Columns.Add("GroupsName", typeof(string));
                    dtResult.Columns.Add("Abbreviation", typeof(string));

                    var result = from prgGroups in Constants._DisConnectedALLTables.Tables["ProgrammeGroups"].AsEnumerable()
                                 where (prgGroups.Field<int>("CompanyID") == _CompanyID)
                                 orderby prgGroups.Field<int>("GroupsID") ascending

                                 select prgGroups;

                    foreach (var v in result)
                    {
                        DataRow dr = dtResult.NewRow();
                        dr["GroupsID"] = v.Field<int>("GroupsID");
                        dr["GroupsName"] = v.Field<string>("GroupsName");
                        dr["Abbreviation"] = v.Field<string>("Abbreviation");
                        dtResult.Rows.Add(dr);
                    }
                    ds.Tables.Add(dtResult);
                }
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet get_AccessTypes()
        {
            try
            {
                DataSet ds;
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                if (Constants._OfflineFlag)
                {
                    Hashtable ht = new Hashtable();
                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@SiteID", _SiteID);

                    ds = sql.ExecuteProcedure("GetAccessTypes", ht);
                }
                else
                {
                    ds = new DataSet();
                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("ID", typeof(long));
                    dtResult.Columns.Add("AccessTypeName", typeof(string));

                    var result = from accessTypes in Constants._DisConnectedALLTables.Tables["AccessTypes"].AsEnumerable()
                                 where (accessTypes.Field<int>("CompanyID") == _CompanyID && accessTypes.Field<int>("SiteID") == _SiteID)

                                 select accessTypes;

                    foreach (var v in result)
                    {
                        DataRow dr = dtResult.NewRow();
                        dr["ID"] = v.Field<long>("ID");
                        dr["AccessTypeName"] = v.Field<string>("AccessTypeName");
                        dtResult.Rows.Add(dr);
                    }
                    ds.Tables.Add(dtResult);
                }
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet get_ALLProgramGroup()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetALLProgrammeGroup", ht);
                //DataSet ds = Services.get_ProgramGroup();
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet get_Program()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetProgramme", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet get_ProgramFindMember()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                DataSet ds = sql.ExecuteProcedure("GetProgrammeFindMember", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet get_ProgramByGroup(int ProgramGroupId, int _Status = 1)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@ProgramGroupId", ProgramGroupId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@Status", _Status);

                DataSet ds = sql.ExecuteProcedure("GetProgrammebyGroup", ht);
                //DataSet ds = Services.get_ProgramByGroup(ProgramGroupId);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet get_ProgrammeDetails(int ProgramId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@ProgramId", ProgramId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetProgrammeDetails", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet get_CardNo()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                DataSet ds = sql.ExecuteProcedure("GetMemberCardNo", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet get_MemberNo()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                DataSet ds = sql.ExecuteProcedure("GetMemberNo", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        
        public int add_Membership(DataTable addMembership)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                int Membershipresult = 0;
                for (int i = 0; i <= addMembership.Rows.Count - 1; i++)
                {
                    if ((Convert.ToInt32(addMembership.Rows[i]["Existing"]) == 0) || (Convert.ToInt32(addMembership.Rows[i]["Existing"]) != 0)) 
                    //IF EXISTING - UPDATE; IF NEW - INSERT
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("@programmegroupId", Convert.ToInt32(addMembership.Rows[i]["ProgramGroupValue"].ToString().Trim()));
                        ht.Add("@programmeId", Convert.ToInt32(addMembership.Rows[i]["ProgramValue"].ToString().Trim()));
                        ht.Add("@state", addMembership.Rows[i]["State"].ToString().Trim());
                        ht.Add("@programStartDate", Convert.ToDateTime(addMembership.Rows[i]["StartDate"].ToString().Trim()));
                        ht.Add("@programEndDate", Convert.ToDateTime(addMembership.Rows[i]["EndDate"].ToString().Trim()));
                        ht.Add("@memberID", Convert.ToInt32(addMembership.Rows[i]["MemberId"]).ToString().Trim());
                        ht.Add("@cardnumber", Convert.ToInt64(addMembership.Rows[i]["CardNo"]).ToString().Trim());
                        ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                        ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                        ht.Add("@AccessType", addMembership.Rows[i]["AccessType"].ToString().Trim());
                        ht.Add("@FullCost", Convert.ToDecimal(addMembership.Rows[i]["FullCost"].ToString().Trim()));
                        ht.Add("@SignUpFee", Convert.ToDecimal(addMembership.Rows[i]["SignUpFee"].ToString().Trim()));
                        ht.Add("@Existing", Convert.ToInt32(addMembership.Rows[i]["Existing"].ToString()));
                        ht.Add("@isUpFront", Convert.ToBoolean(addMembership.Rows[i]["isUpFront"].ToString()));
                        ht.Add("@isNeverExpire", Convert.ToBoolean(addMembership.Rows[i]["isNeverExpire"].ToString()));
                        ht.Add("@UniqueID", Convert.ToInt32(addMembership.Rows[i]["UniqueID"].ToString()));

                        Membershipresult += sql.ExecuteQuery("SaveMembership", ht);
                    }
                }
                return Membershipresult;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }

        public DataSet get_MembershipOnExistingMember(int MemberId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@MemberId", MemberId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                //DataSet ds = sql.ExecuteProcedure("ShowMembershipOnExistingMember", ht);
                DataSet ds = sql.ExecuteProcedure("ShowMembershipOnExistingMember_NEW", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public int add_MemberExtraInfo(MembersAddlInfoDTO membersAddlInfoDTO)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();

                ht.Add("@MemberId", membersAddlInfoDTO.MemberId);
                ht.Add("@FirstName", membersAddlInfoDTO.FirstName);
                ht.Add("@HomePhone", membersAddlInfoDTO.HomePhone);
                ht.Add("@CellPhone", membersAddlInfoDTO.CellPhone);
                ht.Add("@WorkPhone", membersAddlInfoDTO.WorkPhone);

                ht.Add("@AltContactName", membersAddlInfoDTO.AlternateContactName);
                ht.Add("@AltHomePhone", membersAddlInfoDTO.AlternateHomePhone);
                ht.Add("@AltCellPhone", membersAddlInfoDTO.CellPhone);
                ht.Add("@AltWorkPhone", membersAddlInfoDTO.AlternateWorkPhone);

                ht.Add("@MedicalCondition", membersAddlInfoDTO.MedCondition);
                ht.Add("@DoctorName", membersAddlInfoDTO.DoctorName);
                ht.Add("@DoctorPhone", membersAddlInfoDTO.DoctorPhone);
                ht.Add("@Medication", membersAddlInfoDTO.Medication);
                ht.Add("@DateOfJoin", membersAddlInfoDTO.JoiningDt);

                ht.Add("@ReferredBy", membersAddlInfoDTO.ReferredBy);
                ht.Add("@JoiningPromotion", membersAddlInfoDTO.JoiningPromotion);
                ht.Add("@ClientMgr", membersAddlInfoDTO.ClientMgr);
                ht.Add("@LastUpdate", membersAddlInfoDTO.LastUpdate);

                ht.Add("@PostalAddress", membersAddlInfoDTO.PostalAddress);
                ht.Add("@BillingAddress", membersAddlInfoDTO.BillingAddress);
                ht.Add("@MemberAge", membersAddlInfoDTO.Age);

                ht.Add("@Email", membersAddlInfoDTO.Email);
                ht.Add("@password", membersAddlInfoDTO.Password);
                ht.Add("@IsReceiveEmail", membersAddlInfoDTO.IsReceiveEmail);
                ht.Add("@IsReceiveLetter", membersAddlInfoDTO.IsReceiveLetter);
                ht.Add("@IsReceiveSMS", membersAddlInfoDTO.IsReceiveSMS);
                ht.Add("@IsReceivePushNotification", membersAddlInfoDTO.IsReceivePushNotification);

                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                ht.Add("@MedRec_FileName", membersAddlInfoDTO.MedRec_FileName);
                ht.Add("@MedRec_Ext", membersAddlInfoDTO.MedRec_Ext);
                ht.Add("@MedRec_ContentType", membersAddlInfoDTO.MedRec_ContentType);
                ht.Add("@MedRec_FileData", membersAddlInfoDTO.MedRec_FileData);

                int result = sql.ExecuteQuery("SaveMemberAdditionalInfo", ht);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }

        }

        public DataSet get_BusinessPromotionDetails()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                DataSet ds = sql.ExecuteProcedure("GetBusinessPromotionDetails", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }


        public DataSet get_ExtraDetailsByMemberId(int MemberId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@MemberId", MemberId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetExtraDetailsByMemberId", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet get_LastUsedNo(string FieldName)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@fieldName", FieldName);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetLastUsedNo", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet get_LastUsedNoForCompanySite(string FieldName, int CompanyID, int SiteID)
        {
            try
            {
                DataSet ds;
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                if (Constants._OfflineFlag)
                {
                    Hashtable ht = new Hashtable();
                    ht.Add("@fieldName", FieldName);
                    ht.Add("@CompanyID", CompanyID);
                    ht.Add("@SiteID", SiteID);
                    ds = sql.ExecuteProcedure("GetLastUsedNoForCompanySite", ht);
                }
                else
                {
                    ds = new DataSet();
                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("LastUsedNo", typeof(long));

                    var result = from lastUsedNo in Constants._DisConnectedALLTables.Tables["AutoGeneratedNumbers"].AsEnumerable()
                                 where (lastUsedNo.Field<int>("CompanyID") == CompanyID && lastUsedNo.Field<int>("SiteID") == SiteID && lastUsedNo.Field<string>("FieldName") == FieldName)

                                 select lastUsedNo;

                    foreach (var v in result)
                    {
                        DataRow dr = dtResult.NewRow();
                        dr["LastUsedNo"] = v.Field<long>("LastUsedNo") + 1;
                        dtResult.Rows.Add(dr);
                    }
                    ds.Tables.Add(dtResult);
                }
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet get_PaymentType()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                DataSet ds = sql.ExecuteProcedure("GetPaymentType", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet get_ALLMemberDetails(int _SiteID = 0)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                if (_SiteID == 0)
                {
                    ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                }
                else
                {
                    ht.Add("@SiteID", _SiteID);
                }

                //DataSet ds = sql.ExecuteProcedure("GetALLMemberDetails_P", ht);
                DataSet ds = sql.ExecuteProcedure("GetALLMemberDetails_P", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet get_PendingInvoiceDetailsByMemberID(int MemberId, char _isProd)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@MemberId", MemberId);
                ht.Add("@isProd", _isProd);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetInvoiceDetailsByMemberId", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        
        public int add_MembershipPayments(DateTime PaymentDate, int PaymentMode, decimal PaidAmount, string PaymentNotes, string PaymentInvoiceNo, int InvoiceDetailsID)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@PaymentDate", PaymentDate);
                ht.Add("@PaymentMode", PaymentMode);
                ht.Add("@PaidAmount", PaidAmount);
                ht.Add("@PaymentNotes", PaymentNotes);
                ht.Add("@PaymentInvoiceNo", PaymentInvoiceNo);
                ht.Add("@InvoiceDetailsID", InvoiceDetailsID);
                ht.Add("@CompanyID", Convert.ToInt32( ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                int result = sql.ExecuteQuery("AddMembershipPayments", ht);
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }

        public DataSet get_InvoiceNoByMemberId(int MemberId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@MemberId", MemberId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                DataSet ds = sql.ExecuteProcedure("GetInvoiceNoByMemberId", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public int raise_Invoice(DateTime InvoiceDate, int InvoiceNo, decimal InvoiceAmount, decimal AmountPaid, decimal OutstandingAmt, int MemberId, string InvoiceStatus, string FieldNameType)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();

                ht.Add("@InvoiceDate", InvoiceDate);
                ht.Add("@InvoiceNo", InvoiceNo);
                ht.Add("@InvoiceAmt", InvoiceAmount);
                ht.Add("@AmountPaid", AmountPaid);
                ht.Add("@AmountOutstanding", OutstandingAmt);
                ht.Add("@MemberId", MemberId);
                ht.Add("@InvoiceStatus", InvoiceStatus);
                ht.Add("@FieldNameType", FieldNameType);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                ht.Add("@UserName", ConfigurationManager.AppSettings["LoggedInUser"].ToString());

                int result = sql.ExecuteQuery("RaiseInvoice", ht);
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }

        }
        
        public int AddInvoiceDetails(DataTable addInvoiceDetails)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                int result = 0;
                for (int i = 0; i < addInvoiceDetails.Rows.Count; i++)
                {
                    Hashtable ht = new Hashtable();

                    ht.Add("@InvoiceNo", Convert.ToInt32(addInvoiceDetails.Rows[i]["InvoiceNo"].ToString()));
                    ht.Add("@MemberId", Convert.ToInt32(addInvoiceDetails.Rows[i]["MemberNo"].ToString()));
                    ht.Add("@InvoiceDetails", addInvoiceDetails.Rows[i]["InvoiceDetails"].ToString());
                    ht.Add("@InvoiceDetailsAmt", Convert.ToDecimal(addInvoiceDetails.Rows[i]["Amount"].ToString()));
                    ht.Add("@InvoiceStatus", addInvoiceDetails.Rows[i]["Status"].ToString());
                    ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"]) );
                    ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"]));
                    ht.Add("@ProdSale", Convert.ToChar(addInvoiceDetails.Rows[i]["ProdSale"].ToString()));

                    result = sql.ExecuteQuery("AddInvoiceDetails", ht);
                }

                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }

        }
        public DataSet get_AllMemberDetailsAndAlsoByMemberId(int _MemberId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                DataSet ds;
                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                if (Constants._OfflineFlag)
                {
                    Hashtable ht = new Hashtable();
                    ht.Add("@MemberId", _MemberId);
                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@SiteID", _SiteID);

                    ds = sql.ExecuteProcedure("GetAllMemberDetailsAndAlsoByMemberId", ht);
                }
                else
                {
                    ds = new DataSet();
                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("MemberNo", typeof(int));
                    dtResult.Columns.Add("FirstName", typeof(string));
                    dtResult.Columns.Add("LastName", typeof(string));
                    dtResult.Columns.Add("DateofBirth", typeof(string));
                    dtResult.Columns.Add("Gender", typeof(int));
                    dtResult.Columns.Add("Street", typeof(string));
                    dtResult.Columns.Add("Suburb", typeof(string));
                    dtResult.Columns.Add("City", typeof(string));
                    dtResult.Columns.Add("State", typeof(string));
                    dtResult.Columns.Add("PostalCode", typeof(int));
                    dtResult.Columns.Add("HomePhone", typeof(string));
                    dtResult.Columns.Add("MobilePhone", typeof(string));
                    dtResult.Columns.Add("Notes", typeof(string));
                    dtResult.Columns.Add("CardNumber", typeof(long));
                    dtResult.Columns.Add("WorkPhone", typeof(string));
                    dtResult.Columns.Add("Occupation", typeof(string));
                    dtResult.Columns.Add("Organisation", typeof(string));
                    dtResult.Columns.Add("InvolvementType", typeof(int));
                    dtResult.Columns.Add("PersonalTrainer", typeof(int));
                    dtResult.Columns.Add("EmailAddress", typeof(string));
                    dtResult.Columns.Add("CreatedDate", typeof(DateTime));
                    dtResult.Columns.Add("CompanyId", typeof(int));
                    dtResult.Columns.Add("MemberProfilePhotoPath", typeof(string));
                    dtResult.Columns.Add("MemberPic", typeof(byte[]));
                    dtResult.Columns.Add("Owing", typeof(decimal));

                    var result = from MemberDetails in Constants._DisConnectedALLTables.Tables["MemberDetails"].AsEnumerable()
                                 where (MemberDetails.Field<int>("CompanyID") == _CompanyID &&
                                 MemberDetails.Field<int>("SiteID") == _SiteID &&
                                 MemberDetails.Field<int>("MemberNo") == _MemberId)

                                 select MemberDetails;

                    var osResult = from Invoice in Constants._DisConnectedALLTables.Tables["Invoice"].AsEnumerable()
                                   where (Invoice.Field<int>("CompanyID") == _CompanyID &&
                                   Invoice.Field<int>("SiteID") == _SiteID &&
                                   Invoice.Field<int>("MemberID") == _MemberId)
                                   group Invoice by new { CompanyID = Invoice.Field<int>("CompanyID"), SiteID = Invoice.Field<int>("SiteID"), MemberID = Invoice.Field<int>("MemberID") } into grp

                                   select new
                                   { Owing = grp.Sum(r => r.Field<decimal>("AmountOutStanding")) };

                    string _NullCheck;
                    foreach (var v in result)
                    {
                        DataRow dr = dtResult.NewRow();
                        dr["MemberNo"] = v.Field<int>("MemberNo");
                        dr["FirstName"] = (v.Field<object>("FirstName") + "").ToUpper();
                        dr["LastName"] = (v.Field<object>("LastName")+"").ToUpper();
                        dr["DateofBirth"] = v.Field<DateTime>("DateofBirth").ToString("dd/MM/yyyy");
                        dr["Gender"] = v.Field<int>("Gender");
                        dr["Street"] = (v.Field<object>("Street") + "").ToUpper();
                        dr["Suburb"] = (v.Field<object>("Suburb") + "").ToUpper();
                        dr["City"] = (v.Field<object>("City") + "").ToUpper();

                        _NullCheck = v.Field<object>("State") + "";
                        if (_NullCheck == "")
                        {
                            dr["State"] = "";
                        }
                        else
                        { 
                            dr["State"] = v.Field<string>("State").ToUpper();
                        }
                        _NullCheck = v.Field<object>("PostalCode") + "";
                        if (_NullCheck == "") { dr["PostalCode"] = 0; }
                        else {
                            dr["PostalCode"] = v.Field<int>("PostalCode");
                        }
                        dr["HomePhone"] = (v.Field<object>("HomePhone") + "").ToUpper();
                        dr["MobilePhone"] = (v.Field<object>("MobilePhone") + "").ToUpper();
                        dr["Notes"] = (v.Field<object>("Notes") + "").ToUpper();

                        _NullCheck = v.Field<object>("CardNumber") + "";
                        if (_NullCheck == "")
                        {
                            dr["CardNumber"] = 0;
                        }
                        else
                        {
                            dr["CardNumber"] = v.Field<long>("CardNumber");
                        }
                        dr["WorkPhone"] = (v.Field<object>("WorkPhone") + "").ToUpper();
                        dr["Occupation"] = (v.Field<object>("Occupation") + "").ToUpper();
                        dr["Organisation"] = (v.Field<object>("Organisation") + "").ToUpper();
                        dr["InvolvementType"] = Convert.ToInt32(v.Field<string>("InvolvementType"));
                        dr["PersonalTrainer"] = Convert.ToInt32(v.Field<string>("PersonalTrainer"));
                        dr["EmailAddress"] = (v.Field<object>("EmailAddress") + "").ToUpper();

                        _NullCheck = v.Field<object>("CreatedDate") + "";
                        if (_NullCheck == "")
                        {
                            dr["CreatedDate"] = DateTime.Now;
                        }
                        else
                        {
                            dr["CreatedDate"] = v.Field<DateTime>("CreatedDate");
                        }
                        dr["CompanyId"] = v.Field<int>("CompanyId");
                        dr["MemberProfilePhotoPath"] = (v.Field<object>("MemberProfilePhotoPath") + "").ToUpper();
                        dr["MemberPic"] = v.Field<byte[]>("MemberPic");
                        dr["Owing"] = 0m;

                        foreach (var os in osResult)
                        {
                                dr["Owing"] = os.Owing;
                        }
                        dtResult.Rows.Add(dr);
                    }
                    ds.Tables.Add(dtResult);
                }
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public int AddMemberHold 
            (
                int HoldId, DateTime StartDate, DateTime EndDate, string Reason, int FeeType, int MemberId, int CompanyId, 
                bool EndHoldMember, bool ProrataMember, int Status, Decimal FeeAmount, int ProgramId, bool IsFreeTime
                , int MSUniqueID = 0
            )
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();

                ht.Add("@HoldId", Convert.ToInt32(HoldId));
                ht.Add("@HoldStartDate", Convert.ToDateTime(StartDate));
                ht.Add("@HoldEndDate", Convert.ToDateTime(EndDate));
                ht.Add("@HoldReason", Reason.ToString());
                ht.Add("@HoldFeeType", FeeType.ToString());
                ht.Add("@MemberId", Convert.ToInt32(MemberId.ToString()));
                ht.Add("@CompanyID", Convert.ToInt32(CompanyId.ToString()));
                ht.Add("@EndHoldMember", Convert.ToBoolean(EndHoldMember.ToString()));
                ht.Add("@ProrataMember", Convert.ToBoolean(ProrataMember.ToString()));
                ht.Add("@HoldStatus", Convert.ToInt32(Status.ToString()));
                ht.Add("@FeeAmount", Convert.ToDecimal(FeeAmount.ToString()));
                ht.Add("@ProgramId", Convert.ToDecimal(ProgramId.ToString()));
                ht.Add("@IsFreeTime", Convert.ToBoolean(IsFreeTime.ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                ht.Add("@StaffMember", ConfigurationManager.AppSettings["LoggedInUser"].ToString().ToUpper());
                ht.Add("@MSUniqueID", MSUniqueID);

                int result = sql.ExecuteQuery("AddMemberHoldDetails", ht);
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }

        public int MemberHoldImmediate(int _CompanyID, int _SiteID, int _MemberNo, int _ProgramID, int _Immediate = 0)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@MemberId", _MemberNo);
                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@ProgramId", _ProgramID);
                ht.Add("@SiteID", _SiteID);
                ht.Add("@Immediate", _Immediate);

                int result = sql.ExecuteQuery("UpdateMembershipMemberHoldImmediate", ht);

                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        public DataSet get_MemberHoldDetails(int MemberId, int CompanyId, int ProgramID = 0, int MSUniqueID = 0)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@MemberId", MemberId);
                ht.Add("@CompanyID", CompanyId);
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                ht.Add("@ProgramID", ProgramID);
                ht.Add("@MSUniqueID", MSUniqueID);

                DataSet ds = sql.ExecuteProcedure("GetMemberHoldDetails", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

         public int delete_MemberHoldDetails(int HoldId, int CompanyId, int HoldStatus)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@HoldId", HoldId);
                ht.Add("@CompanyId", CompanyId);
                ht.Add("@HoldStatus", HoldStatus);
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                ht.Add("@Staff", ConfigurationManager.AppSettings["LoggedInUser"].ToString().ToUpper());

                int result = sql.ExecuteQuery("DeleteMemberHoldDetails", ht);
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }

        public int add_RemoveMemberHoldDays(int NoOfDays, int HoldId, int CompanyId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@NoOfDays", NoOfDays);
                ht.Add("@HoldId", HoldId);
                ht.Add("@CompanyId", CompanyId);
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                int result = sql.ExecuteQuery("AddRemoveMemberHoldDays", ht);
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }

         public DataSet get_FreeTimeDetails(int HoldId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@HoldId", HoldId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"]));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"]));

                DataSet ds = sql.ExecuteProcedure("GetFreeTimeDetails", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet Get_ActiveMembership(int _MemberId, int _CompanyId, int _SiteId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                DataSet ds;
                if (Constants._OfflineFlag)
                {
                    Hashtable ht = new Hashtable();
                    ht.Add("@MemberId", _MemberId);
                    ht.Add("@CompanyId", _CompanyId);
                    ht.Add("@SiteId", _SiteId);

                    ds = sql.ExecuteProcedure("GetActiveMembership", ht);
                }
                else
                {
                    ds = new DataSet();
                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("ProgrammeName", typeof(string));
                    dtResult.Columns.Add("ProgrammeID", typeof(int));

                    var result = from activeMembership in Constants._DisConnectedALLTables.Tables["Programme"].AsEnumerable()
                                 join memberShip in Constants._DisConnectedALLTables.Tables["Memberships"].AsEnumerable()
                                 on activeMembership.Field<int>("ProgrammeID") equals memberShip.Field<int>("Programme")
                                 where (memberShip.Field<int>("CompanyID") == _CompanyId && activeMembership.Field<int>("CompanyID") == _CompanyId &&
                                 memberShip.Field<int>("siteID") == _SiteId && memberShip.Field<long>("MemberID") == _MemberId)

                                 select activeMembership;

                    foreach (var v in result)
                    {
                        DataRow dr = dtResult.NewRow();
                        dr["ProgrammeName"] = v.Field<string>("ProgrammeName");
                        dr["ProgrammeID"] = v.Field<int>("ProgrammeID");
                        dtResult.Rows.Add(dr);
                    }
                    ds.Tables.Add(dtResult);
                }
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public int Update_UsedVisits(int MemberId, int CompanyId, int SiteId, int Visits, int ProgrammeId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@MemberId", MemberId);
                ht.Add("@CompanyId", CompanyId);
                ht.Add("@SiteId", SiteId);
                ht.Add("@Visits", Visits);
                ht.Add("@Programme", ProgrammeId);
                int result = sql.ExecuteQuery("UpdateUsedVisits", ht);
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }

        public DataSet Get_VisitsCount(int MemberId, int CompanyId, int SiteId, int ProgrammeId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@MemberId", MemberId);
                ht.Add("@CompanyId", CompanyId);
                ht.Add("@SiteId", SiteId);
                ht.Add("@Programme", ProgrammeId);
                DataSet ds = sql.ExecuteProcedure("GetVisitsCount", ht);
                //DataSet ds = Services.get_ProgramGroup();
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet SaveCheckInDetails(int MemberId, int CompanyId, int SiteId, int ProgrammeId, DateTime CheckInDate, string _Door, string _Type = "MANUAL", int _checkInPopup = 0)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                int _checkedInSiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"]);

                Hashtable ht = new Hashtable();
                ht.Add("@MemberId", MemberId);
                ht.Add("@CompanyId", CompanyId);
                ht.Add("@SiteId", SiteId);
                ht.Add("@Programme", ProgrammeId);
                ht.Add("@CheckInDate", CheckInDate);
                ht.Add("@Door", _Door);
                ht.Add("@checkedInSiteID", _checkedInSiteID);
                ht.Add("@TypeOfCheckIn", _Type);
                ht.Add("@CheckInPopup", _checkInPopup);

                DataSet ds = sql.ExecuteProcedure("SaveCheckInDetails", ht);
                //DataSet ds = Services.get_ProgramGroup();
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet GetCheckInDetails(int MemberId, int CompanyId, int SiteId, int ProgrammeId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@MemberId", MemberId);
                ht.Add("@CompanyId", CompanyId);
                ht.Add("@SiteId", SiteId);
                ht.Add("@Programme", ProgrammeId);

                DataSet ds = sql.ExecuteProcedure("GetCheckInDetails", ht);
                //DataSet ds = Services.get_ProgramGroup();
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public int SaveUpdateProspects_SaveCheckIn( ProspectsDTO _ProspectDTO)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@ProspectID", _ProspectDTO.ProspectID);
                ht.Add("@CompanyID", _ProspectDTO.CompanyID);
                ht.Add("@SiteID", _ProspectDTO.SiteID);
                ht.Add("@FirstName", _ProspectDTO.FirstName);
                ht.Add("@LastName", _ProspectDTO.LastName);
                ht.Add("@DateofBirth", _ProspectDTO.DateofBirth);
                ht.Add("@Gender", _ProspectDTO.Gender);
                ht.Add("@Street", _ProspectDTO.Street);
                ht.Add("@Suburb", _ProspectDTO.Suburb);
                ht.Add("@State", _ProspectDTO.State);
                ht.Add("@PostalCode", _ProspectDTO.PostalCode);
                ht.Add("@HomePhone", _ProspectDTO.HomePhone);
                ht.Add("@MobilePhone", _ProspectDTO.MobilePhone);
                ht.Add("@Notes", _ProspectDTO.Notes);
                ht.Add("@AssignedTo", _ProspectDTO.AssignedTo);
                ht.Add("@ContactMethod", _ProspectDTO.ContactMethod);
                ht.Add("@SourcePromotion", _ProspectDTO.SourcePromotion);
                ht.Add("@ReferredByID", _ProspectDTO.ReferredBy);
                ht.Add("@FitnessGoal", _ProspectDTO.FitnessGoal);
                ht.Add("@PreviousGym", _ProspectDTO.PreviousGym);
                ht.Add("@EmailID", _ProspectDTO.EmailId);
                ht.Add("@LeadStrength", _ProspectDTO.LeadStrength);
                ht.Add("@LeadCreated", _ProspectDTO.LeadCreated);
                ht.Add("@Visits", _ProspectDTO.Visits);
                ht.Add("@NotInterested", _ProspectDTO.NotInterested);
                ht.Add("@Convert", _ProspectDTO.Convert);
                ht.Add("@Exercising", _ProspectDTO.Exercising);
                ht.Add("@AssignedToStaffID", _ProspectDTO.AssignedToStaffID);
                ht.Add("@Door", _ProspectDTO.Door);
            
                int result = sql.ExecuteQuery("SaveUpdateProspects_SaveCheckIn", ht);
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }

        public int SaveUpdateProspects_SaveOnly(ProspectsDTO _ProspectDTO)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@ProspectID", _ProspectDTO.ProspectID);
                ht.Add("@CompanyID", _ProspectDTO.CompanyID);
                ht.Add("@SiteID", _ProspectDTO.SiteID);
                ht.Add("@FirstName", _ProspectDTO.FirstName);
                ht.Add("@LastName", _ProspectDTO.LastName);
                ht.Add("@DateofBirth", _ProspectDTO.DateofBirth);
                ht.Add("@Gender", _ProspectDTO.Gender);
                ht.Add("@Street", _ProspectDTO.Street);
                ht.Add("@Suburb", _ProspectDTO.Suburb);
                ht.Add("@State", _ProspectDTO.State);
                ht.Add("@PostalCode", _ProspectDTO.PostalCode);
                ht.Add("@HomePhone", _ProspectDTO.HomePhone);
                ht.Add("@MobilePhone", _ProspectDTO.MobilePhone);
                ht.Add("@Notes", _ProspectDTO.Notes);
                ht.Add("@AssignedTo", _ProspectDTO.AssignedTo);
                ht.Add("@ContactMethod", _ProspectDTO.ContactMethod);
                ht.Add("@SourcePromotion", _ProspectDTO.SourcePromotion);
                ht.Add("@ReferredByID", _ProspectDTO.ReferredBy);
                ht.Add("@FitnessGoal", _ProspectDTO.FitnessGoal);
                ht.Add("@PreviousGym", _ProspectDTO.PreviousGym);
                ht.Add("@EmailID", _ProspectDTO.EmailId);
                ht.Add("@LeadStrength", _ProspectDTO.LeadStrength);
                ht.Add("@LeadCreated", _ProspectDTO.LeadCreated);
                ht.Add("@Visits", _ProspectDTO.Visits);
                ht.Add("@NotInterested", _ProspectDTO.NotInterested);
                ht.Add("@Convert", _ProspectDTO.Convert);
                ht.Add("@Exercising", _ProspectDTO.Exercising);
                ht.Add("@AssignedToStaffID", _ProspectDTO.AssignedToStaffID);
                ht.Add("@Door", _ProspectDTO.Door);

                int result = sql.ExecuteQuery("SaveUpdateProspects_SaveOnly", ht);
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }
        public DataSet GetNotesForMember(int _CompanyID, int _SiteID, int _MemberNo)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                
                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                ht.Add("@MemberNo", _MemberNo);
                DataSet ds = sql.ExecuteProcedure("GetNotesForMember", ht);

                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet GetFormsForMember(int _CompanyID, int _SiteID, int _MemberNo)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();

                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                ht.Add("@MemberNo", _MemberNo);
                DataSet ds = sql.ExecuteProcedure("GetFormsForMember", ht);

                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public int SaveFormsForMember(int _CompanyID, int _SiteID, int _MemberID, DataTable _dtForms)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                ht.Add("@MemberID", _MemberID);
                ht.Add("@Forms_Table", _dtForms);

                int result = sql.ExecuteQuery("SaveFormsForMember", ht);
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }

        public int RemoveFormsForMember(int _CompanyID, int _SiteID, int _MemberID)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                ht.Add("@MemberID", _MemberID);

                int result = sql.ExecuteQuery("RemoveFormsForMember", ht);
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }
    }
}
