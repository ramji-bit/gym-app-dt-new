﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections;
using GymMembership.DTO;
using System.Web.Services;
using System.Windows.Forms;


namespace GymMembership.DAL
{
   public class BillingDAL
    {

        //GymAppWebService.GymAppWebServices Services = new GymAppWebService.GymAppWebServices();

        public DataSet get_BillingHistoryByMemberId(int MemberId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@MemberId", MemberId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                DataSet ds = sql.ExecuteProcedure("GetBillingHistoryByMemberId", ht);

                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }


        public DataSet get_BillingCompany()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                DataSet ds = sql.ExecuteProcedure("GetMemberBillingCompany", ht);

                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }


        public DataSet get_BankDetailsByMemberId(int MemberId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@MemberId", MemberId);
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                DataSet ds = sql.ExecuteProcedure("GetBankDetailByMemberId", ht);

                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet get_BankCardType()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                DataSet ds = sql.ExecuteProcedure("GetBankCardType", ht);

                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public int SaveBankDetails(MemberBankDetailsDTO memberBankDetailsDTO)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();
                ht.Add("@BankProcessID", memberBankDetailsDTO.BankProcessId);
                ht.Add("@BankPayerName", memberBankDetailsDTO.BankPayerName);
                ht.Add("@BankPayerAccount", memberBankDetailsDTO.BankPayerAccount);
                ht.Add("@BankPayerParticular", memberBankDetailsDTO.BankPayerParticulars);
                ht.Add("@BillingCompanyID", memberBankDetailsDTO.BillingCompanyId);
                ht.Add("@MaxMoneyCollected", memberBankDetailsDTO.MaximumMoneyCollected);

                ht.Add("@CreditCardName", memberBankDetailsDTO.CreditCardName);
                ht.Add("@CreditCardNumber", memberBankDetailsDTO.CreditCardNumber);
                ht.Add("@CreditExpireDate", memberBankDetailsDTO.CreditCardExpDate);
                ht.Add("@CreditCareType", memberBankDetailsDTO.CardType);
                ht.Add("@OverdueDeadLineDt", memberBankDetailsDTO.OverDueDeadLine);
                ht.Add("@MemberId", memberBankDetailsDTO.MemberId);
                ht.Add("@CompanyId", memberBankDetailsDTO.CompanyId);
                ht.Add("@ChkCardType", memberBankDetailsDTO.CheckCardType);
                
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                int result = sql.ExecuteQuery("SaveMemberBankDetails", ht);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }

        }

        public DataSet Get_PaymentDetails(int MemberId)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@AppMemberId", MemberId);
                DataSet ds = sql.ExecuteProcedure("GetPaymentDetails", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

    }
}
