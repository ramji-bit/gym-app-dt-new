﻿using System;
using System.Collections;
using System.Linq;
using System.Data;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.IO;
using System.Windows.Media.Imaging;
using System.Configuration;
using System.ComponentModel;
using System.Diagnostics;
using System.Net.Mail;

using GymMembership.Task;
using GymMembership.Comman;
using GymMembership.DTO;
using GymMembership.BAL;
using GymMembership.Navigation;

namespace GymMembership.Prospects
{
    /// <summary>
    /// Interaction logic for Prospect.xaml
    /// </summary>
    public partial class Prospect 
    {
        public string _MemberName;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public DataSet _dSAccessRights;
        public DataSet _ds;
        public string _appVersion = "";

        string _DoorToOpen = "";
        public bool _boolFromCasualMember = false;
        bool _isWindowLoaded = false;
        bool _isNewNotes = false;
        DataSet _dsNotes = new DataSet();
        DataSet _dsOption = new DataSet();
        DataSet _dsTasks = new DataSet();
        DataTable _dtNewOption = new DataTable();
        bool _isNewOption = true;
        int _TaskId = 0;
        int _commDetailsTable = 0;

        int _ProspectsTable = 0;
        bool _IsNewProspect = true;
        bool _defaultNotInterested = false;
        int _ProspectID = -1;
        int _ConvertMemberNo = 0;
        int _ConvertInvolvementType = 0;
        DataSet _dsDoors;
        Hashtable ht;

        public Prospect()
        {
            InitializeComponent();
        }
        private void Prospect_Loaded(object sender, RoutedEventArgs e)
        {
            this.Closing += new CancelEventHandler(Prospect_Closing);
            this.Owner.Hide();
            this.ShowInTaskbar = true;
            if (_boolFromCasualMember)
            {
                colProspectLeftPanel.Width = new GridLength(0);
                listBox.Visibility = Visibility.Collapsed;
                btnbacktoDashBoard.Visibility = Visibility.Collapsed;

                rowTopPanel.Height = new GridLength(0);
                brdCasualCheckIn.Visibility = Visibility.Collapsed;
                brdCasualTasks.Visibility = Visibility.Collapsed;
                colCasualCheckIn.Width = new GridLength(0);
                rowCasualTasks.Height = new GridLength(0);

                //this.MinWidth  = SystemParameters.PrimaryScreenWidth;
                //this.MinHeight = SystemParameters.PrimaryScreenHeight;
                ClearData();
                btnProspectDetailsSave.IsEnabled = false;
                chkProspectDetailsAgreeToTC.IsChecked = false;
                tbProspectHeader.Text = "Casual Member";
                this.Title = "Casual Member";

                ////ALTER MARGIN - FOR CASUAL MEMBER SCREEN
                double _rightMargin = 667;
                double _rightMarginOther = 17;
                txtProspectDetailsStreet.Margin = new Thickness(102, 10, _rightMargin, 0);
                txtProspectDetailsSuburb.Margin = new Thickness(102, 10, _rightMargin, 0);
                txtProspectDetailsNotes.Margin = new Thickness(102, 10, _rightMargin, 0);
                txtProspectEmail.Margin = new Thickness(102, 10, _rightMargin, 0);

                txtProspectDetailsLastName.Margin = new Thickness(102, 10, _rightMarginOther, 0);
                txtProspectPostCode.Margin = new Thickness(102, 10, _rightMarginOther, 0);
                txtProspectDetailsCellPhone.Margin = new Thickness(102, 10, _rightMarginOther, 0);
                ////ALTER MARGIN - FOR CASUAL MEMBER SCREEN
            }
            else
            {
                SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
                ClearData();
                populateData();
                chkOnlyRelevant.IsChecked = false;
                tiTasks.Visibility = Visibility.Visible;
                tbProspectHeader.Text = "Prospect Details";
                this.Title = "Prospects";
            }
            txtProspectDetailsFirstName.Focus();
            _isWindowLoaded = true;
        }
         
        void InvokeKeyPad()
        {
            if (_boolFromCasualMember)
            {
                string progFiles = @"C:\Program Files\Common Files\Microsoft Shared\ink";
                string onScreenKeyboardPath = Path.Combine(progFiles, "TabTip.exe");
                Process.Start(onScreenKeyboardPath);
            }
        }

        void KillKeyPad()
        {
            //Kill all on screen keyboards
            Process[] oskProcessArray = Process.GetProcessesByName("TabTip");
            foreach (Process onscreenProcess in oskProcessArray)
            {
                onscreenProcess.Kill();
            }
        }
        //void onScreenKeyPad()
        //{
        //    //string _filepath = Environment.GetFolderPath(Environment.SpecialFolder.System) + Path.DirectorySeparatorChar + "osk.exe";
        //    string progFiles = @"C:\Program Files\Common Files\Microsoft Shared\ink";
        //    string onScreenKeyboardPath = System.IO.Path.Combine(progFiles, "TabTip.exe");
        //    Process.Start(onScreenKeyboardPath);

        //    //Kill all on screen keyboards
        //    Process[] oskProcessArray = Process.GetProcessesByName("TabTip");
        //    foreach (Process onscreenProcess in oskProcessArray)
        //    {
        //        onscreenProcess.Kill();
        //    }
        //}
        void Prospect_Closing(object sender, CancelEventArgs e)
        {
            (this.Owner as Dashboard)._isDishonourCheckEnabled = true;
            this.Owner.Show();
        }
        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto(_StaffPic);
        }
        private void loadStaffPhoto(byte[] _StaffPic)
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }
        public void populateData()
        {
            try
            {
                Utilities _utilities = new Utilities();
                _utilities.populateAllActiveStaff(cmbProspectDetailsAssignedTo);
                _utilities.populateBusinessPromotionDetails(cmbProspectDetailsSourcePromotion);
                _utilities.populateMemberName(cmbProspectDetailsReferredbyID);

                cmbProspectDetailsLeadStrength.SelectedIndex = 0;

                //_utilities.GetAllContactTypes(cmbProspectDetailsContactMethod);
                if (cmbProspectDetailsContactMethod.Items.Count <= 0)
                {
                    cmbProspectDetailsContactMethod.ItemsSource = null;
                    populateContactTypes();
                    cmbProspectDetailsContactMethod.SelectedIndex = 0;
                }

                loadProspects();
                LoadDoors();
                txtProspectDetailsLeadCreated.Text = DateTime.Now.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private void loadProspects()
        {
            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return;

            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            _ds = new DataSet();
            _ds = sql.ExecuteProcedure("GetAllProspects", ht);

            gvEnquiries.ItemsSource = null;
            gvEnquiries.Items.Clear();
            chkOnlyRelevant.IsChecked = false;
            _ds.Tables[_ProspectsTable].DefaultView.RowFilter = "NotInterested <> 1";
            gvEnquiries.ItemsSource = _ds.Tables[_ProspectsTable].DefaultView;
        }

        public void populateContactTypes()
        {
            cmbProspectDetailsContactMethod.Items.Add("We Initiated Contact"); cmbProspectDetailsContactMethod.SelectedValue = 0;
            cmbProspectDetailsContactMethod.Items.Add("Phone Enquiry"); cmbProspectDetailsContactMethod.SelectedValue = 1;
            cmbProspectDetailsContactMethod.Items.Add("Web Enquiry"); cmbProspectDetailsContactMethod.SelectedValue = 2;
            cmbProspectDetailsContactMethod.Items.Add("Member Referred"); cmbProspectDetailsContactMethod.SelectedValue = 3;
            cmbProspectDetailsContactMethod.Items.Add("Other..."); cmbProspectDetailsContactMethod.SelectedValue = 4;
            cmbProspectDetailsContactMethod.SelectedIndex = 0;
        }

        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnProspectDetailsCheckIn_Click(object sender, RoutedEventArgs e)
        {
            if (validateData_CasualMember())
            {
                if (cmbProspectDetailsDoor.SelectedIndex == -1)
                {
                    MessageBox.Show("Select Door to Check-In!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    cmbProspectDetailsDoor.Focus();
                    return;
                }

                txtProspectDetailsLeadCreated.Text = DateTime.Now.ToString();

                ProspectsDTO _ProspectDTO = new ProspectsDTO();
                _ProspectDTO.AssignedTo = cmbProspectDetailsAssignedTo.Text.Trim().ToUpper();
                if (cmbProspectDetailsAssignedTo.SelectedIndex != -1)
                {
                    _ProspectDTO.AssignedToStaffID = Convert.ToInt32(cmbProspectDetailsAssignedTo.SelectedValue);
                }
                else
                {
                    _ProspectDTO.AssignedToStaffID = 0;
                }
                _ProspectDTO.CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                _ProspectDTO.ContactMethod = Convert.ToInt32(cmbProspectDetailsContactMethod.SelectedIndex);
                _ProspectDTO.Convert = false;
                _ProspectDTO.DateofBirth = Convert.ToDateTime(txtProspectDetailsDOB.Text.ToString());
                _ProspectDTO.Door = cmbProspectDetailsDoor.Text.Trim().ToUpper();
                _ProspectDTO.EmailId = txtProspectEmail.Text;
                _ProspectDTO.Exercising = Convert.ToBoolean(chkProspectDetailsAreyouExercising.IsChecked.ToString());
                _ProspectDTO.FirstName = txtProspectDetailsFirstName.Text.Trim().ToUpper();
                _ProspectDTO.FitnessGoal = txtProspectDetailsFitnessGoal.Text.ToUpper();

                int _gender;
                if (rbtnProspectDetailsFemale.IsChecked == true)
                    _gender = 2;
                else
                    _gender = 1;
                _ProspectDTO.Gender = _gender;

                _ProspectDTO.HomePhone = txtProspectDetailsHomePhone.Text.Trim();
                _ProspectDTO.LastName = txtProspectDetailsLastName.Text.Trim().ToUpper();
                _ProspectDTO.LeadCreated = DateTime.Now;
                _ProspectDTO.LeadStrength = cmbProspectDetailsLeadStrength.Text.Trim();
                _ProspectDTO.MobilePhone = txtProspectDetailsCellPhone.Text.Trim();
                _ProspectDTO.Notes = txtProspectDetailsNotes.Text.Trim();
                _ProspectDTO.NotInterested = _defaultNotInterested;
                _ProspectDTO.PostalCode = Convert.ToInt32(txtProspectPostCode.Text.Trim());
                _ProspectDTO.PreviousGym = txtProspectDetailsPreviousGym.Text.Trim();
                _ProspectDTO.ProspectID = _ProspectID;
                if (cmbProspectDetailsReferredbyID.SelectedIndex != -1)
                {
                    _ProspectDTO.ReferredBy = Convert.ToInt32(cmbProspectDetailsReferredbyID.SelectedValue.ToString());
                }
                else
                {
                    _ProspectDTO.ReferredBy = 0;
                }
                _ProspectDTO.SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                _ProspectDTO.SourcePromotion = cmbProspectDetailsSourcePromotion.Text.Trim();
                _ProspectDTO.State = txtProspectDetailsCity.Text.Trim();
                _ProspectDTO.Street = txtProspectDetailsStreet.Text.Trim();
                _ProspectDTO.Suburb = txtProspectDetailsSuburb.Text.Trim();
                if (txtProspectDetailsVisits.Text.Trim() == "")
                {
                    _ProspectDTO.Visits = 1;
                }
                else
                {
                    _ProspectDTO.Visits = Convert.ToInt32(txtProspectDetailsVisits.Text.Trim()) + 1;
                }

                if (_DoorToOpen.Trim() != "")
                {
                    string _successORErrMsg = Comman.Constants.Create_ACCESS_DOOR_File(_DoorToOpen);
                    if (_successORErrMsg == "1")
                    {
                        //MessageBox.Show("Door Access File Created!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        //MessageBox.Show("ERROR! File NOT Created!\nPlease ensure write permission to the path is properly set!\n" + _successORErrMsg, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Select a Door to Check-In!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

                MemberDetailsBAL _memberBAL = new MemberDetailsBAL();
                int _result = _memberBAL.SaveUpdateProspects_SaveCheckIn(_ProspectDTO);
                if (_result != 0)
                {
                    MessageBox.Show("Casual Member Checked-In!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    ClearData();
                    loadProspects();
                    return;
                }
                else
                {
                    MessageBox.Show("Error!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    ClearData();
                    loadProspects();
                    return;
                }
            }
        }

        private bool validateData()
        {
            try
            {
                FormUtilities _fUtil = new FormUtilities();

                if (txtProspectDetailsFirstName.Text.Trim() == "")
                {
                    MessageBox.Show("First Name cannot be Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsFirstName.Focus();
                    return false;
                }

                if (txtProspectDetailsFirstName.Text.All(c => char.IsNumber(c)))
                {
                    MessageBox.Show("First Name cannot be Numeric!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsFirstName.Focus();
                    return false;
                }

                if (txtProspectDetailsLastName.Text.Trim() == "")
                {
                    MessageBox.Show("Last Name cannot be Empty!", "Prospects", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsLastName.Focus();
                    return false;
                }

                if (txtProspectDetailsLastName.Text.All(c => char.IsNumber(c)))
                {
                    MessageBox.Show("Last Name cannot be Numeric!", "Prospects", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsLastName.Focus();
                    return false;
                }

                if (string.IsNullOrEmpty(txtProspectDetailsDOB.Text.ToString()))
                {
                    MessageBox.Show("Please Enter Date of Birth!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsDOB.Focus();
                    return false;
                }

                if (_fUtil.CheckDate(txtProspectDetailsDOB.Text) == false)
                {
                    MessageBox.Show("Please Enter Date of Birth in dd/mm/yyyy format!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsDOB.Focus();
                    return false;
                }

                DateTime dt = DateTime.Parse(txtProspectDetailsDOB.Text);
                if (dt > DateTime.Now.Date)
                {
                    MessageBox.Show("Date of Birth Cannot be Future Date!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsDOB.Focus();
                    return false;
                }

                if (string.IsNullOrEmpty(txtProspectDetailsStreet.Text.ToString()))
                {
                    MessageBox.Show("Please Enter Street Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsStreet.Focus();
                    return false;
                }

                if (string.IsNullOrEmpty(txtProspectDetailsSuburb.Text.ToString()))
                {
                    MessageBox.Show("Please Enter Suburb!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsSuburb.Focus();
                    return false;
                }

                if (string.IsNullOrEmpty(txtProspectDetailsCity.Text.ToString()))
                {
                    MessageBox.Show("Please Enter State Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsCity.Focus();
                    return false;
                }

                if (string.IsNullOrEmpty(txtProspectPostCode.Text.ToString()))
                {
                    MessageBox.Show("Please Enter PostCode!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectPostCode.Focus();
                    return false;
                }

                if (!txtProspectPostCode.Text.All(c => Char.IsNumber(c)))
                {
                    MessageBox.Show("Please Enter PostCode in Numbers!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectPostCode.Focus();
                    return false;
                }

                if (string.IsNullOrEmpty(txtProspectDetailsHomePhone.Text.ToString()))
                {
                }
                else
                {
                    if (!txtProspectDetailsHomePhone.Text.All(c => Char.IsNumber(c)))
                    {
                        MessageBox.Show("Please Enter Home Phone in Numbers!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        txtProspectDetailsHomePhone.Focus();
                        return false;
                    }
                }

                if (string.IsNullOrEmpty(txtProspectDetailsCellPhone.Text.ToString()))
                {
                    MessageBox.Show("Please Enter Mobile Phone!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsCellPhone.Focus();
                    return false;
                }

                if (!txtProspectDetailsCellPhone.Text.All(c => Char.IsNumber(c)))
                {
                    MessageBox.Show("Please Enter Mobile in Numbers!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsCellPhone.Focus();
                    return false;
                }

                if (string.IsNullOrEmpty(txtProspectEmail.Text.ToString()))
                {
                    MessageBox.Show("Please Enter E-Mail ID!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectEmail.Focus();
                    return false;
                }

                //if (cmbProspectDetailsDoor.SelectedIndex == -1)
                //{
                //    MessageBox.Show("Select Door to Check-In!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //    cmbProspectDetailsDoor.Focus();
                //    return false;
                //}

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message, "Prospects", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        private bool validateData_CasualMember()
        {
            try
            {
                FormUtilities _fUtil = new FormUtilities();

                if (txtProspectDetailsFirstName.Text.Trim() == "")
                {
                    MessageBox.Show("First Name cannot be Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsFirstName.Focus();
                    return false;
                }

                if (txtProspectDetailsFirstName.Text.All(c => char.IsNumber(c)))
                {
                    MessageBox.Show("First Name cannot be Numeric!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsFirstName.Focus();
                    return false;
                }

                if (txtProspectDetailsLastName.Text.Trim() == "")
                {
                    MessageBox.Show("Last Name cannot be Empty!", "Prospects", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsLastName.Focus();
                    return false;
                }

                if (txtProspectDetailsLastName.Text.All(c => char.IsNumber(c)))
                {
                    MessageBox.Show("Last Name cannot be Numeric!", "Prospects", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsLastName.Focus();
                    return false;
                }

                if (string.IsNullOrEmpty(txtProspectDetailsDOB.Text.ToString()))
                {
                    //MessageBox.Show("Please Enter Date of Birth!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //txtProspectDetailsDOB.Focus();
                    //return false;
                }
                else
                {
                    if (_fUtil.CheckDate(txtProspectDetailsDOB.Text) == false)
                    {
                        MessageBox.Show("Please Enter Date of Birth in dd/mm/yyyy format!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        txtProspectDetailsDOB.Focus();
                        return false;
                    }

                    DateTime dt = DateTime.Parse(txtProspectDetailsDOB.Text);
                    if (dt > DateTime.Now.Date)
                    {
                        MessageBox.Show("Date of Birth Cannot be Future Date!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        txtProspectDetailsDOB.Focus();
                        return false;
                    }
                }

                //if (string.IsNullOrEmpty(txtProspectDetailsStreet.Text.ToString()))
                //{
                //    MessageBox.Show("Please Enter Street Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //    txtProspectDetailsStreet.Focus();
                //    return false;
                //}

                //if (string.IsNullOrEmpty(txtProspectDetailsSuburb.Text.ToString()))
                //{
                //    MessageBox.Show("Please Enter Suburb!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //    txtProspectDetailsSuburb.Focus();
                //    return false;
                //}

                //if (string.IsNullOrEmpty(txtProspectDetailsCity.Text.ToString()))
                //{
                //    MessageBox.Show("Please Enter State Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //    txtProspectDetailsCity.Focus();
                //    return false;
                //}

                if (string.IsNullOrEmpty(txtProspectPostCode.Text.ToString()))
                {
                    //MessageBox.Show("Please Enter PostCode!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //txtProspectPostCode.Focus();
                    //return false;
                }
                else
                {
                    if (!txtProspectPostCode.Text.All(c => Char.IsNumber(c)))
                    {
                        MessageBox.Show("Please Enter PostCode in Numbers!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        txtProspectPostCode.Focus();
                        return false;
                    }
                }

                if (string.IsNullOrEmpty(txtProspectDetailsHomePhone.Text.ToString()))
                {
                    //MessageBox.Show("Please Enter Home Phone!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //txtProspectDetailsHomePhone.Focus();
                    //return false;
                }
                else
                {
                    if (!txtProspectDetailsHomePhone.Text.All(c => Char.IsNumber(c)))
                    {
                        MessageBox.Show("Please Enter Home Phone in Numbers!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        txtProspectDetailsHomePhone.Focus();
                        return false;
                    }
                }

                if (string.IsNullOrEmpty(txtProspectDetailsCellPhone.Text.ToString()))
                {
                    MessageBox.Show("Please Enter Mobile Phone!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsCellPhone.Focus();
                    return false;
                }
                else
                {
                    if (!txtProspectDetailsCellPhone.Text.All(c => Char.IsNumber(c)))
                    {
                        MessageBox.Show("Please Enter Mobile in Numbers!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        txtProspectDetailsCellPhone.Focus();
                        return false;
                    }
                }

                if (string.IsNullOrEmpty(txtProspectDetailsNotes.Text.ToString()))
                {
                    MessageBox.Show("Please Enter Reason for Visit!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProspectDetailsNotes.Focus();
                    return false;
                }

                if (string.IsNullOrEmpty(txtProspectEmail.Text.ToString()))
                {
                    //MessageBox.Show("Please Enter E-Mail ID!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //txtProspectEmail.Focus();
                    //return false;
                }

                //if (cmbProspectDetailsDoor.SelectedIndex == -1)
                //{
                //    MessageBox.Show("Select Door to Check-In!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //    cmbProspectDetailsDoor.Focus();
                //    return false;
                //}
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message, "Prospects", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        private void LoadDoors()
        {
            cmbProspectDetailsDoor.ItemsSource = null;
            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return;

            Hashtable ht = new Hashtable();

            ht.Clear();
            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);

            _dsDoors = sql.ExecuteProcedure("GetDoorsForCompanySite", ht);
            if (_dsDoors != null)
            {
                if (_dsDoors.Tables[0].DefaultView.Count > 0)
                {
                    cmbProspectDetailsDoor.ItemsSource = _dsDoors.Tables[0].DefaultView;
                    cmbProspectDetailsDoor.DisplayMemberPath = "DoorName";
                    cmbProspectDetailsDoor.SelectedValuePath = "DoorCodeNo";
                }
                cmbProspectDetailsDoor.SelectedIndex = 0;
            }
        }

        private void txtEnquiriesSearchFor_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            ClearData();
            if (txtEnquiriesSearchFor.Text.Trim() == "")
            {
                gvEnquiries.ItemsSource = null;
                _ds.Tables[_ProspectsTable].DefaultView.RowFilter = "";
                if (chkOnlyRelevant.IsChecked == true)
                    _ds.Tables[_ProspectsTable].DefaultView.RowFilter = "NotInterested = 1";
                else
                    _ds.Tables[_ProspectsTable].DefaultView.RowFilter = "NotInterested <> 1";

                gvEnquiries.ItemsSource = _ds.Tables[_ProspectsTable].DefaultView;
            }
            else
            {
                gvEnquiries.Visibility = Visibility.Visible;
                string _SearchStr = txtEnquiriesSearchFor.Text.Trim();
                if (_ds != null)
                {
                    _ds.Tables[_ProspectsTable].DefaultView.RowFilter = "";
                    
                    if (_ds.Tables[_ProspectsTable].DefaultView.Count > 0)
                    {
                        if (chkOnlyRelevant.IsChecked == true)
                            _ds.Tables[_ProspectsTable].DefaultView.RowFilter = "NotInterested = 1 AND (LastName LIKE '%" + _SearchStr + "%' OR FirstName LIKE '%" + _SearchStr + "%' OR MobilePhone LIKE '%" + _SearchStr + "%')";
                        else
                            _ds.Tables[_ProspectsTable].DefaultView.RowFilter = "NotInterested <> 1 AND (LastName LIKE '%" + _SearchStr + "%' OR FirstName LIKE '%" + _SearchStr + "%' OR MobilePhone LIKE '%" + _SearchStr + "%')";
                        
                        //_ds.Tables[_ProspectsTable].DefaultView.RowFilter = "(LastName LIKE '%" + _SearchStr + "%' OR FirstName LIKE '%" + _SearchStr + "%' OR MobilePhone LIKE '%" + _SearchStr + "%')";
                        gvEnquiries.ItemsSource = null;
                        gvEnquiries.ItemsSource = _ds.Tables[_ProspectsTable].DefaultView;
                    }
                    else
                    {
                        gvEnquiries.ItemsSource = null;
                    }
                }
            }
        }

        private void chkOnlyRelevant_Checked(object sender, RoutedEventArgs e)
        {
            //if (_isWindowLoaded)
            //{
            txtEnquiriesSearchFor.Text = "";
            _ds.Tables[_ProspectsTable].DefaultView.RowFilter = "";
            if (_ds.Tables[_ProspectsTable].DefaultView.Count > 0)
            {
                gvEnquiries.ItemsSource = null;
                gvEnquiries.Items.Clear();
                //_ds.Tables[_ProspectsTable].DefaultView.RowFilter = "NotInterested <> 1";
                _ds.Tables[_ProspectsTable].DefaultView.RowFilter = "NotInterested = 1";
                gvEnquiries.ItemsSource = _ds.Tables[_ProspectsTable].DefaultView;
            }
            ClearData();
            //}
        }

        private void chkOnlyRelevant_UnChecked(object sender, RoutedEventArgs e)
        {
            //if (_isWindowLoaded)
            //{
            txtEnquiriesSearchFor.Text = "";
            _ds.Tables[_ProspectsTable].DefaultView.RowFilter = "";
            if (_ds.Tables[_ProspectsTable].DefaultView.Count > 0)
            {
                gvEnquiries.ItemsSource = null;
                gvEnquiries.Items.Clear();
                //_ds.Tables[_ProspectsTable].DefaultView.RowFilter = "";
                _ds.Tables[_ProspectsTable].DefaultView.RowFilter = "NotInterested <> 1";
                gvEnquiries.ItemsSource = _ds.Tables[_ProspectsTable].DefaultView;
            }
            ClearData();
            //}
        }

        private void gvEnquiries_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if  (gvEnquiries.SelectedIndex != -1)
            {
                _IsNewProspect = false;
                _ProspectID = Convert.ToInt32((gvEnquiries.SelectedItem as DataRowView).Row["ProspectID"].ToString());

                //_ds.Tables[_ProspectsTable].DefaultView.RowFilter = "";
                //_ds.Tables[_ProspectsTable].DefaultView.RowFilter = "ProspectID = " + _ProspectID;
                //LoadSelectedProspectDetails(_ds.Tables[_ProspectsTable].DefaultView.ToTable());
                LoadSelectedProspectDetails((gvEnquiries.SelectedItem as DataRowView));
            }
            else
            {
                _ProspectID = -1;
                _IsNewProspect = true;
            }
        }

        private void LoadSelectedProspectDetails(DataTable _dtProspect)
        {
            txtProspectDetailsFirstName.Text = _dtProspect.Rows[0]["FirstName"].ToString();
            txtProspectDetailsLastName.Text = _dtProspect.Rows[0]["LastName"].ToString();

            for (int i = 0; i < cmbProspectDetailsAssignedTo.Items.Count; i++)
            {
                cmbProspectDetailsAssignedTo.SelectedIndex = i;
                if (cmbProspectDetailsAssignedTo.SelectedValue != null)
                {
                    if (Convert.ToInt32(cmbProspectDetailsAssignedTo.SelectedValue.ToString()) == Convert.ToInt32(_dtProspect.Rows[0]["AssignedToStaffID"].ToString()))
                    {
                        cmbProspectDetailsAssignedTo.SelectedIndex = i;
                        break;
                    }
                }
                cmbProspectDetailsAssignedTo.SelectedIndex = -1;
            }

            cmbProspectDetailsContactMethod.SelectedIndex = Convert.ToInt32(_dtProspect.Rows[0]["ContactMethod"].ToString());
            if (_dtProspect.Rows[0]["DateOfBirth"].ToString() != "")
            {
                txtProspectDetailsDOB.Text = Convert.ToDateTime(_dtProspect.Rows[0]["DateOfBirth"].ToString()).Date.ToString("dd/MM/yyyy");
            }
            txtProspectEmail.Text = _dtProspect.Rows[0]["EmailID"].ToString();
            chkProspectDetailsAreyouExercising.IsChecked = Convert.ToBoolean(_dtProspect.Rows[0]["Exercising"].ToString());
            txtProspectDetailsFitnessGoal.Text = _dtProspect.Rows[0]["FitnessGoal"].ToString();


            if (_dtProspect.Rows[0]["Gender"].ToString() == "1")
                rbtnProspectDetailsMale.IsChecked = true;
            else
                rbtnProspectDetailsFemale.IsChecked = true;

            txtProspectDetailsHomePhone.Text = _dtProspect.Rows[0]["HomePhone"].ToString();
            cmbProspectDetailsLeadStrength.Text = _dtProspect.Rows[0]["LeadStrength"].ToString();
            txtProspectDetailsLeadCreated.Text = _dtProspect.Rows[0]["LeadCreated"].ToString();
            txtProspectDetailsCellPhone.Text = _dtProspect.Rows[0]["MobilePhone"].ToString();
            txtProspectDetailsNotes.Text = _dtProspect.Rows[0]["Notes"].ToString();
            txtProspectPostCode.Text = _dtProspect.Rows[0]["PostalCode"].ToString();
            txtProspectDetailsPreviousGym.Text = _dtProspect.Rows[0]["PreviousGym"].ToString();

            for (int i = 0; i < cmbProspectDetailsReferredbyID.Items.Count; i++)
            {
                cmbProspectDetailsReferredbyID.SelectedIndex = i;
                if (cmbProspectDetailsReferredbyID.SelectedValue != null)
                {
                    if (Convert.ToInt32(cmbProspectDetailsReferredbyID.SelectedValue.ToString()) == Convert.ToInt32(_dtProspect.Rows[0]["ReferredbyID"].ToString()))
                    {
                        cmbProspectDetailsReferredbyID.SelectedIndex = i;
                        break;
                    }
                }
                cmbProspectDetailsReferredbyID.SelectedIndex = -1;
            }
            cmbProspectDetailsSourcePromotion.Text = _dtProspect.Rows[0]["SourcePromotion"].ToString();
            txtProspectDetailsCity.Text = _dtProspect.Rows[0]["State"].ToString();
            txtProspectDetailsStreet.Text = _dtProspect.Rows[0]["Street"].ToString();
            txtProspectDetailsSuburb.Text = _dtProspect.Rows[0]["Suburb"].ToString();
            txtProspectDetailsVisits.Text = _dtProspect.Rows[0]["Visits"].ToString();
            _defaultNotInterested = Convert.ToBoolean(_dtProspect.Rows[0]["NotInterested"].ToString());

            if (_defaultNotInterested)
            {
                btnTasksNotInterested.Content = "Make Active";
            }
            else
            {
                btnTasksNotInterested.Content = "Not Interested";
            }
        }

        private void LoadSelectedProspectDetails(DataRowView _drView)
        {
            txtProspectDetailsFirstName.Text = _drView["FirstName"].ToString();
            txtProspectDetailsLastName.Text = _drView["LastName"].ToString();
            chkProspectDetailsAgreeToTC.IsChecked = true;

            for (int i = 0; i < cmbProspectDetailsAssignedTo.Items.Count; i++)
            {
                cmbProspectDetailsAssignedTo.SelectedIndex = i;
                if (cmbProspectDetailsAssignedTo.SelectedValue != null)
                {
                    if (Convert.ToInt32(cmbProspectDetailsAssignedTo.SelectedValue.ToString()) == Convert.ToInt32(_drView["AssignedToStaffID"].ToString()))
                    {
                        cmbProspectDetailsAssignedTo.SelectedIndex = i;
                        break;
                    }
                }
                cmbProspectDetailsAssignedTo.SelectedIndex = -1;
            }

            cmbProspectDetailsContactMethod.SelectedIndex = Convert.ToInt32(_drView["ContactMethod"].ToString());
            if (_drView["DateOfBirth"].ToString() != "")
            {
                txtProspectDetailsDOB.Text = Convert.ToDateTime(_drView["DateOfBirth"].ToString()).Date.ToString("dd/MM/yyyy");
            }
            txtProspectEmail.Text = _drView["EmailID"].ToString();
            chkProspectDetailsAreyouExercising.IsChecked = Convert.ToBoolean(_drView["Exercising"].ToString());
            txtProspectDetailsFitnessGoal.Text = _drView["FitnessGoal"].ToString();


            if (_drView["Gender"].ToString() == "1")
                rbtnProspectDetailsMale.IsChecked = true;
            else
                rbtnProspectDetailsFemale.IsChecked = true;

            txtProspectDetailsHomePhone.Text = _drView["HomePhone"].ToString();
            cmbProspectDetailsLeadStrength.Text = _drView["LeadStrength"].ToString();
            txtProspectDetailsLeadCreated.Text = _drView["LeadCreated"].ToString();
            txtProspectDetailsCellPhone.Text = _drView["MobilePhone"].ToString();
            txtProspectDetailsNotes.Text = _drView["Notes"].ToString();
            txtProspectPostCode.Text = _drView["PostalCode"].ToString();
            txtProspectDetailsPreviousGym.Text = _drView["PreviousGym"].ToString();

            for (int i = 0; i < cmbProspectDetailsReferredbyID.Items.Count; i++)
            {
                cmbProspectDetailsReferredbyID.SelectedIndex = i;
                if (cmbProspectDetailsReferredbyID.SelectedValue != null)
                {
                    if (Convert.ToInt32(cmbProspectDetailsReferredbyID.SelectedValue.ToString()) == Convert.ToInt32(_drView["ReferredbyID"].ToString()))
                    {
                        cmbProspectDetailsReferredbyID.SelectedIndex = i;
                        break;
                    }
                }
                cmbProspectDetailsReferredbyID.SelectedIndex = -1;
            }
            cmbProspectDetailsSourcePromotion.Text = _drView["SourcePromotion"].ToString();
            txtProspectDetailsCity.Text = _drView["State"].ToString();
            txtProspectDetailsStreet.Text = _drView["Street"].ToString();
            txtProspectDetailsSuburb.Text = _drView["Suburb"].ToString();
            txtProspectDetailsVisits.Text = _drView["Visits"].ToString();
            _defaultNotInterested = Convert.ToBoolean(_drView["NotInterested"].ToString());

            if (_defaultNotInterested)
            {
                btnTasksNotInterested.Content = "Make Active";
            }
            else
            {
                btnTasksNotInterested.Content = "Not Interested";
            }
            tcProspect.SelectedIndex = 0;

            GetVisitDetails(Convert.ToInt32(_drView["ProspectID"].ToString()),"VISITOR");
            GetNotesForProspectCasualMember(Convert.ToInt32(_drView["ProspectID"].ToString()));
            GetProspectOptions(Convert.ToInt32(_drView["ProspectID"].ToString()));
            GetProspectTasks(Convert.ToInt32(_drView["ProspectID"].ToString()));
            GetCommunicationHistory(Convert.ToInt32(_drView["ProspectID"].ToString()), "PROSPECT");
        }

        private void GetNotesForProspectCasualMember(int _ProspectID)
        {
            txtBy.Text = "";
            txtDate.Text = "";
            txtNotes.Text = "";

            SqlHelper _sql = new SqlHelper();
            if (_sql._ConnOpen == 0) return;

            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            ht.Add("@ProspectID", _ProspectID);

            _dsNotes = _sql.ExecuteProcedure("GetNotesForProspect", ht);
            if (_dsNotes != null)
            {
                if (_dsNotes.Tables[0].DefaultView.Count > 0)
                {
                    gvNotes.ItemsSource = _dsNotes.Tables[0].DefaultView;
                }
                else
                    gvNotes.ItemsSource = null;
            }
            else
                gvNotes.ItemsSource = null;
        }

        private void GetProspectTasks(int _ProspectID)
        {
            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            Hashtable ht = new Hashtable();
            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return;

            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            ht.Add("@ProspectID", _ProspectID);

            _dsTasks = sql.ExecuteProcedure("GetProspectTaskDetails", ht);
            gvTaskType.ItemsSource = null;

            if (_dsTasks != null)
            {
                if (_dsTasks.Tables.Count > 0)
                {
                    gvTaskType.ItemsSource = _dsTasks.Tables[0].DefaultView;
                }
            }
        }

        private void GetVisitDetails( int _ProspectID, string _visitorType = "VISITOR")
        {
            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            Hashtable ht = new Hashtable();
            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return;

            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            ht.Add("@MemberOrProspectID", _ProspectID);
            ht.Add("@visitorType", _visitorType);

            DataSet _dsVisits = sql.ExecuteProcedure("GetVisitDetails", ht);
            gvVisits.ItemsSource = null;

            if (_dsVisits != null)
            {
                if (_dsVisits.Tables.Count > 0)
                {
                    gvVisits.ItemsSource = _dsVisits.Tables[0].DefaultView;
                }
            }
        }

        private void GetProspectOptions(int _ProspectID)
        {
            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            Hashtable ht = new Hashtable();
            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return;

            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            ht.Add("@ProspectID", _ProspectID);

            _dsOption = sql.ExecuteProcedure("GetProspectOptions", ht);
            gvOptions.ItemsSource = null;

            if (_dsOption != null)
            {
                if (_dsOption.Tables.Count > 0)
                {
                    if (_dsOption.Tables[0].DefaultView.Count > 0)
                    {
                        gvOptions.ItemsSource = _dsOption.Tables[0].DefaultView;
                        _isNewOption = false;
                    }
                    else
                    {
                        BindNewOptionDataTable();
                    }
                }
                else
                {
                    BindNewOptionDataTable();
                }
            }
            else
            {
                BindNewOptionDataTable();
            }
        }
        private void BindNewOptionDataTable()
        {
            _isNewOption = true;

            _dtNewOption = new DataTable();
            _dtNewOption.Columns.Add("OptionName", typeof(string));
            _dtNewOption.Columns.Add("Option", typeof(bool));
            _dtNewOption.Columns.Add("Comments", typeof(string));

            DataRow _drOption = _dtNewOption.NewRow();
            _drOption["OptionName"] = "7 Day Trial";
            _drOption["Option"] = false;
            _drOption["Comments"] = "";
            _dtNewOption.Rows.Add(_drOption);

            _drOption = _dtNewOption.NewRow();
            _drOption["OptionName"] = "Casual Member";
            _drOption["Option"] = false;
            _drOption["Comments"] = "";
            _dtNewOption.Rows.Add(_drOption);

            gvOptions.ItemsSource = _dtNewOption.DefaultView;
        }
        private void ClearData()
        {
            txtProspectDetailsLeadCreated.Text = DateTime.Now.ToString();
            cmbProspectDetailsAssignedTo.SelectedIndex = 0;
            cmbProspectDetailsContactMethod.SelectedIndex = 0;
            txtProspectDetailsDOB.Text = "";
            cmbProspectDetailsDoor.SelectedIndex = -1;
            txtProspectEmail.Text = "";
            chkProspectDetailsAreyouExercising.IsChecked = false;
            txtProspectDetailsFirstName.Text = "";
            txtProspectDetailsFitnessGoal.Text = "";
            rbtnProspectDetailsFemale.IsChecked = false;
            rbtnProspectDetailsMale.IsChecked = true;
            txtProspectDetailsHomePhone.Text = "";
            txtProspectDetailsLastName.Text = "";
            cmbProspectDetailsLeadStrength.SelectedIndex = 0;
            txtProspectDetailsCellPhone.Text = "";
            txtProspectDetailsNotes.Text = "";
            txtProspectPostCode.Text = "";
            txtProspectDetailsPreviousGym.Text = "";
            cmbProspectDetailsReferredbyID.SelectedIndex = -1;
            cmbProspectDetailsSourcePromotion.SelectedIndex = 0;
            txtProspectDetailsCity.Text = "";
            txtProspectDetailsStreet.Text = "";
            txtProspectDetailsSuburb.Text = "";
            txtProspectDetailsVisits.Text = "";
            txtOptionsComment.Text = "";

            _IsNewProspect = true;
            gvEnquiries.SelectedIndex = -1;
            _defaultNotInterested = false;

            btnProspectDetailsSave.IsEnabled = false;
            chkProspectDetailsAgreeToTC.IsChecked = false;
            if (_boolFromCasualMember)
                txtProspectDetailsFirstName.Focus();

            tcProspect.SelectedIndex = 0;
            gvVisits.ItemsSource = null;
            gvVisits.Items.Clear();

            gvTaskType.ItemsSource = null;
            gvTaskType.Items.Clear();

            gvVisits.ItemsSource = null;
            gvVisits.Items.Clear();
        }

        private void btnTasksNotInterested_Click(object sender, RoutedEventArgs e)
        {
            if (gvEnquiries.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select a Casual Member to Update Status!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            else
            {
                _IsNewProspect = false;
                _ProspectID = Convert.ToInt32((gvEnquiries.SelectedItem as DataRowView).Row["ProspectID"].ToString());

                string _msgQuestion = "Are You sure to update this Casual Member as 'NOT INTERESTED'?";
                if (_defaultNotInterested)
                    _msgQuestion = "Are You sure to update this Casual Member as 'ACTIVE'?";

                if (MessageBox.Show(_msgQuestion, this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    UpdateProspectStatus_NotInterested(_ProspectID, !_defaultNotInterested);
                    ClearData();
                    loadProspects();
                }
            }
        }

        private void UpdateProspectStatus_NotInterested( int _ProspectID, bool _NotInterested = true)
        {
            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
            string _msgResult = "Prospect Status updated - (Not Interested) !!";

            if (!_NotInterested)
                _msgResult = "Prospect Status updated - (Active) !!";

            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return;

            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            ht.Add("@ProspectID", _ProspectID);
            ht.Add("@NotInterested", _NotInterested);

            int result = sql.ExecuteQuery("UpdateProspectStatus_NotInterested", ht);

            if (result == 0)
            {
                MessageBox.Show("Error! Status NOT updated!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            else
            {
                //MessageBox.Show(_msgResult, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private void UpdateProspectStatus_Convert(int _ProspectID)
        {
            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return;

            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            ht.Add("@ProspectID", _ProspectID);
            int result = sql.ExecuteQuery("UpdateProspectStatus_Convert", ht);
        }
        private void GetMemberNo()
        {
            try
            {
                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();

                DataSet ds = _memberDetailsBal.get_LastUsedNoByCompanySite("MemberNo", _CompanyID, _SiteID);
                if ((ds != null) && (ds.Tables.Count > 0))
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        _ConvertMemberNo = Convert.ToInt32(ds.Tables[0].Rows[0]["LastUsedNo"].ToString().Trim());
                    }
                    else
                    {
                        MessageBox.Show("No Data! Member Number - AutoGenerated Number need to be Updated! (CONVERT)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void GetInvolvementType()
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();

                DataSet ds = _memberDetailsBal.get_InvolvementType();
                if ((ds != null) && (ds.Tables.Count > 0))
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ds.Tables[0].DefaultView.RowFilter = "InvolvementTypeName = 'MEMBER'";
                        if (ds.Tables[0].DefaultView.Count > 0)
                        {
                            _ConvertInvolvementType = Convert.ToInt32(ds.Tables[0].DefaultView[0]["InvolvementTypeID"].ToString());
                        }
                        else
                        {
                            _ConvertInvolvementType = 0;
                        }
                    }
                    else
                    {
                        MessageBox.Show("No Data! Involvement Type (CONVERT)!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void btnTasksConvert_Click(object sender, RoutedEventArgs e)
        {
            if (gvEnquiries.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Prospect to Convert as MEMBER!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            else
            {
                if (validateData())
                {
                    _IsNewProspect = false;
                    _ProspectID = Convert.ToInt32((gvEnquiries.SelectedItem as DataRowView).Row["ProspectID"].ToString());

                    if (MessageBox.Show("Are you sure to Convert the Prospect to Member?", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        _ds.Tables[_ProspectsTable].DefaultView.RowFilter = "";
                        _ds.Tables[_ProspectsTable].DefaultView.RowFilter = "ProspectID = " + _ProspectID;

                        GetMemberNo();
                        GetInvolvementType();
                        if (Convert_Prospect_To_Member(_ds.Tables[_ProspectsTable].DefaultView.ToTable(), _ConvertMemberNo, _ConvertInvolvementType) != 0)
                        {
                            UpdateProspectStatus_Convert(_ProspectID);
                        }
                        ClearData();
                        loadProspects();
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }
        private int Convert_Prospect_To_Member(DataTable _dtProspect, int _MemberNo, int _InvolvementTypeID)
        {
            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
            MemberDetailsDTO _memberDetailsDTO = new MemberDetailsDTO();
            DataRow _dr = _dtProspect.Rows[0];

            _memberDetailsDTO.LastName = _dr["LastName"].ToString();
            _memberDetailsDTO.FirstName = _dr["FirstName"].ToString();
            _memberDetailsDTO.DateofBirth = Convert.ToDateTime(_dr["DateofBirth"].ToString());

            _memberDetailsDTO.Gender = Convert.ToInt32(_dr["Gender"].ToString());

            _memberDetailsDTO.Street = _dr["Street"].ToString();
            _memberDetailsDTO.Suburb = _dr["Suburb"].ToString();
            _memberDetailsDTO.City = _dr["State"].ToString();
            _memberDetailsDTO.PostalCode = Convert.ToInt32(_dr["PostalCode"].ToString());
            _memberDetailsDTO.HomePhone = _dr["HomePhone"].ToString();
            _memberDetailsDTO.MobilePhone = _dr["MobilePhone"].ToString();
            _memberDetailsDTO.Notes = _dr["Notes"].ToString();
            _memberDetailsDTO.CardNo = 0;
            _memberDetailsDTO.WorkPhone = "";
            _memberDetailsDTO.Occupation = "";
            _memberDetailsDTO.Organisation = "";
            _memberDetailsDTO.InvolvementType = _InvolvementTypeID.ToString();
            _memberDetailsDTO.PersonalTrainer = _dr["AssignedToStaffID"].ToString();
            _memberDetailsDTO.EmailId = _dr["EmailID"].ToString();
            _memberDetailsDTO.MemberId = _MemberNo;
            _memberDetailsDTO.CompanyID = _CompanyID;
            _memberDetailsDTO.MemberProfilePhotoPath = "";
            _memberDetailsDTO.appVersion = Constants._appVersion;

            byte[] data = new byte[0];
            _memberDetailsDTO.MemberPic = data;
            MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();

            int _addMemberSuccess = _memberDetailsBal.add_MemberDetails(_memberDetailsDTO, 0);
            if (_addMemberSuccess == 0)
            {
                MessageBox.Show("Error! while Converting to MEMBER!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return _addMemberSuccess;
            }
            else
            {
                MessageBox.Show("Prospect Converted to MEMBER Successfully!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return _addMemberSuccess;
            }
        }

        private void btnProspectDetailsSave_Click(object sender, RoutedEventArgs e)
        {
            if (validateData_CasualMember())
            {
                //SAVE CASUAL MEMBER DETAILS
                if (_boolFromCasualMember)
                {
                    txtProspectDetailsLeadCreated.Text = DateTime.Now.ToString();
                }
                ProspectsDTO _ProspectDTO = new ProspectsDTO();

                if (cmbProspectDetailsAssignedTo.SelectedIndex != -1)
                {
                    _ProspectDTO.AssignedToStaffID = Convert.ToInt32(cmbProspectDetailsAssignedTo.SelectedValue);
                    _ProspectDTO.AssignedTo = cmbProspectDetailsAssignedTo.Text.Trim().ToUpper();
                }
                else
                {
                    _ProspectDTO.AssignedToStaffID = 0;
                    _ProspectDTO.AssignedTo = "";
                }
                _ProspectDTO.CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                if (cmbProspectDetailsContactMethod.SelectedIndex != -1)
                {
                    _ProspectDTO.ContactMethod = Convert.ToInt32(cmbProspectDetailsContactMethod.SelectedIndex);
                }
                else
                {
                    _ProspectDTO.ContactMethod = 0;
                }
                _ProspectDTO.Convert = false;
                if (txtProspectDetailsDOB.Text.Trim() != "")
                {
                    _ProspectDTO.DateofBirth = Convert.ToDateTime(txtProspectDetailsDOB.Text.ToString());
                }
                else
                {
                    _ProspectDTO.DateofBirth = DateTime.Now;
                }
                if (cmbProspectDetailsDoor.SelectedIndex != -1)
                {
                    _ProspectDTO.Door = cmbProspectDetailsDoor.Text.Trim().ToUpper();
                }
                else
                {
                    _ProspectDTO.Door = "";
                }
                _ProspectDTO.EmailId = txtProspectEmail.Text;
                _ProspectDTO.Exercising = Convert.ToBoolean(chkProspectDetailsAreyouExercising.IsChecked.ToString());
                _ProspectDTO.FirstName = txtProspectDetailsFirstName.Text.Trim().ToUpper();
                _ProspectDTO.FitnessGoal = txtProspectDetailsFitnessGoal.Text.ToUpper();

                int _gender;
                if (rbtnProspectDetailsFemale.IsChecked == true)
                    _gender = 2;
                else
                    _gender = 1;
                _ProspectDTO.Gender = _gender;

                _ProspectDTO.HomePhone = txtProspectDetailsHomePhone.Text.Trim();
                _ProspectDTO.LastName = txtProspectDetailsLastName.Text.Trim().ToUpper();
                _ProspectDTO.LeadCreated = DateTime.Now;
                _ProspectDTO.LeadStrength = cmbProspectDetailsLeadStrength.Text.Trim();
                _ProspectDTO.MobilePhone = txtProspectDetailsCellPhone.Text.Trim();
                _ProspectDTO.Notes = txtProspectDetailsNotes.Text.Trim();
                _ProspectDTO.NotInterested = _defaultNotInterested;
                if (txtProspectPostCode.Text.Trim() != "")
                {
                    _ProspectDTO.PostalCode = Convert.ToInt32(txtProspectPostCode.Text.Trim());
                }
                else
                {
                    _ProspectDTO.PostalCode = 0;
                }
                _ProspectDTO.PreviousGym = txtProspectDetailsPreviousGym.Text.Trim();
                _ProspectDTO.ProspectID = _ProspectID;
                if (cmbProspectDetailsReferredbyID.SelectedIndex != -1)
                {
                    _ProspectDTO.ReferredBy = Convert.ToInt32(cmbProspectDetailsReferredbyID.SelectedValue.ToString());
                }
                else
                {
                    _ProspectDTO.ReferredBy = 0;
                }
                _ProspectDTO.SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                if (cmbProspectDetailsSourcePromotion.SelectedIndex != -1)
                {
                    _ProspectDTO.SourcePromotion = cmbProspectDetailsSourcePromotion.Text.Trim();
                }
                else
                {
                    _ProspectDTO.SourcePromotion = "";
                }
                _ProspectDTO.State = txtProspectDetailsCity.Text.Trim();
                _ProspectDTO.Street = txtProspectDetailsStreet.Text.Trim();
                _ProspectDTO.Suburb = txtProspectDetailsSuburb.Text.Trim();
                if (txtProspectDetailsVisits.Text.Trim() == "")
                {
                    _ProspectDTO.Visits = 0;
                }
                else
                {
                    _ProspectDTO.Visits = Convert.ToInt32(txtProspectDetailsVisits.Text.Trim());
                }

                MemberDetailsBAL _memberBAL = new MemberDetailsBAL();
                int _result = _memberBAL.SaveUpdateProspects_SaveOnly(_ProspectDTO);

                ClearData();
                loadProspects();
                return;
            }
        }

        private void txtProspectDetailsLastName_GotFocus(object sender, RoutedEventArgs e)
        {
            //if (_boolFromCasualMember)
            //{
            //    InvokeKeyPad();
            //}
        }

        private void txtProspectDetailsLastName_LostFocus(object sender, RoutedEventArgs e)
        {
            //if (_boolFromCasualMember)
            //{
            //    KillKeyPad();
            //}
        }

        private void btnProspectDetailsSave_GotFocus(object sender, RoutedEventArgs e)
        {
            //btnProspectDetailsSave.Style = (Style)this.Resources["GlassButton1"];
        }

        private void btnProspectDetailsSave_LostFocus(object sender, RoutedEventArgs e)
        {
            //btnProspectDetailsSave.Style = (Style)Application.Current.Resources["GlassButton"];
        }

        private void chkProspectDetailsAgreeToTC_Checked(object sender, RoutedEventArgs e)
        {
            btnProspectDetailsSave.IsEnabled = true;
            btnProspectDetailsSave.Style = (Style)this.Resources["GlassButton1"];
        }

        private void chkProspectDetailsAgreeToTC_Unchecked(object sender, RoutedEventArgs e)
        {
            btnProspectDetailsSave.IsEnabled = false;
            btnProspectDetailsSave.Style = (Style)Application.Current.Resources["GlassButton"];
        }

        private void cmbProspectDetailsDoor_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (cmbProspectDetailsDoor.SelectedIndex != -1)
            {
                _DoorToOpen = (cmbProspectDetailsDoor.SelectedValue + "").ToString();
            }
            else
            {
                _DoorToOpen = "";
            }
        }

        private void btnSaveNotes_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_ProspectID > 0)
                {
                    string _userName = ConfigurationManager.AppSettings["LoggedInUser"].ToString();
                    string _Notes = txtNotes.Text.Trim().ToUpper();

                    int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                    Hashtable ht = new Hashtable();
                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@SiteID", _SiteID);
                    ht.Add("@ProspectID", _ProspectID);
                    ht.Add("@Reason", _Notes);
                    ht.Add("@StaffMember", _userName);

                    SqlHelper _sql = new SqlHelper();
                    if (_sql._ConnOpen == 0) return;

                    _sql.ExecuteQuery("UpdateProspectNotes", ht);

                    txtNotes.Text = "";
                    _isNewNotes = false;
                    btnSaveNotes.IsEnabled = false;

                    GetNotesForProspectCasualMember(_ProspectID);
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void txtNotes_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (!_isNewNotes)
            {
                if (txtNotes.Text.Trim() != "")
                {
                    _isNewNotes = true;
                    btnSaveNotes.IsEnabled = true;
                }
                else
                {
                    _isNewNotes = false;
                    btnSaveNotes.IsEnabled = false;
                }
            }
            else if (txtNotes.Text.Trim() != "")
            {
                _isNewNotes = true;
                btnSaveNotes.IsEnabled = true;
            }
            else
            {
                _isNewNotes = false;
                btnSaveNotes.IsEnabled = false;
            }
        }

        private void gvNotes_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            int _index = gvNotes.SelectedIndex;
            if (_index != -1)
            {
                txtNotes.Text = (gvNotes.Items[_index] as DataRowView).Row["Notes"].ToString().ToUpper();
                txtDate.Text = (gvNotes.Items[_index] as DataRowView).Row["NotesDate"].ToString().ToUpper();
                txtBy.Text = (gvNotes.Items[_index] as DataRowView).Row["Staff"].ToString().ToUpper();

                _isNewNotes = false;
                btnSaveNotes.IsEnabled = false;
            }
        }

        private void optSelected_Checked(object sender, RoutedEventArgs e)
        {
            if (gvOptions.SelectedIndex >= 0)
            {
                if (_isNewOption)
                {
                    if (Convert.ToBoolean(_dtNewOption.DefaultView[gvOptions.SelectedIndex]["Option"]) != true)
                        _dtNewOption.DefaultView[gvOptions.SelectedIndex]["Option"] = true;

                    //UnCheckOtherOptions_New(gvOptions.SelectedIndex);
                    gvOptions.ItemsSource = _dtNewOption.DefaultView;
                }
                else
                {
                    if (Convert.ToBoolean(_dsOption.Tables[0].DefaultView[gvOptions.SelectedIndex]["Option"]) != true)
                        _dsOption.Tables[0].DefaultView[gvOptions.SelectedIndex]["Option"] = true;

                    //UnCheckOtherOptions_Existing(gvOptions.SelectedIndex);
                    gvOptions.ItemsSource = _dsOption.Tables[0].DefaultView;
                }
            }
        }

        private void optSelected_Unchecked(object sender, RoutedEventArgs e)
        {
            if (gvOptions.SelectedIndex >= 0)
            {
                if (_isNewOption)
                {
                    if (Convert.ToBoolean(_dtNewOption.DefaultView[gvOptions.SelectedIndex]["Option"]) != false)
                        _dtNewOption.DefaultView[gvOptions.SelectedIndex]["Option"] = false;

                    gvOptions.ItemsSource = _dtNewOption.DefaultView;
                }
                else
                {
                    if (Convert.ToBoolean(_dsOption.Tables[0].DefaultView[gvOptions.SelectedIndex]["Option"]) != false)
                        _dsOption.Tables[0].DefaultView[gvOptions.SelectedIndex]["Option"] = false;

                    gvOptions.ItemsSource = _dsOption.Tables[0].DefaultView;
                }
            }
        }
        private void UnCheckOtherOptions_Existing(int _Index)
        {
            for (int i = 0; i < _dsOption.Tables[0].DefaultView.Count; i++)
            {
                if (i != _Index)
                {
                    _dsOption.Tables[0].DefaultView[i]["Option"] = false;
                }
            }
        }
        private void UnCheckOtherOptions_New(int _Index)
        {
            for ( int i = 0; i < _dtNewOption.DefaultView.Count; i++)
            {
                if (i != _Index)
                {
                    _dtNewOption.DefaultView[i]["Option"] = false;
                }
            }
        }

        private void btnSaveOption_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_ProspectID > 0)
                {
                    bool _7DayTrial = false;
                    bool _CasualMember = false;

                    if (!_isNewOption)
                    {
                        _dsOption.Tables[0].AcceptChanges();
                        _dtNewOption = _dsOption.Tables[0].DefaultView.ToTable();
                    }
                    else
                    {
                        _dtNewOption.AcceptChanges();
                    }
                    for ( int i = 0; i < _dtNewOption.DefaultView.Count; i++)
                    {
                        if (_dtNewOption.DefaultView[i]["OptionName"].ToString().Trim().ToUpper() == "7 DAY TRIAL" )
                        {
                            _7DayTrial = Convert.ToBoolean(_dtNewOption.DefaultView[i]["Option"]);
                        }
                        else if (_dtNewOption.DefaultView[i]["OptionName"].ToString().Trim().ToUpper() == "CASUAL MEMBER")
                        {
                            _CasualMember = Convert.ToBoolean(_dtNewOption.DefaultView[i]["Option"]);
                        }
                    }
                    string _userName = ConfigurationManager.AppSettings["LoggedInUser"].ToString();
                    string _Notes = txtOptionsComment.Text.Trim().ToUpper();

                    int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                    Hashtable ht = new Hashtable();
                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@SiteID", _SiteID);
                    ht.Add("@ProspectID", _ProspectID);
                    ht.Add("@Comments", _Notes);
                    ht.Add("@opt7DayTrial", _7DayTrial);
                    ht.Add("@optCasualMember", _CasualMember);

                    SqlHelper _sql = new SqlHelper();
                    if (_sql._ConnOpen == 0) return;

                    _sql.ExecuteQuery("SaveUpdateProspectOptions", ht);

                    ClearData();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void btnTasksAddTasks_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_ProspectID > 0)
                {
                    AddTask _addTask = new AddTask();
                    _addTask._invokedFromDashboard = true;
                    _addTask._fromProspect = true;
                    _addTask.ExistingMember = true;
                    _addTask.GetMemberDetailsMemberId = 0;
                    _addTask._ProspectID = _ProspectID;
                    _addTask._MemberName = _MemberName;
                    _addTask._CompanyName = _CompanyName;
                    _addTask._CurrentLoginDateTime = _CurrentLoginDateTime;
                    _addTask._SiteName = _SiteName;
                    _addTask._StaffPic = _StaffPic;
                    _addTask._UserName = _UserName;
                    _addTask._CompanyID = Convert.ToInt32( ConfigurationManager.AppSettings["LoggedInOrgID"].ToString() );
                    _addTask._SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                    _addTask.StaffImage.Source = StaffImage.Source;
                    _addTask._dSAccessRights = _dSAccessRights;
                    _addTask.dt = null;
                    _addTask.Owner = this;

                    _addTask.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private void btnTasksCancel_Click(object sender, RoutedEventArgs e)
        {
            if (gvTaskType.SelectedIndex >= 0)
            {
                ActionTaken(3, "CANCELED");
            }
            else
            {
                MessageBox.Show("Select a Task to Cancel!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnTasksCompleteTask_Click(object sender, RoutedEventArgs e)
        {
            if (gvTaskType.SelectedIndex >= 0)
            {
                ActionTaken(4, "COMPLETED");
            }
            else
            {
                MessageBox.Show("Select a Task to Complete!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void gvTaskType_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (gvTaskType.SelectedIndex != -1)
            {
                int _taskStatus = 0;
                _TaskId  = Convert.ToInt32((gvTaskType.SelectedItem as DataRowView).Row["TaskID"].ToString());

                _taskStatus = Convert.ToInt32((gvTaskType.SelectedItem as DataRowView).Row["StatusID"].ToString());
                if ((_taskStatus == 3) || (_taskStatus == 4))
                {
                    btnTasksCancel.IsEnabled = false;
                    btnTasksCompleteTask.IsEnabled = false;
                }
                else
                {
                    btnTasksCancel.IsEnabled = true;
                    btnTasksCompleteTask.IsEnabled = true;
                }
            }
        }
        private void ActionTaken(int TaskStatus, string _ActionTaken = "NONE")
        {
            try
            {
                string _staffActionTaken = _ActionTaken;
                DateTime _taskExpireDate = DateTime.Now;
                TaskBAL _taskBAL = new TaskBAL();
                int _addActionSucess = _taskBAL.add_ProspectStaffTask 
                                                            (
                                                                _TaskId, _ProspectID, 
                                                                Convert.ToInt32(ConfigurationManager.AppSettings["LoggedUserStaffID"].ToString()), 
                                                                TaskStatus, _taskExpireDate, _staffActionTaken
                                                            );

                if (_addActionSucess != 0)
                {
                    MessageBox.Show("Task Details Updated...!",this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
                    ClearData();
                    loadProspects();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnTasksHistory_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (gvTaskType.SelectedIndex >= 0)
                {
                    TaskHistory _taskHistory = new TaskHistory();
                    _taskHistory.TaskHistoryStaffId = Convert.ToInt32((gvTaskType.SelectedItem as DataRowView).Row["StaffID"].ToString());
                    _taskHistory.TaskHistoryMemberId = _ProspectID;
                    _taskHistory.TaskId = _TaskId;
                    _taskHistory._MEMBERorPROSPECT = "PROSPECT";
                    _taskHistory._invokedFromDashboard = true;
                    _taskHistory.ExistingMember = true;
                    _taskHistory.GetMemberDetailsMemberId = _ProspectID;
                    _taskHistory._MemberName = _MemberName;
                    _taskHistory._CanOpenMemberShipNewMember = false;
                    _taskHistory._CompanyName = _CompanyName;
                    _taskHistory._CurrentLoginDateTime = _CurrentLoginDateTime;
                    _taskHistory._SiteName = _SiteName;
                    _taskHistory._StaffPic = _StaffPic;
                    _taskHistory._UserName = _UserName;
                    _taskHistory._CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    _taskHistory._SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                    _taskHistory.StaffImage.Source = StaffImage.Source;
                    _taskHistory._dSAccessRights = _dSAccessRights;
                    _taskHistory.Owner = this;

                    //this.Close();
                    _taskHistory.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void GetCommunicationHistory(int _prospectID, string _receiverType)
        {
            if ( _prospectID > 0 )
            {
                //GET COMMUNICATION LOG
                gvCommunications.ItemsSource = null;
                txtCommunicationsMessage.Text = "";
                txtCommunicationsSubject.Text = "";

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                ht.Add("@receiverID", _prospectID);
                ht.Add("@receiverType", _receiverType);
                ht.Add("@commType", "EMAIL");

                SqlHelper _sql = new SqlHelper();
                if (_sql._ConnOpen == 0) return;

                DataSet _dsComm = _sql.ExecuteProcedure("GetCommunicationHistory", ht);
                gvCommunications.Items.Clear();

                if (_dsComm != null)
                {
                    if (_dsComm.Tables.Count > 0)
                    {
                        if (_dsComm.Tables[_commDetailsTable].DefaultView.Count > 0)
                        {
                            gvCommunications.ItemsSource = _dsComm.Tables[_commDetailsTable].DefaultView;
                        }
                    }
                }
            }
        }

        private void gvCommunications_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (gvCommunications.SelectedIndex >= 0)
            {
                btnSendEmail.IsEnabled = false;

                txtCommunicationsSubject.Text = (gvCommunications.SelectedItem as DataRowView).Row["commSubject"].ToString().ToUpper();
                txtCommunicationsMessage.Text = (gvCommunications.SelectedItem as DataRowView).Row["commBodyFull"].ToString();

                txtCommunicationsMessage.IsReadOnly = true;
                txtCommunicationsSubject.IsReadOnly = true;
            }
            else
            {
                btnSendEmail.IsEnabled = true;
            }
        }

        private void btnSendEmail_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_ProspectID > 0)
                {
                    if (gvCommunications.SelectedIndex != -1)
                    {
                        gvCommunications.SelectedIndex = -1;
                        txtCommunicationsMessage.Text = string.Empty;
                        txtCommunicationsSubject.Text = string.Empty;
                        return;
                    }
                    else
                    {
                        if (txtProspectEmail.Text.Trim() == "")
                        {
                            MessageBox.Show("Prospect eMail ID is empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            txtProspectEmail.Focus();
                            return;
                        }
                        if (txtCommunicationsSubject.Text.Trim() == "")
                        {
                            MessageBox.Show("Please enter Subject!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            txtCommunicationsSubject.Focus();
                            return;
                        }
                        if (txtCommunicationsMessage.Text.Trim() == "")
                        {
                            MessageBox.Show("Please enter eMail Content!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            txtCommunicationsMessage.Focus();
                            return;
                        }
                        Cursor = Cursors.Wait;
                        btnSendEmail.Cursor = Cursors.Wait;

                        sendEmail(_CompanyName, txtProspectEmail.Text, txtCommunicationsSubject.Text, txtCommunicationsMessage.Text);

                        Cursor = Cursors.Arrow;
                        btnSendEmail.Cursor = Cursors.Hand;
                        ClearData();
                    }
                }
            }
            catch(Exception ex)
            {
                string _exMsg = ex.Message;
                this.Cursor = Cursors.Arrow;
                btnSendEmail.Cursor = Cursors.Hand;
            }
        }

        public void sendEmail(string strFromName, string strTo, string strSubject, string strBody, string _filePath = "")
        {
            MailMessage mail = new MailMessage();
            SmtpClient client = new SmtpClient();
            try
            {
                client = new SmtpClient();
                client.UseDefaultCredentials = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                //////////////DO NOT CHANGE THE BELOW LINES //////////////
                client.Port = 587;
                client.Host = "SMTP.GMAIL.COM";
                string _strFrom = "noreply.reda@gmail.com";
                string _strDecryptPassWord = Comman.Constants.passwordDecrypt("fMKQ+IINHCVpOzskqoKffaT7hdtbE+xxX1g5Ybpw+6AATKV9AxcNhvAZ9rZL7uD/",
                                                        Comman.Constants._EncryptKey);
                if ((ConfigurationManager.AppSettings["REDAAutoEmailID"].ToString().Trim() != "") &&
                    (ConfigurationManager.AppSettings["REDAAutoEmailEncPwd"].ToString().Trim() != ""))
                {
                    _strFrom = ConfigurationManager.AppSettings["REDAAutoEmailID"].ToString().Trim();
                    _strDecryptPassWord = Comman.Constants.passwordDecrypt(ConfigurationManager.AppSettings["REDAAutoEmailEncPwd"].ToString(),
                                                            Comman.Constants._EncryptKey);
                }
                //////////////DO NOT CHANGE THE ABOVE LINES //////////////

                MailAddress _mFrom = new MailAddress(_strFrom, strFromName);
                MailAddress _mTo = new MailAddress(strTo);

                mail = new MailMessage(_mFrom, _mTo);
                mail.IsBodyHtml = true;
                System.Net.NetworkCredential objSMTPUserInfo = new System.Net.NetworkCredential(_strFrom, _strDecryptPassWord);
                client.Credentials = objSMTPUserInfo;
                client.EnableSsl = true;
                mail.Subject = strSubject;
                if (mail.IsBodyHtml == true)
                {
                    strBody = strBody.Replace("\n", "<br>");
                }
                else
                {
                    strBody = strBody.Replace("<br>", "\n");
                }
                mail.Body = strBody;
                if (_filePath != "")
                {
                    Attachment _fileAttachment = new Attachment(_filePath);
                    mail.Attachments.Add(_fileAttachment);
                }
                client.Send(mail);
                mail.Dispose();

                Constants.AddToCommunicationLog(_ProspectID, "PROSPECT", txtCommunicationsSubject.Text, txtCommunicationsMessage.Text, strTo, 1);
            }
            catch (Exception x)
            {
                string _exMsg = x.Message;
                Constants.AddToCommunicationLog(_ProspectID, "PROSPECT", "** EMAIL ERROR **", x.Message, strTo, 0);
                mail.Dispose();
                this.Cursor = Cursors.Arrow;
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            gvCommunications.SelectedIndex = -1;
            txtCommunicationsSubject.Text  = "";
            txtCommunicationsMessage.Text = "";
            btnSendEmail.IsEnabled = true;
            txtCommunicationsMessage.IsReadOnly = false;
            txtCommunicationsSubject.IsReadOnly = false;
            txtCommunicationsSubject.Focus();
        }
    }
}
