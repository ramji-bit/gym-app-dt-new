﻿using System.Windows;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Linq;
using System;
using System.Data;
using System.Windows.Input;
using System.IO;
using System.IO.Ports;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Configuration;

using GymMembership.Task;
using GymMembership.Accounts;
using GymMembership.BAL;
using GymMembership.DTO;
using GymMembership.Comman;

namespace GymMembership.BookingInfo
{
    /// <summary>
    /// Interaction logic for MemberBooking.xaml
    /// </summary>
    public partial class MemberBooking 
    {
        public string _MemberName;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public DataSet _dSAccessRights;
        DataSet _dsBookingMembers;
        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto(_StaffPic);
        }

        private void loadStaffPhoto(byte[] _StaffPic)
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }
        public MemberBooking()
        {
            InitializeComponent();
        }

        private string _MemberLastName;
        private int _ResourceTypeID = 0;
        private string _ResourceTypeName = "";
        private int _BookingsTable = 1; private int _MaxCountTable = 0;

        DataSet _dsFacilityGlobal; DataTable _dTresult;
        DataSet _dsSlots; DataSet _dsBookingDetails;
        int _FacilityID = 0; int _SessionNumber = 0;
        int _BookedCountCurrentSession = 0; int _WaitListCountCurrentSession = 0;
        int _MaxMembersCurrentSession = 0; int _MaxWaitListCurrentSession = 0;

        Utilities _utilites = new Utilities();
        BookingBAL _bookingBAL = new BookingBAL();

        private bool _isBookingWaitList = false;
        private string _BookingStatus = "B";
        private int _BookingWLOrder = 0;

        private void Booking_Loaded(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
            cldrMemberBkg.SelectionMode = CalendarSelectionMode.SingleDate;
            gvMemberBkgMemberDetails.ItemsSource = null;

            rwFacility.Visibility = Visibility.Hidden;
            rwPT.Visibility = Visibility.Hidden;
            rwFacility1.Visibility = Visibility.Hidden;
            rwLabel1.Visibility = Visibility.Hidden;
            rwLabel2.Visibility = Visibility.Hidden;

            gvMemberBkgSlotsAvailable.Visibility = Visibility.Hidden;

            getResourceTypes();
            _MemberLastName = "";
            MemberList();
            //cldrMemberBkg.IsEnabled = false;
            Mouse.OverrideCursor = null;
        }
        private void btnMemberBkgFindMember_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                gvMemberBkgMemberDetails.ItemsSource = null;
                _MemberLastName = txtMemberBkgFindMember.Text.ToString().Trim();
                if (_MemberLastName == "")
                {
                    System.Windows.MessageBox.Show("Enter a few characters of Last Name...!", this.Title, MessageBoxButton.OK,MessageBoxImage.Exclamation);
                    return;
                }
                DataSet ds = _bookingBAL.get_BookingMembersDetails(_MemberLastName);
                gvMemberBkgMemberDetails.ItemsSource = ds.Tables[0].AsDataView();
                gvMemberBkgMemberDetails.IsReadOnly = true;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString(),this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnMemberBkgToday_Click(object sender, RoutedEventArgs e)
        {
            //cldrMemberBkg.SelectedDate = DateTime.Now;
            try
            {
                cldrMemberBkg.DisplayDate = DateTime.Now.Date;
                cldrMemberBkg.SelectedDate = (DateTime?)DateTime.Now.Date;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Not able to Show Current Time" + "\n" + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void cmbMemberBkgResourceType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                int ResourceType = Convert.ToInt32(cmbMemberBkgResourceType.SelectedIndex.ToString());
                cldrMemberBkg.IsEnabled = true;

                if (ResourceType == 0)
                {
                    try
                    {
                        rwPT.Visibility = Visibility.Hidden;
                        rwFacility.Visibility = Visibility.Visible;
                        rwFacility1.Visibility = Visibility.Visible;
                        rwLabel1.Visibility = Visibility.Visible;
                        rwLabel2.Visibility = Visibility.Visible;

                        gvMemberBkgTiming.Visibility = Visibility.Hidden;
                        rwslotAvailableDetails.Height = new GridLength(47, GridUnitType.Star);
                        _ResourceTypeID = 1;
                        _ResourceTypeName = "FACILITY";
                        
                        FillBookingGridOnResourceTypeSelection(_ResourceTypeID, _ResourceTypeName);
                        //if (gvMemberBkgMemberDetails.SelectedIndex == -1)
                        //{
                        //    gvMemberBkgTiming.IsEnabled = false;
                        //}

                        //gvMemberBkgSlotsAvailable.Visibility = Visibility.Visible;
                        gvMemberBkgSlotsAvailable.IsReadOnly = true;
                    }
                    catch (Exception ex)
                    {
                        System.Windows.MessageBox.Show(ex.Message.ToString(), this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                } //if
                else
                {
                    try
                    {
                        rwPT.Visibility = Visibility.Visible;
                        rwFacility.Visibility = Visibility.Hidden;
                        rwFacility1.Visibility = Visibility.Hidden;
                        rwLabel1.Visibility = Visibility.Hidden;
                        rwLabel2.Visibility = Visibility.Hidden;

                        rwslotAvailableDetails.Height = new GridLength(0);
                        gvMemberBkgTiming.Visibility = Visibility.Visible;

                        _ResourceTypeID = 2;
                        _ResourceTypeName = "TRAINER";
                        FillBookingGridOnResourceTypeSelection(_ResourceTypeID, _ResourceTypeName);
                        if (gvMemberBkgMemberDetails.SelectedIndex == -1)
                        {
                            gvMemberBkgTiming.IsEnabled = false;
                        }
                        gvMemberBkgSlotsAvailable.Visibility = Visibility.Hidden;
                    }//try
                    catch (Exception ex)
                    {
                        System.Windows.MessageBox.Show("Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }//else
                cldrMemberBkg.SelectedDate = null;
                cldrMemberBkg.SelectedDate = DateTime.Now.Date;
            }
            catch(Exception ex)
            {
                System.Windows.MessageBox.Show("Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void FillBookingGridOnResourceTypeSelection(int iResType, string ResTypeName) //1-Facility; 2-Trainer
        {
            string ft_FieldName = "";
            gvMemberBkgTiming.ItemsSource = null;

            if (iResType == 1)
            {
                _dsFacilityGlobal = _bookingBAL.get_Facilities();
                _dTresult = new DataTable("Booking_Facility");
                ft_FieldName = "FacilityName";

                splButtonFacility.ItemsSource = _dsFacilityGlobal.Tables[0].DefaultView;
                splButtonFacility.DisplayMemberPath = "FacilityName";
                if (_dsFacilityGlobal.Tables[0].DefaultView.Count > 0)
                    splButtonFacility.SelectedIndex = 0;
            }
            else
            {
                _dsFacilityGlobal = _bookingBAL.get_BookingResourceTrainer();
                _dTresult = new DataTable("Booking_Trainer");
                ft_FieldName = "Trainer";
                //}

                DataTable dt = _dsFacilityGlobal.Tables[0];

                TimeSpan tSlotBlock = new TimeSpan(Constants._BookingSlotBlockHr, Constants._BookingSlotBlockMin, Constants._BookingSlotBlockSec);
                TimeSpan tStart = new TimeSpan(Constants._BookingSlotStartHr, Constants._BookingSlotStartMin, Constants._BookingSlotStartSec);
                TimeSpan tEnd = new TimeSpan(Constants._BookingSlotEndHr, Constants._BookingSlotEndMin, Constants._BookingSlotEndSec);
                DateTime tNextSlot = new DateTime();

                double _SlotMinutes = tSlotBlock.TotalMinutes;
                double _EndMinutes = tEnd.TotalMinutes;
                double _StartMinutes = tStart.TotalMinutes;

                tNextSlot = DateTime.Today.Add(tStart);
                int NoOfSlots;

                NoOfSlots = Convert.ToInt32((_EndMinutes - _StartMinutes) / (_SlotMinutes)) + 1;// 17;

                int i = 0; int j = 0;
                gvMemberBkgTiming.Columns.Clear();

                _dTresult.Columns.Add("Time Slot");

                foreach (DataRow row in dt.Rows)
                {
                    _dTresult.Columns.Add(row[ft_FieldName].ToString(), typeof(string));
                }


                for (int m = 0; m < NoOfSlots; m++)
                {
                    DataRow dR = _dTresult.NewRow();

                    for (j = 0; j < _dTresult.Columns.Count; j++)
                    {
                        if (j != 0)
                        {
                            dR[_dTresult.Columns[j]] = "";
                        }
                        else
                        {
                            dR[_dTresult.Columns[j]] = tNextSlot.ToString("hh:mm tt");
                            tNextSlot = tNextSlot.Add(tSlotBlock);
                        }
                    }
                    _dTresult.Rows.Add(dR);
                }

                gvMemberBkgTiming.ItemsSource = _dTresult.DefaultView;
                gvMemberBkgTiming.Columns[0].IsReadOnly = true;
            }
        }

        private void btnMemberBkgAddBooking_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime _BookingDate; int _CompanyID; int _SiteID;

                if (cmbMemberBkgResourceType.SelectedIndex == -1)
                {
                    System.Windows.MessageBox.Show("Please Select a Resource Type!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    cmbMemberBkgResourceType.Focus();
                    return;
                }

                string _BookingTime;
                string _ResourceTypeName; string _MemberLName; string _MemberFName; string _MemberPhone;
                int _MemberID; int _ResourceTypeID; string _RoomOrTrainer;

                _ResourceTypeName = (cmbMemberBkgResourceType.SelectedItem as DataRowView).Row["ResourceTypeName"].ToString().ToUpper();

                if (cldrMemberBkg.SelectedDate.ToString().Trim() == "")
                {
                    _BookingDate = cldrMemberBkg.DisplayDate;
                }
                else
                {
                    _BookingDate = (DateTime)cldrMemberBkg.SelectedDate.Value;
                }

                if (_BookingDate.Date < DateTime.Now.Date)
                {
                    System.Windows.MessageBox.Show("Booking Date Cannot be LESS than Today!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                if (gvMemberBkgMemberDetails.SelectedIndex == -1)
                {
                    System.Windows.MessageBox.Show("Select a Member from List! To Add a Booking...", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    txtMemberBkgFindMember.Focus();
                    return;
                }

                if (_ResourceTypeName != "FACILITIES")
                {
                    if (gvMemberBkgTiming.SelectedCells.Count == 0)
                    {
                        System.Windows.MessageBox.Show("Please Select Atleast one Time Slot Booking!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                }
                else
                {
                    if (splButtonFacility.SelectedIndex == -1)
                    {
                        System.Windows.MessageBox.Show("Select a Facility / Class for Booking!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                    if (lstSessions.SelectedIndex == -1)
                    {
                        System.Windows.MessageBox.Show("Select a Session/TimeSlot for Booking!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                }

                _ResourceTypeID = Convert.ToInt32(cmbMemberBkgResourceType.SelectedValue);
                _MemberFName = (gvMemberBkgMemberDetails.SelectedItem as DataRowView).Row["FirstName"].ToString();
                _MemberLName = (gvMemberBkgMemberDetails.SelectedItem as DataRowView).Row["LastName"].ToString();
                _MemberID = Convert.ToInt32((gvMemberBkgMemberDetails.SelectedItem as DataRowView).Row["MemberNo"].ToString());
                _MemberPhone = (gvMemberBkgMemberDetails.SelectedItem as DataRowView).Row["Phone"].ToString();
                _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                _dsBookingDetails.Tables[_BookingsTable].DefaultView.RowFilter = "";
                _dsBookingDetails.Tables[_BookingsTable].DefaultView.RowFilter = "MemberID = " + _MemberID + " AND CompanyID = " + _CompanyID
                        + " AND SiteID = " + _SiteID + " AND FacilityID = " + _FacilityID  + " AND Date = '" + _BookingDate.Date.ToString() + "' AND BookingStatus <> 'C'"
                        + " AND SessionNumber = " + _SessionNumber;

                if (_dsBookingDetails.Tables[_BookingsTable].DefaultView.Count > 0)
                {
                    System.Windows.MessageBox.Show("Member : " + _MemberFName + " " +_MemberLName +";\nAlready booked on this date (Session No.: " + _dsBookingDetails.Tables[_BookingsTable].DefaultView[0]["SessionNumber"].ToString() + ")!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                bool _CheckThreshold = false;
                if (_ResourceTypeName == "FACILITIES")
                {
                    _isBookingWaitList = false;
                    _CheckThreshold = CheckMaxBookingSession(_FacilityID, _SessionNumber);

                    if (!_CheckThreshold)
                    {
                        decimal _BookingCharge = Convert.ToDecimal(_dsFacilityGlobal.Tables[0].DefaultView[splButtonFacility.SelectedIndex]["ChargePerBooking"].ToString());
                        if (_BookingCharge >= 0)
                        {
                            if (System.Windows.MessageBox.Show("Charges applicable for Booking : $" + _BookingCharge.ToString("##0.#0") + "\n\nContinue to Book?", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                            {
                                return;
                            }
                        }

                        _RoomOrTrainer = (splButtonFacility.SelectedItem as DataRowView).Row["FacilityName"].ToString();
                        _BookingTime = _dsSlots.Tables[0].DefaultView[lstSessions.SelectedIndex]["StartTime"].ToString() + "-" + _dsSlots.Tables[0].DefaultView[lstSessions.SelectedIndex]["EndTime"].ToString();

                        rchtxtMemberBkg.Text = rchtxtMemberBkg.Text + "\nBooking For: " + _MemberLName + ", " + _MemberFName + "; \n" + "Facility / Trainer : " + _RoomOrTrainer + "\n Date : " + _BookingDate.Date.ToString("dd/MMM/yyyy") + "\n Time : " + _BookingTime + "\n Session No.: "+_SessionNumber.ToString("#0");
                        rchtxtMemberBkg.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
                        rchtxtMemberBkg.ScrollToEnd();

                        //Database update code here..
                        BookingTransDTO _bookingDTO = new BookingTransDTO();
                        _bookingDTO.CompanyID = _CompanyID;
                        _bookingDTO.Date = _BookingDate;
                        _bookingDTO.FirstName = _MemberFName;
                        _bookingDTO.LastName = _MemberLName;
                        _bookingDTO.MemberID = _MemberID;
                        _bookingDTO.Phone = _MemberPhone;
                        _bookingDTO.ResourceTypeID = _ResourceTypeID;
                        _bookingDTO.ResourceTypename = _ResourceTypeName;
                        _bookingDTO.RoomOrTrainer = _RoomOrTrainer;
                        _bookingDTO.SiteID = _SiteID;
                        _bookingDTO.Time = _BookingTime;
                        _bookingDTO.BookingStatus = _BookingStatus;
                        _bookingDTO.WaitListNo = _BookingWLOrder;
                        _bookingDTO.FacilityID = _FacilityID;
                        _bookingDTO.SessionNumber = _SessionNumber;

                        string _BookingDesc = "BOOKING CHARGES: " + _RoomOrTrainer + " Dt: " + _BookingDate.Date.ToString("dd/MMM/yy") + " Session No: " + _SessionNumber;
                        int Result = _bookingBAL.addBooking(_bookingDTO);

                        if (Result == 1)
                        {
                            cldrMemberBkg.SelectedDate = _BookingDate;

                            if (_BookingStatus == "B")
                            {
                                //RAISE INVOICE FOR BOOKING CHARGE $ DEFINED IN MASTER
                                if (_BookingCharge >= 0)
                                {
                                    int InvNo = RAISE_BOOKING_FEE_INVOICE(_BookingCharge, _MemberID);
                                    if (InvNo != -1)
                                    {
                                        RAISE_BOOKING_FEE_INVOICE_DETAILS(InvNo, _BookingCharge, _MemberID, _BookingDesc);
                                    }
                                }
                                //RAISE INVOICE FOR BOOKING CHARGE $ DEFINED IN MASTER
                            }
                        }
                        else
                        {
                            System.Windows.MessageBox.Show("Booking Failed..!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("NO BOOKING AVAILABLE for Selected Date & Facility!\n\nMax allowed Slots have been booked!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                }
                else
                {
                    for (int i = 0; i < gvMemberBkgTiming.SelectedCells.Count; i++)
                    {
                        if (gvMemberBkgTiming.SelectedCells[i].Column.DisplayIndex != 0)
                        {
                            _CheckThreshold = false;

                            if (!_CheckThreshold)
                            {
                                (gvMemberBkgTiming.SelectedCells[i].Item as DataRowView).Row[gvMemberBkgTiming.SelectedCells[i].Column.DisplayIndex] = _MemberFName + " " + _MemberLName;
                                _RoomOrTrainer = gvMemberBkgTiming.SelectedCells[i].Column.Header.ToString();
                                _BookingTime = (gvMemberBkgTiming.SelectedCells[i].Item as DataRowView).Row[0].ToString();
                                rchtxtMemberBkg.Text = rchtxtMemberBkg.Text + "\nBooking For: " + _MemberFName + ", " + _MemberLName + "; \n" + "Facility / Trainer : " + _RoomOrTrainer + "\n Date : " + _BookingDate.Date.ToString("dd/MMM/yyyy") + "\n Time : " + _BookingTime + "\n";
                                rchtxtMemberBkg.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
                                rchtxtMemberBkg.ScrollToEnd();

                                //Database update code here..
                                BookingTransDTO _bookingDTO = new BookingTransDTO();
                                _bookingDTO.CompanyID = _CompanyID;
                                _bookingDTO.Date = _BookingDate;
                                _bookingDTO.FirstName = _MemberFName;
                                _bookingDTO.LastName = _MemberLName;
                                _bookingDTO.MemberID = _MemberID;
                                _bookingDTO.Phone = _MemberPhone;
                                _bookingDTO.ResourceTypeID = _ResourceTypeID;
                                _bookingDTO.ResourceTypename = _ResourceTypeName;
                                _bookingDTO.RoomOrTrainer = _RoomOrTrainer;
                                _bookingDTO.SiteID = _SiteID;
                                _bookingDTO.Time = _BookingTime;
                                _bookingDTO.BookingStatus = _BookingStatus;
                                _bookingDTO.WaitListNo = _BookingWLOrder;
                                _bookingDTO.FacilityID = _FacilityID;
                                _bookingDTO.SessionNumber = _SessionNumber;


                                int Result = _bookingBAL.addBooking(_bookingDTO);
                                if (Result == 1)
                                {
                                    //System.Windows.MessageBox.Show("Booking Added Successfully!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                    cldrMemberBkg.SelectedDate = _BookingDate;
                                }
                                else
                                {
                                    System.Windows.MessageBox.Show("Booking Failed..!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                                }
                            }
                            else
                            {
                                System.Windows.MessageBox.Show("NO BOOKING AVAILABLE for selected Date / Facility!\n\nMax allowed Slots have been booked!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                                return;
                            }
                        }
                    }
                }

                gvMemberBkgMemberDetails.SelectedIndex = -1;
                clearFacilityBookingData();
            }
            catch(Exception ex)
            {
                System.Windows.MessageBox.Show("Booking Failed..!\nError: " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void gvMemberBkgMemberDetails_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            gvMemberBkgTiming.IsEnabled = true;
        }

        private void txtMemberBkgFindMember_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                _MemberLastName = txtMemberBkgFindMember.Text.ToString().Trim();
                MemberList_Filter();

                gvMemberBkgTiming.IsEnabled = false;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString(), this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private void getResourceTypes()
        {
            cmbMemberBkgResourceType.Items.Clear();
            _utilites.PopulateResourceTypes(cmbMemberBkgResourceType);
        }
        private void MemberList()
        {
            gvMemberBkgMemberDetails.ItemsSource = null;
            gvMemberBkgMemberDetails.Items.Clear();
            
            _dsBookingMembers = _bookingBAL.get_BookingMembersDetails(_MemberLastName);
            if (_dsBookingMembers != null)
            {
                if (_dsBookingMembers.Tables.Count > 0)
                {
                    gvMemberBkgMemberDetails.ItemsSource = _dsBookingMembers.Tables[0].DefaultView;
                    gvMemberBkgMemberDetails.IsReadOnly = true;
                }
            }
        }
        private void MemberList_Filter()
        {
            gvMemberBkgMemberDetails.ItemsSource = null;
            gvMemberBkgMemberDetails.Items.Clear();

            if (_dsBookingMembers != null)
            {
                if (_dsBookingMembers.Tables.Count > 0)
                {
                    _dsBookingMembers.Tables[0].DefaultView.RowFilter = "";
                    _dsBookingMembers.Tables[0].DefaultView.RowFilter = "(LastName LIKE '%" + _MemberLastName + "%' OR FirstName LIKE '%" + _MemberLastName + "%')";
                    gvMemberBkgMemberDetails.ItemsSource = _dsBookingMembers.Tables[0].DefaultView;
                    gvMemberBkgMemberDetails.IsReadOnly = true;
                }
            }
        }
        private void gvMemberBkgTiming_Booking(object sender, RoutedEventArgs e)
        {
        }
        private void getBooking_ForSelectedDate(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cmbMemberBkgResourceType.SelectedIndex != -1)
                {
                    if (cmbMemberBkgResourceType.SelectedIndex == 1)
                    {
                        getBooking_FillGridDetails();
                    }
                    else
                    {
                        //clearFacilityBookingData();
                        if (lstSessions.Items.Count > 0)
                        {
                            int _currIndex = 0;
                            if (lstSessions.SelectedIndex != -1) _currIndex = lstSessions.SelectedIndex;
                            lstSessions.SelectedIndex = -1;
                            lstSessions.SelectedIndex = _currIndex;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString(), this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void getBooking_FillGridDetails()
        {
            clearBookingData();
            if (cmbMemberBkgResourceType.SelectedIndex != -1)
            {
                DateTime _selDate;
                if (cldrMemberBkg.SelectedDate == null)
                {
                    _selDate = DateTime.Now.Date;
                }
                else
                {
                    _selDate = (DateTime)cldrMemberBkg.SelectedDate;
                }
                int _CompanyID; int _SiteID; int _ResourceTypeID;

                _ResourceTypeID = Convert.ToInt32(cmbMemberBkgResourceType.SelectedValue);
                _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                DataSet ds = _bookingBAL.getBooking(_CompanyID, _SiteID, _ResourceTypeID, _selDate);

                if (ds != null)
                {
                    if (ds.Tables[_BookingsTable].DefaultView.Count > 0)
                    {
                        int _bookingRowIdx = 0;
                        while (_bookingRowIdx < ds.Tables[_BookingsTable].DefaultView.Count)
                        {
                            for (int i = 0; i < gvMemberBkgTiming.Items.Count; i++) //for each row in Grid
                            {
                                if ((gvMemberBkgTiming.Items[i] as DataRowView).Row["Time Slot"].ToString().ToUpper() == ds.Tables[_BookingsTable].DefaultView[_bookingRowIdx]["Time"].ToString().ToUpper())
                                {
                                    for (int j = 1; j < gvMemberBkgTiming.Columns.Count; j++) //each Grid column in that row
                                    {
                                        if (gvMemberBkgTiming.Columns[j].Header.ToString().ToUpper() == ds.Tables[_BookingsTable].DefaultView[_bookingRowIdx]["RoomOrTrainer"].ToString().ToUpper())
                                        {
                                            if ((gvMemberBkgTiming.Items[i] as DataRowView).Row[j].ToString().Trim() == "")
                                            {
                                                (gvMemberBkgTiming.Items[i] as DataRowView).Row[j] = ds.Tables[_BookingsTable].DefaultView[_bookingRowIdx]["FirstName"].ToString() + " " + ds.Tables[_BookingsTable].DefaultView[_bookingRowIdx]["LastName"].ToString();
                                            }
                                            else
                                            {
                                                (gvMemberBkgTiming.Items[i] as DataRowView).Row[j] = (gvMemberBkgTiming.Items[i] as DataRowView).Row[j].ToString() + "\n" + ds.Tables[_BookingsTable].DefaultView[_bookingRowIdx]["FirstName"].ToString() + " " + ds.Tables[_BookingsTable].DefaultView[_bookingRowIdx]["LastName"].ToString();
                                            }
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            _bookingRowIdx++;
                        }
                    }
                    gvMemberBkgSlotsAvailable.ItemsSource = null;
                    if (_ResourceTypeName == "FACILITY")
                    {
                        gvMemberBkgSlotsAvailable.ItemsSource = ds.Tables[_MaxCountTable].DefaultView;
                    }
                }
            }
        }

        private void clearBookingData()
        {
            for (int i = 0; i < gvMemberBkgTiming.Items.Count; i++) //for each row in Grid
            {
                for (int j = 1; j < gvMemberBkgTiming.Columns.Count; j++) //each Grid column in that row
                {
                    (gvMemberBkgTiming.Items[i] as DataRowView).Row[j] = "";
                }
            }
            gvMemberBkgSlotsAvailable.ItemsSource = null;

            clearFacilityBookingData();
        }

        private void clearFacilityBookingData()
        {
            splButtonFacility.SelectedIndex = -1;
            lstSessions.SelectedIndex = -1;
            lblCurrentBooking.Content = "";
            lblTimeSlot.Content = "";
        }

        private bool CheckMaxBooking(string _FacilityName)
        {
            bool _isFacListed = false;
            for (int i = 0; i < gvMemberBkgSlotsAvailable.Items.Count; i++)
            {
                if ((gvMemberBkgSlotsAvailable.Items[i] as DataRowView).Row["Facility Name"].ToString().ToUpper() == _FacilityName.ToUpper())
                {
                    _isFacListed = true;
                    if (Convert.ToInt32((gvMemberBkgSlotsAvailable.Items[i] as DataRowView).Row["Available Slots"].ToString()) <= 0)
                    {
                        //MAX Slots filled
                        if (Convert.ToInt32((gvMemberBkgSlotsAvailable.Items[i] as DataRowView).Row["Open Wait List"].ToString()) <= 0)
                        {
                            return true; //Max. Waitlist Slots also filled
                        }
                        else
                        {
                            _isBookingWaitList = true;
                            int _CurrWLBooking = 0; int _AvailableWL = 0;
                            _CurrWLBooking = Convert.ToInt32((gvMemberBkgSlotsAvailable.Items[i] as DataRowView).Row["Current Wait List"].ToString());
                            _AvailableWL = Convert.ToInt32((gvMemberBkgSlotsAvailable.Items[i] as DataRowView).Row["Open Wait List"].ToString());

                            _CurrWLBooking = _CurrWLBooking + 1;
                            _BookingWLOrder = _CurrWLBooking;
                            _BookingStatus = "W";
                            _AvailableWL = _AvailableWL - 1;

                            (gvMemberBkgSlotsAvailable.Items[i] as DataRowView).Row["Current Wait List"] = _CurrWLBooking.ToString();
                            (gvMemberBkgSlotsAvailable.Items[i] as DataRowView).Row["Open Wait List"] = _AvailableWL.ToString();
                        }
                    }
                    else
                    {
                        int _CurrBooking; int _Available;
                        _CurrBooking = Convert.ToInt32((gvMemberBkgSlotsAvailable.Items[i] as DataRowView).Row["Current Booking"].ToString());
                        _Available = Convert.ToInt32((gvMemberBkgSlotsAvailable.Items[i] as DataRowView).Row["Available Slots"].ToString());

                        _CurrBooking = _CurrBooking + 1;
                        _Available = _Available - 1;

                        (gvMemberBkgSlotsAvailable.Items[i] as DataRowView).Row["Current Booking"] = _CurrBooking.ToString();
                        (gvMemberBkgSlotsAvailable.Items[i] as DataRowView).Row["Available Slots"] = _Available.ToString();
                    }
                    break;
                }
            }
            
            return false;
        }

        private bool CheckMaxBookingSession(int _FacilityID, int _SessionNumber)
        {
            if (_FacilityID != 0)
            {
                if (_SessionNumber != 0)
                {
                    if (_MaxWaitListCurrentSession <= _WaitListCountCurrentSession)
                    {
                        return true;
                    }
                    if (_MaxMembersCurrentSession <= _BookedCountCurrentSession)
                    {
                        _isBookingWaitList = true;
                        int _CurrWLBooking = 0; int _AvailableWL = 0;

                        _dsSlots.Tables[0].DefaultView.RowFilter = "";

                        _CurrWLBooking = _WaitListCountCurrentSession;
                        _AvailableWL = _MaxWaitListCurrentSession - _WaitListCountCurrentSession;

                        _CurrWLBooking = _CurrWLBooking + 1;
                        _BookingWLOrder = _CurrWLBooking;
                        _BookingStatus = "W";
                    }
                    else
                    {
                        _isBookingWaitList = false;
                        _BookingStatus = "B";
                        return false;
                    }
                }
                return false;
            }
            return false;
        }

        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void gvMemberBkgTiming_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void splButtonFacility_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int i = splButtonFacility.SelectedIndex;
            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            lblTimeSlot.Content = "";
            lblCurrentBooking.Content = "";
            lstSessions.ItemsSource = null;
            rchtxtMemberBkg.Text = "";

            if (i != -1)
            {
                _FacilityID = Convert.ToInt32(_dsFacilityGlobal.Tables[0].DefaultView[i]["FacilityID"].ToString());
                _dsSlots = _bookingBAL.getFacilitySlots(_FacilityID);

                if (_dsSlots != null)
                {
                    if (_dsSlots.Tables.Count > 0)
                    {
                        if (_dsSlots.Tables[0].DefaultView.Count > 0)
                        {
                            lstSessions.ItemsSource = _dsSlots.Tables[0].DefaultView;
                            lstSessions.DisplayMemberPath = "SessionName";
                            lstSessions.SelectedValuePath = "SlotSequence";

                            lstSessions.SelectedIndex = 0;
                            getBookingDetails_Facility(false);
                        }
                    }
                }
            }
        }

        private void lstSessions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int _selectedSessionIndex = lstSessions.SelectedIndex;
            int _selectedFacilityIndex = splButtonFacility.SelectedIndex;

            if (_selectedSessionIndex != -1)
            {
                getBookingDetails_Facility(false);

                lblTimeSlot.Content = "From   - " + _dsSlots.Tables[0].DefaultView[_selectedSessionIndex]["StartTime"].ToString() + 
                    "\nTo     - " + _dsSlots.Tables[0].DefaultView[_selectedSessionIndex]["EndTime"].ToString() +
                    "\n\nCharge - $" + Convert.ToDecimal(_dsFacilityGlobal.Tables[0].DefaultView[_selectedFacilityIndex]["ChargePerBooking"].ToString()).ToString("##0.00");

                _MaxMembersCurrentSession = Convert.ToInt32(_dsFacilityGlobal.Tables[0].DefaultView[_selectedFacilityIndex]["MaxMembers"].ToString());
                _MaxWaitListCurrentSession = Convert.ToInt32(_dsFacilityGlobal.Tables[0].DefaultView[_selectedFacilityIndex]["MaxWaitList"].ToString());

                lblCurrentBooking.Content = "Max. Slots : "+ _dsFacilityGlobal.Tables[0].DefaultView[_selectedFacilityIndex]["MaxMembers"].ToString() + 
                    "\nAvlb Slots : " + (Convert.ToInt32 (_dsFacilityGlobal.Tables[0].DefaultView[_selectedFacilityIndex]["MaxMembers"].ToString()) - _BookedCountCurrentSession).ToString() + 
                    "\n\nMax. Wait List : " + _dsFacilityGlobal.Tables[0].DefaultView[_selectedFacilityIndex]["MaxWaitList"].ToString() +
                    "\nAvlb Wait List : " + (Convert.ToInt32(_dsFacilityGlobal.Tables[0].DefaultView[_selectedFacilityIndex]["MaxWaitList"].ToString()) - _WaitListCountCurrentSession).ToString();
            }
        }
        
        private void getBookingDetails_Facility(bool _Filter = false)
        {
            if (cmbMemberBkgResourceType.SelectedIndex != -1)
            {
                if (_ResourceTypeName == "FACILITY")
                {
                    _BookedCountCurrentSession = 0;
                    _WaitListCountCurrentSession = 0;
                    rchtxtMemberBkg.Text = "";

                    DateTime _selDate;
                    if (cldrMemberBkg.SelectedDate == null)
                    {
                        _selDate = DateTime.Now.Date;
                    }
                    else
                    {
                        _selDate = (DateTime)cldrMemberBkg.SelectedDate;
                    }
                    int _CompanyID; int _SiteID; int _ResourceTypeID;

                    _ResourceTypeID = Convert.ToInt32(cmbMemberBkgResourceType.SelectedValue);
                    _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                    _SessionNumber = Convert.ToInt32(lstSessions.SelectedValue);

                    if (!_Filter)
                    {
                        _dsBookingDetails = _bookingBAL.getBooking(_CompanyID, _SiteID, _ResourceTypeID, _selDate, 0, _FacilityID);
                    }

                    if (_dsBookingDetails != null)
                    {
                        if (_dsBookingDetails.Tables.Count > 0)
                        {
                            if (_dsBookingDetails.Tables[_BookingsTable].DefaultView.Count > 0)
                            {
                                rchtxtMemberBkg.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                                _dsBookingDetails.Tables[_BookingsTable].DefaultView.RowFilter = "";
                                _dsBookingDetails.Tables[_BookingsTable].DefaultView.RowFilter = "BookingStatus = 'B' AND SessionNumber = " + _SessionNumber;
                                if (_dsBookingDetails.Tables[_BookingsTable].DefaultView.Count > 0)
                                {
                                    _BookedCountCurrentSession = _dsBookingDetails.Tables[_BookingsTable].DefaultView.Count;
                                    rchtxtMemberBkg.Text = rchtxtMemberBkg.Text + _dsBookingDetails.Tables[_BookingsTable].DefaultView[0]["RoomOrTrainer"].ToString() + " (BOOKED)\nDate : "
                                            + Convert.ToDateTime(_dsBookingDetails.Tables[_BookingsTable].DefaultView[0]["Date"]).Date.ToString("dd/MMM/yyyy")
                                            + " Session No. " + Convert.ToInt32(_dsBookingDetails.Tables[_BookingsTable].DefaultView[0]["SessionNumber"]).ToString("#0") + "\n\n";

                                    for (int i = 0; i < _dsBookingDetails.Tables[_BookingsTable].DefaultView.Count; i++)
                                    {
                                        rchtxtMemberBkg.Text = rchtxtMemberBkg.Text + (i+1).ToString("#0") + ". "+ _dsBookingDetails.Tables[_BookingsTable].DefaultView[i]["FirstName"].ToString() + ", "
                                            + _dsBookingDetails.Tables[_BookingsTable].DefaultView[i]["LastName"].ToString() + "\n";
                                    }
                                }

                                _dsBookingDetails.Tables[_BookingsTable].DefaultView.RowFilter = "";
                                _dsBookingDetails.Tables[_BookingsTable].DefaultView.RowFilter = "BookingStatus = 'W' AND SessionNumber = " + _SessionNumber;
                                _dsBookingDetails.Tables[_BookingsTable].DefaultView.Sort = " WaitListNo ASC ";

                                if (_dsBookingDetails.Tables[_BookingsTable].DefaultView.Count > 0)
                                {
                                    _WaitListCountCurrentSession = _dsBookingDetails.Tables[_BookingsTable].DefaultView.Count;
                                    rchtxtMemberBkg.Text = rchtxtMemberBkg.Text + "\n" +_dsBookingDetails.Tables[_BookingsTable].DefaultView[0]["RoomOrTrainer"].ToString() + " (WAIT LISTED)\nDate : "
                                            + Convert.ToDateTime(_dsBookingDetails.Tables[_BookingsTable].DefaultView[0]["Date"]).Date.ToString("dd/MMM/yyyy")
                                            + " Session No. " + Convert.ToInt32(_dsBookingDetails.Tables[_BookingsTable].DefaultView[0]["SessionNumber"]).ToString("#0") + "\n\n";

                                    for (int i = 0; i < _dsBookingDetails.Tables[_BookingsTable].DefaultView.Count; i++)
                                    {
                                        rchtxtMemberBkg.Text = rchtxtMemberBkg.Text + (i + 1).ToString("#0") + ". " + _dsBookingDetails.Tables[_BookingsTable].DefaultView[i]["FirstName"].ToString() + ", "
                                            + _dsBookingDetails.Tables[_BookingsTable].DefaultView[i]["LastName"].ToString() + " (Waitlist No. " + _dsBookingDetails.Tables[_BookingsTable].DefaultView[i]["WaitListNo"].ToString()+ ")\n";
                                    }
                                }
                                _dsBookingDetails.Tables[_BookingsTable].DefaultView.RowFilter = "";
                            }
                        }
                    }
                }
            }
        }

        //INVOICE RAISED; FOR BOOKING
        private int RAISE_BOOKING_FEE_INVOICE(decimal _BookingCharge, int _MemberID)
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                int _memberInvoiceNo = GetMembershipInvoiceNo();
                if (_memberInvoiceNo != -1)
                {
                    DateTime _currentDate = DateTime.Now;

                    decimal _invoiceAmount = _BookingCharge;
                    decimal _amountPaid = 0m;
                    decimal _amountOutStanding = Convert.ToDecimal(_invoiceAmount - _amountPaid);
                    int _invoiceMemberId = _MemberID;
                    string _invoiceStatus = "Pending";
                    _memberDetailsBal.raise_Invoice(_currentDate, _memberInvoiceNo, _invoiceAmount, _amountPaid, _amountOutStanding, _invoiceMemberId, _invoiceStatus, "InvoiceNo");
                    return _memberInvoiceNo;
                }
                return -1;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString(), this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return -1;
            }
        }

        private void RAISE_BOOKING_FEE_INVOICE_DETAILS(int _InvNo, decimal _BookingCharge, int _MemberID, string _BookingDesc)
        {
            DataTable _InvoiceTable = new DataTable("InvoiceDetails");
            PointOfSaleBAL _InvoiceDetailsBAL = new PointOfSaleBAL();

            _InvoiceTable.Columns.Add("InvoiceNo", typeof(int));
            _InvoiceTable.Columns.Add("MemberNo", typeof(int));
            _InvoiceTable.Columns.Add("InvoiceDetails", typeof(string));
            _InvoiceTable.Columns.Add("Amount", typeof(decimal));
            _InvoiceTable.Columns.Add("Status", typeof(string));
            _InvoiceTable.Columns.Add("ProdSale", typeof(char));
            _InvoiceTable.Columns.Add("ProdID", typeof(int));
            _InvoiceTable.Columns.Add("ProdQty", typeof(int));

            DataRow dR = _InvoiceTable.NewRow();
            decimal _invoiceAmount = _BookingCharge;

            dR["InvoiceNo"] = _InvNo;
            dR["MemberNo"] = _MemberID;
            dR["InvoiceDetails"] = _BookingDesc;
            dR["Amount"] = _invoiceAmount;
            dR["Status"] = "Pending";
            dR["ProdSale"] = 'C';
            dR["ProdID"] = 0;
            dR["ProdQty"] = 0;

            _InvoiceTable.Rows.Add(dR);
            _InvoiceDetailsBAL.SavePOSInvoiceDetails(_InvoiceTable);
        }

        private int GetMembershipInvoiceNo()
        {
            try
            {
                int _invoiceNo;

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                DataSet ds = _memberDetailsBal.get_LastUsedNoByCompanySite("InvoiceNo", _CompanyID, _SiteID);
                if (ds != null)
                {
                    _invoiceNo = Convert.ToInt32(ds.Tables[0].Rows[0]["LastUsedNo"].ToString().Trim());
                    return _invoiceNo;
                }
                else
                {
                    System.Windows.MessageBox.Show("No Data! (Invoice Number AutoGenerated)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return -1;
                }
            }
            catch (Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return -1;
            }
        }
    }
}
