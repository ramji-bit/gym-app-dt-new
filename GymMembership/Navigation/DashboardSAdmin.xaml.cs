﻿using System;
using System.Windows.Media;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Data;
using System.Windows.Threading;
using System.ComponentModel;
using System.Windows.Input;
using System.Configuration;

using Microsoft.Win32;

using GymMembership.Accounts;
using GymMembership.MemberInfo;
using GymMembership.Pointofsale;
using GymMembership.LastVisitors;
using GymMembership.Reports;
using GymMembership.Settings;

namespace GymMembership.Navigation
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class DashboardSAdmin
    {
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _CompanyAndSiteName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public Login _Login;

        public bool _isSessionNeverExpire = false;
        public bool _isDishonourCheckEnabled = true;

        DispatcherTimer mIdle;
        private const long cIdleSeconds = 900;

        bool _IsIdle = false;
        int _CompanyID;
        int _LoginID;
        int _SiteID;

        DataTable _dtOrgNamesG;
        DataTable _dtSiteNamesG;

        int _currentOrgIndex = -1;
        int _currentSiteIndex = -1;
        int _currentSelectedOrg = -1;
        int _currentSelectedSite = -1;

        Brush _siteButtonBkColor = Brushes.SkyBlue;
        Brush _orgButtonBkColor = Brushes.MediumAquamarine;
        bool _fromRedButton = false;
        public DashboardSAdmin()
        {
            try
            {
                InitializeComponent();
                this.Closing += new CancelEventHandler(DashBoard_Closing);

                SystemEvents.SessionSwitch += new SessionSwitchEventHandler(SystemEvents_SessionSwitch);

                InputManager.Current.PreProcessInput += Idle_PreProcessInput;
                mIdle = new DispatcherTimer();
                mIdle.Interval = new TimeSpan(cIdleSeconds * 1000 * 10000);
                mIdle.IsEnabled = true;
                mIdle.Tick += Idle_Tick;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        ~DashboardSAdmin()
        {
            SystemEvents.SessionSwitch -= new SessionSwitchEventHandler(SystemEvents_SessionSwitch);
        }

        void SystemEvents_SessionSwitch(object sender, SessionSwitchEventArgs e)
        {
            switch (e.Reason)
            {
                case SessionSwitchReason.SessionUnlock:
                    break;

                case SessionSwitchReason.SessionLock:
                    _IsIdle = true;
                    this.Close();
                    break;
            }
        }
        void Idle_Tick(object sender, EventArgs e)
        {
            if (!_isSessionNeverExpire)
            {
                _IsIdle = true;
                this.Close();
            }
        }
        void Idle_PreProcessInput(object sender, PreProcessInputEventArgs e)
        {
            mIdle.IsEnabled = false;
            mIdle.IsEnabled = true;
        }
        void DashBoard_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                if (!_IsIdle)
                {
                    if (!Comman.Constants._emailBeingSent)
                    {
                        if (MessageBox.Show("Are you sure to LOGOFF ?", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            if (!Comman.Constants._SuperAdmin)
                            {
                                Comman.Constants._SuperAdmin = false;
                                ConfigurationManager.AppSettings["SuperAdmin"] = "";
                                ConfigurationManager.AppSettings["IsAdminUser"] = "";
                                ConfigurationManager.AppSettings["LoggedInUser"] = "";
                                ConfigurationManager.AppSettings["LoggedUserLoginID"] = "";

                                if (!Comman.Constants._OfflineFlag)
                                {
                                    Comman.Constants._DisConnectedDataAdapter.Update(Comman.Constants._DisConnectedALLTables);
                                }
                                Comman.Constants._OfflineFlag = true;

                                _Login.cmbOrgName.Text = _CompanyName.ToUpper();
                                _Login.txtPassword.Password = "";
                                _Login.txtUserName.Text = "";
                                _Login.ShowInTaskbar = true;
                                _Login.multiSiteAccessFile.Start();
                                _Login.Show();

                                _Login.txtUserName.Focus();
                            }
                            else
                            {
                                Comman.Constants._SuperAdmin = false;
                                ConfigurationManager.AppSettings["SuperAdmin"] = "";
                                ConfigurationManager.AppSettings["IsAdminUser"] = "";
                                ConfigurationManager.AppSettings["LoggedInUser"] = "";
                                ConfigurationManager.AppSettings["LoggedUserLoginID"] = "";

                                _Login.txtPassword.Password = "";
                                _Login.txtUserName.Text = "";
                                _Login.ShowInTaskbar = true;
                                _Login.Show();
                            }
                        }
                        else
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                    else
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                else
                {
                    _Login.cmbOrgName.Text = _CompanyName.ToUpper();
                    _Login.txtPassword.Password = "";
                    _Login.txtUserName.Text = "";
                    _Login.ShowInTaskbar = true;
                    _Login.multiSiteAccessFile.Start();
                    _Login.Show();
                    _Login.txtUserName.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Comman.Constants._SuperAdmin)
                {
                    lblUserName.Content = "SUPER ADMIN";
                    lblLoggedInTime.Content = _CurrentLoginDateTime;

                    DisableHideItemsOnLoad();

                    LoadOrgNames();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                this.Cursor = Cursors.Arrow;
                return;
            }
        }
        private void LoadOrgNames()
        {
            try
            {
                SqlHelper _sql = new SqlHelper();
                if (_sql._ConnOpen == 0) return;

                Hashtable ht = new Hashtable();
                DataSet _dsOrg = _sql.ExecuteProcedure("GetCompanyNames", ht);
                if (_dsOrg != null)
                {
                    if (_dsOrg.Tables.Count > 0)
                    {
                        _dtOrgNamesG = _dsOrg.Tables[0];
                        if (_dsOrg.Tables.Count > 1)
                        {
                            _dtSiteNamesG = _dsOrg.Tables[1];
                        }

                        if (_dsOrg.Tables[0].DefaultView.Count > 0)
                        {
                            for (int i = 0; i < _dsOrg.Tables[0].DefaultView.Count; i++)
                            {
                                if (i <= 9)
                                {
                                    switch (i)
                                    {
                                        case 0:
                                            btnOrg1.IsEnabled = true;
                                            btnOrg1.Visibility = Visibility.Visible;
                                            btnOrg1.Content = _dtOrgNamesG.DefaultView[i]["CompanyName"].ToString();
                                            break;
                                        case 1:
                                            btnOrg2.IsEnabled = true;
                                            btnOrg2.Visibility = Visibility.Visible;
                                            btnOrg2.Content = _dtOrgNamesG.DefaultView[i]["CompanyName"].ToString();
                                            break;
                                        case 2:
                                            btnOrg3.IsEnabled = true;
                                            btnOrg3.Visibility = Visibility.Visible;
                                            btnOrg3.Content = _dtOrgNamesG.DefaultView[i]["CompanyName"].ToString();
                                            break;
                                        case 3:
                                            btnOrg4.IsEnabled = true;
                                            btnOrg4.Visibility = Visibility.Visible;
                                            btnOrg4.Content = _dtOrgNamesG.DefaultView[i]["CompanyName"].ToString();
                                            break;
                                        case 4:
                                            btnOrg5.IsEnabled = true;
                                            btnOrg5.Visibility = Visibility.Visible;
                                            btnOrg5.Content = _dtOrgNamesG.DefaultView[i]["CompanyName"].ToString();
                                            break;
                                        case 5:
                                            btnOrg6.IsEnabled = true;
                                            btnOrg6.Visibility = Visibility.Visible;
                                            btnOrg6.Content = _dtOrgNamesG.DefaultView[i]["CompanyName"].ToString();
                                            break;
                                        case 6:
                                            btnOrg7.IsEnabled = true;
                                            btnOrg7.Visibility = Visibility.Visible;
                                            btnOrg7.Content = _dtOrgNamesG.DefaultView[i]["CompanyName"].ToString();
                                            break;
                                        case 7:
                                            btnOrg8.IsEnabled = true;
                                            btnOrg8.Visibility = Visibility.Visible;
                                            btnOrg8.Content = _dtOrgNamesG.DefaultView[i]["CompanyName"].ToString();
                                            break;
                                        case 8:
                                            btnOrg9.IsEnabled = true;
                                            btnOrg9.Visibility = Visibility.Visible;
                                            btnOrg9.Content = _dtOrgNamesG.DefaultView[i]["CompanyName"].ToString();
                                            break;
                                        case 9:
                                            btnOrg10.IsEnabled = true;
                                            btnOrg10.Visibility = Visibility.Visible;
                                            btnOrg10.Content = _dtOrgNamesG.DefaultView[i]["CompanyName"].ToString();
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }
        private void HideDisableORGButtons()
        {
            btnOrg1.Visibility = Visibility.Hidden;
            btnOrg1.IsEnabled = false;
            arrowToSite1.Visibility = Visibility.Hidden;

            btnOrg2.Visibility = Visibility.Hidden;
            btnOrg2.IsEnabled = false;
            arrowToSite2.Visibility = Visibility.Hidden;

            btnOrg3.Visibility = Visibility.Hidden;
            btnOrg3.IsEnabled = false;
            arrowToSite3.Visibility = Visibility.Hidden;

            btnOrg4.Visibility = Visibility.Hidden;
            btnOrg4.IsEnabled = false;
            arrowToSite4.Visibility = Visibility.Hidden;

            btnOrg5.Visibility = Visibility.Hidden;
            btnOrg5.IsEnabled = false;
            arrowToSite5.Visibility = Visibility.Hidden;

            btnOrg6.Visibility = Visibility.Hidden;
            btnOrg6.IsEnabled = false;
            arrowToSite6.Visibility = Visibility.Hidden;

            btnOrg7.Visibility = Visibility.Hidden;
            btnOrg7.IsEnabled = false;
            arrowToSite7.Visibility = Visibility.Hidden;

            btnOrg8.Visibility = Visibility.Hidden;
            btnOrg8.IsEnabled = false;
            arrowToSite8.Visibility = Visibility.Hidden;

            btnOrg9.Visibility = Visibility.Hidden;
            btnOrg9.IsEnabled = false;
            arrowToSite9.Visibility = Visibility.Hidden;

            btnOrg10.Visibility = Visibility.Hidden;
            btnOrg10.IsEnabled = false;
            arrowToSite10.Visibility = Visibility.Hidden;
        }
        private void HideDisableSITEButtons()
        {
            btnSite1.Visibility = Visibility.Hidden;
            btnSite1.IsEnabled = false;
            btnSite1.Background = _siteButtonBkColor;
            subMenuToSite1.Visibility = Visibility.Hidden;

            btnSite2.Visibility = Visibility.Hidden;
            btnSite2.IsEnabled = false;
            btnSite2.Background = _siteButtonBkColor;
            subMenuToSite2.Visibility = Visibility.Hidden;

            btnSite3.Visibility = Visibility.Hidden;
            btnSite3.IsEnabled = false;
            btnSite3.Background = _siteButtonBkColor;
            subMenuToSite3.Visibility = Visibility.Hidden;

            btnSite4.Visibility = Visibility.Hidden;
            btnSite4.IsEnabled = false;
            btnSite4.Background = _siteButtonBkColor;
            subMenuToSite4.Visibility = Visibility.Hidden;

            btnSite5.Visibility = Visibility.Hidden;
            btnSite5.IsEnabled = false;
            btnSite5.Background = _siteButtonBkColor;
            subMenuToSite5.Visibility = Visibility.Hidden;

            btnSite6.Visibility = Visibility.Hidden;
            btnSite6.IsEnabled = false;
            btnSite6.Background = _siteButtonBkColor;
            subMenuToSite6.Visibility = Visibility.Hidden;

            btnSite7.Visibility = Visibility.Hidden;
            btnSite7.IsEnabled = false;
            btnSite7.Background = _siteButtonBkColor;
            subMenuToSite7.Visibility = Visibility.Hidden;

            btnSite8.Visibility = Visibility.Hidden;
            btnSite8.IsEnabled = false;
            btnSite8.Background = _siteButtonBkColor;
            subMenuToSite8.Visibility = Visibility.Hidden;

            btnSite9.Visibility = Visibility.Hidden;
            btnSite9.IsEnabled = false;
            btnSite9.Background = _siteButtonBkColor;
            subMenuToSite9.Visibility = Visibility.Hidden;

            btnSite10.Visibility = Visibility.Hidden;
            btnSite10.IsEnabled = false;
            btnSite10.Background = _siteButtonBkColor;
            subMenuToSite10.Visibility = Visibility.Hidden;

            ActionPanelHide();
        }
        private void DisableHideItemsOnLoad()
        {
            try
            {
                lblOrgName.Visibility = Visibility.Collapsed;
                cmbOrgName.Visibility = Visibility.Collapsed;
                cmbSite.Visibility = Visibility.Collapsed;
                tblockSiteNames.Visibility = Visibility.Hidden;

                imggreenTick.Visibility = Visibility.Hidden;
                imgredTick.Visibility = Visibility.Hidden;

                ActionPanelHide();

                //ORG. NAME BUTTONS
                HideDisableORGButtons();

                //SITE NAME BUTTONS
                HideDisableSITEButtons();
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }
        private void Window_UnLoaded(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void btnDashboardQuit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
                //Closing event called
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void cmbSite_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (cmbSite.SelectedIndex != -1)
            //{
            //    ConfigurationManager.AppSettings["LoggedInSiteID"] = cmbSite.SelectedValue.ToString();
            //    ConfigurationManager.AppSettings["LoggedInSite"] = (cmbSite.SelectedItem as DataRowView).Row["SiteName"].ToString();
            //    _SiteName = ConfigurationManager.AppSettings["LoggedInSite"].ToString().ToUpper();
            //    _CompanyAndSiteName = _CompanyName + " ( " + _SiteName.Replace("_", "__") + " )";

            //    //lblSiteName.Content = (cmbSite.SelectedItem as DataRowView).Row["SiteName"].ToString().Replace("_", "__");
            //    if (_dsSites.Tables[0].DefaultView.Count > 0)
            //    {
            //        _dsSites.Tables[0].DefaultView.RowFilter = "";
            //        //_dsSites.Tables[0].DefaultView.RowFilter = "SiteName = '" + (cmbSite.SelectedItem as DataRowView).Row["SiteName"].ToString() + "'";
            //        _dsSites.Tables[0].DefaultView.RowFilter = "SiteID = " +  Convert.ToInt32(cmbSite.SelectedValue);

            //        if (_dsSites.Tables[0].DefaultView.Count > 0)
            //        {
            //            ConfigurationManager.AppSettings["AccessControl_FilePath"] = _dsSites.Tables[0].DefaultView[0]["AccessControl_FilePath"].ToString();
            //            ConfigurationManager.AppSettings["AccessControl_FileName"] = _dsSites.Tables[0].DefaultView[0]["AccessControl_FileName"].ToString();

            //            //EMAIL config from SiteMaster
            //            ConfigurationManager.AppSettings["SMTPHost"] = _dsSites.Tables[0].DefaultView[0]["SMTPHost"].ToString();
            //            ConfigurationManager.AppSettings["SMTPPort"] = _dsSites.Tables[0].DefaultView[0]["SMTPPort"].ToString();
            //            ConfigurationManager.AppSettings["SMTPUserName"] = _dsSites.Tables[0].DefaultView[0]["SMTPUserName"].ToString();
            //            ConfigurationManager.AppSettings["SMTPEncryptPassword"] = _dsSites.Tables[0].DefaultView[0]["SMTPEncryptPassword"].ToString();

            //            //Paychoice config from SiteMaster
            //            ConfigurationManager.AppSettings["PaychoiceSandboxURL"] = _dsSites.Tables[0].DefaultView[0]["SandboxURL"].ToString();
            //            ConfigurationManager.AppSettings["PaychoiceSandboxUsername"] = _dsSites.Tables[0].DefaultView[0]["SandboxUsername"].ToString();
            //            ConfigurationManager.AppSettings["PaychoiceSandboxPassword"] = _dsSites.Tables[0].DefaultView[0]["SandboxPassword"].ToString();
            //            ConfigurationManager.AppSettings["PaychoiceSecuredURL"] = _dsSites.Tables[0].DefaultView[0]["SecureURL"].ToString();
            //            ConfigurationManager.AppSettings["PaychoiceSecuredUsername"] = _dsSites.Tables[0].DefaultView[0]["SecureUsername"].ToString();
            //            ConfigurationManager.AppSettings["PaychoiceSecuredPassword"] = _dsSites.Tables[0].DefaultView[0]["SecurePassword"].ToString();
            //            ConfigurationManager.AppSettings["PaychoiceUsagePortal"] = _dsSites.Tables[0].DefaultView[0]["ActivePortal"].ToString();

            //            ////Address details from SiteMaster - for POS
            //            ConfigurationManager.AppSettings["selectedSiteAddr1"] = _dsSites.Tables[0].DefaultView[0]["Addr1"].ToString();
            //            ConfigurationManager.AppSettings["selectedSiteAddr2"] = _dsSites.Tables[0].DefaultView[0]["Addr2"].ToString();
            //            ConfigurationManager.AppSettings["selectedSiteState"] = _dsSites.Tables[0].DefaultView[0]["State"].ToString();

            //            ////POS details from SiteMaster
            //            ConfigurationManager.AppSettings["POSOpeningBalance"] = _dsSites.Tables[0].DefaultView[0]["POS_OpeningBalance"].ToString();
            //            ConfigurationManager.AppSettings["POSReportEmail"] = _dsSites.Tables[0].DefaultView[0]["POS_Report_Email"].ToString();
            //            ConfigurationManager.AppSettings["POSStockLevelAlertEmail"] = _dsSites.Tables[0].DefaultView[0]["POS_Stock_Level_Alert_Email"].ToString();
            //        }
            //        _dsSites.Tables[0].DefaultView.RowFilter = "";
            //    }
            //    txtMemberNameorCardNo.Text = "";
            //    LoadDoors();
            //    //LoadMemberListFromDBOnLoad();
            //    GetRecentCheckInDetails();
            //}
        }
        private void cmbOrgName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                //LOAD SITES BASED ON 'ORG. NAME' SELECTED
                int i = cmbOrgName.SelectedIndex;
                if (i >= 0)
                {
                    //UPDATE ORG. ID IN APP-SETTINGS

                }
            }
            catch (Exception ex)
            {
                string _msg = ex.Message;
            }
        }

        private void btnOrg_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _currentSelectedSite = -1;
                _currentSiteIndex = -1;

                HideDisableSITEButtons();

                Button _currButton = (sender as Button);
                string _currButtonName = _currButton.Name.ToUpper();
                _CompanyName = _currButton.Content.ToString().ToUpper();

                if (_currentSelectedOrg >= 1)
                {
                    switch (_currentSelectedOrg)
                    {
                        case 1:
                            btnOrg1.Background = _orgButtonBkColor;
                            break;
                        case 2:
                            btnOrg2.Background = _orgButtonBkColor;
                            break;
                        case 3:
                            btnOrg3.Background = _orgButtonBkColor;
                            break;
                        case 4:
                            btnOrg4.Background = _orgButtonBkColor;
                            break;
                        case 5:
                            btnOrg5.Background = _orgButtonBkColor;
                            break;
                        case 6:
                            btnOrg6.Background = _orgButtonBkColor;
                            break;
                        case 7:
                            btnOrg7.Background = _orgButtonBkColor;
                            break;
                        case 8:
                            btnOrg8.Background = _orgButtonBkColor;
                            break;
                        case 9:
                            btnOrg9.Background = _orgButtonBkColor;
                            break;
                        case 10:
                            btnOrg10.Background = _orgButtonBkColor;
                            break;
                        default:
                            break;
                    }
                }
                _currButton.Background = Brushes.BurlyWood;

                switch (_currButtonName)
                {
                    case "BTNORG1":
                        _currentOrgIndex = 0; //PAGEINDEX * 10 + 0;
                        _currentSelectedOrg = 1;

                        arrowToSite1.Visibility = Visibility.Visible;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG2":
                        _currentOrgIndex = 1;
                        _currentSelectedOrg = 2;

                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Visible;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG3":
                        _currentOrgIndex = 2;
                        _currentSelectedOrg = 3;

                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Visible;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG4":
                        _currentOrgIndex = 3;
                        _currentSelectedOrg = 4;

                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Visible;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG5":
                        _currentOrgIndex = 4;
                        _currentSelectedOrg = 5;

                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Visible;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG6":
                        _currentOrgIndex = 5;
                        _currentSelectedOrg = 6;

                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Visible;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG7":
                        _currentOrgIndex = 6;
                        _currentSelectedOrg = 7;

                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Visible;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG8":
                        _currentOrgIndex = 7;
                        _currentSelectedOrg = 8;

                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Visible;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG9":
                        _currentOrgIndex = 8;
                        _currentSelectedOrg = 9;

                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Visible;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG10":
                        _currentOrgIndex = 9;
                        _currentSelectedOrg = 10;

                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Visible;
                        break;
                    default:
                        _currentOrgIndex = -1;
                        _currentSelectedOrg = -1;
                        break;
                }

                if (_currentOrgIndex >= 0)
                {
                    _CompanyID = Convert.ToInt32(_dtOrgNamesG.DefaultView[_currentOrgIndex]["CompanyID"].ToString());
                    ConfigurationManager.AppSettings["LoggedInOrg"] = _CompanyName;
                    ConfigurationManager.AppSettings["LoggedInOrgID"] = _CompanyID.ToString();

                    if (_dtSiteNamesG.DefaultView.Count > 0)
                    {
                        _dtSiteNamesG.DefaultView.RowFilter = "CompanyID = " + _CompanyID;
                        if (_dtSiteNamesG.DefaultView.Count > 0)
                        {
                            tblockSiteNames.Visibility = Visibility.Visible;
                            for (int i = 0; i < _dtSiteNamesG.DefaultView.Count; i++)
                            {
                                if (i <= 9)
                                {
                                    switch (i)
                                    {
                                        case 0:
                                            btnSite1.IsEnabled = true;
                                            btnSite1.Visibility = Visibility.Visible;
                                            btnSite1.Content = _dtSiteNamesG.DefaultView[i]["SiteName"].ToString().ToUpper();
                                            break;
                                        case 1:
                                            btnSite2.IsEnabled = true;
                                            btnSite2.Visibility = Visibility.Visible;
                                            btnSite2.Content = _dtSiteNamesG.DefaultView[i]["SiteName"].ToString().ToUpper();
                                            break;
                                        case 2:
                                            btnSite3.IsEnabled = true;
                                            btnSite3.Visibility = Visibility.Visible;
                                            btnSite3.Content = _dtSiteNamesG.DefaultView[i]["SiteName"].ToString().ToUpper();
                                            break;
                                        case 3:
                                            btnSite4.IsEnabled = true;
                                            btnSite4.Visibility = Visibility.Visible;
                                            btnSite4.Content = _dtSiteNamesG.DefaultView[i]["SiteName"].ToString().ToUpper();
                                            break;
                                        case 4:
                                            btnSite5.IsEnabled = true;
                                            btnSite5.Visibility = Visibility.Visible;
                                            btnSite5.Content = _dtSiteNamesG.DefaultView[i]["SiteName"].ToString().ToUpper();
                                            break;
                                        case 5:
                                            btnSite6.IsEnabled = true;
                                            btnSite6.Visibility = Visibility.Visible;
                                            btnSite6.Content = _dtSiteNamesG.DefaultView[i]["SiteName"].ToString().ToUpper();
                                            break;
                                        case 6:
                                            btnSite7.IsEnabled = true;
                                            btnSite7.Visibility = Visibility.Visible;
                                            btnSite7.Content = _dtSiteNamesG.DefaultView[i]["SiteName"].ToString().ToUpper();
                                            break;
                                        case 7:
                                            btnSite8.IsEnabled = true;
                                            btnSite8.Visibility = Visibility.Visible;
                                            btnSite8.Content = _dtSiteNamesG.DefaultView[i]["SiteName"].ToString().ToUpper();
                                            break;
                                        case 8:
                                            btnSite9.IsEnabled = true;
                                            btnSite9.Visibility = Visibility.Visible;
                                            btnSite9.Content = _dtSiteNamesG.DefaultView[i]["SiteName"].ToString().ToUpper();
                                            break;
                                        case 9:
                                            btnSite10.IsEnabled = true;
                                            btnSite10.Visibility = Visibility.Visible;
                                            btnSite10.Content = _dtSiteNamesG.DefaultView[i]["SiteName"].ToString().ToUpper();
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }

        private void btnSite_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ActionPanelShow();

                Button _currButton = (sender as Button);
                string _currButtonName = _currButton.Name.ToUpper();
                _CompanyAndSiteName = _CompanyName + " (" + _currButton.Content.ToString().ToUpper() + ")";

                if (_currentSelectedSite >= 1)
                {
                    switch (_currentSelectedSite)
                    {
                        case 1:
                            btnSite1.Background = _siteButtonBkColor;
                            break;
                        case 2:
                            btnSite2.Background = _siteButtonBkColor;
                            break;
                        case 3:
                            btnSite3.Background = _siteButtonBkColor;
                            break;
                        case 4:
                            btnSite4.Background = _siteButtonBkColor;
                            break;
                        case 5:
                            btnSite5.Background = _siteButtonBkColor;
                            break;
                        case 6:
                            btnSite6.Background = _siteButtonBkColor;
                            break;
                        case 7:
                            btnSite7.Background = _siteButtonBkColor;
                            break;
                        case 8:
                            btnSite8.Background = _siteButtonBkColor;
                            break;
                        case 9:
                            btnSite9.Background = _siteButtonBkColor;
                            break;
                        case 10:
                            btnSite10.Background = _siteButtonBkColor;
                            break;
                        default:
                            break;
                    }
                }
                _currButton.Background = Brushes.BurlyWood;

                switch (_currButtonName)
                {
                    case "BTNSITE1":
                        _currentSiteIndex = 0; //PAGEINDEX * 10 + 0
                        _currentSelectedSite = 1;

                        subMenuToSite1.Visibility = Visibility.Visible;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE2":
                        _currentSiteIndex = 1;
                        _currentSelectedSite = 2;

                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Visible;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE3":
                        _currentSiteIndex = 2;
                        _currentSelectedSite = 3;

                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Visible;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE4":
                        _currentSiteIndex = 3;
                        _currentSelectedSite = 4;

                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Visible;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE5":
                        _currentSiteIndex = 4;
                        _currentSelectedSite = 5;

                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Visible;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE6":
                        _currentSiteIndex = 5;
                        _currentSelectedSite = 6;

                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Visible;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE7":
                        _currentSiteIndex = 6;
                        _currentSelectedSite = 7;

                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Visible;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE8":
                        _currentSiteIndex = 7;
                        _currentSelectedSite = 8;

                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Visible;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE9":
                        _currentSiteIndex = 8;
                        _currentSelectedSite = 9;

                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Visible;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE10":
                        _currentSiteIndex = 9;
                        _currentSelectedSite = 10;

                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Visible;
                        break;
                    default:
                        _currentSiteIndex = -1;
                        ActionPanelHide();

                        break;
                }

                if (_currentSiteIndex >= 0)
                {
                    SetCompanySiteDetailsAppSettings();
                    ActionPanelShow();
                }
                else
                {
                    _SiteID = 0;
                    ActionPanelHide();
                }
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }
        private void SetCompanySiteDetailsAppSettings()
        {
            _SiteID = Convert.ToInt32(_dtSiteNamesG.DefaultView[_currentSiteIndex]["SiteID"].ToString());

            ConfigurationManager.AppSettings["LoggedInOrgID"] = _CompanyID.ToString();
            ConfigurationManager.AppSettings["LoggedInSiteID"] = _SiteID.ToString();

            ConfigurationManager.AppSettings["AccessControl_FilePath"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["AccessControl_FilePath"].ToString();
            ConfigurationManager.AppSettings["AccessControl_FileName"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["AccessControl_FileName"].ToString();

            //EMAIL config from SiteMaster
            ConfigurationManager.AppSettings["SMTPHost"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["SMTPHost"].ToString();
            ConfigurationManager.AppSettings["SMTPPort"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["SMTPPort"].ToString();
            ConfigurationManager.AppSettings["SMTPUserName"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["SMTPUserName"].ToString();
            ConfigurationManager.AppSettings["SMTPEncryptPassword"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["SMTPEncryptPassword"].ToString();

            //Paychoice config from SiteMaster
            ConfigurationManager.AppSettings["PaychoiceSandboxURL"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["SandboxURL"].ToString();
            ConfigurationManager.AppSettings["PaychoiceSandboxUsername"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["SandboxUsername"].ToString();
            ConfigurationManager.AppSettings["PaychoiceSandboxPassword"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["SandboxPassword"].ToString();
            ConfigurationManager.AppSettings["PaychoiceSecuredURL"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["SecureURL"].ToString();
            ConfigurationManager.AppSettings["PaychoiceSecuredUsername"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["SecureUsername"].ToString();
            ConfigurationManager.AppSettings["PaychoiceSecuredPassword"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["SecurePassword"].ToString();
            ConfigurationManager.AppSettings["PaychoiceUsagePortal"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["ActivePortal"].ToString();

            ////Address details from SiteMaster - for POS
            ConfigurationManager.AppSettings["selectedSiteAddr1"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["Addr1"].ToString();
            ConfigurationManager.AppSettings["selectedSiteAddr2"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["Addr2"].ToString();
            ConfigurationManager.AppSettings["selectedSiteState"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["State"].ToString();

            ////POS details from SiteMaster
            ConfigurationManager.AppSettings["POSOpeningBalance"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["POS_OpeningBalance"].ToString();
            ConfigurationManager.AppSettings["POSReportEmail"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["POS_Report_Email"].ToString();
            ConfigurationManager.AppSettings["POSStockLevelAlertEmail"] = _dtSiteNamesG.DefaultView[_currentSiteIndex]["POS_Stock_Level_Alert_Email"].ToString();

            //GAS-19 TECHNOGYM PARAMETERS
            ConfigurationManager.AppSettings["test_technoGym_URL"] = _dtSiteNamesG.DefaultView[0]["test_technoGym_URL"].ToString();
            ConfigurationManager.AppSettings["test_technoGym_Facility"] = _dtSiteNamesG.DefaultView[0]["test_technoGym_Facility"].ToString();
            ConfigurationManager.AppSettings["test_technoGym_UniqueID"] = _dtSiteNamesG.DefaultView[0]["test_technoGym_UniqueID"].ToString();
            ConfigurationManager.AppSettings["test_technoGym_hdr_CLIENT"] = _dtSiteNamesG.DefaultView[0]["test_technoGym_hdr_CLIENT"].ToString();
            ConfigurationManager.AppSettings["test_technoGym_hdr_APIKEY"] = _dtSiteNamesG.DefaultView[0]["test_technoGym_hdr_APIKEY"].ToString();
            ConfigurationManager.AppSettings["test_technoGym_param_Username"] = _dtSiteNamesG.DefaultView[0]["test_technoGym_param_Username"].ToString();
            ConfigurationManager.AppSettings["test_technoGym_param_Password"] = _dtSiteNamesG.DefaultView[0]["test_technoGym_param_Password"].ToString();

            ConfigurationManager.AppSettings["PROD_technoGym_URL"] = _dtSiteNamesG.DefaultView[0]["PROD_technoGym_URL"].ToString();
            ConfigurationManager.AppSettings["PROD_technoGym_Facility"] = _dtSiteNamesG.DefaultView[0]["PROD_technoGym_Facility"].ToString();
            ConfigurationManager.AppSettings["PROD_technoGym_UniqueID"] = _dtSiteNamesG.DefaultView[0]["PROD_technoGym_UniqueID"].ToString();
            ConfigurationManager.AppSettings["PROD_technoGym_hdr_CLIENT"] = _dtSiteNamesG.DefaultView[0]["PROD_technoGym_hdr_CLIENT"].ToString();
            ConfigurationManager.AppSettings["PROD_technoGym_hdr_APIKEY"] = _dtSiteNamesG.DefaultView[0]["PROD_technoGym_hdr_APIKEY"].ToString();
            ConfigurationManager.AppSettings["PROD_technoGym_param_Username"] = _dtSiteNamesG.DefaultView[0]["PROD_technoGym_param_Username"].ToString();
            ConfigurationManager.AppSettings["PROD_technoGym_param_Password"] = _dtSiteNamesG.DefaultView[0]["PROD_technoGym_param_Password"].ToString();

            ConfigurationManager.AppSettings["TechnoGym_Test_OR_Prod"] = _dtSiteNamesG.DefaultView[0]["TechnoGym_Test_OR_Prod"].ToString().ToUpper();

            imggreenTick.Visibility = Visibility.Hidden;
            imgredTick.Visibility = Visibility.Hidden;
        }
        private void ActionPanelHide()
        {
            borderActions.Visibility = Visibility.Hidden;
            borderActions2.Visibility = Visibility.Hidden;
            tblockActions.Visibility = Visibility.Hidden;
            borderAllSite.Visibility = Visibility.Hidden;
            borderSelSite.Visibility = Visibility.Hidden;

            colActions.Width = new GridLength(51, GridUnitType.Star);
            colActionData.Width = new GridLength(20);
            borderActionData.Visibility = Visibility.Hidden;
            lvReportView.Visibility = Visibility.Hidden;
            lblActionDataNODATA.Visibility = Visibility.Hidden;

            imggreenTick.Visibility = Visibility.Hidden;
            imgredTick.Visibility = Visibility.Hidden;
        }
        private void ActionPanelShow()
        {
            tblockActions.Visibility = Visibility.Visible;
            borderActions.Visibility = Visibility.Visible;
            borderActions2.Visibility = Visibility.Visible;
            borderAllSite.Visibility = Visibility.Visible;
            borderSelSite.Visibility = Visibility.Visible;

            colActions.Width = new GridLength(51, GridUnitType.Star);
            colActionData.Width = new GridLength(20);
            borderActionData.Visibility = Visibility.Hidden;
            lvReportView.Visibility = Visibility.Hidden;
            lblActionDataNODATA.Visibility = Visibility.Hidden;
        }
        private void btnOrg_MouseEnter(object sender, MouseEventArgs e)
        {
            try
            {
                Button _currButton = (sender as Button);
                string _currButtonName = _currButton.Name.ToUpper();
                switch (_currButtonName)
                {
                    case "BTNORG1":
                        arrowToSite1.Visibility = Visibility.Visible;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG2":
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Visible;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG3":
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Visible;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG4":
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Visible;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG5":
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Visible;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG6":
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Visible;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG7":
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Visible;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG8":
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Visible;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG9":
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Visible;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNORG10":
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Visible;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }

        private void btnSite_MouseEnter(object sender, MouseEventArgs e)
        {
            try
            {
                Button _currButton = (sender as Button);
                string _currButtonName = _currButton.Name.ToUpper();

                switch (_currButtonName)
                {
                    case "BTNSITE1":
                        subMenuToSite1.Visibility = Visibility.Visible;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE2":
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Visible;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE3":
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Visible;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE4":
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Visible;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE5":
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Visible;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE6":
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Visible;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE7":
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Visible;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE8":
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Visible;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE9":
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Visible;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case "BTNSITE10":
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Visible;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }

        private void btnOrg_MouseLeave(object sender, MouseEventArgs e)
        {
            try
            {
                switch (_currentSelectedOrg)
                {
                    case 1:
                        arrowToSite1.Visibility = Visibility.Visible;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 2:
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Visible;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 3:
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Visible;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 4:
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Visible;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 5:
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Visible;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 6:
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Visible;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 7:
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Visible;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 8:
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Visible;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 9:
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Visible;
                        arrowToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 10:
                        arrowToSite1.Visibility = Visibility.Hidden;
                        arrowToSite2.Visibility = Visibility.Hidden;
                        arrowToSite3.Visibility = Visibility.Hidden;
                        arrowToSite4.Visibility = Visibility.Hidden;
                        arrowToSite5.Visibility = Visibility.Hidden;
                        arrowToSite6.Visibility = Visibility.Hidden;
                        arrowToSite7.Visibility = Visibility.Hidden;
                        arrowToSite8.Visibility = Visibility.Hidden;
                        arrowToSite9.Visibility = Visibility.Hidden;
                        arrowToSite10.Visibility = Visibility.Visible;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }

        private void btnDashboardFind_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FindMember fm = new FindMember();
                fm.Name = "FIND";
                fm.Owner = this;
                fm._dashBoard = null;
                fm.SetAccessRightsDS(null);

                fm.StaffImage.Source = this.StaffImage.ImageSource;
                fm._CurrentLoginDateTime = _CurrentLoginDateTime;
                fm._CompanyName = _CompanyAndSiteName;
                fm._UserName = _UserName;
                fm._appVersion = lblVersion.Content.ToString();
                this.Hide();

                fm.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnPOS_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClockInCheck _ClockInCheck = new ClockInCheck();
                _ClockInCheck = Comman.Constants.POS_CheckClockIn(_ClockInCheck);

                if (!_ClockInCheck._notClockedIn)
                {
                    if (_ClockInCheck._clockedInNotClockedOut)
                    {
                        this.Cursor = Cursors.Wait;
                        MakeaSale makeaSale = new MakeaSale();
                        makeaSale.Owner = this;
                        makeaSale.SetAccessRightsDS(null);
                        this.Cursor = Cursors.Arrow;
                        makeaSale._CurrentLoginDateTime = this._CurrentLoginDateTime;
                        makeaSale._CompanyName = _CompanyAndSiteName;
                        makeaSale._SiteName = this._SiteName;
                        makeaSale._UserName = this._UserName;
                        makeaSale._StaffPic = this._StaffPic;
                        makeaSale.StaffImage.Source = this.StaffImage.ImageSource;
                        makeaSale.ShowDialog();
                    }
                    else if (_ClockInCheck._clockedInAndClockedOut)
                    {
                        MessageBox.Show("End-Of-Shift has been completed for Today!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
                else
                {
                    decimal _clockInBalance = -1;
                    string _strClockInBalance = null;

                    _strClockInBalance = ConfigurationManager.AppSettings["POSOpeningBalance"].ToString();
                    if (_strClockInBalance == "")
                        _strClockInBalance = "0.00";

                    _clockInBalance = Convert.ToDecimal(_strClockInBalance);
                    if (_clockInBalance < 0)
                    {
                        MessageBox.Show("Amount CANNOT be less than Zero!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }

                    int _result = Comman.Constants.POS_ClockIn_User(_clockInBalance);

                    if (_result > 0)
                    {
                        this.Cursor = Cursors.Wait;
                        MakeaSale makeaSale = new MakeaSale();
                        makeaSale.Owner = this;
                        makeaSale.SetAccessRightsDS(null);
                        this.Cursor = Cursors.Arrow;
                        makeaSale._CurrentLoginDateTime = this._CurrentLoginDateTime;
                        makeaSale._CompanyName = _CompanyAndSiteName;
                        makeaSale._SiteName = this._SiteName;
                        makeaSale._UserName = this._UserName;
                        makeaSale._StaffPic = this._StaffPic;
                        makeaSale.StaffImage.Source = this.StaffImage.ImageSource;
                        makeaSale.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Error in Staff Clock-In!", this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnLastVisitors_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;
                Mouse.OverrideCursor = Cursors.Wait;
                LastVisitor lV = new LastVisitor();
                lV.Owner = this;
                lV.SetAccessRightsDS(null);

                lV._CurrentLoginDateTime = this._CurrentLoginDateTime;
                lV._CompanyName = _CompanyAndSiteName;
                lV._SiteName = this._SiteName;
                lV._UserName = this._UserName;
                lV._StaffPic = this._StaffPic;
                lV.StaffImage.Source = this.StaffImage.ImageSource;

                this.Cursor = Cursors.Arrow;
                Mouse.OverrideCursor = null;
                lV.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnBilling_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ConfigurationManager.AppSettings["LoggedInOrgID"].ToString() != "")
                {
                    if (ConfigurationManager.AppSettings["LoggedInSiteID"].ToString() != "")
                    {
                        Mouse.OverrideCursor = Cursors.Wait;
                        this.Cursor = Cursors.Wait;
                        PleaseWait _plsWait = new PleaseWait();

                        if (_fromRedButton)
                        {
                            _plsWait = new PleaseWait("Payment status Sync-Up & Access File Update...! Please Wait...");
                        }
                        _plsWait.Name = "PLEASEWAIT";
                        _plsWait.Owner = this;
                        _plsWait.Show();

                        Comman.Constants.disHonourCheck_Tick();

                        _plsWait.Close();
                        this.Cursor = Cursors.Arrow;
                        Mouse.OverrideCursor = null;

                        int _fileRet = Comman.Constants.Create_ACCESS_DAT_File(false, -2, "MULTIPLE");

                        if (_fileRet == 0)
                        {
                            imggreenTick.Visibility = Visibility.Hidden;
                            imgredTick.Visibility = Visibility.Visible;
                        }
                        else if (_fileRet == 1)
                        {
                            imggreenTick.Visibility = Visibility.Visible;
                            imgredTick.Visibility = Visibility.Hidden;

                            if (_fromRedButton)
                            {
                                _fromRedButton = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string _msg = ex.Message;

                imgredTick.Visibility = Visibility.Visible;
                imggreenTick.Visibility = Visibility.Hidden;
            }
        }

        private void btnReports_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;
                Report mainReport = new Report();

                mainReport._CompanyName = _CompanyAndSiteName;
                mainReport._UserName = this._UserName;
                mainReport._CurrentLoginDateTime = this._CurrentLoginDateTime;
                mainReport._StaffPic = this._StaffPic;
                mainReport.StaffImage.Source = StaffImage.ImageSource;

                mainReport.Owner = this;
                this.Cursor = Cursors.Arrow;
                mainReport.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
                {
                    this.Cursor = Cursors.Wait;
                    CompanyDetails _CompanySiteDetails = new CompanyDetails();
                    _CompanySiteDetails._CompanyName = _CompanyAndSiteName;
                    _CompanySiteDetails._UserName = this._UserName;
                    _CompanySiteDetails._CurrentLoginDateTime = this._CurrentLoginDateTime;
                    _CompanySiteDetails._StaffPic = this._StaffPic;
                    _CompanySiteDetails.StaffImage.Source = StaffImage.ImageSource;

                    _CompanySiteDetails.Owner = this;
                    this.Cursor = Cursors.Arrow;
                    _CompanySiteDetails._Login = this._Login;
                    _CompanySiteDetails.ShowDialog();
                }
                else
                {
                    MessageBox.Show("ACCESS DENIED! \n\nYou MUST be Admin user to access Settings!", "Dashboard", MessageBoxButton.OK, MessageBoxImage.Stop);
                    this.Cursor = Cursors.Arrow;
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private void btnSite_MouseLeave(object sender, MouseEventArgs e)
        {
            try
            {
                switch (_currentSelectedSite)
                {
                    case 1:
                        subMenuToSite1.Visibility = Visibility.Visible;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 2:
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Visible;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 3:
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Visible;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 4:
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Visible;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 5:
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Visible;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 6:
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Visible;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 7:
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Visible;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 8:
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Visible;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 9:
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Visible;
                        subMenuToSite10.Visibility = Visibility.Hidden;
                        break;
                    case 10:
                        subMenuToSite1.Visibility = Visibility.Hidden;
                        subMenuToSite2.Visibility = Visibility.Hidden;
                        subMenuToSite3.Visibility = Visibility.Hidden;
                        subMenuToSite4.Visibility = Visibility.Hidden;
                        subMenuToSite5.Visibility = Visibility.Hidden;
                        subMenuToSite6.Visibility = Visibility.Hidden;
                        subMenuToSite7.Visibility = Visibility.Hidden;
                        subMenuToSite8.Visibility = Visibility.Hidden;
                        subMenuToSite9.Visibility = Visibility.Hidden;
                        subMenuToSite10.Visibility = Visibility.Visible;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }

        private void btnPaymentSyncStatus_Click(object sender, RoutedEventArgs e)
        {
            colActions.Width = new GridLength(0);
            colActionData.Width = new GridLength(51, GridUnitType.Star);
            borderActionData.Visibility = Visibility.Visible;

            lvReportView.ItemsSource = null;
            lvReportView.Items.Clear();
            lvReportView.Visibility = Visibility.Hidden;
            lblActionDataNODATA.Visibility = Visibility.Visible;

            SqlHelper _Sql = new SqlHelper();
            if (_Sql._ConnOpen == 0) return;

            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", _CompanyID);

            lblActionDataHeader.Content = "Payment Status Sync Report";
            DataSet _dsReport = _Sql.ExecuteProcedure("SuperAdmin_PaymentSyncReport", ht);
            if (_dsReport != null)
            {
                if (_dsReport.Tables.Count > 0)
                {
                    if (_dsReport.Tables[0].DefaultView.Count > 0)
                    {
                        lvReportView.ItemsSource = _dsReport.Tables[0].DefaultView;
                        lvReportView.Visibility = Visibility.Visible;
                        lblActionDataNODATA.Visibility = Visibility.Hidden;
                    }
                }
            }
        }

        private void btnPOSClockInOut_Click(object sender, RoutedEventArgs e)
        {
            colActions.Width = new GridLength(0);
            colActionData.Width = new GridLength(51, GridUnitType.Star);
            borderActionData.Visibility = Visibility.Visible;

            lvReportView.ItemsSource = null;
            lvReportView.Items.Clear();
            lvReportView.Visibility = Visibility.Hidden;
            lblActionDataNODATA.Visibility = Visibility.Visible;

            SqlHelper _Sql = new SqlHelper();
            if (_Sql._ConnOpen == 0) return;

            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", _CompanyID);

            lblActionDataHeader.Content = "POS Clock In/Out Report";
            DataSet _dsReport = _Sql.ExecuteProcedure("SuperAdmin_POS_ClockInOut_Report", ht);
            if (_dsReport != null)
            {
                if (_dsReport.Tables.Count > 0)
                {
                    if (_dsReport.Tables[0].DefaultView.Count > 0)
                    {
                        lvReportView.ItemsSource = _dsReport.Tables[0].DefaultView;
                        lvReportView.Visibility = Visibility.Visible;
                        lblActionDataNODATA.Visibility = Visibility.Hidden;
                    }
                }
            }
        }

        private void imgredTick_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _fromRedButton = true;
            btnBilling_Click(sender, e);
        }

        private void btnLogReport_Click(object sender, RoutedEventArgs e)
        {
            colActions.Width = new GridLength(0);
            colActionData.Width = new GridLength(51, GridUnitType.Star);
            borderActionData.Visibility = Visibility.Visible;

            lvReportView.ItemsSource = null;
            lvReportView.Items.Clear();
            lvReportView.Visibility = Visibility.Hidden;
            lblActionDataNODATA.Visibility = Visibility.Visible;

            SqlHelper _Sql = new SqlHelper();
            if (_Sql._ConnOpen == 0) return;

            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", _CompanyID);

            lblActionDataHeader.Content = "Log Report";
            DataSet _dsReport = _Sql.ExecuteProcedure("SuperAdmin_Log_Report", ht);
            if (_dsReport != null)
            {
                if (_dsReport.Tables.Count > 0)
                {
                    if (_dsReport.Tables[0].DefaultView.Count > 0)
                    {
                        lvReportView.ItemsSource = _dsReport.Tables[0].DefaultView;
                        lvReportView.Visibility = Visibility.Visible;
                        lblActionDataNODATA.Visibility = Visibility.Hidden;
                    }
                }
            }
        }
    }
}
