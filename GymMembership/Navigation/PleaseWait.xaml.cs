﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GymMembership.Navigation
{
    /// <summary>
    /// Interaction logic for PleaseWait.xaml
    /// </summary>
    public partial class PleaseWait
    {
        public PleaseWait(string _lblCaption = "Payment Status Sync-Up is running!  Please Wait...")
        {
            InitializeComponent();
            lblDHMsg.Content = _lblCaption;
        }
        public bool _isDHStatusCheck = false;
        public bool _isOtherStatusCheck = false;
    }
}
