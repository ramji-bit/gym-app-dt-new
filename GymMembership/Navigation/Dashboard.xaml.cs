﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Data;
using System.Windows.Threading;
using System.ComponentModel;
using System.Windows.Input;
using Microsoft.VisualBasic;
using System.Configuration;
using System.IO;
using System.IO.Ports;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using System.Security;
using System.Threading.Tasks;
using System.Security.Principal;

using Microsoft.Win32;

using GymMembership.Accounts;
using GymMembership.MemberInfo;
using GymMembership.BookingInfo;
using GymMembership.Task;
using GymMembership.Pointofsale;
using GymMembership.LastVisitors;
using GymMembership.Communications;
using GymMembership.Settings;
using GymMembership.Reports;
using GymMembership.Prospects;
using GymMembership.DAL;
using GymMembership.BAL;
using Paychoice_Payment;

namespace GymMembership.Navigation
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public class Img
    {
        public Img(string value, Image img) { Str = value; Image = img; }
        public string Str { get; set; }
        public Image Image { get; set; }
    }

    public partial class Dashboard
    {
        ClockInCheck _ClockInCheck = new ClockInCheck();
        public int _maxDHDeferCounter = 1;

        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _CompanyAndSiteName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public Login _Login;

        DispatcherTimer dispatcherTimer = new DispatcherTimer();
        DispatcherTimer disHonourCheck = new DispatcherTimer(DispatcherPriority.Background);
        DispatcherTimer expireCheckOnLogin;
        DispatcherTimer expireTaskAlert;
        DispatcherTimer multiSiteAccessFile;
        DispatcherTimer _accessControlCOMPort;
        DispatcherTimer _swipeInPopup;
        //CheckInPopUp _chkPopUp = new CheckInPopUp();

        //COM PORT READER - ACCESS CONTROL
        string _DataCardNumber_READEXISTING = "";
        //COM PORT READER - ACCESS CONTROL

        TimeSpan _DH_TimeSpan = new TimeSpan(0, 0, 0, 1);

        public bool _isSessionNeverExpire = false;
        public bool _isDishonourCheckEnabled = true;

        private int _percent;
        private int _ModuleTable = 0;
        private int _SubModuleTable = 1;

        Hashtable ht = new Hashtable();
        DataSet _dsUserRights = new DataSet();
        DataSet _dsSites = new DataSet();
        DataSet _dsDoors = new DataSet();
        string _DoorToOpen = "";
        public int _checkInProgrammeID = 0;
        //public DataSet _disconnected_DataSet;

        DispatcherTimer mIdle;
        private const long cIdleSeconds = 900;
        //private const long cIdleSeconds = 30;

        bool _IsIdle = false;
        bool _FindMemberAccessRights = false;
        bool _ExpireCheckOnLogin = false;
        bool _ExpireTaskAlert = false;
        bool _multiSiteAccessFile = false; bool _showPlsWaitmultiFile = false;

        bool _DH_FirstRun_Complete = false; bool _DH_SecondRun_Complete = false;
        bool _DH_FirstRun_Cpy_Complete = false; bool _DH_SecondRun_Cpy_Complete = false;

        bool _fromRedButton = false;

        int _CompanyID;
        int _LoginID;
        int _SiteID;
        int _MemberDetailsDataTable = 0;
        DataSet _ds;

        //SERIAL PORT / ACCESS CARD READER RELATED
        SerialPort _serialPort = new SerialPort();
        List<SerialPort> _serialPortsList = new List<SerialPort>();
        string[] _portNames = SerialPort.GetPortNames();
        bool _isPopupShown = false;
        int nOrder = 1;
        public Dashboard()
        {
            try
            {
                InitializeComponent();
                this.Closing += new CancelEventHandler(DashBoard_Closing);

                SystemEvents.SessionSwitch += new SessionSwitchEventHandler(SystemEvents_SessionSwitch);

                InputManager.Current.PreProcessInput += Idle_PreProcessInput;
                mIdle = new DispatcherTimer();
                mIdle.Interval = new TimeSpan(cIdleSeconds * 1000 * 10000);
                mIdle.IsEnabled = true;
                mIdle.Tick += Idle_Tick;

                if (!Comman.Constants._SuperAdmin)
                {
                    _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    _LoginID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedUserLoginID"].ToString());
                    _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                    SettingsDAL _SettingsDal = new SettingsDAL();
                    _dsUserRights = _SettingsDal.GetAccessRightForUser(_CompanyID, _SiteID, _LoginID);

                    SetDashBoard_ACCESS_RIGHTS();

                    grdFindMember.ItemsSource = null;
                    grdFindMember.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        ~Dashboard()
        {
            SystemEvents.SessionSwitch -= new SessionSwitchEventHandler(SystemEvents_SessionSwitch);
            //if (_serialPort.IsOpen) _serialPort.Close();
        }

        void SystemEvents_SessionSwitch(object sender, SessionSwitchEventArgs e)
        {
            switch (e.Reason)
            {
                case SessionSwitchReason.SessionUnlock:
                    break;

                case SessionSwitchReason.SessionLock:
                    _IsIdle = true;
                    this.Close();
                    break;
            }
        }
        private void LoadMemberListFromDBOnLoad()
        {
            _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return;

            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            _ds = new DataSet();
            _ds = sql.ExecuteProcedure("GetMemberDetailsFindMemberDashboard", ht);
        }
        void Idle_Tick(object sender, EventArgs e)
        {
            if (!_isSessionNeverExpire)
            {
                _IsIdle = true;
                this.Close();
            }
        }
        void Idle_PreProcessInput(object sender, PreProcessInputEventArgs e)
        {
            mIdle.IsEnabled = false;
            mIdle.IsEnabled = true;
        }
        void DashBoard_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                if (!_IsIdle)
                {
                    if (!Comman.Constants._emailBeingSent)
                    {
                        if (MessageBox.Show("Are you sure to LOGOFF ?", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            if (!Comman.Constants._SuperAdmin)
                            {
                                Comman.Constants._SuperAdmin = false;
                                if (!Comman.Constants._OfflineFlag)
                                {
                                    Comman.Constants._DisConnectedDataAdapter.Update(Comman.Constants._DisConnectedALLTables);
                                }
                                Comman.Constants._OfflineFlag = true;
                                dispatcherTimer.Stop();
                                disHonourCheck.Stop();
                                multiSiteAccessFile.Stop();
                                _accessControlCOMPort.Stop();

                                //if (_serialPort.IsOpen) _serialPort.Close();

                                _Login.cmbOrgName.Text = _CompanyName.ToUpper();
                                _Login.txtPassword.Password = "";
                                _Login.txtUserName.Text = "";
                                _Login.ShowInTaskbar = true;
                                _Login.multiSiteAccessFile.Start();
                                _Login.Show();

                                _Login.txtUserName.Focus();
                            }
                            else
                            {
                                Comman.Constants._SuperAdmin = false;
                                _Login.txtPassword.Password = "";
                                _Login.txtUserName.Text = "";
                                _Login.ShowInTaskbar = true;
                                _Login.Show();
                            }
                        }
                        else
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                    else
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                else
                {
                    dispatcherTimer.Stop();
                    disHonourCheck.Stop();
                    multiSiteAccessFile.Stop();
                    _accessControlCOMPort.Stop();

                    _Login.cmbOrgName.Text = _CompanyName.ToUpper();
                    _Login.txtPassword.Password = "";
                    _Login.txtUserName.Text = "";
                    _Login.ShowInTaskbar = true;
                    _Login.multiSiteAccessFile.Start();
                    _Login.Show();
                    _Login.txtUserName.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;
                MemberDetails Member = new MemberDetails();
                Member.Owner = this;
                Member.MemberDetails_AccessRights(_dsUserRights);
                this.Cursor = Cursors.Arrow;
                Member._CurrentLoginDateTime = this._CurrentLoginDateTime;
                Member._CompanyName = _CompanyAndSiteName;
                Member._SiteName = this._SiteName;
                Member._UserName = this._UserName;
                Member._StaffPic = this._StaffPic;
                //Member.StaffImage.Source = this.StaffImage.Source;
                Member.StaffImage.Source = this.StaffImage.ImageSource;
                Member._appVersion = lblVersion.Content.ToString();
                Member.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!Comman.Constants._SuperAdmin)
                {
                    /////////////////////// ********** UNCOMMENT, WHILE IMPLEMENTING 'STAFF ROSTER' 26/JUN/2017 ******************///////////
                    btnStaffRoster.Visibility = Visibility.Visible;
                    /////////////////////// ********** UNCOMMENT, WHILE IMPLEMENTING 'STAFF ROSTER' 26/JUN/2017 ******************///////////

                    this.Cursor = Cursors.Wait;

                    imggreenTick.Visibility = Visibility.Hidden;
                    imgredTick.Visibility = Visibility.Hidden;
                    rwTickButton.Height = new GridLength(0);

                    this.Title = "DASHBOARD";

                    lblOrgName.Content = _CompanyName.ToString().Replace("_", "__");
                    lblUserName.Content = _UserName.ToString().Replace("_", "__");
                    lblLoggedInTime.Content = _CurrentLoginDateTime;
                    loadStaffPhoto();

                    if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
                    {
                        cmbSite.IsEnabled = true;
                        LoadSites();
                        cmbSite.Visibility = Visibility.Visible;

                        cmbDoors.IsEnabled = true;
                        LoadDoors();
                        cmbDoors.Visibility = Visibility.Visible;
                        btnDoorOpen.IsEnabled = true;
                        btnDoorOpen.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        cmbSite.IsEnabled = true;
                        LoadSites();
                        cmbSite.Visibility = Visibility.Visible;

                        cmbDoors.IsEnabled = false;
                        LoadDoors();
                        cmbDoors.Visibility = Visibility.Hidden;
                        btnDoorOpen.IsEnabled = false;
                        btnDoorOpen.Visibility = Visibility.Hidden;
                    }
                    Comman.Constants._appVersion = lblVersion.Content.ToString();
                    this.Cursor = Cursors.Wait;
                    LoadMemberListFromDBOnLoad();
                    GetRecentCheckInDetails();


                    ////CHECK-IN POPUP *** DO NOT REMOVE THE COMMENTED LINES *** ////
                    //dispatcherTimer = new DispatcherTimer();
                    //dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
                    //dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 7);
                    //dispatcherTimer.Start();
                    ////CHECK-IN POPUP *** DO NOT REMOVE THE COMMENTED LINES *** ////

                    expireCheckOnLogin = new DispatcherTimer(DispatcherPriority.ApplicationIdle);
                    expireCheckOnLogin.Tick += new EventHandler(expireCheckOnLogin_Tick);
                    expireCheckOnLogin.Interval = new TimeSpan(0, 0, 30, 0);
                    expireCheckOnLogin.Start();

                    expireTaskAlert = new DispatcherTimer(DispatcherPriority.Background);
                    expireTaskAlert.Tick += new EventHandler(expireTaskAlert_Tick);
                    expireTaskAlert.Interval = new TimeSpan(0, 0, 25, 0);
                    expireTaskAlert.Start();

                    multiSiteAccessFile = new DispatcherTimer(DispatcherPriority.Background);
                    multiSiteAccessFile.Tick += new EventHandler(multiSiteAccessFile_Tick);
                    multiSiteAccessFile.Interval = new TimeSpan(0, 0, 10, 0);
                    multiSiteAccessFile.Start();

                    disHonourCheck = new DispatcherTimer(DispatcherPriority.Background);
                    disHonourCheck.Tick += new EventHandler(disHonourCheck_Tick);
                    disHonourCheck.Interval = _DH_TimeSpan;
                    disHonourCheck.Start();

                    _accessControlCOMPort = new DispatcherTimer(DispatcherPriority.Input);
                    _accessControlCOMPort.Tick += new EventHandler(accessControl_ReadCOMPort);
                    _accessControlCOMPort.Interval = new TimeSpan(0, 0, 0, 1);
                    _accessControlCOMPort.Start();

                    //ReadCOMPort();
                    this.Cursor = Cursors.Arrow;

                    //string _userName = WindowsIdentity.GetCurrent().Name;
                    //MessageBox.Show("User Name : " + _userName);
                }
                else
                {
                    lblUserName.Content = "SUPER ADMIN";
                    lblLoggedInTime.Content = _CurrentLoginDateTime;

                    lblOrgName.Visibility = Visibility.Hidden;

                    row2Dashboard.Height = new GridLength(0);
                    row3Dashboard.Height = new GridLength(0);

                    row2Dashboard.IsEnabled = false;
                    row3Dashboard.IsEnabled = false;
                    rwTickButton.IsEnabled = false;

                    borderSAdminPanel.Visibility = Visibility.Visible;
                    gridSAdminPanel.Visibility = Visibility.Visible;
                    borderSAdminPanel.BorderBrush = Brushes.Red;
                    borderSAdminPanel.BorderThickness = new Thickness(1);

                    canGraph1.Visibility = Visibility.Hidden;
                    canGraph2.Visibility = Visibility.Hidden;
                    canGraph3.Visibility = Visibility.Hidden;
                    canGraph4.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                this.Cursor = Cursors.Arrow;
                return;
            }
        }
        private void GetRecentCheckInDetails()
        {
            try
            {
                if (ConfigurationManager.AppSettings["LoggedInOrgID"].ToString() != "")
                {
                    lstMemberCheckIn.Items.Clear();

                    SqlHelper _Sql = new SqlHelper();
                    if (_Sql._ConnOpen == 0) return;

                    int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                    DateTime _currDate = DateTime.Now;
                    Hashtable ht = new Hashtable();

                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@SiteID", _SiteID);
                    ht.Add("@currDateTime", _currDate);

                    DataSet _dsRecentCheckIn = _Sql.ExecuteProcedure("GetRecentCheckInDetails", ht);
                    if (_dsRecentCheckIn != null)
                    {
                        if (_dsRecentCheckIn.Tables.Count > 0)
                        {
                            if (_dsRecentCheckIn.Tables[0].DefaultView.Count > 0)
                            {
                                string _recentCheckIn = "";
                                for (int i = 0; i < _dsRecentCheckIn.Tables[0].DefaultView.Count; i++)
                                {
                                    _recentCheckIn = _dsRecentCheckIn.Tables[0].DefaultView[i]["MemberName"].ToString().ToUpper()
                                            + ", " + _dsRecentCheckIn.Tables[0].DefaultView[i]["AccessTypeName"].ToString().ToUpper()
                                            + ", " + _dsRecentCheckIn.Tables[0].DefaultView[i]["CheckInTime"].ToString().ToUpper();

                                    lstMemberCheckIn.Items.Add(_recentCheckIn);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string _msg = ex.Message;
            }
        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (Comman.Constants._OfflineFlag)
            {
                if (ConfigurationManager.AppSettings["LoggedInOrgID"].ToString() != "")
                {
                    Comman.Constants.Popup_dispatcherTimer_Tick();
                }
            }
        }
        private void accessControl_ReadCOMPort(object sender, EventArgs e)
        {
            //ReadCOMPort();
            _portNames = SerialPort.GetPortNames();
            if (_portNames.Length > 0)
            {
                for (int i = 0; i < _portNames.Length; i++)
                {
                    ReadCOMPort(_portNames[i]);
                }
            }
            _accessControlCOMPort.Interval = new TimeSpan(0, 0, 0, 1);
        }
        private void disHonourCheck_Tick(object sender, EventArgs e)
        {
            try
            {
                ////Refresh Payment Status Every 1 hour; & Change Access Type (DH) if Payment is 'Dishonoured'

                //_isDishonourCheckEnabled = false;
                //for (int i = 0; i < Application.Current.Windows.Count; i++)
                //{
                //    if ((Application.Current.Windows[i].Name.ToUpper() == "DASHBOARD") && (Application.Current.Windows[i].IsActive))
                //    {
                //        _isDishonourCheckEnabled = true;
                //        break;
                //    }
                //}

                if (_isDishonourCheckEnabled)
                {
                    if (Comman.Constants._OfflineFlag)
                    {
                        if (ConfigurationManager.AppSettings["LoggedInOrgID"].ToString() != "")
                        {
                            if (ConfigurationManager.AppSettings["LoggedInSiteID"].ToString() != "")
                            {
                                //CHECK IF FIRSTRUN IS ALREADY COMPLETED
                                _DH_FirstRun_Cpy_Complete = Comman.Constants.DH_Status_Check_FirstRun_Company();
                                //_DH_FirstRun_Complete = Comman.Constants.DH_Status_Check_FirstRun();

                                if (!_DH_FirstRun_Cpy_Complete)
                                {
                                    int _currSiteIndex = cmbSite.SelectedIndex;

                                    Mouse.OverrideCursor = Cursors.Wait;
                                    this.Cursor = Cursors.Wait;
                                    PleaseWait _plsWait = new PleaseWait("Payment Sync (1st Run) in progress...! Please Wait...");
                                    _plsWait.Name = "PLEASEWAIT";
                                    _plsWait.Owner = this;
                                    _plsWait.Show();

                                    multiSiteAccessFile.Stop();

                                    for (int _siteIdx = 0; _siteIdx < cmbSite.Items.Count; _siteIdx++)
                                    {
                                        cmbSite.SelectedIndex = _siteIdx;
                                        Comman.Constants.expireCheck_Tick();
                                        Comman.Constants.disHonourCheck_Tick();
                                        disHonourCheck.Interval = new TimeSpan(0, 3, 0, 0);
                                    }
                                    Comman.Constants._dishonourConnMsg = false;
                                    
                                    cmbSite.SelectedIndex = _currSiteIndex;
                                    int _fileRet = Comman.Constants.Create_ACCESS_DAT_File(false, -2, "MULTIPLE");

                                    _plsWait.Close();
                                    this.Cursor = Cursors.Arrow;
                                    _maxDHDeferCounter = 1;
                                    Mouse.OverrideCursor = null;

                                    if (_fileRet == 0)
                                    {
                                        imggreenTick.Visibility = Visibility.Hidden;
                                        imgredTick.Visibility = Visibility.Visible;
                                    }
                                    else if (_fileRet == 1)
                                    {
                                        imggreenTick.Visibility = Visibility.Visible;
                                        imgredTick.Visibility = Visibility.Hidden;

                                        _fromRedButton = false;

                                        Comman.Constants.DH_StatusCheck_FirstRun_Update();

                                        _DH_FirstRun_Complete = true;
                                    }
                                    else
                                    {
                                        imggreenTick.Visibility = Visibility.Visible;
                                        imgredTick.Visibility = Visibility.Hidden;

                                        _fromRedButton = false;

                                        Comman.Constants.DH_StatusCheck_FirstRun_Update();

                                        _DH_FirstRun_Complete = true;
                                    }

                                    multiSiteAccessFile.Start();
                                }
                                else //IF FIRST RUN_CPY COMPLETE
                                {
                                    _DH_FirstRun_Complete = Comman.Constants.DH_Status_Check_FirstRun();
                                    if (!_DH_FirstRun_Complete)
                                    {
                                        Mouse.OverrideCursor = Cursors.Wait;
                                        PleaseWait _plsWait = new PleaseWait("Payment Sync (1st Run) DAT file in progress...! Please Wait...");
                                        _plsWait.Name = "PLEASEWAIT";
                                        _plsWait.Owner = this;
                                        _plsWait.Show();

                                        multiSiteAccessFile.Stop();
                                        int _fileRet = Comman.Constants.Create_ACCESS_DAT_File(false, -2, "MULTIPLE");

                                        if (_fileRet == 0)
                                        {
                                            imggreenTick.Visibility = Visibility.Hidden;
                                            imgredTick.Visibility = Visibility.Visible;
                                        }
                                        else if (_fileRet == 1)
                                        {
                                            imggreenTick.Visibility = Visibility.Visible;
                                            imgredTick.Visibility = Visibility.Hidden;

                                            _fromRedButton = false;

                                            Comman.Constants.DH_StatusCheck_FirstRun_Update();

                                            _DH_FirstRun_Complete = true;
                                        }
                                        else
                                        {
                                            imggreenTick.Visibility = Visibility.Visible;
                                            imgredTick.Visibility = Visibility.Hidden;

                                            _fromRedButton = false;

                                            Comman.Constants.DH_StatusCheck_FirstRun_Update();

                                            _DH_FirstRun_Complete = true;
                                        }

                                        _plsWait.Close();
                                        Mouse.OverrideCursor = null;
                                        disHonourCheck.Interval = new TimeSpan(0, 3, 0, 0);

                                        multiSiteAccessFile.Start();
                                    }
                                    else
                                    {
                                        imggreenTick.Visibility = Visibility.Visible;
                                        imgredTick.Visibility = Visibility.Hidden;
                                        disHonourCheck.Interval = new TimeSpan(0, 3, 0, 0);

                                        //_DH_SecondRun_Complete = Comman.Constants.DH_Status_Check_SecondRun();
                                        _DH_SecondRun_Cpy_Complete = Comman.Constants.DH_Status_Check_SecondRun_Company();

                                        if (!_DH_SecondRun_Cpy_Complete)
                                        {
                                            DateTime _DH_NextRun = Comman.Constants._DH_FirstRun_DateTime.AddHours(7);
                                            if ((_DH_NextRun <= DateTime.Now) && (Comman.Constants._DH_FirstRun_DateTime.Date == DateTime.Now.Date))
                                            {
                                                imggreenTick.Visibility = Visibility.Hidden;
                                                imgredTick.Visibility = Visibility.Hidden;

                                                int _currSiteIndex = cmbSite.SelectedIndex;
                                                Mouse.OverrideCursor = Cursors.Wait;
                                                this.Cursor = Cursors.Wait;

                                                PleaseWait _plsWait = new PleaseWait("Payment Sync (2nd Run) in progress...! Please Wait...");
                                                _plsWait.Name = "PLEASEWAIT";
                                                _plsWait.Owner = this;
                                                _plsWait.Show();

                                                multiSiteAccessFile.Stop();

                                                for (int _siteIdx = 0; _siteIdx < cmbSite.Items.Count; _siteIdx++)
                                                {
                                                    cmbSite.SelectedIndex = _siteIdx;
                                                    Comman.Constants.expireCheck_Tick();
                                                    Comman.Constants.disHonourCheck_Tick();
                                                    disHonourCheck.Interval = new TimeSpan(0, 3, 0, 0);
                                                }
                                                Comman.Constants._dishonourConnMsg = false;
                                                
                                                cmbSite.SelectedIndex = _currSiteIndex;
                                                int _fileRet = Comman.Constants.Create_ACCESS_DAT_File(false, -2, "MULTIPLE");

                                                _plsWait.Close();
                                                this.Cursor = Cursors.Arrow;
                                                _maxDHDeferCounter = 1;
                                                Mouse.OverrideCursor = null;

                                                if (_fileRet == 0)
                                                {
                                                    imggreenTick.Visibility = Visibility.Hidden;
                                                    imgredTick.Visibility = Visibility.Visible;

                                                    _DH_FirstRun_Complete = true;
                                                    _DH_SecondRun_Complete = false;
                                                }
                                                else if (_fileRet == 1)
                                                {
                                                    imggreenTick.Visibility = Visibility.Visible;
                                                    imgredTick.Visibility = Visibility.Hidden;

                                                    _fromRedButton = false;

                                                    Comman.Constants.DH_StatusCheck_SecondRun_Update();

                                                    _DH_FirstRun_Complete = true;
                                                    _DH_SecondRun_Complete = true;
                                                }
                                                else
                                                {
                                                    imggreenTick.Visibility = Visibility.Visible;
                                                    imgredTick.Visibility = Visibility.Hidden;

                                                    _fromRedButton = false;

                                                    Comman.Constants.DH_StatusCheck_SecondRun_Update();

                                                    _DH_FirstRun_Complete = true;
                                                    _DH_SecondRun_Complete = true;
                                                }

                                                multiSiteAccessFile.Start();
                                            }
                                        }
                                        else //IF ! DH SECOND RUN-CPY
                                        {
                                            //RUN DAT FILE ONLY
                                            DateTime _DH_NextRun = Comman.Constants._DH_FirstRun_DateTime.AddHours(7);
                                            if ((_DH_NextRun <= DateTime.Now) && (Comman.Constants._DH_FirstRun_DateTime.Date == DateTime.Now.Date))
                                            {
                                                imggreenTick.Visibility = Visibility.Hidden;
                                                imgredTick.Visibility = Visibility.Hidden;

                                                Mouse.OverrideCursor = Cursors.Wait;

                                                PleaseWait _plsWait = new PleaseWait("Payment Sync (2nd Run) DAT file in progress...! Please Wait...");
                                                _plsWait.Name = "PLEASEWAIT";
                                                _plsWait.Owner = this;
                                                _plsWait.Show();

                                                multiSiteAccessFile.Stop();

                                                int _fileRet = Comman.Constants.Create_ACCESS_DAT_File(false, -2, "MULTIPLE");

                                                _plsWait.Close();
                                                this.Cursor = Cursors.Arrow;
                                                _maxDHDeferCounter = 1;
                                                Mouse.OverrideCursor = null;

                                                if (_fileRet == 0)
                                                {
                                                    imggreenTick.Visibility = Visibility.Hidden;
                                                    imgredTick.Visibility = Visibility.Visible;

                                                    _DH_FirstRun_Complete = true;
                                                    _DH_SecondRun_Complete = false;
                                                }
                                                else if (_fileRet == 1)
                                                {
                                                    imggreenTick.Visibility = Visibility.Visible;
                                                    imgredTick.Visibility = Visibility.Hidden;

                                                    _fromRedButton = false;

                                                    Comman.Constants.DH_StatusCheck_SecondRun_Update();

                                                    _DH_FirstRun_Complete = true;
                                                    _DH_SecondRun_Complete = true;
                                                }
                                                else
                                                {
                                                    imggreenTick.Visibility = Visibility.Visible;
                                                    imgredTick.Visibility = Visibility.Hidden;

                                                    _fromRedButton = false;

                                                    Comman.Constants.DH_StatusCheck_SecondRun_Update();

                                                    _DH_FirstRun_Complete = true;
                                                    _DH_SecondRun_Complete = true;
                                                }

                                                multiSiteAccessFile.Start();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                _accessControlCOMPort.Start();
            }
            catch (Exception ex)
            {
                //rwTickButton.Height = new GridLength(25, GridUnitType.Star);

                imggreenTick.Visibility = Visibility.Hidden;
                imgredTick.Visibility = Visibility.Visible;
                //tbAccessMsg.Visibility = Visibility.Visible;
                //tbAccessMsg.Text = "Access File Failed !";
                //tbAccessMsg.Foreground = System.Windows.Media.Brushes.Red;

                string _msg = ex.Message;
                multiSiteAccessFile.Start();
                _accessControlCOMPort.Start();
            }
        }
        private void expireCheckOnLogin_Tick(object sender, EventArgs e)
        {
            _ExpireCheckOnLogin = true;
            for (int i = 0; i < Application.Current.Windows.Count; i++)
            {
                if ((Application.Current.Windows[i].Name.ToUpper() == "DASHBOARD") && (Application.Current.Windows[i].IsActive))
                {
                    _ExpireCheckOnLogin = false;
                    break;
                }
            }
            if (_ExpireCheckOnLogin) return;

            if (!_ExpireCheckOnLogin)
            {
                if (ConfigurationManager.AppSettings["LoggedInOrgID"].ToString() != "")
                {
                    if (ConfigurationManager.AppSettings["LoggedInSiteID"].ToString() != "")
                    {
                        _ExpireCheckOnLogin = true;
                        Comman.Constants.expireCheck_Tick();
                        expireCheckOnLogin.Stop();
                    }
                }
            }
        }
        private void expireTaskAlert_Tick(object sender, EventArgs e)
        {
            _ExpireTaskAlert = true;
            for (int i = 0; i < Application.Current.Windows.Count; i++)
            {
                if ((Application.Current.Windows[i].Name.ToUpper() == "DASHBOARD") && (Application.Current.Windows[i].IsActive))
                {
                    _ExpireTaskAlert = false;
                    break;
                }
            }
            if (_ExpireTaskAlert) return;

            if (!_ExpireTaskAlert)
            {
                if (ConfigurationManager.AppSettings["LoggedInOrgID"].ToString() != "")
                {
                    if (ConfigurationManager.AppSettings["LoggedInSiteID"].ToString() != "")
                    {
                        _ExpireTaskAlert = true;
                        Comman.Constants.expireTaskAlert_Tick();
                        expireTaskAlert.Stop();
                    }
                }
            }
        }
        private void multiSiteAccessFile_Tick(object sender, EventArgs e)
        {
            //_multiSiteAccessFile = true;
            //_showPlsWaitmultiFile = false;
            //for (int i = 0; i < Application.Current.Windows.Count; i++)
            //{
            //    if ((Application.Current.Windows[i].Name.ToUpper() == "DASHBOARD") && (Application.Current.Windows[i].IsActive))
            //    {
            //        _multiSiteAccessFile = false;
            //        _showPlsWaitmultiFile = true;
            //        break;
            //    }
            //}

            if (!_multiSiteAccessFile)
            {
                if (ConfigurationManager.AppSettings["LoggedInOrgID"].ToString() != "")
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                    this.Cursor = Cursors.Wait;

                    if (_showPlsWaitmultiFile)
                    {
                        PleaseWait _plsWait = new PleaseWait("Access Control Files Update... Please Wait!");
                        _plsWait.Name = "PLEASEWAIT";
                        _plsWait.Owner = this;
                        _plsWait.Show();

                        Comman.Constants.multiSiteAccessFile_Tick();
                        multiSiteAccessFile.Interval = new TimeSpan(0, 2, 0, 0);
                        //multiSiteAccessFile.Interval = new TimeSpan(0, 0, 2, 0);

                        _plsWait.Close();
                    }
                    else
                    {
                        Comman.Constants.multiSiteAccessFile_Tick();
                        multiSiteAccessFile.Interval = new TimeSpan(0, 2, 0, 0);
                        //multiSiteAccessFile.Interval = new TimeSpan(0, 0, 2, 0);
                    }

                    this.Cursor = Cursors.Arrow;
                    Mouse.OverrideCursor = null;
                }
            }
        }
        private void Window_UnLoaded(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void btnDashboardBooking_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;
                Mouse.OverrideCursor = Cursors.Wait;
                MemberBooking memberBooking = new MemberBooking();
                memberBooking.Owner = this;
                //memberBooking.StaffImage.Source = StaffImage.Source;
                memberBooking.StaffImage.Source = StaffImage.ImageSource;
                memberBooking._StaffPic = _StaffPic;
                memberBooking._UserName = _UserName;
                memberBooking._CurrentLoginDateTime = _CurrentLoginDateTime;
                memberBooking._CompanyName = _CompanyAndSiteName;
                memberBooking._dSAccessRights = _dsUserRights;

                this.Cursor = Cursors.Arrow;
                Mouse.OverrideCursor = null;
                memberBooking.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnDashboardFind_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;
                FindMember fm = new FindMember();
                fm.Name = "FIND";
                fm.Owner = this;
                fm._dashBoard = this;
                fm.SetAccessRightsDS(_dsUserRights);
                this.Cursor = Cursors.Arrow;
                //fm.StaffImage.Source = this.StaffImage.Source;
                fm.StaffImage.Source = this.StaffImage.ImageSource;
                fm._StaffPic = _StaffPic;
                fm._UserName = _UserName;
                fm._CurrentLoginDateTime = _CurrentLoginDateTime;
                fm._CompanyName = _CompanyAndSiteName;
                fm._appVersion = lblVersion.Content.ToString();
                this.Hide();
                fm.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnDashboardQuit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
                //Closing event called
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }
        private void btnPOS_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClockInCheck _ClockInCheck = new ClockInCheck();
                _ClockInCheck = Comman.Constants.POS_CheckClockIn(_ClockInCheck);

                if (!_ClockInCheck._notClockedIn)
                {
                    if (_ClockInCheck._clockedInNotClockedOut)
                    {
                        //GAS - 27 POS Home Club : 19/SEP/2017
                        if (ConfigurationManager.AppSettings["SuperAdmin"].ToString().ToUpper().Trim() != "YES")
                        {
                            if
                            (
                                ConfigurationManager.AppSettings["LoggedInSiteID"].ToString().ToUpper().Trim() !=
                                ConfigurationManager.AppSettings["HomeClubSiteID"].ToString().ToUpper().Trim()
                            )
                            {
                                MessageBoxResult _HomeClubResult =
                                MessageBox.Show
                                    (
                                        "Your selected site is different to the Home site!" +
                                        "\nWould you like to still continue with POS transaction - Yes or No?" +
                                        "\n\n(CLICK 'YES' TO PROCEED, 'NO' TO EXIT!)",
                                        this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No
                                    );
                                if (
                                        _HomeClubResult == MessageBoxResult.No || _HomeClubResult == MessageBoxResult.Cancel
                                   )
                                {
                                    return;
                                }
                            }
                        }
                        //GAS - 27 POS Home Club : 19/SEP/2017

                        this.Cursor = Cursors.Wait;
                        MakeaSale makeaSale = new MakeaSale();
                        makeaSale.Owner = this;
                        makeaSale.SetAccessRightsDS(_dsUserRights);
                        this.Cursor = Cursors.Arrow;
                        makeaSale._CurrentLoginDateTime = this._CurrentLoginDateTime;
                        makeaSale._CompanyName = _CompanyAndSiteName;
                        makeaSale._SiteName = this._SiteName;
                        makeaSale._UserName = this._UserName;
                        makeaSale._StaffPic = this._StaffPic;
                        //makeaSale.StaffImage.Source = this.StaffImage.Source;
                        makeaSale.StaffImage.Source = this.StaffImage.ImageSource;
                        makeaSale.ShowDialog();
                    }
                    else if (_ClockInCheck._clockedInAndClockedOut)
                    {
                        MessageBox.Show("End-Of-Shift has been completed for Today!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
                else
                {
                    decimal _clockInBalance = -1;
                    string _strClockInBalance = null;

                    _strClockInBalance = ConfigurationManager.AppSettings["POSOpeningBalance"].ToString();
                    if (_strClockInBalance == "")
                        _strClockInBalance = "0.00";

                    //_strClockInBalance = Interaction.InputBox("Enter Opening Cash Balance : ", "POS Clock-In");
                    //if ((_strClockInBalance == "") || (_strClockInBalance == null))
                    //{
                    //    MessageBox.Show("No Amount entered!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //    return;
                    //}
                    //if (!Regex.IsMatch(_strClockInBalance, @"^\d{1,7}(\.\d{1,2})?$"))
                    //{
                    //    MessageBox.Show("Amount should be Numeric! (upto 2 decimal places)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //    return;
                    //}
                    _clockInBalance = Convert.ToDecimal(_strClockInBalance);
                    if (_clockInBalance < 0)
                    {
                        MessageBox.Show("Amount CANNOT be less than Zero!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }

                    int _result = Comman.Constants.POS_ClockIn_User(_clockInBalance);

                    if (_result > 0)
                    {
                        //GAS - 27 POS Home Club : 19/SEP/2017
                        if (ConfigurationManager.AppSettings["SuperAdmin"].ToString().ToUpper().Trim() != "YES")
                        {
                            if
                            (
                                ConfigurationManager.AppSettings["LoggedInSiteID"].ToString().ToUpper().Trim() !=
                                ConfigurationManager.AppSettings["HomeClubSiteID"].ToString().ToUpper().Trim()
                            )
                            {
                                MessageBoxResult _HomeClubResult =
                                MessageBox.Show
                                    (
                                        "Your selected site is different to the Home site!" +
                                        "\nWould you like to still continue with POS transaction - Yes or No?" +
                                        "\n\n(CLICK 'YES' TO PROCEED, 'NO' TO EXIT!)",
                                        this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No
                                    );
                                if (
                                        _HomeClubResult == MessageBoxResult.No || _HomeClubResult == MessageBoxResult.Cancel
                                   )
                                {
                                    return;
                                }
                            }
                        }
                        //GAS - 27 POS Home Club : 19/SEP/2017

                        this.Cursor = Cursors.Wait;
                        MakeaSale makeaSale = new MakeaSale();
                        makeaSale.Owner = this;
                        makeaSale.SetAccessRightsDS(_dsUserRights);
                        this.Cursor = Cursors.Arrow;
                        makeaSale._CurrentLoginDateTime = this._CurrentLoginDateTime;
                        makeaSale._CompanyName = _CompanyAndSiteName;
                        makeaSale._SiteName = this._SiteName;
                        makeaSale._UserName = this._UserName;
                        makeaSale._StaffPic = this._StaffPic;
                        //makeaSale.StaffImage.Source = this.StaffImage.Source;
                        makeaSale.StaffImage.Source = this.StaffImage.ImageSource;
                        makeaSale.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Error in Staff Clock-In!", this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnProspects_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;
                Prospect mainProspect = new Prospect();
                mainProspect.Owner = this;
                //mainProspect.StaffImage.Source = StaffImage.Source;
                mainProspect.StaffImage.Source = StaffImage.ImageSource;
                mainProspect._StaffPic = _StaffPic;
                mainProspect._UserName = _UserName;
                mainProspect._CurrentLoginDateTime = _CurrentLoginDateTime;
                mainProspect._CompanyName = _CompanyAndSiteName;
                mainProspect._dSAccessRights = _dsUserRights;
                this.Cursor = Cursors.Arrow;
                mainProspect.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }
        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;
                Tasks Tk = new Tasks();
                Tk.Owner = this;

                Tk._invokedFromDashboard = true;
                Tk._CurrentLoginDateTime = this._CurrentLoginDateTime;
                Tk._CompanyName = _CompanyAndSiteName;
                Tk._SiteName = this._SiteName;
                Tk._UserName = this._UserName;
                Tk._StaffPic = this._StaffPic;
                //Tk.StaffImage.Source = this.StaffImage.Source;
                Tk.StaffImage.Source = this.StaffImage.ImageSource;

                Tk.SetAccessRightsDS(_dsUserRights);
                this.Cursor = Cursors.Arrow;
                Tk.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }
        private void btnLastVisitors_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;
                Mouse.OverrideCursor = Cursors.Wait;
                LastVisitor lV = new LastVisitor();
                lV.Owner = this;
                lV.SetAccessRightsDS(_dsUserRights);

                lV._CurrentLoginDateTime = this._CurrentLoginDateTime;
                lV._CompanyName = _CompanyAndSiteName;
                lV._SiteName = this._SiteName;
                lV._UserName = this._UserName;
                lV._StaffPic = this._StaffPic;
                //lV.StaffImage.Source = this.StaffImage.Source;
                lV.StaffImage.Source = this.StaffImage.ImageSource;

                this.Cursor = Cursors.Arrow;
                Mouse.OverrideCursor = null;
                lV.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
        }

        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
                {
                    this.Cursor = Cursors.Wait;
                    CompanyDetails _CompanySiteDetails = new CompanyDetails();
                    _CompanySiteDetails._CompanyName = _CompanyAndSiteName;
                    _CompanySiteDetails._UserName = this._UserName;
                    _CompanySiteDetails._CurrentLoginDateTime = this._CurrentLoginDateTime;
                    _CompanySiteDetails._StaffPic = this._StaffPic;
                    //_CompanySiteDetails.StaffImage.Source = StaffImage.Source;
                    _CompanySiteDetails.StaffImage.Source = StaffImage.ImageSource;

                    _CompanySiteDetails.Owner = this;
                    this.Cursor = Cursors.Arrow;
                    _CompanySiteDetails._Login = this._Login;
                    _CompanySiteDetails.ShowDialog();
                }
                else
                {
                    MessageBox.Show("ACCESS DENIED! \n\nYou MUST be Admin user to access Settings!", "Dashboard", MessageBoxButton.OK, MessageBoxImage.Stop);
                    this.Cursor = Cursors.Arrow;
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnReports_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;
                Report mainReport = new Report();

                mainReport._CompanyName = _CompanyAndSiteName;
                mainReport._UserName = this._UserName;
                mainReport._CurrentLoginDateTime = this._CurrentLoginDateTime;
                mainReport._StaffPic = this._StaffPic;
                mainReport.StaffImage.Source = StaffImage.ImageSource;

                mainReport.Owner = this;
                this.Cursor = Cursors.Arrow;
                mainReport.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnCommunication_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;
                Communication mainComm = new Communication();
                mainComm.Owner = this;

                mainComm._isfromDashboardComm = true;
                mainComm._CompanyName = _CompanyAndSiteName;
                mainComm._UserName = _UserName;
                mainComm._StaffPic = _StaffPic;
                //mainComm.StaffImage.Source = StaffImage.Source;
                mainComm.StaffImage.Source = StaffImage.ImageSource;
                mainComm._CurrentLoginDateTime = _CurrentLoginDateTime;

                mainComm.SetAccessRightsDS(_dsUserRights);
                this.Cursor = Cursors.Arrow;
                mainComm.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnBilling_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                #region "ACTUAL BUTTON STATUS CHECK CODE"
                if (ConfigurationManager.AppSettings["LoggedInOrgID"].ToString() != "")
                {
                    if (ConfigurationManager.AppSettings["LoggedInSiteID"].ToString() != "")
                    {
                        int _currSiteIndex = cmbSite.SelectedIndex;
                        Mouse.OverrideCursor = Cursors.Wait;
                        this.Cursor = Cursors.Wait;
                        PleaseWait _plsWait = new PleaseWait();

                        if (_fromRedButton)
                        {
                            _plsWait = new PleaseWait("Payment status Sync-Up & Access File Update...! Please Wait...");
                        }
                        _plsWait.Name = "PLEASEWAIT";
                        _plsWait.Owner = this;
                        _plsWait.Show();

                        if (!_fromRedButton)
                        {
                            for (int _siteIdx = 0; _siteIdx < cmbSite.Items.Count; _siteIdx++)
                            {
                                cmbSite.SelectedIndex = _siteIdx;
                                Comman.Constants.disHonourCheck_Tick();
                            }
                        }

                        cmbSite.SelectedIndex = _currSiteIndex;
                        int _fileRet = Comman.Constants.Create_ACCESS_DAT_File(false, -2, "MULTIPLE");

                        _plsWait.Close();
                        this.Cursor = Cursors.Arrow;
                        _maxDHDeferCounter = 1;
                        Mouse.OverrideCursor = null;

                        if (_fileRet == 0)
                        {
                            //rwTickButton.Height = new GridLength(25, GridUnitType.Star);

                            imggreenTick.Visibility = Visibility.Hidden;
                            imgredTick.Visibility = Visibility.Visible;
                            //tbAccessMsg.Visibility = Visibility.Visible;
                            //tbAccessMsg.Text = "Access File Failed !";
                            //tbAccessMsg.Foreground = System.Windows.Media.Brushes.Red;
                        }
                        else if (_fileRet == 1)
                        {
                            //rwTickButton.Height = new GridLength(25, GridUnitType.Star);

                            imggreenTick.Visibility = Visibility.Visible;
                            imgredTick.Visibility = Visibility.Hidden;
                            //tbAccessMsg.Visibility = Visibility.Visible;
                            //tbAccessMsg.Text = "Access File Success !";
                            //tbAccessMsg.Foreground = System.Windows.Media.Brushes.Green;

                            if (_fromRedButton)
                            {
                                if (!_DH_FirstRun_Complete)
                                {
                                    _DH_FirstRun_Complete = true;
                                    Comman.Constants.DH_StatusCheck_FirstRun_Update();
                                }
                                else
                                {
                                    _DH_SecondRun_Complete = true;
                                    Comman.Constants.DH_StatusCheck_SecondRun_Update();
                                }
                                _fromRedButton = false;
                            }
                        }
                    }
                }
                #endregion "BTN BILLING CODE"

                ////FOR ACTIVE SUBSCRIPTION STATUS UPDATE...
                //UpdateActiveMembershipItems();
                ////FOR ACTIVE SUBSCRIPTION STATUS UPDATE...
            }
            catch (Exception ex)
            {
                string _msg = ex.Message;
                //rwTickButton.Height = new GridLength(25, GridUnitType.Star);

                imgredTick.Visibility = Visibility.Visible;
                imggreenTick.Visibility = Visibility.Hidden;
                //tbAccessMsg.Visibility = Visibility.Visible;
                //tbAccessMsg.Text = "Access File Failed !";
                //tbAccessMsg.Foreground = System.Windows.Media.Brushes.Red;
            }
        }
        private void UpdateActiveMembershipItems(string _ImportFlag = "IMPORT-13-MAR-2017")
        {
            //Hashtable ht = new Hashtable();
            //SqlHelper _sql = new SqlHelper();

            //if (_sql._ConnOpen == 0)
            //{
            //    MessageBox.Show("No connection!");
            //    return;
            //}
            ////ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ////ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ////ht.Add("@ImportFlag", _ImportFlag);

            //DataSet _ds = _sql.ExecuteProcedure("EMF_PF_TOKEN_IDS", ht);

            //if (_ds != null)
            //{
            //    if (_ds.Tables.Count > 0)
            //    {
            //        if (_ds.Tables[0].DefaultView.Count > 0)
            //        {
            //            Mouse.OverrideCursor = Cursors.Wait;
            //            for (int i = 0; i < _ds.Tables[0].DefaultView.Count; i++)
            //            {
            //                if (_ds.Tables[0].DefaultView[i]["subscription_guid"].ToString() == "4fd4cf9b-8b61-4ffa-8f39-ed33699fe220")
            //                {
            //                    i = i;
            //                }
            //                paychoice.Update_Status_PaychoiceScheduledItems(_ds.Tables[0].DefaultView[i]["subscription_guid"].ToString(), _ds.Tables[0].DefaultView[i]["customer_guid"].ToString(), DateTime.Now);

            //                if (i >= 97)
            //                {
            //                    i = i;
            //                }
            //            }
            //            MessageBox.Show("status updated!");
            //            Mouse.OverrideCursor = null;
            //        }
            //    }
            //}
            Mouse.OverrideCursor = null;
        }
        private void btnReportsLeftPanel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;
                Report mainReport = new Report();
                mainReport.Owner = this;
                this.Cursor = Cursors.Arrow;
                mainReport.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void SetDashBoard_ACCESS_RIGHTS()
        {
            try
            {
                if (_dsUserRights != null)
                {
                    if (_dsUserRights.Tables.Count > 0)
                    {
                        if (_dsUserRights.Tables[_ModuleTable].DefaultView != null)
                        {
                            //VALIDATE ALL LEFT PANEL BUTTONS IN DASHBOARD FOR ACCESS RIGHTS!
                            //// 1 - DASHBOARD
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "ModuleName = 'DASHBOARD' AND Active = 1";
                            if (_dsUserRights.Tables[_ModuleTable].DefaultView.Count > 0)
                            {
                                SetDashBoard_SubModules_ACCESS_RIGHTS("DASHBOARD");
                            }
                            else
                            {
                                Disable_DashBoard_SubModules();
                            }
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "";

                            //// 2 - TASK
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "ModuleName = 'TASK' AND Active = 1";
                            if (_dsUserRights.Tables[_ModuleTable].DefaultView.Count > 0)
                            {
                                //btn_Task.IsEnabled = true;
                                btnTasks.IsEnabled = true;
                                btnTasks.Opacity = 1.0;
                            }
                            else
                            {
                                //btn_Task.IsEnabled = false;
                                btnTasks.IsEnabled = false;
                                btnTasks.Opacity = 0.5;
                            }
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "";

                            //// 3 - FIND MEMBER
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "ModuleName = 'FIND MEMBER' AND Active = 1";
                            if (_dsUserRights.Tables[_ModuleTable].DefaultView.Count > 0)
                            {
                                //btn_FindMember.IsEnabled = true;
                                if (_FindMemberAccessRights)
                                {
                                    btnFindMember.IsEnabled = true;
                                    btnFindMember.Opacity = 1.00;
                                }
                                else
                                {
                                    btnFindMember.IsEnabled = true;
                                    btnFindMember.Opacity = 1.00;
                                }
                            }
                            else
                            {
                                //btn_FindMember.IsEnabled = false;
                                btnFindMember.IsEnabled = false;
                            }
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "";

                            //// 4 - POINT OF SALE
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "ModuleName = 'POINT OF SALE' AND Active = 1";
                            if (_dsUserRights.Tables[_ModuleTable].DefaultView.Count > 0)
                            {
                                //btn_POS.IsEnabled = true;
                                btnPOS.IsEnabled = true;
                                btnPOS.Opacity = 1.0;
                            }
                            else
                            {
                                //btn_POS.IsEnabled = false;
                                btnPOS.IsEnabled = false;
                                btnPOS.Opacity = 0.5;
                            }
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "";

                            //// 5 - LAST VISITORS
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "ModuleName = 'LAST VISITORS' AND Active = 1";
                            if (_dsUserRights.Tables[_ModuleTable].DefaultView.Count > 0)
                            {
                                //btn_LastVisitors.IsEnabled = true;
                                btnLastVisitors.IsEnabled = true;
                                btnLastVisitors.Opacity = 1.00;
                            }
                            else
                            {
                                //btn_LastVisitors.IsEnabled = false;
                                btnLastVisitors.IsEnabled = false;
                            }
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "";

                            //// 6 - COMMUNICATION
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "ModuleName = 'COMMUNICATION' AND Active = 1";
                            if (_dsUserRights.Tables[_ModuleTable].DefaultView.Count > 0)
                            {
                                //btn_Comm.IsEnabled = true;
                                btnCommunication.IsEnabled = true;
                                btnCommunication.Opacity = 1.00;
                            }
                            else
                            {
                                //btn_Comm.IsEnabled = false;
                                btnCommunication.IsEnabled = false;
                            }
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "";

                            //// 7 - BILLING
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "ModuleName = 'BILLING' AND Active = 1";
                            if (_dsUserRights.Tables[_ModuleTable].DefaultView.Count > 0)
                            {
                                //btn_Billing.IsEnabled = true;
                                btnBilling.IsEnabled = true;
                                btnBilling.Opacity = 1.00;
                            }
                            else
                            {
                                //btn_Billing.IsEnabled = false;
                                btnBilling.IsEnabled = false;
                            }
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "";

                            //// 8 - REPORTS
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "ModuleName = 'REPORTS' AND Active = 1";
                            if (_dsUserRights.Tables[_ModuleTable].DefaultView.Count > 0)
                            {
                                //btn_Rpt.IsEnabled = true;
                                btnReports.IsEnabled = true;
                                btnReports.Opacity = 1.00;
                            }
                            else
                            {
                                //btn_Rpt.IsEnabled = false;
                                btnReports.IsEnabled = false;
                            }
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "";

                            //// 9 - SETTINGS
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "ModuleName = 'SETTINGS' AND Active = 1";
                            if (_dsUserRights.Tables[_ModuleTable].DefaultView.Count > 0)
                            {
                                //btn_Settings.IsEnabled = true;
                                btnSettings.IsEnabled = true;
                                btnSettings.Opacity = 1.00;
                            }
                            else
                            {
                                //btn_Settings.IsEnabled = false;
                                btnSettings.IsEnabled = false;
                            }
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "";

                            //// 10 - HELP
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "ModuleName = 'HELP' AND Active = 1";
                            if (_dsUserRights.Tables[_ModuleTable].DefaultView.Count > 0)
                            {
                                //btn_Help.IsEnabled = true;
                                btnHelp.IsEnabled = true;
                                btnHelp.Opacity = 1.00;
                            }
                            else
                            {
                                //btn_Help.IsEnabled = false;
                                btnHelp.IsEnabled = false;
                            }

                            //// 11 - ACCESS RESET
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "ModuleName = 'ACCESS RESET' AND Active = 1";
                            if (_dsUserRights.Tables[_ModuleTable].DefaultView.Count > 0)
                            {
                                //btn_AccessReset.IsEnabled = true;
                                btnAccessReset.IsEnabled = true;
                                btnAccessReset.Opacity = 1.00;
                            }
                            else
                            {
                                //btn_AccessReset.IsEnabled = false;
                                btnAccessReset.IsEnabled = false;
                            }
                            _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "";
                            //VALIDATE ALL LEFT PANEL BUTTONS IN DASHBOARD FOR ACCESS RIGHTS!
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private void SetDashBoard_SubModules_ACCESS_RIGHTS(string ModuleName)
        {
            if (_dsUserRights != null)
            {
                if (_dsUserRights.Tables[_SubModuleTable].DefaultView != null)
                {
                    _dsUserRights.Tables[_SubModuleTable].DefaultView.RowFilter = "";
                    switch (ModuleName.ToUpper())
                    {
                        case "DASHBOARD":
                            Disable_DashBoard_SubModules();
                            break;
                        default:
                            break;
                    }

                    _dsUserRights.Tables[_SubModuleTable].DefaultView.RowFilter = "ModuleName = '" + ModuleName + "' AND Active = 1";
                    if (_dsUserRights.Tables[_SubModuleTable].DefaultView.Count > 0)
                    {
                        for (int i = 0; i < _dsUserRights.Tables[_SubModuleTable].DefaultView.Count; i++)
                        {
                            switch (_dsUserRights.Tables[_SubModuleTable].DefaultView[i]["SubModuleName"].ToString().ToUpper())
                            {
                                case "ADD MEMBER":
                                    btnAddMember.IsEnabled = true;
                                    btnAddMember.Opacity = 1.00;
                                    break;
                                case "FIND MEMBER":
                                    btnFindMember.IsEnabled = true;
                                    _FindMemberAccessRights = true;
                                    btnFindMember.Opacity = 1.00;
                                    break;
                                case "BOOKING":
                                    btnDashboardBooking.IsEnabled = true;
                                    btnDashboardBooking.Opacity = 1.00;
                                    break;
                                case "PROSPECTS":
                                    btnProspects.IsEnabled = true;
                                    btnProspects.Opacity = 1.0;
                                    break;
                                case "TASKS":
                                    btnTasks.IsEnabled = true;
                                    btnTasks.Opacity = 1.0;
                                    break;
                                case "LAST VISITORS":
                                    btnLastVisitors.IsEnabled = true;
                                    btnLastVisitors.Opacity = 1.0;
                                    break;
                                case "MAKE A SALE":
                                    btnPOS.IsEnabled = true;
                                    btnPOS.Opacity = 1.0;
                                    break;
                                case "REPORTS":
                                    btnReports.IsEnabled = true;
                                    btnReports.Opacity = 1.0;
                                    break;
                                case "BILLING":
                                    btnBilling.IsEnabled = true;
                                    btnBilling.Opacity = 1.0;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        }
        private void Disable_DashBoard_SubModules()
        {
            btnAddMember.IsEnabled = false;
            btnAddMember.Opacity = 0.50;

            btnFindMember.IsEnabled = false;
            btnFindMember.Opacity = 0.50;

            btnDashboardBooking.IsEnabled = false;
            btnDashboardBooking.Opacity = 0.50;

            btnProspects.IsEnabled = false;
            btnProspects.Opacity = 0.50;

            btnTasks.IsEnabled = false;
            btnTasks.Opacity = 0.50;

            btnLastVisitors.IsEnabled = false;
            btnLastVisitors.Opacity = 0.50;

            btnPOS.IsEnabled = false;
            btnPOS.Opacity = 0.50;

            btnReports.IsEnabled = false;
            btnReports.Opacity = 0.50;

            btnBilling.IsEnabled = false;
            btnBilling.Opacity = 0.50;
        }

        //private void LoadProgressBarMEMBER()
        //{
        //    int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
        //    int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
        //    Hashtable ht = new Hashtable();

        //    ht.Add("@CompanyID", _CompanyID);
        //    ht.Add("@SiteID", _SiteID);
        //    DataSet ds = sql.ExecuteProcedure("GetMemberActiveInActivePercent", ht);

        //    if (ds != null)
        //    {
        //        if (ds.Tables[0].DefaultView != null)
        //        {
        //            _percent = Convert.ToInt32(ds.Tables[0].Rows[0]["Percentage"].ToString()); 
        //            lblMemberMsg.Content = "Member Retention";

        //            DispatcherTimer dispatcherTimer1 = new DispatcherTimer();
        //            dispatcherTimer1.Tick += new EventHandler(dispatcherTimer1_Tick);
        //            dispatcherTimer1.Interval = new TimeSpan(0, 0, 0, 0, 10);
        //            pBarText.Text = "0 %";
        //            pBarMember.Value = 0;
        //            dispatcherTimer1.Start();
        //            if (pBarMember.Value >= _percent)
        //            {
        //                dispatcherTimer1.Stop();
        //            }
        //        }
        //    }
        //}

        private void btnAccessReset_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
                {
                    ////ACCESS CONTROL FILE TO BE CREATED FOR ALL MEMBERS WITH "P|RESET"
                    int _access = Comman.Constants.Create_ACCESS_DAT_File(true);
                    if (_access == 1)
                    {
                        MessageBox.Show("(RESET) Access Control File Created!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else if (_access == -1)
                    {
                        MessageBox.Show("No Data!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else if (_access == 0)
                    {
                        MessageBox.Show("ERROR! File NOT Created!\nPlease ensure you have Write permission to the folder path!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBox.Show("ACCESS DENIED! \n\nYou MUST be Admin user to access this Screen!", "Dashboard", MessageBoxButton.OK, MessageBoxImage.Stop);
                    this.Cursor = Cursors.Arrow;
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private void btnAccessWithoutReset_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
                {
                    ////ACCESS CONTROL FILE TO BE CREATED FOR ALL MEMBERS WITH "P|RESET"
                    int _access = Comman.Constants.Create_ACCESS_DAT_File(false);
                    if (_access == 1)
                    {
                        MessageBox.Show("Access Control File Created! (NO RESET)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else if (_access == -1)
                    {
                        MessageBox.Show("No Data!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else if (_access == 0)
                    {
                        MessageBox.Show("ERROR! File NOT Created!\nPlease ensure you have Write permission to the folder path!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBox.Show("ACCESS DENIED! \n\nYou MUST be Admin user to access this Screen!", "Dashboard", MessageBoxButton.OK, MessageBoxImage.Stop);
                    this.Cursor = Cursors.Arrow;
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        //private void btn_AccessReset_MouseOver(object sender, MouseEventArgs e)
        //{
        //    //btn_AccessReset.Background = Brushes.Wheat;
        //    this.Cursor = Cursors.Hand;
        //}
        //private void btn_AccessReset_MouseLeave(object sender, MouseEventArgs e)
        //{
        //    btn_AccessReset.Background = Brushes.LightGray;
        //    this.Cursor = Cursors.Arrow;
        //}

        private void LoadSites()
        {
            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return;

            cmbSite.ItemsSource = null;
            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            ht.Clear();
            ht.Add("@CompanyID", _CompanyID);
            _dsSites = sql.ExecuteProcedure("GetSitesForCompanyID", ht);
            if (_dsSites != null)
            {
                if (_dsSites.Tables[0].DefaultView.Count > 0)
                {
                    cmbSite.ItemsSource = _dsSites.Tables[0].DefaultView;
                    cmbSite.DisplayMemberPath = "SiteName";
                    cmbSite.SelectedValuePath = "SiteID";
                }
                cmbSite.Text = ConfigurationManager.AppSettings["LoggedInSite"].ToString();
            }
        }

        private void LoadDoors()
        {
            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return;

            cmbDoors.ItemsSource = null;
            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            ht.Clear();
            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);

            _dsDoors = sql.ExecuteProcedure("GetDoorsForCompanySite", ht);
            if (_dsDoors != null)
            {
                if (_dsDoors.Tables[0].DefaultView.Count > 0)
                {
                    cmbDoors.ItemsSource = _dsDoors.Tables[0].DefaultView;
                    cmbDoors.DisplayMemberPath = "DoorName";
                    cmbDoors.SelectedValuePath = "DoorCodeNo";
                }
            }
        }
        private void cmbSite_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbSite.SelectedIndex != -1)
            {
                ConfigurationManager.AppSettings["LoggedInSiteID"] = cmbSite.SelectedValue.ToString();
                ConfigurationManager.AppSettings["LoggedInSite"] = (cmbSite.SelectedItem as DataRowView).Row["SiteName"].ToString();
                _SiteName = ConfigurationManager.AppSettings["LoggedInSite"].ToString().ToUpper();
                _CompanyAndSiteName = _CompanyName + " ( " + _SiteName.Replace("_", "__") + " )";

                //lblSiteName.Content = (cmbSite.SelectedItem as DataRowView).Row["SiteName"].ToString().Replace("_", "__");
                if (_dsSites.Tables[0].DefaultView.Count > 0)
                {
                    _dsSites.Tables[0].DefaultView.RowFilter = "";
                    //_dsSites.Tables[0].DefaultView.RowFilter = "SiteName = '" + (cmbSite.SelectedItem as DataRowView).Row["SiteName"].ToString() + "'";
                    _dsSites.Tables[0].DefaultView.RowFilter = "SiteID = " + Convert.ToInt32(cmbSite.SelectedValue);

                    if (_dsSites.Tables[0].DefaultView.Count > 0)
                    {
                        ConfigurationManager.AppSettings["AccessControl_FilePath"] = _dsSites.Tables[0].DefaultView[0]["AccessControl_FilePath"].ToString();
                        ConfigurationManager.AppSettings["AccessControl_FileName"] = _dsSites.Tables[0].DefaultView[0]["AccessControl_FileName"].ToString();

                        //EMAIL config from SiteMaster
                        ConfigurationManager.AppSettings["SMTPHost"] = _dsSites.Tables[0].DefaultView[0]["SMTPHost"].ToString();
                        ConfigurationManager.AppSettings["SMTPPort"] = _dsSites.Tables[0].DefaultView[0]["SMTPPort"].ToString();
                        ConfigurationManager.AppSettings["SMTPUserName"] = _dsSites.Tables[0].DefaultView[0]["SMTPUserName"].ToString();
                        ConfigurationManager.AppSettings["SMTPEncryptPassword"] = _dsSites.Tables[0].DefaultView[0]["SMTPEncryptPassword"].ToString();

                        //Paychoice config from SiteMaster
                        ConfigurationManager.AppSettings["PaychoiceSandboxURL"] = _dsSites.Tables[0].DefaultView[0]["SandboxURL"].ToString();
                        ConfigurationManager.AppSettings["PaychoiceSandboxUsername"] = _dsSites.Tables[0].DefaultView[0]["SandboxUsername"].ToString();
                        ConfigurationManager.AppSettings["PaychoiceSandboxPassword"] = _dsSites.Tables[0].DefaultView[0]["SandboxPassword"].ToString();
                        ConfigurationManager.AppSettings["PaychoiceSecuredURL"] = _dsSites.Tables[0].DefaultView[0]["SecureURL"].ToString();
                        ConfigurationManager.AppSettings["PaychoiceSecuredUsername"] = _dsSites.Tables[0].DefaultView[0]["SecureUsername"].ToString();
                        ConfigurationManager.AppSettings["PaychoiceSecuredPassword"] = _dsSites.Tables[0].DefaultView[0]["SecurePassword"].ToString();
                        ConfigurationManager.AppSettings["PaychoiceUsagePortal"] = _dsSites.Tables[0].DefaultView[0]["ActivePortal"].ToString();

                        ////Address details from SiteMaster - for POS
                        ConfigurationManager.AppSettings["selectedSiteAddr1"] = _dsSites.Tables[0].DefaultView[0]["Addr1"].ToString();
                        ConfigurationManager.AppSettings["selectedSiteAddr2"] = _dsSites.Tables[0].DefaultView[0]["Addr2"].ToString();
                        ConfigurationManager.AppSettings["selectedSiteState"] = _dsSites.Tables[0].DefaultView[0]["State"].ToString();

                        ////POS details from SiteMaster
                        ConfigurationManager.AppSettings["POSOpeningBalance"] = _dsSites.Tables[0].DefaultView[0]["POS_OpeningBalance"].ToString();
                        ConfigurationManager.AppSettings["POSReportEmail"] = _dsSites.Tables[0].DefaultView[0]["POS_Report_Email"].ToString();
                        ConfigurationManager.AppSettings["POSStockLevelAlertEmail"] = _dsSites.Tables[0].DefaultView[0]["POS_Stock_Level_Alert_Email"].ToString();

                        //COM PORT
                        ConfigurationManager.AppSettings["AccessControlPORT"] = _dsSites.Tables[0].DefaultView[0]["AccessControlPORT"].ToString();

                        //GAS-19 TECHNOGYM PARAMETERS
                        ConfigurationManager.AppSettings["test_technoGym_URL"] = _dsSites.Tables[0].DefaultView[0]["test_technoGym_URL"].ToString();
                        ConfigurationManager.AppSettings["test_technoGym_Facility"] = _dsSites.Tables[0].DefaultView[0]["test_technoGym_Facility"].ToString();
                        ConfigurationManager.AppSettings["test_technoGym_UniqueID"] = _dsSites.Tables[0].DefaultView[0]["test_technoGym_UniqueID"].ToString();
                        ConfigurationManager.AppSettings["test_technoGym_hdr_CLIENT"] = _dsSites.Tables[0].DefaultView[0]["test_technoGym_hdr_CLIENT"].ToString();
                        ConfigurationManager.AppSettings["test_technoGym_hdr_APIKEY"] = _dsSites.Tables[0].DefaultView[0]["test_technoGym_hdr_APIKEY"].ToString();
                        ConfigurationManager.AppSettings["test_technoGym_param_Username"] = _dsSites.Tables[0].DefaultView[0]["test_technoGym_param_Username"].ToString();
                        ConfigurationManager.AppSettings["test_technoGym_param_Password"] = _dsSites.Tables[0].DefaultView[0]["test_technoGym_param_Password"].ToString();

                        ConfigurationManager.AppSettings["PROD_technoGym_URL"] = _dsSites.Tables[0].DefaultView[0]["PROD_technoGym_URL"].ToString();
                        ConfigurationManager.AppSettings["PROD_technoGym_Facility"] = _dsSites.Tables[0].DefaultView[0]["PROD_technoGym_Facility"].ToString();
                        ConfigurationManager.AppSettings["PROD_technoGym_UniqueID"] = _dsSites.Tables[0].DefaultView[0]["PROD_technoGym_UniqueID"].ToString();
                        ConfigurationManager.AppSettings["PROD_technoGym_hdr_CLIENT"] = _dsSites.Tables[0].DefaultView[0]["PROD_technoGym_hdr_CLIENT"].ToString();
                        ConfigurationManager.AppSettings["PROD_technoGym_hdr_APIKEY"] = _dsSites.Tables[0].DefaultView[0]["PROD_technoGym_hdr_APIKEY"].ToString();
                        ConfigurationManager.AppSettings["PROD_technoGym_param_Username"] = _dsSites.Tables[0].DefaultView[0]["PROD_technoGym_param_Username"].ToString();
                        ConfigurationManager.AppSettings["PROD_technoGym_param_Password"] = _dsSites.Tables[0].DefaultView[0]["PROD_technoGym_param_Password"].ToString();

                        ConfigurationManager.AppSettings["TechnoGym_Test_OR_Prod"] = _dsSites.Tables[0].DefaultView[0]["TechnoGym_Test_OR_Prod"].ToString().ToUpper();
                        //GAS-19 TECHNOGYM PARAMETERS
                    }
                    _dsSites.Tables[0].DefaultView.RowFilter = "";
                }
                txtMemberNameorCardNo.Text = "";
                LoadDoors();
                //LoadMemberListFromDBOnLoad();
                GetRecentCheckInDetails();
            }
        }
        private void loadStaffPhoto()
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    //StaffImage.Source = bi;
                    StaffImage.ImageSource = bi;
                }
            }
        }

        private void cmbDoors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbDoors.SelectedIndex != -1)
            {
                _DoorToOpen = (cmbDoors.SelectedValue + "").ToString();
            }
        }

        private void btnDoorOpen_Click(object sender, RoutedEventArgs e)
        {
            if (_DoorToOpen.Trim() != "")
            {
                string _successORErrMsg = Comman.Constants.Create_ACCESS_DOOR_File(_DoorToOpen);
                if (_successORErrMsg == "1")
                {
                    //MessageBox.Show("Door Access File Created!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("ERROR! File NOT Created!\nPlease ensure write permission to the path is properly set!\n" + _successORErrMsg, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                MessageBox.Show("Select a Door to Open!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (txtMemberNameorCardNo.Text.Trim() != "")
            {
                string _SearchStr = txtMemberNameorCardNo.Text.Trim();
                if (_ds != null)
                {
                    _ds.Tables[_MemberDetailsDataTable].DefaultView.RowFilter = "";
                    grdFindMember.Visibility = Visibility.Visible;
                    if (_ds.Tables[_MemberDetailsDataTable].DefaultView.Count > 0)
                    {
                        _ds.Tables[_MemberDetailsDataTable].DefaultView.RowFilter = "(LastName LIKE '%" + _SearchStr + "%' OR FirstName LIKE '%"
                                                                                    + _SearchStr + "%' OR CardNumber LIKE '%" + _SearchStr + "%')";
                        grdFindMember.ItemsSource = null;
                        grdFindMember.ItemsSource = _ds.Tables[_MemberDetailsDataTable].DefaultView;
                    }
                    else
                    {
                        grdFindMember.ItemsSource = null;
                    }
                }
            }
        }

        private void txtMemberNameorCardNo_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtMemberNameorCardNo.Text.Trim() == "")
            {
                grdFindMember.ItemsSource = null;
                grdFindMember.Visibility = Visibility.Hidden;
            }
            else
            {
                grdFindMember.Visibility = Visibility.Visible;
                string _SearchStr = txtMemberNameorCardNo.Text.Trim();
                if (_ds != null)
                {
                    _ds.Tables[_MemberDetailsDataTable].DefaultView.RowFilter = "";
                    if (_ds.Tables[_MemberDetailsDataTable].DefaultView.Count > 0)
                    {
                        _ds.Tables[_MemberDetailsDataTable].DefaultView.RowFilter = "(LastName LIKE '%"
                                                                                    + _SearchStr.Replace("'", "''") + "%' OR FirstName LIKE '%"
                                                                                    + _SearchStr.Replace("'", "''") + "%' OR CardNumber LIKE '%"
                                                                                    + _SearchStr.Replace("'", "''") + "%')";
                        grdFindMember.ItemsSource = null;
                        grdFindMember.ItemsSource = _ds.Tables[_MemberDetailsDataTable].DefaultView;
                    }
                    else
                    {
                        grdFindMember.ItemsSource = null;
                    }
                }
            }
        }

        private void btnCheckInNow_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ValidateData())
                {
                    int GetMemberDetailsMemberId; int _ProgrammeID = 0;
                    string _Door = cmbDoors.Text.ToUpper();
                    DateTime CheckInDate = DateTime.Now;
                    string _memName;

                    _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    //_SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                    _SiteID = Convert.ToInt32(_ds.Tables[_MemberDetailsDataTable].DefaultView[grdFindMember.SelectedIndex]["SiteID"].ToString());

                    if (_Door.Trim() == "")
                    {
                        if (cmbDoors.Items.Count > 0)
                        {
                            _Door = (cmbDoors.Items[0] as DataRowView).Row["DoorName"].ToString().ToUpper();
                        }
                        else
                        {
                            _Door = "RECEPTION";
                        }
                    }

                    if (grdFindMember.SelectedIndex != -1)
                    {
                        GetMemberDetailsMemberId = Convert.ToInt32((grdFindMember.SelectedItem as DataRowView).Row["MemberNo"].ToString());
                        _memName = (grdFindMember.SelectedItem as DataRowView).Row["FirstName"].ToString() + ' ' + (grdFindMember.SelectedItem as DataRowView).Row["LastName"].ToString();
                    }
                    else
                    {
                        GetMemberDetailsMemberId = 0;
                        MessageBox.Show("No Member Has been Selected!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                    DataSet ds = _memberDetailsBal.get_ActiveMembership(GetMemberDetailsMemberId, _CompanyID, _SiteID);
                    if (ds != null)
                    {
                        if (ds.Tables[0].DefaultView.Count > 0)
                        {
                            if (ds.Tables[0].DefaultView.Count == 1)
                            {
                                _ProgrammeID = Convert.ToInt32(ds.Tables[0].DefaultView[0]["ProgrammeID"].ToString());
                            }
                            //select active membership
                            else
                            {
                                CheckIn _checkIn = new CheckIn(ds.Tables[0]);
                                _checkIn.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                                _checkIn._MemberName = _memName;
                                _checkIn.Owner = this;
                                _checkIn.ShowDialog();
                                _ProgrammeID = _checkInProgrammeID;
                            }
                            if (_ProgrammeID == 0)
                            {
                                _ProgrammeID = 0;
                                GetMemberDetailsMemberId = 0;
                                MessageBox.Show("Membership to Check-In NOT selected!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                return;
                            }
                        }
                        else
                        {
                            _ProgrammeID = 0;
                            GetMemberDetailsMemberId = 0;
                            MessageBox.Show("There are NO active Memberships for this Member!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            return;
                        }
                    }
                    _memberDetailsBal.SaveCheckInDetails(GetMemberDetailsMemberId, _CompanyID, _SiteID, _ProgrammeID, CheckInDate, _Door);

                    txtMemberNameorCardNo.Text = "";
                    grdFindMember.ItemsSource = null;
                    grdFindMember.Visibility = Visibility.Hidden;

                    Comman.Constants.Popup_dispatcherTimer_Tick(GetMemberDetailsMemberId, _SiteID);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Check-In Error : " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private bool ValidateData()
        {
            if (grdFindMember.SelectedIndex == -1)
            {
                MessageBox.Show("No Member Has been Selected!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            //if (cmbDoors.SelectedIndex == -1)
            //{
            //    MessageBox.Show("Select Door to Check-In!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //    return false;
            //}
            return true;
        }

        private void btnPOS1_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            POS frmPOS = new POS();
            frmPOS.Owner = this;
            frmPOS.SetAccessRightsDS(_dsUserRights);
            Cursor = Cursors.Arrow;
            frmPOS._CurrentLoginDateTime = _CurrentLoginDateTime;
            frmPOS._CompanyName = _CompanyAndSiteName;
            frmPOS._SiteName = _SiteName;
            frmPOS._UserName = _UserName;
            frmPOS._StaffPic = _StaffPic;
            //frmPOS.StaffImage.Source = StaffImage.Source;
            frmPOS.StaffImage.Source = StaffImage.ImageSource;
            frmPOS.ShowDialog();
        }

        private void MetroWindow_Activated(object sender, EventArgs e)
        {
            txtMemberNameorCardNo.Text = "";
            //LoadMemberListFromDBOnLoad();
        }

        private void btnCasualMember_Click(object sender, RoutedEventArgs e)
        {
            _isDishonourCheckEnabled = false;
            this.Cursor = Cursors.Wait;
            Prospect mainProspect = new Prospect();
            mainProspect.Owner = this;
            //mainProspect.StaffImage.Source = StaffImage.Source;
            mainProspect.StaffImage.Source = StaffImage.ImageSource;
            mainProspect._StaffPic = _StaffPic;
            mainProspect._UserName = _UserName;
            mainProspect._CurrentLoginDateTime = _CurrentLoginDateTime;
            mainProspect._CompanyName = _CompanyAndSiteName;
            mainProspect._dSAccessRights = _dsUserRights;
            mainProspect._boolFromCasualMember = true;
            this.Cursor = Cursors.Arrow;
            mainProspect.ShowDialog();
        }

        private void imgredTick_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _fromRedButton = true;
            btnBilling_Click(sender, e);
        }
        public void GetDisConnectedDataSetALLTables() //TO WORK OFFLINE
        {
            try
            {
                Comman.Constants.disconnected_DataSet();
                for (int i = 0; i <= Comman.Constants._DisConnectedALLTables.Tables[0].Rows.Count - 1; i++)
                {
                    Comman.Constants._DisConnectedALLTables.Tables[i + 1].TableName = Comman.Constants._DisConnectedALLTables.Tables[0].Rows[i]["Name"].ToString();
                }
            }
            catch (Exception ex)
            {
                string _msg = ex.Message;
            }
        }
        private void ReadCOMPort(string _PortName = "")
        {
            try
            {
                //string[] _portNames = SerialPort.GetPortNames();

                if (_serialPort != null || _serialPort.IsOpen)
                {
                    _serialPort.Close();
                }
                if (_PortName == "")
                    _PortName = ConfigurationManager.AppSettings["AccessControlPORT"].ToString().ToUpper().Trim();

                //SETTINGS FOR TEST; NEED TO BE ASSIGNED FROM 'MASTER' SETTINGS
                _serialPort = new SerialPort();
                _serialPort.PortName = _PortName;
                _serialPort.BaudRate = 9600;
                _serialPort.Parity = Parity.None;
                _serialPort.StopBits = StopBits.One;
                _serialPort.DataBits = 8;
                _serialPort.DiscardNull = true;

                // Subscribe to event and open serial port for data
                _serialPort.DataReceived +=
                    new SerialDataReceivedEventHandler(_serialPort_DataReceived);
                _serialPort.Open();
            }
            catch (Exception ex)
            {
                string _msg = ex.Message;
            }
        }
        private void ReadCOMPort_Multiple(string _PortName = "")
        {
            try
            {
                if (_portNames.Length > 0)
                {
                    _serialPortsList.Clear();
                    for (int i = 0; i < _portNames.Length; i++)
                    {
                        _serialPortsList.Add(new SerialPort());
                        _serialPortsList[i].PortName = _portNames[i];
                        
                        //if (_serialPortsList[i] != null || _serialPortsList[i].IsOpen)
                        //{
                            //_serialPortsList[i].Close();
                            
                            //SETTINGS FOR TEST; NEED TO BE ASSIGNED FROM 'MASTER' SETTINGS
                            //_serialPortsList[i] = new SerialPort();
                            //_serialPortsList[i].PortName = _portNames[i];
                            _serialPortsList[i].BaudRate = 9600;
                            _serialPortsList[i].Parity = Parity.None;
                            _serialPortsList[i].StopBits = StopBits.One;
                            _serialPortsList[i].DataBits = 8;
                            _serialPortsList[i].DiscardNull = true;

                            // Subscribe to event and open serial port for data
                            _serialPortsList[i].DataReceived +=
                                new SerialDataReceivedEventHandler(_serialPort_DataReceived);
                            _serialPortsList[i].Open();
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                string _msg = ex.Message;
            }
        }

        void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                int dataLength = _serialPort.BytesToRead;
                if (dataLength >= 10)
                //if (dataLength >= 4)
                {
                    _DataCardNumber_READEXISTING = _serialPort.ReadExisting();

                    //REMOVE '\r' Special Chars
                    while (_DataCardNumber_READEXISTING.IndexOf('\r') >= 0)
                    {
                        _DataCardNumber_READEXISTING = _DataCardNumber_READEXISTING.Remove(_DataCardNumber_READEXISTING.IndexOf('\r'), 1);
                    }
                    //REMOVE '\r' Special Chars

                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();

                    Application.Current.Dispatcher.Invoke(new Action(() => { listCard(); }));
                    _DataCardNumber_READEXISTING = "";
                }
            }
            catch (Exception x)
            {
                //MessageBox.Show("Error : " + x.Message);
                string _msg = x.Message;
            }
        }
        void listCard()
        {
            try
            {
                //Stored Proc Name : COM_Port_GetMemberDetails
                Console.Beep(4000, 100);
                long _cardNumber = Convert.ToInt64(_DataCardNumber_READEXISTING);
                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());

                SqlHelper _Sql = new SqlHelper();
                if (_Sql._ConnOpen == 0)
                {
                    lstMemberCheckIn.Items.Insert(0, "ERROR : COULD NOT READ CARD");
                    return;
                }

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@CardNumber", _cardNumber);
                DataSet _dsCardMember = _Sql.ExecuteProcedure("COM_Port_GetMemberDetails", ht);
                if (_dsCardMember != null)
                {
                    if (_dsCardMember.Tables.Count > 0)
                    {
                        if (_dsCardMember.Tables[0].DefaultView.Count > 0)
                        {
                            string _Door = "";
                            if (cmbDoors.Items.Count > 0)
                            {
                                if (cmbDoors.SelectedIndex >= 0)
                                {
                                    //SELECT SELECTED DOOR
                                    _Door = (cmbDoors.SelectedItem as DataRowView).Row["DoorName"].ToString().ToUpper();
                                }
                                else
                                {
                                    _Door = (cmbDoors.Items[0] as DataRowView).Row["DoorName"].ToString().ToUpper();
                                }
                            }
                            int _MemberNo = Convert.ToInt32(_dsCardMember.Tables[0].DefaultView[0]["MemberNo"].ToString());
                            string _CardNumber = _dsCardMember.Tables[0].DefaultView[0]["CardNumber"].ToString();
                            string _AccessTypeName = _dsCardMember.Tables[0].DefaultView[0]["AccessTypeName"].ToString().ToUpper();
                            int _SiteID_Member = Convert.ToInt32(_dsCardMember.Tables[0].DefaultView[0]["SiteID"].ToString());
                            byte[] _data = null;

                            string _MemName = _dsCardMember.Tables[0].DefaultView[0]["FirstName"].ToString().ToUpper() + " " + _dsCardMember.Tables[0].DefaultView[0]["LastName"].ToString().ToUpper();

                            MemberDetailsDAL _memberDAL = new MemberDetailsDAL();
                            _memberDAL.SaveCheckInDetails(_MemberNo, _CompanyID, _SiteID_Member, 0, DateTime.Now, _Door, "AUTO", 1);

                            lstMemberCheckIn.Items.Insert(0, _MemName + ", " + _AccessTypeName + " , " + DateTime.Now.ToString(" hh:mm tt"));
                            if (_dsCardMember.Tables[0].DefaultView[0]["MemberPic"] != DBNull.Value)
                            {
                                _data = (byte[])_dsCardMember.Tables[0].DefaultView[0]["MemberPic"];
                            }
                            PopUp_Auto_Checkin(_data, _MemName, _CardNumber, _AccessTypeName);
                        }
                        else
                        {
                            lstMemberCheckIn.Items.Insert(0, "ERROR : COULD NOT FIND CARD NUMBER (" + _DataCardNumber_READEXISTING + ")");
                        }
                    }
                    else
                    {
                        lstMemberCheckIn.Items.Insert(0, "ERROR : COULD NOT FIND CARD NUMBER (" + _DataCardNumber_READEXISTING + ")");
                    }
                }
                else
                {
                    lstMemberCheckIn.Items.Insert(0, "ERROR : COULD NOT FIND CARD NUMBER (" + _DataCardNumber_READEXISTING + ")");
                }
            }
            catch (Exception ex)
            {
                string _msg = ex.Message;
                lstMemberCheckIn.Items.Insert(0, "ERROR : COULD NOT READ CARD");
            }
        }
        private void PopUp_Auto_Checkin(byte[] data, string _MName = "", string _CardNum = "", string _AccessTypeName = "")
        {
            try
            {
                CheckInPopUp _chkPopUp = new CheckInPopUp(nOrder);
                nOrder++; if (nOrder > 1) nOrder = 1;

                _chkPopUp._MemberName = _MName;
                _chkPopUp._CardNumber = _CardNum + " (" + _AccessTypeName.ToUpper() + ")";
                _chkPopUp._data = data;

                _isPopupShown = true;

                //_swipeInPopup = new DispatcherTimer(DispatcherPriority.Input);
                //_swipeInPopup.Tick += new EventHandler(_PopupDelay);
                //_swipeInPopup.Interval = new TimeSpan(0, 0, 0, 5);
                //_swipeInPopup.Start();
                //_chkPopUp.Owner = 
                _chkPopUp.Topmost = true;
                _chkPopUp.Show();
                System.Threading.Thread.Sleep(3000);
                _chkPopUp.Close();
            }
            catch(Exception ex)
            {
                string _msg = ex.Message;
            }
        }
        private void _PopupDelay(object sender, EventArgs e)
        {
            _isPopupShown = false;
            _swipeInPopup.Stop();

            //if (_chkPopUp != null)
            //_chkPopUp.Close();
        }
        private void btnStaffRoster_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                    StaffRoster _StaffRoster = new StaffRoster();
                    _StaffRoster.Owner = this;
                    _StaffRoster.StaffImage.Source = StaffImage.ImageSource;
                    _StaffRoster._StaffPic = _StaffPic;
                    _StaffRoster._UserName = _UserName;
                    _StaffRoster._CurrentLoginDateTime = _CurrentLoginDateTime;
                    _StaffRoster._CompanyName = _CompanyAndSiteName;
                    Mouse.OverrideCursor = null;
                    _StaffRoster.ShowDialog();
                }
                else
                {
                    MessageBox.Show("ACCESS DENIED! \n\nYou MUST be Admin user to access this Screen!", "Dashboard", MessageBoxButton.OK, MessageBoxImage.Stop);
                    this.Cursor = Cursors.Arrow;
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnAutoEmailReport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;
                ReportEmailConfig mainReport = new ReportEmailConfig();

                mainReport._CompanyName = _CompanyAndSiteName;
                mainReport._UserName = this._UserName;
                mainReport._CurrentLoginDateTime = this._CurrentLoginDateTime;
                mainReport._StaffPic = this._StaffPic;
                mainReport.StaffImage.Source = StaffImage.ImageSource;

                mainReport.Owner = this;
                this.Cursor = Cursors.Arrow;
                mainReport.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }
    }
}
