﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymMembership.DTO
{
    public class UserLoginDTO
    {
        //LOGIN TABLE FIELDS
        public int LoginID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int UserRole { get; set; }
        public int UserStatus { get; set; }
        public string CompanyName { get; set; }
        public string SiteName { get; set; }

        //STAFF TABLE FIELDS    
        public int StaffID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public int Gender { get; set; }
        public string Street { get; set; }
        public string Suburb { get; set; }
        public int PostalCode { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public int IsMember { get; set; }
        public int MemberID { get; set; }
        public int StaffRoleID { get; set; }
        public byte[] StaffPic { get; set; }
        public int SiteID { get; set; }
        public bool isAppTimeOutNeverExpire { get; set; }
        public string EmailID { get; set; }
    }
}
