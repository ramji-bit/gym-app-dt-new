﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymMembership.DTO
{
    public class MemberDetailsDTO
    {
        public int MemberId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateofBirth { get; set; }
        public int Gender { get; set; }
        public string Street { get; set; }
        public string Suburb { get; set; }
        public string City { get; set; }
        public int PostalCode { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string Notes { get; set; }
        public long CardNo { get; set; }
        public string WorkPhone { get; set; }
        public string Occupation { get; set; }
        public string Organisation { get; set; }
        public string InvolvementType { get; set; }
        public string PersonalTrainer { get; set; }
        public string EmailId { get; set; }
        public int CompanyID { get; set; }
        public string MemberProfilePhotoPath { get; set;}
        public byte[] MemberPic { get; set; }
        public string appVersion { get; set; }
        public int cardReplicate { get; set; } = 0;
        public string technoGymPermanentToken { get; set; } = "";
        public string technoGymEnv { get; set; } = "TEST";
    }
    public class MembershipDTO
    {
        public string MS_ProgramGroup { get; set; }
        public string MS_Program { get; set; }
        public DateTime MS_ProgramSdate { get; set; }
        public DateTime MS_ProgramEdate { get; set; }
        public int MS_Price { get; set; }
        public string MS_Condition { get; set; }
        public long MS_CardNo { get; set; }
        public int MS_SignUpFee { get; set; }
        public int MS_NoOfVisits { get; set; }
    }

    public class MembersAddlInfoDTO
    {
        public int MemberId{ get; set; }
        public string FirstName { get; set; }
        public string HomePhone{ get; set; }
        public string CellPhone { get; set; }
        public string WorkPhone { get; set; }

        public string AlternateContactName { get; set; }
        public string AlternateHomePhone { get; set; }
        public string AlternateCellPhone { get; set; }
        public string AlternateWorkPhone { get; set; }

        public string MedCondition { get; set; }
        public string DoctorName { get; set; }
        public string DoctorPhone { get; set; }
        public string Medication { get; set; }

        public DateTime JoiningDt { get; set; }
        public string ReferredBy{ get; set; }
        public int JoiningPromotion { get; set; }
        public int ClientMgr { get; set; }

        public string LastUpdate { get; set; }
        public string PostalAddress { get; set; }
        public string BillingAddress { get; set; }
        public int Age { get; set; }

        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsReceiveEmail { get; set; }
        public bool IsReceiveLetter { get; set; }
        public bool IsReceiveSMS { get; set; }
        public bool IsReceivePushNotification { get; set; }
        public string MedRec_FileName { get; set; }
        public string MedRec_Ext { get; set; }
        public string MedRec_ContentType { get; set; }
        public byte[] MedRec_FileData { get; set; }
    }
    public class MemberBankDetailsDTO
    {
        public int BankProcessId { get; set; }
        public string BankPayerName { get; set; }
        public string BankPayerAccount { get; set; }
        public string BankPayerParticulars { get; set; }
        public int BillingCompanyId { get; set; }
        public Decimal MaximumMoneyCollected { get; set; }
        public string CreditCardName { get; set; }
        public string CreditCardNumber { get; set; }
        public DateTime CreditCardExpDate { get; set; }
        public int CheckCardType { get; set; }
        public int CardType { get; set; }
        public DateTime OverDueDeadLine { get; set; }
        public int MemberId { get; set; }
        public int CompanyId { get; set; }
    }

    public class ProspectsDTO
    {
        public int ProspectID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateofBirth { get; set; }
        public int Gender { get; set; }
        public string Street { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public int PostalCode { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string Notes { get; set; }
        public string AssignedTo { get; set; }
        public int ContactMethod { get; set; }
        public string SourcePromotion { get; set; }
        public int ReferredBy { get; set; }
        public string FitnessGoal { get; set; }
        public string PreviousGym { get; set; }
        public string EmailId { get; set; }
        public string LeadStrength { get; set; }
        public DateTime LeadCreated { get; set; }
        public int Visits { get; set; }
        public bool NotInterested { get; set; }
        public bool Convert { get; set; }
        public bool Exercising { get; set; }
        public int CompanyID { get; set; }
        public int SiteID { get; set; }
        public bool isUpdate { get; set; }
        public int AssignedToStaffID { get; set; }
        public string Door { get; set; }
    }
}


