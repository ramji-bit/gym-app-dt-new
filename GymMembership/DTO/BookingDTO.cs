﻿using System;
using System.Data;

namespace GymMembership.DTO
{
    public class BookingTransDTO
    {
        //public int BookingID { get; set; }
        public string ResourceTypename { get; set; }
        public int ResourceTypeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public DateTime Date { get; set; }
        public string Time { get; set; }
        public string RoomOrTrainer { get; set; }
        public int MemberID { get; set; }
        public int CompanyID { get; set; }
        public int SiteID { get; set; }
        public string BookingStatus { get; set; }
        public int WaitListNo { get; set; }
        public int FacilityID { get; set; }
        public int SessionNumber { get; set; }
    }
    public class BookingResourceDTO
    {
        public string _FacilityName { get; set; }
        public int _FacilityStatus { get; set; }
        public int _ResourceTypeID { get; set; }
        public int _FacilityID { get; set; }
        public int _MaxMembers { get; set; }
        public int _MaxWaitList { get; set; }
        public int _NumTimeSlots { get; set; }
        public decimal _ChargePerSlot { get; set; }
        public int _isUpdate { get; set; }
        public DataTable _dtSlots { get; set; }
    }
}
