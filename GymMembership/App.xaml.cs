﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Threading;
using GymMembership.Comman;
using GymMembership.Accounts;
namespace GymMembership
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public string UserName;
        public int UserRole;
        public int CompanyID;
        public int SiteID;
        public string AppPath;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            //Set default date formate forthe application.
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Thread.CurrentThread.CurrentCulture.Name);
            ci.DateTimeFormat.ShortDatePattern = Constants.DEFAULT_DATE_FORMAT;
            ci.DateTimeFormat.DateSeparator = "/";
            Thread.CurrentThread.CurrentCulture = ci;
            //MessageBox.Show("Path : " + AppPath);

            if (System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Count() > 1)
            {
                MessageBox.Show("Application is already Running!", "REDA Management Software", MessageBoxButton.OK, MessageBoxImage.Information);
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }

            //Starting point for application
            Login _login = new Login();
            _login.Show();
        }
    }
}
   
