﻿using System;
using System.Collections;
using System.Text.RegularExpressions;
using System.Data;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Configuration;

using GymMembership.Comman;
using GymMembership.BAL;
using Paychoice_Payment;

namespace GymMembership.MemberInfo
{
    /// <summary>
    /// Interaction logic for ExpireMembership.xaml
    /// </summary>
    public partial class ExpireMembership 
    {
        public int _MemberNo;
        public int _CompanyID;
        public int _SiteID;
        public int _ProgrammeID;
        public int _UniqueID;

        public ExpireMembership()
        {
            InitializeComponent();
            btnUnexpire.IsEnabled = false;
            btnUnexpire.Opacity = 0.5;
        }

        private void btnExpireMsCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnExpireMsOK_Click(object sender, RoutedEventArgs e)
        {
            if (validateData())
            {
                double _CancellationFee = Convert.ToDouble(txtCancelationFee.Text.Trim());
                if (_CancellationFee > 0)
                {
                    //if (MessageBox.Show("Cancellation Fee is $" + _CancellationFee.ToString("###.00") + "! Is that Correct?", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    //{
                    //    return;
                    //}
                }
                else if (_CancellationFee < 0)
                {
                    return;
                }

                this.Cursor = Cursors.Wait;
                btnExpireMsOK.Cursor = Cursors.Wait;
                DateTime edit_fromdate;

                if (dtCancelDate.SelectedDate != null)
                    edit_fromdate = Convert.ToDateTime(dtCancelDate.SelectedDate);
                else
                    edit_fromdate = Convert.ToDateTime(dtCancelDate.Text);

                Hashtable htguid = new Hashtable();
                DataManager dal = new DataManager();

                htguid.Clear();
                htguid.Add("@MemberID", _MemberNo);
                htguid.Add("@Programme", _ProgrammeID);
                htguid.Add("@CompanyID", _CompanyID);
                htguid.Add("@SiteID", _SiteID);
                htguid.Add("@UniqueID", _UniqueID);
                DataSet paychoice_GUID_List = dal.ExecuteProcedureReader("FetchSubscriptionID_Memberships_NEW", htguid);

                string _Result = ""; string _ResultArchive = "";
                bool _isArchived = false;
                if (paychoice_GUID_List != null)
                {
                    if (paychoice_GUID_List.Tables[0].DefaultView.Count > 0)
                    {
                        string subscription_GUID = paychoice_GUID_List.Tables[0].Rows[0]["Subscription_Guid"].ToString();
                        string customer_GUID = paychoice_GUID_List.Tables[0].Rows[0]["Customer_GUID"].ToString();

                        if (subscription_GUID != "")
                        {
                            _Result = paychoice.Delete_Payment_Schedule(subscription_GUID, edit_fromdate);
                            
                            //paychoice.Update_Status_PaychoiceScheduledItems(subscription_GUID, customer_GUID, DateTime.Now, "EXPIRED");
                            paychoice.Update_Status_PaychoiceScheduledItems(subscription_GUID, customer_GUID, edit_fromdate, "EXPIRED");

                            if (edit_fromdate.Date <= DateTime.Now.Date)
                            {
                                _Result = paychoice.Archive_Subscription(subscription_GUID);
                                _isArchived = true;
                            }
                            if ((_Result.ToUpper() == "TRUE") || (_Result.ToUpper() == "SUCCESS"))
                                _Result = "Success";
                        }
                        //MessageBox.Show("Membership Expire: \n\n" + _Result, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        MessageBox.Show("Membership Expire: COMPLETED!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        if (_Result.StartsWith("The Payment schedule has been"))
                            _Result = "SUCCESS";
                    }
                    else
                    {
                        MessageBox.Show("Subscription ID - NOT Available!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        this.Cursor = Cursors.Arrow;
                        btnExpireMsOK.Cursor = Cursors.Hand;
                        _Result = "SUCCESS";
                        //return;
                    }
                }

                int i = 0;
                SqlHelper _sql = new SqlHelper();
                if (_sql._ConnOpen == 0) return;

                if (_Result.ToUpper().Trim() == "SUCCESS")
                {
                    htguid.Clear();
                    htguid.Add("@MemberID", _MemberNo);
                    htguid.Add("@Programme", _ProgrammeID);
                    htguid.Add("@CompanyID", _CompanyID);
                    htguid.Add("@SiteID", _SiteID);
                    htguid.Add("@EndDate", edit_fromdate);
                    htguid.Add("@accFile", 1);
                    htguid.Add("@UniqueID", _UniqueID);
                    if (_isArchived)
                    {
                        htguid.Add("@isArchived", 1);
                    }
                    i = _sql.ExecuteQuery("UpdateMembershipAsExpired", htguid);
                }

                if (i != 0)
                {
                    Constants.Create_ACCESS_DAT_File(false, _MemberNo);

                    htguid.Clear();
                    htguid.Add("@MemberID", _MemberNo);
                    htguid.Add("@Programme", _ProgrammeID);
                    htguid.Add("@CompanyID", _CompanyID);
                    htguid.Add("@SiteID", _SiteID);
                    htguid.Add("@EndDate", edit_fromdate);
                    htguid.Add("@accFile", 0);
                    htguid.Add("@UniqueID", _UniqueID);
                    htguid.Add("@expiredMembershipPaychoiceStatus", 1);

                    i = _sql.ExecuteQuery("UpdateMembershipAsExpired", htguid);

                    if (i != 0)
                    {
                        htguid.Clear();
                        string _Notes = txtExpireMsReson.Text.Trim().ToUpper() + "[EXPIRY-Effective Dt: " + edit_fromdate.ToString("dd MMM yyyy") + " - CANCELATION FEE: $" + _CancellationFee.ToString("##00.00") + " ]";
                        htguid.Add("@MemberID", _MemberNo);
                        htguid.Add("@CompanyID", _CompanyID);
                        htguid.Add("@SiteID", _SiteID);
                        htguid.Add("@Reason", _Notes);
                        htguid.Add("@StaffMember", ConfigurationManager.AppSettings["LoggedInUser"].ToString());
                        i = _sql.ExecuteQuery("UpdateNotes", htguid);

                        if (_CancellationFee > 0)
                        {
                            int InvNo = RAISE_CANCELATION_FEE_INVOICE();
                            if (InvNo != -1)
                            {
                                RAISE_CANCELATION_FEE_INVOICE_DETAILS(InvNo);
                            }
                        }
                    }

                    if (i != 0)
                    {
                        if (_Result == "")
                        {
                            MessageBox.Show("Membership Expire: Completed!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                }

                this.Cursor = Cursors.Arrow;
                btnExpireMsOK.Cursor = Cursors.Hand;
                this.Close();
            }
        }

        private bool validateData()
        {
            if (dtCancelDate.Text.Trim() == "")
            {
                MessageBox.Show("Select effective Cancellation Date!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                dtCancelDate.Focus();
                return false;
            }

            FormUtilities _fUtil = new FormUtilities();
            if (!_fUtil.CheckDate(dtCancelDate.Text))
            {
                MessageBox.Show("Enter Cancellation Date in correct format! [dd/MM/yyyy]", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                dtCancelDate.Focus();
                return false;
            }

            DateTime _dt = DateTime.Parse(dtCancelDate.Text);
            if (_dt.Date < DateTime.Now.Date)
            {
                MessageBox.Show("Cancellation Date CANNOT be less than Today!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                dtCancelDate.Focus();
                return false;
            }
            if (txtExpireMsReson.Text.Trim() == "")
            {
                MessageBox.Show("Please enter REASON for Cancellation!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtExpireMsReson.Focus();
                return false;
            }

            if (txtCancelationFee.Text.Trim() == "")
            {
                MessageBox.Show("Enter Cancellation Fee! (enter 0.00 if NOT applicable)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtCancelationFee.Text = "0.00";
                txtCancelationFee.Focus();
                return false;
            }
            if (!Regex.IsMatch(txtCancelationFee.Text, @"^\d{1,3}(\.\d{1,2})?$"))
            {
                txtCancelationFee.Text = "0.00";
                MessageBox.Show("Please enter Cancellation Fee in Numbers only (upto 2 decimal places)\n(OR)\nCharge should be Less than 999.99!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtCancelationFee.Focus();
                return false;
            }
            if (Convert.ToDecimal(txtCancelationFee.Text) < 0)
            {
                txtCancelationFee.Text = "0.00";
                MessageBox.Show("Cancellation Fee CANNOT be Negative!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtCancelationFee.Focus();
                return false;
            }

            return true;
        }

        private int RAISE_CANCELATION_FEE_INVOICE()
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                int _memberInvoiceNo = GetMembershipInvoiceNo();
                if (_memberInvoiceNo != -1)
                {
                    DateTime _currentDate = DateTime.Now;

                    decimal _invoiceAmount = Convert.ToDecimal(txtCancelationFee.Text.ToString());
                    decimal _amountPaid = 0m;
                    decimal _amountOutStanding = Convert.ToDecimal(_invoiceAmount - _amountPaid);
                    int _invoiceMemberId = _MemberNo;
                    string _invoiceStatus = "Pending";
                    _memberDetailsBal.raise_Invoice(_currentDate, _memberInvoiceNo, _invoiceAmount, _amountPaid, _amountOutStanding, _invoiceMemberId, _invoiceStatus, "InvoiceNo");
                    return _memberInvoiceNo;
                }
                return -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return -1;
            }
        }

        private void RAISE_CANCELATION_FEE_INVOICE_DETAILS(int _InvNo)
        {
            DataTable _InvoiceTable = new DataTable("InvoiceDetails");
            PointOfSaleBAL _InvoiceDetailsBAL = new PointOfSaleBAL();

            _InvoiceTable.Columns.Add("InvoiceNo", typeof(int));
            _InvoiceTable.Columns.Add("MemberNo", typeof(int));
            _InvoiceTable.Columns.Add("InvoiceDetails", typeof(string));
            _InvoiceTable.Columns.Add("Amount", typeof(decimal));
            _InvoiceTable.Columns.Add("Status", typeof(string));
            _InvoiceTable.Columns.Add("ProdSale", typeof(char));
            _InvoiceTable.Columns.Add("ProdID", typeof(int));
            _InvoiceTable.Columns.Add("ProdQty", typeof(int));

            DataRow dR = _InvoiceTable.NewRow();
            decimal _invoiceAmount = Convert.ToDecimal(txtCancelationFee.Text.ToString());

            dR["InvoiceNo"] = _InvNo;
            dR["MemberNo"] = _MemberNo;
            dR["InvoiceDetails"] = "MEMBERSHIP CANCELLATION FEE";
            dR["Amount"] = _invoiceAmount;
            dR["Status"] = "Pending";
            dR["ProdSale"] = 'C';
            dR["ProdID"] = 0;
            dR["ProdQty"] = 0;

            _InvoiceTable.Rows.Add(dR);
            _InvoiceDetailsBAL.SavePOSInvoiceDetails(_InvoiceTable);
        }

        private int GetMembershipInvoiceNo()
        {
            try
            {
                int _invoiceNo;

                _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                DataSet ds = _memberDetailsBal.get_LastUsedNoByCompanySite("InvoiceNo", _CompanyID, _SiteID);
                if (ds != null)
                {
                    _invoiceNo = Convert.ToInt32(ds.Tables[0].Rows[0]["LastUsedNo"].ToString().Trim());
                    return _invoiceNo;
                }
                else
                {
                    MessageBox.Show("No Data! (Invoice Number AutoGenerated)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return -1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return -1;
            }
        }
    }
}
