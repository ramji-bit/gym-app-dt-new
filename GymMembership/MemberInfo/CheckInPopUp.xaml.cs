﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Drawing.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace GymMembership.MemberInfo
{
    /// <summary>
    /// Interaction logic for CheckInPopUp.xaml
    /// </summary>
    public partial class CheckInPopUp : Window
    {
        public CheckInPopUp(int nOrder = 1, string _MemberName="", string _CardNumber="")
        {
            InitializeComponent();
            this.Left = SystemParameters.PrimaryScreenWidth - this.Width - 10;
            this.Top = SystemParameters.PrimaryScreenHeight - (this.Height * nOrder) - 50;

            lblMemberName.Content = _MemberName;
            lblCardNumber.Content = _CardNumber;
            if (_data != null)
            {
                if (_data.Length > 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_data, 0, _data.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    Member_Image_Img.Source = bi;
                }
            }
        }
        public byte[] _data;
        public string _MemberName;
        public string _CardNumber;
        private void Popup_Loaded(object sender, RoutedEventArgs e)
        {
            lblMemberName.Content = _MemberName;
            lblCardNumber.Content = _CardNumber;
            if (_data != null)
            {
                if (_data.Length > 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_data, 0, _data.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    Member_Image_Img.Source = bi;
                }
            }
        }
    }
}
