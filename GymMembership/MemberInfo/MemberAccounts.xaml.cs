﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;

using Paychoice_Payment;
using GymMembership.Communications;
using GymMembership.Task;
using GymMembership.BAL;

namespace GymMembership.MemberInfo
{
    /// <summary>
    /// Interaction logic for MemberAccounts.xaml
    /// </summary>
    public partial class MemberAccounts 
    {
        public string _MemberName;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public bool _CanOpenMemberShipNewMember = false;
        public bool _CanOpenBillingBankNewMember = false;
        public bool ExistingMember = false;
        public int GetMemberDetailsMemberId; public long CardNumber;
        public DataSet _dSAccessRights;
        public DataTable dt = new DataTable();
        DataSet DS;
        bool _fromRefreshPaymentButton = false;

        public int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
        public int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
        public int _RowEdited = -1; bool _loaded = true;
        bool _offlineErrorMsgPayChoice = false;
        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;

            if (_MemberName != null)
            {
                if (_MemberName.Trim() != "")
                {
                    Heading.Text = Heading.Text + " - " + _MemberName;
                }
            }
            //loadStaffPhoto(_StaffPic);
        }
        private void loadStaffPhoto(byte[] _StaffPic)
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }
        public MemberAccounts()
        {
            InitializeComponent();
        }

        public int MemberID, CompanyID, SiteID;
        
        Hashtable ht = new Hashtable();
        string ProcName;
        private void Accounts_Loaded(object sender, RoutedEventArgs e)
        {
            //Change this to BAL/DAL methods;
            try
            {
                SqlHelper _SqlHelper = new SqlHelper();
                if (_SqlHelper._ConnOpen == 0) return;

                SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
                if (MemberID != 0)
                {
                    CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                    //OverView TAB
                    ht.Add("@MemberID", MemberID);
                    ht.Add("@CompanyID", CompanyID);
                    ht.Add("@SiteID", SiteID);

                    ProcName = "GetAccountOverviewForMember";
                    DS = _SqlHelper.ExecuteProcedure(ProcName, ht);
                    gvOverview.ItemsSource = null;
                    gvOverview.ItemsSource = DS.Tables[0].AsDataView();

                    //Payments TAB
                    ProcName = "GetAccountPaymentsForMember";
                    DS = null;
                    DS = _SqlHelper.ExecuteProcedure(ProcName, ht);
                    gvPayments.ItemsSource = null;
                    gvPayments.ItemsSource = DS.Tables[0].AsDataView();

                    //Charges TAB
                    ProcName = "GetAccountInvoiceForMember";
                    DS = null;
                    DS = _SqlHelper.ExecuteProcedure(ProcName, ht);
                    gvInvoice.ItemsSource = null;
                    gvInvoice.ItemsSource = DS.Tables[0].AsDataView();

                    //Product Sale TAB
                    ProcName = "GetAccountProdSaleForMember";
                    DS = null;
                    DS = _SqlHelper.ExecuteProcedure(ProcName, ht);
                    gvProductsSold.ItemsSource = null;
                    gvProductsSold.ItemsSource = DS.Tables[0].AsDataView();

                    //Payment Schedule TAB
                    ProcName = "GetAccountPaymentScheduleForMember";
                    DS = null;
                    DS = _SqlHelper.ExecuteProcedure(ProcName, ht);
                    gvPaymentSchedule.ItemsSource = null;
                    if (DS != null)
                    {
                        if (DS.Tables.Count > 0)
                        {
                            if (DS.Tables[0].DefaultView.Count > 0)
                            {
                                gvPaymentSchedule.ItemsSource = DS.Tables[0].AsDataView();
                                if (DS.Tables[0].DefaultView.Count > 0)
                                {
                                    btnRefresh.IsEnabled = true;
                                }
                                else
                                {
                                    btnRefresh.IsEnabled = false;
                                }
                            }
                        }
                    }

                    tcMemberAccounts.SelectedIndex = 1;
                    tcMemberAccounts.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error! " + ex.Message);
                if ((ex.Message.ToUpper().StartsWith("CONNECTION TIMEOUT")) ||(ex.Message.ToUpper().Contains("TIMEOUT")))
                {
                    MessageBox.Show(
                                        "Error! Unable to get Paychoice Status!\n\nTIMEOUT EXPIRED! Please try again!!\n\n",
                                        this.Title, MessageBoxButton.OK, MessageBoxImage.Information
                                    );
                }
                else
                {
                    if (!_offlineErrorMsgPayChoice)
                    {
                        MessageBox.Show(
                                            "Error! Unable to get Paychoice Status!\n\nReasons could be:\n\n(1) No Network Connection!\n(2) Offline DB is NOT set!\n" +
                                            "(3) TIMEOUT EXPIRED (Or) Database Connection Settings issue!\n\n",
                                            this.Title, MessageBoxButton.OK, MessageBoxImage.Information
                                        );
                        _offlineErrorMsgPayChoice = true;
                    }
                }
                Mouse.OverrideCursor = null;
                return;
            }
        }
        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void btnShowMembership_Click(object sender, RoutedEventArgs e)
        {
            if (!ExistingMember)
            {
                if (_CanOpenMemberShipNewMember)
                {
                    Memberships _mDetails = new Memberships();
                    _mDetails.ExistingMember = ExistingMember;
                    _mDetails.CardNumber = CardNumber;
                    _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    _mDetails._MemberName = _MemberName;
                    _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    _mDetails._CompanyName = _CompanyName;
                    _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                    _mDetails._SiteName = _SiteName;
                    _mDetails._StaffPic = _StaffPic;
                    _mDetails._UserName = _UserName;
                    _mDetails._CompanyID = _CompanyID;
                    _mDetails._SiteID = _SiteID;
                    _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                    _mDetails.StaffImage.Source = StaffImage.Source;
                    _mDetails._dSAccessRights = _dSAccessRights;
                    _mDetails.dt = this.dt;
                    _mDetails.Owner = this.Owner;

                    this.Close();
                    _mDetails.ShowDialog();
                }
                else
                {
                    System.Windows.MessageBox.Show("New Member Details have to be Created First!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            else
            {
                Memberships _mDetails = new Memberships();
                _mDetails.ExistingMember = ExistingMember;
                _mDetails.CardNumber = CardNumber;
                _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _mDetails._MemberName = _MemberName;
                _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _mDetails._CompanyName = _CompanyName;
                _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                _mDetails._SiteName = _SiteName;
                _mDetails._StaffPic = _StaffPic;
                _mDetails._UserName = _UserName;
                _mDetails._CompanyID = _CompanyID;
                _mDetails._SiteID = _SiteID;
                _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                _mDetails.StaffImage.Source = StaffImage.Source;
                _mDetails._dSAccessRights = _dSAccessRights;
                _mDetails.dt = this.dt;
                _mDetails.Owner = this.Owner;

                this.Close();
                _mDetails.ShowDialog();
            }
        }

        private void btnAccounts_Click(object sender, RoutedEventArgs e)
        {
            //SAME WINDOW; NO ACTION
        }

        private void btnMemberBillingDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ExistingMember)
                {
                    if (dt.Rows.Count > 0)
                    { 
                        this.Cursor = System.Windows.Input.Cursors.Wait;
                        PaymentDetails PD = new PaymentDetails(GetMemberDetailsMemberId, _CompanyID, _SiteID, dt);
                        this.Cursor = System.Windows.Input.Cursors.Arrow;

                        PD._CurrentLoginDateTime = _CurrentLoginDateTime;
                        PD._CompanyName = _CompanyName;
                        PD._SiteName = _SiteName;
                        PD._UserName = _UserName;
                        PD._StaffPic = _StaffPic;
                        PD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                        PD.ExistingMember = ExistingMember;
                        PD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                        PD._MemberName = _MemberName;
                        PD.Member_Image_Img.Source = Member_Image_Img.Source;
                        PD.StaffImage.Source = StaffImage.Source;
                        PD._dSAccessRights = _dSAccessRights;
                        PD.dt = this.dt;

                        PD.Owner = this.Owner;
                        this.Close();
                        PD.ShowDialog();
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Membership has to be Created!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnMemberExtraDetails_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                ExtraDetails extra = new ExtraDetails();
                extra.strExtraDetailsMemberID = GetMemberDetailsMemberId;

                extra._CurrentLoginDateTime = _CurrentLoginDateTime;
                extra._CompanyName = _CompanyName;
                extra._SiteName = _SiteName;
                extra._UserName = _UserName;
                extra._StaffPic = _StaffPic;
                extra._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                extra.ExistingMember = ExistingMember;
                extra.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                extra._MemberName = _MemberName;
                extra.Member_Image_Img.Source = Member_Image_Img.Source;
                extra.StaffImage.Source = StaffImage.Source;
                extra._dSAccessRights = _dSAccessRights;
                extra.dt = this.dt;

                extra.Owner = this.Owner;
                this.Close();
                extra.ShowDialog();
            }
        }

        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                Tasks Tsk = new Tasks();
                Tsk.strMemberNo = GetMemberDetailsMemberId;
                Tsk.Owner = this.Owner;
                Tsk.SetAccessRightsDS(_dSAccessRights);

                Tsk._CurrentLoginDateTime = _CurrentLoginDateTime;
                Tsk._CompanyName = _CompanyName;
                Tsk._SiteName = _SiteName;
                Tsk._UserName = _UserName;
                Tsk._StaffPic = _StaffPic;
                Tsk._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                Tsk.ExistingMember = ExistingMember;
                Tsk.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                Tsk._MemberName = _MemberName;
                Tsk.Member_Image_Img.Source = Member_Image_Img.Source;
                Tsk.StaffImage.Source = StaffImage.Source;
                Tsk._dSAccessRights = _dSAccessRights;
                Tsk.dt = this.dt;

                this.Close();
                Tsk.ShowDialog();
            }
        }

        private void btnCommunications_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Communication _Comm = new Communication();
                _Comm.Owner = this.Owner;
                _Comm.SetAccessRightsDS(_dSAccessRights);
                this.Cursor = System.Windows.Input.Cursors.Arrow;

                _Comm._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Comm._CompanyName = _CompanyName;
                _Comm._SiteName = _SiteName;
                _Comm._UserName = _UserName;
                _Comm._StaffPic = _StaffPic;
                _Comm._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Comm.ExistingMember = ExistingMember;
                _Comm.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Comm._MemberName = _MemberName;
                _Comm.Member_Image_Img.Source = Member_Image_Img.Source;
                _Comm.StaffImage.Source = StaffImage.Source;
                _Comm._dSAccessRights = _dSAccessRights;
                _Comm.dt = this.dt;

                this.Close();
                _Comm.ShowDialog();
            }
        }

        private void btnNotes_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Notes _Notes = new Notes();
                _Notes.Owner = this.Owner;

                _Notes._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Notes._CompanyName = _CompanyName;
                _Notes._SiteName = _SiteName;
                _Notes._UserName = _UserName;
                _Notes._StaffPic = _StaffPic;
                _Notes._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Notes.ExistingMember = ExistingMember;
                _Notes.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Notes._MemberName = _MemberName;
                _Notes.Member_Image_Img.Source = Member_Image_Img.Source;
                _Notes.StaffImage.Source = StaffImage.Source;
                _Notes._dSAccessRights = _dSAccessRights;
                _Notes.dt = this.dt;

                this.Cursor = System.Windows.Input.Cursors.Arrow;
                this.Close();
                _Notes.ShowDialog();
            }
        }

        private void btnPersonalTraining_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                PersonalTraining _pt = new PersonalTraining();
                _pt.Owner = this.Owner;

                _pt._CurrentLoginDateTime = _CurrentLoginDateTime;
                _pt._CompanyName = _CompanyName;
                _pt._SiteName = _SiteName;
                _pt._UserName = _UserName;
                _pt._StaffPic = _StaffPic;
                _pt._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _pt.ExistingMember = ExistingMember;
                _pt.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _pt._MemberName = _MemberName;
                _pt.Member_Image_Img.Source = Member_Image_Img.Source;
                _pt.StaffImage.Source = StaffImage.Source;
                _pt._dSAccessRights = _dSAccessRights;
                _pt.dt = this.dt;

                this.Close();
                _pt.ShowDialog();
            }
        }
        private void tcMemberAccounts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (tcMemberAccounts.SelectedIndex == 0)
                {
                    if (!_loaded)
                    {
                        RefreshPaymentStatus();
                        Cursor = Cursors.Arrow;
                        btnRefresh.Cursor = Cursors.Hand;

                        _loaded = true;
                    }
                }
                else
                {
                    _loaded = false;
                    _offlineErrorMsgPayChoice = false;
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error! " + ex.Message);
                if ((ex.Message.ToUpper().StartsWith("CONNECTION TIMEOUT")) || (ex.Message.ToUpper().Contains("TIMEOUT")))
                {
                    MessageBox.Show(
                                        "Error! Unable to get Paychoice Status!\n\nTIMEOUT EXPIRED! Please try again!!\n\n",
                                        this.Title, MessageBoxButton.OK, MessageBoxImage.Information
                                    );
                }
                else
                {
                    if (!_offlineErrorMsgPayChoice)
                    {
                        MessageBox.Show(
                                            "Error! Unable to get Paychoice Status!\n\nReasons could be:\n\n(1) No Network Connection!\n(2) Offline DB is NOT set!\n" +
                                            "(3) TIMEOUT EXPIRED (Or) Database Connection Settings issue!\n\n",
                                            this.Title, MessageBoxButton.OK, MessageBoxImage.Information
                                        );
                        _offlineErrorMsgPayChoice = true;
                    }
                }
                Mouse.OverrideCursor = null;
                return;
            }
        }
        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberDetails mD = new MemberDetails();
                mD.Owner = this.Owner;

                mD._CurrentLoginDateTime = _CurrentLoginDateTime;
                mD._CompanyName = _CompanyName;
                mD._SiteName = _SiteName;
                mD._UserName = _UserName;
                mD._StaffPic = _StaffPic;
                mD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mD.ExistingMember = ExistingMember;
                mD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mD._MemberName = _MemberName;
                mD.Member_Image_Img.Source = Member_Image_Img.Source;
                mD.StaffImage.Source = StaffImage.Source;
                mD._dSAccessRights = _dSAccessRights;
                mD.dt = this.dt;

                this.Close();
                mD.ShowDialog();
            }
        }
        private void Skip_Delete_ScheduleAmount(object sender, RoutedEventArgs e)
        {
            if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
            {
                SqlHelper _sql = new SqlHelper();
                if (_sql._ConnOpen == 0) return;

                int i = gvPaymentSchedule.SelectedIndex;
                if (i >= 0)
                {
                    //MessageBox.Show("row : " + i + " PaymentItemGUID : " +  DS.Tables[0].DefaultView[i]["PaymentScheduleItemGUID"].ToString());
                    if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "OPEN")
                    {
                        MessageBox.Show("Cannot Skip/Void Item in 'Open (Processing)' Status!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "PROCESSED")
                    {
                        MessageBox.Show("Cannot Skip/Void Item in 'Processed' Status!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "VOIDED")
                    {
                        MessageBox.Show("Cannot Skip/Void Item in 'Voided' Status!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "APPROVED")
                    {
                        MessageBox.Show("Cannot Skip/Void Item in 'Approved' Status!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "PROCESSING")
                    {
                        MessageBox.Show("Cannot Skip/Void Item in 'Processing' Status!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "DISHONOURED")
                    {
                        if (DS.Tables[0].DefaultView[i]["InvMainStatus"].ToString().ToUpper() == "OPEN")
                        {
                            MessageBox.Show("Cannot Skip/Void Item in 'Dishonoured' Status!\nUse 'Make Payment' instead", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            return;
                        }
                        else
                        {
                            MessageBox.Show("Cannot Skip/Void Item in 'Dishonoured' Status!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            return;
                        }
                    }
                    else if (DS.Tables[0].DefaultView[i]["ItemDesc"].ToString().ToUpper() == "DISHONOURRECOVERYSERVICESFEE")
                    {
                        MessageBox.Show("Cannot Skip/Void Item 'Dishonour Fee'!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }

                    //CALL SKIP_SINGLE_ITEM PROCEDURE::RR:19/07
                    string _reasonSuffix = " - [SKIP/DELETE : Schedule Date: " + DS.Tables[0].DefaultView[i]["PaymentScheduledDate"].ToString() + " - " + DS.Tables[0].DefaultView[i]["Remarks"].ToString() + " ]";
                    string _subscriptionID = DS.Tables[0].DefaultView[i]["SubscriptionGUID"].ToString();
                    string _paymentscheduleID = DS.Tables[0].DefaultView[i]["PaymentScheduleItemGUID"].ToString();
                    string _customerGUID = DS.Tables[0].DefaultView[i]["customerGUID"].ToString();
                    string _result = ""; string _inputAmendReason = null;

                    _inputAmendReason = Interaction.InputBox("Enter Reason for Skip Payment!", "Skip/Delete Payment", "");
                    if ((_inputAmendReason == "") || (_inputAmendReason == null))
                    {
                        MessageBox.Show("Reason cannot be blank!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    _inputAmendReason = _inputAmendReason.ToUpper();
                    _inputAmendReason = _inputAmendReason + "" + _reasonSuffix;

                    Cursor = Cursors.Wait;
                    _result = paychoice.Skip_Delete_Single_Payment_Schedule(_subscriptionID, _paymentscheduleID);
                    Paychoice_Parameter _parameter = new Paychoice_Parameter();
                    _parameter.Update_Single_Payment_Schedule_Item_Status(_paymentscheduleID, _subscriptionID, _customerGUID);
                    if (_result.ToUpper() == "SUCCESS")
                    {
                        string _userName = ConfigurationManager.AppSettings["LoggedInUser"].ToString();
                        Hashtable ht = new Hashtable();
                        ht.Add("@CompanyID", _CompanyID);
                        ht.Add("@SiteID", _SiteID);
                        ht.Add("@MemberID", GetMemberDetailsMemberId);
                        ht.Add("@Reason", _inputAmendReason);
                        ht.Add("@StaffMember", _userName);

                        _sql.ExecuteQuery("UpdateNotes", ht);
                    }
                    Cursor = Cursors.Arrow;
                    MessageBox.Show(_result, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //CALL SKIP_SINGLE_ITEM PROCEDURE::RR:19/07
                    RefreshPaymentStatus();
                }
            }
            else
            {
                Cursor = Cursors.Arrow;
                MessageBox.Show("You MUST be Admin User to perform this operation!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void Amend_ScheduleAmount(object sender, RoutedEventArgs e)
        {
            if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
            {
                SqlHelper _sql = new SqlHelper();
                if (_sql._ConnOpen == 0) return;

                int i = gvPaymentSchedule.SelectedIndex;
                if (i >= 0)
                {
                    if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "OPEN")
                    {
                        MessageBox.Show("Cannot Amend Item in 'Open (Processing)' Status!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "PROCESSED")
                    {
                        MessageBox.Show("Cannot Amend Item in 'Processed' Status!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "VOIDED")
                    {
                        MessageBox.Show("Cannot Amend Item in 'Voided' Status!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "APPROVED")
                    {
                        MessageBox.Show("Cannot Amend Item in 'Approved' Status!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "PROCESSING")
                    {
                        MessageBox.Show("Cannot Amend Item in 'Processing' Status!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "DISHONOURED")
                    {
                        MessageBox.Show("Cannot Amend Item in 'Dishonoured' Status!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    ////CALL AMEND_SINGLE_ITEM PROCEDURE::RR:20/07
                    string _reasonSuffix = " - [AMEND : Schedule Date: " + DS.Tables[0].DefaultView[i]["PaymentScheduledDate"].ToString() + " - " + DS.Tables[0].DefaultView[i]["Remarks"].ToString()+ " ]";
                    string _subscriptionID = DS.Tables[0].DefaultView[i]["SubscriptionGUID"].ToString();
                    string _paymentscheduleID = DS.Tables[0].DefaultView[i]["PaymentScheduleItemGUID"].ToString();
                    string _result = ""; string _inputAmendAmount = null; string _inputAmendReason = null;

                    _inputAmendAmount = Interaction.InputBox("Enter Amended Amount", "Amend Payment", "");
                    if ((_inputAmendAmount == "") || (_inputAmendAmount == null))
                    {
                        MessageBox.Show("No Amount entered!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    if (!Regex.IsMatch(_inputAmendAmount, @"^\d{1,7}(\.\d{1,2})?$"))
                    {
                        MessageBox.Show("Amount should be Numeric! (upto 2 decimal places)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    double _amount = Convert.ToDouble(_inputAmendAmount);

                    _inputAmendReason = Interaction.InputBox("Enter Reason for Amendment", "Amend Payment", "");
                    if ((_inputAmendReason == "") || (_inputAmendReason == null))
                    {
                        MessageBox.Show("Reason cannot be blank!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    _inputAmendReason = _inputAmendReason.ToUpper();
                    _inputAmendReason = _inputAmendReason + "" + _reasonSuffix;
                    _result = paychoice.Amend_Single_Payment_Schedule(_subscriptionID, _paymentscheduleID, _amount);
                    if (_result.ToUpper() == "SUCCESS")
                    {
                        string _userName = ConfigurationManager.AppSettings["LoggedInUser"].ToString();
                        Hashtable ht = new Hashtable();
                        ht.Add("@CompanyID", _CompanyID);
                        ht.Add("@SiteID", _SiteID);
                        ht.Add("@MemberID", GetMemberDetailsMemberId);
                        ht.Add("@Reason", _inputAmendReason);
                        ht.Add("@StaffMember", _userName);

                        _sql.ExecuteQuery("UpdateNotes", ht);
                    }
                    MessageBox.Show(_result, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    ////CALL AMEND_SINGLE_ITEM PROCEDURE::RR:20/07
                    RefreshPaymentStatus();
                }
            }
            else
            {
                MessageBox.Show("You MUST be Admin User to perform this operation!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnHarley_Click(object sender, RoutedEventArgs e)
        {
            //////string sGuid = "ef093cdc-6c5e-4c45-9fb4-c62ab1e0eb7b";
            string cGuid = "2c16a889-ce35-4748-a2ac-1749ce15d5e1";
            //string cGuid = "01bb4242-e747-46d4-8a13-a3163554f820";
            Paychoice_Parameter parameter = new Paychoice_Parameter();
            parameter.List_Subscriptions_All(cGuid);
        }

        private void Make_DH_Payment_Cancel_Item(object sender, RoutedEventArgs e)
        {
            if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
            {
                SqlHelper _sql = new SqlHelper();
                if (_sql._ConnOpen == 0) return;

                string _validationMsg = "Pay in Club can be made ONLY for 'Dishonoured' (OPEN) Item!";
                bool _isDH_Open = false;

                int i = gvPaymentSchedule.SelectedIndex;
                if (i >= 0)
                {
                    if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "PROCESSED")
                    {
                        MessageBox.Show(_validationMsg, "Pay in Club", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "VOIDED")
                    {
                        MessageBox.Show(_validationMsg, "Pay in Club", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "APPROVED")
                    {
                        MessageBox.Show(_validationMsg, "Pay in Club", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "PROCESSING")
                    {
                        MessageBox.Show(_validationMsg, "Pay in Club", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "SCHEDULED")
                    {
                        MessageBox.Show(_validationMsg, "Pay in Club", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "OPEN")
                    {
                        MessageBox.Show(_validationMsg, "Pay in Club", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "DISHONOURED")
                    {
                        if (DS.Tables[0].DefaultView[i]["InvMainStatus"].ToString().ToUpper() != "OPEN")
                        {
                            MessageBox.Show(_validationMsg, "Pay in Club", MessageBoxButton.OK, MessageBoxImage.Information);
                            return;
                        }
                        else
                        {
                            _isDH_Open = true;
                        }
                    }

                    if (_isDH_Open)
                    {
                        if (MessageBox.Show("This action WILL Void the Payment Item!\n\nAND Create Payment @ Club! Confirm ?", "Pay In Club", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            //VOID DISHONOURED ITEM, WITH Pay in Club RR:29/AUG/2016
                            string _reasonSuffix = "Pay In Club: Schedule Date: " + DS.Tables[0].DefaultView[i]["PaymentScheduledDate"].ToString() + " - " + DS.Tables[0].DefaultView[i]["Remarks"].ToString() + " ";
                            string _inputAmendReason = null;
                            string _subscriptionID = DS.Tables[0].DefaultView[i]["SubscriptionGUID"].ToString();
                            string _paymentscheduleID = DS.Tables[0].DefaultView[i]["PaymentScheduleItemGUID"].ToString();
                            string _customerGUID = DS.Tables[0].DefaultView[i]["CustomerGUID"].ToString();
                            string _result = "";
                            //decimal _amount = Convert.ToDecimal(DS.Tables[0].DefaultView[i]["Amount"].ToString());
                            decimal _amount = Convert.ToDecimal(DS.Tables[0].DefaultView[i]["InvoiceAmt"].ToString());

                            _inputAmendReason = _reasonSuffix;

                            Cursor = Cursors.Wait;
                            _result = paychoice.Void_Dishonoured_Invoice_with_Alternate_Payment(_paymentscheduleID, _subscriptionID, _customerGUID);

                            if (_result.ToUpper() == "SUCCESS")
                            {
                                string _userName = ConfigurationManager.AppSettings["LoggedInUser"].ToString();
                                Hashtable ht = new Hashtable();
                                ht.Add("@CompanyID", _CompanyID);
                                ht.Add("@SiteID", _SiteID);
                                ht.Add("@MemberID", GetMemberDetailsMemberId);
                                ht.Add("@Reason", _inputAmendReason);
                                ht.Add("@StaffMember", _userName);

                                _sql.ExecuteQuery("UpdateNotes", ht);

                                if (_amount > 0)
                                {
                                    int InvNo = RAISE_PayInClub_INVOICE(_amount);
                                    if (InvNo != -1)
                                    {
                                        RAISE_PayInClub_INVOICE_DETAILS(InvNo, _amount, _inputAmendReason);
                                    }
                                }

                                Comman.Constants.disHonourCheck_Tick(_subscriptionID);
                            }
                            Cursor = Cursors.Arrow;
                            MessageBox.Show(_result, "Pay In Club", MessageBoxButton.OK, MessageBoxImage.Information);
                            //VOID DISHONOURED ITEM, WITH Pay in Club RR:29/AUG/2016
                            RefreshPaymentStatus();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("You MUST be Admin User to perform this operation!", "Pay in Club", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void Make_DH_Payment_thru_Gateway(object sender, RoutedEventArgs e)
        {
            if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
            {
                SqlHelper _sql = new SqlHelper();
                if (_sql._ConnOpen == 0) return;

                string _validationMsg = "Pay via DDC can be made ONLY for 'Dishonoured' (OPEN) Item!";
                bool _isDH_Open = false;

                int i = gvPaymentSchedule.SelectedIndex;
                if (i >= 0)
                {
                    if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "PROCESSED")
                    {
                        MessageBox.Show(_validationMsg, "Pay via DDC", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "VOIDED")
                    {
                        MessageBox.Show(_validationMsg, "Pay via DDC", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "APPROVED")
                    {
                        MessageBox.Show(_validationMsg, "Pay via DDC", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "PROCESSING")
                    {
                        MessageBox.Show(_validationMsg, "Pay via DDC", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "SCHEDULED")
                    {
                        MessageBox.Show(_validationMsg, "Pay via DDC", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "OPEN")
                    {
                        MessageBox.Show(_validationMsg, "Pay in Club", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else if (DS.Tables[0].DefaultView[i]["PaymentStatus"].ToString().ToUpper() == "DISHONOURED")
                    {
                        if (DS.Tables[0].DefaultView[i]["InvMainStatus"].ToString().ToUpper() != "OPEN")
                        {
                            MessageBox.Show(_validationMsg, "Pay via DDC", MessageBoxButton.OK, MessageBoxImage.Information);
                            return;
                        }
                        else
                        {
                            _isDH_Open = true;
                        }
                    }

                    if (_isDH_Open)
                    {
                        if (MessageBox.Show("This action WILL attempt payment again thru Gateway!\n\nConfirm ?", "Pay via DDC", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            //PAY DISHONOURED ITEM, WITH Pay via DDC RR:29/AUG/2016
                            string _reasonSuffix = "[Pay-thru-Gateway: Schedule Date: " + DS.Tables[0].DefaultView[i]["PaymentScheduledDate"].ToString() + " - " + DS.Tables[0].DefaultView[i]["Remarks"].ToString() + " ]";
                            string _inputAmendReason = null;
                            string _subscriptionID = DS.Tables[0].DefaultView[i]["SubscriptionGUID"].ToString();
                            string _paymentscheduleID = DS.Tables[0].DefaultView[i]["PaymentScheduleItemGUID"].ToString();
                            string _customerGUID = DS.Tables[0].DefaultView[i]["CustomerGUID"].ToString();
                            string _result = "";
                            //double _amount = Convert.ToDouble(DS.Tables[0].DefaultView[i]["Amount"].ToString());
                            double _amount = Convert.ToDouble(DS.Tables[0].DefaultView[i]["InvoiceAmt"].ToString());

                            _inputAmendReason = _reasonSuffix;

                            Cursor = Cursors.Wait;
                            _result = paychoice.Pay_Dishonoured_Invoice_with_Alternate_Payment(_paymentscheduleID, _subscriptionID, _customerGUID, _amount);

                            if (_result.ToUpper() == "SUCCESS")
                            {
                                string _userName = ConfigurationManager.AppSettings["LoggedInUser"].ToString();
                                Hashtable ht = new Hashtable();
                                ht.Add("@CompanyID", _CompanyID);
                                ht.Add("@SiteID", _SiteID);
                                ht.Add("@MemberID", GetMemberDetailsMemberId);
                                ht.Add("@Reason", _inputAmendReason);
                                ht.Add("@StaffMember", _userName);

                                _sql.ExecuteQuery("UpdateNotes", ht);

                                Comman.Constants.disHonourCheck_Tick(_subscriptionID);
                            }
                            Cursor = Cursors.Arrow;
                            //MessageBox.Show(_result, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            MessageBox.Show("Payment Initiated thru Gateway!", "Pay via DDC", MessageBoxButton.OK, MessageBoxImage.Information);
                            //PAY DISHONOURED ITEM, WITH Pay via DDC RR:29/AUG/2016
                            RefreshPaymentStatus();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("You MUST be Admin User to perform this operation!", "Pay via DDC", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;
                btnRefresh.Cursor = Cursors.Wait;

                _fromRefreshPaymentButton = true;
                RefreshPaymentStatus();
                //Comman.Constants.disHonourCheck_Tick();
                _fromRefreshPaymentButton = false;

                Cursor = Cursors.Arrow;
                btnRefresh.Cursor = Cursors.Hand;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error during status update! - " + ex.Message,this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
                Cursor = Cursors.Arrow;
            }
        }
        private void RefreshPaymentStatus()
        {
            SqlHelper _SqlHelper = new SqlHelper();
            if (_SqlHelper._ConnOpen == 0) return;

            Mouse.OverrideCursor = Cursors.Wait;
            gvPaymentSchedule.ItemsSource = null;

            //// UPDATE PAYMENT STATUS
            Hashtable htguid = new Hashtable();
            DataManager dal = new DataManager();
            htguid.Add("@MemberID", GetMemberDetailsMemberId);
            htguid.Add("@Programme", 0);
            htguid.Add("@CompanyID", _CompanyID);
            htguid.Add("@SiteID", _SiteID);
            DataSet paychoice_GUID_List = dal.ExecuteProcedureReader("FetchSubscriptionID_Memberships_NEW", htguid);

            if (paychoice_GUID_List != null)
            {
                if (paychoice_GUID_List.Tables[0].Rows.Count > 0)
                {
                    string _customerGUID_Owing = paychoice_GUID_List.Tables[0].Rows[0]["Customer_GUID"].ToString();
                    double _OwingDDC = paychoice.Retrieve_Customer_Balance_pc(_customerGUID_Owing);
                    lblOwingBal.Content = _OwingDDC.ToString("#0.00");

                    if ((_fromRefreshPaymentButton) || (true))
                    {
                        for (int i = 0; i < paychoice_GUID_List.Tables[0].Rows.Count; i++)
                        {
                            string subscription_id = paychoice_GUID_List.Tables[0].Rows[i]["Subscription_Guid"].ToString();
                            string _customerGUID = paychoice_GUID_List.Tables[0].Rows[i]["Customer_GUID"].ToString();
                            string _state = paychoice_GUID_List.Tables[0].Rows[i]["State"].ToString();
                            DateTime _prgEndDate = Convert.ToDateTime(paychoice_GUID_List.Tables[0].Rows[i]["ProgramEndDate"].ToString());

                            int _expiredMembershipPaychoiceStatus = Convert.ToInt32(paychoice_GUID_List.Tables[0].Rows[i]["expiredMembershipPaychoiceStatus"].ToString());
                            int _isArchived = Convert.ToInt32(paychoice_GUID_List.Tables[0].Rows[i]["isArchived"].ToString());

                            if (_state.Trim().ToUpper() != "EXPIRED")
                                _state = "ACTIVE";

                            if (subscription_id.Trim() != "")
                            {
                                if (_expiredMembershipPaychoiceStatus == 0)
                                {
                                    paychoice.Update_Status_PaychoiceScheduledItems(subscription_id, _customerGUID, _prgEndDate, _state);
                                }
                                else
                                {
                                    if (_isArchived == 0)
                                    {
                                        paychoice.Update_Status_PaychoiceScheduledItems(subscription_id, _customerGUID, _prgEndDate, _state);
                                    }
                                    else
                                    {
                                        paychoice.Update_Status_PaychoiceScheduledItems(subscription_id, _customerGUID, _prgEndDate, _state, true);
                                    }
                                }
                            }
                        }
                    }
                        
                    //Payment Schedule TAB
                    ProcName = "GetAccountPaymentScheduleForMember";
                    DS = _SqlHelper.ExecuteProcedure(ProcName, ht);
                    gvPaymentSchedule.ItemsSource = null;
                    btnRefresh.IsEnabled = false;
                    if (DS != null)
                    {
                        if (DS.Tables.Count > 0)
                        {
                            if (DS.Tables[0].DefaultView.Count > 0)
                            {
                                gvPaymentSchedule.ItemsSource = DS.Tables[0].AsDataView();
                                btnRefresh.IsEnabled = true;
                            }
                        }
                    }
                }
                else
                {
                    gvPaymentSchedule.ItemsSource = null;
                    btnRefresh.IsEnabled = false;
                }
            }
            else
            {
                gvPaymentSchedule.ItemsSource = null;
                btnRefresh.IsEnabled = false;
            }
            Mouse.OverrideCursor = null;
        }

        private int RAISE_PayInClub_INVOICE(decimal _amount)
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                int _memberInvoiceNo = GetMembershipInvoiceNo();
                if (_memberInvoiceNo != -1)
                {
                    DateTime _currentDate = DateTime.Now;

                    decimal _invoiceAmount = _amount;
                    decimal _amountPaid = 0m;
                    decimal _amountOutStanding = Convert.ToDecimal(_invoiceAmount - _amountPaid);
                    int _invoiceMemberId = GetMemberDetailsMemberId;
                    string _invoiceStatus = "Pending";
                    _memberDetailsBal.raise_Invoice(_currentDate, _memberInvoiceNo, _invoiceAmount, _amountPaid, _amountOutStanding, _invoiceMemberId, _invoiceStatus, "InvoiceNo");
                    return _memberInvoiceNo;
                }
                return -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return -1;
            }
        }

        private void RAISE_PayInClub_INVOICE_DETAILS(int _InvNo, decimal _amount, string _Details)
        {
            DataTable _InvoiceTable = new DataTable("InvoiceDetails");
            PointOfSaleBAL _InvoiceDetailsBAL = new PointOfSaleBAL();

            _InvoiceTable.Columns.Add("InvoiceNo", typeof(int));
            _InvoiceTable.Columns.Add("MemberNo", typeof(int));
            _InvoiceTable.Columns.Add("InvoiceDetails", typeof(string));
            _InvoiceTable.Columns.Add("Amount", typeof(decimal));
            _InvoiceTable.Columns.Add("Status", typeof(string));
            _InvoiceTable.Columns.Add("ProdSale", typeof(char));
            _InvoiceTable.Columns.Add("ProdID", typeof(int));
            _InvoiceTable.Columns.Add("ProdQty", typeof(int));

            DataRow dR = _InvoiceTable.NewRow();
            decimal _invoiceAmount = _amount;

            dR["InvoiceNo"] = _InvNo;
            dR["MemberNo"] = GetMemberDetailsMemberId;
            dR["InvoiceDetails"] = _Details;
            dR["Amount"] = _invoiceAmount;
            dR["Status"] = "Pending";
            dR["ProdSale"] = 'C';
            dR["ProdID"] = 0;
            dR["ProdQty"] = 0;

            _InvoiceTable.Rows.Add(dR);
            _InvoiceDetailsBAL.SavePOSInvoiceDetails(_InvoiceTable);
        }

        private int GetMembershipInvoiceNo()
        {
            try
            {
                int _invoiceNo;

                _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                DataSet ds = _memberDetailsBal.get_LastUsedNoByCompanySite("InvoiceNo", _CompanyID, _SiteID);
                if (ds != null)
                {
                    _invoiceNo = Convert.ToInt32(ds.Tables[0].Rows[0]["LastUsedNo"].ToString().Trim());
                    return _invoiceNo;
                }
                else
                {
                    MessageBox.Show("No Data! (Invoice Number AutoGenerated)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return -1;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return -1;
            }
        }
    }
}
