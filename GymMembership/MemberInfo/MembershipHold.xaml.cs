﻿using System;

using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration;
using System.Data;

using GymMembership.Task;
using GymMembership.Communications;
using GymMembership.BAL;

using Paychoice_Payment;
namespace GymMembership.MemberInfo
{
    /// <summary>
    /// Interaction logic for MembershipHold.xaml
    /// </summary>
    public partial class MembershipHold
    {
        public MembershipHold()
        {
            InitializeComponent();
        }

        AddFreeTime _addFreeTime;

        public int strMemberNo = 0;
        public int strProgramId = 0;
        public int MSUniqueID = 0;

        private int _companyId = 0;
        private int _holdId = 0;
        private int _holdScuccess = 0;
        private int _holdStatus = 0;
        private int _noOfDays = 0;
        private int _addSuccess;
        private bool _isFreeTime= false;

        public string _MemberName;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public bool _CanOpenMemberShipNewMember = false;
        public bool _CanOpenBillingBankNewMember = false;
        public bool ExistingMember = false;
        public int GetMemberDetailsMemberId; public long CardNumber;
        public DataSet _dSAccessRights;// = new DataSet();
        public DataTable dt = new DataTable();
        public int _CompanyID;
        public int _SiteID;

        int _atleastOneActive = 0;
        string _endHoldSubscriptionGUID = "";
        DateTime _actualHoldEndDate;
        DateTime _actualHoldStartDate;
        
        double _OriginalPaymentAmt = 0;
        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;

            if (_MemberName != null)
            {
                if (_MemberName.Trim() != "")
                {
                    Heading.Text = Heading.Text + " - " + _MemberName;
                }
            }
            //loadStaffPhoto(_StaffPic);
        }
        private void loadStaffPhoto(byte[] _StaffPic)
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
            populateData();
        }

        public void populateData()
        {
            LoadMemberHoldDetails();
            clearData();
        }

        private void btnMsHoldClose_Click(object sender, RoutedEventArgs e)
        {
            //this.Close();
            btnShowMembership_Click(sender, e);
        }

        public void LoadMemberHoldDetails()
        {
            try
            {
                _atleastOneActive = 0;
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                _companyId = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());

                DataSet MemberHoldDs = _memberDetailsBal.get_MemberHoldDetails(strMemberNo, _companyId, strProgramId, MSUniqueID);

                btnMsHoldEndHold.IsEnabled = false;
                btnMsHoldAddHold.IsEnabled = false;

                if (MemberHoldDs != null)
                {
                    if (MemberHoldDs.Tables.Count > 0)
                    {
                        if (MemberHoldDs.Tables[0] != null)
                        {
                            //gvMembershipHold.ItemsSource = MemberHoldDs.Tables[0].AsDataView();
                            gvMembershipHold.ItemsSource = MemberHoldDs.Tables[0].DefaultView;
                            if (MemberHoldDs.Tables[0].DefaultView.Count > 0)
                            {
                                btnMsHoldEndHold.IsEnabled = true;
                                btnMsHoldAddHold.IsEnabled = false;
                            }
                            else
                            {
                                btnMsHoldEndHold.IsEnabled = false;
                                btnMsHoldAddHold.IsEnabled = true;
                            }
                        }
                        //TO CHECK IF ATLEAST 1 IS ACTIVE; ELSE 'ADD HOLD' SHOULD BE ENABLED
                        if (MemberHoldDs.Tables.Count > 1)
                        {
                            if (MemberHoldDs.Tables[1] != null)
                            {
                                if (MemberHoldDs.Tables[1].DefaultView.Count > 0)
                                {
                                    _atleastOneActive = Convert.ToInt32(MemberHoldDs.Tables[1].Rows[0]["ActiveHOLD"].ToString());
                                }
                            }
                        }
                        if (_atleastOneActive == 0)
                        {
                            btnMsHoldAddHold.IsEnabled = true;
                        }
                        else
                        {
                            btnMsHoldAddHold.IsEnabled = false;
                        }
                    }
                }
                
                //IF atleaset 1 active; or no previous entries; enable ADD HOLD
                if (_atleastOneActive == 0)
                {
                    btnMsHoldAddHold.IsEnabled = true;
                }
                else
                {
                    btnMsHoldAddHold.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private void btnMsHoldAddHold_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddMembershipHold _addMembershipHold = new AddMembershipHold();
                _addMembershipHold.strMemberNo = Convert.ToInt32(strMemberNo);
                _addMembershipHold.strProgramId = Convert.ToInt32(strProgramId);
                _addMembershipHold.MSUniqueID = MSUniqueID;

                _addMembershipHold.Owner = this;
                _addMembershipHold.ShowDialog();
                LoadMemberHoldDetails();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private void btnMsHoldAddFreeTime_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ShowFreeTimeDetails();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private void ShowFreeTimeDetails()
        {
            _addFreeTime = new AddFreeTime();
            _addFreeTime.strMemberNo = Convert.ToInt32(strMemberNo);
            _addFreeTime.strProgramId = Convert.ToInt32(strProgramId);
            _addFreeTime.Owner = this;
            _addFreeTime.ShowDialog();
        }

        private void MemberHoldAction(int HoldId, int CompanyId, int HoldStatus)
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                _holdScuccess = _memberDetailsBal.delete_MemberHoldDetails(HoldId, CompanyId, HoldStatus);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private void btnMsHoldDeleteSelect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int i = -1;
                i = gvMembershipHold.SelectedIndex;
                _holdStatus = 3;

                if (i >= 0)
                {
                    _holdId = Convert.ToInt32((gvMembershipHold.Items[i] as DataRowView).Row.ItemArray[0].ToString());
                    if (MessageBox.Show("Member Hold, Are you sure to Delete?", "Delete Member Hold", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        MemberHoldAction(_holdId, _companyId, _holdStatus);
                        if (_holdScuccess != 0)
                        {
                            LoadMemberHoldDetails();
                            MessageBox.Show("Selected Row deleted successfully").ToString();
                        }
                    }
                    i = -1;
                }
                else
                {
                    i = -1;
                    MessageBox.Show("Select a Row to Delete Hold").ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private void btnMsHoldEndHold_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int i = -1;
                i = gvMembershipHold.SelectedIndex;
                _holdStatus = 2;
                if (i >= 0)
                {
                    //_holdId = Convert.ToInt32((gvMembershipHold.Items[i] as DataRowView).Row.ItemArray[0].ToString());
                    _holdId = Convert.ToInt32((gvMembershipHold.Items[i] as DataRowView).Row["HoldId"].ToString());
                    string _confirmMsg = "";
                    _confirmMsg = "Are you sure to End Hold?\n\n(If HOLD is being Ended before Actual end date,\nthis will revert the forthcoming Payment Schedules to Original Amount!)";

                    if (MessageBox.Show(_confirmMsg, this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        //UPDATE PAYMENT SCHEDULE ITEMS WITH ORIGINAL AMOUNT
                        if (_endHoldSubscriptionGUID.Trim() != "")
                        {
                            if ((_endHoldSubscriptionGUID.Trim().Length >= 8) && (_OriginalPaymentAmt > 0))
                            {
                                if (_actualHoldStartDate.Date < DateTime.Now.Date)
                                    _actualHoldStartDate = DateTime.Now;

                                UpdatePaymentItemsToOriginalScheduleAmount(_endHoldSubscriptionGUID, _actualHoldStartDate, _actualHoldEndDate, _OriginalPaymentAmt);
                            }
                        }
                        //UPDATE PAYMENT SCHEDULE ITEMS WITH ORIGINAL AMOUNT

                        _holdScuccess = 0;
                        MemberHoldAction(_holdId, _companyId, _holdStatus);
                        if (_holdScuccess != 0)
                        {
                            LoadMemberHoldDetails();
                            //MessageBox.Show("Selected Row Ended successfully!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    i = -1;
                }
                else
                {
                    i = -1;
                    MessageBox.Show("Select a Row to End Hold", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        void UpdatePaymentItemsToOriginalScheduleAmount
            (
                string p_endHoldSubscriptionGUID, DateTime p_FromDate, DateTime p_actualHoldEndDate, double p_OriginalPaymentAmt
            )
        {
            //GET PAYMENT ITEMS FROM CURR.DATE TILL ACTUAL END DATE & 
            //UPDATE AMOUNT TO ORIGINAL SUBSCR5IPTION AMOUNT //SP Name to get payment Items in this period : "EndHold_GetPayment_Items_For_Update"

            SqlHelper _Sql = new SqlHelper();
            if (_Sql._ConnOpen == 0) return;

            Hashtable ht = new Hashtable();
            ht.Add("@SubscriptionGUID", p_endHoldSubscriptionGUID);
            ht.Add("@FromDate", p_FromDate);
            ht.Add("@ToDate", p_actualHoldEndDate);

            DataSet _dsRemoveHOLD = _Sql.ExecuteProcedure("EndHold_GetPayment_Items_For_Update", ht);
            if (_dsRemoveHOLD != null)
            {
                if (_dsRemoveHOLD.Tables.Count > 0)
                {
                    if (_dsRemoveHOLD.Tables[0].DefaultView.Count > 0)
                    {
                        //BEGIN : FOR EACH PAYMENT ITEM : AMEND AMOUNT TO ORIGINAL AMOUNT
                        for (int i = 1; i < _dsRemoveHOLD.Tables[0].DefaultView.Count; i++) //assign i = 0 : If all items to be updated (or) i = 1 : if 1st item to be skipped
                        {
                            string p_PaymentScheduleItemGUID = _dsRemoveHOLD.Tables[0].DefaultView[i]["PaymentScheduleItemGUID"].ToString();
                            paychoice.Amend_Single_Payment_Schedule(p_endHoldSubscriptionGUID, p_PaymentScheduleItemGUID, p_OriginalPaymentAmt);
                        }
                        //END : FOR EACH PAYMENT ITEM : AMEND AMOUNT TO ORIGINAL AMOUNT
                    }
                }
            }
        }
        private void gvMembershipHold_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                clearData();
                int i = gvMembershipHold.SelectedIndex;

                if (i >= 0)
                {
                    txtHoldFee.Text = (gvMembershipHold.Items[i] as DataRowView).Row["FeeAmount"].ToString();
                    _isFreeTime = Convert.ToBoolean((gvMembershipHold.Items[i] as DataRowView).Row["IsFreeTime"].ToString());

                    btnMsHoldEndHold.IsEnabled = true;
                    btnMsHoldAddHold.IsEnabled = false;

                    if ((gvMembershipHold.Items[i] as DataRowView).Row["HoldStatus"].ToString().ToUpper() == "HOLD REMOVED")
                    {
                        btnMsHoldEndHold.IsEnabled = false;
                        _endHoldSubscriptionGUID = "";
                    }
                    else
                    {
                        btnMsHoldEndHold.IsEnabled = true;

                        _actualHoldEndDate = Convert.ToDateTime((gvMembershipHold.Items[i] as DataRowView).Row["EndDateActual"]);
                        _actualHoldStartDate = Convert.ToDateTime((gvMembershipHold.Items[i] as DataRowView).Row["StartDateActual"]);
                        _endHoldSubscriptionGUID = (gvMembershipHold.Items[i] as DataRowView).Row["SubscriptionGUID"].ToString();
                        _OriginalPaymentAmt = Convert.ToDouble((gvMembershipHold.Items[i] as DataRowView).Row["OriginalAmount"]);
                    }

                    //if atleast 1 active; or all removed; or no previous holds - enable 'ADD HOLD'
                    if (_atleastOneActive == 0)
                    {
                        btnMsHoldAddHold.IsEnabled = true;
                        _endHoldSubscriptionGUID = "";
                    }
                    else
                    {
                        btnMsHoldAddHold.IsEnabled = false;
                    }
                }
                else
                {
                    btnMsHoldEndHold.IsEnabled = false;
                    btnMsHoldAddHold.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private void AddRemoveHoldDays(int NoOfDays)
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                int i = -1;
                 _addSuccess = 0;
                i = gvMembershipHold.SelectedIndex;

                if (i >= 0)
                {
                    _holdId = Convert.ToInt32((gvMembershipHold.Items[i] as DataRowView).Row.ItemArray[0].ToString());
                    _addSuccess = _memberDetailsBal.add_RemoveMemberHoldDays(NoOfDays, _holdId, _companyId);
                }
                else
                {
                    _holdId = -1;
                    MessageBox.Show("Select a Row to Add or Remove").ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private void btnMsHoldAddDay_Click(object sender, RoutedEventArgs e)
        {
            int i = -1;
            i = gvMembershipHold.SelectedIndex;
            _noOfDays = 1;

            if (i >= 0)
            {
                if (MessageBox.Show("Member Hold, Are you sure to Add a Day?", "Add Hold Day", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    AddRemoveHoldDays(_noOfDays);
                    LoadMemberHoldDetails();
                    if (_addSuccess != 0)
                    {
                        MessageBox.Show("One Day successfully Added to Member Hold Day").ToString();
                    }
                }
                i = -1;
            }
            else
            {
                i = -1;
                MessageBox.Show("Select a Row to Add Day").ToString();
            }
        }

        private void btnMsHoldRemoveHold_Click(object sender, RoutedEventArgs e)
        {
            int i = -1;
            i = gvMembershipHold.SelectedIndex;
            _noOfDays = -1;

            if (i >= 0)
            {

                if (MessageBox.Show("Member Hold, Are you sure to Remove a Day?", "Remove Hold Day", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    AddRemoveHoldDays(_noOfDays);
                    LoadMemberHoldDetails();

                    if (_addSuccess != 0)
                    {
                        MessageBox.Show("One Day successfully Removed from Member Hold Day").ToString();
                    }
                }
                i = -1;
            }

            else
            {
                i = -1;
                MessageBox.Show("Select a Row to Remove Day").ToString();
            }
        }

        private void btnMsHoldAddWeek_Click(object sender, RoutedEventArgs e)
        {
            int i = -1;
            i = gvMembershipHold.SelectedIndex;
            _noOfDays = 7;

            if (i >= 0)
            {
                if (MessageBox.Show("Member Hold, Are you sure to Add a Week?", "Add Hold Week", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    AddRemoveHoldDays(_noOfDays);
                    LoadMemberHoldDetails();
                    if (_addSuccess != 0)
                    {
                        MessageBox.Show("One Week successfully Added to Member Hold Day").ToString();
                    }
                }
                i = -1;
            }
            else
            {
                i = -1;
                MessageBox.Show("Select a Row to Add Week").ToString();
            }
        }

        private void btnMsHoldRemoveWeek_Click(object sender, RoutedEventArgs e)
        {
            int i = -1;
            i = gvMembershipHold.SelectedIndex;
            _noOfDays = -7;

            if (i >= 0)
            {

                if (MessageBox.Show("Member Hold, Are you sure to Remove a Week?", "Remove Hold Week", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    AddRemoveHoldDays(_noOfDays);
                    LoadMemberHoldDetails();

                    if (_addSuccess != 0)
                    {
                        MessageBox.Show("One Week successfully Removed from Member Hold Day").ToString();
                    }
                }
                i = -1;
            }
            else
            {
                i = -1;
                MessageBox.Show("Select a Row to Remove Week").ToString();
            }
        }

        private void clearData()
        {
            txtHoldFee.Text = string.Empty;
        }

        private void btnMsHoldEditFreeTime_Click(object sender, RoutedEventArgs e)
        {
            ShowFreeTimeDetails();

            int i = -1;
            i = gvMembershipHold.SelectedIndex;

            if (i >= 0)
            {
                _holdId = Convert.ToInt32((gvMembershipHold.Items[i] as DataRowView).Row.ItemArray[0].ToString());
                _addFreeTime.strHoldId = Convert.ToInt32(_holdId);
            }
        }


        private void btnShowMembership_Click(object sender, RoutedEventArgs e)
        {
            if (!ExistingMember)
            {
                if (_CanOpenMemberShipNewMember)
                {
                    Memberships _mDetails = new Memberships();
                    _mDetails.ExistingMember = ExistingMember;
                    _mDetails.CardNumber = CardNumber;
                    _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    _mDetails._MemberName = _MemberName;
                    _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    _mDetails._CompanyName = _CompanyName;
                    _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                    _mDetails._SiteName = _SiteName;
                    _mDetails._StaffPic = _StaffPic;
                    _mDetails._UserName = _UserName;
                    _mDetails._CompanyID = _CompanyID;
                    _mDetails._SiteID = _SiteID;
                    _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                    _mDetails.StaffImage.Source = StaffImage.Source;
                    _mDetails._dSAccessRights = _dSAccessRights;
                    _mDetails.dt = this.dt;
                    _mDetails.Owner = this.Owner;

                    this.Close();
                    _mDetails.ShowDialog();
                }
                else
                {
                    MessageBox.Show("New Member Details have to be Created First!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            else
            {
                Memberships _mDetails = new Memberships();
                _mDetails.ExistingMember = ExistingMember;
                _mDetails.CardNumber = CardNumber;
                _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _mDetails._MemberName = _MemberName;
                _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _mDetails._CompanyName = _CompanyName;
                _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                _mDetails._SiteName = _SiteName;
                _mDetails._StaffPic = _StaffPic;
                _mDetails._UserName = _UserName;
                _mDetails._CompanyID = _CompanyID;
                _mDetails._SiteID = _SiteID;
                _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                _mDetails.StaffImage.Source = StaffImage.Source;
                _mDetails._dSAccessRights = _dSAccessRights;
                _mDetails.dt = this.dt;
                _mDetails.Owner = this.Owner;

                this.Close();
                _mDetails.ShowDialog();
            }
        }

        private void btnAccounts_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberAccounts mAccounts = new MemberAccounts();
                mAccounts.MemberID = GetMemberDetailsMemberId;
                mAccounts._CurrentLoginDateTime = _CurrentLoginDateTime;
                mAccounts._CompanyName = _CompanyName;
                mAccounts._SiteName = _SiteName;
                mAccounts._UserName = _UserName;
                mAccounts._StaffPic = _StaffPic;
                mAccounts._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mAccounts.ExistingMember = ExistingMember;
                mAccounts.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mAccounts._MemberName = _MemberName;
                mAccounts.Member_Image_Img.Source = Member_Image_Img.Source;
                mAccounts.StaffImage.Source = StaffImage.Source;
                mAccounts._dSAccessRights = _dSAccessRights;
                mAccounts.dt = this.dt;

                mAccounts.Owner = this.Owner;
                this.Cursor = System.Windows.Input.Cursors.Wait;
                btnAccounts.Cursor = System.Windows.Input.Cursors.Wait;
                this.Close();
                mAccounts.ShowDialog();
            }
        }

        private void btnMemberBillingDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ExistingMember && _CanOpenBillingBankNewMember)
                {
                    this.Cursor = System.Windows.Input.Cursors.Wait;
                    PaymentDetails PD = new PaymentDetails(GetMemberDetailsMemberId, _CompanyID, _SiteID, dt);
                    this.Cursor = System.Windows.Input.Cursors.Arrow;

                    PD._CurrentLoginDateTime = _CurrentLoginDateTime;
                    PD._CompanyName = _CompanyName;
                    PD._SiteName = _SiteName;
                    PD._UserName = _UserName;
                    PD._StaffPic = _StaffPic;
                    PD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    PD._CanOpenBillingBankNewMember = _CanOpenBillingBankNewMember;
                    PD.ExistingMember = ExistingMember;
                    PD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    PD._MemberName = _MemberName;
                    PD.Member_Image_Img.Source = Member_Image_Img.Source;
                    PD.StaffImage.Source = StaffImage.Source;
                    PD._dSAccessRights = _dSAccessRights;
                    PD.dt = this.dt;

                    PD.Owner = this.Owner;
                    this.Close();
                    PD.ShowDialog();
                }
                else
                {
                    if (dt.Rows.Count > 0)
                    {
                        System.Windows.MessageBox.Show("Membership has to be Created!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnMemberExtraDetails_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                ExtraDetails extra = new ExtraDetails();
                extra.strExtraDetailsMemberID = GetMemberDetailsMemberId; //Convert.ToInt32(txtMemberNumber.Text.ToString().Trim());

                extra._CurrentLoginDateTime = _CurrentLoginDateTime;
                extra._CompanyName = _CompanyName;
                extra._SiteName = _SiteName;
                extra._UserName = _UserName;
                extra._StaffPic = _StaffPic;
                extra._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                extra.ExistingMember = ExistingMember;
                extra.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                extra._MemberName = _MemberName;
                extra.Member_Image_Img.Source = Member_Image_Img.Source;
                extra.StaffImage.Source = StaffImage.Source;
                extra._dSAccessRights = _dSAccessRights;
                extra.dt = this.dt;

                extra.Owner = this.Owner;
                this.Close();
                extra.ShowDialog();
            }
        }

        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                Tasks Tsk = new Tasks();
                Tsk.strMemberNo = GetMemberDetailsMemberId; //Convert.ToInt32(txtMemberNumber.Text);
                Tsk.Owner = this.Owner;
                Tsk.SetAccessRightsDS(_dSAccessRights);

                Tsk._CurrentLoginDateTime = _CurrentLoginDateTime;
                Tsk._CompanyName = _CompanyName;
                Tsk._SiteName = _SiteName;
                Tsk._UserName = _UserName;
                Tsk._StaffPic = _StaffPic;
                Tsk._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                Tsk.ExistingMember = ExistingMember;
                Tsk.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                Tsk._MemberName = _MemberName;
                Tsk.Member_Image_Img.Source = Member_Image_Img.Source;
                Tsk.StaffImage.Source = StaffImage.Source;
                Tsk._dSAccessRights = _dSAccessRights;
                Tsk.dt = this.dt;

                this.Close();
                Tsk.ShowDialog();
            }
        }

        private void btnCommunications_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Communication _Comm = new Communication();
                _Comm.Owner = this.Owner;
                _Comm.SetAccessRightsDS(_dSAccessRights);
                this.Cursor = System.Windows.Input.Cursors.Arrow;

                _Comm._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Comm._CompanyName = _CompanyName;
                _Comm._SiteName = _SiteName;
                _Comm._UserName = _UserName;
                _Comm._StaffPic = _StaffPic;
                _Comm._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Comm.ExistingMember = ExistingMember;
                _Comm.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Comm._MemberName = _MemberName;
                _Comm.Member_Image_Img.Source = Member_Image_Img.Source;
                _Comm.StaffImage.Source = StaffImage.Source;
                _Comm._dSAccessRights = _dSAccessRights;
                _Comm.dt = this.dt;

                this.Close();
                _Comm.ShowDialog();
            }
        }

        private void btnNotes_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Notes _Notes = new Notes();
                _Notes.Owner = this.Owner;

                _Notes._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Notes._CompanyName = _CompanyName;
                _Notes._SiteName = _SiteName;
                _Notes._UserName = _UserName;
                _Notes._StaffPic = _StaffPic;
                _Notes._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Notes.ExistingMember = ExistingMember;
                _Notes.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Notes._MemberName = _MemberName;
                _Notes.Member_Image_Img.Source = Member_Image_Img.Source;
                _Notes.StaffImage.Source = StaffImage.Source;
                _Notes._dSAccessRights = _dSAccessRights;
                _Notes.dt = this.dt;

                this.Cursor = System.Windows.Input.Cursors.Arrow;
                this.Close();
                _Notes.ShowDialog();
            }
        }

        private void btnPersonalTraining_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                PersonalTraining _pt = new PersonalTraining();
                _pt.Owner = this.Owner;

                _pt._CurrentLoginDateTime = _CurrentLoginDateTime;
                _pt._CompanyName = _CompanyName;
                _pt._SiteName = _SiteName;
                _pt._UserName = _UserName;
                _pt._StaffPic = _StaffPic;
                _pt._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _pt.ExistingMember = ExistingMember;
                _pt.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _pt._MemberName = _MemberName;
                _pt.Member_Image_Img.Source = Member_Image_Img.Source;
                _pt.StaffImage.Source = StaffImage.Source;
                _pt._dSAccessRights = _dSAccessRights;
                _pt.dt = this.dt;

                this.Close();
                _pt.ShowDialog();
            }
        }

        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberDetails mD = new MemberDetails();
                mD.Owner = this.Owner;

                mD._CurrentLoginDateTime = _CurrentLoginDateTime;
                mD._CompanyName = _CompanyName;
                mD._SiteName = _SiteName;
                mD._UserName = _UserName;
                mD._StaffPic = _StaffPic;
                mD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mD.ExistingMember = ExistingMember;
                mD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mD._MemberName = _MemberName;
                mD.Member_Image_Img.Source = Member_Image_Img.Source;
                mD.StaffImage.Source = StaffImage.Source;
                mD._dSAccessRights = _dSAccessRights;
                mD.dt = this.dt;

                this.Close();
                mD.ShowDialog();
            }
        }

        private void btnbacktoMembership_Click(object sender, RoutedEventArgs e)
        {
            //this.Close();
            btnShowMembership_Click(sender, e);
        }
    }
}
