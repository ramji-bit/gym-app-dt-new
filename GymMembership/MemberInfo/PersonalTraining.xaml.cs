﻿using System;
using System.IO;
using System.Data;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Collections;
using System.Configuration;

using GymMembership.Communications;
using GymMembership.Task;

namespace GymMembership.MemberInfo
{
    /// <summary>
    /// Interaction logic for PersonalTraining.xaml
    /// </summary>
    public partial class PersonalTraining 
    {
        public string _MemberName;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public bool _CanOpenMemberShipNewMember = false;
        public bool ExistingMember = false;
        public int GetMemberDetailsMemberId; public long CardNumber;
        public DataSet _dSAccessRights;
        public DataTable dt = new DataTable();

        DataTable _dtMeasurements = new DataTable();
        DataSet _dsPTDetails = new DataSet();
        int _PTBookingTable = 0; int _PTMeasureMainTable = 0; int _PTMeasureDetailsTable = 1;

        public int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
        public int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
        public PersonalTraining()
        {
            InitializeComponent();
        }
        
        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnShowMembership_Click(object sender, RoutedEventArgs e)
        {
            if (!ExistingMember)
            {
                if (_CanOpenMemberShipNewMember)
                {
                    Memberships _mDetails = new Memberships();
                    _mDetails.ExistingMember = ExistingMember;
                    _mDetails.CardNumber = CardNumber;
                    _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    _mDetails._MemberName = _MemberName;
                    _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    _mDetails._CompanyName = _CompanyName;
                    _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                    _mDetails._SiteName = _SiteName;
                    _mDetails._StaffPic = _StaffPic;
                    _mDetails._UserName = _UserName;
                    _mDetails._CompanyID = _CompanyID;
                    _mDetails._SiteID = _SiteID;
                    _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                    _mDetails.StaffImage.Source = StaffImage.Source;
                    _mDetails._dSAccessRights = _dSAccessRights;
                    _mDetails.dt = this.dt;
                    _mDetails.Owner = this.Owner;

                    this.Close();
                    _mDetails.ShowDialog();
                }
                else
                {
                    System.Windows.MessageBox.Show("New Member Details have to be Created First!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            else
            {
                Memberships _mDetails = new Memberships();
                _mDetails.ExistingMember = ExistingMember;
                _mDetails.CardNumber = CardNumber;
                _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _mDetails._MemberName = _MemberName;
                _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _mDetails._CompanyName = _CompanyName;
                _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                _mDetails._SiteName = _SiteName;
                _mDetails._StaffPic = _StaffPic;
                _mDetails._UserName = _UserName;
                _mDetails._CompanyID = _CompanyID;
                _mDetails._SiteID = _SiteID;
                _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                _mDetails.StaffImage.Source = StaffImage.Source;
                _mDetails._dSAccessRights = _dSAccessRights;
                _mDetails.dt = this.dt;
                _mDetails.Owner = this.Owner;

                this.Close();
                _mDetails.ShowDialog();
            }
        }

        private void btnAccounts_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberAccounts mAccounts = new MemberAccounts();
                mAccounts.MemberID = GetMemberDetailsMemberId;
                mAccounts._CurrentLoginDateTime = _CurrentLoginDateTime;
                mAccounts._CompanyName = _CompanyName;
                mAccounts._SiteName = _SiteName;
                mAccounts._UserName = _UserName;
                mAccounts._StaffPic = _StaffPic;
                mAccounts._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mAccounts.ExistingMember = ExistingMember;
                mAccounts.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mAccounts._MemberName = _MemberName;
                mAccounts.Member_Image_Img.Source = Member_Image_Img.Source;
                mAccounts.StaffImage.Source = StaffImage.Source;
                mAccounts._dSAccessRights = _dSAccessRights;
                mAccounts.dt = this.dt;

                mAccounts.Owner = this.Owner;
                this.Cursor = System.Windows.Input.Cursors.Wait;
                btnAccounts.Cursor = System.Windows.Input.Cursors.Wait;
                this.Close();
                mAccounts.ShowDialog();
            }
        }

        private void btnMemberBillingDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ExistingMember)
                {
                    this.Cursor = System.Windows.Input.Cursors.Wait;
                    PaymentDetails PD = new PaymentDetails(GetMemberDetailsMemberId, _CompanyID, _SiteID, dt);
                    this.Cursor = System.Windows.Input.Cursors.Arrow;

                    PD._CurrentLoginDateTime = _CurrentLoginDateTime;
                    PD._CompanyName = _CompanyName;
                    PD._SiteName = _SiteName;
                    PD._UserName = _UserName;
                    PD._StaffPic = _StaffPic;
                    PD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    PD.ExistingMember = ExistingMember;
                    PD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    PD._MemberName = _MemberName;
                    PD.Member_Image_Img.Source = Member_Image_Img.Source;
                    PD.StaffImage.Source = StaffImage.Source;
                    PD._dSAccessRights = _dSAccessRights;
                    PD.dt = this.dt;

                    PD.Owner = this.Owner;
                    this.Close();
                    PD.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnMemberExtraDetails_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                ExtraDetails extra = new ExtraDetails();
                extra.strExtraDetailsMemberID = GetMemberDetailsMemberId;

                extra._CurrentLoginDateTime = _CurrentLoginDateTime;
                extra._CompanyName = _CompanyName;
                extra._SiteName = _SiteName;
                extra._UserName = _UserName;
                extra._StaffPic = _StaffPic;
                extra._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                extra.ExistingMember = ExistingMember;
                extra.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                extra._MemberName = _MemberName;
                extra.Member_Image_Img.Source = Member_Image_Img.Source;
                extra.StaffImage.Source = StaffImage.Source;
                extra._dSAccessRights = _dSAccessRights;
                extra.dt = this.dt;

                extra.Owner = this.Owner;
                this.Close();
                extra.ShowDialog();
            }
        }

        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                Tasks Tsk = new Tasks();
                Tsk.strMemberNo = GetMemberDetailsMemberId;
                Tsk.Owner = this.Owner;
                Tsk.SetAccessRightsDS(_dSAccessRights);

                Tsk._CurrentLoginDateTime = _CurrentLoginDateTime;
                Tsk._CompanyName = _CompanyName;
                Tsk._SiteName = _SiteName;
                Tsk._UserName = _UserName;
                Tsk._StaffPic = _StaffPic;
                Tsk._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                Tsk.ExistingMember = ExistingMember;
                Tsk.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                Tsk._MemberName = _MemberName;
                Tsk.Member_Image_Img.Source = Member_Image_Img.Source;
                Tsk.StaffImage.Source = StaffImage.Source;
                Tsk._dSAccessRights = _dSAccessRights;
                Tsk.dt = this.dt;

                this.Close();
                Tsk.ShowDialog();
            }
        }

        private void btnCommunications_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Communication _Comm = new Communication();
                _Comm.Owner = this.Owner;
                _Comm.SetAccessRightsDS(_dSAccessRights);
                this.Cursor = System.Windows.Input.Cursors.Arrow;

                _Comm._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Comm._CompanyName = _CompanyName;
                _Comm._SiteName = _SiteName;
                _Comm._UserName = _UserName;
                _Comm._StaffPic = _StaffPic;
                _Comm._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Comm.ExistingMember = ExistingMember;
                _Comm.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Comm._MemberName = _MemberName;
                _Comm.Member_Image_Img.Source = Member_Image_Img.Source;
                _Comm.StaffImage.Source = StaffImage.Source;
                _Comm._dSAccessRights = _dSAccessRights;
                _Comm.dt = this.dt;

                this.Close();
                _Comm.ShowDialog();
            }
        }

        private void btnNotes_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Notes _Notes = new Notes();
                _Notes.Owner = this.Owner;

                _Notes._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Notes._CompanyName = _CompanyName;
                _Notes._SiteName = _SiteName;
                _Notes._UserName = _UserName;
                _Notes._StaffPic = _StaffPic;
                _Notes._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Notes.ExistingMember = ExistingMember;
                _Notes.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Notes._MemberName = _MemberName;
                _Notes.Member_Image_Img.Source = Member_Image_Img.Source;
                _Notes.StaffImage.Source = StaffImage.Source;
                _Notes._dSAccessRights = _dSAccessRights;
                _Notes.dt = this.dt;

                this.Cursor = System.Windows.Input.Cursors.Arrow;
                this.Close();
                _Notes.ShowDialog();
            }
        }

        private void btnPersonalTraining_Click(object sender, RoutedEventArgs e)
        {
            //SAME WINDOW; NO ACTION
        }

        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberDetails mD = new MemberDetails();
                mD.Owner = this.Owner;

                mD._CurrentLoginDateTime = _CurrentLoginDateTime;
                mD._CompanyName = _CompanyName;
                mD._SiteName = _SiteName;
                mD._UserName = _UserName;
                mD._StaffPic = _StaffPic;
                mD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mD.ExistingMember = ExistingMember;
                mD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mD._MemberName = _MemberName;
                mD.Member_Image_Img.Source = Member_Image_Img.Source;
                mD.StaffImage.Source = StaffImage.Source;
                mD._dSAccessRights = _dSAccessRights;
                mD.dt = this.dt;

                this.Close();
                mD.ShowDialog();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
            ClearDetails();
            GetBookingDetailsForMember();
            GetMeasurementDetailsForMember();
        }
        private void ClearDetails()
        {
            gvBookingDetails.ItemsSource = null;
            gvBookingDetails.Items.Clear();

            gvMeasurementDate.ItemsSource = null;
            gvMeasurementDate.Items.Clear();

            gvMeasurementDetails.ItemsSource = null;
            gvMeasurementDetails.Items.Clear();

            txtCurrentGoal.Text = "";
            txtPTToDo.Text = "";
            dtStartDate.DisplayDate = DateTime.Now.Date;
            dtEndDate.DisplayDate = DateTime.Now.Date;

            dtStartDate.Text = dtStartDate.DisplayDate.ToString("dd/MM/yyyy");
            dtEndDate.Text = dtEndDate.DisplayDate.ToString("dd/MM/yyyy");
            dtEndDate.DisplayDateStart = dtStartDate.DisplayDate;
        }
        private void GetBookingDetailsForMember()
        {
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@MemberID", GetMemberDetailsMemberId);

            SqlHelper _sqlH = new SqlHelper();
            if (_sqlH._ConnOpen == 0) return;

            gvBookingDetails.ItemsSource = null;
            gvBookingDetails.Items.Clear();

            DataSet ds = _sqlH.ExecuteProcedure("GetBookingDetailsMember", ht);
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[_PTBookingTable].Rows.Count > 0)
                    {
                        gvBookingDetails.ItemsSource = ds.Tables[_PTBookingTable].DefaultView;
                    }
                }
            }
        }
        private void GetMeasurementDetailsForMember()
        {
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@MemberID", GetMemberDetailsMemberId);

            SqlHelper _sqlH = new SqlHelper();
            if (_sqlH._ConnOpen == 0) return;

            gvMeasurementDate.ItemsSource = null;
            gvMeasurementDate.Items.Clear();

            _dsPTDetails = _sqlH.ExecuteProcedure("GetMeasurementDetailsForMember", ht);
            if (_dsPTDetails != null)
            {
                if (_dsPTDetails.Tables.Count > 0)
                {
                    if (_dsPTDetails.Tables[_PTMeasureMainTable].Rows.Count > 0)
                    {
                        gvMeasurementDate.ItemsSource = _dsPTDetails.Tables[_PTMeasureMainTable].DefaultView;
                    }
                    else
                    {
                        GridInitializeNewMeasurement();
                    }
                }
                else
                {
                    GridInitializeNewMeasurement();
                }
            }
            else
            {
                GridInitializeNewMeasurement();
            }
        }

        private void GridInitializeNewMeasurement()
        {
            _dtMeasurements = new DataTable("Measurement");

            _dtMeasurements.Columns.Add("ID", typeof(int));
            _dtMeasurements.Columns.Add("CompanyID", typeof(int));
            _dtMeasurements.Columns.Add("SiteID", typeof(int));
            _dtMeasurements.Columns.Add("MemberID", typeof(int));
            _dtMeasurements.Columns.Add("UniqueID", typeof(string));

            _dtMeasurements.Columns.Add("Measurement", typeof(string));
            _dtMeasurements.Columns.Add("Value", typeof(decimal));
            _dtMeasurements.Columns.Add("Attention", typeof(string));

            gvMeasurementDetails.ItemsSource = _dtMeasurements.DefaultView;
        }
    
    private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;

            if (_MemberName != null)
            {
                if (_MemberName.Trim() != "")
                {
                    Heading.Text = Heading.Text + " - " + _MemberName;
                }
            }
            //loadStaffPhoto(_StaffPic);
        }
        private void loadStaffPhoto(byte[] _StaffPic)
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }

        private void dtStartDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            dtEndDate.Text = "";
            dtEndDate.DisplayDateStart = dtStartDate.SelectedDate;
        }

        private void gvMeasurementDate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                gvMeasurementDetails.ItemsSource = null;
                gvMeasurementDetails.Items.Clear();

                if (gvMeasurementDate.SelectedIndex != -1)
                {
                    int i = gvMeasurementDate.SelectedIndex;
                    string _uniqueID = _dsPTDetails.Tables[_PTMeasureMainTable].DefaultView[i]["UniqueID"].ToString();
                    txtCurrentGoal.Text = _dsPTDetails.Tables[_PTMeasureMainTable].DefaultView[i]["CurrentGoal"].ToString().ToUpper();
                    txtPTToDo.Text = _dsPTDetails.Tables[_PTMeasureMainTable].DefaultView[i]["PTNotes"].ToString().ToUpper();
                    dtStartDate.Text = Convert.ToDateTime(_dsPTDetails.Tables[_PTMeasureMainTable].DefaultView[i]["StartDate"].ToString()).ToString("dd/MM/yyyy");
                    dtEndDate.Text = Convert.ToDateTime(_dsPTDetails.Tables[_PTMeasureMainTable].DefaultView[i]["EndDate"].ToString()).ToString("dd/MM/yyyy");

                    if (_dsPTDetails.Tables.Count > 1)
                    {
                        _dsPTDetails.Tables[_PTMeasureDetailsTable].DefaultView.RowFilter = "UniqueID = '" + _uniqueID + "'";
                        if (_dsPTDetails.Tables[_PTMeasureDetailsTable].DefaultView.Count > 0)
                        {
                            //gvMeasurementDetails.ItemsSource = _dsPTDetails.Tables[_PTMeasureDetailsTable].DefaultView;
                            _dtMeasurements = _dtMeasurements = _dsPTDetails.Tables[_PTMeasureDetailsTable].DefaultView.ToTable();
                            gvMeasurementDetails.ItemsSource = _dtMeasurements.DefaultView;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                string _xMsg = ex.Message;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateData())
            {
                if (SavePTDetailsForMember() > 0)
                {
                    MessageBox.Show("Measurements Saved Successfully !", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    ClearDetails();
                    GetBookingDetailsForMember();
                    GetMeasurementDetailsForMember();
                }
                else
                    MessageBox.Show("Save Failed!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private int SavePTDetailsForMember()
        {
            SqlHelper _sqlH = new SqlHelper();
            if (_sqlH._ConnOpen == 0) return 0;

            string _uniqueID = DateTime.Now.ToString("yyyyMMddhhmmss");
            DateTime _measureDate = DateTime.Now;

            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@MemberID", GetMemberDetailsMemberId);
            ht.Add("@UniqueID", _uniqueID);
            ht.Add("@CurrentGoal", txtCurrentGoal.Text.Trim().ToUpper());
            ht.Add("@StartDate", dtStartDate.SelectedDate);
            ht.Add("@EndDate", dtEndDate.SelectedDate);
            ht.Add("@PTNotes", txtPTToDo.Text.Trim().ToUpper());
            ht.Add("@MeasureDate", _measureDate);
            ht.Add("@PTMeasures", _dtMeasurements);
            
            return _sqlH.ExecuteQuery("SavePTForMember", ht);
        }
        private bool ValidateData()
        {
            _dtMeasurements.AcceptChanges();

            if (_dtMeasurements.DefaultView.Count <= 0)
            {
                MessageBox.Show("Measurement Details not entered!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            
            if (txtCurrentGoal.Text.Trim() == "")
            {
                MessageBox.Show("Current Goal is Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            if (txtPTToDo.Text.Trim() == "")
            {
                MessageBox.Show("Personal Trainer 'TO DO' Notes is Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            if (dtStartDate.SelectedDate == null)
            {
                dtStartDate.SelectedDate = dtStartDate.DisplayDate;
            }
            if (dtEndDate.SelectedDate == null)
            {
                dtEndDate.SelectedDate = dtEndDate.DisplayDate;
            }
            return true;
        }
    }
}
