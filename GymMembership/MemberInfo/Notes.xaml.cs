﻿using System;
using System.IO;
using System.Windows.Media.Imaging;
using System.Data;
using System.Threading.Tasks;
using System.Collections;
using System.Windows;
using System.Windows.Forms;
using System.Configuration;
using System.Diagnostics;

using GymMembership.Task;
using GymMembership.Communications;
using GymMembership.BAL;
using GymMembership.Comman;

namespace GymMembership.MemberInfo
{
    /// <summary>
    /// Interaction logic for Notes.xaml
    /// </summary>
    public partial class Notes 
    {
        public string _MemberName;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public bool _CanOpenMemberShipNewMember = false;
        public bool ExistingMember = false;
        public int GetMemberDetailsMemberId; public long CardNumber;
        public DataSet _dSAccessRights;
        public DataTable dt = new DataTable();

        public int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
        public int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
        DataSet _dsNotes = new DataSet();
        DataSet _dsForms = new DataSet();

        byte[] _MemberForms_FileData = null;
        string _MemberForms_FileName;
        string _MemberForms_Ext;
        string _MemberForms_ContentType;
        int _SerialNo = 0;
        bool _isNewNotes = false;

        public Notes()
        {
            InitializeComponent();
        }
        
        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void btnShowMembership_Click(object sender, RoutedEventArgs e)
        {
            if (!ExistingMember)
            {
                if (_CanOpenMemberShipNewMember)
                {
                    Memberships _mDetails = new Memberships();
                    _mDetails.ExistingMember = ExistingMember;
                    _mDetails.CardNumber = CardNumber;
                    _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    _mDetails._MemberName = _MemberName;
                    _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    _mDetails._CompanyName = _CompanyName;
                    _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                    _mDetails._SiteName = _SiteName;
                    _mDetails._StaffPic = _StaffPic;
                    _mDetails._UserName = _UserName;
                    _mDetails._CompanyID = _CompanyID;
                    _mDetails._SiteID = _SiteID;
                    _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                    _mDetails.StaffImage.Source = StaffImage.Source;
                    _mDetails._dSAccessRights = _dSAccessRights;
                    _mDetails.dt = this.dt;
                    _mDetails.Owner = this.Owner;

                    this.Close();
                    _mDetails.ShowDialog();
                }
                else
                {
                    System.Windows.MessageBox.Show("New Member Details have to be Created First!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            else
            {
                Memberships _mDetails = new Memberships();
                _mDetails.ExistingMember = ExistingMember;
                _mDetails.CardNumber = CardNumber;
                _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _mDetails._MemberName = _MemberName;
                _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _mDetails._CompanyName = _CompanyName;
                _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                _mDetails._SiteName = _SiteName;
                _mDetails._StaffPic = _StaffPic;
                _mDetails._UserName = _UserName;
                _mDetails._CompanyID = _CompanyID;
                _mDetails._SiteID = _SiteID;
                _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                _mDetails.StaffImage.Source = StaffImage.Source;
                _mDetails._dSAccessRights = _dSAccessRights;
                _mDetails.dt = this.dt;
                _mDetails.Owner = this.Owner;

                this.Close();
                _mDetails.ShowDialog();
            }
        }

        private void btnAccounts_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberAccounts mAccounts = new MemberAccounts();
                mAccounts.MemberID = GetMemberDetailsMemberId;
                mAccounts._CurrentLoginDateTime = _CurrentLoginDateTime;
                mAccounts._CompanyName = _CompanyName;
                mAccounts._SiteName = _SiteName;
                mAccounts._UserName = _UserName;
                mAccounts._StaffPic = _StaffPic;
                mAccounts._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mAccounts.ExistingMember = ExistingMember;
                mAccounts.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mAccounts._MemberName = _MemberName;
                mAccounts.Member_Image_Img.Source = Member_Image_Img.Source;
                mAccounts.StaffImage.Source = StaffImage.Source;
                mAccounts._dSAccessRights = _dSAccessRights;
                mAccounts.dt = this.dt;

                mAccounts.Owner = this.Owner;
                this.Cursor = System.Windows.Input.Cursors.Wait;
                btnAccounts.Cursor = System.Windows.Input.Cursors.Wait;
                this.Close();
                mAccounts.ShowDialog();
            }
        }

        private void btnMemberBillingDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ExistingMember)
                {
                    this.Cursor = System.Windows.Input.Cursors.Wait;
                    PaymentDetails PD = new PaymentDetails(GetMemberDetailsMemberId, _CompanyID, _SiteID, dt);
                    this.Cursor = System.Windows.Input.Cursors.Arrow;

                    PD._CurrentLoginDateTime = _CurrentLoginDateTime;
                    PD._CompanyName = _CompanyName;
                    PD._SiteName = _SiteName;
                    PD._UserName = _UserName;
                    PD._StaffPic = _StaffPic;
                    PD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    PD.ExistingMember = ExistingMember;
                    PD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    PD._MemberName = _MemberName;
                    PD.Member_Image_Img.Source = Member_Image_Img.Source;
                    PD.StaffImage.Source = StaffImage.Source;
                    PD._dSAccessRights = _dSAccessRights;
                    PD.dt = this.dt;

                    PD.Owner = this.Owner;
                    this.Close();
                    PD.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnMemberExtraDetails_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                ExtraDetails extra = new ExtraDetails();
                extra.strExtraDetailsMemberID = GetMemberDetailsMemberId;

                extra._CurrentLoginDateTime = _CurrentLoginDateTime;
                extra._CompanyName = _CompanyName;
                extra._SiteName = _SiteName;
                extra._UserName = _UserName;
                extra._StaffPic = _StaffPic;
                extra._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                extra.ExistingMember = ExistingMember;
                extra.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                extra._MemberName = _MemberName;
                extra.Member_Image_Img.Source = Member_Image_Img.Source;
                extra.StaffImage.Source = StaffImage.Source;
                extra._dSAccessRights = _dSAccessRights;
                extra.dt = this.dt;

                extra.Owner = this.Owner;
                this.Close();
                extra.ShowDialog();
            }
        }

        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                Tasks Tsk = new Tasks();
                Tsk.strMemberNo = GetMemberDetailsMemberId;
                Tsk.Owner = this.Owner;
                Tsk.SetAccessRightsDS(_dSAccessRights);

                Tsk._CurrentLoginDateTime = _CurrentLoginDateTime;
                Tsk._CompanyName = _CompanyName;
                Tsk._SiteName = _SiteName;
                Tsk._UserName = _UserName;
                Tsk._StaffPic = _StaffPic;
                Tsk._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                Tsk.ExistingMember = ExistingMember;
                Tsk.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                Tsk._MemberName = _MemberName;
                Tsk.Member_Image_Img.Source = Member_Image_Img.Source;
                Tsk.StaffImage.Source = StaffImage.Source;
                Tsk._dSAccessRights = _dSAccessRights;
                Tsk.dt = this.dt;

                this.Close();
                Tsk.ShowDialog();
            }
        }

        private void btnCommunications_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Communication _Comm = new Communication();
                _Comm.Owner = this.Owner;
                _Comm.SetAccessRightsDS(_dSAccessRights);
                this.Cursor = System.Windows.Input.Cursors.Arrow;

                _Comm._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Comm._CompanyName = _CompanyName;
                _Comm._SiteName = _SiteName;
                _Comm._UserName = _UserName;
                _Comm._StaffPic = _StaffPic;
                _Comm._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Comm.ExistingMember = ExistingMember;
                _Comm.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Comm._MemberName = _MemberName;
                _Comm.Member_Image_Img.Source = Member_Image_Img.Source;
                _Comm.StaffImage.Source = StaffImage.Source;
                _Comm._dSAccessRights = _dSAccessRights;
                _Comm.dt = this.dt;

                this.Close();
                _Comm.ShowDialog();
            }
        }

        private void btnNotes_Click(object sender, RoutedEventArgs e)
        {
            //SAME WINDOW; NO ACTION
        }

        private void btnPersonalTraining_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                PersonalTraining _pt = new PersonalTraining();
                _pt.Owner = this.Owner;

                _pt._CurrentLoginDateTime = _CurrentLoginDateTime;
                _pt._CompanyName = _CompanyName;
                _pt._SiteName = _SiteName;
                _pt._UserName = _UserName;
                _pt._StaffPic = _StaffPic;
                _pt._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _pt.ExistingMember = ExistingMember;
                _pt.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _pt._MemberName = _MemberName;
                _pt.Member_Image_Img.Source = Member_Image_Img.Source;
                _pt.StaffImage.Source = StaffImage.Source;
                _pt._dSAccessRights = _dSAccessRights;
                _pt.dt = this.dt;

                this.Close();
                _pt.ShowDialog();
            }
        }

        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberDetails mD = new MemberDetails();
                mD.Owner = this.Owner;

                mD._CurrentLoginDateTime = _CurrentLoginDateTime;
                mD._CompanyName = _CompanyName;
                mD._SiteName = _SiteName;
                mD._UserName = _UserName;
                mD._StaffPic = _StaffPic;
                mD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mD.ExistingMember = ExistingMember;
                mD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mD._MemberName = _MemberName;
                mD.Member_Image_Img.Source = Member_Image_Img.Source;
                mD.StaffImage.Source = StaffImage.Source;
                mD._dSAccessRights = _dSAccessRights;
                mD.dt = this.dt;

                this.Close();
                mD.ShowDialog();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
            gvForms.ItemsSource = null;
            gvNotes.ItemsSource = null;
            GetNotesForMember();
            GetFormsForMember();
        }
        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;

            if (_MemberName != null)
            {
                if (_MemberName.Trim() != "")
                {
                    Heading.Text = Heading.Text + " - " + _MemberName;
                }
            }
            //loadStaffPhoto(_StaffPic);
        }
        private void loadStaffPhoto(byte[] _StaffPic)
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }
        private void GetNotesForMember()
        {
            MemberDetailsBAL _memberBAL = new MemberDetailsBAL();
            _dsNotes = _memberBAL.GetNotesForMember(_CompanyID, _SiteID, GetMemberDetailsMemberId);
            if (_dsNotes != null)
            {
                if (_dsNotes.Tables[0].DefaultView.Count > 0)
                {
                    gvNotes.ItemsSource = _dsNotes.Tables[0].DefaultView;
                }
                else
                    gvNotes.ItemsSource = null;
            }
            else
                gvNotes.ItemsSource = null;
        }
        private void GetFormsForMember()
        {
            MemberDetailsBAL _memberBAL = new MemberDetailsBAL();
            _dsForms = _memberBAL.GetFormsForMember(_CompanyID, _SiteID, GetMemberDetailsMemberId);
            if (_dsForms != null)
            {
                if (_dsForms.Tables[0].DefaultView.Count > 0)
                {
                    gvForms.ItemsSource = _dsForms.Tables[0].DefaultView;
                    _SerialNo = _dsForms.Tables[0].DefaultView.Count;
                }
                else
                    gvForms.ItemsSource = null;
            }
            else
                gvForms.ItemsSource = null;
        }
        private void gvNotes_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            int _index = gvNotes.SelectedIndex;
            if (_index != -1)
            {
                txtNotes.Text = (gvNotes.Items[_index] as DataRowView).Row["Notes"].ToString().ToUpper();
                txtDate.Text = (gvNotes.Items[_index] as DataRowView).Row["NotesDate"].ToString().ToUpper();
                txtBy.Text = (gvNotes.Items[_index] as DataRowView).Row["Staff"].ToString().ToUpper();

                _isNewNotes = false;
                btnSaveNotes.IsEnabled = false;
            }
        }
        private void btnShow_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int _index = gvForms.SelectedIndex;
                if (_index == -1)
                {
                    System.Windows.MessageBox.Show("Please select a File to View...!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                else
                {
                    _MemberForms_Ext = _dsForms.Tables[0].DefaultView[_index]["Form_Ext"].ToString();
                    _MemberForms_FileData = (byte[])_dsForms.Tables[0].DefaultView[_index]["Form_FileData"];
                    _MemberForms_FileName = _dsForms.Tables[0].DefaultView[_index]["Form_FileName"].ToString();

                    //string _tmpFilePath = Constants.AppPath + "\\MemberForm" + _MemberForms_Ext;
                    string _tmpFilePath = Constants.AppPath + "\\" + _MemberForms_FileName;
                    FileStream fs = new FileStream(_tmpFilePath, FileMode.Create);
                    fs.Write(_MemberForms_FileData, 0, _MemberForms_FileData.Length);
                    fs.Close();
                    Process.Start(_tmpFilePath);
                }
            }
            catch(Exception ex)
            {
                System.Windows.MessageBox.Show("Error Viewing Member Form..!\n\n" + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private byte[] GetMemberFormRecord()
        {
            try
            {
                OpenFileDialog commonOpenFileDialog = new OpenFileDialog();
                commonOpenFileDialog.Filter = "Documents|*.pdf;*.doc;*.docx|Spread sheets|*.xls;*.xlsx|Plain Text|*.txt|PowerPoint|*.ppt;*.pptx|Pictures & Images|*.jpg;*.jpeg;*.gif;*.png;*.bmp;*.tif;*.tiff";
                commonOpenFileDialog.Title = "Select Member Forms..";
                commonOpenFileDialog.Multiselect = false;
                commonOpenFileDialog.ShowDialog();

                string fileNamePath_MemberMedRec = commonOpenFileDialog.FileName;

                if (fileNamePath_MemberMedRec != "")
                {
                    _MemberForms_FileName = commonOpenFileDialog.SafeFileName;
                    _MemberForms_Ext = Path.GetExtension(fileNamePath_MemberMedRec);

                    FileStream fs = new FileStream(fileNamePath_MemberMedRec, FileMode.Open, FileAccess.Read);
                    byte[] data = new byte[fs.Length];
                    if (Convert.ToInt32(fs.Length) > 1048576) //1024 * 1024 -> 1MB
                    {
                        System.Windows.MessageBox.Show("File Size should be less than 1MB! \nPlease Select a different File to Upload!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return null;
                    }
                    //txtMedRecFileName.Text = _MemberForms_FileName;
                    switch (_MemberForms_Ext.ToLower())
                    {
                        case ".doc":
                            _MemberForms_ContentType = "application/msword";
                            break;
                        case ".docx":
                            _MemberForms_ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                            break;
                        case ".bmp":
                            _MemberForms_ContentType = "image/bmp";
                            break;
                        case ".gif":
                            _MemberForms_ContentType = "image/gif";
                            break;
                        case ".jpeg":
                            _MemberForms_ContentType = "image/jpeg";
                            break;
                        case ".jpg":
                            _MemberForms_ContentType = "image/jpeg";
                            break;
                        case ".png":
                            _MemberForms_ContentType = "image/png";
                            break;
                        case ".tif":
                            _MemberForms_ContentType = "image/tiff";
                            break;
                        case ".tiff":
                            _MemberForms_ContentType = "image/tiff";
                            break;
                        case ".pdf":
                            _MemberForms_ContentType = "application/pdf";
                            break;
                        case ".ppt":
                            _MemberForms_ContentType = "application/vnd.ms-powerpoint";
                            break;
                        case ".pptx":
                            _MemberForms_ContentType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                            break;
                        case ".xlsx":
                            _MemberForms_ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            break;
                        case ".xls":
                            _MemberForms_ContentType = "application/vnd.ms-excel";
                            break;
                        case ".txt":
                            _MemberForms_ContentType = "text/plain";
                            break;
                        default:
                            _MemberForms_ContentType = "";
                            break;
                    }

                    fs.Read(data, 0, System.Convert.ToInt32(fs.Length));
                    fs.Close();
                    return data;
                }
                return null;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Not a Valid File!\nError Details: ( " + ex.Message + " )", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return null;
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            _MemberForms_FileData = GetMemberFormRecord();
            if (_MemberForms_FileData != null)
            {
                _SerialNo = _SerialNo + 1;

                ValidateAndAddColumns();

                DataRow dR = _dsForms.Tables[0].NewRow();
                dR["SNo"] = _SerialNo;
                dR["Form_FileName"] = _MemberForms_FileName;
                dR["Form_Ext"] = _MemberForms_Ext;
                dR["Form_ContentType"] = _MemberForms_ContentType;
                dR["Form_FileData"] = _MemberForms_FileData;
                dR["Date_Added"] = DateTime.Now.Date.ToString("dd MMM yyyy");
                dR["Existing"] = 0;

                _dsForms.Tables[0].Rows.Add(dR);

                gvForms.ItemsSource = _dsForms.Tables[0].DefaultView;
            }
        }
        private void ValidateAndAddColumns()
        {
            if (_dsForms.Tables[0].Columns.Count <= 0)
            {
                CreateColumns();
            }
        }
        private void CreateColumns()
        {
            _dsForms.Tables[0].Columns.Add("SNo", typeof(int));
            _dsForms.Tables[0].Columns.Add("Form_FileName", typeof(string));
            _dsForms.Tables[0].Columns.Add("Form_Ext", typeof(string));
            _dsForms.Tables[0].Columns.Add("Form_ContentType", typeof(string));
            _dsForms.Tables[0].Columns.Add("Form_FileData", typeof(byte[]));
            _dsForms.Tables[0].Columns.Add("Date_Added", typeof(string));
            _dsForms.Tables[0].Columns.Add("Existing", typeof(int));
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int _index = gvForms.SelectedIndex;
                if (_index == -1)
                {
                    System.Windows.MessageBox.Show("Please select a File to Remove...!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                else
                {
                    if (System.Windows.MessageBox.Show("Are you sure to Remove the Selected File ?", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        _dsForms.Tables[0].Rows.RemoveAt(_index);
                        _dsForms.Tables[0].AcceptChanges();

                        _SerialNo = _SerialNo - 1;
                        if (_SerialNo <= 0) _SerialNo = 0;

                        for (int i = 0; i < _dsForms.Tables[0].Rows.Count; i++)
                        {
                            _dsForms.Tables[0].DefaultView[i]["SNo"] = i + 1;
                        }
                        gvForms.ItemsSource = _dsForms.Tables[0].DefaultView;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error Removing Form..!\n\n" + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (gvForms.Items.Count > 0)
            {
                if (_dsForms.Tables[0].Rows.Count > 0)
                {
                    MemberDetailsBAL _memberdetailsBAL = new MemberDetailsBAL();
                    _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                    int i =_memberdetailsBAL.SaveFormsForMember(_CompanyID, _SiteID, GetMemberDetailsMemberId, _dsForms.Tables[0]);
                    if (i != 0)
                    {
                        System.Windows.MessageBox.Show("Forms/Attachments Saved Successfully!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
            }
            else
            {
                MemberDetailsBAL _memberdetailsBAL = new MemberDetailsBAL();
                _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                _memberdetailsBAL.RemoveFormsForMember(_CompanyID, _SiteID, GetMemberDetailsMemberId);

                System.Windows.MessageBox.Show("No Form available to Save!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnSaveNotes_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string _userName = ConfigurationManager.AppSettings["LoggedInUser"].ToString();
                string _Notes = txtNotes.Text.Trim().ToUpper();

                _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                ht.Add("@MemberID", GetMemberDetailsMemberId);
                ht.Add("@Reason", _Notes);
                ht.Add("@StaffMember", _userName);

                SqlHelper _sql = new SqlHelper();
                if (_sql._ConnOpen == 0) return;

                _sql.ExecuteQuery("UpdateNotes", ht);

                txtNotes.Text = "";
                _isNewNotes = false;
                btnSaveNotes.IsEnabled = false;

                GetNotesForMember();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void txtNotes_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (!_isNewNotes)
            {
                if (txtNotes.Text.Trim() != "")
                {
                    _isNewNotes = true;
                    btnSaveNotes.IsEnabled = true;
                }
                else
                {
                    _isNewNotes = false;
                    btnSaveNotes.IsEnabled = false;
                }
            }
            else if (txtNotes.Text.Trim() != "")
            {
                _isNewNotes = true;
                btnSaveNotes.IsEnabled = true;
            }
            else
            {
                _isNewNotes = false;
                btnSaveNotes.IsEnabled = false;
            }
        }
    }
}
