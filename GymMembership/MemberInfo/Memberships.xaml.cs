﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GymMembership.Task;
using GymMembership.Communications;
using GymMembership.BAL;
using GymMembership.DTO;
using GymMembership.Comman;
using System.IO;
using System.Data;
using System.Configuration;

namespace GymMembership.MemberInfo
{
    /// <summary>
    /// Interaction logic for Memberships.xaml
    /// </summary>
    public partial class Memberships 
    {
        public string _MemberName;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public bool _CanOpenMemberShipNewMember = false;
        public bool _CanOpenBillingBankNewMember = false;
        public bool ExistingMember = false;
        public int GetMemberDetailsMemberId; public long CardNumber;
        public DataSet _dSAccessRights;// = new DataSet();
        public DataTable dt = new DataTable();
        public int _CompanyID;
        public int _SiteID;
        bool _isMembershipEdited = false;

        private int _programmeGroupId = 0;
        private int _programId;
        private int _expireProgramID;
        private int _expireUniqueID = 0;

        public int v;
        private bool _isUpFront = false;
        private bool _isNeverExpire = false;

        private int _invoiceNo;
        private int _addMembershipSuccess;
        private int _addInvoiceDetailsSuccess;

        string fileNamePath_MemberPhoto;
        private string _AutoGenerateInvoiceNo = "InvoiceNo";

        private DateTime _currentDate;
        private decimal _invoiceAmount;
        private decimal _amountPaid = 0;
        private decimal _amountOutStanding = 0;
        private int _invoiceMemberId = 0;
        private string _invoiceStatus;
        private string _fieldNameType = string.Empty;
        private int _memberInvoiceNo;
        public decimal InvoiceAmount = 0m;

        public string strLnameFindmember = "";
        public bool _isFromMemberDetailsNewMember = false;
        MemberDetailsDTO _memberDetailsDTO = new MemberDetailsDTO();

        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;

            if (_MemberName != null)
            {
                if (_MemberName.Trim() != "")
                {
                    Heading.Text = Heading.Text + " - " + _MemberName;
                }
            }
            //loadStaffPhoto(_StaffPic);
        }
        private void loadStaffPhoto(byte[] _StaffPic)
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }
        public Memberships()
        {
            InitializeComponent();
        }
        private void Change_Member_Photo(object sender, MouseButtonEventArgs e)
        {
            try
            {
                OpenFileDialog commonOpenFileDialog = new OpenFileDialog();
                commonOpenFileDialog.Filter = "Image Files|*.jpeg;*.jpg;*.png;*.bmp;*.tif;*.gif;|JPEG Files|*.jpg;*.jpeg|PNG Files|*.png|BMP Files|*.bmp|TIF Files|*.tif|GIF Files|*.gif";
                commonOpenFileDialog.Title = "Select Member Profile PHOTO...";
                commonOpenFileDialog.Multiselect = false;
                commonOpenFileDialog.ShowDialog();

                fileNamePath_MemberPhoto = commonOpenFileDialog.FileName;
                if (fileNamePath_MemberPhoto != "")
                {
                    FileStream fs = new FileStream(fileNamePath_MemberPhoto, FileMode.Open, FileAccess.Read);
                    byte[] data = new byte[fs.Length];
                    if (Convert.ToInt32(fs.Length) > 250000)
                    {
                        System.Windows.MessageBox.Show("FILE SIZE EXCEEDS ALLOWED LIMIT! \nPlease Select a different Photo to Upload!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                    fs.Read(data, 0, System.Convert.ToInt32(fs.Length));
                    fs.Close();
                    _memberDetailsDTO.MemberPic = data;

                    Member_Image_Img.Source = new BitmapImage(new Uri(fileNamePath_MemberPhoto, UriKind.RelativeOrAbsolute));
                    _memberDetailsDTO.MemberProfilePhotoPath = fileNamePath_MemberPhoto;
                    //_IsImageEdited = true;
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error! NOT a Valid Image File! \nError Details: ( " + ex.Message + " )", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        
        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnSaveAndComplete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                DataTable DtMembership = GetMembership();

                if (DtMembership != null)
                {
                    _addMembershipSuccess = _memberDetailsBal.add_Membership(DtMembership);

                    if (_addMembershipSuccess != 0)
                    {
                        DataTable DtInvoiceDetails = GetInvoiceDetails();
                        RaiseMembershipInvoice();
                        DtInvoiceDetails = UpdateInvoiceDetailsInvoiceNo(DtInvoiceDetails);
                        _addInvoiceDetailsSuccess = _memberDetailsBal.AddInvoiceDetails(DtInvoiceDetails);

                        System.Windows.MessageBox.Show("Membership Added / Updated Successfully!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        _CanOpenBillingBankNewMember = true;
                        Constants.Create_ACCESS_DAT_File(false, GetMemberDetailsMemberId);
                        clearData();
                        HideControlsOnLoad();
                        DisableControlsOnLoad();

                        //GetMemberDetailsMemberId = 0;
                        ExistingMember = true;
                        btnNext.IsEnabled = true;
                        btnNext.Visibility = Visibility.Visible;

                        DataSet ds = _memberDetailsBal.get_MembershipOnExistingMember(GetMemberDetailsMemberId);
                        if (ds != null)
                        {
                            if (ds.Tables.Count > 0)
                            {
                                dt = new DataTable();
                                dt = ds.Tables[0].AsDataView().ToTable();
                            }
                        }
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Failed! Adding / Updating Membership details..\n", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error: " + ex.Message.ToString(), this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void DisableControlsOnLoad()
        {
            cmbProgramGroup.IsEnabled = false;
            cmbProgram.IsEnabled = false;
        }

        private void HideControlsOnLoad()
        {
            //has to be hide
            btnAddMembership.Visibility = Visibility.Hidden;

            btnMembershipEdit.Visibility = Visibility.Hidden;
            btnMembershipExpire.Visibility = Visibility.Hidden;
            btnMembershipHold.Visibility = Visibility.Hidden;
            btnAddPayment.Visibility = Visibility.Hidden;
            // has to be hide
            btnSaveAndComplete.Visibility = Visibility.Hidden;
        }
        public void clearData()
        {
            cmbProgramGroup.SelectedIndex = -1;
            clearMemberShipData();
        }

        private DataTable UpdateInvoiceDetailsInvoiceNo(DataTable DtInvoiceDetails)
        {
            for (int i = 0; i < DtInvoiceDetails.Rows.Count; i++)
            {
                DtInvoiceDetails.Rows[i]["InvoiceNo"] = _invoiceNo;
            }
            return DtInvoiceDetails;
        }

        private int GetMembershipInvoiceNo()
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                DataSet ds = _memberDetailsBal.get_LastUsedNoByCompanySite(_AutoGenerateInvoiceNo, _CompanyID, _SiteID);
                if (ds != null)
                {
                    _invoiceNo = Convert.ToInt32(ds.Tables[0].Rows[0]["LastUsedNo"].ToString().Trim());
                    return _invoiceNo;
                }
                else
                {
                    System.Windows.MessageBox.Show("No Data! (Invoice Number AutoGenerated)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return 0;
                }
            }
            catch (Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return 0;
            }
        }

        private void RaiseMembershipInvoice()
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                _memberInvoiceNo = GetMembershipInvoiceNo();
                if (_memberInvoiceNo != 0)
                {
                    _currentDate = DateTime.Now;

                    _invoiceAmount = Convert.ToDecimal(InvoiceAmount);

                    _amountPaid = 0m;
                    _amountOutStanding = Convert.ToDecimal(_invoiceAmount - _amountPaid);
                    _invoiceMemberId = GetMemberDetailsMemberId; //Convert.ToInt32(txtMemberNumber.Text.ToString().Trim());
                    _invoiceStatus = "Pending";
                    _fieldNameType = _AutoGenerateInvoiceNo;
                    _memberDetailsBal.raise_Invoice(_currentDate, _memberInvoiceNo, _invoiceAmount, _amountPaid, _amountOutStanding, _invoiceMemberId, _invoiceStatus, _fieldNameType);
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString(), this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private DataTable GetInvoiceDetails()
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                DataTable DtMembership = null;

                DtMembership = GetMembership();

                int InvoiceNo = _invoiceNo; //GetMembershipInvoiceNo();

                DataTable tblAddInvoiceDetails = new DataTable("InvoiceDetails");

                tblAddInvoiceDetails.Columns.Add("InvoiceNo", typeof(int));
                tblAddInvoiceDetails.Columns.Add("MemberNo", typeof(int));
                tblAddInvoiceDetails.Columns.Add("InvoiceDetails", typeof(string));
                tblAddInvoiceDetails.Columns.Add("Amount", typeof(decimal));
                tblAddInvoiceDetails.Columns.Add("Status", typeof(string));
                tblAddInvoiceDetails.Columns.Add("ProdSale", typeof(char));

                int i = 0; int j = 0;
                int ProgramGrpValue; int ProgramValue;
                DataSet ProgramDs;

                for (i = 0; i < DtMembership.Rows.Count; i++)
                {
                    if ((Convert.ToInt32((DtMembership.Rows[i]["Existing"].ToString())) == 0) && Convert.ToBoolean(DtMembership.Rows[i]["isUpFront"].ToString()))
                    {
                        DataRow dR = tblAddInvoiceDetails.NewRow();

                        ProgramGrpValue = Convert.ToInt32((DtMembership.Rows[i]["ProgramGroupValue"].ToString()));
                        ProgramValue = Convert.ToInt32((DtMembership.Rows[i]["ProgramValue"].ToString()));
                        ProgramDs = _memberDetailsBal.get_ProgrammeDetails(ProgramValue);
                        dR["ProdSale"] = 'N';
                        dR["InvoiceNo"] = InvoiceNo; //GetMembershipInvoiceNo();
                        dR["MemberNo"] = GetMemberDetailsMemberId; //Convert.ToInt32(txtMemberNumber.Text.ToString().Trim());
                        dR["InvoiceDetails"] = DtMembership.Rows[i]["Program"].ToString();
                        dR["Amount"] = Convert.ToDecimal(ProgramDs.Tables[0].Rows[0]["FullCost"].ToString());
                        dR["Status"] = "Pending";

                        if (Convert.ToBoolean(DtMembership.Rows[i]["isUpFront"].ToString()))
                        {
                            dR["ProdSale"] = 'C';
                            dR["InvoiceDetails"] = DtMembership.Rows[i]["Program"].ToString() + " [Upfront Payment]";
                        }
                        InvoiceAmount = InvoiceAmount + Convert.ToDecimal(ProgramDs.Tables[0].Rows[0]["FullCost"].ToString());

                        tblAddInvoiceDetails.Rows.Add(dR);
                    }
                }
                return tblAddInvoiceDetails;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString(), this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return null;
            }
        }
        private void btnMembershipHold_Click(object sender, RoutedEventArgs e)
        {
            int i; int programId = 0; string ss;

            i = gvMemberShip.SelectedIndex;
            if (i != -1)
            {
                programId = Convert.ToInt32((gvMemberShip.Items[i] as DataRowView).Row["ProgramValue"].ToString());

                MembershipHold mD = new MembershipHold();
                mD.strMemberNo = GetMemberDetailsMemberId;
                mD.strProgramId = programId;
                mD.MSUniqueID = _expireUniqueID;

                mD._CompanyID = _CompanyID;
                mD._SiteID = _SiteID;
                mD.Owner = this.Owner;

                mD._CurrentLoginDateTime = _CurrentLoginDateTime;
                mD._CompanyName = _CompanyName;
                mD._SiteName = _SiteName;
                mD._UserName = _UserName;
                mD._StaffPic = _StaffPic;
                mD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mD._CanOpenBillingBankNewMember = _CanOpenBillingBankNewMember;
                mD.ExistingMember = ExistingMember;
                mD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mD._MemberName = _MemberName;
                mD.Member_Image_Img.Source = Member_Image_Img.Source;
                mD.StaffImage.Source = StaffImage.Source;
                mD._dSAccessRights = _dSAccessRights;
                mD.dt = this.dt;

                this.Close();
                mD.ShowDialog();
            }
            else
            {
                System.Windows.MessageBox.Show("Select a Membership Programme below - to Appy or Remove HOLD!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private DataTable GetMembership()
        {
            try
            {
                DataTable tblAddMembership = new DataTable("Membership");

                tblAddMembership.Columns.Add("ProgramGroup", typeof(string));
                tblAddMembership.Columns.Add("Program", typeof(string));
                tblAddMembership.Columns.Add("StartDate", typeof(string));
                tblAddMembership.Columns.Add("EndDate", typeof(string));
                tblAddMembership.Columns.Add("NoOfVisits", typeof(string));
                tblAddMembership.Columns.Add("State", typeof(string));
                tblAddMembership.Columns.Add("AccessType", typeof(string));
                tblAddMembership.Columns.Add("ProgramGroupValue", typeof(string));
                tblAddMembership.Columns.Add("ProgramValue", typeof(string));
                tblAddMembership.Columns.Add("MemberId", typeof(string));
                tblAddMembership.Columns.Add("CardNo", typeof(string));
                tblAddMembership.Columns.Add("Existing", typeof(string));
                tblAddMembership.Columns.Add("FullCost", typeof(decimal));
                tblAddMembership.Columns.Add("Subscription_GUID", typeof(string));
                tblAddMembership.Columns.Add("SignUpFee", typeof(decimal));
                tblAddMembership.Columns.Add("isUpFront", typeof(bool));
                tblAddMembership.Columns.Add("isNeverExpire", typeof(bool));
                tblAddMembership.Columns.Add("UniqueID", typeof(int));

                int i = 0; int j = 0;
                for (i = 0; i < gvMemberShip.Items.Count; i++)
                {
                    DataRow dR = tblAddMembership.NewRow();
                    for (j = 0; j <= tblAddMembership.Columns.Count - 1; j++)
                    {
                        for (int jj = 0; jj < dt.Columns.Count; jj++)
                        {
                            if (tblAddMembership.Columns[j].ColumnName.ToUpper() == dt.Columns[jj].ColumnName.ToUpper())
                            {
                                if (tblAddMembership.Columns[j].DataType.Name.ToUpper() == "BOOLEAN")
                                {
                                    dR[tblAddMembership.Columns[j]] = Convert.ToBoolean((gvMemberShip.Items[i] as DataRowView).Row.ItemArray[jj].ToString());
                                }
                                else
                                {
                                    dR[tblAddMembership.Columns[j]] = (gvMemberShip.Items[i] as DataRowView).Row.ItemArray[jj].ToString();
                                }
                            }
                        }
                    }
                    tblAddMembership.Rows.Add(dR);
                }
                return tblAddMembership;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return null;
            }
        }
        private bool ValidateDuplicateMembership(int _ProgramID)
        {
            DataTable _DuplicatMemberShipValidate = GetMembership();

            for (int i = 0; i < _DuplicatMemberShipValidate.Rows.Count; i++)
            {
                if (Convert.ToInt32(_DuplicatMemberShipValidate.Rows[i]["ProgramValue"]) == _ProgramID)
                {
                    if (_DuplicatMemberShipValidate.Rows[i]["State"].ToString().ToUpper() != "EXPIRED")
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private void btnAddMembership_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!ValidateDuplicateMembership(Convert.ToInt32(cmbProgram.SelectedValue.ToString())))
                {
                    if (cmbAccessType.SelectedIndex == -1)
                    {
                        System.Windows.MessageBox.Show("Select Access Type for this Membership!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        cmbAccessType.Focus();
                        return;
                    }
                    DataRow programRow = dt.NewRow();
                    FormUtilities _formUtilities = new FormUtilities();

                    programRow["ProgramGroup"] = cmbProgramGroup.Text.ToString();
                    programRow["Program"] = cmbProgram.Text.ToString();

                    if (_formUtilities.CheckDate(txtMembershipStartDate.Text.ToString()))
                    {
                        programRow["StartDate"] = txtMembershipStartDate.Text.ToString();
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("MemberShip Start Date is Invalid!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }

                    if (_formUtilities.CheckDate(txtMembershipEndDate.Text.ToString()))
                    {
                        if (DateTime.Compare(Convert.ToDateTime(txtMembershipStartDate.Text.ToString()), Convert.ToDateTime(txtMembershipEndDate.Text.ToString())) > 0)
                        {
                            System.Windows.MessageBox.Show("Start Date CANNOT be Greater than End Date!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            return;
                        }
                        programRow["EndDate"] = txtMembershipEndDate.Text.ToString();
                    }
                    else if (txtMembershipEndDate.Text.ToString() == "0")
                    {
                        programRow["EndDate"] = "31/12/9999";
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("MemberShip End Date is Invalid!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }

                    programRow["NoOfVisits"] = txtMembershipNoOfVisits.Text.ToString();
                    programRow["State"] = "Active";
                    programRow["ProgramGroupValue"] = cmbProgramGroup.SelectedValue.ToString();
                    programRow["ProgramValue"] = cmbProgram.SelectedValue.ToString();
                    programRow["MemberId"] = GetMemberDetailsMemberId;
                    programRow["CardNo"] = CardNumber;
                    programRow["Existing"] = 0; //FOR MEMBERSHIP DUPLICATION CHECK
                    programRow["AccessType"] = cmbAccessType.Text.ToUpper(); //ACCESS TYPE, FOR ACCESS/CONTROL
                    if (txtMembershipProgramPrice.Text.Trim() == "")
                    {
                        programRow["FullCost"] = 0;
                    }
                    else
                    {
                        programRow["FullCost"] = txtMembershipProgramPrice.Text;
                    }

                    if (txtMembershipSignUpFee.Text.Trim() == "")
                    {
                        programRow["SignUpFee"] = 0;
                    }
                    else
                    {
                        programRow["SignUpFee"] = txtMembershipSignUpFee.Text;
                    }
                    programRow["isUpFront"] = _isUpFront;
                    programRow["isNeverExpire"] = _isNeverExpire;
                    programRow["UniqueID"] = 0;

                    dt.Rows.Add(programRow);
                    gvMemberShip.ItemsSource = dt.AsDataView();

                    ////Make cols Program Value -> Existing as Hidden
                    //gvMemberShip.Columns[6].Visibility = Visibility.Hidden;
                    //gvMemberShip.Columns[7].Visibility = Visibility.Hidden;
                    //gvMemberShip.Columns[8].Visibility = Visibility.Hidden;
                    //gvMemberShip.Columns[9].Visibility = Visibility.Hidden;
                    //gvMemberShip.Columns[10].Visibility = Visibility.Hidden;
                    //gvMemberShip.Columns[13].Visibility = Visibility.Hidden;
                    ////Make cols Program Value -> Existing as Hidden

                    btnSaveAndComplete.IsEnabled = true;
                    cmbProgramGroup.SelectedIndex = -1;
                    clearMemberShipData();
                }
                else
                {
                    System.Windows.MessageBox.Show("Duplicate!!\n\nMembership Programme already Exists for this Member!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnAddPayment_Click(object sender, RoutedEventArgs e)
        {
            MembershipAddPayment _mDetails = new MembershipAddPayment();
            _mDetails.Owner = this.Owner;
            _mDetails.strAddPaymentMemberID = GetMemberDetailsMemberId;

            _mDetails.ExistingMember = ExistingMember;
            _mDetails.CardNumber = CardNumber;
            _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
            _mDetails._MemberName = _MemberName;
            _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
            _mDetails._CanOpenBillingBankNewMember = _CanOpenBillingBankNewMember;
            _mDetails._CompanyName = _CompanyName;
            _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
            _mDetails._SiteName = _SiteName;
            _mDetails._StaffPic = _StaffPic;
            _mDetails._UserName = _UserName;
            _mDetails._CompanyID = _CompanyID;
            _mDetails._SiteID = _SiteID;
            _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
            _mDetails.StaffImage.Source = StaffImage.Source;
            _mDetails._dSAccessRights = _dSAccessRights;
            _mDetails.dt = this.dt;

            this.Close();
            _mDetails.ShowDialog();
        }

        private void Memberships_Loaded(object sender, RoutedEventArgs e)
        {

            SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);

            if (_isFromMemberDetailsNewMember)
            {
                btnNext.Visibility = Visibility.Visible;
            }
            else
            {
                btnNext.Visibility = Visibility.Hidden;
            }

            Utilities _utilities = new Utilities();
            _utilities.populateProgramGroup(cmbProgramGroup);
            _utilities.populateAccessTypes(cmbAccessType);

            if (GetMemberDetailsMemberId != 0)
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();

                DataSet ds = _memberDetailsBal.get_MembershipOnExistingMember(GetMemberDetailsMemberId);
                if (ds != null)
                {
                    dt = ds.Tables[0].AsDataView().ToTable();
                    gvMemberShip.ItemsSource = dt.DefaultView;


                    dt.DefaultView.RowFilter = "State <> 'EXPIRED'";
                    if (dt.DefaultView.Count > 0)
                    {
                        _CanOpenBillingBankNewMember = true;
                        dt.DefaultView.RowFilter = "";
                    }
                    else
                    {
                        _CanOpenBillingBankNewMember = false;
                        dt.DefaultView.RowFilter = "";
                    }
                }
                else
                {
                    dt = null;
                }
                ////Make cols Program Value -> Existing as Hidden
                //gvMemberShip.Columns[6].Visibility = Visibility.Hidden;
                //gvMemberShip.Columns[7].Visibility = Visibility.Hidden;
                //gvMemberShip.Columns[8].Visibility = Visibility.Hidden;
                //gvMemberShip.Columns[9].Visibility = Visibility.Hidden;
                //gvMemberShip.Columns[10].Visibility = Visibility.Hidden;
                //gvMemberShip.Columns[13].Visibility = Visibility.Hidden;
                ////Make cols Program Value -> Existing as Hidden

                ShowControlsOnAddMemberClick();

                btnMembershipEdit.Visibility = Visibility.Visible;
                btnMembershipEdit.IsEnabled = true;
                btnMembershipHold.Visibility = Visibility.Visible;
                btnMembershipHold.IsEnabled = true;
                btnAddPayment.Visibility = Visibility.Visible;
                btnAddPayment.IsEnabled = true;

                cmbProgramGroup.SelectedIndex = -1;
                clearMemberShipData();
            }
        }
        
        public void clearMemberShipData()
        {
            txtMembershipStartDate.Text = string.Empty;
            txtMembershipEndDate.Text = string.Empty;
            txtMembershipProgramPrice.Text = string.Empty;
            txtMembershipFullCost.Text = string.Empty;
            txtMembershipProgramCondition.Text = string.Empty;
            txtMembershipSignUpFee.Text = string.Empty;
            txtMembershipNoOfVisits.Text = string.Empty;

            cmbAccessType.SelectedIndex = -1;
            //gvMemberShip.ItemsSource = null;
        }

        private void ShowControlsOnAddMemberClick()
        {
            btnAddMembership.Visibility = Visibility.Visible;
            btnMembershipEdit.Visibility = Visibility.Visible;
            btnMembershipExpire.Visibility = Visibility.Visible;
            btnMembershipHold.Visibility = Visibility.Visible;
            btnAddPayment.Visibility = Visibility.Visible;
            
            btnSaveAndComplete.Visibility = Visibility.Visible;

            cmbProgramGroup.IsEnabled = true;
            cmbProgram.IsEnabled = true;
            btnAddMembership.IsEnabled = false;
            btnMembershipEdit.IsEnabled = false;
            btnMembershipExpire.IsEnabled = false;
            btnMembershipHold.IsEnabled = false;
            btnAddPayment.IsEnabled = false;
            // has to be enabled
            //btnSaveAndComplete.IsEnabled = false;
            btnSaveAndComplete.IsEnabled = false;
        }
        private void cmbProgramGroup_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Utilities _utilities = new Utilities();

                _programmeGroupId = Convert.ToInt32(cmbProgramGroup.SelectedValue);
                _utilities.get_ProgramByGroup(cmbProgram, _programmeGroupId);

                btnAddMembership.IsEnabled = false;
                btnMembershipEdit.IsEnabled = false;
                btnMembershipExpire.IsEnabled = false;
                btnMembershipHold.IsEnabled = false;
                btnAddPayment.IsEnabled = false;

                clearMemberShipData();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Program Group Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void cmbProgram_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();

                _programId = Convert.ToInt32(cmbProgram.SelectedValue);
                DataSet ds = _memberDetailsBal.get_ProgrammeDetails(_programId);

                if (_programId != 0)
                {
                    txtMembershipStartDate.Text = ds.Tables[1].Rows[0]["ProgrammeSdate"].ToString().Trim();
                    txtMembershipEndDate.Text = ds.Tables[1].Rows[0]["ProgrammeEdate"].ToString().Trim();
                    txtMembershipProgramPrice.Text = ds.Tables[0].Rows[0]["ProgrammePrice"].ToString().Trim();
                    txtMembershipFullCost.Text = ds.Tables[0].Rows[0]["FullCost"].ToString().Trim();
                    txtMembershipProgramCondition.Text = ds.Tables[0].Rows[0]["Conditions"].ToString().Trim();
                    txtMembershipSignUpFee.Text = ds.Tables[0].Rows[0]["SignUpFee"].ToString().Trim();
                    txtMembershipNoOfVisits.Text = ds.Tables[0].Rows[0]["NoOfVisits"].ToString().Trim();

                    if (ds.Tables[1].Rows[0]["ProgrammeEDate"].ToString() != "31/12/9999")
                    {
                        TimeSpan _prgDateDiff = DateTime.Parse(ds.Tables[1].Rows[0]["ProgrammeEDate"].ToString()).Subtract(DateTime.Parse(ds.Tables[1].Rows[0]["ProgrammeSDate"].ToString()));
                        double _prgDiffDays = Convert.ToDouble(_prgDateDiff.Days);

                        if (Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy")) > DateTime.Parse(ds.Tables[1].Rows[0]["ProgrammeSDate"].ToString()))
                        {
                            txtMembershipStartDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                            txtMembershipEndDate.Text = DateTime.Now.AddDays(_prgDiffDays).Date.ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            txtMembershipStartDate.Text = ds.Tables[1].Rows[0]["ProgrammeSDate"].ToString();
                        }
                    }

                    if (Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy")) > DateTime.Parse(ds.Tables[1].Rows[0]["ProgrammeSDate"].ToString()))
                    {
                        txtMembershipStartDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        txtMembershipStartDate.Text = ds.Tables[1].Rows[0]["ProgrammeSDate"].ToString();
                    }

                    ////RR:CR-UPFRONT-NEVEREXPIRE-03AUG2016
                    _isUpFront = Convert.ToBoolean(ds.Tables[0].Rows[0]["isUpFront"].ToString());
                    _isNeverExpire = Convert.ToBoolean(ds.Tables[0].Rows[0]["isNeverExpire"].ToString());
                    ////RR:CR-UPFRONT-NEVEREXPIRE-03AUG2016

                    btnAddMembership.IsEnabled = true;
                    btnMembershipEdit.IsEnabled = true;
                    btnMembershipExpire.IsEnabled = false;//false
                    btnMembershipHold.IsEnabled = true;
                    btnAddPayment.IsEnabled = true;
                    cmbAccessType.IsEnabled = true;

                    txtMembershipProgramPrice.IsEnabled = true;
                    txtMembershipSignUpFee.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);

            }
        }

        private void gvMemberShip_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (gvMemberShip.SelectedIndex != -1)
            {
                _expireProgramID = Convert.ToInt32((dt.Rows[gvMemberShip.SelectedIndex]["ProgramValue"].ToString()));

                if (dt.Rows[gvMemberShip.SelectedIndex]["UniqueID"].ToString().Trim() != "")
                {
                    _expireUniqueID = Convert.ToInt32((dt.Rows[gvMemberShip.SelectedIndex]["UniqueID"].ToString()));
                    btnMembershipExpire.IsEnabled = true;
                }
                else
                {
                    _expireUniqueID = 0;
                    btnMembershipExpire.IsEnabled = false;
                }
                btnMembershipEdit.Content = "Edit";
                _isMembershipEdited = false;

                DisableControlsForEdit();
                DisplaySelectedMembershipDetails();

                if (dt.Rows[gvMemberShip.SelectedIndex]["State"].ToString().ToUpper() == "EXPIRED")
                {
                    txtMembershipStartDate.IsEnabled = false;
                    txtMembershipEndDate.IsEnabled = false;
                    cmbAccessType.IsEnabled = false;
                    btnMembershipExpire.IsEnabled = false;

                    btnMembershipEdit.IsEnabled = false;
                    btnMembershipHold.IsEnabled = false;
                    btnMembershipHold.Content = "Membership Hold";
                }
                else
                {
                    if ( Convert.ToBoolean(dt.Rows[gvMemberShip.SelectedIndex]["isNeverExpire"].ToString()) == true)
                    {
                        btnMembershipExpire.IsEnabled = true;
                        btnMembershipEdit.IsEnabled = true;
                        cmbAccessType.IsEnabled = true;

                        txtMembershipStartDate.IsEnabled = true;
                        txtMembershipEndDate.IsEnabled = false;
                        btnMembershipHold.IsEnabled = true;
                    }
                    else
                    {
                        btnMembershipEdit.IsEnabled = true;

                        if (_expireUniqueID == 0)
                            btnMembershipExpire.IsEnabled = false;
                        else
                            btnMembershipExpire.IsEnabled = true;

                        cmbAccessType.IsEnabled = true;

                        txtMembershipStartDate.IsEnabled = true;
                        txtMembershipEndDate.IsEnabled = true;
                        btnMembershipHold.IsEnabled = true;
                    }
                    if (dt.Rows[gvMemberShip.SelectedIndex]["State"].ToString().ToUpper() == "HOLD")
                    {
                        //btnMembershipHold.IsEnabled = false;
                        btnMembershipHold.IsEnabled = true;
                        btnMembershipHold.Content = "Remove HOLD";
                    }
                    else
                    {
                        btnMembershipHold.IsEnabled = true;
                        btnMembershipHold.Content = "Membership Hold";
                    }
                }
            }
        }

        private void btnMembershipExpire_Click(object sender, RoutedEventArgs e)
        {
            ExpireMembership _expireMembership = new ExpireMembership();
            _expireMembership._MemberNo = GetMemberDetailsMemberId;
            _expireMembership._ProgrammeID = _expireProgramID;
            _expireMembership._UniqueID = _expireUniqueID;
            _expireMembership._CompanyID = _CompanyID;
            _expireMembership._SiteID = _SiteID;

            _expireMembership.Owner = this;
            _expireMembership.ShowDialog();

            ////SHOW UPDATE MEMBERSHP WITH 'EXPIRED' STATE
            MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
            DataSet ds = _memberDetailsBal.get_MembershipOnExistingMember(GetMemberDetailsMemberId);
            dt = ds.Tables[0].AsDataView().ToTable();
            gvMemberShip.ItemsSource = dt.DefaultView;
            ////SHOW UPDATE MEMBERSHP WITH 'EXPIRED' STATE
        }
        private void btnShowMembership_Click(object sender, RoutedEventArgs e)
        {
            //SAME WINDOW BUTTON; DO NOT CALL
        }

        private void btnAccounts_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberAccounts mAccounts = new MemberAccounts();
                mAccounts.MemberID = GetMemberDetailsMemberId;
                mAccounts._CurrentLoginDateTime = _CurrentLoginDateTime;
                mAccounts._CompanyName = _CompanyName;
                mAccounts._SiteName = _SiteName;
                mAccounts._UserName = _UserName;
                mAccounts._StaffPic = _StaffPic;
                mAccounts._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mAccounts.ExistingMember = ExistingMember;
                mAccounts.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mAccounts._MemberName = _MemberName;
                mAccounts.Member_Image_Img.Source = Member_Image_Img.Source;
                mAccounts.StaffImage.Source = StaffImage.Source;
                mAccounts._dSAccessRights = _dSAccessRights;
                mAccounts.dt = this.dt;

                mAccounts.Owner = this.Owner;
                this.Cursor = System.Windows.Input.Cursors.Wait;
                btnAccounts.Cursor = System.Windows.Input.Cursors.Wait;
                this.Close();
                mAccounts.ShowDialog();
            }
        }

        private void btnMemberBillingDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ExistingMember && _CanOpenBillingBankNewMember)
                {
                    this.Cursor = System.Windows.Input.Cursors.Wait;
                    PaymentDetails PD = new PaymentDetails(GetMemberDetailsMemberId, _CompanyID, _SiteID, dt);
                    this.Cursor = System.Windows.Input.Cursors.Arrow;

                    PD._CurrentLoginDateTime = _CurrentLoginDateTime;
                    PD._CompanyName = _CompanyName;
                    PD._SiteName = _SiteName;
                    PD._UserName = _UserName;
                    PD._StaffPic = _StaffPic;
                    PD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    PD._CanOpenBillingBankNewMember = _CanOpenBillingBankNewMember;
                    PD.ExistingMember = ExistingMember;
                    PD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    PD._MemberName = _MemberName;
                    PD.Member_Image_Img.Source = Member_Image_Img.Source;
                    PD.StaffImage.Source = StaffImage.Source;
                    PD._dSAccessRights = _dSAccessRights;
                    PD.dt = this.dt;

                    PD.Owner = this.Owner;
                    this.Close();
                    PD.ShowDialog();
                }
                else
                {
                    if (gvMemberShip.Items.Count > 0)
                    {
                        System.Windows.MessageBox.Show("Membership has to be Created!!\nPlease Click 'Complete and Save' after adding Membership!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Membership has to be Created!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnMemberExtraDetails_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                ExtraDetails extra = new ExtraDetails();
                extra.strExtraDetailsMemberID = GetMemberDetailsMemberId; //Convert.ToInt32(txtMemberNumber.Text.ToString().Trim());

                extra._CurrentLoginDateTime = _CurrentLoginDateTime;
                extra._CompanyName = _CompanyName;
                extra._SiteName = _SiteName;
                extra._UserName = _UserName;
                extra._StaffPic = _StaffPic;
                extra._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                extra.ExistingMember = ExistingMember;
                extra.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                extra._MemberName = _MemberName;
                extra.Member_Image_Img.Source = Member_Image_Img.Source;
                extra.StaffImage.Source = StaffImage.Source;
                extra._dSAccessRights = _dSAccessRights;
                extra.dt = this.dt;

                extra.Owner = this.Owner;
                this.Close();
                extra.ShowDialog();
            }
        }

        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                Tasks Tsk = new Tasks();
                Tsk.strMemberNo = GetMemberDetailsMemberId; //Convert.ToInt32(txtMemberNumber.Text);
                Tsk.Owner = this.Owner;
                Tsk.SetAccessRightsDS(_dSAccessRights);

                Tsk._CurrentLoginDateTime = _CurrentLoginDateTime;
                Tsk._CompanyName = _CompanyName;
                Tsk._SiteName = _SiteName;
                Tsk._UserName = _UserName;
                Tsk._StaffPic = _StaffPic;
                Tsk._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                Tsk.ExistingMember = ExistingMember;
                Tsk.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                Tsk._MemberName = _MemberName;
                Tsk.Member_Image_Img.Source = Member_Image_Img.Source;
                Tsk.StaffImage.Source = StaffImage.Source;
                Tsk._dSAccessRights = _dSAccessRights;
                Tsk.dt = this.dt;

                this.Close();
                Tsk.ShowDialog();
            }
        }

        private void btnCommunications_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Communication _Comm = new Communication();
                _Comm.Owner = this.Owner;
                _Comm.SetAccessRightsDS(_dSAccessRights);
                this.Cursor = System.Windows.Input.Cursors.Arrow;

                _Comm._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Comm._CompanyName = _CompanyName;
                _Comm._SiteName = _SiteName;
                _Comm._UserName = _UserName;
                _Comm._StaffPic = _StaffPic;
                _Comm._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Comm.ExistingMember = ExistingMember;
                _Comm.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Comm._MemberName = _MemberName;
                _Comm.Member_Image_Img.Source = Member_Image_Img.Source;
                _Comm.StaffImage.Source = StaffImage.Source;
                _Comm._dSAccessRights = _dSAccessRights;
                _Comm.dt = this.dt;

                this.Close();
                _Comm.ShowDialog();
            }
        }

        private void btnNotes_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Notes _Notes = new Notes();
                _Notes.Owner = this.Owner;

                _Notes._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Notes._CompanyName = _CompanyName;
                _Notes._SiteName = _SiteName;
                _Notes._UserName = _UserName;
                _Notes._StaffPic = _StaffPic;
                _Notes._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Notes.ExistingMember = ExistingMember;
                _Notes.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Notes._MemberName = _MemberName;
                _Notes.Member_Image_Img.Source = Member_Image_Img.Source;
                _Notes.StaffImage.Source = StaffImage.Source;
                _Notes._dSAccessRights = _dSAccessRights;
                _Notes.dt = this.dt;

                this.Cursor = System.Windows.Input.Cursors.Arrow;
                this.Close();
                _Notes.ShowDialog();
            }
        }

        private void btnPersonalTraining_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                PersonalTraining _pt = new PersonalTraining();
                _pt.Owner = this.Owner;

                _pt._CurrentLoginDateTime = _CurrentLoginDateTime;
                _pt._CompanyName = _CompanyName;
                _pt._SiteName = _SiteName;
                _pt._UserName = _UserName;
                _pt._StaffPic = _StaffPic;
                _pt._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _pt.ExistingMember = ExistingMember;
                _pt.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _pt._MemberName = _MemberName;
                _pt.Member_Image_Img.Source = Member_Image_Img.Source;
                _pt.StaffImage.Source = StaffImage.Source;
                _pt._dSAccessRights = _dSAccessRights;
                _pt.dt = this.dt;

                this.Close();
                _pt.ShowDialog();
            }
        }

        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberDetails mD = new MemberDetails();
                mD.Owner = this.Owner;

                mD._CurrentLoginDateTime = _CurrentLoginDateTime;
                mD._CompanyName = _CompanyName;
                mD._SiteName = _SiteName;
                mD._UserName = _UserName;
                mD._StaffPic = _StaffPic;
                mD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mD.ExistingMember = ExistingMember;
                mD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mD._MemberName = _MemberName;
                mD.Member_Image_Img.Source = Member_Image_Img.Source;
                mD.StaffImage.Source = StaffImage.Source;
                mD._dSAccessRights = _dSAccessRights;
                mD.dt = this.dt;

                this.Close();
                mD.ShowDialog();
            }
        }

        private void btnMembershipEdit_Click(object sender, RoutedEventArgs e)
        {
            if (gvMemberShip.SelectedIndex == -1)
            {
                System.Windows.MessageBox.Show("Please Select a Membership to Edit!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            int _Index = gvMemberShip.SelectedIndex;

            if (!_isMembershipEdited)
            {
                _isMembershipEdited = true;

                cmbProgramGroup.Text = dt.Rows[_Index]["ProgramGroup"].ToString();
                cmbProgram.Text = dt.Rows[_Index]["Program"].ToString();

                txtMembershipStartDate.Text = dt.Rows[_Index]["StartDate"].ToString();
                txtMembershipEndDate.Text = dt.Rows[_Index]["EndDate"].ToString();
                txtMembershipNoOfVisits.Text = dt.Rows[_Index]["NoOfVisits"].ToString();
                cmbAccessType.Text = dt.Rows[_Index]["AccessType"].ToString();
                //txtMembershipFullCost.Text = dt.Rows[_Index]["FullCost"].ToString();
                txtMembershipProgramPrice.Text = dt.Rows[_Index]["FullCost"].ToString();
                txtMembershipSignUpFee.Text = dt.Rows[_Index]["SignUpFee"].ToString();

                DisableControlsForEdit();
                btnAddMembership.IsEnabled = false;
                btnMembershipEdit.Content = "Update";
            }
            else
            {
                FormUtilities _formUtilities = new FormUtilities();

                if (!_formUtilities.CheckDate(txtMembershipStartDate.Text.ToString()))
                {
                    System.Windows.MessageBox.Show("MemberShip Start Date is Invalid!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtMembershipStartDate.Focus();
                    return;
                }

                if (_formUtilities.CheckDate(txtMembershipEndDate.Text.ToString()))
                { 
                    if (DateTime.Compare(Convert.ToDateTime(txtMembershipStartDate.Text.ToString()), Convert.ToDateTime(txtMembershipEndDate.Text.ToString())) > 0)
                    {
                        System.Windows.MessageBox.Show("Start Date CANNOT be Greater than End Date!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        txtMembershipStartDate.Focus();
                        return;
                    }
                }
                else if (txtMembershipEndDate.Text.ToString() == "0")
                {
                    dt.Rows[_Index]["EndDate"] = "31/12/9999";
                }
                else
                {
                    System.Windows.MessageBox.Show("MemberShip End Date is Invalid!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtMembershipEndDate.Focus();
                    return;
                }
                dt.Rows[_Index]["StartDate"] = txtMembershipStartDate.Text.ToString();
                dt.Rows[_Index]["EndDate"] = txtMembershipEndDate.Text.ToString();
                dt.Rows[_Index]["AccessType"] = cmbAccessType.Text.ToString();

                _isMembershipEdited = false;
                btnMembershipEdit.Content = "Edit";
                EnableControlsOnUpdate();
                gvMemberShip.SelectedIndex = -1;
                btnSaveAndComplete.IsEnabled = true;

                System.Windows.MessageBox.Show("Click \"Complete & Save\" To Update Changes..", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void DisableControlsForEdit()
        {
            cmbProgramGroup.IsEnabled = false;
            cmbProgram.IsEnabled = false;
            txtMembershipProgramPrice.IsEnabled = false;
            txtMembershipSignUpFee.IsEnabled = false;
            txtMembershipFullCost.IsEnabled = false;
            txtMembershipNoOfVisits.IsEnabled = false;
            txtMembershipProgramCondition.IsEnabled = false;

            txtMembershipStartDate.IsEnabled = true;
            txtMembershipEndDate.IsEnabled = true;

            btnAddMembership.IsEnabled = true;
        }
        private void DisableControlsForBeforeEdit()
        {
            cmbProgramGroup.IsEnabled = true;
            cmbProgram.IsEnabled = true;
            txtMembershipProgramPrice.IsEnabled = false;
            txtMembershipSignUpFee.IsEnabled = false;
            txtMembershipFullCost.IsEnabled = false;
            txtMembershipNoOfVisits.IsEnabled = false;
            txtMembershipProgramCondition.IsEnabled = false;

            txtMembershipStartDate.IsEnabled = false;
            txtMembershipEndDate.IsEnabled = false;

            btnAddMembership.IsEnabled = false;
        }
        private void EnableControlsOnUpdate()
        {
            cmbProgramGroup.IsEnabled = true;
            cmbProgram.IsEnabled = true;
            txtMembershipProgramPrice.IsEnabled = true;
            txtMembershipSignUpFee.IsEnabled = true;
            txtMembershipFullCost.IsEnabled = true;
            txtMembershipNoOfVisits.IsEnabled = true;
            txtMembershipProgramCondition.IsEnabled = true;
            btnAddMembership.IsEnabled = false;

            btnMembershipEdit.IsEnabled = false;
        }

        private void DisplaySelectedMembershipDetails()
        {
            if (gvMemberShip.SelectedIndex == -1)
            {
                System.Windows.MessageBox.Show("Please Select a Membership to Edit (or) Expire!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            int _Index = gvMemberShip.SelectedIndex;
            
            if (!_isMembershipEdited)
            {
                cmbProgramGroup.Text = dt.Rows[_Index]["ProgramGroup"].ToString();
                cmbProgram.Text = dt.Rows[_Index]["Program"].ToString();

                txtMembershipStartDate.Text = dt.Rows[_Index]["StartDate"].ToString();
                txtMembershipEndDate.Text = dt.Rows[_Index]["EndDate"].ToString();
                txtMembershipNoOfVisits.Text = dt.Rows[_Index]["NoOfVisits"].ToString();
                cmbAccessType.Text = dt.Rows[_Index]["AccessType"].ToString();
                //txtMembershipFullCost.Text = dt.Rows[_Index]["FullCost"].ToString();
                txtMembershipProgramPrice.Text = dt.Rows[_Index]["FullCost"].ToString();
                txtMembershipSignUpFee.Text = dt.Rows[_Index]["SignUpFee"].ToString();

                DisableControlsForBeforeEdit();
            }
        }

        private void txtMembershipProgramPrice_TextChanged(object sender, TextChangedEventArgs e)
        {
            double _fullCost = 0, _programPrice = 0, _signUpFee = 0;

            if (txtMembershipProgramPrice.Text.Trim() == "")
            {
                _programPrice = 0;
            }
            if (txtMembershipSignUpFee.Text.Trim() == "")
            {
                _signUpFee = 0;
            }
            if (Regex.IsMatch(txtMembershipProgramPrice.Text, @"^\d{1,7}(\.\d{1,2})?$"))
            {
                _programPrice = Convert.ToDouble(txtMembershipProgramPrice.Text.Trim());
            }
            if (Regex.IsMatch(txtMembershipSignUpFee.Text, @"^\d{1,7}(\.\d{1,2})?$"))
            {
                _signUpFee = Convert.ToDouble(txtMembershipSignUpFee.Text.Trim());
            }
            _fullCost = _programPrice + _signUpFee;
            txtMembershipFullCost.Text = _fullCost.ToString("0.00");
        }

        private void txtMembershipSignUpFee_TextChanged(object sender, TextChangedEventArgs e)
        {
            double _fullCost = 0, _programPrice = 0, _signUpFee = 0;

            if (txtMembershipProgramPrice.Text.Trim() == "")
            {
                _programPrice = 0;
            }
            if (txtMembershipSignUpFee.Text.Trim() == "")
            {
                _signUpFee = 0;
            }
            if (Regex.IsMatch(txtMembershipProgramPrice.Text, @"^\d{1,7}(\.\d{1,2})?$"))
            {
                _programPrice = Convert.ToDouble(txtMembershipProgramPrice.Text.Trim());
            }
            if (Regex.IsMatch(txtMembershipSignUpFee.Text, @"^\d{1,7}(\.\d{1,2})?$"))
            {
                _signUpFee = Convert.ToDouble(txtMembershipSignUpFee.Text.Trim());
            }
            _fullCost = _programPrice + _signUpFee;
            txtMembershipFullCost.Text = _fullCost.ToString("0.00");
        }
    }
}
