﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GymMembership.MemberInfo;
using System.Configuration;
using GymMembership.BAL;
using GymMembership.Comman;
using System.Data;

namespace GymMembership.MemberInfo
{
    /// <summary>
    /// Interaction logic for CheckIn.xaml
    /// </summary>
    public partial class CheckIn 
    {
        public int _addSuccess;
        public int UsedVisits =0;
        public int UserVisited;
        public int _CompanyID;
        public int _SiteID;
        public int _programmeId;
        public DateTime CheckInDate = DateTime.Now;//DateTime.Today;
        public int GetMemberDetailsMemberId; public long CardNumber;
        public string _MemberName;
        public byte[] _data; //(byte[])MemberDs.Tables[0].Rows[0]["MemberPic"];
        Utilities _utilites = new Utilities();
        MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
        DataTable _dtMembership;
        public CheckIn(DataTable _dtM)
        {
            InitializeComponent();
            _dtMembership = _dtM;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (ConfigurationManager.AppSettings["LoggedInOrgID"] != "")
            {
                _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
            }
            if (GetMemberDetailsMemberId != 0)
            {
                txtmemberName.Text = _MemberName;
            }
            //_utilites.populateActiveMembership(cmbSelectMembership, GetMemberDetailsMemberId, _CompanyID, _SiteID);
            cmbSelectMembership.ItemsSource = null;
            cmbSelectMembership.ItemsSource = _dtMembership.DefaultView;
            cmbSelectMembership.DisplayMemberPath = "ProgrammeName";
            cmbSelectMembership.SelectedValuePath = "ProgrammeID";
        }
      
        private void btnCheckIn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ValidateData())
                {
                    _programmeId = Convert.ToInt32((cmbSelectMembership.SelectedValue).ToString());

                    //DASHBOARD CHECK-IN
                    for (int i = 0; i < Application.Current.Windows.Count; i++)
                    {
                        if (Application.Current.Windows[i].Name.ToUpper() == "DASHBOARD")
                        {
                            (Application.Current.Windows[i] as Navigation.Dashboard)._checkInProgrammeID = _programmeId;
                            break;
                        }
                    }
                    this.Close();
                    //DASHBOARD CHECK-IN

                    //DataSet ds = _memberDetailsBal.get_VisitsCount(GetMemberDetailsMemberId, _CompanyID, _SiteID, _programmeId);
                    //if (ds != null)
                    //{
                    //    if (!Convert.IsDBNull(ds.Tables[0].Rows[0]["Visits"]))
                    //    {
                    //        UsedVisits = Convert.ToInt32(ds.Tables[0].Rows[0]["Visits"].ToString());
                    //        if (UsedVisits >= Convert.ToInt32(ds.Tables[0].Rows[0]["TotalVisitsAllowed"].ToString()))
                    //        {
                    //            MessageBox.Show("Allowed No. Of Visits has been Exhausted!", this.Title, MessageBoxButton.OK, MessageBoxImage.Stop);
                    //            return;
                    //        }
                    //    }
                    //}
                    //DataSet ds1;
                    //DataSet ds2 = _memberDetailsBal.GetCheckInDetails(GetMemberDetailsMemberId, _CompanyID, _SiteID, _programmeId);
                    //if (ds2 != null)
                    //{
                    //    if (ds2.Tables[0].DefaultView.Count > 0)
                    //    {
                    //        DateTime _checkInDate = Convert.ToDateTime(ds2.Tables[0].Rows[0]["CheckInDate"].ToString());
                    //        if (CheckInDate.Date.ToString() != _checkInDate.Date.ToString())
                    //        {
                    //            ds1 = _memberDetailsBal.SaveCheckInDetails(GetMemberDetailsMemberId, _CompanyID, _SiteID, _programmeId, CheckInDate, cmbDoortoCheckIn.Text.ToUpper());
                    //            if (!Convert.IsDBNull(ds.Tables[0].Rows[0]["Visits"]))
                    //            {
                    //                UsedVisits = Convert.ToInt32(ds.Tables[0].Rows[0]["Visits"].ToString());
                    //            }
                    //            UserVisited = UsedVisits + 1;
                    //            _addSuccess = _memberDetailsBal.Update_usedVisits(GetMemberDetailsMemberId, _CompanyID, _SiteID, UserVisited, _programmeId);

                    //            Constants.Popup_dispatcherTimer_Tick();
                    //            this.Close();
                    //        }
                    //        else
                    //        {
                    //            MessageBox.Show("Member Already Checked-In For this Membership!", this.Title, MessageBoxButton.OK, MessageBoxImage.Stop);
                    //            return;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        ds1 = _memberDetailsBal.SaveCheckInDetails(GetMemberDetailsMemberId, _CompanyID, _SiteID, _programmeId, CheckInDate, cmbDoortoCheckIn.Text.ToUpper());
                    //        if (!Convert.IsDBNull(ds.Tables[0].Rows[0]["Visits"]))
                    //        {
                    //            UsedVisits = Convert.ToInt32(ds.Tables[0].Rows[0]["Visits"].ToString());
                    //        }
                    //        UserVisited = UsedVisits + 1;
                    //        _addSuccess = _memberDetailsBal.Update_usedVisits(GetMemberDetailsMemberId, _CompanyID, _SiteID, UserVisited, _programmeId);

                    //        Constants.Popup_dispatcherTimer_Tick();

                    //        this.Close();
                    //    }
                    //}
                }
            }
            catch(Exception)
            { }
        }
        private void btnOpenDoor_Click(object sender, RoutedEventArgs e)
        {
          
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            //DASHBOARD CHECK-IN
            for (int i = 0; i < Application.Current.Windows.Count; i++)
            {
                if (Application.Current.Windows[i].Name.ToUpper() == "DASHBOARD")
                {
                    (Application.Current.Windows[i] as Navigation.Dashboard)._checkInProgrammeID = 0;
                    break;
                }
            }
            //DASHBOARD CHECK-IN

            this.Close();
        }
        private bool ValidateData()
        {
            if (txtmemberName.Text.Trim() == "")
            {
                MessageBox.Show("No Member Has been Selected!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            if (cmbSelectMembership.Items.Count <= 0)
            {
                MessageBox.Show("There are NO Active Memberships!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            //if (cmbDoortoCheckIn.SelectedIndex == -1)
            //{
            //    MessageBox.Show("Select Door to Check-In!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //    return false;
            //}
            return true;
        }
    }
}
