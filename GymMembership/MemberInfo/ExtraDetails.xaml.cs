﻿using System;
using System.IO;
using System.Windows.Media.Imaging;
using System.Linq;
using System.Windows;
using System.Diagnostics;
using System.Windows.Forms;
using System.Data;
using System.Configuration;

using GymMembership.Task;
using GymMembership.Communications;
using GymMembership.Comman;
using GymMembership.DTO;
using GymMembership.BAL;

namespace GymMembership.MemberInfo
{
    /// <summary>
    /// Interaction logic for ExtraDetails.xaml
    /// </summary>
    public partial class ExtraDetails 
    {
        public int strExtraDetailsMemberID = 0;
        public string _MemberName;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public bool _CanOpenMemberShipNewMember = false;
        public bool ExistingMember = false;
        public int GetMemberDetailsMemberId; public long CardNumber;
        public DataSet _dSAccessRights;
        public DataTable dt = new DataTable();

        public int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
        public int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

        byte[] _MedRec_FileData = null;
        string _MedRec_FileName;
        string _MedRec_Ext;
        string _MedRec_ContentType;
        public ExtraDetails()
        {
            InitializeComponent();
        }

        MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
        Utilities _utilites = new Utilities();
        MembersAddlInfoDTO _membersAddlInfoDTO = new MembersAddlInfoDTO();
        private bool reval;
        private int _addExtraInfoSuccess;

        //TextRange PostalAddresstextRange;
        //TextRange BillingAddresstextRange;
        private void AddMemberAddlnfo_Loaded(object sender, RoutedEventArgs e)
        {
            SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
            clearData();
            populateData();
            ShowExtraMemberDetails();
            txtFirstContactName.Focus();
        }

        private void HideControlsOnLoad()
        {
            btnAddInfo.Visibility = Visibility.Hidden;
            btnUpdateInfo.Visibility = Visibility.Hidden;
        }

        public void clearData()
        {
            ClearEmergencyDetails();
            ClearMedicalDetails();
            ClearJoiningDetails();
            ClearWebAccessDetails();
            ClearContactPreferenceDetails();
            btnShowMedRec.IsEnabled = false;
        }
        public void ClearEmergencyDetails()
        {
            txtFirstContactName.Text = string.Empty;
            txtFirstContactHomePhone.Text = string.Empty;
            txtFirstContactCellPhone.Text = string.Empty;
            txtFirstContactWorkPhone.Text = string.Empty;

            txtAlternativeContactName.Text = string.Empty;
            txtAlternativeContactHomePhone.Text = string.Empty;
            txtAlternativeContactCellPhone.Text = string.Empty;
            txtAlternativeContactWorkPhone.Text = string.Empty;
        }
        public void ClearMedicalDetails()
        {
            txtMedicalConditions.Text = string.Empty;
            txtDoctorName.Text = string.Empty;
            txtDoctorPhone.Text = string.Empty;
            txtMedication.Text = string.Empty;
            txtMedRecFileName.Text = string.Empty;
        }
        public void ClearJoiningDetails()
        {
            txtJoiningDate.Text = string.Empty;
            txtJoiningDetailsReferredby.Text = string.Empty;
            txtJoiningLastUpdate.Text = string.Empty;
            txtJoiningDetailsPostalAddress.Text = string.Empty;
            txtJoiningDetailsBillingAddress.Text = string.Empty;
            txtJoiningDetailsAge.Text = string.Empty;
        }
        public void ClearWebAccessDetails()
        {
            txtWebAccessAccountEmail.Text = string.Empty;
            txtWebAccessAccountPassword.Password = string.Empty;

        }
        public void ClearContactPreferenceDetails()
        {
            chkAgreedtoReceivemails.IsChecked = false;
            chkAgreedtoReceiveLetters.IsChecked = false;
            chkAgreedtoReceiveSMS.IsChecked = false;
            chkAgreedtoReceivePushNotification.IsChecked = false;
        }

        public void populateData()
        {
            try
            {
                _utilites.populatePersonalTrainer(cmbJoiningDetailsClientManager);
                _utilites.populateBusinessPromotionDetails(cmbJoiningDetailsJoiningPromotion);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void AddUpdateData()
        {
            try
            {
                if (validateData())
                {
                    _membersAddlInfoDTO.MemberId = strExtraDetailsMemberID;
                    _membersAddlInfoDTO.FirstName = txtFirstContactName.Text.ToString();
                    _membersAddlInfoDTO.HomePhone = txtFirstContactHomePhone.Text.ToString().Trim();
                    _membersAddlInfoDTO.CellPhone = txtFirstContactCellPhone.Text.ToString().Trim();
                    _membersAddlInfoDTO.WorkPhone = txtFirstContactWorkPhone.Text.ToString().Trim();
                    _membersAddlInfoDTO.AlternateContactName = txtAlternativeContactName.Text.ToString();
                    _membersAddlInfoDTO.AlternateHomePhone = txtAlternativeContactHomePhone.Text.ToString().Trim();
                    _membersAddlInfoDTO.AlternateCellPhone = txtAlternativeContactCellPhone.Text.ToString().Trim();
                    _membersAddlInfoDTO.AlternateWorkPhone = txtAlternativeContactWorkPhone.Text.ToString().Trim();
                    _membersAddlInfoDTO.MedCondition = txtMedicalConditions.Text.ToString();
                    _membersAddlInfoDTO.DoctorName = txtDoctorName.Text.ToString();
                    _membersAddlInfoDTO.DoctorPhone = txtDoctorPhone.Text.ToString().Trim();
                    _membersAddlInfoDTO.Medication = txtMedication.Text.ToString();

                    if (txtJoiningDate.Text.Trim() != "")
                    {
                        _membersAddlInfoDTO.JoiningDt = Convert.ToDateTime(txtJoiningDate.Text.ToString().Trim());
                    }
                    else
                    {
                        _membersAddlInfoDTO.JoiningDt = DateTime.Now;
                    }
                    _membersAddlInfoDTO.ReferredBy = txtJoiningDetailsReferredby.Text.ToString();
                    _membersAddlInfoDTO.JoiningPromotion = Convert.ToInt32(cmbJoiningDetailsJoiningPromotion.SelectedIndex.ToString().Trim());
                    _membersAddlInfoDTO.ClientMgr = Convert.ToInt32(cmbJoiningDetailsClientManager.SelectedIndex.ToString().Trim());
                    _membersAddlInfoDTO.LastUpdate = txtJoiningLastUpdate.Text.ToString();
                    _membersAddlInfoDTO.PostalAddress = txtJoiningDetailsPostalAddress.Text.ToString();
                    _membersAddlInfoDTO.BillingAddress = txtJoiningDetailsBillingAddress.Text.ToString();
                    if (txtJoiningDetailsAge.Text.Trim() != "")
                    {
                        _membersAddlInfoDTO.Age = Convert.ToInt32(txtJoiningDetailsAge.Text.ToString());
                    }
                    else
                    {
                        _membersAddlInfoDTO.Age = 0;
                    }
                    _membersAddlInfoDTO.Email = txtWebAccessAccountEmail.Text.ToString();
                    _membersAddlInfoDTO.Password = txtWebAccessAccountPassword.Password.ToString();
                    _membersAddlInfoDTO.IsReceiveEmail = Convert.ToBoolean(chkAgreedtoReceivemails.IsChecked.ToString());
                    _membersAddlInfoDTO.IsReceiveLetter = Convert.ToBoolean(chkAgreedtoReceiveLetters.IsChecked.ToString());
                    _membersAddlInfoDTO.IsReceiveSMS = Convert.ToBoolean(chkAgreedtoReceiveSMS.IsChecked.ToString());
                    _membersAddlInfoDTO.IsReceivePushNotification = Convert.ToBoolean(chkAgreedtoReceivePushNotification.IsChecked.ToString());

                    _membersAddlInfoDTO.MedRec_Ext = _MedRec_Ext;
                    _membersAddlInfoDTO.MedRec_ContentType = _MedRec_ContentType;
                    _membersAddlInfoDTO.MedRec_FileName = _MedRec_FileName;
                    _membersAddlInfoDTO.MedRec_FileData = _MedRec_FileData;

                    _addExtraInfoSuccess = _memberDetailsBal.add_MemberExtraInfo(_membersAddlInfoDTO);
                    clearData();
                    System.Windows.MessageBox.Show("Additional Information Added / Updated!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnAddInfo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddUpdateData();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        private bool validateData()
        {
            reval = true;

            if (string.IsNullOrEmpty(txtFirstContactName.Text.ToString()))
            {
                System.Windows.MessageBox.Show("Please Enter First Contact Name", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtFirstContactName.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtFirstContactHomePhone.Text.ToString()))
            {
                //System.Windows.MessageBox.Show("Please Enter First Contact Phone No.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                //txtFirstContactHomePhone.Focus();
                //reval = false;
                //return reval;
            }
            else
            {
                if (!txtFirstContactHomePhone.Text.All(c => Char.IsNumber(c)))
                {
                    System.Windows.MessageBox.Show("Please Enter First Contact Phone No in Numbers", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                    txtFirstContactHomePhone.Focus();
                    reval = false;
                    txtFirstContactHomePhone.Text = string.Empty;
                    return reval;
                }
            }

            if (string.IsNullOrEmpty(txtFirstContactCellPhone.Text.ToString()))
            {
                System.Windows.MessageBox.Show("Please Enter First Contact Mobile No.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                txtFirstContactCellPhone.Focus();
                reval = false;
                return reval;
            }

            if (!txtFirstContactCellPhone.Text.All(c => Char.IsNumber(c)))
            {
                System.Windows.MessageBox.Show("Please Enter First Contact Mobile in Numbers.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                txtFirstContactCellPhone.Focus();
                reval = false;
                txtFirstContactCellPhone.Text = string.Empty;
                return reval;
            }

            if (string.IsNullOrEmpty(txtFirstContactWorkPhone.Text.ToString()))
            {
                //System.Windows.MessageBox.Show("Please Enter First Contact Work Phone No.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                //txtFirstContactWorkPhone.Focus();
                //reval = false;
                //return reval;
            }
            else
            {
                if (!txtFirstContactWorkPhone.Text.All(c => Char.IsNumber(c)))
                {
                    System.Windows.MessageBox.Show("Please Enter First Contact Work Phone in Numbers.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                    txtFirstContactWorkPhone.Focus();
                    reval = false;
                    txtFirstContactWorkPhone.Text = string.Empty;
                    return reval;
                }
            }


            if (string.IsNullOrEmpty(txtAlternativeContactName.Text.ToString()))
            {
                System.Windows.MessageBox.Show("Please Enter Alternate Contact Name.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                txtAlternativeContactName.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtAlternativeContactHomePhone.Text.ToString()))
            {
                //System.Windows.MessageBox.Show("Please Enter Alternate Contact Phone No.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                //txtAlternativeContactHomePhone.Focus();
                //reval = false;
                //return reval;
            }
            else
            {
                if (!txtAlternativeContactHomePhone.Text.All(c => Char.IsNumber(c)))
                {
                    System.Windows.MessageBox.Show("Please Enter Alternate Contact Phone No in Numbers.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                    txtAlternativeContactHomePhone.Focus();
                    reval = false;
                    txtAlternativeContactHomePhone.Text = string.Empty;
                    return reval;
                }
            }

            if (string.IsNullOrEmpty(txtAlternativeContactCellPhone.Text.ToString()))
            {
                System.Windows.MessageBox.Show("Please Enter Alternate Contact Mobile No.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                txtAlternativeContactCellPhone.Focus();
                reval = false;
                return reval;
            }
            else
            {
                if (!txtAlternativeContactCellPhone.Text.All(c => Char.IsNumber(c)))
                {
                    System.Windows.MessageBox.Show("Please Enter Alternate Contact Mobile in Numbers", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                    txtAlternativeContactCellPhone.Focus();
                    reval = false;
                    txtAlternativeContactCellPhone.Text = string.Empty;
                    return reval;
                }
            }

            if (string.IsNullOrEmpty(txtAlternativeContactWorkPhone.Text.ToString()))
            {
                //System.Windows.MessageBox.Show("Please Alternate Contact Work Phone No.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                //txtAlternativeContactWorkPhone.Focus();
                //reval = false;
                //return reval;
            }
            else
            {
                if (!txtAlternativeContactWorkPhone.Text.All(c => Char.IsNumber(c)))
                {
                    System.Windows.MessageBox.Show("Please Enter Alternate Contact Work Phone in Numbers.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                    txtAlternativeContactWorkPhone.Focus();
                    reval = false;
                    txtAlternativeContactWorkPhone.Text = string.Empty;
                    return reval;
                }
            }

            //if (string.IsNullOrEmpty(txtMedicalConditions.Text.ToString()))
            //{
            //    System.Windows.MessageBox.Show("Please Enter Medical Condition.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
            //    txtMedicalConditions.Focus();
            //    reval = false;
            //    return reval;
            //}

            if (string.IsNullOrEmpty(txtDoctorName.Text.ToString()))
            {
                //System.Windows.MessageBox.Show("Please Enter Doctor Name.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                //txtDoctorName.Focus();
                //reval = false;
                //return reval;
            }
            else
            {
                if (string.IsNullOrEmpty(txtDoctorPhone.Text.ToString()))
                {
                    System.Windows.MessageBox.Show("Please Enter Doctor Phone.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                    txtDoctorPhone.Focus();
                    reval = false;
                    return reval;
                }
                else
                {
                    if (!txtDoctorPhone.Text.All(c => Char.IsNumber(c)))
                    {
                        System.Windows.MessageBox.Show("Please Enter Doctor Phone No in Numbers.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                        txtDoctorPhone.Focus();
                        reval = false;
                        txtDoctorPhone.Text = string.Empty;
                        return reval;
                    }
                }
            }

            if (string.IsNullOrEmpty(txtJoiningDetailsAge.Text.ToString()))
            {
                //System.Windows.MessageBox.Show("Please Enter Age.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                //txtJoiningDetailsAge.Focus();
                //reval = false;
                //return reval;
            }
            else
            {
                if (!txtJoiningDetailsAge.Text.All(c => Char.IsNumber(c)))
                {
                    System.Windows.MessageBox.Show("Please Enter Age in Numbers.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                    txtJoiningDetailsAge.Focus();
                    reval = false;
                    txtJoiningDetailsAge.Text = string.Empty;
                    return reval;
                }
            }

            //if (string.IsNullOrEmpty(txtMedication.Text.ToString()))
            //{
            //    System.Windows.MessageBox.Show("Please Enter Medication.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
            //    txtMedication.Focus();
            //    reval = false;
            //    return reval;
            //}

            if (string.IsNullOrEmpty(txtJoiningDate.Text.ToString()))
            {
                //System.Windows.MessageBox.Show("Please Enter Date of Join.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
                //txtJoiningDate.Focus();
                //reval = false;
                //return reval;
            }
            else
            {
                FormUtilities _formUtilities = new FormUtilities();
                if (_formUtilities.CheckDate(txtJoiningDate.Text) == false)
                {
                    System.Windows.MessageBox.Show("Please Enter Joining Date in dd/mm/yyyy format! OR Leave blank..", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtJoiningDate.Focus();
                    reval = false;
                    return reval;
                }

                DateTime dt = DateTime.Parse(txtJoiningDate.Text);
                if (dt > DateTime.Now.Date)
                {
                    System.Windows.MessageBox.Show("Joining Date Cannot be Future Date!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtJoiningDate.Focus();
                    reval = false;
                    return reval;
                }
            }

            //if (string.IsNullOrEmpty(txtJoiningDetailsReferredby.Text.ToString()))
            //{
            //    System.Windows.MessageBox.Show("Please Enter Referred By.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
            //    txtJoiningDetailsReferredby.Focus();
            //    reval = false;
            //    return reval;
            //}

            //if (string.IsNullOrEmpty(txtJoiningLastUpdate.Text.ToString()))
            //{
            //    System.Windows.MessageBox.Show("Please Enter Last Update.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
            //    txtJoiningLastUpdate.Focus();
            //    reval = false;
            //    return reval;
            //}

            //if (string.IsNullOrEmpty(txtWebAccessAccountEmail.Text.ToString()))
            //{
            //    System.Windows.MessageBox.Show("Please Enter E-Mail.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
            //    txtWebAccessAccountEmail.Focus();
            //    reval = false;
            //    return reval;
            //}


            //if (string.IsNullOrEmpty(txtWebAccessAccountPassword.Password.ToString()))
            //{
            //    System.Windows.MessageBox.Show("Please Enter Password.", this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
            //    txtWebAccessAccountPassword.Focus();
            //    reval = false;
            //    return reval;
            //}

            return true;
        }

        private void ShowExtraMemberDetails()
        {
            try
            {
                if (strExtraDetailsMemberID != 0)
                {
                    DataSet ExtraDetailsDs = _memberDetailsBal.get_ExtraDetailsByMemberId(strExtraDetailsMemberID);
                    if (ExtraDetailsDs != null)
                    {
                        if (ExtraDetailsDs.Tables.Count > 0)
                        {
                            DataTable ExtraDetailsDt = ExtraDetailsDs.Tables[0];
                            //DataRow ExtraDetailsDr = ExtraDetailsDt.Rows[0];

                            if (ExtraDetailsDt.Rows.Count > 0)
                            {
                                txtFirstContactName.Text = ExtraDetailsDs.Tables[0].Rows[0]["FirstContact"].ToString();
                                txtFirstContactHomePhone.Text = ExtraDetailsDs.Tables[0].Rows[0]["Cont_HomePhone"].ToString();
                                txtFirstContactCellPhone.Text = ExtraDetailsDs.Tables[0].Rows[0]["Cont_MobilePhone"].ToString();
                                txtFirstContactWorkPhone.Text = ExtraDetailsDs.Tables[0].Rows[0]["Cont_WorkPhone"].ToString();
                                txtAlternativeContactName.Text = ExtraDetailsDs.Tables[0].Rows[0]["AlternateContact"].ToString();
                                txtAlternativeContactHomePhone.Text = ExtraDetailsDs.Tables[0].Rows[0]["Alt_HomePhone"].ToString();
                                txtAlternativeContactCellPhone.Text = ExtraDetailsDs.Tables[0].Rows[0]["Alt_MobilePhone"].ToString();
                                txtAlternativeContactWorkPhone.Text = ExtraDetailsDs.Tables[0].Rows[0]["Alt_WorkPhone"].ToString();
                                txtMedicalConditions.Text = ExtraDetailsDs.Tables[0].Rows[0]["MedicalConditons"].ToString();
                                txtDoctorName.Text = ExtraDetailsDs.Tables[0].Rows[0]["DoctorName"].ToString();
                                txtDoctorPhone.Text = ExtraDetailsDs.Tables[0].Rows[0]["DoctorPhone"].ToString();
                                txtMedication.Text = ExtraDetailsDs.Tables[0].Rows[0]["Medication"].ToString();
                                txtJoiningDate.Text = ExtraDetailsDs.Tables[0].Rows[0]["JoiningDate"].ToString();
                                txtJoiningDetailsReferredby.Text = ExtraDetailsDs.Tables[0].Rows[0]["ReferedBy"].ToString();
                                cmbJoiningDetailsJoiningPromotion.SelectedIndex = Convert.ToInt32(ExtraDetailsDs.Tables[0].Rows[0]["JoiningPromotion"].ToString());
                                cmbJoiningDetailsClientManager.SelectedIndex = Convert.ToInt32(ExtraDetailsDs.Tables[0].Rows[0]["ClientManager"].ToString());
                                txtJoiningLastUpdate.Text = ExtraDetailsDs.Tables[0].Rows[0]["LastUpdate"].ToString();
                                txtJoiningDetailsPostalAddress.Text = ExtraDetailsDs.Tables[0].Rows[0]["PostalAddress"].ToString();
                                txtJoiningDetailsBillingAddress.Text = ExtraDetailsDs.Tables[0].Rows[0]["BillingAddress"].ToString();
                                txtJoiningDetailsAge.Text = ExtraDetailsDs.Tables[0].Rows[0]["Age"].ToString();
                                txtWebAccessAccountEmail.Text = ExtraDetailsDs.Tables[0].Rows[0]["Email"].ToString();
                                txtWebAccessAccountPassword.Password = ExtraDetailsDs.Tables[0].Rows[0]["Password"].ToString();
                                chkAgreedtoReceivemails.IsChecked = Convert.ToBoolean(ExtraDetailsDs.Tables[0].Rows[0]["ReceiveMails"].ToString());
                                chkAgreedtoReceiveLetters.IsChecked = Convert.ToBoolean(ExtraDetailsDs.Tables[0].Rows[0]["ReceiveLetters"].ToString());
                                chkAgreedtoReceiveSMS.IsChecked = Convert.ToBoolean(ExtraDetailsDs.Tables[0].Rows[0]["ReceiveSMS"].ToString());
                                chkAgreedtoReceivePushNotification.IsChecked = Convert.ToBoolean(ExtraDetailsDs.Tables[0].Rows[0]["IsReceivePushNotification"].ToString());

                                if (ExtraDetailsDs.Tables[0].Rows[0]["MedRec_Filedata"] == DBNull.Value)
                                {
                                    _MedRec_FileData = null;
                                    btnShowMedRec.IsEnabled = false;
                                }
                                else
                                {
                                    _MedRec_FileData = (byte[])ExtraDetailsDs.Tables[0].Rows[0]["MedRec_Filedata"];
                                    txtMedRecFileName.Text = ExtraDetailsDs.Tables[0].Rows[0]["MedRec_FileName"].ToString();
                                    _MedRec_Ext = ExtraDetailsDs.Tables[0].Rows[0]["MedRec_Ext"].ToString();
                                    btnShowMedRec.IsEnabled = true;
                                }

                                btnAddInfo.Visibility = Visibility.Hidden;
                                btnUpdateInfo.Visibility = Visibility.Visible;
                            }
                            else
                            {
                                btnAddInfo.Visibility = Visibility.Visible;
                                btnUpdateInfo.Visibility = Visibility.Hidden;
                            }
                        }
                        else
                        {
                            btnAddInfo.Visibility = Visibility.Visible;
                            btnUpdateInfo.Visibility = Visibility.Hidden;
                        }
                    }
                    else
                    {
                        btnAddInfo.Visibility = Visibility.Visible;
                        btnUpdateInfo.Visibility = Visibility.Hidden;
                    }
                }

                else
                {
                    btnAddInfo.Visibility = Visibility.Visible;
                    btnUpdateInfo.Visibility = Visibility.Hidden;
                    clearData();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Hand);
            }

        }

        private void btnUpdateInfo_Click(object sender, RoutedEventArgs e)
        {
            AddUpdateData();
        }

        private void btnCancelInfo_Click(object sender, RoutedEventArgs e)
        {
            clearData();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void btnShowMembership_Click(object sender, RoutedEventArgs e)
        {
            if (!ExistingMember)
            {
                if (_CanOpenMemberShipNewMember)
                {
                    Memberships _mDetails = new Memberships();
                    _mDetails.ExistingMember = ExistingMember;
                    _mDetails.CardNumber = CardNumber;
                    _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    _mDetails._MemberName = _MemberName;
                    _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    _mDetails._CompanyName = _CompanyName;
                    _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                    _mDetails._SiteName = _SiteName;
                    _mDetails._StaffPic = _StaffPic;
                    _mDetails._UserName = _UserName;
                    _mDetails._CompanyID = _CompanyID;
                    _mDetails._SiteID = _SiteID;
                    _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                    _mDetails.StaffImage.Source = StaffImage.Source;
                    _mDetails.dt = this.dt;
                    _mDetails._dSAccessRights = _dSAccessRights;
                    _mDetails.Owner = this.Owner;

                    this.Close();
                    _mDetails.ShowDialog();
                }
                else
                {
                    System.Windows.MessageBox.Show("New Member Details have to be Created First!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            else
            {
                Memberships _mDetails = new Memberships();
                _mDetails.ExistingMember = ExistingMember;
                _mDetails.CardNumber = CardNumber;
                _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _mDetails._MemberName = _MemberName;
                _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _mDetails._CompanyName = _CompanyName;
                _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                _mDetails._SiteName = _SiteName;
                _mDetails._StaffPic = _StaffPic;
                _mDetails._UserName = _UserName;
                _mDetails._CompanyID = _CompanyID;
                _mDetails._SiteID = _SiteID;
                _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                _mDetails.StaffImage.Source = StaffImage.Source;
                _mDetails._dSAccessRights = _dSAccessRights;
                _mDetails.dt = this.dt;
                _mDetails.Owner = this.Owner;

                this.Close();
                _mDetails.ShowDialog();
            }
        }

        private void btnAccounts_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberAccounts mAccounts = new MemberAccounts();
                mAccounts.MemberID = GetMemberDetailsMemberId;
                mAccounts._CurrentLoginDateTime = _CurrentLoginDateTime;
                mAccounts._CompanyName = _CompanyName;
                mAccounts._SiteName = _SiteName;
                mAccounts._UserName = _UserName;
                mAccounts._StaffPic = _StaffPic;
                mAccounts._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mAccounts.ExistingMember = ExistingMember;
                mAccounts.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mAccounts._MemberName = _MemberName;
                mAccounts.Member_Image_Img.Source = Member_Image_Img.Source;
                mAccounts.StaffImage.Source = StaffImage.Source;
                mAccounts._dSAccessRights = _dSAccessRights;
                mAccounts.dt = this.dt;

                mAccounts.Owner = this.Owner;
                this.Cursor = System.Windows.Input.Cursors.Wait;
                btnAccounts.Cursor = System.Windows.Input.Cursors.Wait;
                this.Close();
                mAccounts.ShowDialog();
            }
        }

        private void btnMemberBillingDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ExistingMember)
                {
                    this.Cursor = System.Windows.Input.Cursors.Wait;
                    PaymentDetails PD = new PaymentDetails(GetMemberDetailsMemberId, _CompanyID, _SiteID, dt);
                    this.Cursor = System.Windows.Input.Cursors.Arrow;

                    PD._CurrentLoginDateTime = _CurrentLoginDateTime;
                    PD._CompanyName = _CompanyName;
                    PD._SiteName = _SiteName;
                    PD._UserName = _UserName;
                    PD._StaffPic = _StaffPic;
                    PD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    PD.ExistingMember = ExistingMember;
                    PD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    PD._MemberName = _MemberName;
                    PD.Member_Image_Img.Source = Member_Image_Img.Source;
                    PD.StaffImage.Source = StaffImage.Source;
                    PD._dSAccessRights = _dSAccessRights;
                    PD.dt = this.dt;

                    PD.Owner = this.Owner;
                    this.Close();
                    PD.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnMemberExtraDetails_Click(object sender, RoutedEventArgs e)
        {
            //SAME WINDOW; NO ACTION
        }

        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                Tasks Tsk = new Tasks();
                Tsk.strMemberNo = GetMemberDetailsMemberId;
                Tsk.Owner = this.Owner;
                Tsk.SetAccessRightsDS(_dSAccessRights);

                Tsk._CurrentLoginDateTime = _CurrentLoginDateTime;
                Tsk._CompanyName = _CompanyName;
                Tsk._SiteName = _SiteName;
                Tsk._UserName = _UserName;
                Tsk._StaffPic = _StaffPic;
                Tsk._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                Tsk.ExistingMember = ExistingMember;
                Tsk.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                Tsk._MemberName = _MemberName;
                Tsk.Member_Image_Img.Source = Member_Image_Img.Source;
                Tsk.StaffImage.Source = StaffImage.Source;
                Tsk._dSAccessRights = _dSAccessRights;
                Tsk.dt = this.dt;

                this.Close();
                Tsk.ShowDialog();
            }
        }

        private void btnCommunications_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Communication _Comm = new Communication();
                _Comm.Owner = this.Owner;
                _Comm.SetAccessRightsDS(_dSAccessRights);
                this.Cursor = System.Windows.Input.Cursors.Arrow;

                _Comm._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Comm._CompanyName = _CompanyName;
                _Comm._SiteName = _SiteName;
                _Comm._UserName = _UserName;
                _Comm._StaffPic = _StaffPic;
                _Comm._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Comm.ExistingMember = ExistingMember;
                _Comm.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Comm._MemberName = _MemberName;
                _Comm.Member_Image_Img.Source = Member_Image_Img.Source;
                _Comm.StaffImage.Source = StaffImage.Source;
                _Comm._dSAccessRights = _dSAccessRights;
                _Comm.dt = this.dt;

                this.Close();
                _Comm.ShowDialog();
            }
        }

        private void btnNotes_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Notes _Notes = new Notes();
                _Notes.Owner = this.Owner;

                _Notes._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Notes._CompanyName = _CompanyName;
                _Notes._SiteName = _SiteName;
                _Notes._UserName = _UserName;
                _Notes._StaffPic = _StaffPic;
                _Notes._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Notes.ExistingMember = ExistingMember;
                _Notes.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Notes._MemberName = _MemberName;
                _Notes.Member_Image_Img.Source = Member_Image_Img.Source;
                _Notes.StaffImage.Source = StaffImage.Source;
                _Notes._dSAccessRights = _dSAccessRights;
                _Notes.dt = this.dt;

                this.Cursor = System.Windows.Input.Cursors.Arrow;
                this.Close();
                _Notes.ShowDialog();
            }
        }

        private void btnPersonalTraining_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                PersonalTraining _pt = new PersonalTraining();
                _pt.Owner = this.Owner;

                _pt._CurrentLoginDateTime = _CurrentLoginDateTime;
                _pt._CompanyName = _CompanyName;
                _pt._SiteName = _SiteName;
                _pt._UserName = _UserName;
                _pt._StaffPic = _StaffPic;
                _pt._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _pt.ExistingMember = ExistingMember;
                _pt.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _pt._MemberName = _MemberName;
                _pt.Member_Image_Img.Source = Member_Image_Img.Source;
                _pt.StaffImage.Source = StaffImage.Source;
                _pt._dSAccessRights = _dSAccessRights;
                _pt.dt = this.dt;

                this.Close();
                _pt.ShowDialog();
            }
        }

        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberDetails mD = new MemberDetails();
                mD.Owner = this.Owner;

                mD._CurrentLoginDateTime = _CurrentLoginDateTime;
                mD._CompanyName = _CompanyName;
                mD._SiteName = _SiteName;
                mD._UserName = _UserName;
                mD._StaffPic = _StaffPic;
                mD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mD.ExistingMember = ExistingMember;
                mD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mD._MemberName = _MemberName;
                mD.Member_Image_Img.Source = Member_Image_Img.Source;
                mD.StaffImage.Source = StaffImage.Source;
                mD._dSAccessRights = _dSAccessRights;
                mD.dt = this.dt;

                this.Close();
                mD.ShowDialog();
            }
        }
        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;

            if (_MemberName != null)
            {
                if (_MemberName.Trim() != "")
                {
                    Heading.Text = Heading.Text + " - " + _MemberName;
                }
            }
            //loadStaffPhoto(_StaffPic);
        }
        private void loadStaffPhoto(byte[] _StaffPic)
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }

        private void btnAddMedRec_Click(object sender, RoutedEventArgs e)
        {
             _MedRec_FileData = GetMedicalRecord();
        }

        private byte[] GetMedicalRecord()
        {
            try
            {
                OpenFileDialog commonOpenFileDialog = new OpenFileDialog();
                //commonOpenFileDialog.Filter = "Documents (or) Image Files|*.pdf;*.doc;*.docx;*.bmp;*.tif;*.gif;|JPEG Files|*.jpg;*.jpeg|PNG Files|*.png|BMP Files|*.bmp|TIF Files|*.tif|GIF Files|*.gif";
                commonOpenFileDialog.Filter = "Documents|*.pdf;*.doc;*.docx|Pictures / Images|*.jpg;*.jpeg;*.gif;*.png;*.bmp;*.tif;*.tiff";
                commonOpenFileDialog.Title = "Select Member Medical Record...";
                commonOpenFileDialog.Multiselect = false;
                commonOpenFileDialog.ShowDialog();

                string fileNamePath_MemberMedRec = commonOpenFileDialog.FileName;

                if (fileNamePath_MemberMedRec != "")
                {
                    _MedRec_FileName = commonOpenFileDialog.SafeFileName;
                    _MedRec_Ext = Path.GetExtension(fileNamePath_MemberMedRec);

                    FileStream fs = new FileStream(fileNamePath_MemberMedRec, FileMode.Open, FileAccess.Read);
                    byte[] data = new byte[fs.Length];
                    if (Convert.ToInt32(fs.Length) > 1048576) //1024 * 1024 -> 1MB
                    {
                        System.Windows.MessageBox.Show("File Size should be less than 1MB! \nPlease Select a different File to Upload!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return null;
                    }
                    txtMedRecFileName.Text = _MedRec_FileName;

                    switch (_MedRec_Ext.ToLower())
                    {
                        case ".doc":
                            _MedRec_ContentType = "application/msword";
                            break;
                        case ".docx":
                            _MedRec_ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                            break;
                        case ".bmp":
                            _MedRec_ContentType = "image/bmp";
                            break;
                        case ".gif":
                            _MedRec_ContentType = "image/gif";
                            break;
                        case ".jpeg":
                            _MedRec_ContentType = "image/jpeg";
                            break;
                        case ".jpg":
                            _MedRec_ContentType = "image/jpeg";
                            break;
                        case ".png":
                            _MedRec_ContentType = "image/png";
                            break;
                        case ".tif":
                            _MedRec_ContentType = "image/tiff";
                            break;
                        case ".tiff":
                            _MedRec_ContentType = "image/tiff";
                            break;
                        case ".pdf":
                            _MedRec_ContentType = "application/pdf";
                            break;
                        default:
                            _MedRec_ContentType = "";
                            break;
                    }

                    fs.Read(data, 0, System.Convert.ToInt32(fs.Length));
                    fs.Close();
                    return data;
                }
                return null;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Not a Valid File!\nError Details: ( " + ex.Message + " )", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return null;
            }
        }

        private void btnShowMedRec_Click(object sender, RoutedEventArgs e)
        {
            if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
            {
                string _tmpFilePath = Constants.AppPath + "\\MedRec" + _MedRec_Ext;
                FileStream fs = new FileStream(_tmpFilePath, FileMode.Create);
                fs.Write(_MedRec_FileData, 0, _MedRec_FileData.Length);
                fs.Close();
                Process.Start(_tmpFilePath);
            }
            else
            {
                System.Windows.MessageBox.Show("You MUST be Admin User to View the Medical Record!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
    }
}
