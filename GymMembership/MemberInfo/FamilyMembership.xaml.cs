﻿using System;
using System.Collections;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Data;
using System.Configuration;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GymMembership.MemberInfo
{
    public partial class FamilyMembership 
    {
        bool _allFieldsBlank = false;
        bool _zeroMemberDetails = true;
        DataTable _dtFamilyDetails;

        int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
        int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
        string _userName = ConfigurationManager.AppSettings["LoggedInUser"].ToString();
        int _MemberID;
        public FamilyMembership(DataTable _dtFamily = null, int _MemberIDParam = 0)
        {
            InitializeComponent();

            if (_dtFamily != null)
            {
                _dtFamilyDetails = _dtFamily;
                _MemberID = _MemberIDParam;

                if (_dtFamily.DefaultView.Count > 0)
                {
                    _zeroMemberDetails = false;
                    showFamilyMemberDetails(_dtFamilyDetails);
                }
            }
            txtFMemberFName1.Focus();
        }
        private void showFamilyMemberDetails(DataTable _dtF)
        {
            ////SHOW FAMILY MEMBER DETAILS
            if (_dtF != null)
            {
                if (!_zeroMemberDetails)
                {
                    for (int i = 0; i < _dtF.DefaultView.Count; i++)
                    {
                        switch (i)
                        {
                            case 0:
                                txtFMemberFName1.Text = _dtF.DefaultView[i]["FirstName"].ToString().ToUpper();
                                txtFMemberLName1.Text = _dtF.DefaultView[i]["LastName"].ToString().ToUpper();
                                txtFMemberCard1.Text = _dtF.DefaultView[i]["CardNumber"].ToString().ToUpper();
                                break;
                            case 1:
                                txtFMemberFName2.Text = _dtF.DefaultView[i]["FirstName"].ToString().ToUpper();
                                txtFMemberLName2.Text = _dtF.DefaultView[i]["LastName"].ToString().ToUpper();
                                txtFMemberCard2.Text = _dtF.DefaultView[i]["CardNumber"].ToString().ToUpper();
                                break;
                            case 2:
                                txtFMemberFName3.Text = _dtF.DefaultView[i]["FirstName"].ToString().ToUpper();
                                txtFMemberLName3.Text = _dtF.DefaultView[i]["LastName"].ToString().ToUpper();
                                txtFMemberCard3.Text = _dtF.DefaultView[i]["CardNumber"].ToString().ToUpper();
                                break;
                            case 3:
                                txtFMemberFName4.Text = _dtF.DefaultView[i]["FirstName"].ToString().ToUpper();
                                txtFMemberLName4.Text = _dtF.DefaultView[i]["LastName"].ToString().ToUpper();
                                txtFMemberCard4.Text = _dtF.DefaultView[i]["CardNumber"].ToString().ToUpper();
                                break;
                        }
                    }
                }
            }
        }
        private void txt_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private static bool IsTextAllowed_UnitPrice(string text)
        {
            Regex regex = new Regex("[^0-9.]"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ////VALIDATE IF EXISTING MEMBER; & IF ALREADY FAMILY-MEMBER DETAILS AVAILABLE
            ////THEN SHOW DETAILS
            txtFMemberFName1.Focus();
        }

        private void MetroWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) || (e.Key == Key.Return))
            {
                btnSave_Click(sender, e);
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (validateData())
            {
                if (!_zeroMemberDetails && _allFieldsBlank)
                {
                    if (MessageBox.Show("Do you want to REMOVE All Family member details?",this.Title,MessageBoxButton.YesNo,MessageBoxImage.Question) == MessageBoxResult.No)
                    {
                        this.Close();
                        return;
                    }
                }
                ////SAVE DETAILS
                for (int i = _dtFamilyDetails.DefaultView.Count-1; i >= 0; i--)
                {
                    _dtFamilyDetails.DefaultView[i].Delete();
                }
                _dtFamilyDetails.AcceptChanges();

                if (!_allFieldsBlank)
                {
                    //ADD1 : MEMBER 1 DETAILS -> MANDATORY
                    DataRow _dR = _dtFamilyDetails.NewRow();
                    _dR["CompanyID"] = _CompanyID;
                    _dR["SiteID"] = _SiteID;
                    _dR["MemberID"] = _MemberID;
                    _dR["FirstName"] = txtFMemberFName1.Text.Trim();
                    _dR["LastName"] = txtFMemberLName1.Text.Trim();
                    _dR["CardNumber"] = Convert.ToInt64(txtFMemberCard1.Text);

                    _dtFamilyDetails.Rows.Add(_dR);

                    //ADD2 : MEMBER 2 DETAILS -> IF DETAILS EXIST
                    if (txtFMemberFName2.Text.Trim() != "")
                    {
                        _dR = _dtFamilyDetails.NewRow();
                        _dR["CompanyID"] = _CompanyID;
                        _dR["SiteID"] = _SiteID;
                        _dR["MemberID"] = _MemberID;
                        _dR["FirstName"] = txtFMemberFName2.Text.Trim();
                        _dR["LastName"] = txtFMemberLName2.Text.Trim();
                        _dR["CardNumber"] = Convert.ToInt64(txtFMemberCard2.Text);

                        _dtFamilyDetails.Rows.Add(_dR);

                        //ADD3 : MEMBER 3 DETAILS -> IF DETAILS EXIST
                        if (txtFMemberFName3.Text.Trim() != "")
                        {
                            _dR = _dtFamilyDetails.NewRow();
                            _dR["CompanyID"] = _CompanyID;
                            _dR["SiteID"] = _SiteID;
                            _dR["MemberID"] = _MemberID;
                            _dR["FirstName"] = txtFMemberFName3.Text.Trim();
                            _dR["LastName"] = txtFMemberLName3.Text.Trim();
                            _dR["CardNumber"] = Convert.ToInt64(txtFMemberCard3.Text);

                            _dtFamilyDetails.Rows.Add(_dR);

                            //ADD4 : MEMBER 4 DETAILS -> IF DETAILS EXIST
                            if (txtFMemberFName4.Text.Trim() != "")
                            {
                                _dR = _dtFamilyDetails.NewRow();
                                _dR["CompanyID"] = _CompanyID;
                                _dR["SiteID"] = _SiteID;
                                _dR["MemberID"] = _MemberID;
                                _dR["FirstName"] = txtFMemberFName4.Text.Trim();
                                _dR["LastName"] = txtFMemberLName4.Text.Trim();
                                _dR["CardNumber"] = Convert.ToInt64(txtFMemberCard4.Text);

                                _dtFamilyDetails.Rows.Add(_dR);
                            }
                        }
                    }
                }

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                ht.Add("@MemberID", _MemberID);
                ht.Add("@timeStampLocal", DateTime.Now);
                ht.Add("@UserName", _userName);
                ht.Add("@FamilyDetails", _dtFamilyDetails);

                SqlHelper _sql = new SqlHelper();
                if (_sql._ConnOpen == 0)
                    return;

                int _Result = 0;
                _Result = _sql.ExecuteQuery("saveFamilyMembershipDetails", ht);
                if (_Result > 0)
                {
                    MessageBox.Show("Family Member(s) details Updated!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Close();
                }
                else
                {
                    if (!_allFieldsBlank)
                    {
                        MessageBox.Show("Please try again!!\n\nDetails may not be updated properly!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Family Member(s) details Cleared!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        this.Close();
                    }
                }
            }
        }
        private bool validateData()
        {
            try
            {
                if (!_zeroMemberDetails && isAllFieldsBlank())
                {
                    return true;
                }

                bool _SecondMemberAllBlank = true;
                bool _ThirdMemberAllBlank = true;

                #region Family Member 1 Details
                //ROW 1 - MEMBER 1 DETAILS
                if (txtFMemberFName1.Text.Trim() == "")
                {
                    MessageBox.Show("First Name (Member 1) cannot be Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtFMemberFName1.Focus();
                    return false;
                }
                if (txtFMemberLName1.Text.Trim() == "")
                {
                    MessageBox.Show("Last Name (Member 1) cannot be Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtFMemberLName1.Focus();
                    return false;
                }
                if (txtFMemberCard1.Text.Trim() == "")
                {
                    MessageBox.Show("Card Number (Member 1) cannot be Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtFMemberCard1.Focus();
                    return false;
                }
                else
                {
                    long _cardNo = Convert.ToInt64(txtFMemberCard1.Text);
                    if (_cardNo == 0)
                    {
                        MessageBox.Show("Card Number (Member 1) cannot be Zero!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        txtFMemberCard1.Focus();
                        return false;
                    }
                }
                #endregion

                #region Family Member 2 Details
                //ROW 2 - MEMBER 2 DETAILS
                if (txtFMemberFName2.Text.Trim() == "")
                {
                    if (txtFMemberLName2.Text.Trim() != "" || (txtFMemberCard2.Text.Trim() != "" && txtFMemberCard2.Text.Trim() != "0"))
                    {
                        MessageBox.Show("First Name (Member 2) cannot be Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        txtFMemberFName2.Focus();
                        return false;
                    }
                }
                else
                {
                    _SecondMemberAllBlank = false;
                }

                if (txtFMemberLName2.Text.Trim() == "")
                {
                    if (txtFMemberFName2.Text.Trim() != "" || (txtFMemberCard2.Text.Trim() != "" && txtFMemberCard2.Text.Trim() != "0"))
                    {
                        MessageBox.Show("Last Name (Member 2) cannot be Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        txtFMemberLName2.Focus();
                        return false;
                    }
                }
                else
                {
                    _SecondMemberAllBlank = false;
                }

                if (txtFMemberCard2.Text.Trim() == "")
                {
                    if (txtFMemberFName2.Text.Trim() != "" || txtFMemberLName2.Text.Trim() != "")
                    {
                        MessageBox.Show("Card Number (Member 2) cannot be Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        txtFMemberCard2.Focus();
                        return false;
                    }
                }
                else
                {
                    long _cardNo = Convert.ToInt64(txtFMemberCard2.Text);
                    if (_cardNo == 0)
                    {
                        if (txtFMemberFName2.Text.Trim() != "" || txtFMemberLName2.Text.Trim() != "")
                        {
                            MessageBox.Show("Card Number (Member 2) cannot be Zero!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            txtFMemberCard2.Focus();
                            return false;
                        }
                    }
                    else
                    {
                        _SecondMemberAllBlank = false;
                    }
                }
                #endregion

                #region Family Member 3 Details
                //ROW 3 - MEMBER 3 DETAILS
                if (_SecondMemberAllBlank)
                {
                    if (txtFMemberFName3.Text.Trim() != "" || txtFMemberLName3.Text.Trim() != "" || (txtFMemberCard3.Text.Trim() != "" && txtFMemberCard3.Text.Trim() != "0"))
                    {
                            MessageBox.Show("Member 2 Details should be filled! ", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            txtFMemberFName2.Focus();
                            return false;
                    }
                }
                else
                {
                    if (txtFMemberFName3.Text.Trim() == "")
                    {
                        if (txtFMemberLName3.Text.Trim() != "" || (txtFMemberCard3.Text.Trim() != "" && txtFMemberCard3.Text.Trim() != "0"))
                        {
                            MessageBox.Show("First Name (Member 3) cannot be Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            txtFMemberFName3.Focus();
                            return false;
                        }
                    }
                    else
                    {
                        _ThirdMemberAllBlank = false;
                    }

                    if (txtFMemberLName3.Text.Trim() == "")
                    {
                        if (txtFMemberFName3.Text.Trim() != "" || (txtFMemberCard3.Text.Trim() != "" && txtFMemberCard3.Text.Trim() != "0"))
                        {
                            MessageBox.Show("Last Name (Member 3) cannot be Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            txtFMemberLName3.Focus();
                            return false;
                        }
                    }
                    else
                    {
                        _ThirdMemberAllBlank = false;
                    }

                    if (txtFMemberCard3.Text.Trim() == "")
                    {
                        if (txtFMemberFName3.Text.Trim() != "" || txtFMemberLName3.Text.Trim() != "")
                        {
                            MessageBox.Show("Card Number (Member 3) cannot be Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            txtFMemberCard3.Focus();
                            return false;
                        }
                    }
                    else
                    {
                        long _cardNo = Convert.ToInt64(txtFMemberCard3.Text);
                        if (_cardNo == 0)
                        {
                            if (txtFMemberFName3.Text.Trim() != "" || txtFMemberLName3.Text.Trim() != "")
                            {
                                MessageBox.Show("Card Number (Member 3) cannot be Zero!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                txtFMemberCard3.Focus();
                                return false;
                            }
                        }
                        else
                        {
                            _ThirdMemberAllBlank = false;
                        }
                    }
                }
                #endregion

                #region Family Member 4 Details
                //ROW 4 - MEMBER 4 DETAILS
                if (_ThirdMemberAllBlank)
                {
                    if (txtFMemberFName4.Text.Trim() != "" || txtFMemberLName4.Text.Trim() != "" || (txtFMemberCard4.Text.Trim() != "" && txtFMemberCard4.Text.Trim() != "0"))
                    {
                        MessageBox.Show("Member 3 Details should be filled! ", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        txtFMemberFName3.Focus();
                        return false;
                    }
                }
                else
                {
                    if (txtFMemberFName4.Text.Trim() == "")
                    {
                        if (txtFMemberLName4.Text.Trim() != "" || (txtFMemberCard4.Text.Trim() != "" && txtFMemberCard4.Text.Trim() != "0"))
                        {
                            MessageBox.Show("First Name (Member 4) cannot be Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            txtFMemberFName4.Focus();
                            return false;
                        }
                    }
                    if (txtFMemberLName4.Text.Trim() == "")
                    {
                        if (txtFMemberFName4.Text.Trim() != "" || (txtFMemberCard4.Text.Trim() != "" && txtFMemberCard4.Text.Trim() != "0"))
                        {
                            MessageBox.Show("Last Name (Member 4) cannot be Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            txtFMemberLName4.Focus();
                            return false;
                        }
                    }
                    if (txtFMemberCard4.Text.Trim() == "")
                    {
                        if (txtFMemberFName4.Text.Trim() != "" || txtFMemberLName4.Text.Trim() != "")
                        {
                            MessageBox.Show("Card Number (Member 4) cannot be Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            txtFMemberCard4.Focus();
                            return false;
                        }
                    }
                    else
                    {
                        long _cardNo = Convert.ToInt64(txtFMemberCard4.Text);
                        if (_cardNo == 0)
                        {
                            if (txtFMemberFName4.Text.Trim() != "" || txtFMemberLName4.Text.Trim() != "")
                            {
                                MessageBox.Show("Card Number (Member 4) cannot be Zero!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                txtFMemberCard4.Focus();
                                return false;
                            }
                        }
                    }
                }
                #endregion
                return true;
            }
            catch(Exception ex)
            {
                string _msg = ex.Message;
                return false;
            }
        }

        private bool isAllFieldsBlank()
        {
            if (txtFMemberFName1.Text.Trim() == "")
            {
                if (txtFMemberLName1.Text.Trim() == "")
                {
                    if (txtFMemberCard1.Text.Trim() == "")
                    {
                        if (txtFMemberFName2.Text.Trim() == "")
                        {
                            if (txtFMemberLName2.Text.Trim() == "")
                            {
                                if (txtFMemberCard2.Text.Trim() == "")
                                {
                                    if (txtFMemberFName3.Text.Trim() == "")
                                    {
                                        if (txtFMemberLName3.Text.Trim() == "")
                                        {
                                            if (txtFMemberCard3.Text.Trim() == "")
                                            {
                                                if (txtFMemberFName3.Text.Trim() == "")
                                                {
                                                    if (txtFMemberLName3.Text.Trim() == "")
                                                    {
                                                        if (txtFMemberCard3.Text.Trim() == "")
                                                        {
                                                            _allFieldsBlank = true;
                                                            return true;
                                                        }
                                                        else
                                                        {
                                                            _allFieldsBlank = false;
                                                            return false;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _allFieldsBlank = false;
                                                        return false;
                                                    }
                                                }
                                                else
                                                {
                                                    _allFieldsBlank = false;
                                                    return false;
                                                }
                                            }
                                            else
                                            {
                                                _allFieldsBlank = false;
                                                return false;
                                            }
                                        }
                                        else
                                        {
                                            _allFieldsBlank = false;
                                            return false;
                                        }
                                    }
                                    else
                                    {
                                        _allFieldsBlank = false;
                                        return false;
                                    }
                                }
                                else
                                {
                                    _allFieldsBlank = false;
                                    return false;
                                }
                            }
                            else
                            {
                                _allFieldsBlank = false;
                                return false;
                            }
                        }
                        else
                        {
                            _allFieldsBlank = false;
                            return false;
                        }
                    }
                    else
                    {
                        _allFieldsBlank = false;
                        return false;
                    }
                }
                else
                {
                    _allFieldsBlank = false;
                    return false;
                }
            }
            else
            {
                _allFieldsBlank = false;
                return false;
            }
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnSave_GotFocus(object sender, RoutedEventArgs e)
        {
            btnSave.Background = Brushes.Gold;
        }

        private void btnSave_LostFocus(object sender, RoutedEventArgs e)
        {
            btnSave.Background = Brushes.Chocolate;
        }

        private void btnCancel_GotFocus(object sender, RoutedEventArgs e)
        {
            btnCancel.Background = Brushes.Gold;
        }

        private void btnCancel_LostFocus(object sender, RoutedEventArgs e)
        {
            btnCancel.Background = Brushes.Chocolate;
        }
        
    }
}
