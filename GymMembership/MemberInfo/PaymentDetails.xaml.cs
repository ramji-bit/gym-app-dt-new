﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Data;
using System.Collections;
using System.Text.RegularExpressions;
using System.Configuration;

using Paychoice_Payment;
using GymMembership.Task;
using GymMembership.Comman;
using GymMembership.BAL;
using GymMembership.Communications;

namespace GymMembership.MemberInfo
{
    /// <summary>
    /// Interaction logic for PaymentDetails.xaml
    /// </summary>
    public partial class PaymentDetails 
    {
        public string _MemberName;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public bool _CanOpenMemberShipNewMember = false;
        public bool _CanOpenBillingBankNewMember = false;
        public bool ExistingMember = false;
        public int GetMemberDetailsMemberId; public long CardNumber;
        public DataSet _dSAccessRights;
        public DataTable dt = new DataTable();

        public int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
        public int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

        private bool reval;
        public int GetMemberDetailsMemberId_Payment;
        public int _paymemberID;
        private int CompanyId;
        private int SiteId;

        DataSet MemberDs = new DataSet();
        DataTable MemberDt = new DataTable();

        Hashtable htbankaccount = new Hashtable();
        Hashtable htcreditcard = new Hashtable();
        private int programId;
        private bool _isSubscriptionCreatedForSelectedMembership = false;
        private string _selectedPaymentMode = "";
        private string _customerGUID = "";
        private string _cardTokenGUID = "";
        private string _bankTokenGUID = "";
        private string _defaultPM = "";
        int _currSelectedMSUniqueID = 0;
        public PaymentDetails(int payer_memberid, int _Companyid, int _Siteid, DataTable _dt = null)
        {
            InitializeComponent();
            creditcardDetailsEmpty();
            BankAccountDetailsEmpty();
            LoadData(payer_memberid);
            CompanyId = _Companyid;
            SiteId = _Siteid;
            LoadExpiryYear();
            int MemberId = paychoice.FetchMemberId(CompanyId, SiteId, payer_memberid);

            get_BankDetails(MemberId);
            dt = _dt;
            LoadSchemaDetails(_dt);
            LoadDirectDebitDetailsIfExists(MemberId);

            grbCreditCardDetails.Visibility = Visibility.Hidden;
        }

        private void LoadDirectDebitDetailsIfExists(int _identityMemberID)
        {
            Hashtable htvalidatepaychoicemember = new Hashtable();
            DataManager dal = new DataManager();
            htvalidatepaychoicemember.Add("@appmemberid", _identityMemberID);
            DataSet ds = dal.ExecuteProcedureReader("Validate_Paychoice_Customer", htvalidatepaychoicemember);
            if (ds != null)
            {
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    _customerGUID = dt.Rows[0]["Customer_GUID"].ToString();

                    //dt = ds.Tables[1];
                    //if (dt.Rows.Count > 0)
                    //{
                    //    //cmbSchemaName.Text = dt.Rows[0]["Subscription_Name"].ToString();
                    //    cmbCurrency.Text = dt.Rows[0]["Currency"].ToString();
                    //    txtAmount.Text = dt.Rows[0]["Amount"].ToString();
                    //    txtPaymentDuration.Text = dt.Rows[0]["Payment_Schedule_Unit"].ToString();
                    //    cmbPaymentDuration.Text = dt.Rows[0]["Payment_Schedule"].ToString();
                    //    txtMinimumPaymentDuration.Text = dt.Rows[0]["min_Payment_Schedule_Unit"].ToString();
                    //    cmbminPaymentDuration.Text = dt.Rows[0]["Minimum_Payment_Schedule"].ToString();
                    //    txtCancellationFee.Text = dt.Rows[0]["Cancel_Amount"].ToString();
                    //    txtStartDate.Text = dt.Rows[0]["Start_Date"].ToString();
                    //}
                }
            }
        }
        private void LoadDirectDebitDetailsIfExists(string _subscriptionGUID)
        {
            Hashtable htvalidatepaychoicemember = new Hashtable();
            DataManager dal = new DataManager();
            htvalidatepaychoicemember.Add("@SubcriptionGUID", _subscriptionGUID);
            DataSet ds = dal.ExecuteProcedureReader("Validate_Paychoice_Customer_Subscription", htvalidatepaychoicemember);
            if (ds != null)
            {
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                        //cmbSchemaName.Text = dt.Rows[0]["Subscription_Name"].ToString();
                        cmbCurrency.Text = dt.Rows[0]["Currency"].ToString();
                        txtAmount.Text = dt.Rows[0]["Amount"].ToString();
                        txtPaymentDuration.Text = dt.Rows[0]["Payment_Schedule_Unit"].ToString();
                        cmbPaymentDuration.Text = dt.Rows[0]["Payment_Schedule"].ToString();
                        txtMinimumPaymentDuration.Text = dt.Rows[0]["min_Payment_Schedule_Unit"].ToString();
                        cmbminPaymentDuration.Text = dt.Rows[0]["Minimum_Payment_Schedule"].ToString();
                        txtCancellationFee.Text = dt.Rows[0]["Cancel_Amount"].ToString();
                        txtStartDate.Text = dt.Rows[0]["Start_Date"].ToString();
                }
            }
        }
        public void LoadExpiryYear()
        {
            for (int i = 0; i <= 20; i++)
            {
                //DateTime currentDate = DateTime.Now;
                string nextYearDate = DateTime.Now.AddYears(i).ToString("yy");
                cmbExpiryYear.Items.Add(nextYearDate);
            }
        }
        public void LoadSchemaDetails(DataTable dt)
        {
            try
            {
                if (dt != null)
                {
                    if (dt.DefaultView.Count > 0)
                    {
                        cmbSchemaName.ItemsSource = dt.DefaultView;
                        cmbSchemaName.DisplayMemberPath = "Program";
                        cmbSchemaName.SelectedValuePath = "ProgramValue";

                        cmbSchemaName.SelectedIndex = 0;
                        dt.DefaultView.RowFilter = "";
                        LoadSelectedSchemaDetails(cmbSchemaName.SelectedIndex, dt);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void LoadSelectedSchemaDetails(int _Index, DataTable _dtLoadSchema)
        {
            if (_Index != -1)
            {
                //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017
                _currSelectedMSUniqueID = Convert.ToInt32(_dtLoadSchema.Rows[_Index]["UniqueID"].ToString());
                //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017

                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                programId = Convert.ToInt32(_dtLoadSchema.Rows[_Index]["ProgramValue"].ToString());
                DataSet ProgrammeDs = _memberDetailsBal.get_ProgrammeDetails(programId);

                //cmbSchemaName.Text = _dtLoadSchema.Rows[_Index]["Program"].ToString(); 
                txtAmount.Text = _dtLoadSchema.Rows[_Index]["FullCost"].ToString();
                if (txtAmount.Text == "0.00")
                {
                    txtAmount.Text = ProgrammeDs.Tables[0].Rows[0]["ProgrammePrice"].ToString();
                }

                txtSetupFee.Text = _dtLoadSchema.Rows[_Index]["SignUpFee"].ToString();
                if (txtSetupFee.Text == "0.00")
                {
                    txtSetupFee.Text = ProgrammeDs.Tables[0].Rows[0]["SignUpFee"].ToString();
                }

                if (Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy")) > DateTime.Parse(ProgrammeDs.Tables[1].Rows[0]["ProgrammeSDate"].ToString()))
                {
                    txtStartDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                }
                else
                {
                    txtStartDate.Text = ProgrammeDs.Tables[1].Rows[0]["ProgrammeSDate"].ToString();
                }

                txtPaymentDuration.Text = "";
                txtMinimumPaymentDuration.Text = "";

                if (_dtLoadSchema.Rows[_Index]["State"].ToString().ToUpper() == "EXPIRED")
                {
                    btnOk.IsEnabled = false;
                    lblMembershipState.Content = "** EXPIRED **";
                }
                else if (Convert.ToBoolean(_dtLoadSchema.Rows[_Index]["isUpFront"].ToString()))
                {
                    btnOk.IsEnabled = false;
                    lblMembershipState.Content = "** UPFRONT PAYMENT **";
                }
                else
                {
                    btnOk.IsEnabled = true;
                    lblMembershipState.Content = "";

                    if (_dtLoadSchema.Rows[_Index]["Subscription_GUID"].ToString() != "")
                    {
                        _isSubscriptionCreatedForSelectedMembership = true;
                        LoadDirectDebitDetailsIfExists(_dtLoadSchema.Rows[_Index]["Subscription_GUID"].ToString());
                        btnOk.IsEnabled = false;
                    }
                    else
                    {
                        _isSubscriptionCreatedForSelectedMembership = false;
                        btnOk.IsEnabled = true;
                    }
                }
            }
        }
        private void get_BankDetails(int memberid)
        {
            DataSet _BankDetails = new DataSet();
            BillingBAL _billingbal = new BillingBAL();
            _BankDetails = _billingbal.get_PaymentDetails(memberid);

            if (_BankDetails != null)
            {
                if (_BankDetails.Tables[0].DefaultView.Count > 0)
                {
                    for (int i = 0; i < _BankDetails.Tables[0].DefaultView.Count; i++)
                    {
                        if (_BankDetails.Tables[0].DefaultView[i]["Payment_Mode"].ToString().ToUpper() == "BANK ACCOUNT")
                        {
                            txtbankAcNo1.Text = _BankDetails.Tables[0].Rows[i]["Bank_Account_No"].ToString();
                            txtBSBNo.Text = _BankDetails.Tables[0].Rows[i]["BSB"].ToString();
                            txtPayerName.Text = _BankDetails.Tables[0].Rows[i]["Payer_Name"].ToString();

                            if (_BankDetails.Tables[0].Rows[i]["isDefault"].ToString().ToUpper() == "DEFAULT")
                            {
                                BankHeader.Foreground = System.Windows.Media.Brushes.Yellow;
                                BankHeader.Text = "Bank Account Details ( *** DEFAULT PAYMENT METHOD ***)";
                                _defaultPM = "BANK";
                            }
                            else
                            {
                                BankHeader.Foreground = System.Windows.Media.Brushes.White;
                                BankHeader.Text = "Bank Account Details";
                            }
                            _bankTokenGUID = _BankDetails.Tables[0].Rows[i]["Token_Guid"].ToString();
                        }
                        else
                        {
                            txtCreditCardName.Text = _BankDetails.Tables[0].Rows[i]["Payer_Name"].ToString();
                            txtCreditCardNo.Text = _BankDetails.Tables[0].Rows[i]["Card_Masked_Number"].ToString();
                            cmbExpiryMonth.Text = _BankDetails.Tables[0].Rows[i]["Card_Expiry_Month"].ToString();
                            cmbExpiryYear.Text = _BankDetails.Tables[0].Rows[i]["Card_Expiry_year"].ToString();

                            if (_BankDetails.Tables[0].Rows[i]["isDefault"].ToString().ToUpper() == "DEFAULT")
                            {
                                CCHeader.Foreground = System.Windows.Media.Brushes.Yellow;
                                CCHeader.Text = "Credit Card Details ( *** DEFAULT PAYMENT METHOD ***)";
                                _defaultPM = "CARD";
                            }
                            else
                            {
                                CCHeader.Foreground = System.Windows.Media.Brushes.White;
                                CCHeader.Text = "Credit Card Details";
                            }
                            _cardTokenGUID = _BankDetails.Tables[0].Rows[i]["Token_Guid"].ToString();
                        }
                    }
                }
            }
        }
        private void cmbPaymentDetails_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbPaymentDetails.SelectedIndex == 0)
            {
                lblCardExpiryMsg.Visibility = Visibility.Hidden;
                grbBankDetails.Visibility = Visibility.Hidden;
                grbCreditCardDetails.Visibility = Visibility.Visible;
                _selectedPaymentMode = "Credit Card";

                //if (txtCreditCardNo.Text.Trim() != "")
                if (_customerGUID.Trim() != "")
                {
                    btnUpdateBankCC.Content = "Update CC Details";
                    btnUpdateBankCC.IsEnabled = true;
                    btnUpdateBankCC.Visibility = Visibility.Visible;

                    if (CCHeader.Text.Contains("DEFAULT PAYMENT METHOD"))
                    {
                        btnSetAsDefault.Visibility = Visibility.Collapsed;
                    }
                    else if (_cardTokenGUID != "")
                    {
                        btnSetAsDefault.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        btnSetAsDefault.Visibility = Visibility.Collapsed;
                    }
                }
                else
                {
                    btnUpdateBankCC.IsEnabled = false;
                    btnUpdateBankCC.Visibility = Visibility.Hidden;
                    lblCardExpiryMsg.Visibility = Visibility.Hidden;
                }
            }
            else if (cmbPaymentDetails.SelectedIndex == 1)
            {
                lblCardExpiryMsg.Visibility = Visibility.Hidden;
                grbCreditCardDetails.Visibility = Visibility.Hidden;
                grbBankDetails.Visibility = Visibility.Visible;
                _selectedPaymentMode = "Bank Account";

                //if (txtbankAcNo1.Text.Trim() != "" && txtBSBNo.Text.Trim() != "")
                if (_customerGUID.Trim() != "")
                {
                    btnUpdateBankCC.Content = "Update Bank Details";
                    btnUpdateBankCC.IsEnabled = true;
                    btnUpdateBankCC.Visibility = Visibility.Visible;

                    if (BankHeader.Text.Contains("DEFAULT PAYMENT METHOD"))
                    {
                        btnSetAsDefault.Visibility = Visibility.Collapsed;
                    }
                    else if (_bankTokenGUID != "")
                    {
                        btnSetAsDefault.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        btnSetAsDefault.Visibility = Visibility.Collapsed;
                    }
                }
                else
                {
                    btnUpdateBankCC.IsEnabled = false;
                    btnUpdateBankCC.Visibility = Visibility.Hidden;
                }
            }
        }
        private void LoadData(int member_id)
        {
            MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
            _paymemberID = member_id;
            MemberDs = _memberDetailsBal.get_AllMemberDetailsAndAlsoByMemberId(_paymemberID);
            MemberDt = MemberDs.Tables[0];
            string lastname = MemberDt.Rows[0]["LastName"].ToString();
            DataRow dr = MemberDt.Rows[0];
            try
            {
                txtFirstName.Text = dr["FirstName"].ToString();
                txtLastName.Text = dr["LastName"].ToString();
                txtPhoneNo.Text = dr["HomePhone"].ToString();
                txtMobileNo.Text = dr["MobilePhone"].ToString();
                txtEmail.Text = dr["EmailAddress"].ToString();
                txtAddress.Text = dr["Street"].ToString();
                txtSuburb.Text = dr["Suburb"].ToString();
                //txtState.Text = dr["City"].ToString();
                txtState.Text = dr["State"].ToString();
                txtPostalCode.Text = dr["PostalCode"].ToString();
                //txtCountry.Text = dr[].ToString();

                txtCreditCardName.Text = (txtFirstName.Text + " " + txtLastName.Text).Trim();
                txtPayerName.Text = txtCreditCardName.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            if (tabControl.SelectedIndex == 0)
            {
                if (validateCommonDetails())
                {
                    tabControl.SelectedIndex = 1;
                    bankDetails.IsEnabled = true;
                    details.IsEnabled = false;
                    btnPrevious.Visibility = Visibility.Visible;

                    if (_defaultPM.Trim().ToUpper() == "BANK")
                    {
                        cmbPaymentDetails.SelectedIndex = 1;
                        grbCreditCardDetails.Visibility = Visibility.Hidden;
                        grbBankDetails.Visibility = Visibility.Visible;
                    }
                    else if (_defaultPM.Trim().ToUpper() == "CARD")
                    {
                        cmbPaymentDetails.SelectedIndex = 0;
                        grbCreditCardDetails.Visibility = Visibility.Visible;
                        grbBankDetails.Visibility = Visibility.Hidden;
                    }
                }
            }
            else if (tabControl.SelectedIndex == 1)
            {
                if (string.IsNullOrEmpty(cmbPaymentDetails.Text.ToString()))
                {
                    MessageBox.Show("Please Select Payment Method", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    cmbPaymentDetails.Focus();
                    return;
                }
                if (cmbPaymentDetails.SelectedIndex == 0)
                {
                    if (validateCreditCardData())
                    {
                        tabControl.SelectedIndex = 2;
                        PaymentSchedule.IsEnabled = true;
                        bankDetails.IsEnabled = false;
                        btnNext.Visibility = Visibility.Hidden;
                        //disable();
                    }
                }
                else if (cmbPaymentDetails.SelectedIndex == 1)
                {

                    if (validateBankData())
                    {
                        tabControl.SelectedIndex = 2;
                        PaymentSchedule.IsEnabled = true;
                        bankDetails.IsEnabled = false;
                        btnNext.Visibility = Visibility.Hidden;
                        //disable(); 
                    }
                }
            }
        }
        private void creditcardDetailsEmpty()
        {
            txtCreditCardName.Text = string.Empty;
            txtCreditCardNo.Text = string.Empty;
            txtCVVNO.Password = string.Empty;
            cmbExpiryMonth.Text = string.Empty;
            cmbExpiryYear.Text = string.Empty;

            btnUpdateBankCC.IsEnabled = false;
            btnUpdateBankCC.Visibility = Visibility.Hidden;
        }

        private void BankAccountDetailsEmpty()
        {
            txtbankAcNo1.Text = string.Empty;
            txtPayerName.Text = string.Empty;
            txtBSBNo.Text = string.Empty;
        }

        private void disable()
        {
            cmbPaymentDetails.IsEnabled = false;
            grbBankDetails.IsEnabled = false;
            grbCreditCardDetails.IsEnabled = false;
        }
        private bool validateCommonDetails()
        {
            reval = true;

            if (string.IsNullOrEmpty(txtFirstName.Text.ToString()))
            {
                MessageBox.Show("Please Enter First Name.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtFirstName.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtLastName.Text.ToString()))
            {
                MessageBox.Show("Please Enter Last Name.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtLastName.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtPhoneNo.Text.ToString()))
            {
                //MessageBox.Show("Please Enter Phone No.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //txtPhoneNo.Focus();
                //reval = false;
                //return reval;
            }
            else
            {
                if (!txtPhoneNo.Text.All(c => Char.IsNumber(c)))
                {
                    MessageBox.Show("Please Enter  Phone No in Numbers!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtPhoneNo.Focus();
                    reval = false;
                    return reval;
                }
            }
            if (string.IsNullOrEmpty(txtMobileNo.Text.ToString()))
            {
                MessageBox.Show("Please Enter Mobile No.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtMobileNo.Focus();
                reval = false;
                return reval;
            }

            if (!txtMobileNo.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Please Enter  Mobile No in Numbers!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtMobileNo.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtEmail.Text.ToString()))
            {
                MessageBox.Show("Please Enter Email.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtEmail.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtAddress.Text.ToString()))
            {
                MessageBox.Show("Please Enter Address.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtAddress.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtSuburb.Text.ToString()))
            {
                MessageBox.Show("Please Enter Suburb.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtSuburb.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtState.Text.ToString()))
            {
                MessageBox.Show("Please Enter State.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtState.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtPostalCode.Text.ToString()))
            {
                MessageBox.Show("Please Enter Postal Code.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtPostalCode.Focus();
                reval = false;
                return reval;
            }

            if (!txtPostalCode.Text.All(c => Char.IsNumber(c)))
            {
                System.Windows.MessageBox.Show("Please Enter PostCode in Numbers!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtPostalCode.Focus();
                reval = false;
                return reval;

            }

            if (string.IsNullOrEmpty(cmbCountry.Text.ToString()))
            {
                System.Windows.MessageBox.Show("Please Select Country!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                cmbCountry.Focus();
                reval = false;
                return reval;
            }
            return true;
        }

        private bool validateBankData()
        {
            reval = true;
            
            if (string.IsNullOrEmpty(txtPayerName.Text.ToString()))
            {
                MessageBox.Show("Please Enter Payer Name.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtPayerName.Focus();
                reval = false;
                return reval;
            }

            if (!System.Text.RegularExpressions.Regex.IsMatch(txtPayerName.Text, "^[a-zA-Z]"))
            {
                MessageBox.Show("Please Enter  Payer Name in Text.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtPayerName.Focus();
                reval = false;
                txtPayerName.Text = string.Empty;
                return reval;
            }
            if (string.IsNullOrEmpty(txtBSBNo.Text.ToString()))
            {
                MessageBox.Show("Please Enter BSB No.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtBSBNo.Focus();
                reval = false;
                return reval;
            }

            if (!txtBSBNo.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Please Enter  BSB No in Numbers.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtBSBNo.Focus();
                reval = false;
                txtBSBNo.Text = string.Empty;
                return reval;
            }

            if (string.IsNullOrEmpty(txtbankAcNo1.Text.ToString()))
            {
                MessageBox.Show("Please Enter Bank Account No.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtbankAcNo1.Focus();
                reval = false;
                return reval;
            }

            if (!txtbankAcNo1.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Please Enter  Bank Account No in Numbers.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtbankAcNo1.Focus();
                reval = false;
                txtbankAcNo1.Text = string.Empty;
                return reval;
            }
            return true;
        }

        private bool validateCreditCardData(int _update = 0)
        {
            reval = true;

            if (string.IsNullOrEmpty(txtCreditCardName.Text.ToString()))
            {
                MessageBox.Show("Please enter Credit Card Name.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtCreditCardName.Focus();
                reval = false;
                return reval;
            }

            if (!System.Text.RegularExpressions.Regex.IsMatch(txtCreditCardName.Text, "^[a-zA-Z]"))
            {
                MessageBox.Show("Please Enter Credit Card Name in Text.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtCreditCardName.Focus();
                reval = false;
                txtCreditCardName.Text = string.Empty;
                return reval;
            }

            if (string.IsNullOrEmpty(txtCreditCardNo.Text.ToString()))
            {
                MessageBox.Show("Please Enter Credit Card No.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtCreditCardNo.Focus();
                reval = false;
                return reval;
            }

            if (_update == 1)
            {
                if (!txtCreditCardNo.Text.All(c => Char.IsNumber(c)))
                {
                    MessageBox.Show("Please Enter Credit Card in Numbers.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtCreditCardNo.Focus();
                    reval = false;
                    //txtCreditCardNo.Text = string.Empty;
                    return reval;
                }
            }
            if (string.IsNullOrEmpty(cmbExpiryMonth.Text.ToString()))
            {
                MessageBox.Show("Please Select Month.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                cmbExpiryMonth.Focus();
                reval = false;
                return reval;
            }


            if (string.IsNullOrEmpty(cmbExpiryYear.Text.ToString()))
            {
                MessageBox.Show("Please Select Year.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                cmbExpiryYear.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtCVVNO.Password.ToString()))
            {
                //MessageBox.Show("Please Enter Cvv Number.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //txtCVVNO.Focus();
                //reval = false;
                //return reval;
            }
            else
            {
                if (!txtCVVNO.Password.All(c => Char.IsNumber(c)))
                {
                    MessageBox.Show("Please Enter CVV No. as Numeric", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtCVVNO.Focus();
                    reval = false;
                    txtCVVNO.Password = string.Empty;
                    return reval;
                }
                else
                if ((txtCVVNO.Password.ToString()).Length != 3)
                {
                    MessageBox.Show("Please Enter CVV Number in 3 Digits", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtCVVNO.Focus();
                    reval = false;
                    return reval;
                }
            }
            return true;
        }

        private bool validatePaymentScheduleData()
        {
            reval = true;
            FormUtilities _FormUtil = new FormUtilities();

            if (string.IsNullOrEmpty(cmbSchemaName.Text.ToString()))
            {
                MessageBox.Show("Please Enter Schema Name.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                cmbSchemaName.Focus();
                reval = false;
                return reval;
            }

            //if (!System.Text.RegularExpressions.Regex.IsMatch(cmbSchemaName.Text, "^[a-zA-Z]"))
            //{
            //    MessageBox.Show("Please Enter  Schema Name in Text.").ToString();
            //    cmbSchemaName.Focus();
            //    reval = false;
            //    cmbSchemaName.Text = string.Empty;
            //    return reval;
            //}

            if (string.IsNullOrEmpty(cmbCurrency.Text.ToString()))
            {
                MessageBox.Show("Please Select Currency.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                cmbCurrency.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtSetupFee.Text.ToString()))
            {
                MessageBox.Show("Please Enter Setup Fee.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtSetupFee.Focus();
                reval = false;
                return reval;
            }

            if (!Regex.IsMatch(txtSetupFee.Text, @"^\d{1,7}(\.\d{1,2})?$"))
            {
                txtSetupFee.Text = "";
                MessageBox.Show("Please Enter Setup Fee in Numbers only (upto 2 decimal places)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtSetupFee.Focus();
                reval = false;
                return reval;
            }

            if (Convert.ToDecimal(txtSetupFee.Text.Trim()) <= -1m)
            {
                txtSetupFee.Text = "";
                MessageBox.Show("Setup Fee Cannot be Less than Zero!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtSetupFee.Focus();
                reval = false;
                return reval;
            }
            if (string.IsNullOrEmpty(txtAmount.Text.ToString()))
            {
                MessageBox.Show("Please Enter Amount.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtAmount.Focus();
                reval = false;
                return reval;
            }

            if (!Regex.IsMatch(txtAmount.Text, @"^\d{1,7}(\.\d{1,2})?$"))
            {
                txtAmount.Text = "";
                MessageBox.Show("Please Enter Amount in Numbers only (upto 2 decimal places)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtAmount.Focus();
                reval = false;
                return reval;
            }
            if (Convert.ToDecimal(txtAmount.Text.Trim()) <= 0m)
            {
                txtAmount.Text = "";
                MessageBox.Show("Amount Cannot be Zero OR Less!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtAmount.Focus();
                reval = false;
                return reval;
            }


            if (string.IsNullOrEmpty(txtPaymentDuration.Text.ToString()))
            {
                MessageBox.Show("Please Enter Payment Duration.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtPaymentDuration.Focus();
                reval = false;
                return reval;
            }

            if (!txtPaymentDuration.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Please Enter Payment Duration in Numbers.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtPaymentDuration.Focus();
                reval = false;
                txtPaymentDuration.Text = string.Empty;
                return reval;
            }

            if (string.IsNullOrEmpty(cmbPaymentDuration.Text.ToString()))
            {
                MessageBox.Show("Please Select Payment Duration.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                cmbPaymentDuration.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtPaymentDuration.Text.ToString()))
            {
                MessageBox.Show("Please Enter Minimum Payment Duration.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtPaymentDuration.Focus();
                reval = false;
                return reval;
            }

            if (!txtMinimumPaymentDuration.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Please Enter Minimum Payment Duration in Numbers.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtMinimumPaymentDuration.Focus();
                reval = false;
                txtMinimumPaymentDuration.Text = string.Empty;
                return reval;
            }

            //if (string.IsNullOrEmpty(cmbminPaymentDuration.Text.ToString()))
            //{
            //    MessageBox.Show("Please Select Minimum Payment Duration.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //    cmbminPaymentDuration.Focus();
            //    reval = false;
            //    return reval;
            //}

            if (string.IsNullOrEmpty(txtCancellationFee.Text.ToString()))
            {
                MessageBox.Show("Please Enter Cancellation Fee.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtCancellationFee.Focus();
                reval = false;
                return reval;
            }

            //if (!txtCancellationFee.Text.All(c => Char.IsNumber(c)))
            //{
            //    MessageBox.Show("Please Enter Cancellation Fee in Numbers.").ToString();
            //    txtCancellationFee.Focus();
            //    reval = false;
            //    txtCancellationFee.Text = string.Empty;
            //    return reval;
            //}
            if (!Regex.IsMatch(txtCancellationFee.Text, @"^\d{1,7}(\.\d{1,2})?$"))
            {
                txtCancellationFee.Text = "";
                MessageBox.Show("Please Enter Cancellation Fee in Numbers only (upto 2 decimal places)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtCancellationFee.Focus();
                reval = false;
                return reval;
            }

            if (Convert.ToDecimal(txtCancellationFee.Text.Trim()) <= -1m)
            {
                txtCancellationFee.Text = "";
                MessageBox.Show("Cancellation Fee Cannot be Less than Zero!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtCancellationFee.Focus();
                reval = false;
                return reval;
            }

            if (txtStartDate.Text == "")
            {
                txtStartDate.Text = "";
                MessageBox.Show("Please Enter Subscription Start Date!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtStartDate.Focus();
                reval = false;
                return reval;
            }
            else
            {
                if (!_FormUtil.CheckDate(txtStartDate.Text.Trim()))
                {
                    MessageBox.Show("Date Format Incorrect!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtStartDate.Focus();
                    reval = false;
                    return reval;
                }
                DateTime dTime = DateTime.Parse(txtStartDate.Text);
                if (dTime < DateTime.Now.Date)
                {
                    MessageBox.Show("Subscription Start Date Cannot be Less than Today!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtStartDate.Focus();
                    reval = false;
                    return reval;
                }
            }

            return true;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnOk.IsEnabled = false;
                this.Cursor = System.Windows.Input.Cursors.Wait;
                btnOk.Cursor = System.Windows.Input.Cursors.Wait;

                if (ConfigurationManager.AppSettings["isAdminUser"].ToString().ToUpper() != "YES")
                {
                    MessageBox.Show("Only ADMIN user Can perform this Operation!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Cursor = System.Windows.Input.Cursors.Arrow;
                    btnOk.Cursor = System.Windows.Input.Cursors.Hand;
                    btnOk.IsEnabled = true;
                    return;
                }
                else
                {
                    int appmemberid = FetchMemberId(CompanyId, SiteId, _paymemberID);
                    Hashtable htvalidatepaychoicemember = new Hashtable();
                    DataManager dal = new DataManager();
                    htvalidatepaychoicemember.Add("@appmemberid", appmemberid);
                    DataSet ds = dal.ExecuteProcedureReader("Validate_Paychoice_Customer", htvalidatepaychoicemember);
                    
                    if (ds != null)
                    {
                        DataTable dt = ds.Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            if (!_isSubscriptionCreatedForSelectedMembership)
                            {
                                //call only subscription
                                if (_selectedPaymentMode == "Bank Account")
                                {
                                    if (validateBankData() && validatePaymentScheduleData())
                                    {
                                        htbankaccount.Clear();

                                        htbankaccount.Add("Company_Id", CompanyId);
                                        htbankaccount.Add("Site_Id", SiteId);
                                        htbankaccount.Add("App_Member_id", _paymemberID);
                                        htbankaccount.Add("First_Name", txtFirstName.Text);
                                        htbankaccount.Add("Last_Name", txtLastName.Text);
                                        htbankaccount.Add("Phone_No", txtPhoneNo.Text);
                                        htbankaccount.Add("Mobile_No", txtMobileNo.Text);
                                        htbankaccount.Add("Email", txtEmail.Text);
                                        htbankaccount.Add("Address", txtAddress.Text);
                                        htbankaccount.Add("Suburb", txtSuburb.Text);
                                        htbankaccount.Add("State", txtState.Text);
                                        htbankaccount.Add("PostalCode", txtPostalCode.Text);
                                        htbankaccount.Add("Country", cmbCountry.Text);
                                        htbankaccount.Add("BankAccount_No", txtbankAcNo1.Text);
                                        htbankaccount.Add("Payer_Name", txtPayerName.Text);
                                        htbankaccount.Add("BSB_No", txtBSBNo.Text);
                                        htbankaccount.Add("Subscription_Name", cmbSchemaName.Text);
                                        htbankaccount.Add("Currency", cmbCurrency.Text.ToString());
                                        htbankaccount.Add("Set_Up_Fee", txtSetupFee.Text);
                                        htbankaccount.Add("Amount", txtAmount.Text);
                                        htbankaccount.Add("Billing_Cycle_Length", txtPaymentDuration.Text);
                                        htbankaccount.Add("Billing_Cycle_Unit", cmbPaymentDuration.Text);
                                        htbankaccount.Add("Billing_Cycle_Min_Length", txtMinimumPaymentDuration.Text);
                                        htbankaccount.Add("Cancellation_Fee", txtCancellationFee.Text);

                                        //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017
                                        htbankaccount.Add("msUniqueID", _currSelectedMSUniqueID);
                                        //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017

                                        DateTime dTime = DateTime.Parse(txtStartDate.Text);
                                        htbankaccount.Add("Subscription_Start_Date", dTime);
                                        htbankaccount.Add("Programme_id", programId);
                                        paychoice.CreateSubscriptionforExistingCustomer(dt.Rows[0]["Customer_GUID"].ToString().Trim(), _selectedPaymentMode, htbankaccount);

                                        this.Cursor = System.Windows.Input.Cursors.Arrow;
                                        btnOk.Cursor = System.Windows.Input.Cursors.Hand;
                                        MessageBox.Show("Payment Schedule Created Successfully!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                        Constants.Create_ACCESS_DAT_File(false, _paymemberID);
                                        this.Close();
                                    }
                                }
                                else if (_selectedPaymentMode == "Credit Card")
                                {
                                    if (validateCreditCardData() && validatePaymentScheduleData())
                                    {
                                        htcreditcard.Clear();

                                        string cardnumbersplit = "";
                                        string creditcardnumber = txtCreditCardNo.Text;
                                        int chunkSize = 4;
                                        int stringLength = creditcardnumber.Length;
                                        for (int i = 0; i < stringLength; i += chunkSize)
                                        {
                                            if (i + chunkSize > stringLength) chunkSize = stringLength - i;
                                            if (cardnumbersplit == "")
                                            {
                                                cardnumbersplit = creditcardnumber.Substring(i, chunkSize);
                                            }
                                            else
                                            {
                                                cardnumbersplit = cardnumbersplit + "-" + creditcardnumber.Substring(i, chunkSize);
                                            }

                                        }
                                        htcreditcard.Add("Company_Id", CompanyId);
                                        htcreditcard.Add("Site_Id", SiteId);
                                        htcreditcard.Add("App_Member_id", _paymemberID);
                                        htcreditcard.Add("First_Name", txtFirstName.Text);
                                        htcreditcard.Add("Last_Name", txtLastName.Text);
                                        htcreditcard.Add("Phone_No", txtPhoneNo.Text);
                                        htcreditcard.Add("Mobile_No", txtMobileNo.Text);
                                        htcreditcard.Add("Email", txtEmail.Text);
                                        htcreditcard.Add("Address", txtAddress.Text);
                                        htcreditcard.Add("Suburb", txtSuburb.Text);
                                        htcreditcard.Add("State", txtState.Text);
                                        htcreditcard.Add("PostalCode", txtPostalCode.Text);
                                        htcreditcard.Add("Country", cmbCountry.Text);

                                        htcreditcard.Add("CreditCard_Name", txtCreditCardName.Text);
                                        htcreditcard.Add("CreditCard_No", txtCreditCardNo.Text);
                                        htcreditcard.Add("Expiry_Month", cmbExpiryMonth.Text);
                                        htcreditcard.Add("Expiry_Year", cmbExpiryYear.Text);
                                        htcreditcard.Add("Cvv_No", txtCVVNO.Password);

                                        htcreditcard.Add("Subscription_Name", cmbSchemaName.Text);
                                        htcreditcard.Add("Currency", cmbCurrency.Text.ToString());
                                        htcreditcard.Add("Set_Up_Fee", txtSetupFee.Text);
                                        htcreditcard.Add("Amount", txtAmount.Text);
                                        htcreditcard.Add("Billing_Cycle_Length", txtPaymentDuration.Text);
                                        htcreditcard.Add("Billing_Cycle_Unit", cmbPaymentDuration.Text);
                                        htcreditcard.Add("Billing_Cycle_Min_Length", txtMinimumPaymentDuration.Text);
                                        htcreditcard.Add("Cancellation_Fee", txtCancellationFee.Text);

                                        //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017
                                        htcreditcard.Add("msUniqueID", _currSelectedMSUniqueID);
                                        //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017

                                        DateTime dTime = DateTime.Parse(txtStartDate.Text);
                                        htcreditcard.Add("Subscription_Start_Date", dTime);
                                        htcreditcard.Add("Programme_id", programId);
                                        paychoice.CreateSubscriptionforExistingCustomer(dt.Rows[0]["Customer_GUID"].ToString().Trim(), _selectedPaymentMode, htcreditcard);

                                        this.Cursor = System.Windows.Input.Cursors.Arrow;
                                        btnOk.Cursor = System.Windows.Input.Cursors.Hand;
                                        MessageBox.Show("Payment Schedule Created Successfully!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                        Constants.Create_ACCESS_DAT_File(false, _paymemberID);
                                        this.Close();
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Payment Mode Not Selected!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                    this.Cursor = System.Windows.Input.Cursors.Arrow;
                                    btnOk.Cursor = System.Windows.Input.Cursors.Hand;
                                    btnOk.IsEnabled = true;
                                    return;
                                }
                            }
                            else
                            {
                                MessageBox.Show("Direct Debit Details already added for this Member / Membership!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                this.Cursor = System.Windows.Input.Cursors.Arrow;
                                btnOk.Cursor = System.Windows.Input.Cursors.Hand;
                                btnOk.IsEnabled = true;
                                return;
                            }
                        }
                        else
                        {
                            if (cmbPaymentDetails.SelectedIndex == 1)
                            {
                                if (validateBankData() && validatePaymentScheduleData())
                                {
                                    htbankaccount.Clear();

                                    htbankaccount.Add("Company_Id", CompanyId);
                                    htbankaccount.Add("Site_Id", SiteId);
                                    htbankaccount.Add("App_Member_id", _paymemberID);
                                    htbankaccount.Add("First_Name", txtFirstName.Text);
                                    htbankaccount.Add("Last_Name", txtLastName.Text);
                                    htbankaccount.Add("Phone_No", txtPhoneNo.Text);
                                    htbankaccount.Add("Mobile_No", txtMobileNo.Text);
                                    htbankaccount.Add("Email", txtEmail.Text);
                                    htbankaccount.Add("Address", txtAddress.Text);
                                    htbankaccount.Add("Suburb", txtSuburb.Text);
                                    htbankaccount.Add("State", txtState.Text);
                                    htbankaccount.Add("PostalCode", txtPostalCode.Text);
                                    htbankaccount.Add("Country", cmbCountry.Text);
                                    htbankaccount.Add("BankAccount_No", txtbankAcNo1.Text);
                                    htbankaccount.Add("Payer_Name", txtPayerName.Text);
                                    htbankaccount.Add("BSB_No", txtBSBNo.Text);
                                    htbankaccount.Add("Subscription_Name", cmbSchemaName.Text);
                                    htbankaccount.Add("Currency", cmbCurrency.Text.ToString());
                                    htbankaccount.Add("Set_Up_Fee", txtSetupFee.Text);
                                    htbankaccount.Add("Amount", txtAmount.Text);
                                    htbankaccount.Add("Billing_Cycle_Length", txtPaymentDuration.Text);
                                    htbankaccount.Add("Billing_Cycle_Unit", cmbPaymentDuration.Text);
                                    htbankaccount.Add("Billing_Cycle_Min_Length", txtMinimumPaymentDuration.Text);
                                    htbankaccount.Add("Cancellation_Fee", txtCancellationFee.Text);

                                    //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017
                                    htbankaccount.Add("msUniqueID", _currSelectedMSUniqueID);
                                    //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017

                                    //Subscription start date will be specified by admin
                                    DateTime dTime = DateTime.Parse(txtStartDate.Text);
                                    htbankaccount.Add("Subscription_Start_Date", dTime);
                                    //Subscription start date will be specified by admin
                                    htbankaccount.Add("Programme_id", programId);
                                    int result = paychoice.pay_via_bankaccount(htbankaccount);
                                    htbankaccount.Clear();
                                    //this.Close();
                                    Constants.Create_ACCESS_DAT_File(false, _paymemberID);

                                    this.Cursor = System.Windows.Input.Cursors.Arrow;
                                    btnOk.Cursor = System.Windows.Input.Cursors.Hand;
                                    //MessageBox.Show("Payment Schedule Created Successfully!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                    this.Close();
                                }
                            }
                            else if (cmbPaymentDetails.SelectedIndex == 0)
                            {
                                if (validateCreditCardData() && validatePaymentScheduleData())
                                {
                                    htcreditcard.Clear();

                                    string cardnumbersplit = "";
                                    string creditcardnumber = txtCreditCardNo.Text;
                                    int chunkSize = 4;
                                    int stringLength = creditcardnumber.Length;
                                    for (int i = 0; i < stringLength; i += chunkSize)
                                    {
                                        if (i + chunkSize > stringLength) chunkSize = stringLength - i;
                                        if (cardnumbersplit == "")
                                        {
                                            cardnumbersplit = creditcardnumber.Substring(i, chunkSize);
                                        }
                                        else
                                        {
                                            cardnumbersplit = cardnumbersplit + "-" + creditcardnumber.Substring(i, chunkSize);
                                        }
                                    }

                                    htcreditcard.Add("Company_Id", CompanyId);
                                    htcreditcard.Add("Site_Id", SiteId);
                                    htcreditcard.Add("App_Member_id", _paymemberID);
                                    htcreditcard.Add("First_Name", txtFirstName.Text);
                                    htcreditcard.Add("Last_Name", txtLastName.Text);
                                    htcreditcard.Add("Phone_No", txtPhoneNo.Text);
                                    htcreditcard.Add("Mobile_No", txtMobileNo.Text);
                                    htcreditcard.Add("Email", txtEmail.Text);
                                    htcreditcard.Add("Address", txtAddress.Text);
                                    htcreditcard.Add("Suburb", txtSuburb.Text);
                                    htcreditcard.Add("State", txtState.Text);
                                    htcreditcard.Add("PostalCode", txtPostalCode.Text);
                                    htcreditcard.Add("Country", cmbCountry.Text);
                                    htcreditcard.Add("CreditCard_Name", txtCreditCardName.Text);

                                    htcreditcard.Add("CreditCard_No", txtCreditCardNo.Text);
                                    htcreditcard.Add("Expiry_Month", cmbExpiryMonth.Text);
                                    htcreditcard.Add("Expiry_Year", cmbExpiryYear.Text);
                                    htcreditcard.Add("Cvv_No", txtCVVNO.Password);
                                    htcreditcard.Add("Subscription_Name", cmbSchemaName.Text);
                                    htcreditcard.Add("Currency", cmbCurrency.Text.ToString());
                                    htcreditcard.Add("Set_Up_Fee", txtSetupFee.Text);
                                    htcreditcard.Add("Amount", txtAmount.Text);
                                    htcreditcard.Add("Billing_Cycle_Length", txtPaymentDuration.Text);
                                    htcreditcard.Add("Billing_Cycle_Unit", cmbPaymentDuration.Text);
                                    htcreditcard.Add("Billing_Cycle_Min_Length", txtMinimumPaymentDuration.Text);
                                    htcreditcard.Add("Cancellation_Fee", txtCancellationFee.Text);

                                    //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017
                                    htcreditcard.Add("msUniqueID", _currSelectedMSUniqueID);
                                    //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017

                                    //Subscription start date will be specified by admin
                                    DateTime dTime = DateTime.Parse(txtStartDate.Text);
                                    htcreditcard.Add("Subscription_Start_Date", dTime);
                                    //Subscription start date will be specified by admin
                                    htcreditcard.Add("Programme_id", programId);
                                    paychoice.pay_via_credit_card(htcreditcard);
                                    htcreditcard.Clear();
                                    //this.Close();
                                    Constants.Create_ACCESS_DAT_File(false, _paymemberID);

                                    this.Cursor = System.Windows.Input.Cursors.Arrow;
                                    btnOk.Cursor = System.Windows.Input.Cursors.Hand;
                                    //MessageBox.Show("Payment Schedule Created Successfully!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                    this.Close();
                                }
                            }
                        }
                    }
                    else
                    {
                        if (cmbPaymentDetails.SelectedIndex == 1)
                        {
                            if (validateBankData() && validatePaymentScheduleData())
                            {
                                htbankaccount.Clear();

                                htbankaccount.Add("Company_Id", CompanyId);
                                htbankaccount.Add("Site_Id", SiteId);
                                htbankaccount.Add("App_Member_id", _paymemberID);
                                htbankaccount.Add("First_Name", txtFirstName.Text);
                                htbankaccount.Add("Last_Name", txtLastName.Text);
                                htbankaccount.Add("Phone_No", txtPhoneNo.Text);
                                htbankaccount.Add("Mobile_No", txtMobileNo.Text);
                                htbankaccount.Add("Email", txtEmail.Text);
                                htbankaccount.Add("Address", txtAddress.Text);
                                htbankaccount.Add("Suburb", txtSuburb.Text);
                                htbankaccount.Add("State", txtState.Text);
                                htbankaccount.Add("PostalCode", txtPostalCode.Text);
                                htbankaccount.Add("Country", cmbCountry.Text);
                                htbankaccount.Add("BankAccount_No", txtbankAcNo1.Text);
                                htbankaccount.Add("Payer_Name", txtPayerName.Text);
                                htbankaccount.Add("BSB_No", txtBSBNo.Text);
                                htbankaccount.Add("Subscription_Name", cmbSchemaName.Text);
                                htbankaccount.Add("Currency", cmbCurrency.Text.ToString());
                                htbankaccount.Add("Set_Up_Fee", txtSetupFee.Text);
                                htbankaccount.Add("Amount", txtAmount.Text);
                                htbankaccount.Add("Billing_Cycle_Length", txtPaymentDuration.Text);
                                htbankaccount.Add("Billing_Cycle_Unit", cmbPaymentDuration.Text);
                                htbankaccount.Add("Billing_Cycle_Min_Length", txtMinimumPaymentDuration.Text);
                                htbankaccount.Add("Cancellation_Fee", txtCancellationFee.Text);

                                //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017
                                htbankaccount.Add("msUniqueID", _currSelectedMSUniqueID);
                                //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017

                                //Subscription start date will be specified by admin
                                DateTime dTime = DateTime.Parse(txtStartDate.Text);
                                htbankaccount.Add("Subscription_Start_Date", dTime);
                                //Subscription start date will be specified by admin
                                htbankaccount.Add("Programme_id", programId);
                                paychoice.pay_via_bankaccount(htbankaccount);
                                htbankaccount.Clear();
                                //this.Close();
                                Constants.Create_ACCESS_DAT_File(false, _paymemberID);

                                this.Cursor = System.Windows.Input.Cursors.Arrow;
                                btnOk.Cursor = System.Windows.Input.Cursors.Hand;
                                //MessageBox.Show("Payment Schedule Created Successfully!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                this.Close();
                            }
                        }
                        else if (cmbPaymentDetails.SelectedIndex == 0)
                        {
                            if (validateCreditCardData() && validatePaymentScheduleData())
                            {
                                htcreditcard.Clear();

                                string cardnumbersplit = "";
                                string creditcardnumber = txtCreditCardNo.Text;
                                int chunkSize = 4;
                                int stringLength = creditcardnumber.Length;
                                for (int i = 0; i < stringLength; i += chunkSize)
                                {
                                    if (i + chunkSize > stringLength) chunkSize = stringLength - i;
                                    if (cardnumbersplit == "")
                                    {
                                        cardnumbersplit = creditcardnumber.Substring(i, chunkSize);
                                    }
                                    else
                                    {
                                        cardnumbersplit = cardnumbersplit + "-" + creditcardnumber.Substring(i, chunkSize);
                                    }
                                }
                                htcreditcard.Add("Company_Id", CompanyId);
                                htcreditcard.Add("Site_Id", SiteId);
                                htcreditcard.Add("App_Member_id", _paymemberID);
                                htcreditcard.Add("First_Name", txtFirstName.Text);
                                htcreditcard.Add("Last_Name", txtLastName.Text);
                                htcreditcard.Add("Phone_No", txtPhoneNo.Text);
                                htcreditcard.Add("Mobile_No", txtMobileNo.Text);
                                htcreditcard.Add("Email", txtEmail.Text);
                                htcreditcard.Add("Address", txtAddress.Text);
                                htcreditcard.Add("Suburb", txtSuburb.Text);
                                htcreditcard.Add("State", txtState.Text);
                                htcreditcard.Add("PostalCode", txtPostalCode.Text);
                                htcreditcard.Add("Country", cmbCountry.Text);
                                htcreditcard.Add("CreditCard_Name", txtCreditCardName.Text);

                                htcreditcard.Add("CreditCard_No", txtCreditCardNo.Text);
                                htcreditcard.Add("Expiry_Month", cmbExpiryMonth.Text);
                                htcreditcard.Add("Expiry_Year", cmbExpiryYear.Text);
                                htcreditcard.Add("Cvv_No", txtCVVNO.Password);
                                htcreditcard.Add("Subscription_Name", cmbSchemaName.Text);
                                htcreditcard.Add("Currency", cmbCurrency.Text.ToString());
                                htcreditcard.Add("Set_Up_Fee", txtSetupFee.Text);
                                htcreditcard.Add("Amount", txtAmount.Text);
                                htcreditcard.Add("Billing_Cycle_Length", txtPaymentDuration.Text);
                                htcreditcard.Add("Billing_Cycle_Unit", cmbPaymentDuration.Text);
                                htcreditcard.Add("Billing_Cycle_Min_Length", txtMinimumPaymentDuration.Text);
                                htcreditcard.Add("Cancellation_Fee", txtCancellationFee.Text);

                                //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017
                                htcreditcard.Add("msUniqueID", _currSelectedMSUniqueID);
                                //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017

                                //Subscription start date will be specified by admin
                                DateTime dTime = DateTime.Parse(txtStartDate.Text);
                                htcreditcard.Add("Subscription_Start_Date", dTime);
                                //Subscription start date will be specified by admin
                                htcreditcard.Add("Programme_id", programId);
                                paychoice.pay_via_credit_card(htcreditcard);
                                htcreditcard.Clear();
                                //this.Close();
                                Constants.Create_ACCESS_DAT_File(false, _paymemberID);

                                this.Cursor = System.Windows.Input.Cursors.Arrow;
                                btnOk.Cursor = System.Windows.Input.Cursors.Hand;
                                //MessageBox.Show("Payment Schedule Created Successfully!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                this.Close();
                            }
                        }
                    }
                }
                this.Cursor = System.Windows.Input.Cursors.Arrow;
                btnOk.Cursor = System.Windows.Input.Cursors.Hand;
                btnOk.IsEnabled = true;
                return;
            }
            catch(Exception ex)
            {
                try
                {
                    Hashtable htpaychoicecustomer = new Hashtable();
                    int appmemberid = FetchMemberId(CompanyId, SiteId, _paymemberID);
                    int Member_ID = FetchMemberId(CompanyId, SiteId, appmemberid);
                    DataManager dal = new DataManager();

                    htpaychoicecustomer.Add("@AppMemberID", Member_ID);
                    //dal.ExecuteNonQueryProcedure("DelPayChoiceCustomer", htpaychoicecustomer);

                    MessageBox.Show("Error Creating Payment Details!\n\n" + ex.Message);
                    this.Cursor = System.Windows.Input.Cursors.Arrow;
                    btnOk.Cursor = System.Windows.Input.Cursors.Hand;
                    cmbSchemaName.SelectedIndex = -1;
                    btnOk.IsEnabled = true;
                }
                catch(Exception ex1)
                {
                    MessageBox.Show("Error Creating Payment Details!\n\n(Error Rolling back!)\n" + ex1.Message);
                    this.Cursor = System.Windows.Input.Cursors.Arrow;
                    btnOk.Cursor = System.Windows.Input.Cursors.Hand;
                    cmbSchemaName.SelectedIndex = -1;
                    btnOk.IsEnabled = true;
                }
            }
        }
        public static int FetchMemberId(int CompanyId, int SiteID, int AppCustomerID)
        {
            Hashtable htmemeberid = new Hashtable();
            DataManager dal = new DataManager();
            htmemeberid.Add("@CompanyID", CompanyId);
            htmemeberid.Add("@SiteID", SiteID);
            htmemeberid.Add("@Memberno", AppCustomerID);
            DataSet ds_member_ID = dal.ExecuteProcedureReader("FetchMemeberID_Payment", htmemeberid);
            htmemeberid.Clear();
            if (ds_member_ID != null)
            {
                if (ds_member_ID.Tables[0].Rows.Count > 0)
                {
                    return Int32.Parse(ds_member_ID.Tables[0].Rows[0]["MemberID"].ToString());
                }
                else
                {
                    return 0;
                }
            }
            return 0;
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            if (tabControl.SelectedIndex == 1)
            {
                btnPrevious.Visibility = Visibility.Hidden;
                tabControl.SelectedIndex = 0;
                details.IsEnabled = true;
                bankDetails.IsEnabled = false;
                btnNext.Visibility = Visibility.Visible;
            }
            else if (tabControl.SelectedIndex == 2)
            {
                tabControl.SelectedIndex = 1;
                bankDetails.IsEnabled = true;
                PaymentSchedule.IsEnabled = false;
                btnNext.Visibility = Visibility.Visible;
            }
        }
        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void btnShowMembership_Click(object sender, RoutedEventArgs e)
        {
            if (!ExistingMember)
            {
                if (_CanOpenMemberShipNewMember)
                {
                    Memberships _mDetails = new Memberships();
                    _mDetails.ExistingMember = ExistingMember;
                    _mDetails.CardNumber = CardNumber;
                    _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    _mDetails._MemberName = _MemberName;
                    _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    _mDetails._CompanyName = _CompanyName;
                    _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                    _mDetails._SiteName = _SiteName;
                    _mDetails._StaffPic = _StaffPic;
                    _mDetails._UserName = _UserName;
                    _mDetails._CompanyID = _CompanyID;
                    _mDetails._SiteID = _SiteID;
                    _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                    _mDetails.StaffImage.Source = StaffImage.Source;
                    _mDetails._dSAccessRights = _dSAccessRights;
                    _mDetails.dt = this.dt;
                    _mDetails.Owner = this.Owner;

                    this.Close();
                    _mDetails.ShowDialog();
                }
                else
                {
                    MessageBox.Show("New Member Details have to be Created First!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            else
            {
                Memberships _mDetails = new Memberships();
                _mDetails.ExistingMember = ExistingMember;
                _mDetails.CardNumber = CardNumber;
                _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _mDetails._MemberName = _MemberName;
                _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _mDetails._CompanyName = _CompanyName;
                _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                _mDetails._SiteName = _SiteName;
                _mDetails._StaffPic = _StaffPic;
                _mDetails._UserName = _UserName;
                _mDetails._CompanyID = _CompanyID;
                _mDetails._SiteID = _SiteID;
                _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                _mDetails.StaffImage.Source = StaffImage.Source;
                _mDetails._dSAccessRights = _dSAccessRights;
                _mDetails.dt = this.dt;
                _mDetails.Owner = this.Owner;

                this.Close();
                _mDetails.ShowDialog();
            }
        }

        private void btnAccounts_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberAccounts mAccounts = new MemberAccounts();
                mAccounts.MemberID = GetMemberDetailsMemberId;
                mAccounts._CurrentLoginDateTime = _CurrentLoginDateTime;
                mAccounts._CompanyName = _CompanyName;
                mAccounts._SiteName = _SiteName;
                mAccounts._UserName = _UserName;
                mAccounts._StaffPic = _StaffPic;
                mAccounts._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mAccounts.ExistingMember = ExistingMember;
                mAccounts.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mAccounts._MemberName = _MemberName;
                mAccounts.Member_Image_Img.Source = Member_Image_Img.Source;
                mAccounts.StaffImage.Source = StaffImage.Source;
                mAccounts._dSAccessRights = _dSAccessRights;
                mAccounts.dt = this.dt;

                mAccounts.Owner = this.Owner;
                this.Cursor = System.Windows.Input.Cursors.Wait;
                btnAccounts.Cursor = System.Windows.Input.Cursors.Wait;
                this.Close();
                mAccounts.ShowDialog();
            }
        }

        private void btnMemberBillingDetails_Click(object sender, RoutedEventArgs e)
        {
            //SAME WINDOW; NO ACTION
        }

        private void btnMemberExtraDetails_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                ExtraDetails extra = new ExtraDetails();
                extra.strExtraDetailsMemberID = GetMemberDetailsMemberId;

                extra._CurrentLoginDateTime = _CurrentLoginDateTime;
                extra._CompanyName = _CompanyName;
                extra._SiteName = _SiteName;
                extra._UserName = _UserName;
                extra._StaffPic = _StaffPic;
                extra._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                extra.ExistingMember = ExistingMember;
                extra.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                extra._MemberName = _MemberName;
                extra.Member_Image_Img.Source = Member_Image_Img.Source;
                extra.StaffImage.Source = StaffImage.Source;
                extra._dSAccessRights = _dSAccessRights;
                extra.dt = this.dt;

                extra.Owner = this.Owner;
                this.Close();
                extra.ShowDialog();
            }
        }

        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                Tasks Tsk = new Tasks();
                Tsk.strMemberNo = GetMemberDetailsMemberId;
                Tsk.Owner = this.Owner;
                Tsk.SetAccessRightsDS(_dSAccessRights);

                Tsk._CurrentLoginDateTime = _CurrentLoginDateTime;
                Tsk._CompanyName = _CompanyName;
                Tsk._SiteName = _SiteName;
                Tsk._UserName = _UserName;
                Tsk._StaffPic = _StaffPic;
                Tsk._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                Tsk.ExistingMember = ExistingMember;
                Tsk.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                Tsk._MemberName = _MemberName;
                Tsk.Member_Image_Img.Source = Member_Image_Img.Source;
                Tsk.StaffImage.Source = StaffImage.Source;
                Tsk._dSAccessRights = _dSAccessRights;
                Tsk.dt = this.dt;

                this.Close();
                Tsk.ShowDialog();
            }
        }

        private void btnCommunications_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Communication _Comm = new Communication();
                _Comm.Owner = this.Owner;
                _Comm.SetAccessRightsDS(_dSAccessRights);
                this.Cursor = System.Windows.Input.Cursors.Arrow;

                _Comm._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Comm._CompanyName = _CompanyName;
                _Comm._SiteName = _SiteName;
                _Comm._UserName = _UserName;
                _Comm._StaffPic = _StaffPic;
                _Comm._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Comm.ExistingMember = ExistingMember;
                _Comm.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Comm._MemberName = _MemberName;
                _Comm.Member_Image_Img.Source = Member_Image_Img.Source;
                _Comm.StaffImage.Source = StaffImage.Source;
                _Comm._dSAccessRights = _dSAccessRights;
                _Comm.dt = this.dt;

                this.Close();
                _Comm.ShowDialog();
            }
        }

        private void btnNotes_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Notes _Notes = new Notes();
                _Notes.Owner = this.Owner;

                _Notes._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Notes._CompanyName = _CompanyName;
                _Notes._SiteName = _SiteName;
                _Notes._UserName = _UserName;
                _Notes._StaffPic = _StaffPic;
                _Notes._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Notes.ExistingMember = ExistingMember;
                _Notes.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Notes._MemberName = _MemberName;
                _Notes.Member_Image_Img.Source = Member_Image_Img.Source;
                _Notes.StaffImage.Source = StaffImage.Source;
                _Notes._dSAccessRights = _dSAccessRights;
                _Notes.dt = this.dt;

                this.Cursor = System.Windows.Input.Cursors.Arrow;
                this.Close();
                _Notes.ShowDialog();
            }
        }

        private void btnPersonalTraining_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                PersonalTraining _pt = new PersonalTraining();
                _pt.Owner = this.Owner;

                _pt._CurrentLoginDateTime = _CurrentLoginDateTime;
                _pt._CompanyName = _CompanyName;
                _pt._SiteName = _SiteName;
                _pt._UserName = _UserName;
                _pt._StaffPic = _StaffPic;
                _pt._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _pt.ExistingMember = ExistingMember;
                _pt.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _pt._MemberName = _MemberName;
                _pt.Member_Image_Img.Source = Member_Image_Img.Source;
                _pt.StaffImage.Source = StaffImage.Source;
                _pt._dSAccessRights = _dSAccessRights;
                _pt.dt = this.dt;

                this.Close();
                _pt.ShowDialog();
            }
        }

        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberDetails mD = new MemberDetails();
                mD.Owner = this.Owner;

                mD._CurrentLoginDateTime = _CurrentLoginDateTime;
                mD._CompanyName = _CompanyName;
                mD._SiteName = _SiteName;
                mD._UserName = _UserName;
                mD._StaffPic = _StaffPic;
                mD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mD._CanOpenBillingBankNewMember = _CanOpenBillingBankNewMember;
                mD.ExistingMember = ExistingMember;
                mD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mD._MemberName = _MemberName;
                mD.Member_Image_Img.Source = Member_Image_Img.Source;
                mD.StaffImage.Source = StaffImage.Source;
                mD._dSAccessRights = _dSAccessRights;
                mD.dt = this.dt;

                this.Close();
                mD.ShowDialog();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
            cmbCountry.SelectedIndex = 0;
            cmbCurrency.SelectedIndex = 0;
        }
        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;

            if (_MemberName != null)
            {
                if (_MemberName.Trim() != "")
                {
                    Heading.Text = Heading.Text + " - " + _MemberName;
                }
            }
            //loadStaffPhoto(_StaffPic);
        }
        private void loadStaffPhoto(byte[] _StaffPic)
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }

        private void cmbSchemaName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbSchemaName.SelectedIndex != -1)
            {
                LoadSelectedSchemaDetails(cmbSchemaName.SelectedIndex, dt);
            }
            else
            {
                txtAmount.Text = "";
                txtSetupFee.Text = "";
                cmbPaymentDuration.SelectedIndex = -1;
                txtPaymentDuration.Text = "";
                txtMinimumPaymentDuration.Text = "";
            }
        }

        private void cmbCountry_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cmbCurrency.SelectedIndex = cmbCountry.SelectedIndex;
        }

        private void btnUpdateBankCC_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("This action will Update " + _selectedPaymentMode +
                        " details and set it as Default Payment Method !\n\nAre you sure to Update?", "Update " + _selectedPaymentMode + " Details", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    Cursor = System.Windows.Input.Cursors.Wait;
                    if (_selectedPaymentMode == "Bank Account")
                    {
                        if (validateBankData())
                        {
                            htbankaccount = new Hashtable();
                            htbankaccount.Add("BankAccount_No", txtbankAcNo1.Text);
                            htbankaccount.Add("Payer_Name", txtPayerName.Text);
                            htbankaccount.Add("BSB_No", txtBSBNo.Text);
                            paychoice.Update_new_bank_account(htbankaccount, _selectedPaymentMode, _customerGUID);
                            Cursor = System.Windows.Input.Cursors.Arrow;
                            MessageBox.Show(_selectedPaymentMode + " details Updated successfully!", "Update " + _selectedPaymentMode + " Details", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else if (_selectedPaymentMode == "Credit Card")
                    {
                        if (validateCreditCardData(1))
                        {
                            htcreditcard = new Hashtable();
                            htcreditcard.Add("CreditCard_Name", txtCreditCardName.Text);
                            htcreditcard.Add("CreditCard_No", txtCreditCardNo.Text);
                            htcreditcard.Add("Expiry_Month", cmbExpiryMonth.Text);
                            htcreditcard.Add("Expiry_Year", cmbExpiryYear.Text);
                            htcreditcard.Add("Cvv_No", txtCVVNO.Password);
                            paychoice.Update_new_credit_card(htcreditcard, _selectedPaymentMode, _customerGUID, _cardTokenGUID);
                            Cursor = System.Windows.Input.Cursors.Arrow;
                            MessageBox.Show(_selectedPaymentMode + " details Updated successfully!", "Update " + _selectedPaymentMode + " Details", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    Cursor = System.Windows.Input.Cursors.Arrow;
                }
            }
            catch(Exception ex)
            {
                Cursor = System.Windows.Input.Cursors.Arrow;
                MessageBox.Show("Error: "+ex.Message, "Update " + _selectedPaymentMode + " Details", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnSetAsDefault_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool _isDefaultSet = false;
                string _payMode = "";
                string _procName = "";
                string _tokenID = "";

                if (cmbPaymentDetails.SelectedIndex == 0) //CARD
                {
                    _payMode = "Credit Card";
                    _procName = "UpdatePayChoicePayMode_Default_Card";
                    if ((_customerGUID.Trim() != "") && (_cardTokenGUID.Trim() != ""))
                    {
                        paychoice.Update_cc_default_payment_method(_customerGUID, _cardTokenGUID);
                        _isDefaultSet = true;
                        _tokenID = _cardTokenGUID;
                    }
                }
                else if (cmbPaymentDetails.SelectedIndex == 1) //BANK
                {
                    _payMode = "Bank Account";
                    _procName = "UpdatePayChoicePayMode_Default_Bank";
                    if ((_customerGUID.Trim() != "") && (_bankTokenGUID.Trim() != ""))
                    {
                        paychoice.Update_default_payment_method(_customerGUID, _bankTokenGUID);
                        _isDefaultSet = true;
                        _tokenID = _bankTokenGUID;
                    }
                }

                if (_isDefaultSet)
                {
                    Hashtable ht = new Hashtable();
                    DataManager dal = new DataManager();
                    ht.Add("@CustomerGUID", _customerGUID);
                    ht.Add("@PayMode", _payMode);
                    ht.Add("@tokenGUID", _tokenID);

                    dal.ExecuteNonQueryProcedure(_procName, ht);
                    MessageBox.Show("Default Payment Method Updated successfully!", "Set Default Payment Method", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message,"Set Default Payment Method", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
