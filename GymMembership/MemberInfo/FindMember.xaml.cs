﻿using System;
using System.Collections;
using System.IO;
using System.Windows;
using System.Windows.Threading;
using System.Data;
using System.Windows.Controls;
using System.Windows.Data;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration;
using GymMembership.Comman;
using GymMembership.Navigation;
using Paychoice_Payment;

namespace GymMembership.MemberInfo
{
    /// <summary>
    /// Interaction logic for FindMember.xaml
    /// </summary>
    public partial class FindMember 
    {
        public string _MemberName;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public bool _CanOpenMemberShipNewMember = false;
        public bool ExistingMember = false;
        public string _appVersion="";

        public Dashboard _dashBoard;
        DispatcherTimer _fullMemberList;

        DataSet _dsAccessRights = new DataSet();
        bool _AtleastOneMemberExist = false;
        bool _isShowMemberAccess = false;
        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto(_StaffPic);
        }
        private void loadStaffPhoto(byte[] _StaffPic)
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }

        DataSet _ds = new DataSet();
        int _MemberDetailsDataTable = 0; int _MemberShipDataTable = 1; int _MemPicTable = 2;
        
        int _CompanyID;
        int _SiteID;
        int _ModuleTable = 0; int _SubModuleTable = 1;
        bool _Window_Loaded = false;

        public FindMember()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Closing += new CancelEventHandler(FindMember_Closing);
                this.ShowInTaskbar = true;

                if (!Comman.Constants._OfflineFlag)
                {
                    LoadMemberListFromDBOnLoad_Disconnected();
                    PopulateProgramFindMember_Disconnected();
                }
                else
                {
                    LoadMemberListFromDBOnLoad(true);
                    //LoadMemberListFromDBOnLoad();

                    _fullMemberList = new DispatcherTimer(DispatcherPriority.Background);
                    _fullMemberList.Tick += new EventHandler(Get_fullMemberList);
                    _fullMemberList.Interval = new TimeSpan(0, 0, 0, 1);
                    _fullMemberList.Start();

                    Utilities _Util = new Utilities();
                    _Util.populateProgramFindMember(cmbProgramme);
                }
                txtSearchfor.Focus();
                cmbProgramme.SelectedIndex = 0;

                _Window_Loaded = true;
                SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);

                if (!Constants._SuperAdmin)
                {
                    EnableDisableControls_AccessRights();
                }

                this.IsMaxRestoreButtonEnabled = false;
                this.IsMinButtonEnabled = false;
                this.IsWindowDraggable = false;

                if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper().Trim() == "YES")
                {
                    btnArchiveMember.IsEnabled = true;
                }
                else
                {
                    btnArchiveMember.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void Get_fullMemberList(object sender, EventArgs e)
        {
            if (_AtleastOneMemberExist)
            {
                PleaseWait _plsWait = new PleaseWait("Loading Member Data.. Pls Wait!");
                Mouse.OverrideCursor = Cursors.Wait;
                _plsWait.Show();
                LoadMemberListFromDBOnLoad(false);
                _plsWait.Close();
            }
            Mouse.OverrideCursor = null;
            _fullMemberList.Stop();
        }
        void FindMember_Closing(object sender, CancelEventArgs e)
        {
            if (!Constants._SuperAdmin)
            {
                if (_dashBoard != null)
                {
                    _dashBoard.Show();
                    _dashBoard.Activate();
                }
            }
            else
            {
                this.Owner.Show();
                this.Owner.Activate();
            }
        }

        private void btnShowMember_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int i; int _MemberID;

                i = grdFindMember.SelectedIndex;
                if (i != -1)
                {
                    _MemberID = Convert.ToInt32((grdFindMember.Items[i] as DataRowView).Row["MemberNo"].ToString());
                    _MemberName = (grdFindMember.Items[i] as DataRowView).Row["FirstName"].ToString().ToUpper() + " " + (grdFindMember.Items[i] as DataRowView).Row["LastName"].ToString().ToUpper();
                    MemberDetails mD = new MemberDetails();
                    
                    mD.GetMemberDetailsMemberId = _MemberID;
                    mD._MemberName = _MemberName;
                    mD.MemberDetails_AccessRights(_dsAccessRights);
                    mD._CurrentLoginDateTime = this._CurrentLoginDateTime;
                    mD._CompanyName = this._CompanyName;
                    mD._SiteName = this._SiteName;
                    mD._UserName = this._UserName;
                    mD._StaffPic = this._StaffPic;
                    mD.StaffImage.Source = this.StaffImage.Source;
                    mD._appVersion = _appVersion;
                    
                    //mD.Owner = this.Owner;
                    //this.Close();

                    mD.Owner = this;
                    mD.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Please Select a Member from the List!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error!" + ex.Message);
                return;
            }
        }
        private void btnCheckIn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //    int i; int _MemberID;
                //    i = grdFindMember.SelectedIndex;
                //    if (i != -1)
                //    {
                //        CheckIn _Check = new CheckIn();
                //        _MemberID = Convert.ToInt32((grdFindMember.Items[i] as DataRowView).Row.ItemArray[0].ToString());
                //        _Check.GetMemberDetailsMemberId = _MemberID;
                //        _Check._MemberName = (grdFindMember.Items[i] as DataRowView).Row["FirstName"].ToString() + " " + (grdFindMember.Items[i] as DataRowView).Row["LastName"].ToString();
                //        _ds.Tables[_MemPicTable].DefaultView.RowFilter = "MemberNo = " + Convert.ToInt32((grdFindMember.Items[i] as DataRowView).Row["MemberNo"].ToString());
                //        if (_ds.Tables[_MemPicTable].DefaultView.Count > 0)
                //        {
                //            if (_ds.Tables[_MemPicTable].DefaultView[0]["MemberPic"] == DBNull.Value)
                //            {
                //                _Check._data = null;
                //            }
                //            else
                //            {
                //                _Check._data = (byte[])_ds.Tables[_MemPicTable].DefaultView[0]["MemberPic"];
                //            }
                //        }
                //        _Check.Owner = this;
                //        _Check.ShowDialog();
                //    }
                //    else
                //    {
                //        MessageBox.Show("Please Select a Member from the List!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                //        return;
                //    }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error!" + ex.Message);
                return;
            }
        }
        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            MemberDetails mD = new MemberDetails();
            mD.Owner = this.Owner;
            mD.GetMemberDetailsMemberId = 0;
            mD._MemberName = "";
            mD.ExistingMember = false;
            mD.MemberDetails_AccessRights(_dsAccessRights);
            mD._CurrentLoginDateTime = this._CurrentLoginDateTime;
            mD._CompanyName = this._CompanyName;
            mD._SiteName = this._SiteName;
            mD._UserName = this._UserName;
            mD._StaffPic = this._StaffPic;
            mD._appVersion = _appVersion;
            this.Close();

            mD.ShowDialog();
        }
        private void txtSearchfor_TextChanged(object sender, TextChangedEventArgs e)
        {
            string _SearchStr = "(LastName LIKE '%" + txtSearchfor.Text.Trim().Replace("'","''") 
                + "%' OR FirstName LIKE '%" + txtSearchfor.Text.Trim().Replace("'","''") 
                + "%' OR CardNumber LIKE '%" + txtSearchfor.Text.Trim().Replace("'", "''") 
                + "%' OR [Cell Phone] LIKE '%" + txtSearchfor.Text.Trim().Replace("'", "''") + "%')";
            _ds.Tables[_MemberShipDataTable].DefaultView.RowFilter = "";
            _ds.Tables[_MemberShipDataTable].DefaultView.RowFilter = "Programme = " + cmbProgramme.SelectedValue + " AND State <> 'EXPIRED'";
            if (_ds.Tables[_MemberShipDataTable].DefaultView.Count > 0)
            {
                _SearchStr = _SearchStr + " AND [MemberNumSearch] IN (";
                for (int i = 0; i < _ds.Tables[_MemberShipDataTable].DefaultView.Count; i++)
                {
                    if (i > 0)
                    {
                        _SearchStr = _SearchStr + "," + _ds.Tables[_MemberShipDataTable].DefaultView[i]["MemberID"].ToString();
                    }
                    else
                    {
                        _SearchStr = _SearchStr + _ds.Tables[_MemberShipDataTable].DefaultView[i]["MemberID"].ToString();
                    }
                }
                _SearchStr = _SearchStr + ")";
            }
            LoadMemberListOnSearch(_SearchStr);
        }
        private void LoadMemberListFromDBOnLoad(bool _windowLoading = false)
        {
            try
            {
                _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                SqlHelper sql = new SqlHelper();
                //SqlHelperOffOn sql = new SqlHelperOffOn();
                if (sql._ConnOpen == 0) return;

                Hashtable ht = new Hashtable();

                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                _ds = new DataSet();
                if (_windowLoading)
                {
                    _ds = sql.ExecuteProcedure("GetMemberDetailsFindMember_Top10", ht);
                }
                else
                {
                    _ds = sql.ExecuteProcedure("GetMemberDetailsFindMember", ht);
                }

                grdFindMember.ItemsSource = null;

                _ds.Tables[_MemberDetailsDataTable].DefaultView.RowFilter = "";

                if (_ds.Tables[_MemberDetailsDataTable].DefaultView.Count > 0) _AtleastOneMemberExist = true;

                _ds.Tables[_MemberDetailsDataTable].DefaultView.RowFilter = "Status = 'Active'";
                grdFindMember.ItemsSource = _ds.Tables[_MemberDetailsDataTable].DefaultView;
            }
            catch(Exception ex)
            {
                string xMsg = ex.Message;
            }
        }
        private void LoadMemberListFromDBOnLoad_Disconnected()
        {
            try
            {
                _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                grdFindMember.ItemsSource = null;
                //table 0: member details
                Constants._DisConnectedALLTables.Tables["MemberDetails"].DefaultView.RowFilter = "CompanyID = " + _CompanyID + " AND SiteID = " + _SiteID + " AND ActiveMember = 1";
                var table = Constants._DisConnectedALLTables.Tables["MemberDetails"].DefaultView.ToTable("fm_MemberDetails", false, "MemberNo", "FirstName", "LastName", "CardNumber", "MobilePhone", "Street", "Suburb", "City", "EmailAddress");
                _ds.Tables.Add(table);

                grdFindMember.ItemsSource = table.DefaultView;
                Constants._DisConnectedALLTables.Tables["MemberDetails"].DefaultView.RowFilter = "";

                //Table1: memberships
                Constants._DisConnectedALLTables.Tables["Memberships"].DefaultView.RowFilter = "CompanyID = " + _CompanyID + " AND SiteID = " + _SiteID;
                table = Constants._DisConnectedALLTables.Tables["MemberShips"].DefaultView.ToTable("fm_MemberShips", false);
                _ds.Tables.Add(table);

                //Table1: memberpic
                Constants._DisConnectedALLTables.Tables["MemberDetails"].DefaultView.RowFilter = "CompanyID = " + _CompanyID + " AND SiteID = " + _SiteID;
                table = Constants._DisConnectedALLTables.Tables["MemberDetails"].DefaultView.ToTable("fm_MemberPic", false, "MemberNo", "MemberPic");
                _ds.Tables.Add(table);
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
                return;
            }
        }
        private void LoadMemberListOnSearch(string _SearchStr)
        {
            if (chkArchive.IsChecked == true )
            {
                _SearchStr = _SearchStr + " AND Status = 'Archived'";
            }
            else if (chkArchive.IsChecked == false)
            {
                _SearchStr = _SearchStr + " AND Status = 'Active'";
            }
            _ds.Tables[_MemberDetailsDataTable].DefaultView.RowFilter = "";
            _ds.Tables[_MemberDetailsDataTable].DefaultView.RowFilter = _SearchStr;
            grdFindMember.ItemsSource = null;
            grdFindMember.ItemsSource = _ds.Tables[_MemberDetailsDataTable].DefaultView;
        }
        private void cmbProgramme_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                string _SearchStr = "(LastName LIKE '%" + txtSearchfor.Text.Trim().Replace("'", "''") 
                    + "%' OR FirstName LIKE '%" + txtSearchfor.Text.Trim().Replace("'", "''") 
                    + "%' OR CardNumber LIKE '%" + txtSearchfor.Text.Trim().Replace("'", "''")
                    + "%' OR [Cell Phone] LIKE '%" + txtSearchfor.Text.Trim().Replace("'", "''") + "%')";
                if (Convert.ToInt32(cmbProgramme.SelectedValue) > 0)
                {
                    _ds.Tables[_MemberShipDataTable].DefaultView.RowFilter = "";
                    _ds.Tables[_MemberShipDataTable].DefaultView.RowFilter = "Programme = " + cmbProgramme.SelectedValue + " AND State <> 'EXPIRED'";
                    _SearchStr = _SearchStr + " AND [MemberNumSearch] IN ( 0 ";
                    if (_ds.Tables[_MemberShipDataTable].DefaultView.Count > 0)
                    {
                        //_SearchStr = _SearchStr + " AND MemberNo IN (";
                        for (int i = 0; i < _ds.Tables[_MemberShipDataTable].DefaultView.Count; i++)
                        {
                            if (i > 0)
                            {
                                _SearchStr = _SearchStr + "," + _ds.Tables[_MemberShipDataTable].DefaultView[i]["MemberID"].ToString();
                            }
                            else
                            {
                                //_SearchStr = _SearchStr + _ds.Tables[_MemberShipDataTable].DefaultView[i]["MemberID"].ToString();
                                _SearchStr = _SearchStr + "," + _ds.Tables[_MemberShipDataTable].DefaultView[i]["MemberID"].ToString();
                            }
                        }
                        //_SearchStr = _SearchStr + ")";
                    }
                    _SearchStr = _SearchStr + ")";
                }
                LoadMemberListOnSearch(_SearchStr);
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }
        
        public void SetAccessRightsDS(DataSet ds)
        {
            _dsAccessRights = ds;
        }

        private void EnableDisableControls_AccessRights()
        {
            btnAddMember.IsEnabled = false;
            btnAddMember.Opacity = 0.5;

            btnShowMember.IsEnabled = false;
            btnShowMember.Opacity = 0.5;

            btnCheckIn.IsEnabled = false;
            btnCheckIn.Opacity = 0.5;

            btnArchiveMember.IsEnabled = false;
            btnArchiveMember.Opacity = 0.5;

            if (_dsAccessRights != null)
            {
                if (_dsAccessRights.Tables.Count > 0)
                {
                    _dsAccessRights.Tables[_ModuleTable].DefaultView.RowFilter = "";
                    _dsAccessRights.Tables[_SubModuleTable].DefaultView.RowFilter = "";

                    if (_dsAccessRights.Tables[_SubModuleTable].DefaultView.Count > 0)
                    {
                        _dsAccessRights.Tables[_SubModuleTable].DefaultView.RowFilter = "ModuleName = 'FIND MEMBER' AND ACTIVE = 1";
                        for (int i = 0; i < _dsAccessRights.Tables[_SubModuleTable].DefaultView.Count; i++)
                        {
                            switch (_dsAccessRights.Tables[_SubModuleTable].DefaultView[i]["SubModuleName"].ToString().ToUpper())
                            {
                                case "ADD MEMBER":
                                    btnAddMember.IsEnabled = true;
                                    btnAddMember.Opacity = 1.00;
                                    break;
                                case "SHOW MEMBER":
                                    btnShowMember.IsEnabled = true;
                                    btnShowMember.Opacity = 1.00;
                                    _isShowMemberAccess = true;
                                    break;
                                case "CHECKIN MEMBER":
                                    btnCheckIn.IsEnabled = true;
                                    btnCheckIn.Opacity = 1.00;
                                    break;
                                case "HOLD MEMBER ACCESS":
                                    btnCheckIn.IsEnabled = true;
                                    btnCheckIn.Opacity = 1.00;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
            {
                btnArchiveMember.IsEnabled = true;
                btnArchiveMember.Opacity = 1.00;
            }
        }
        private void PopulateProgramFindMember_Disconnected()
        {
            _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            cmbProgramme.ItemsSource = null;
            Constants._DisConnectedALLTables.Tables["Programme"].DefaultView.RowFilter = "CompanyID = " + _CompanyID;
            DataTable table = new DataTable();
            table.Columns.Add("ProgrammeID", typeof(int));
            table.Columns.Add("ProgrammeName", typeof(string));
            DataRow dR = table.NewRow();
            dR["ProgrammeID"] = 0;
            dR["ProgrammeName"] = "<ALL>";
            table.Rows.Add(dR);
            table.Merge(Constants._DisConnectedALLTables.Tables["Programme"].DefaultView.ToTable("fm_Programme", false, "ProgrammeID", "ProgrammeName"));

            cmbProgramme.ItemsSource = table.DefaultView;
            cmbProgramme.DisplayMemberPath = "ProgrammeName";
            cmbProgramme.SelectedValuePath = "ProgrammeID";

            Constants._DisConnectedALLTables.Tables["Programme"].DefaultView.RowFilter = "";
        }

        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnArchiveMember_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string _Caption = "Archive Member";
                string _Msg = "This action will Archive the Member!\n\n"
                                                + "AND Purge all Membership and related Details!\n\n"
                                                + "Do you wish to ARCHIVE ?";
                bool _Flag = false;

                if (chkArchive.IsChecked == true)
                {
                    _Caption = "Make Active";
                    _Msg = "Do you wish to MAKE ACTIVE the Archived Member ?";
                    _Flag = true;
                }

                int i; int _MemberID;
                i = grdFindMember.SelectedIndex;
                string _CustomerGUID = "";

                if (i >= 0)
                {
                    if ((grdFindMember.SelectedItem as DataRowView).Row["Membership Status"].ToString().ToUpper() == "ACTIVE")
                    {
                        MessageBox.Show("Only Members who have NO ACTIVE MEMBERSHIP can be Archived!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                    else
                    { 
                        _MemberID = Convert.ToInt32((grdFindMember.Items[i] as DataRowView).Row["MemberNo"].ToString());
                        _CustomerGUID = (grdFindMember.Items[i] as DataRowView).Row["CustomerGUID"].ToString();

                        if (_MemberID > 0)
                        {
                            if (MessageBox.Show(_Msg, _Caption, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                            {
                                ////ARCHIVE THE MEMBER
                                Mouse.OverrideCursor = Cursors.Wait;
                                Archive_Member_MemberDetails(_MemberID, _Flag, _CustomerGUID);
                                chkArchive.IsChecked = false;
                                LoadMemberListFromDBOnLoad();
                                Mouse.OverrideCursor = null;
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Please Select a Member from the List!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error!" + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private void Archive_Member_MemberDetails(int _MemberID, bool _Flag, string _customerGUID = "")
        {
            _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            SqlHelper sql = new SqlHelper();
            //if (sql._ConnOpen == 0) return;
            if (sql._ConnOpen == 0) return;

            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            ht.Add("@MemberNo", _MemberID);
            ht.Add("@Flag", _Flag);

            sql.ExecuteQuery("Archive_Member_MemberDetails", ht);
            if (!_Flag)
            {
                if (_customerGUID.Trim() != "")
                {
                    ////ARCHIVE IN PAYCHOICE
                    Delete_All_PayChoice_Tables(_customerGUID);
                    paychoice.Delete_Paychoice_Customer(_customerGUID);
                }
            }
        }
        private void Delete_All_PayChoice_Tables(string _customerGUID)
        {
            ////CANCEL ALL SUBS. FOR THIS CUSTOMER.ID IN PAYCHOICE, AND DELETE LOCAL TABLES

            SqlHelper sql = new SqlHelper();
            //if (sql._ConnOpen == 0) return;
            if (sql._ConnOpen == 0) return;

            Hashtable ht = new Hashtable();
            ht.Clear();
            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            ht.Add("@CustomerGUID", _customerGUID);
            DataSet _dsSubsIDs = sql.ExecuteProcedure("Archive_GetIDs_Paychoice_Tables", ht);

            if (_dsSubsIDs != null)
            {
                if (_dsSubsIDs.Tables.Count > 0)
                {
                    if (_dsSubsIDs.Tables[0].DefaultView.Count > 0)
                    {
                        for (int i = 0; i < _dsSubsIDs.Tables[0].DefaultView.Count; i++)
                        {
                            string _subscriptionGUID = _dsSubsIDs.Tables[0].DefaultView[i]["Subscription_GUID"].ToString();
                            if (_subscriptionGUID.Trim() != "")
                            {
                                paychoice.Archive_Subscription(_subscriptionGUID);
                            }
                        }
                    }
                }
            }

            ht.Clear();
            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            ht.Add("@CustomerGUID", _customerGUID);
            sql.ExecuteQuery("Archive_Delete_Paychoice_Tables", ht);
        }
        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (btnShowMember.IsEnabled)
            {
                btnShowMember_Click(sender, e);
            }
        }

        private void chkArchive_Checked(object sender, RoutedEventArgs e)
        {
            string _SearchStr = "(LastName LIKE '%" + txtSearchfor.Text.Trim().Replace("'", "''") 
                + "%' OR FirstName LIKE '%" + txtSearchfor.Text.Trim().Replace("'", "''") 
                + "%' OR CardNumber LIKE '%" + txtSearchfor.Text.Trim().Replace("'", "''")
                + "%' OR [Cell Phone] LIKE '%" + txtSearchfor.Text.Trim().Replace("'", "''") + "%')";
            _ds.Tables[_MemberShipDataTable].DefaultView.RowFilter = "";
            _ds.Tables[_MemberShipDataTable].DefaultView.RowFilter = "Programme = " + cmbProgramme.SelectedValue + " AND State <> 'EXPIRED'";
            if (_ds.Tables[_MemberShipDataTable].DefaultView.Count > 0)
            {
                _SearchStr = _SearchStr + " AND [MemberNumSearch] IN (";
                for (int i = 0; i < _ds.Tables[_MemberShipDataTable].DefaultView.Count; i++)
                {
                    if (i > 0)
                    {
                        _SearchStr = _SearchStr + "," + _ds.Tables[_MemberShipDataTable].DefaultView[i]["MemberID"].ToString();
                    }
                    else
                    {
                        _SearchStr = _SearchStr + _ds.Tables[_MemberShipDataTable].DefaultView[i]["MemberID"].ToString();
                    }
                }
                _SearchStr = _SearchStr + ")";
            }
            _SearchStr = _SearchStr + " AND Status = 'Archived'";
            LoadMemberListOnSearch(_SearchStr);
            btnArchiveMember.Content = "Make Active";
            btnShowMember.IsEnabled = false;
        }

        private void chkArchive_Unchecked(object sender, RoutedEventArgs e)
        {
            string _SearchStr = "(LastName LIKE '%" + txtSearchfor.Text.Trim().Replace("'", "''") 
                + "%' OR FirstName LIKE '%" + txtSearchfor.Text.Trim().Replace("'", "''") 
                + "%' OR CardNumber LIKE '%" + txtSearchfor.Text.Trim().Replace("'", "''")
                + "%' OR [Cell Phone] LIKE '%" + txtSearchfor.Text.Trim().Replace("'", "''") + "%')";
            _ds.Tables[_MemberShipDataTable].DefaultView.RowFilter = "";
            _ds.Tables[_MemberShipDataTable].DefaultView.RowFilter = "Programme = " + cmbProgramme.SelectedValue + " AND State <> 'EXPIRED'";
            if (_ds.Tables[_MemberShipDataTable].DefaultView.Count > 0)
            {
                _SearchStr = _SearchStr + " AND [MemberNumSearch] IN (";
                for (int i = 0; i < _ds.Tables[_MemberShipDataTable].DefaultView.Count; i++)
                {
                    if (i > 0)
                    {
                        _SearchStr = _SearchStr + "," + _ds.Tables[_MemberShipDataTable].DefaultView[i]["MemberID"].ToString();
                    }
                    else
                    {
                        _SearchStr = _SearchStr + _ds.Tables[_MemberShipDataTable].DefaultView[i]["MemberID"].ToString();
                    }
                }
                _SearchStr = _SearchStr + ")";
            }
            _SearchStr = _SearchStr + " AND Status = 'Active'";
            LoadMemberListOnSearch(_SearchStr);
            btnArchiveMember.Content = "Archive Member";

            if (_isShowMemberAccess)
            {
                btnShowMember.IsEnabled = true;
            }
        }

        private void grdFindMember_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = "  " + (e.Row.GetIndex()+1).ToString("####0") + "  ";
        }

        private void grdFindMember_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int i = grdFindMember.SelectedIndex;

            if (i >= 0)
            {
                if (btnArchiveMember.Content.ToString().ToUpper() == "MAKE ACTIVE")
                {
                    btnArchiveMember.IsEnabled = true;
                }
                else
                {
                    if ((grdFindMember.SelectedItem as DataRowView).Row["Membership Status"].ToString().ToUpper() == "ACTIVE")
                    {
                        btnArchiveMember.IsEnabled = false;
                    }
                    else
                    {
                        btnArchiveMember.IsEnabled = true;
                    }
                }
            }
        }
    }
}
