﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;

using GymMembership.Comman;
using GymMembership.BAL;
using GymMembership.Task;
using GymMembership.Communications;

namespace GymMembership.MemberInfo
{
    /// <summary>
    /// Interaction logic for MembershipAddPayment.xaml
    /// </summary>
    public partial class MembershipAddPayment
    {
        public MembershipAddPayment()
        {
            InitializeComponent();
        }
        DataSet DSInvoice = new DataSet();
        Utilities _utilites = new Utilities();
        MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
        FormUtilities _formUtilities = new FormUtilities();

        public string _MemberName;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public bool _CanOpenMemberShipNewMember = false;
        public bool _CanOpenBillingBankNewMember = false;
        public bool ExistingMember = false;
        public int GetMemberDetailsMemberId; public long CardNumber;
        public DataSet _dSAccessRights;
        public DataTable dt = new DataTable();
        public int _CompanyID;
        public int _SiteID;

        public char _isProd;
        public int strAddPaymentMemberID = 0;
        private decimal _amountPaid = 0m;
        private int _paymentMode = 0;
        private string _paymentNotes;
        private DateTime _paymentDate;
        private string _invoiceNo;
        private int _PaidForItemNo;

        private int _addMembershipPaymentSuccess = 0;
        private bool reval;
        bool _canExitWithoutPayment = true;

        private void AddPayment_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                EnableControlsOnPending();
                populateData();
                ClearPaymentData();

                if (strAddPaymentMemberID == 0)
                {
                    decimal _totNonMember = 0m;
                    for (int i = 0; i < DSInvoice.Tables[0].Rows.Count; i++)
                    {
                        if (DSInvoice.Tables[0].Rows[i]["ChkInd"].ToString() == "1")
                        {
                            _totNonMember += Convert.ToDecimal(DSInvoice.Tables[0].Rows[i]["Amount"].ToString());
                        }
                    }
                    txtMSTotalPaid.Text = _totNonMember.ToString("###0.#0");

                    _canExitWithoutPayment = false;
                    this.IsCloseButtonEnabled = false;
                    this.IsMaxRestoreButtonEnabled = false;
                    this.IsMinButtonEnabled = false;
                    btnbacktoDashBoard.IsEnabled = false;

                    listBox.Visibility = Visibility.Collapsed;
                    leftPanel.Width = new GridLength(38);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Add Payment Load Error! " + ex.Message,this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
            }
        }

        private void btnMSCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (grdMembershipAddPayment.Items.Count > 0)
                {
                    if (MessageBox.Show("Edit in Progress, Do you want to Close?", "Add Payment", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        this.Close();
                    }
                }
                else
                {
                    this.Close();
                }
            }
            catch(Exception ex)
            {
                string _exMsg = ex.Message;
            }
        }
        public void populateData()
        {
            try
            {
                _utilites.PopulatePaymentType(cmbMSPaymentMtd);
                _utilites.PopulateInvoiceNoByMemberId(cmbMSForaInvoice, strAddPaymentMemberID);

                if (_isProd == '\0') { _isProd = 'C'; }
                showPendingInvoiceDetails(_isProd);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void ClearPaymentData()
        {
            txtMSTotalPaid.Text = "0.00";
            txtMSNotes.Text = string.Empty;
            txtMSPaymentDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            cmbMSPaymentMtd.SelectedIndex = -1;
            cmbMSForaInvoice.SelectedIndex = -1;
        }

        private bool validateData()
        {
            reval = false;

            if (string.IsNullOrEmpty(txtMSTotalPaid.Text.ToString()))
            {
                MessageBox.Show("Please Enter Paid Amount", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtMSTotalPaid.Focus();
                reval = false;
                return reval;
            }

            if (txtMSTotalPaid.Text.Trim() == "0.00")
            {
                MessageBox.Show("Please Select atleast 1 invoice Item to make Payment!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                reval = false;
                return reval;
            }

            if (cmbMSPaymentMtd.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Payment Mode.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                cmbMSPaymentMtd.Focus();
                reval = false;
                return reval;
            }

            //if (string.IsNullOrEmpty(txtMSNotes.Text.ToString()))
            //{
            //    MessageBox.Show("Please Enter Notes", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //    txtMSNotes.Focus();
            //    reval = false;
            //    return reval;
            //}

            if (string.IsNullOrEmpty(txtMSPaymentDate.Text.ToString()))
            {
                MessageBox.Show("Please Enter Payment Date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtMSPaymentDate.Focus();
                reval = false;
                return reval;
            }

            if (_formUtilities.CheckDate(txtMSPaymentDate.Text) == false)
            {
                MessageBox.Show("Please Enter Payment Date in dd/mm/yyyy format.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtMSPaymentDate.Focus();
                reval = false;
                return reval;
            }
            return true;
        }

        private void btnMSAddPayment_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (validateData())
                {
                    for (int i = 0; i < grdMembershipAddPayment.Items.Count; i++)
                    {
                        if (DSInvoice.Tables[0].Rows[i]["ChkInd"].ToString() == "1")
                        {
                            _paymentDate = Convert.ToDateTime(txtMSPaymentDate.Text.ToString().Trim());
                            _paymentMode = Convert.ToInt32(cmbMSPaymentMtd.SelectedValue.ToString());
                            _amountPaid = Convert.ToDecimal(DSInvoice.Tables[0].Rows[i]["Amount"].ToString());
                            _paymentNotes = txtMSNotes.Text.ToString().Trim();
                            _invoiceNo = DSInvoice.Tables[0].Rows[i]["InvoiceNo"].ToString();
                            _PaidForItemNo = Convert.ToInt32(DSInvoice.Tables[0].Rows[i]["ID"].ToString());

                            _addMembershipPaymentSuccess = _memberDetailsBal.add_MembershipPayments(_paymentDate, _paymentMode, _amountPaid, _paymentNotes, _invoiceNo, _PaidForItemNo);
                        }
                    }
                    if (_addMembershipPaymentSuccess != 0)
                    {
                        MessageBox.Show("Payment Added Successfully!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        if (strAddPaymentMemberID == 0)
                        {
                            this.Close();
                        }
                        ClearPaymentData();
                        showPendingInvoiceDetails(_isProd);
                    }
                    else
                    {
                        MessageBox.Show("Payment was NOT Successful!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        ClearPaymentData();
                        showPendingInvoiceDetails(_isProd);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void showPendingInvoiceDetails(char _isProd)
        {
            try
            {
                grdMembershipAddPayment.ItemsSource = null;
                DSInvoice = _memberDetailsBal.get_PendingInvoiceDetailsByMemberID(strAddPaymentMemberID, _isProd);

                //if (DSInvoice.Tables[0] != null)
                if (DSInvoice.Tables[0].Rows.Count > 0)
                {
                    grdMembershipAddPayment.ItemsSource = DSInvoice.Tables[0].AsDataView();
                    btnMSAddPayment.IsEnabled = true;
                }
                else
                {
                    DisableControlsOnNOPending();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void PaidFor_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                decimal TotPaid = Convert.ToDecimal(txtMSTotalPaid.Text);
                int i = grdMembershipAddPayment.SelectedIndex;
                if (i >= 0)
                {
                    //Column: 4 - Is actually Amount and NOT column:3 !!!
                    DSInvoice.Tables[0].Rows[i]["ChkInd"] = "1";
                    TotPaid = TotPaid + Convert.ToDecimal((grdMembershipAddPayment.Items[i] as DataRowView).Row.ItemArray[4].ToString());
                    txtMSTotalPaid.Text = TotPaid.ToString();

                    //grdMembershipAddPayment.ItemsSource = DSInvoice.Tables[0].AsDataView();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! - AddPayment Screen \n" + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void PaidFor_UnChecked(object sender, RoutedEventArgs e)
        {
            try
            {
                decimal TotPaid = Convert.ToDecimal(txtMSTotalPaid.Text);
                int i = grdMembershipAddPayment.SelectedIndex;
                if (i >= 0)
                {
                    //Column: 4 - Is actually Amount and NOT column:3 !!!
                    DSInvoice.Tables[0].Rows[i]["ChkInd"] = "0";
                    TotPaid = TotPaid - Convert.ToDecimal((grdMembershipAddPayment.Items[i] as DataRowView).Row.ItemArray[4].ToString());
                    txtMSTotalPaid.Text = TotPaid.ToString();

                    //grdMembershipAddPayment.ItemsSource = DSInvoice.Tables[0].AsDataView();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! - AddPayment Screen \n" + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void DisableControlsOnNOPending()
        {
            lblNoPendingMsg.Visibility = Visibility.Visible;
            btnMSAddPayment.IsEnabled = false;
            txtMSTotalPaid.IsEnabled = false;
            cmbMSPaymentMtd.IsEnabled = false;
            txtMSNotes.IsEnabled = false;
            txtMSPaymentDate.IsEnabled = false;
        }
        private void EnableControlsOnPending()
        {
            lblNoPendingMsg.Visibility = Visibility.Hidden;
            btnMSAddPayment.IsEnabled = true;
            txtMSTotalPaid.IsEnabled = false;
            cmbMSPaymentMtd.IsEnabled = true;
            txtMSNotes.IsEnabled = true;
            txtMSPaymentDate.IsEnabled = true;
        }

        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnShowMembership_Click(object sender, RoutedEventArgs e)
        {
            Memberships _mDetails = new Memberships();
            _mDetails.ExistingMember = ExistingMember;
            _mDetails.CardNumber = CardNumber;
            _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
            _mDetails._MemberName = _MemberName;
            _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
            _mDetails._CanOpenBillingBankNewMember = _CanOpenBillingBankNewMember;
            _mDetails._CompanyName = _CompanyName;
            _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
            _mDetails._SiteName = _SiteName;
            _mDetails._StaffPic = _StaffPic;
            _mDetails._UserName = _UserName;
            _mDetails._CompanyID = _CompanyID;
            _mDetails._SiteID = _SiteID;
            _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
            _mDetails.StaffImage.Source = StaffImage.Source;
            _mDetails._dSAccessRights = _dSAccessRights;
            _mDetails.dt = this.dt;
            _mDetails.Owner = this.Owner;

            this.Close();
            _mDetails.ShowDialog();
        }
        private void btnAccounts_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberAccounts mAccounts = new MemberAccounts();
                mAccounts.MemberID = GetMemberDetailsMemberId;
                mAccounts._CurrentLoginDateTime = _CurrentLoginDateTime;
                mAccounts._CompanyName = _CompanyName;
                mAccounts._SiteName = _SiteName;
                mAccounts._UserName = _UserName;
                mAccounts._StaffPic = _StaffPic;
                mAccounts._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mAccounts.ExistingMember = ExistingMember;
                mAccounts.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mAccounts._MemberName = _MemberName;
                mAccounts.Member_Image_Img.Source = Member_Image_Img.Source;
                mAccounts.StaffImage.Source = StaffImage.Source;
                mAccounts._dSAccessRights = _dSAccessRights;
                mAccounts.dt = this.dt;

                mAccounts.Owner = this.Owner;
                this.Cursor = System.Windows.Input.Cursors.Wait;
                btnAccounts.Cursor = System.Windows.Input.Cursors.Wait;
                this.Close();
                mAccounts.ShowDialog();
            }
        }

        private void btnMemberBillingDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ExistingMember && _CanOpenBillingBankNewMember)
                {
                    this.Cursor = System.Windows.Input.Cursors.Wait;
                    PaymentDetails PD = new PaymentDetails(GetMemberDetailsMemberId, _CompanyID, _SiteID, dt);
                    this.Cursor = System.Windows.Input.Cursors.Arrow;

                    PD._CurrentLoginDateTime = _CurrentLoginDateTime;
                    PD._CompanyName = _CompanyName;
                    PD._SiteName = _SiteName;
                    PD._UserName = _UserName;
                    PD._StaffPic = _StaffPic;
                    PD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    PD._CanOpenBillingBankNewMember = _CanOpenBillingBankNewMember;
                    PD.ExistingMember = ExistingMember;
                    PD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    PD._MemberName = _MemberName;
                    PD.Member_Image_Img.Source = Member_Image_Img.Source;
                    PD.StaffImage.Source = StaffImage.Source;
                    PD._dSAccessRights = _dSAccessRights;
                    PD.dt = this.dt;

                    PD.Owner = this.Owner;
                    this.Close();
                    PD.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnMemberExtraDetails_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                ExtraDetails extra = new ExtraDetails();
                extra.strExtraDetailsMemberID = GetMemberDetailsMemberId; //Convert.ToInt32(txtMemberNumber.Text.ToString().Trim());

                extra._CurrentLoginDateTime = _CurrentLoginDateTime;
                extra._CompanyName = _CompanyName;
                extra._SiteName = _SiteName;
                extra._UserName = _UserName;
                extra._StaffPic = _StaffPic;
                extra._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                extra.ExistingMember = ExistingMember;
                extra.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                extra._MemberName = _MemberName;
                extra.Member_Image_Img.Source = Member_Image_Img.Source;
                extra.StaffImage.Source = StaffImage.Source;
                extra._dSAccessRights = _dSAccessRights;
                extra.dt = this.dt;

                extra.Owner = this.Owner;
                this.Close();
                extra.ShowDialog();
            }
        }

        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                Tasks Tsk = new Tasks();
                Tsk.strMemberNo = GetMemberDetailsMemberId; //Convert.ToInt32(txtMemberNumber.Text);
                Tsk.Owner = this.Owner;
                Tsk.SetAccessRightsDS(_dSAccessRights);

                Tsk._CurrentLoginDateTime = _CurrentLoginDateTime;
                Tsk._CompanyName = _CompanyName;
                Tsk._SiteName = _SiteName;
                Tsk._UserName = _UserName;
                Tsk._StaffPic = _StaffPic;
                Tsk._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                Tsk.ExistingMember = ExistingMember;
                Tsk.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                Tsk._MemberName = _MemberName;
                Tsk.Member_Image_Img.Source = Member_Image_Img.Source;
                Tsk.StaffImage.Source = StaffImage.Source;
                Tsk._dSAccessRights = _dSAccessRights;
                Tsk.dt = this.dt;

                this.Close();
                Tsk.ShowDialog();
            }
        }

        private void btnCommunications_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Communication _Comm = new Communication();
                _Comm.Owner = this.Owner;
                _Comm.SetAccessRightsDS(_dSAccessRights);
                this.Cursor = System.Windows.Input.Cursors.Arrow;

                _Comm._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Comm._CompanyName = _CompanyName;
                _Comm._SiteName = _SiteName;
                _Comm._UserName = _UserName;
                _Comm._StaffPic = _StaffPic;
                _Comm._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Comm.ExistingMember = ExistingMember;
                _Comm.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Comm._MemberName = _MemberName;
                _Comm.Member_Image_Img.Source = Member_Image_Img.Source;
                _Comm.StaffImage.Source = StaffImage.Source;
                _Comm._dSAccessRights = _dSAccessRights;
                _Comm.dt = this.dt;

                this.Close();
                _Comm.ShowDialog();
            }
        }

        private void btnNotes_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Notes _Notes = new Notes();
                _Notes.Owner = this.Owner;

                _Notes._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Notes._CompanyName = _CompanyName;
                _Notes._SiteName = _SiteName;
                _Notes._UserName = _UserName;
                _Notes._StaffPic = _StaffPic;
                _Notes._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Notes.ExistingMember = ExistingMember;
                _Notes.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Notes._MemberName = _MemberName;
                _Notes.Member_Image_Img.Source = Member_Image_Img.Source;
                _Notes.StaffImage.Source = StaffImage.Source;
                _Notes._dSAccessRights = _dSAccessRights;
                _Notes.dt = this.dt;

                this.Cursor = System.Windows.Input.Cursors.Arrow;
                this.Close();
                _Notes.ShowDialog();
            }
        }

        private void btnPersonalTraining_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                PersonalTraining _pt = new PersonalTraining();
                _pt.Owner = this.Owner;

                _pt._CurrentLoginDateTime = _CurrentLoginDateTime;
                _pt._CompanyName = _CompanyName;
                _pt._SiteName = _SiteName;
                _pt._UserName = _UserName;
                _pt._StaffPic = _StaffPic;
                _pt._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _pt.ExistingMember = ExistingMember;
                _pt.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _pt._MemberName = _MemberName;
                _pt.Member_Image_Img.Source = Member_Image_Img.Source;
                _pt.StaffImage.Source = StaffImage.Source;
                _pt._dSAccessRights = _dSAccessRights;
                _pt.dt = this.dt;

                this.Close();
                _pt.ShowDialog();
            }
        }

        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberDetails mD = new MemberDetails();
                mD.Owner = this.Owner;

                mD._CurrentLoginDateTime = _CurrentLoginDateTime;
                mD._CompanyName = _CompanyName;
                mD._SiteName = _SiteName;
                mD._UserName = _UserName;
                mD._StaffPic = _StaffPic;
                mD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mD.ExistingMember = ExistingMember;
                mD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mD._MemberName = _MemberName;
                mD.Member_Image_Img.Source = Member_Image_Img.Source;
                mD.StaffImage.Source = StaffImage.Source;
                mD._dSAccessRights = _dSAccessRights;
                mD.dt = this.dt;

                this.Close();
                mD.ShowDialog();
            }
        }

        private void cmbMSPaymentMtd_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbMSPaymentMtd.SelectedIndex != -1)
            {
                if (strAddPaymentMemberID == 0)
                {
                    string _payMethod = (cmbMSPaymentMtd.SelectedItem as DataRowView).Row["PaymentType"].ToString().ToUpper();
                    if (_payMethod == "PART OF MEMBERSHIP")
                    {
                        MessageBox.Show("Payment Mode Irrelevant for NON-MEMBER!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        cmbMSPaymentMtd.SelectedIndex = -1;
                    }
                }
            }
        }
    }
}
