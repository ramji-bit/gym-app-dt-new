﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GymMembership.BAL;
using System.Configuration;
using System.Data;

namespace GymMembership.MemberInfo
{
    /// <summary>
    /// Interaction logic for AddFreeTime.xaml
    /// </summary>
    public partial class AddFreeTime 
    {
        public AddFreeTime()
        {
            InitializeComponent();
        }

        public int strMemberNo = 0;
        public int strProgramId = 0;
        public int strHoldId = 0;

        private int _timeLength = 0;
        private int _timeDuration = 0;
        private DateTime StartDt;
        private DateTime EndDt;
        private bool reval;
        private DateTime _FreeTimetartDate;
        private DateTime _FreeTimeEndDate;
        private string _FreeTimeReason = string.Empty;
        private bool _isFreeTime = true;
        private int _freeTimeStatus = 0;
        private int _feeType = 0;
        private bool _endHoldMember = false;
        private bool _prorataMember = false;
        private decimal _feeAmount = 0m;
        private int _companyId = 0;



        MemberDetailsBAL _memberDetailsBAL = new MemberDetailsBAL();
        MembershipHold _membershipHold = new MembershipHold();

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            PopulateData();
        }

        private void PopulateData()
        {
            try
            {
                clearData();
                cmbFreeTimeDuration.SelectedIndex = 0;

                cldrStartDate.SelectedDate = (Convert.ToDateTime(DateTime.Now.ToString()));
                cldrEndDate.SelectedDate = (Convert.ToDateTime(DateTime.Now.ToString()));
                if (strHoldId != 0)
                {
                    LoadFreeTimeDetails();
                }
                txtFreeTimeLength.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        private void clearData()
        {
            txtReason.Text = string.Empty;
        }


        private void cmbFreeTimeLength_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AddDate();
        }

        private void txtFreeTimeLength_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                AddDate();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void AddDate()
        {
            try
            {
                StartDt = Convert.ToDateTime(cldrStartDate.SelectedDate);
                _timeDuration = Convert.ToInt32(cmbFreeTimeDuration.SelectedValue.ToString());

                if (txtFreeTimeLength.Text.ToString() != "")
                {
                    _timeLength = Convert.ToInt32(txtFreeTimeLength.Text.ToString());
                }
                else
                {
                    _timeLength = 0;
                }



                if (_timeDuration == 1)
                {
                    EndDt = StartDt.AddDays(_timeLength);
                }
                else if (_timeDuration == 2)
                {
                    _timeLength = (_timeLength * 7);
                    EndDt = StartDt.AddDays(_timeLength);
                }
                else if (_timeDuration == 3)
                {
                    EndDt = StartDt.AddDays(1).AddMonths(_timeLength).AddDays(-1);
                }
                else
                {
                    EndDt = StartDt.AddYears(_timeLength);
                }


                if (_timeLength == 0)
                {
                    cldrEndDate.SelectedDate = (Convert.ToDateTime(DateTime.Now.ToString()));
                    cldrEndDate.DisplayDate = (Convert.ToDateTime(DateTime.Now.ToString()));
                }
                else
                {
                    cldrEndDate.SelectedDate = (Convert.ToDateTime(EndDt.ToString()));
                    cldrEndDate.DisplayDate = (Convert.ToDateTime(EndDt.ToString()));
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void LoadFreeTimeDetails()
        {
            DataSet FreeTimeDs = _memberDetailsBAL.get_FreeTimeDetails(strHoldId);

            if (FreeTimeDs.Tables[0] != null)
            {
                txtReason.Text = FreeTimeDs.Tables[0].Rows[0]["HoldReason"].ToString();
                cldrStartDate.SelectedDate = Convert.ToDateTime(FreeTimeDs.Tables[0].Rows[0]["HoldStartDate"].ToString());
                cldrEndDate.SelectedDate = Convert.ToDateTime(FreeTimeDs.Tables[0].Rows[0]["HoldEndDate"].ToString());
            }


        }

        private void btnAddFreeTime_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _FreeTimetartDate = (DateTime)cldrStartDate.SelectedDate;
                _FreeTimeEndDate = (DateTime)cldrEndDate.SelectedDate;
                _FreeTimeReason = txtReason.Text.ToString();
                _freeTimeStatus = 1;
                _companyId = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());

                int addFreeTimeSuccess = _memberDetailsBAL.AddMemberHold(strHoldId, _FreeTimetartDate, _FreeTimeEndDate, _FreeTimeReason, _feeType, strMemberNo, _companyId, _endHoldMember, _prorataMember, _freeTimeStatus, _feeAmount, strProgramId, _isFreeTime);

                if (addFreeTimeSuccess != 0)
                {
                    _membershipHold.strMemberNo = strMemberNo;
                    MessageBox.Show("Member Free Time Details Added Successfully...!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    _membershipHold.LoadMemberHoldDetails();
                    this.Close();
                    return;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Membership Free, Are you sure to Close?", "Add Membership Free Time", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                this.Close();
            }
        }

        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnShowMembership_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnAccounts_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnMemberBillingDetails_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnMemberExtraDetails_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnCommunications_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnNotes_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnPersonalTraining_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
