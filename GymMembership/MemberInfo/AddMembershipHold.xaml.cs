﻿using System;
using System.Collections;
using System.Linq;
using System.Data;
using System.Text.RegularExpressions;
using System.Windows;
using System.Configuration;

using GymMembership.BAL;
using Paychoice_Payment;

namespace GymMembership.MemberInfo
{
    /// <summary>
    /// Interaction logic for AddMembershipHold.xaml
    /// </summary>
    public partial class AddMembershipHold 
    {
        public AddMembershipHold()
        {
            InitializeComponent();
        }
        MemberDetailsBAL _memberDetailsBAL = new MemberDetailsBAL();
        MembershipHold _membershipHold = new MembershipHold();

        public int strMemberNo = 0;
        public int strProgramId = 0;
        public int MSUniqueID = 0;

        private string _holdReason = string.Empty;
        private DateTime _holdStartDate;
        private DateTime _holdEndDate;
        private int _holdFeeType = 0;
        private int _companyId = 0;
        private int _siteId = 0;
        private decimal _feeAmount = 0m;
        private DateTime _holdSdate;
        private DateTime _holdEdate;
        private bool _endHoldMember = false;
        private bool _prorataMember = false;
        private int _holdStatus = 0;
        private bool _freeFee;
        private bool _fullCostFee;
        private bool _setupCostFee;
        private bool _onGoingFee;
        private bool reval;
        private bool _isFreeTime = false;
        private int _holdId = 0;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            PopulateData();
            txtHoldReason.Focus();
        }
        private void PopulateData()
        {
            clearData();
            HideSetUpCostFee();
            grpHoldFee.Visibility = Visibility.Hidden;
            //HideOnGoingFee();
        }
        private bool validateData()
        {
            reval = true;

            if (string.IsNullOrEmpty(txtHoldReason.Text.ToString()))
            {
                MessageBox.Show("Please Enter Hold Reason", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtHoldReason.Focus();
                reval = false;
                return reval;
            }

            if (rbHoldTypeImmediate.IsChecked == true)
            {

            }
            else
            {
                if (cldrHoldStartDate.SelectedDate.ToString().Trim() == "")
                {
                    _holdSdate = (DateTime)cldrHoldStartDate.DisplayDate;
                }
                else
                {
                    _holdSdate = (DateTime)cldrHoldStartDate.SelectedDate;
                }

                if (cldrHoldEndDate.SelectedDate.ToString().Trim() == "")
                {
                    _holdEdate = (DateTime)cldrHoldEndDate.DisplayDate;
                }
                else
                {
                    _holdEdate = (DateTime)cldrHoldEndDate.SelectedDate;
                }

                if (_holdSdate.Date < DateTime.Now.Date)
                {
                    MessageBox.Show("Start Date CANNOT be less than Today!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    cldrHoldStartDate.Focus();
                    reval = false;
                    return reval;
                }
                if (_holdSdate.Date >= _holdEdate.Date)
                {
                    MessageBox.Show("End Date CANNOT be less than (OR) same as Start Date!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    cldrHoldEndDate.Focus();
                    reval = false;
                    return reval;
                }
            }            

            if (rbHoldTypeOR.IsChecked == true)
            {
                if (Convert.ToBoolean(rbSetupCost.IsChecked.ToString()) == true)
                {

                }

                if (Convert.ToBoolean(rbOngoingFee.IsChecked.ToString()) == true)
                {

                }

                if (Convert.ToBoolean(rbAmendFee.IsChecked.ToString()) == true)
                {
                    if (txtAmendFee.Text.Trim() == "")
                    {
                        MessageBox.Show("Please enter Amended Fee!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        txtAmendFee.Focus();
                        reval = false;
                        return reval;
                    }
                    else
                    {
                        if (!Regex.IsMatch(txtAmendFee.Text, @"^\d{1,7}(\.\d{1,2})?$"))
                        {
                            txtAmendFee.Text = "";
                            MessageBox.Show("Please Enter Amended Fee in Numbers only! (upto 2 decimal places)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            txtAmendFee.Focus();
                            reval = false;
                            return reval;
                        }
                    }
                }
            }
            return true;
        }

        private void btnSaveHoldDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string _ResultMessage = "";
                Cursor = System.Windows.Input.Cursors.Wait;
                btnSaveHoldDetails.Cursor = System.Windows.Input.Cursors.Wait;

                if (validateData())
                {
                    _holdStartDate = (DateTime)cldrHoldStartDate.SelectedDate;
                    _holdEndDate = (DateTime)cldrHoldEndDate.SelectedDate;
                    _holdReason = txtHoldReason.Text.ToString();
                    _freeFee = Convert.ToBoolean(rbHoldFree.IsChecked.ToString());
                    _fullCostFee = Convert.ToBoolean(rbHoldFullCost.IsChecked.ToString());
                    _setupCostFee = Convert.ToBoolean(rbSetupCost.IsChecked.ToString());
                    _onGoingFee = Convert.ToBoolean(rbOngoingFee.IsChecked.ToString());

                    if (_freeFee == true)
                    {
                        _holdFeeType = 1;
                        _feeAmount = 0;
                    }
                    else if (_fullCostFee == true)
                    {
                        _holdFeeType = 2;
                        _feeAmount = 0;
                    }
                    else if (_setupCostFee == true)
                    {
                        _holdFeeType = 3;
                        _feeAmount = Convert.ToDecimal(txtInitialSetupFee.Text.ToString());
                    }
                    else if (rbAmendFee.IsChecked == true)
                    {
                         _feeAmount = Convert.ToDecimal( txtAmendFee.Text.ToString());
                    }
                    else
                    {
                        _holdFeeType = 4;
                        _feeAmount = 0;
                    }

                    _companyId = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    _siteId = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                    _endHoldMember = Convert.ToBoolean(chkMemberReturnHold.IsChecked.ToString());
                    _prorataMember = Convert.ToBoolean(chkProrataMembershipFee.IsChecked.ToString());
                    _holdStatus = 1;

                    //PAYCHOICE
                    if (rbHoldTypeOR.IsChecked == true)
                    {
                        if (rbOngoingFee.IsChecked == false)
                        {
                            Hashtable htguid = new Hashtable();
                            DataManager dal = new DataManager();
                            htguid.Add("@MemberID", strMemberNo);
                            htguid.Add("@Programme", strProgramId); //Programme Id Input
                            htguid.Add("@CompanyID", _companyId);
                            htguid.Add("@SiteID", _siteId);
                            DataSet paychoice_GUID_List = dal.ExecuteProcedureReader("FetchSubscriptionID_Memberships_NEW", htguid);
                            htguid.Clear();
                            if (paychoice_GUID_List != null)
                            {
                                if (paychoice_GUID_List.Tables.Count > 0)
                                {
                                    if (paychoice_GUID_List.Tables[0].DefaultView.Count > 0)
                                    {
                                        string subscription_id = paychoice_GUID_List.Tables[0].Rows[0]["Subscription_Guid"].ToString();
                                        if (subscription_id.Trim() != "")
                                        {
                                            DateTime edit_fromdate = _holdSdate;
                                            DateTime edit_todate = _holdEdate;
                                            double amount = Convert.ToDouble(_feeAmount);
                                            _ResultMessage = paychoice.Edit_Payment_Schedule(subscription_id, edit_fromdate, edit_todate, amount);
                                        }
                                    }
                                }
                            }

                            if (_ResultMessage.Trim() == "")
                            {
                                MessageBox.Show("Membership Hold Applied Successfully...! (NO SUBSCRIPTIONS TO AMEND!!) \n\n" + _ResultMessage, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                    }
                    else if(rbHoldTypeImmediate.IsChecked == true)
                    {
                        _memberDetailsBAL.MemberHoldImmediate(_companyId, _siteId, strMemberNo, strProgramId, 1);
                        Comman.Constants.Create_ACCESS_DAT_File(false, strMemberNo);
                        _memberDetailsBAL.MemberHoldImmediate(_companyId, _siteId, strMemberNo, strProgramId, 0);
                    }
                    //PAYCHOICE

                    int _result = _memberDetailsBAL.MemberHoldImmediate(_companyId, _siteId, strMemberNo, strProgramId, 0);
                    int addMemberHoldSuccess = _memberDetailsBAL.AddMemberHold(_holdId,_holdStartDate, _holdEndDate, _holdReason, _holdFeeType, strMemberNo, _companyId, _endHoldMember, _prorataMember, _holdStatus, _feeAmount, strProgramId, _isFreeTime, MSUniqueID);

                    if (addMemberHoldSuccess != 0)
                    {
                        _membershipHold.strMemberNo = strMemberNo;
                        if (_ResultMessage.Trim() != "")
                        {
                            MessageBox.Show("Membership Hold Applied Successfully...!\n\n" + _ResultMessage, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        _membershipHold.LoadMemberHoldDetails();
                        this.Cursor = System.Windows.Input.Cursors.Arrow;
                        btnSaveHoldDetails.Cursor = System.Windows.Input.Cursors.Hand;
                        this.Close();
                        return;
                    }
                }
                Cursor = System.Windows.Input.Cursors.Arrow;
                btnSaveHoldDetails.Cursor = Cursor = System.Windows.Input.Cursors.Hand;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                Cursor = System.Windows.Input.Cursors.Arrow;
                btnSaveHoldDetails.Cursor = Cursor = System.Windows.Input.Cursors.Hand;
            }
        }

        private void HideSetUpCostFee()
        {
            lblInitialSetupFee.Visibility = Visibility.Hidden;
            txtInitialSetupFee.Visibility = Visibility.Hidden;
        }

        private void ShowSetUpCostFee()
        {
            lblInitialSetupFee.Visibility = Visibility.Visible;
            txtInitialSetupFee.Visibility = Visibility.Visible;
        }

        private void HideOnGoingFee()
        {
            lblAmendFee.Visibility = Visibility.Hidden;
            txtAmendFee.Visibility = Visibility.Hidden;
        }

        private void ShowOnGoingFee()
        {
            lblAmendFee.Visibility = Visibility.Visible;
            txtAmendFee.Visibility = Visibility.Visible;
        }

        private void clearData()
        {
            txtHoldReason.Text = string.Empty;
            cldrHoldStartDate.SelectedDate = (DateTime?)DateTime.Now;
            cldrHoldEndDate.SelectedDate = (DateTime?)DateTime.Now;
            rbOngoingFee.IsChecked = true;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            //if (MessageBox.Show("Member Hold, Are you sure to Close?", "Add Member Hold", MessageBoxButton.YesNo,MessageBoxImage.Question) == MessageBoxResult.Yes)
            //{
            //    this.Close();
            //}
            Close();
        }

        private void rbHoldFree_Checked(object sender, RoutedEventArgs e)
        {
            HideSetUpCostFee();
            HideOnGoingFee();
        }

        private void rbHoldFullCost_Checked(object sender, RoutedEventArgs e)
        {
            HideSetUpCostFee();
            HideOnGoingFee();
        }

        private void rbSetupCost_Checked(object sender, RoutedEventArgs e)
        {
            HideOnGoingFee();
            ShowSetUpCostFee();
        }

        private void rbOngoingFee_Checked(object sender, RoutedEventArgs e)
        {
            HideSetUpCostFee();
            ShowOnGoingFee();
        }

        private void rbHoldTypeOR_Checked(object sender, RoutedEventArgs e)
        {
            grpHoldFee.Visibility = Visibility.Visible;
            grpAmendFee.Visibility = Visibility.Visible;

            cldrHoldStartDate.IsEnabled = true;
            cldrHoldEndDate.IsEnabled = true;
        }

        private void rbHoldTypeOR_Unchecked(object sender, RoutedEventArgs e)
        {
            grpHoldFee.Visibility = Visibility.Hidden;
            grpAmendFee.Visibility = Visibility.Hidden;

            cldrHoldStartDate.IsEnabled = false;
            cldrHoldEndDate.IsEnabled = false;
        }
    }
}
