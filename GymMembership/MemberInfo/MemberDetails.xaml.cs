﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GymMembership.Task;
using GymMembership.Communications;
using GymMembership.BAL;
using GymMembership.DTO;
using GymMembership.Comman;
using System.IO;
using System.Data;
using System.Configuration;
using WebEye.Controls.Wpf;

using Paychoice_Payment;
using TechnoGymAPI;

namespace GymMembership.MemberInfo
{
    /// <summary>
    /// Interaction logic for MemberDetails.xaml
    /// </summary>
    public partial class MemberDetails 
    {
        List<WebCameraId> cameras;

        public string _MemberName;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public bool _CanOpenMemberShipNewMember = false;
        public bool _CanOpenBillingBankNewMember = false;
        public bool ExistingMember = false;
        public int GetMemberDetailsMemberId; public long CardNumber;
        public bool _isFromMemberDetailsNewMember = false;
        public string _appVersion = "";

        string _OldCardNo = "0";
        string _datCONTENT = "";
        bool _isCardDenyCreated = false;

        public int _memberUniqueID = 0;
        public string _OldFirstName, _OldLastName;

        public bool _technoGymToggle = false;
        public string _technoGymPermanentToken = "";
        public string _technoGymFacilityEntityID = "";
        public string _technoGymAccessToken = "";
        string _technoGymMsg = "";

        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;

            if (_MemberName != null)
            {
                if (_MemberName.Trim() != "")
                {
                    Heading.Text = Heading.Text + " - " + _MemberName;
                }
            }
            //loadStaffPhoto(_StaffPic);
        }
        private void loadStaffPhoto(byte[] _StaffPic)
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }
        public MemberDetails()
        {
            InitializeComponent();
            this.Closing += new CancelEventHandler(MemberDetails_Closing);
            if (ConfigurationManager.AppSettings["LoggedInOrgID"] != "")
            {
                _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
            }
        }

        void MemberDetails_Closing(object sender, CancelEventArgs e)
        {
            if (_IsCameraStarted)
            {
                //_webcam.Stop();
                //_webcam = null;
                if (!_IsCaptured)
                { _webCamMS.StopCapture(); }
            }
        }
        //webCam _webcam = new webCam();
        
        string fileNamePath_MemberPhoto;
        private string _AutoGenerateMemberNo = "MemberNo";
        private string _AutoGenerateCardNo = "CardNo";
        private string _AutoGenerateInvoiceNo = "InvoiceNo";

        private DateTime _currentDate;
        private decimal _invoiceAmount;
        private decimal _amountPaid = 0;
        private decimal _amountOutStanding = 0;
        private int _invoiceMemberId = 0;
        private string _invoiceStatus;
        private string _fieldNameType = string.Empty;
        private int _memberInvoiceNo;
        public decimal InvoiceAmount = 0m;

        public string strLnameFindmember = "";
        public int _CompanyID;
        public int _SiteID;
        public DataSet _dSAccessRights;// = new DataSet();
        public DataTable dt = new DataTable();
        //MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
        MemberDetailsDTO _memberDetailsDTO = new MemberDetailsDTO();
        //FormUtilities _formUtilities = new FormUtilities();
        //Utilities _utilites = new Utilities();

        public int v;
        private bool reval;
        private int _addMemberSuccess;
        private bool _genderMale;
        private bool _genderFemale;
        private int _programmeGroupId = 0;
        private int _programId;
        private int _invoiceNo;
        private int _addMembershipSuccess;
        private int _addInvoiceDetailsSuccess;

        int ShowMembership_MemberId = 0;
        public bool _isMemberEdit = false; bool _IsImageEdited = false; bool _AlreadyPhotoExists = false;
        public bool _IsCameraStarted = false; bool _IsCaptured = false; public bool _IsWebCamConnected = false;
        private int _memberID = 0;
        
        private void AddMember_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                btnFamilyMembership.Visibility = Visibility.Collapsed;

                cameras = new List<WebCameraId>(_webCamMS.GetVideoCaptureDevices());
                if (cameras.Count <= 0) { _IsWebCamConnected = false; } else { _IsWebCamConnected = true; }

                SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
                clearData();
                populateData();
                HideControlsOnLoad();
                DisableControlsOnLoad();
                txtFirstName.Focus();
                _datCONTENT = "";

                btnToggle.IsEnabled = false;
                _technoGymPermanentToken = "";

                rowTechnoGymToken.Height = new GridLength(0);

                if (GetMemberDetailsMemberId != 0)
                {
                    //btnDeactivateCard.Visibility = Visibility.Collapsed;

                    btnDeactivateCard.Visibility = Visibility.Visible;
                    btnDeactivateCard.IsEnabled = true;
                    rctDCard.Fill = Brushes.Gold;
                    txtCardNo.IsEnabled = false;

                    btnZoomCamera.Visibility = Visibility.Visible;
                    _isMemberEdit = true;
                    btnNext.Visibility = Visibility.Hidden;
                    LoadExistingMemberDataByMemberId();
                    if ((dt == null))
                    {
                        MemberDetailsBAL _memberBAL = new MemberDetailsBAL();
                        DataSet ds = _memberBAL.get_MembershipOnExistingMember(GetMemberDetailsMemberId);
                        if (ds != null)
                        {
                            if (ds.Tables[0].DefaultView.Count > 0)
                            {
                                dt = ds.Tables[0];
                                dt.DefaultView.RowFilter = "State <> 'EXPIRED'";
                                if (dt.DefaultView.Count > 0)
                                {
                                    _CanOpenBillingBankNewMember = true;
                                    dt.DefaultView.RowFilter = "";
                                }
                                else
                                {
                                    _CanOpenBillingBankNewMember = false;
                                    dt.DefaultView.RowFilter = "";
                                }
                            }
                        }
                    }
                    else
                    {
                        if ((dt.DataSet == null) || (dt.Rows.Count <= 0))
                        {
                            MemberDetailsBAL _memberBAL = new MemberDetailsBAL();
                            DataSet ds = _memberBAL.get_MembershipOnExistingMember(GetMemberDetailsMemberId);
                            if (ds != null)
                            {
                                if (ds.Tables[0].DefaultView.Count > 0)
                                {
                                    dt = ds.Tables[0];
                                    dt.DefaultView.RowFilter = "State <> 'EXPIRED'";
                                    if (dt.DefaultView.Count > 0)
                                    {
                                        _CanOpenBillingBankNewMember = true;
                                        dt.DefaultView.RowFilter = "";
                                    }
                                    else
                                    {
                                        _CanOpenBillingBankNewMember = false;
                                        dt.DefaultView.RowFilter = "";
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    btnDeactivateCard.Visibility = Visibility.Collapsed;
                    btnDeactivateCard.IsEnabled = false;
                    rctDCard.Fill = Brushes.LightGreen;
                    txtCardNo.IsEnabled = true;
                    labelTechnoGymAPI.Content = "Add to TechnoGym DB?";

                    btnToggle.IsEnabled = true;
                    btnToggle.Background = Brushes.Red;

                    btnShowPermanentToken.Visibility = Visibility.Hidden;
                    btnShowPermanentToken.IsEnabled = false;
                    txtPermanentToken.Visibility = Visibility.Hidden;
                    rowTechnoGymToken.Height = new GridLength(0);
                }

            }
            catch (Exception ex)
            {
                if (!ex.Message.ToString().ToUpper().Contains("(DLL)"))
                {
                    System.Windows.MessageBox.Show("Member Form Load Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }

        public void populateData()
        {
            try
            {
                Utilities _utilites = new Utilities();
                _utilites.populatePersonalTrainer(cmbPersonalTrainer);
                _utilites.populateInvolvementType(cmbInvolvementType);

                if (GetMemberDetailsMemberId == 0)
                {
                    GetMemberNo(); //NEW MEMBER NUMBER
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString(), this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void txtLastName_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                int addedLenght = e.Changes.ElementAt(0).AddedLength;
                int removedLenght = e.Changes.ElementAt(0).Offset;
                if (addedLenght > 0)
                {
                    btnSaveMemberandMembership.IsEnabled = true;
                }
                else if (removedLenght <= 0)
                {
                    btnSaveMemberandMembership.IsEnabled = false;
                }
            }
            catch (Exception)
            {
                System.Windows.MessageBox.Show("Error! Last Name Field!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private bool validateData()
        {
            FormUtilities _formUtilities = new FormUtilities();
            reval = true;

           
            if (string.IsNullOrEmpty(txtFirstName.Text.ToString()))
            {
                System.Windows.MessageBox.Show("Please Enter First Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtFirstName.Focus();
                reval = false;
                return reval;
            }
            else
            {
                if (txtFirstName.Text.All(c => Char.IsNumber(c)))
                {
                    System.Windows.MessageBox.Show("First Name Cannot be Numeric!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtFirstName.Focus();
                    reval = false;
                    txtFirstName.Text = string.Empty;
                    return reval;
                }
            }

            if (string.IsNullOrEmpty(txtLastName.Text.ToString()))
            {
                System.Windows.MessageBox.Show("Please Enter Last Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtLastName.Focus();
                reval = false;
                return reval;
            }
            else
            {
                if (txtLastName.Text.All(c => Char.IsNumber(c)))
                {
                    System.Windows.MessageBox.Show("Last Name Cannot be Numeric!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtLastName.Focus();
                    reval = false;
                    txtLastName.Text = string.Empty;
                    return reval;
                }
            }
            if (string.IsNullOrEmpty(txtHomePhone.Text.ToString()))
            {
                //System.Windows.MessageBox.Show("Please Enter Home Phone!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //txtHomePhone.Focus();
                //reval = false;
                //return reval;
            }
            else
            {
                if (!txtHomePhone.Text.All(c => Char.IsNumber(c)))
                {
                    System.Windows.MessageBox.Show("Please Enter Home Phone in Numbers!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtHomePhone.Focus();
                    reval = false;
                    txtHomePhone.Text = string.Empty;
                    return reval;
                }
            }

            if (string.IsNullOrEmpty(txtWorkPhone.Text.ToString()))
            {
                //System.Windows.MessageBox.Show("Please Enter Work Phone!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //txtWorkPhone.Focus();
                //reval = false;
                //return reval;
            }
            else
            {
                if (!txtWorkPhone.Text.All(c => Char.IsNumber(c)))
                {
                    System.Windows.MessageBox.Show("Please Enter Work Phone in Numbers!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtWorkPhone.Focus();
                    reval = false;
                    txtWorkPhone.Text = string.Empty;
                    return reval;
                }
            }

            if (string.IsNullOrEmpty(txtCellPhone.Text.ToString()))
            {
                System.Windows.MessageBox.Show("Please Enter Mobile Phone!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtCellPhone.Focus();
                reval = false;
                return reval;
            }

            if (!txtCellPhone.Text.All(c => Char.IsNumber(c)))
            {
                System.Windows.MessageBox.Show("Please Enter Mobile in Numbers!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtCellPhone.Focus();
                reval = false;
                txtCellPhone.Text = string.Empty;
                return reval;
            }

            if (string.IsNullOrEmpty(txtStreet.Text.ToString()))
            {
                System.Windows.MessageBox.Show("Please Enter Street Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtStreet.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtSubrb.Text.ToString()))
            {
                System.Windows.MessageBox.Show("Please Enter Suburb!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtSubrb.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtPostalCode.Text.ToString()))
            {
                System.Windows.MessageBox.Show("Please Enter PostCode!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtPostalCode.Focus();
                reval = false;
                return reval;
            }

            if (!txtPostalCode.Text.All(c => Char.IsNumber(c)))
            {
                System.Windows.MessageBox.Show("Please Enter PostCode in Numbers!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtPostalCode.Focus();
                reval = false;
                txtPostalCode.Text = string.Empty;
                return reval;
            }

            if (string.IsNullOrEmpty(txtCity.Text.ToString()))
            {
                System.Windows.MessageBox.Show("Please Enter State Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtCity.Focus();
                reval = false;
                return reval;
            }

            if (txtCity.Text.All(c => Char.IsNumber(c)))
            {
                System.Windows.MessageBox.Show("State Code CANNOT be Numeric!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtCity.Focus();
                reval = false;
                txtCity.Text = string.Empty;
                return reval;
            }

            if (string.IsNullOrEmpty(txtDateofBirth.Text.ToString()))
            {
                System.Windows.MessageBox.Show("Please Enter Date of Birth!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtDateofBirth.Focus();
                reval = false;
                return reval;
            }

            if (_formUtilities.CheckDate(txtDateofBirth.Text) == false)
            {
                System.Windows.MessageBox.Show("Please Enter Date of Birth in dd/mm/yyyy format!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtDateofBirth.Focus();
                reval = false;
                return reval;
            }

            DateTime dt = DateTime.Parse(txtDateofBirth.Text);
            if (dt > DateTime.Now.Date)
            {
                System.Windows.MessageBox.Show("Date of Birth Cannot be Future Date!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtDateofBirth.Focus();
                reval = false;
                return reval;
            }

            //if (string.IsNullOrEmpty(txtOccupation.Text.ToString()))
            //{
            //    System.Windows.MessageBox.Show("Please Enter Occuption!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //    txtOccupation.Focus();
            //    reval = false;
            //    return reval;
            //}

            //if (string.IsNullOrEmpty(txtOrganisation.Text.ToString()))
            //{
            //    System.Windows.MessageBox.Show("Please Enter Organisation!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //    txtOrganisation.Focus();
            //    reval = false;
            //    return reval;
            //}

            if (string.IsNullOrEmpty(txtEmail.Text.ToString()))
            {
                System.Windows.MessageBox.Show("Please Enter E-Mail ID!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtEmail.Focus();
                reval = false;
                return reval;
            }

            if (cmbInvolvementType.SelectedIndex == -1)
            {
                System.Windows.MessageBox.Show("Please Select Membership Type!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                cmbInvolvementType.Focus();
                reval = false;
                return reval;
            }

            if (cmbPersonalTrainer.SelectedIndex == -1)
            {
                System.Windows.MessageBox.Show("Please Select Personal Trainer!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                cmbPersonalTrainer.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtNotes.Text.ToString()))
            {
                //System.Windows.MessageBox.Show("Please Enter I.D.!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //txtNotes.Focus();
                //reval = false;
                //return reval;
            }

            if (string.IsNullOrEmpty(txtCardNo.Text.ToString()))
            {
                //System.Windows.MessageBox.Show("Please Enter  Card No!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //txtCardNo.Focus();
                //reval = false;
                //return reval;
                txtCardNo.Text = "0";
            }
            else
            {
                if (!txtCardNo.Text.All(c => Char.IsNumber(c)))
                {
                    System.Windows.MessageBox.Show("Please Enter Card No in Numbers!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtCardNo.Focus();
                    reval = false;
                    txtCardNo.Text = string.Empty;
                    return reval;
                }
            }

            //CHECK FOR DUPLICATE CARD NUMBER
            if (txtCardNo.Text.Trim() != "0")
            {
                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                if (CheckIfDuplicateCardNo(_CompanyID, _SiteID, txtCardNo.Text.Trim(), txtMemberNumber.Text.Trim())) //RETURNS TRUE, IF DUPLICATE
                {
                    System.Windows.MessageBox.Show("Duplicate OR Invalid Card Number!\nPlease enter a different Card Number!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtCardNo.Focus();
                    reval = false;
                    txtCardNo.Text = string.Empty;
                    return reval;
                }
            }
            //CHECK FOR DUPLICATE CARD NUMBER
            return true;
        }
        public void clearData()
        {
            clearMemberData();
            //clearMemberShipData();
            ClearMemberExtraDetails();
        }
        #region ClearData
        public void clearMemberData()
        {
            txtLastName.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtDateofBirth.Text = string.Empty;
            rbMale.IsChecked = true;
            txtStreet.Text = string.Empty;
            txtSubrb.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtPostalCode.Text = string.Empty;
            txtHomePhone.Text = string.Empty;
            txtCellPhone.Text = string.Empty;
            txtNotes.Text = string.Empty;
            txtCardNo.Text = string.Empty;
            txtWorkPhone.Text = string.Empty;
            txtOccupation.Text = string.Empty;
            txtOrganisation.Text = string.Empty;
            txtEmail.Text = string.Empty;
        }
        //public void clearMemberShipData()
        //{
        //    txtMembershipStartDate.Text = string.Empty;
        //    txtMembershipEndDate.Text = string.Empty;
        //    txtMembershipProgramPrice.Text = string.Empty;
        //    txtMembershipFullCost.Text = string.Empty;
        //    txtMembershipProgramCondition.Text = string.Empty;
        //    txtMembershipCardNo.Text = string.Empty;
        //    txtMembershipSignUpFee.Text = string.Empty;
        //    txtMembershipNoOfVisits.Text = string.Empty;

        //    cmbProgramGroup.SelectedIndex = -1;
        //    cmbAccessType.SelectedIndex = -1;
        //}
        public void ClearMemberExtraDetails()
        {
            //txtCardNoLeft.Text = string.Empty;
            //txtOwing.Text = string.Empty;
            Member_Image_Img.Source = new BitmapImage(new Uri(@"..\Images\User_Image.png", UriKind.RelativeOrAbsolute));
            //gvMemberShip.ItemsSource = null;
        }

        private void GetMemberNo()
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();

                DataSet ds = _memberDetailsBal.get_LastUsedNoByCompanySite(_AutoGenerateMemberNo, _CompanyID, _SiteID);
                if ((ds != null))
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            txtMemberNumber.Text = ds.Tables[0].Rows[0]["LastUsedNo"].ToString().Trim();
                        }
                        else
                        {
                            System.Windows.MessageBox.Show("No Data! Member Number - AutoGenerated Number need to be Updated!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            //this.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void LoadExistingMemberDataByMemberId()
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                DataSet MemberDs = new DataSet();
                DataTable MemberDt = new DataTable();

                if (GetMemberDetailsMemberId != 0)
                {
                    _memberID = GetMemberDetailsMemberId;
                    ExistingMember = true;
                }

                MemberDs = _memberDetailsBal.get_AllMemberDetailsAndAlsoByMemberId(_memberID);
                MemberDt = MemberDs.Tables[0];
                if (MemberDt.Rows.Count > 0)
                {
                    DataRow dr = MemberDt.Rows[0];

                    //ShowMembership_MemberId = Convert.ToInt32(dr["MemberNo"].ToString());
                    _memberUniqueID = Convert.ToInt32(dr["MemberID"].ToString());
                    txtMemberNumber.Text = dr["MemberNo"].ToString();
                    txtCardNo.Text = dr["CardNumber"].ToString(); //txtCardNo.IsReadOnly = true;

                    if (dr["Owing"].ToString().Trim() != "")
                    {
                        if (Convert.ToDecimal(dr["Owing"].ToString()) > 0m)
                        {
                            txtOwing.Foreground = System.Windows.Media.Brushes.Red;
                        }
                    }

                    txtOwing.Text = dr["Owing"].ToString();
                    txtLastName.Text = dr["LastName"].ToString();
                    _OldLastName = dr["LastName"].ToString();

                    txtFirstName.Text = dr["FirstName"].ToString();
                    _OldFirstName = dr["FirstName"].ToString();

                    txtDateofBirth.Text = dr["DateofBirth"].ToString();
                    if (dr["Gender"].ToString() == "1")
                    {
                        rbMale.IsChecked = true;
                    }
                    else
                    {
                        rbFemale.IsChecked = true;
                    }
                    txtStreet.Text = dr["Street"].ToString();
                    txtSubrb.Text = dr["suburb"].ToString();
                    //txtCity.Text = dr["city"].ToString();
                    txtCity.Text = dr["State"].ToString();
                    txtPostalCode.Text = dr["PostalCode"].ToString();
                    txtHomePhone.Text = dr["HomePhone"].ToString();
                    txtCellPhone.Text = dr["MobilePhone"].ToString();
                    txtNotes.Text = dr["Notes"].ToString();
                    txtCardNoLeft.Text = dr["CardNumber"].ToString();
                    txtWorkPhone.Text = dr["Workphone"].ToString();
                    txtOccupation.Text = dr["Occupation"].ToString();
                    txtOrganisation.Text = dr["Organisation"].ToString();

                    for (int i = 0; i < cmbInvolvementType.Items.Count; i++)
                    {
                        cmbInvolvementType.SelectedIndex = i;
                        if (Convert.ToInt32(cmbInvolvementType.SelectedValue.ToString()) == Convert.ToInt32(dr["InvolvementType"].ToString()))
                        {
                            cmbInvolvementType.SelectedIndex = i;
                            break;
                        }
                        cmbInvolvementType.SelectedIndex = -1;
                    }

                    for (int i = 0; i < cmbPersonalTrainer.Items.Count; i++)
                    {
                        cmbPersonalTrainer.SelectedIndex = i;
                        if (cmbPersonalTrainer.SelectedValue != null)
                        {
                            if (Convert.ToInt32(cmbPersonalTrainer.SelectedValue.ToString()) == Convert.ToInt32(dr["PersonalTrainer"].ToString()))
                            {
                                cmbPersonalTrainer.SelectedIndex = i;
                                break;
                            }
                        }
                        cmbPersonalTrainer.SelectedIndex = -1;
                    }

                    txtEmail.Text = dr["EmailAddress"].ToString();

                    //08/Apr/2016
                    string fileName = dr["MemberProfilePhotoPath"].ToString();
                    if (fileName.Trim() != "")
                    {
                        //Member_Image_Img.Source = new BitmapImage(new Uri(fileName, UriKind.RelativeOrAbsolute));
                    }
                    else
                    {
                        //fileName = @"..\Images\User_Image.png";
                        //Member_Image_Img.Source = new BitmapImage(new Uri(fileName, UriKind.RelativeOrAbsolute));
                    }

                    if (MemberDs.Tables[0].Rows[0]["MemberPic"] != DBNull.Value)
                    {
                        byte[] data = (byte[])MemberDs.Tables[0].Rows[0]["MemberPic"];
                        if (data.Length > 0)
                        {
                            MemoryStream strm = new MemoryStream();
                            strm.Write(data, 0, data.Length);
                            strm.Position = 0;

                            System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                            BitmapImage bi = new BitmapImage();
                            bi.BeginInit();
                            MemoryStream ms = new MemoryStream();
                            img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                            ms.Seek(0, SeekOrigin.Begin);
                            bi.StreamSource = ms;
                            bi.EndInit();

                            Member_Image_Img.Source = bi;
                            _memberDetailsDTO.MemberPic = (byte[])MemberDs.Tables[0].Rows[0]["MemberPic"];
                            _memberDetailsDTO.MemberProfilePhotoPath = MemberDs.Tables[0].Rows[0]["MemberProfilePhotoPath"].ToString();
                            _AlreadyPhotoExists = true;
                        }
                    }
                    //08/Apr/2016

                    //Disable & Visible false for Buttons not relevant in Existing Member
                    if (_isMemberEdit)
                    {
                        btnSaveMemberandMembership.IsEnabled = true;
                        btnSaveMemberandMembership.Visibility = Visibility.Visible;
                        btnSaveMemberandMembership.Content = "Update";// Member Details";
                                                                      //btnSaveMemberandMembership.Margin = new Thickness(305, 0, 0, 215); //Margin="305,0,0,201"
                    }
                    else
                    {
                        btnSaveMemberandMembership.IsEnabled = false;
                        btnSaveMemberandMembership.Visibility = Visibility.Hidden;
                        btnSaveMemberandMembership.Content = "Save";// Member and Add Membership";
                                                                    //btnSaveMemberandMembership.Margin = new Thickness(483, 0, 0, 215); //Margin="483,0,0,201"
                    }

                    if (MemberDs.Tables[0].Rows[0]["TechnoGymGUID"].ToString().Trim() != "")
                    {
                        btnToggle.IsChecked = true;
                        btnToggle.IsEnabled = false;
                        
                        //labelTechnoGymAPI.FontSize = 11;
                        //labelTechnoGymAPI.Content = "TechnoGym GUID: < " + MemberDs.Tables[0].Rows[0]["TechnoGymGUID"].ToString().Trim() + " >";

                        labelTechnoGymAPI.Content = "TechnoGym Permanent Token created? : YES";
                        txtPermanentToken.Text = MemberDs.Tables[0].Rows[0]["TechnoGymGUID"].ToString().Trim();
                        btnToggle.Background = Brushes.Orange;
                        btnShowPermanentToken.Visibility = Visibility.Visible;
                        btnShowPermanentToken.IsEnabled = true;
                        txtPermanentToken.Visibility = Visibility.Hidden;
                        rowTechnoGymToken.Height = new GridLength(15, GridUnitType.Star);

                        _technoGymPermanentToken = txtPermanentToken.Text; //PERMANENT TOKEN FOR THE MEMBER
                    }
                    else
                    {
                        btnToggle.IsEnabled = true;
                        btnToggle.IsChecked = false;
                        btnToggle.Background = Brushes.Red;
                        labelTechnoGymAPI.Content = "Add to TechnoGym DB ?";

                        btnShowPermanentToken.Visibility = Visibility.Hidden;
                        btnShowPermanentToken.IsEnabled = false;
                        rowTechnoGymToken.Height = new GridLength(0);
                        _technoGymPermanentToken = "";
                    }
                }
                else
                {
                    System.Windows.MessageBox.Show("No Data Found!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }//try
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error! " + ex.Message + " Please Contact System Administrator!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void HideControlsOnLoad()
        {
            ////has to be hide
            //btnAddMembership.Visibility = Visibility.Hidden;

            //btnMembershipEdit.Visibility = Visibility.Hidden;
            //btnMembershipExpire.Visibility = Visibility.Hidden;
            //btnMembershipHold.Visibility = Visibility.Hidden;
            //btnAddPayment.Visibility = Visibility.Hidden;
            //// has to be hide
            //btnSaveAndComplete.Visibility = Visibility.Hidden;
            btnZoomCamera.Visibility = Visibility.Hidden;
        }

        //private void ShowControlsOnAddMemberClick()
        //{
        //    btnAddMembership.Visibility = Visibility.Visible;
        //    btnMembershipEdit.Visibility = Visibility.Visible;
        //    btnMembershipExpire.Visibility = Visibility.Visible;
        //    btnMembershipHold.Visibility = Visibility.Visible;
        //    btnAddPayment.Visibility = Visibility.Visible;

        //    if (_isMemberEdit)
        //    {
        //        btnSaveMemberandMembership.IsEnabled = true;
        //        btnSaveMemberandMembership.Visibility = Visibility.Visible;
        //        btnSaveMemberandMembership.Content = "Update Member Details";
        //        btnSaveMemberandMembership.Margin = new Thickness(305, 0, 0, 215); //Margin="305,0,0,201"
        //    }
        //    else
        //    {
        //        btnSaveMemberandMembership.IsEnabled = false;
        //        btnSaveMemberandMembership.Visibility = Visibility.Hidden;
        //        btnSaveMemberandMembership.Content = "Save Member and Add Membership";
        //        btnSaveMemberandMembership.Margin = new Thickness(483, 0, 0, 215); //Margin="483,0,0,201"
        //    }

        //    btnSaveAndComplete.Visibility = Visibility.Visible;
        //    lblMemberShipMsg.Visibility = Visibility.Hidden;

        //    cmbProgramGroup.IsEnabled = true;
        //    cmbProgram.IsEnabled = true;
        //    btnAddMembership.IsEnabled = false;
        //    btnMembershipEdit.IsEnabled = false;
        //    btnMembershipExpire.IsEnabled = false;
        //    btnMembershipHold.IsEnabled = false;
        //    btnAddPayment.IsEnabled = false;
        //    // has to be enabled
        //    //btnSaveAndComplete.IsEnabled = false;
        //    btnSaveAndComplete.IsEnabled = false;

        //}
        private void DisableControlsOnLoad()
        {
            btnSaveMemberandMembership.IsEnabled = false;
            //cmbProgramGroup.IsEnabled = false;
            //cmbProgram.IsEnabled = false;
        }
        #endregion

        private void btnSaveMemberandMembership_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (validateData())
                {
                    MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();

                    _memberDetailsDTO.LastName = txtLastName.Text.ToString();
                    _memberDetailsDTO.FirstName = txtFirstName.Text.ToString();
                    _memberDetailsDTO.DateofBirth = Convert.ToDateTime(txtDateofBirth.Text.ToString());

                    _genderMale = Convert.ToBoolean(rbMale.IsChecked.ToString());
                    _genderFemale = Convert.ToBoolean(rbFemale.IsChecked.ToString());

                    if (_genderMale == true)
                    {
                        _memberDetailsDTO.Gender = 1;
                    }
                    else
                    {
                        _memberDetailsDTO.Gender = 2;
                    }
                    _memberDetailsDTO.Street = txtStreet.Text.ToString().Trim();
                    _memberDetailsDTO.Suburb = txtSubrb.Text.ToString().Trim();
                    _memberDetailsDTO.City = txtCity.Text.ToString().Trim();
                    _memberDetailsDTO.PostalCode = Convert.ToInt32(txtPostalCode.Text.ToString().Trim());
                    _memberDetailsDTO.HomePhone = txtHomePhone.Text.ToString().Trim();
                    _memberDetailsDTO.MobilePhone = txtCellPhone.Text.ToString().Trim();
                    _memberDetailsDTO.Notes = txtNotes.Text.ToString().Trim();
                    _memberDetailsDTO.CardNo = Convert.ToInt64(txtCardNo.Text.ToString().Trim());
                    _memberDetailsDTO.WorkPhone = txtWorkPhone.Text.ToString().Trim();
                    _memberDetailsDTO.Occupation = txtOccupation.Text.ToString().Trim();
                    _memberDetailsDTO.Organisation = txtOrganisation.Text.ToString().Trim();
                    _memberDetailsDTO.InvolvementType = cmbInvolvementType.SelectedValue.ToString();
                    _memberDetailsDTO.PersonalTrainer = cmbPersonalTrainer.SelectedValue.ToString();
                    _memberDetailsDTO.EmailId = txtEmail.Text.ToString().Trim();
                    _memberDetailsDTO.appVersion = Constants._appVersion;

                    if (!_isMemberEdit)
                    {
                        GetMemberNo(); //GET THE LATEST MEMBER NO.
                    }
                    _memberDetailsDTO.MemberId = Convert.ToInt32(txtMemberNumber.Text.ToString().Trim());

                    //RAMJI 24/MAR/2016
                    _memberDetailsDTO.CompanyID = _CompanyID;
                    //RAMJI 24/MAR/2016

                    if (((fileNamePath_MemberPhoto == null) || (fileNamePath_MemberPhoto == "")) && (!_isMemberEdit))
                    {
                        _memberDetailsDTO.MemberProfilePhotoPath = "";
                        if (!_isMemberEdit)
                        {
                            if (!_IsImageEdited)
                            {
                                if (!_AlreadyPhotoExists)
                                {
                                    byte[] data = new byte[0];
                                    _memberDetailsDTO.MemberPic = data;
                                }
                                _memberDetailsDTO.MemberProfilePhotoPath = "";
                            }
                        }
                    }
                    else
                    {
                        //Save pic TO DB
                        _memberDetailsDTO.MemberProfilePhotoPath = fileNamePath_MemberPhoto;
                        if (_IsImageEdited)
                        {
                            FileStream fs = new FileStream(_memberDetailsDTO.MemberProfilePhotoPath, FileMode.Open, FileAccess.Read);
                            byte[] data = new byte[fs.Length];
                            fs.Read(data, 0, System.Convert.ToInt32(fs.Length));
                            fs.Close();
                            _memberDetailsDTO.MemberPic = data;
                        }
                        else
                        {
                            if (!_AlreadyPhotoExists)
                            {
                                byte[] data = new byte[0];
                                _memberDetailsDTO.MemberPic = data;
                            }
                            _memberDetailsDTO.MemberProfilePhotoPath = "";
                        }
                    }

                    if (_isCardDenyCreated)
                    {
                        _memberDetailsDTO.cardReplicate = 1;
                    }
                    else
                    {
                        _memberDetailsDTO.cardReplicate = 0;
                    }

                    //TECHNOGYM ADD MEMBER
                    _technoGymMsg = "";
                    if (_technoGymToggle == true && _technoGymPermanentToken == "")
                    {
                        _technoGymMsg = "";
                        addMemberToTechnoGymDB();
                    }
                    else if (_technoGymToggle == true && _technoGymPermanentToken != "")
                    {
                        _technoGymMsg = "";
                        updateMemberToTechnoGymDB();
                    }

                    if (txtPermanentToken.Text != "")
                        _technoGymPermanentToken = txtPermanentToken.Text;

                    if (_technoGymPermanentToken.Length > 34)
                    {
                        _memberDetailsDTO.technoGymPermanentToken = _technoGymPermanentToken;
                        if (ConfigurationManager.AppSettings["TechnoGym_Test_OR_Prod"].ToString().ToUpper() == "TEST")
                        {
                            _memberDetailsDTO.technoGymEnv = "TEST";
                        }
                        else
                        {
                            _memberDetailsDTO.technoGymEnv = "PROD";
                        }
                    }
                    else
                    {
                        _memberDetailsDTO.technoGymPermanentToken = "";
                        _memberDetailsDTO.technoGymEnv = "";
                    }

                    labelTechnoGymAPI.FontSize = 11;
                    if (_technoGymPermanentToken.Trim() != "")
                    {
                        labelTechnoGymAPI.FontSize = 11;

                        //labelTechnoGymAPI.Content = "TechnoGym GUID: < " + _technoGymGUID + " >";

                        labelTechnoGymAPI.Content = "TechnoGym Permanent Token created? : YES";
                        txtPermanentToken.Text = _technoGymPermanentToken;
                        btnToggle.IsEnabled = false;
                        btnShowPermanentToken.Visibility = Visibility.Visible;
                        btnShowPermanentToken.IsEnabled = true;
                        rowTechnoGymToken.Height = new GridLength(15, GridUnitType.Star);

                        if (rctPermanentToken.Fill == Brushes.LawnGreen)
                            txtPermanentToken.Visibility = Visibility.Hidden;
                        else
                            txtPermanentToken.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        btnShowPermanentToken.Visibility = Visibility.Hidden;
                        btnShowPermanentToken.IsEnabled = false;
                        txtPermanentToken.Visibility = Visibility.Hidden;
                    }
                    //TECHNOGYM ADD MEMBER

                    if (_isMemberEdit)
                    {
                        //Call UPDATE procedure
                        _addMemberSuccess = _memberDetailsBal.add_MemberDetails(_memberDetailsDTO, 1);
                    }
                    else
                    {
                        _addMemberSuccess = _memberDetailsBal.add_MemberDetails(_memberDetailsDTO, 0);
                    }

                    if (_addMemberSuccess != 0)
                    {
                        if (!_isMemberEdit)
                        {
                            _CanOpenMemberShipNewMember = true;
                            GetMemberDetailsMemberId = _memberDetailsDTO.MemberId;
                            ExistingMember = true;
                            btnNext.IsEnabled = true;
                            _isFromMemberDetailsNewMember = true;
                            DisableControlsOnLoad();
                            Constants.Create_ACCESS_DAT_File(false, GetMemberDetailsMemberId);

                            System.Windows.MessageBox.Show("\nNew Member Added Successfully! (in REDA)\n\n" + _technoGymMsg, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            if (_datCONTENT.Trim() != "")
                            {
                                if (_OldCardNo.Trim().ToUpper() != txtCardNo.Text.Trim().ToUpper())
                                {
                                    Constants.Create_ACCESS_DAT_File(false, GetMemberDetailsMemberId, "", _datCONTENT);
                                }
                                else
                                {
                                    Constants.Create_ACCESS_DAT_File(false, GetMemberDetailsMemberId);
                                }
                                _OldCardNo = txtCardNo.Text.Trim();
                                _datCONTENT = "";
                            }
                            else
                            {
                                Constants.Create_ACCESS_DAT_File(false, GetMemberDetailsMemberId);
                            }
                            //IF MEMBER FIRSTNAME OR LAST NAME CHANGED; CHECK & UPDATE PAYCHOICE BUSINESS NAME; & MEMBER NAME
                            //RR:29-JUL-2016
                            if ((_OldFirstName.Trim().ToUpper() != _memberDetailsDTO.FirstName.Trim().ToUpper()) || (_OldLastName.Trim().ToUpper() != _memberDetailsDTO.LastName.Trim().ToUpper()))
                            {
                                //CHECK IF PAYCHOICE ENTRY CREATED & UPDATE NAME
                                Hashtable hteditcustomer = new Hashtable();
                                string _customerGUID = "";
                                hteditcustomer.Add("@appmemberid", _memberUniqueID);
                                SqlHelper _sql = new SqlHelper();
                                if (_sql._ConnOpen == 0) return;

                                DataSet _ds = _sql.ExecuteProcedure("Validate_Paychoice_Customer", hteditcustomer);

                                if (_ds != null)
                                {
                                    if (_ds.Tables.Count > 0)
                                    {
                                        if (_ds.Tables[0].DefaultView.Count > 0)
                                        {
                                            _customerGUID = _ds.Tables[0].DefaultView[0]["Customer_GUID"].ToString();
                                            if (_customerGUID.Trim() != "")
                                            {
                                                hteditcustomer.Clear();
                                                hteditcustomer.Add("business_name", _memberDetailsDTO.FirstName + " " + _memberDetailsDTO.LastName);
                                                hteditcustomer.Add("account_number", "C" + _CompanyID.ToString("0000") + "-S" + _SiteID.ToString("0000") + "-M" + GetMemberDetailsMemberId.ToString("00000"));
                                                hteditcustomer.Add("first_name", _memberDetailsDTO.FirstName);
                                                hteditcustomer.Add("last_name", _memberDetailsDTO.LastName);

                                                hteditcustomer.Add("email", "");
                                                hteditcustomer.Add("phone", "");
                                                hteditcustomer.Add("mobile", "");
                                                hteditcustomer.Add("address_line1", "");
                                                hteditcustomer.Add("address_line2", "");
                                                hteditcustomer.Add("address_suburb", "");
                                                hteditcustomer.Add("address_state", "");
                                                hteditcustomer.Add("address_postcode", "");
                                                hteditcustomer.Add("address_country", "");

                                                Paychoice_Parameter parameter = new Paychoice_Parameter();
                                                string _result = parameter.Edit_Existing_Customer(_customerGUID, hteditcustomer);
                                                if (_result.ToUpper() == "SUCCESS")
                                                {
                                                    hteditcustomer.Clear();
                                                    hteditcustomer.Add("@CustomerGUID", _customerGUID);
                                                    hteditcustomer.Add("@FirstName", _memberDetailsDTO.FirstName);
                                                    hteditcustomer.Add("@LastName", _memberDetailsDTO.LastName);
                                                    _sql.ExecuteProcedure("Update_Paychoice_Payment_Mode_Name", hteditcustomer);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //RR:29-JUL-2016
                            //IF MEMBER FIRSTNAME OR LAST NAME CHANGED; CHECK & UPDATE PAYCHOICE BUSINESS NAME; & MEMBER NAME

                            txtCardNo.IsEnabled = false;
                            btnDeactivateCard.IsEnabled = true;
                            rctDCard.Fill = Brushes.Gold;
                            _OldCardNo = txtCardNo.Text;

                            System.Windows.MessageBox.Show("Member Details Updated Successfully! (in REDA)\n\n" + _technoGymMsg, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Error Adding / Updating Member Details! (in REDA)\n\n" + _technoGymMsg, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        public void MemberDetails_AccessRights(DataSet dS)
        {
            _dSAccessRights = new DataSet();
            _dSAccessRights = dS;
        }

        public string showDetailsFindMember(string LName)
        {
            strLnameFindmember = LName;
            return strLnameFindmember;
        }
        private void Change_Member_Photo(object sender, MouseButtonEventArgs e)
        {
            try
            {
                OpenFileDialog commonOpenFileDialog = new OpenFileDialog();
                commonOpenFileDialog.Filter = "Image Files|*.jpeg;*.jpg;*.png;*.bmp;*.tif;*.gif;|JPEG Files|*.jpg;*.jpeg|PNG Files|*.png|BMP Files|*.bmp|TIF Files|*.tif|GIF Files|*.gif";
                commonOpenFileDialog.Title = "Select Member Profile PHOTO...";
                commonOpenFileDialog.Multiselect = false;
                commonOpenFileDialog.ShowDialog();

                fileNamePath_MemberPhoto = commonOpenFileDialog.FileName;
                if (fileNamePath_MemberPhoto != "")
                {
                    FileStream fs = new FileStream(fileNamePath_MemberPhoto, FileMode.Open, FileAccess.Read);
                    byte[] data = new byte[fs.Length];
                    if (Convert.ToInt32(fs.Length) > 250000)
                    {
                        System.Windows.MessageBox.Show("FILE SIZE EXCEEDS ALLOWED LIMIT! \nPlease Select a different Photo to Upload!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                    fs.Read(data, 0, System.Convert.ToInt32(fs.Length));
                    fs.Close();
                    _memberDetailsDTO.MemberPic = data;

                    Member_Image_Img.Source = new BitmapImage(new Uri(fileNamePath_MemberPhoto, UriKind.RelativeOrAbsolute));
                    _memberDetailsDTO.MemberProfilePhotoPath = fileNamePath_MemberPhoto;
                    _IsImageEdited = true;
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error! NOT a Valid Image File! \nError Details: ( " + ex.Message + " )", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private void btnStartCamera_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!_IsCameraStarted)
                {
                    //_webcam = new webCam();
                    //_webcam.InitializeWebCam(ref Member_Image_Img);
                    //_webcam.Start();
                    //_IsCameraStarted = true;

                    if (!_IsWebCamConnected)
                    {
                        cameras = new List<WebCameraId>(_webCamMS.GetVideoCaptureDevices());
                        if (cameras.Count <= 0) { _IsWebCamConnected = false; } else { _IsWebCamConnected = true; }
                    }

                    if (_IsWebCamConnected)
                    {
                        Member_Image_Img.Visibility = Visibility.Hidden;
                        _webCamMS.Visibility = Visibility.Visible;
                        _IsCameraStarted = true;
                        _webCamMS.StartCapture(cameras[0]);
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("No WebCam Detected!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("WebCam may not be properly Installed! Unable to start WebCam!\n\n(Details: " + ex.Message + " )", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //_webcam = null;
            }
        }

        private void btnCapture_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (_IsCameraStarted)
                //{
                //    Member_Image_Img.Source = Member_Image_Img.Source;
                //    Helper.SaveImageCapture((BitmapSource)Member_Image_Img.Source);
                //    _IsImageEdited = true;
                //    fileNamePath_MemberPhoto = Comman.Constants.AppPath + "\\Member.JPG";
                //    _webcam.Stop();
                //    _webcam = null;
                //    _IsCaptured = true;
                //    _IsCameraStarted = false;
                //}
                if (_IsCameraStarted)
                {
                    Member_Image_Img.Source = Constants._getBitMapImage(_webCamMS.GetCurrentImage());
                    fileNamePath_MemberPhoto = Comman.Constants.AppPath + "\\Member.JPG";
                    Helper.SaveImageCapture((BitmapSource)Member_Image_Img.Source);

                    _IsImageEdited = true;
                    _IsCaptured = true;
                    _IsCameraStarted = false;

                    Member_Image_Img.Visibility = Visibility.Visible;
                    _webCamMS.Visibility = Visibility.Hidden;
                    _webCamMS.StopCapture();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("WebCam may not be properly Installed! Unable to capture Image!\n\n(Details: " + ex.Message + " )", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //_webcam = null;
            }
        }
        
        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            Constants._ShowDashBoard();
            this.Close();
        }
        private bool CheckIfDuplicateCardNo(int _CompanyID, int _SiteID, string _CardNo, string _MemberNo)
        {
            MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
            DataSet ds = _memberDetailsBal.CheckIfDuplicateCardNo(_CompanyID, _SiteID, _CardNo, _MemberNo);
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView.Count > 0)
                {
                    if (ds.Tables[0].DefaultView[0]["Result"].ToString() == "0")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Constants._ShowDashBoard();
            this.Close();
        }

        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            //WITHIN THE SAME FORM; NO ACTION
        }

        private void btnShowMembership_Click(object sender, RoutedEventArgs e)
        {
            if (!ExistingMember)
            {
                if (_CanOpenMemberShipNewMember)
                {
                    Memberships _mDetails = new Memberships();
                    _mDetails.ExistingMember = ExistingMember;
                    _mDetails.CardNumber = CardNumber;
                    CardNumber = Convert.ToInt64(txtCardNo.Text.ToString()); 
                    _mDetails.CardNumber = Convert.ToInt64(txtCardNo.Text.ToString());
                    _mDetails.GetMemberDetailsMemberId = Convert.ToInt32(txtMemberNumber.Text.Trim()); //this.GetMemberDetailsMemberId;
                    _mDetails._MemberName = _MemberName;
                    _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    _mDetails._CanOpenBillingBankNewMember = _CanOpenBillingBankNewMember;
                    _mDetails._isFromMemberDetailsNewMember = _isFromMemberDetailsNewMember;
                    _mDetails._CompanyName = _CompanyName;
                    _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                    _mDetails._SiteName = _SiteName;
                    _mDetails._StaffPic = _StaffPic;
                    _mDetails._UserName = _UserName;
                    _mDetails._CompanyID = _CompanyID;
                    _mDetails._SiteID = _SiteID;
                    _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                    _mDetails.StaffImage.Source = StaffImage.Source;
                    _mDetails._dSAccessRights = _dSAccessRights;
                    _mDetails.dt = this.dt;
                    _mDetails.Owner = this.Owner;

                    this.Close();
                    _mDetails.ShowDialog();
                }
                else
                {
                    System.Windows.MessageBox.Show("New Member Details have to be Created First!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            else
            {
                Memberships _mDetails = new Memberships();
                _mDetails.ExistingMember = ExistingMember;
                _mDetails.CardNumber = CardNumber;
                _mDetails.CardNumber = Convert.ToInt64(txtCardNo.Text.ToString());
                _mDetails.GetMemberDetailsMemberId = Convert.ToInt32(txtMemberNumber.Text.Trim()); //this.GetMemberDetailsMemberId;
                _mDetails._MemberName = _MemberName;
                _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _mDetails._CompanyName = _CompanyName;
                _mDetails._CanOpenBillingBankNewMember = _CanOpenBillingBankNewMember;
                _mDetails._isFromMemberDetailsNewMember = _isFromMemberDetailsNewMember;
                _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                _mDetails._SiteName = _SiteName;
                _mDetails._StaffPic = _StaffPic;
                _mDetails._UserName = _UserName;
                _mDetails._CompanyID = _CompanyID;
                _mDetails._SiteID = _SiteID;
                _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                _mDetails.StaffImage.Source = StaffImage.Source;
                _mDetails._dSAccessRights = _dSAccessRights;
                _mDetails.dt = this.dt;
                _mDetails.Owner = this.Owner;

                this.Close();
                _mDetails.ShowDialog();
            }
        }

        private void btnAccounts_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberAccounts mAccounts = new MemberAccounts();
                mAccounts.MemberID = Convert.ToInt32(txtMemberNumber.Text.Trim());
                mAccounts._CurrentLoginDateTime = _CurrentLoginDateTime;
                mAccounts._CompanyName = _CompanyName;
                mAccounts._SiteName = _SiteName;
                mAccounts._UserName = _UserName;
                mAccounts._StaffPic = _StaffPic;
                mAccounts._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mAccounts.ExistingMember = ExistingMember;
                mAccounts.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mAccounts._MemberName = _MemberName;
                mAccounts.Member_Image_Img.Source = Member_Image_Img.Source;
                mAccounts.StaffImage.Source = StaffImage.Source;
                mAccounts._dSAccessRights = _dSAccessRights;
                mAccounts.dt = this.dt;

                mAccounts.Owner = this.Owner;
                this.Cursor = System.Windows.Input.Cursors.Wait;
                btnAccounts.Cursor = System.Windows.Input.Cursors.Wait;

                this.Close();
                mAccounts.ShowDialog();
            }
        }

        private void btnMemberBillingDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ExistingMember && _CanOpenBillingBankNewMember)
                {
                    this.Cursor = System.Windows.Input.Cursors.Wait;
                    PaymentDetails PD = new PaymentDetails(GetMemberDetailsMemberId, _CompanyID, _SiteID, dt);
                    this.Cursor = System.Windows.Input.Cursors.Arrow;

                    PD._CurrentLoginDateTime = _CurrentLoginDateTime;
                    PD._CompanyName = _CompanyName;
                    PD._SiteName = _SiteName;
                    PD._UserName = _UserName;
                    PD._StaffPic = _StaffPic;
                    PD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    PD._CanOpenBillingBankNewMember = _CanOpenBillingBankNewMember;
                    PD.ExistingMember = ExistingMember;
                    PD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    PD._MemberName = _MemberName;
                    PD.Member_Image_Img.Source = Member_Image_Img.Source;
                    PD.StaffImage.Source = StaffImage.Source;
                    PD._dSAccessRights = _dSAccessRights;
                    PD.dt = this.dt;

                    PD.Owner = this.Owner;
                    this.Close();
                    PD.ShowDialog();
                }
                else
                {
                    System.Windows.MessageBox.Show("Membership has to be Created!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnMemberExtraDetails_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                ExtraDetails extra = new ExtraDetails();
                extra.strExtraDetailsMemberID = GetMemberDetailsMemberId; //Convert.ToInt32(txtMemberNumber.Text.ToString().Trim());

                extra._CurrentLoginDateTime = _CurrentLoginDateTime;
                extra._CompanyName = _CompanyName;
                extra._SiteName = _SiteName;
                extra._UserName = _UserName;
                extra._StaffPic = _StaffPic;
                extra._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                extra.ExistingMember = ExistingMember;
                extra.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                extra._MemberName = _MemberName;
                extra.Member_Image_Img.Source = Member_Image_Img.Source;
                extra.StaffImage.Source = StaffImage.Source;
                extra._dSAccessRights = _dSAccessRights;
                extra.dt = this.dt;

                extra.Owner = this.Owner;
                this.Close();
                extra.ShowDialog();
            }
        }

        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                Tasks Tsk = new Tasks();
                Tsk.strMemberNo = GetMemberDetailsMemberId; //Convert.ToInt32(txtMemberNumber.Text);
                Tsk.Owner = this.Owner;
                Tsk.SetAccessRightsDS(_dSAccessRights);

                Tsk._CurrentLoginDateTime = _CurrentLoginDateTime;
                Tsk._CompanyName = _CompanyName;
                Tsk._SiteName = _SiteName;
                Tsk._UserName = _UserName;
                Tsk._StaffPic = _StaffPic;
                Tsk._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                Tsk.ExistingMember = ExistingMember;
                Tsk.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                Tsk._MemberName = _MemberName;
                Tsk.Member_Image_Img.Source = Member_Image_Img.Source;
                Tsk.StaffImage.Source = StaffImage.Source;
                Tsk._dSAccessRights = _dSAccessRights;
                Tsk.dt = this.dt;

                this.Close();

                Tsk.ShowDialog();
            }
        }

        private void btnCommunications_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Communication _Comm = new Communication();
                _Comm.Owner = this.Owner;
                _Comm.SetAccessRightsDS(_dSAccessRights);
                this.Cursor = System.Windows.Input.Cursors.Arrow;

                _Comm._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Comm._CompanyName = _CompanyName;
                _Comm._SiteName = _SiteName;
                _Comm._UserName = _UserName;
                _Comm._StaffPic = _StaffPic;
                _Comm._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Comm.ExistingMember = ExistingMember;
                _Comm.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Comm._MemberName = _MemberName;
                _Comm.Member_Image_Img.Source = Member_Image_Img.Source;
                _Comm.StaffImage.Source = StaffImage.Source;
                _Comm._dSAccessRights = _dSAccessRights;
                _Comm.dt = this.dt;

                this.Close();

                _Comm.ShowDialog();
            }
        }

        private void btnNotes_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Notes _Notes = new Notes();
                _Notes.Owner = this.Owner;

                _Notes._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Notes._CompanyName = _CompanyName;
                _Notes._SiteName = _SiteName;
                _Notes._UserName = _UserName;
                _Notes._StaffPic = _StaffPic;
                _Notes._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Notes.ExistingMember = ExistingMember;
                _Notes.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Notes._MemberName = _MemberName;
                _Notes.Member_Image_Img.Source = Member_Image_Img.Source;
                _Notes.StaffImage.Source = StaffImage.Source;
                _Notes._dSAccessRights = _dSAccessRights;
                _Notes.dt = this.dt;

                this.Cursor = System.Windows.Input.Cursors.Arrow;
                this.Close();
                _Notes.ShowDialog();
            }
        }

        private void btnPersonalTraining_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                PersonalTraining _pt = new PersonalTraining();
                _pt.Owner = this.Owner;

                _pt._CurrentLoginDateTime = _CurrentLoginDateTime;
                _pt._CompanyName = _CompanyName;
                _pt._SiteName = _SiteName;
                _pt._UserName = _UserName;
                _pt._StaffPic = _StaffPic;
                _pt._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _pt.ExistingMember = ExistingMember;
                _pt.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _pt._MemberName = _MemberName;
                _pt.Member_Image_Img.Source = Member_Image_Img.Source;
                _pt.StaffImage.Source = StaffImage.Source;
                _pt._dSAccessRights = _dSAccessRights;
                _pt.dt = this.dt;

                this.Close();
                _pt.ShowDialog();
            }
        }

        private void btnZoomCamera_Click(object sender, RoutedEventArgs e)
        {
            MemberImageZoom _ImgZoom = new MemberImageZoom();
            _ImgZoom.Title = _MemberName;
            _ImgZoom.Owner = this;
            _ImgZoom.imgMemberZoom.Source = this.Member_Image_Img.Source;
            _ImgZoom.ShowDialog();            
        }

        private void cmbInvolvementType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbInvolvementType.SelectedIndex != -1)
            {
                if ((cmbInvolvementType.SelectedItem as DataRowView).Row["InvolvementTypeName"].ToString().ToUpper().Trim() == "FAMILY MEMBERSHIP")
                {
                    btnFamilyMembership.Visibility = Visibility.Visible; //Visible
                }
                else
                {
                    e.Handled = true;
                    btnFamilyMembership.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void btnDeactivateCard_Click(object sender, RoutedEventArgs e)
        {
            if ((_OldCardNo.Trim() == "0") || (_OldCardNo.Trim() == ""))
            {
                _OldCardNo = txtCardNo.Text.Trim();
            }

            ////CREATE 'DENY' WITH OLD CARD NO.
            if (_OldCardNo.Trim() != "" && _OldCardNo.Trim() != "0")
            {
                if (txtCardNo.Text.Trim() != "")
                {
                    if (txtCardNo.Text.Trim() != "0")
                    {
                        _isCardDenyCreated = true;

                        string _StartDate = DateTime.Now.Date.ToString("ddMMyy");
                        string _EndDate = DateTime.Now.Date.ToString("ddMMyy");
                        string _EndTime = DateTime.Now.ToString("HHmm");

                        dt.DefaultView.RowFilter = "";
                        if (dt.DefaultView.Count > 0)
                        {
                            _StartDate = dt.Rows[0]["StartDate"].ToString();
                            if (_StartDate.Trim() != "")
                            {
                                _StartDate = _StartDate.Replace("/", "");
                                if (_StartDate.Length > 6)
                                {
                                    _StartDate = _StartDate.Substring(0, 4) + _StartDate.Substring(6, 2);
                                }
                            }
                            else
                            {
                                _StartDate = DateTime.Now.Date.ToString("ddMMyy");
                            }
                        }
                        string _delimeter = "|";
                        //P|15718494|C|Mary West|Deny|3|3|260117|0000|060317|2021
                        _datCONTENT = "P" + _delimeter;
                        _datCONTENT += _OldCardNo + _delimeter;
                        _datCONTENT += "C" + _delimeter;
                        _datCONTENT += txtFirstName.Text.ToUpper() + " " + txtLastName.Text.ToUpper() + _delimeter;
                        _datCONTENT += "DENY" + _delimeter;
                        _datCONTENT += txtMemberNumber.Text + _delimeter;
                        _datCONTENT += txtMemberNumber.Text + _delimeter;
                        _datCONTENT += _StartDate + _delimeter;
                        _datCONTENT += "0000" + _delimeter;
                        _datCONTENT += _EndDate + _delimeter;
                        _datCONTENT += _EndTime;

                        //Constants.Create_ACCESS_DENY_File(_datCONTENT);
                        //_datCONTENT = "";
                    }
                }
            }
            ////CREATE 'DENY' WITH OLD CARD NO.

            txtCardNo.IsEnabled = true;
            btnDeactivateCard.IsEnabled = false;
            rctDCard.Fill = Brushes.LightGreen;
        }

        private void btnFamilyMembership_Click(object sender, RoutedEventArgs e)
        {
            ////IF EXISTS; GET FAMILY MEMBERS' DETAILS; IF NEW - OPEN BLANK POP-UP
            try
            {
                SqlHelper _sql = new SqlHelper();
                if (_sql._ConnOpen == 0) return;

                DataSet _dsFamily = new DataSet();
                DataTable _dtFamily = new DataTable();
                Hashtable ht = new Hashtable();

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                int _MemberID = Convert.ToInt32(txtMemberNumber.Text);

                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                ht.Add("@MemberID", _MemberID);

                _dsFamily = _sql.ExecuteProcedure("getFamilyMembershipDetails", ht);
                _dtFamily = null;

                if (_dsFamily != null)
                {
                    if (_dsFamily.Tables.Count > 0)
                    {
                        _dtFamily = _dsFamily.Tables[0];
                    }
                }

                FamilyMembership _familyMembership = new FamilyMembership(_dtFamily, _MemberID);
                _familyMembership.Owner = this;
                _familyMembership.ShowDialog();
            }
            catch(Exception ex)
            {
                string _msg = ex.Message;
            }
        }

        private void btnToggle_Unchecked(object sender, RoutedEventArgs e)
        {
            _technoGymToggle = false;
        }

        private void btnToggle_Checked(object sender, RoutedEventArgs e)
        {
            _technoGymToggle = true;
        }

        private void txtFirstName_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                int addedLength = e.Changes.ElementAt(0).AddedLength;
                int removedLength = e.Changes.ElementAt(0).Offset;
                if (addedLength > 0)
                {
                    btnSaveMemberandMembership.IsEnabled = true;
                }
                else if (removedLength <= 0)
                {
                    btnSaveMemberandMembership.IsEnabled = false;
                }
            }
            catch (Exception)
            {
                System.Windows.MessageBox.Show("Error! First Name Field!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void addMemberToTechnoGymDB()
        {
            if (ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"].ToString().Trim() == "")
            {
                ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"] = "";
                getTechnoGymToken().Trim();
            }

            if (ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"].ToString().Trim() != "")
            {
                _technoGymPermanentToken = addTechnoGymMemberDetails();
                if (_technoGymPermanentToken == "" || _technoGymPermanentToken.ToUpper() == "TOKENNOTVALID")
                {
                    ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"] = "";
                    getTechnoGymToken();

                    if ( ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"].ToString().Trim() != "")
                    {
                        _technoGymPermanentToken = addTechnoGymMemberDetails();
                        _technoGymMsg = "Member details added to TechnoGym Database!";
                    }
                    else
                    {
                        _technoGymMsg = "FAILED to add in TechnoGym Database! Please check parameters!";
                    }
                }
                else
                {
                    _technoGymMsg = "Member details added to TechnoGym Database!";
                }
            }
            else
            {
                _technoGymMsg = "FAILED to add in TechnoGym Database! Please check parameters!";
            }
        }
        private void updateMemberToTechnoGymDB()
        {
            string _technoGymMemberFacilityUserId = "";
            _technoGymAccessToken = ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"].ToString().Trim();
            _technoGymFacilityEntityID = ConfigurationManager.AppSettings["TechnoGym_FacilityEntityID_Token"].ToString().Trim();

            if (_technoGymAccessToken.Trim() == "")
            {
                getTechnoGymToken().Trim();
                _technoGymAccessToken = ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"].ToString().Trim();
                _technoGymFacilityEntityID = ConfigurationManager.AppSettings["TechnoGym_FacilityEntityID_Token"].ToString().Trim();
            }

            if (_technoGymAccessToken != "")
            {
                _technoGymMemberFacilityUserId = GetMemberFacilityUserId();
                if (_technoGymMemberFacilityUserId != null)
                {
                    if (_technoGymMemberFacilityUserId.Trim() == "" || _technoGymMemberFacilityUserId.Trim().ToUpper() == "TOKENNOTVALID")
                    {
                        Mouse.OverrideCursor = null;
                        //System.Windows.MessageBox.Show("Error! Token NOT Valid\n\nPlease try again!!", "TechnoGym API Integration", MessageBoxButton.OK, MessageBoxImage.Error);
                        _technoGymMsg = "FAILED to add in TechnoGym Database! Invalid TOKEN!";
                    }
                    else
                    {
                        //UPDATE DETAILS HERE
                        UpdateMemberDetails_To_TechnoGymDB(_technoGymMemberFacilityUserId);
                    }
                }
                else
                {
                    Mouse.OverrideCursor = null;
                    //System.Windows.MessageBox.Show("Error! Please check TechnoGym parameters!", "TechnoGym API Integration", MessageBoxButton.OK, MessageBoxImage.Error);
                    _technoGymMsg = "FAILED to add in TechnoGym Database! Please check parameters!";
                }
            }
            else
            {
                Mouse.OverrideCursor = null;
                //System.Windows.MessageBox.Show("Error! Please check TechnoGym parameters!", "TechnoGym API Integration", MessageBoxButton.OK, MessageBoxImage.Error);
                _technoGymMsg = "FAILED to add in TechnoGym Database! Please check parameters!";
            }
        }
        private string getTechnoGymToken()
        { 
            try
            {
                Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                string technoGymURL = "";
                string technoGymFacility = "";
                string technoGymUniqueID = "";
                string technoGymCLIENT = "";
                string technoGymAPIKEY = "";
                string technoGymUsername = "";
                string technoGymPassword = "";

                if (ConfigurationManager.AppSettings["TechnoGym_Test_OR_Prod"].ToString().ToUpper() == "TEST")
                {
                    technoGymURL = ConfigurationManager.AppSettings["test_technoGym_URL"].ToString();
                    technoGymFacility = ConfigurationManager.AppSettings["test_technoGym_Facility"].ToString();
                    technoGymUniqueID = ConfigurationManager.AppSettings["test_technoGym_UniqueID"].ToString();
                    technoGymCLIENT = ConfigurationManager.AppSettings["test_technoGym_hdr_CLIENT"].ToString();
                    technoGymAPIKEY = ConfigurationManager.AppSettings["test_technoGym_hdr_APIKEY"].ToString();
                    technoGymUsername = ConfigurationManager.AppSettings["test_technoGym_param_Username"].ToString();
                    technoGymPassword = ConfigurationManager.AppSettings["test_technoGym_param_Password"].ToString();
                }
                else if (ConfigurationManager.AppSettings["TechnoGym_Test_OR_Prod"].ToString().ToUpper() == "PROD")
                {
                    technoGymURL = ConfigurationManager.AppSettings["PROD_technoGym_URL"].ToString();
                    technoGymFacility = ConfigurationManager.AppSettings["PROD_technoGym_Facility"].ToString();
                    technoGymUniqueID = ConfigurationManager.AppSettings["PROD_technoGym_UniqueID"].ToString();
                    technoGymCLIENT = ConfigurationManager.AppSettings["PROD_technoGym_hdr_CLIENT"].ToString();
                    technoGymAPIKEY = ConfigurationManager.AppSettings["PROD_technoGym_hdr_APIKEY"].ToString();
                    technoGymUsername = ConfigurationManager.AppSettings["PROD_technoGym_param_Username"].ToString();
                    technoGymPassword = ConfigurationManager.AppSettings["PROD_technoGym_param_Password"].ToString();
                }
                else
                {
                    technoGymURL = "";
                    technoGymFacility = "";
                    technoGymUniqueID = "";
                    technoGymCLIENT = "";
                    technoGymAPIKEY = "";
                    technoGymUsername = "";
                    technoGymPassword = "";
                }

                if (
                       (
                                technoGymURL.Trim() != ""
                            && technoGymFacility != ""
                            && technoGymUniqueID != ""
                            && technoGymCLIENT != ""
                            && technoGymAPIKEY != ""
                            && technoGymUsername != ""
                            && technoGymPassword != ""
                       )
                            &&
                       (
                            ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"].ToString().Trim() == ""
                       )
                   )
                {
                    ConfigurationManager.AppSettings["globalErrorMessage"] = "";
                    TechnoGym_Parameter _technoGymParameter = new TechnoGym_Parameter();
                    if (ConfigurationManager.AppSettings["globalErrorMessage"].ToString().Trim() != "")
                    {
                        System.Windows.MessageBox.Show("Some errors might have occurred!\n\nDetails:\n" + ConfigurationManager.AppSettings["globalErrorMessage"].ToString().Trim(), "TechnoGymAPI Integration (getAccessToken)", MessageBoxButton.OK, MessageBoxImage.Error);
                        return "";
                    }

                    TechnoGymToken _tokenObj = new TechnoGymToken();
                    _tokenObj = _technoGymParameter.GetTechnoGym_Token
                                        (
                                            technoGymURL, technoGymFacility, technoGymUniqueID, technoGymCLIENT,
                                            technoGymAPIKEY, technoGymUsername, technoGymPassword
                                        );

                    if (_tokenObj.StatCode == null)
                    {
                        _technoGymAccessToken = _tokenObj.Token;
                        ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"] = _tokenObj.Token;
                        _technoGymFacilityEntityID = _tokenObj.facilityID;
                        ConfigurationManager.AppSettings["TechnoGym_FacilityEntityID_Token"] = _technoGymFacilityEntityID;
                    }
                    else if (_tokenObj.StatCode == "400")
                    {
                        System.Windows.MessageBox.Show("ERROR 400: \n\nBad Request! Invalid Parameters or Headers!\n\n" + _tokenObj.Token, "TechnoGym API Integration", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else if (_tokenObj.StatCode == "404")
                    {
                        System.Windows.MessageBox.Show("ERROR 404: \n\nNOT FOUND!\n\n" + _tokenObj.Token, "TechnoGym API Integration", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Unknown error in API response!", "TechnoGym API Integration", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    Mouse.OverrideCursor = null;
                    return _technoGymAccessToken;
                }
                else
                {
                    if (ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"].ToString().Trim() == "")
                    {
                        System.Windows.MessageBox.Show("(Access Token)\n\nRequired TechnoGym parameters Missing!\n\nPlease contact Admin to update TechnoGym parameters in\nSettings -> Site Details -> TechnoGym Config", "TechnoGym API Integration", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    Mouse.OverrideCursor = null;
                    return ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"].ToString().Trim();
                }
            }
            catch (Exception ex)
            {
                string _msg = ex.Message;
                Mouse.OverrideCursor = null;
                return "";
            }
        }

        private void btnShowPermanentToken_Click(object sender, RoutedEventArgs e)
        {
            if (txtPermanentToken.Visibility == Visibility.Visible)
            {
                txtPermanentToken.Visibility = Visibility.Hidden;
                rctPermanentToken.Fill = Brushes.LawnGreen;
            }
            else if (txtPermanentToken.Visibility == Visibility.Hidden)
            {
                txtPermanentToken.Visibility = Visibility.Visible;
                rctPermanentToken.Fill = Brushes.BurlyWood;
            }
            else
            {
                txtPermanentToken.Visibility = Visibility.Hidden;
                rctPermanentToken.Fill = Brushes.LawnGreen;
            }
        }

        private string addTechnoGymMemberDetails()
        {
            try
            {
                Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                string technoGymURL = "";
                string technoGymFacility = "";
                string technoGymUniqueID = "";
                string technoGymCLIENT = "";
                string technoGymAPIKEY = "";
                string technoGymUsername = "";
                string technoGymPassword = "";
                string memberExternalID = "";
                string memberGender = "M";
                string memberMobile = "";

                _technoGymAccessToken = ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"].ToString().Trim();
                _technoGymFacilityEntityID = ConfigurationManager.AppSettings["TechnoGym_FacilityEntityID_Token"].ToString().Trim();

                if (ConfigurationManager.AppSettings["TechnoGym_Test_OR_Prod"].ToString().ToUpper() == "TEST")
                {
                    technoGymURL = ConfigurationManager.AppSettings["test_technoGym_URL"].ToString();
                    technoGymFacility = ConfigurationManager.AppSettings["test_technoGym_Facility"].ToString();
                    technoGymUniqueID = ConfigurationManager.AppSettings["test_technoGym_UniqueID"].ToString();
                    technoGymCLIENT = ConfigurationManager.AppSettings["test_technoGym_hdr_CLIENT"].ToString();
                    technoGymAPIKEY = ConfigurationManager.AppSettings["test_technoGym_hdr_APIKEY"].ToString();
                    technoGymUsername = ConfigurationManager.AppSettings["test_technoGym_param_Username"].ToString();
                    technoGymPassword = ConfigurationManager.AppSettings["test_technoGym_param_Password"].ToString();
                }
                else if (ConfigurationManager.AppSettings["TechnoGym_Test_OR_Prod"].ToString().ToUpper() == "PROD")
                {
                    technoGymURL = ConfigurationManager.AppSettings["PROD_technoGym_URL"].ToString();
                    technoGymFacility = ConfigurationManager.AppSettings["PROD_technoGym_Facility"].ToString();
                    technoGymUniqueID = ConfigurationManager.AppSettings["PROD_technoGym_UniqueID"].ToString();
                    technoGymCLIENT = ConfigurationManager.AppSettings["PROD_technoGym_hdr_CLIENT"].ToString();
                    technoGymAPIKEY = ConfigurationManager.AppSettings["PROD_technoGym_hdr_APIKEY"].ToString();
                    technoGymUsername = ConfigurationManager.AppSettings["PROD_technoGym_param_Username"].ToString();
                    technoGymPassword = ConfigurationManager.AppSettings["PROD_technoGym_param_Password"].ToString();
                }
                else
                {
                    technoGymURL = "";
                    technoGymFacility = "";
                    technoGymUniqueID = "";
                    technoGymCLIENT = "";
                    technoGymAPIKEY = "";
                    technoGymUsername = "";
                    technoGymPassword = "";
                }

                if (
                       (
                                technoGymURL.Trim() != ""
                            && technoGymFacility != ""
                            && technoGymUniqueID != ""
                            && technoGymCLIENT != ""
                            && technoGymAPIKEY != ""
                       )
                            &&
                       (
                            _technoGymAccessToken != ""
                       )
                   )
                {

                    TechnoGym_Parameter _technoGymParameter = new TechnoGym_Parameter();
                    TechnoGymMemberParams _tokenObj = new TechnoGymMemberParams();
                    technoGymUniqueID = ConfigurationManager.AppSettings["TechnoGym_FacilityEntityID_Token"].ToString().Trim();
                    if (GetMemberDetailsMemberId == 0)
                    {
                        memberExternalID = "C" + _CompanyID.ToString("0000") + "-S" + _SiteID.ToString("0000") + "-M" + Convert.ToInt32(txtMemberNumber.Text.Trim()).ToString("00000");
                    }
                    else
                    {
                        memberExternalID = "C" + _CompanyID.ToString("0000") + "-S" + _SiteID.ToString("0000") + "-M" + GetMemberDetailsMemberId.ToString("00000");
                    }

                    if (rbFemale.IsChecked == true)
                    {
                        memberGender = "F";
                    }

                    if (txtCellPhone.Text.Trim() != "")
                    {
                        memberMobile = txtCellPhone.Text.Trim();
                    }

                    _tokenObj = _technoGymParameter.AddMember_To_TechnoGym
                                        (
                                            technoGymURL, technoGymFacility, technoGymUniqueID, technoGymCLIENT,
                                            technoGymAPIKEY, technoGymUsername, technoGymPassword
                                            , _technoGymAccessToken
                                            , txtFirstName.Text.ToUpper()
                                            , txtLastName.Text.ToUpper()
                                            , txtEmail.Text
                                            , memberExternalID
                                            , memberGender
                                            , memberMobile
                                        );
                    if (_tokenObj.PermanentToken != null)
                    {
                        if ((_tokenObj.PermanentToken.Trim() != "") && (_tokenObj.PermanentToken.Trim().ToUpper() != "TOKENNOTVALID"))
                        {
                            _technoGymPermanentToken = _tokenObj.PermanentToken;
                        }
                    }
                    else
                    {
                        if (_tokenObj.ErrorMsg != null)
                        {
                            //_technoGymPermanentToken = "";
                            _technoGymPermanentToken = _tokenObj.ErrorMsg;
                        }
                    }
                    Mouse.OverrideCursor = null;
                    return _technoGymPermanentToken;
                }
                else
                {
                    System.Windows.MessageBox.Show("Required TechnoGym parameters Missing!\n\nPlease contact Admin to update TechnoGym parameters in\nSettings -> Site Details -> TechnoGym Config", "TechnoGym API Integration", MessageBoxButton.OK, MessageBoxImage.Error);
                    Mouse.OverrideCursor = null;
                    return "";
                }
            }
            catch (Exception ex)
            {
                string _msg = ex.Message;
                Mouse.OverrideCursor = null;
                return "";
            }
        }
        private void UpdateMemberDetails_To_TechnoGymDB(string _technoGymFacilityUserID)
        {
            try
            {
                Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                string technoGymURL = "";
                string technoGymFacility = "";
                string technoGymUniqueID = "";
                string technoGymCLIENT = "";
                string technoGymAPIKEY = "";
                string technoGymUsername = "";
                string technoGymPassword = "";
                string memberExternalID = "";
                string memberGender = "M";
                string result = "";

                _technoGymAccessToken = ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"].ToString().Trim();
                _technoGymFacilityEntityID = ConfigurationManager.AppSettings["TechnoGym_FacilityEntityID_Token"].ToString().Trim();

                if (ConfigurationManager.AppSettings["TechnoGym_Test_OR_Prod"].ToString().ToUpper() == "TEST")
                {
                    technoGymURL = ConfigurationManager.AppSettings["test_technoGym_URL"].ToString();
                    technoGymFacility = ConfigurationManager.AppSettings["test_technoGym_Facility"].ToString();
                    technoGymUniqueID = ConfigurationManager.AppSettings["test_technoGym_UniqueID"].ToString();
                    technoGymCLIENT = ConfigurationManager.AppSettings["test_technoGym_hdr_CLIENT"].ToString();
                    technoGymAPIKEY = ConfigurationManager.AppSettings["test_technoGym_hdr_APIKEY"].ToString();
                    technoGymUsername = ConfigurationManager.AppSettings["test_technoGym_param_Username"].ToString();
                    technoGymPassword = ConfigurationManager.AppSettings["test_technoGym_param_Password"].ToString();
                }
                else if (ConfigurationManager.AppSettings["TechnoGym_Test_OR_Prod"].ToString().ToUpper() == "PROD")
                {
                    technoGymURL = ConfigurationManager.AppSettings["PROD_technoGym_URL"].ToString();
                    technoGymFacility = ConfigurationManager.AppSettings["PROD_technoGym_Facility"].ToString();
                    technoGymUniqueID = ConfigurationManager.AppSettings["PROD_technoGym_UniqueID"].ToString();
                    technoGymCLIENT = ConfigurationManager.AppSettings["PROD_technoGym_hdr_CLIENT"].ToString();
                    technoGymAPIKEY = ConfigurationManager.AppSettings["PROD_technoGym_hdr_APIKEY"].ToString();
                    technoGymUsername = ConfigurationManager.AppSettings["PROD_technoGym_param_Username"].ToString();
                    technoGymPassword = ConfigurationManager.AppSettings["PROD_technoGym_param_Password"].ToString();
                }
                else
                {
                    technoGymURL = "";
                    technoGymFacility = "";
                    technoGymUniqueID = "";
                    technoGymCLIENT = "";
                    technoGymAPIKEY = "";
                    technoGymUsername = "";
                    technoGymPassword = "";
                }

                if (
                       (
                                technoGymURL.Trim() != ""
                            && technoGymFacility != ""
                            && technoGymUniqueID != ""
                            && technoGymCLIENT != ""
                            && technoGymAPIKEY != ""
                       )
                            &&
                       (
                            _technoGymAccessToken != ""
                       )
                   )
                {

                    TechnoGym_Parameter _technoGymParameter = new TechnoGym_Parameter();
                    TechnoGymMemberParams _tokenObj = new TechnoGymMemberParams();
                    technoGymUniqueID = ConfigurationManager.AppSettings["TechnoGym_FacilityEntityID_Token"].ToString().Trim();
                    memberExternalID = "C" + _CompanyID.ToString("0000") + "-S" + _SiteID.ToString("0000") + "-M" + GetMemberDetailsMemberId.ToString("00000");

                    if (rbFemale.IsChecked == true)
                    {
                        memberGender = "F";
                    }

                    _tokenObj = _technoGymParameter.updateMember_To_TechnoGym
                                        (
                                            technoGymURL, technoGymFacility, _technoGymFacilityUserID, technoGymCLIENT,
                                            technoGymAPIKEY, technoGymUsername, technoGymPassword
                                            , _technoGymAccessToken
                                            , txtFirstName.Text.ToUpper()
                                            , txtLastName.Text.ToUpper()
                                            , txtEmail.Text
                                            , memberExternalID
                                            , memberGender
                                            , txtCellPhone.Text.Trim()
                                        );

                    if (_tokenObj.PermanentToken != null)
                    {
                            result = _tokenObj.PermanentToken;
                    }
                    else
                    {
                        result = "";
                    }
                    Mouse.OverrideCursor = null;
                    if (result.Trim().ToUpper() == "" || result.Trim().ToUpper() == "FAILED")
                    {
                        //System.Windows.MessageBox.Show("FAILED to update in TechnoGym DB!", "TechnoGym API Integration", MessageBoxButton.OK, MessageBoxImage.Information);
                        Mouse.OverrideCursor = null;
                        _technoGymMsg = "FAILED to add in TechnoGym Database! Please check parameters!";
                    }
                    else if (result.Trim().ToUpper() == "SUCCESS")
                    {
                        //System.Windows.MessageBox.Show("SUCCESSFULLY updated in TechnoGym DB!", "TechnoGym API Integration", MessageBoxButton.OK, MessageBoxImage.Information);
                        Mouse.OverrideCursor = null;
                        _technoGymMsg = "SUCCESSFULLY updated in TechnoGym Database!";
                    }
                }
                else
                {
                    //System.Windows.MessageBox.Show("Required TechnoGym parameters Missing!\n\nPlease contact Admin to update TechnoGym parameters in\nSettings -> Site Details -> TechnoGym Config", "TechnoGym API Integration", MessageBoxButton.OK, MessageBoxImage.Error);
                    Mouse.OverrideCursor = null;
                    _technoGymMsg = _technoGymMsg = "FAILED to add in TechnoGym Database! Please check parameters!";
                }
            }
            catch (Exception ex)
            {
                string _msg = ex.Message;
                Mouse.OverrideCursor = null;
            }
        }
        private string GetMemberFacilityUserId()
        {
            try
            {
                Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                string technoGymURL = "";
                string technoGymFacility = "";
                string technoGymUniqueID = "";
                string technoGymCLIENT = "";
                string technoGymAPIKEY = "";
                string technoGymUsername = "";
                string technoGymPassword = "";
                string memberExternalID = "";

                _technoGymAccessToken = ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"].ToString().Trim();
                _technoGymFacilityEntityID = ConfigurationManager.AppSettings["TechnoGym_FacilityEntityID_Token"].ToString().Trim();

                if (ConfigurationManager.AppSettings["TechnoGym_Test_OR_Prod"].ToString().ToUpper() == "TEST")
                {
                    technoGymURL = ConfigurationManager.AppSettings["test_technoGym_URL"].ToString();
                    technoGymFacility = ConfigurationManager.AppSettings["test_technoGym_Facility"].ToString();
                    technoGymUniqueID = ConfigurationManager.AppSettings["test_technoGym_UniqueID"].ToString();
                    technoGymCLIENT = ConfigurationManager.AppSettings["test_technoGym_hdr_CLIENT"].ToString();
                    technoGymAPIKEY = ConfigurationManager.AppSettings["test_technoGym_hdr_APIKEY"].ToString();
                    technoGymUsername = ConfigurationManager.AppSettings["test_technoGym_param_Username"].ToString();
                    technoGymPassword = ConfigurationManager.AppSettings["test_technoGym_param_Password"].ToString();
                }
                else if (ConfigurationManager.AppSettings["TechnoGym_Test_OR_Prod"].ToString().ToUpper() == "PROD")
                {
                    technoGymURL = ConfigurationManager.AppSettings["PROD_technoGym_URL"].ToString();
                    technoGymFacility = ConfigurationManager.AppSettings["PROD_technoGym_Facility"].ToString();
                    technoGymUniqueID = ConfigurationManager.AppSettings["PROD_technoGym_UniqueID"].ToString();
                    technoGymCLIENT = ConfigurationManager.AppSettings["PROD_technoGym_hdr_CLIENT"].ToString();
                    technoGymAPIKEY = ConfigurationManager.AppSettings["PROD_technoGym_hdr_APIKEY"].ToString();
                    technoGymUsername = ConfigurationManager.AppSettings["PROD_technoGym_param_Username"].ToString();
                    technoGymPassword = ConfigurationManager.AppSettings["PROD_technoGym_param_Password"].ToString();
                }
                else
                {
                    technoGymURL = "";
                    technoGymFacility = "";
                    technoGymUniqueID = "";
                    technoGymCLIENT = "";
                    technoGymAPIKEY = "";
                    technoGymUsername = "";
                    technoGymPassword = "";
                }

                if (
                       (
                                technoGymURL.Trim() != ""
                            && technoGymFacility != ""
                            && technoGymUniqueID != ""
                            && technoGymCLIENT != ""
                            && technoGymAPIKEY != ""
                       )
                            &&
                       (
                            _technoGymAccessToken != ""
                       )
                   )
                {

                    TechnoGym_Parameter _technoGymParameter = new TechnoGym_Parameter();
                    TechnoGymMemberDetailsParams _tokenObj = new TechnoGymMemberDetailsParams();
                    technoGymUniqueID = ConfigurationManager.AppSettings["TechnoGym_FacilityEntityID_Token"].ToString().Trim();
                    memberExternalID = "C" + _CompanyID.ToString("0000") + "-S" + _SiteID.ToString("0000") + "-M" + GetMemberDetailsMemberId.ToString("00000");
                    
                    //_tokenObj = _technoGymParameter.AddMember_To_TechnoGym
                    _tokenObj = _technoGymParameter.Get_TechnoGym_User_FacilityID
                                        (
                                            technoGymURL,technoGymFacility, _technoGymFacilityEntityID, 
                                            technoGymCLIENT, technoGymAPIKEY, technoGymUsername, 
                                            technoGymPassword, _technoGymAccessToken, _technoGymPermanentToken
                                        );
                    if (_tokenObj.facilityUserId != null)
                    {
                        if ((_tokenObj.facilityUserId.Trim() != "") && (_tokenObj.facilityUserId.Trim().ToUpper() != "TOKENNOTVALID"))
                        {
                            Mouse.OverrideCursor = null;
                            return _tokenObj.facilityUserId;
                        }
                        Mouse.OverrideCursor = null;
                        return _tokenObj.facilityUserId;
                    }
                    else
                    {
                        Mouse.OverrideCursor = null;
                        return "";
                    }
                }
                else
                {
                    //System.Windows.MessageBox.Show("Required TechnoGym parameters Missing!\n\nPlease contact Admin to update TechnoGym parameters in\nSettings -> Site Details -> TechnoGym Config", "TechnoGym API Integration", MessageBoxButton.OK, MessageBoxImage.Error);
                    Mouse.OverrideCursor = null;
                    _technoGymMsg = "FAILED to add in TechnoGym Database! Please check parameters!";
                    return "";
                }
            }
            catch (Exception ex)
            {
                string _msg = ex.Message;
                Mouse.OverrideCursor = null;
                return "";
            }
        }
    }
}
