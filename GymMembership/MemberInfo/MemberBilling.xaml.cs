﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GymMembership.Comman;
using GymMembership.BAL;
using System.Data;
using GymMembership.DTO;
using System.Configuration;


namespace GymMembership.MemberInfo
{
    /// <summary>
    /// Interaction logic for MemberBilling.xaml
    /// </summary>
    public partial class MemberBilling : Window
    {
        public MemberBilling()
        {
            InitializeComponent();
        }

        public int strMemberNo;
        
        BillingBAL _billingBAL = new BillingBAL();
        Utilities _utilites = new Utilities();
        FormUtilities _formUtilities = new FormUtilities();
        MemberBankDetailsDTO _memberBankDetailsDTO = new MemberBankDetailsDTO();

        private int _addBankDetailsSuccess = 0;
        private string _BankAccountNo = string.Empty ;
        private string _expiryDt = string.Empty;
        private int _bankProcessId = 0;
        private bool chkCCardType;
        private bool reval;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            populateData();
            txtBankPayerName.Focus();
        }
        public void populateData()
        {
            try
            {
                _utilites.populateBillingCompany(cmbBillingCompany);
                _utilites.populateBankCardType(cbCC_Type);
                showBillingHistory();
                HideBankCardType();
                ShowBankDetails();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }
        private void HideBankCardType()
        {
            lblCCType.Visibility = Visibility.Hidden;
            cbCC_Type.Visibility = Visibility.Hidden;
        }
        private void ShowBankCardType()
        {
            lblCCType.Visibility = Visibility.Visible;
            cbCC_Type.Visibility = Visibility.Visible;
        }
        private void showBillingHistory()
        {
            try
            {
                DataSet MemberBillingHistory = _billingBAL.get_BillingHistoryByMemberId(strMemberNo);
                
                if (MemberBillingHistory.Tables[0] != null)
                {
                    gvMemberBillingHistory.ItemsSource = MemberBillingHistory.Tables[0].AsDataView();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }
        private void ShowBankDetails()
        {
            try
            {
                DataSet BankDetailsDS = new DataSet();
                BankDetailsDS = _billingBAL.get_BankDetailsByMemberId(strMemberNo);

                if (BankDetailsDS.Tables[0].Rows.Count > 0)
                {

                    _bankProcessId = Convert.ToInt32(BankDetailsDS.Tables[0].Rows[0]["BankProcessID"].ToString());
                    txtBankPayerName.Text = BankDetailsDS.Tables[0].Rows[0]["BankpayerName"].ToString();
                    txtBankPayerAccount1.Text = BankDetailsDS.Tables[0].Rows[0]["BankPayerAccount1"].ToString();
                    txtBankPayerAccount2.Text = BankDetailsDS.Tables[0].Rows[0]["BankPayerAccount2"].ToString();
                    txtBankPayerAccount3.Text = BankDetailsDS.Tables[0].Rows[0]["BankPayerAccount3"].ToString();
                    txtBankPayerAccount4.Text = BankDetailsDS.Tables[0].Rows[0]["BankPayerAccount4"].ToString();
                    txtBankPayerParticular.Text = BankDetailsDS.Tables[0].Rows[0]["BankPayerParticulars"].ToString();
                    cmbBillingCompany.SelectedValue = Convert.ToInt32(BankDetailsDS.Tables[0].Rows[0]["BillingCompany"].ToString());
                    txtBankPayerName.Text = BankDetailsDS.Tables[0].Rows[0]["BankpayerName"].ToString();
                    txtMaxMoneyCollected.Text = BankDetailsDS.Tables[0].Rows[0]["MaximumMoneyCollected"].ToString();
                    txtMemberBilling_CCName.Text = BankDetailsDS.Tables[0].Rows[0]["CardName"].ToString();
                    txtMemberBilling_CCNumber.Text = BankDetailsDS.Tables[0].Rows[0]["CardNumber"].ToString();
                    txtCC_ExpireMonth.Text = BankDetailsDS.Tables[0].Rows[0]["CardExpiryMonth"].ToString();
                    txtCC_ExpireYear.Text = BankDetailsDS.Tables[0].Rows[0]["CardExpiryYear"].ToString();
                    txtOverDueDeadLine.Text = BankDetailsDS.Tables[0].Rows[0]["OverDueDeadLine"].ToString();
                    chkCCardType = Convert.ToBoolean(BankDetailsDS.Tables[0].Rows[0]["CheckCardType"].ToString());

                    if (chkCCardType == true)
                    {
                        chkCCType.IsChecked = true;
                        ShowBankCardType();
                        cbCC_Type.SelectedValue = Convert.ToInt32(BankDetailsDS.Tables[0].Rows[0]["CardType"].ToString());
                    }
                    else
                    {
                        chkCCType.IsChecked = false;
                        HideBankCardType();
                    }

                    btnSaveBankDetails.Content = "Update";
                }
                else
                {
                    btnSaveBankDetails.Content = "Add";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }
        private void chkCCType_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                ShowBankCardType();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }
        private void chkCCType_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                HideBankCardType();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }
        private void btnSaveBankDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (validateData())
                {
                    _memberBankDetailsDTO.BankProcessId = _bankProcessId;
                    _memberBankDetailsDTO.BankPayerName = txtBankPayerName.Text.ToString();
                    _BankAccountNo = txtBankPayerAccount1.Text.ToString() + txtBankPayerAccount2.Text.ToString() + txtBankPayerAccount3.Text.ToString() + txtBankPayerAccount1.Text.ToString();
                    _memberBankDetailsDTO.BankPayerAccount = _BankAccountNo.ToString();
                    _memberBankDetailsDTO.BankPayerParticulars = txtBankPayerParticular.Text.ToString();
                    _memberBankDetailsDTO.BillingCompanyId = Convert.ToInt32(cmbBillingCompany.SelectedValue.ToString());
                    _memberBankDetailsDTO.MaximumMoneyCollected = Convert.ToDecimal(txtMaxMoneyCollected.Text.ToString());
                    _memberBankDetailsDTO.CreditCardName = txtMemberBilling_CCName.Text.ToString();
                    _memberBankDetailsDTO.CreditCardNumber = txtMemberBilling_CCNumber.Text.ToString();
                    _expiryDt = txtCC_ExpireMonth.Text.ToString() + "/" + "1" + "/" + txtCC_ExpireYear.Text.ToString();
                    _memberBankDetailsDTO.CreditCardExpDate = Convert.ToDateTime(_expiryDt.ToString());
                    if (chkCCType.IsChecked == true)
                    {
                        _memberBankDetailsDTO.CheckCardType = 1;
                        _memberBankDetailsDTO.CardType = Convert.ToInt32(cbCC_Type.SelectedValue.ToString());
                    }
                    else
                    {
                        _memberBankDetailsDTO.CardType = 0;
                        _memberBankDetailsDTO.CheckCardType = 0;
                    }
                    _memberBankDetailsDTO.OverDueDeadLine = Convert.ToDateTime(txtOverDueDeadLine.Text.ToString());
                    _memberBankDetailsDTO.MemberId = strMemberNo;
                    _memberBankDetailsDTO.CompanyId = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());

                    _addBankDetailsSuccess = _billingBAL.SaveBankDetails(_memberBankDetailsDTO);

                    if (_addBankDetailsSuccess != 0)
                    {
                        MessageBox.Show("Bank Details Add/Updated successfully");
                        this.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }
        private void btnCancelBankDetails_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void btnEncryptCC_Click(object sender, RoutedEventArgs e)
        {

        }
        private bool validateData()
        {
            reval = true;

            if (string.IsNullOrEmpty(txtBankPayerName.Text.ToString()))
            {
                MessageBox.Show("Please Enter Payer Name.").ToString();
                txtBankPayerName.Focus();
                reval = false;
                return reval;
            }
            if (txtBankPayerName.Text.Length > 0)
            {
                if (txtBankPayerName.Text.Any(c => Char.IsNumber(c)))
                {
                    MessageBox.Show("Please Enter Payer Name in Characters");
                    //txtBankPayerName.Text = string.Empty;
                    txtBankPayerName.Focus();
                    reval = false;
                    return reval;
                }
            }
            if (string.IsNullOrEmpty(txtBankPayerAccount1.Text.ToString()))
            {
                MessageBox.Show("Please Enter Payer Account No.").ToString();
                txtBankPayerAccount1.Focus();
                reval = false;
                return reval;
            }

            if (!txtBankPayerAccount1.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Please Enter Account No. in Numbers.");
                txtBankPayerAccount1.Focus();
                reval = false;
                //txtBankPayerAccount1.Text = string.Empty;
                return reval;
            }

            if (string.IsNullOrEmpty(txtBankPayerAccount2.Text.ToString()))
            {
                MessageBox.Show("Please Enter Payer Account No.").ToString();
                txtBankPayerAccount2.Focus();
                reval = false;
                return reval;
            }

            if (!txtBankPayerAccount2.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Please Enter Account No. in Numbers.");
                txtBankPayerAccount2.Focus();
                reval = false;
                //txtBankPayerAccount2.Text = string.Empty;
                return reval;
            }

            if (string.IsNullOrEmpty(txtBankPayerAccount3.Text.ToString()))
            {
                MessageBox.Show("Please Enter Payer Account No.").ToString();
                txtBankPayerAccount3.Focus();
                reval = false;
                return reval;
            }

            if (!txtBankPayerAccount3.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Please Enter Account No. in Numbers.");
                txtBankPayerAccount3.Focus();
                reval = false;
                //txtBankPayerAccount3.Text = string.Empty;
                return reval;
            }

            if (string.IsNullOrEmpty(txtBankPayerAccount4.Text.ToString()))
            {
                MessageBox.Show("Please Enter Payer Account No.").ToString();
                txtBankPayerAccount4.Focus();
                reval = false;
                return reval;
            }

            if (!txtBankPayerAccount4.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Please Enter Account No. in Numbers.");
                txtBankPayerAccount4.Focus();
                reval = false;
                //txtBankPayerAccount4.Text = string.Empty;
                return reval;
            }

            if (string.IsNullOrEmpty(txtBankPayerParticular.Text.ToString()))
            {
                MessageBox.Show("Please Enter Bank Payer Particular");
                txtBankPayerParticular.Focus();
                reval = false;
                return reval;
            }

            if (cmbBillingCompany.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Billing Company!");
                cmbBillingCompany.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtMaxMoneyCollected.Text.ToString()))
            {
                MessageBox.Show("Please Enter Maximum Collected Money in Batch");
                txtMaxMoneyCollected.Focus();
                reval = false;
                return reval;
            }

            if (txtMaxMoneyCollected.Text.Any(c => Char.IsLetter(c)))
            {
                MessageBox.Show("Please Enter Maximum Collected Money in Numbers.");
                txtMaxMoneyCollected.Focus();
                reval = false;
                //txtBankPayerAccount4.Text = string.Empty;
                return reval;
            }

            if (string.IsNullOrEmpty(txtMemberBilling_CCName.Text.ToString()))
            {
                MessageBox.Show("Please Enter Credit Card Name.");
                txtMemberBilling_CCName.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtMemberBilling_CCNumber.Text.ToString()))
            {
                MessageBox.Show("Please Enter Credit Card Number");
                txtMemberBilling_CCNumber.Focus();
                reval = false;
                return reval;
            }
            
            if (!txtMemberBilling_CCNumber.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Please Enter Credit Card Number in Numbers.");
                txtMemberBilling_CCNumber.Focus();
                reval = false;
                //txtMemberBilling_CCNumber.Text = string.Empty;
                return reval;
            }
          
            if ((txtMemberBilling_CCNumber.Text.ToString()).Length != 16)
            {
                MessageBox.Show("Please Enter Credit Card Number with 16 Digit");
                txtMemberBilling_CCNumber.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtCC_ExpireMonth.Text.ToString()))
            {
                MessageBox.Show("Please Enter Expiry Month");
                txtCC_ExpireMonth.Focus();
                reval = false;
                return reval;
            }

            if (!txtCC_ExpireMonth.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Please Enter Expiry Month in Numbers.");
                txtCC_ExpireMonth.Focus();
                reval = false;
                //txtCC_ExpireMonth.Text = string.Empty;
                return reval;
            }

            if (string.IsNullOrEmpty(txtCC_ExpireYear.Text.ToString()))
            {
                MessageBox.Show("Please Enter Expiry Year");
                txtCC_ExpireYear.Focus();
                reval = false;
                return reval;
            }

            if (!txtCC_ExpireYear.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Please Enter Expiry Year in Numbers.").ToString();
                txtCC_ExpireYear.Focus();
                reval = false;
                //txtCC_ExpireYear.Text = string.Empty;
                return reval;
            }

            if (chkCCType.IsChecked == true)
            {
                if (cbCC_Type.SelectedIndex == -1)
                {
                    MessageBox.Show("Please Select Card Type");
                    cbCC_Type.Focus();
                    reval = false;
                    return reval;
                }
            }

            if (string.IsNullOrEmpty(txtOverDueDeadLine.Text.ToString()))
            {
                MessageBox.Show("Please select Overdue Dead Line date.");
                txtOverDueDeadLine.Focus();
                reval = false;
                return reval;
            }
            return true;
        }
    }
}
