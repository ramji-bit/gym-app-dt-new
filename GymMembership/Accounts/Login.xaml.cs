﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Data;
using System.Configuration;
using Microsoft.VisualBasic;
using System.Windows.Threading;
using System.ComponentModel;
using GymMembership.BAL;
using GymMembership.Navigation;
using GymMembership.Settings;


namespace GymMembership.Accounts
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login
    {
        public Login()
        {
            InitializeComponent();
            this.Closing += new CancelEventHandler(Login_Closing);
        }

        UserDetailsBAL _userDetailsBAL = new UserDetailsBAL();
        private string _userName;
        private string _password;
        public string _CompanyName;
        public DispatcherTimer dispatcherTimer = new DispatcherTimer();
        public DispatcherTimer multiSiteAccessFile;
        bool _multiSiteAccessFile = false;

        int reda_Auto_Table = 2;
        int site_Details_Table = 1;
        int login_Validation_Table = 0;

        private bool reval;
        private int _loginStatus;
        private int _CompanyExistsInDB;
        private bool _FirstTimeUsage = true;
        bool _isAlreadyClosed = false;
        bool _isAppTimeOutNeverExpire = false;
        bool _currentSAMode = true; //false for SA login screen; true for Normal user;
        void Login_Closing(object sender, CancelEventArgs e)
        {
            if (!_isAlreadyClosed)
            {
                _isAlreadyClosed = true;
                if (MessageBox.Show("Are You sure to Shutdown ?", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    _isAlreadyClosed = true;
                    if (Comman.Constants._DisConnectedDataAdapter != null)
                    {
                        Comman.Constants._DisConnectedDataAdapter.Update(Comman.Constants._DisConnectedALLTables);
                    }
                    System.GC.Collect();
                    Application.Current.Shutdown();
                }
                else
                {
                    e.Cancel = true;
                    _isAlreadyClosed = false;
                    return;
                }
            }
            Application.Current.Shutdown();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GetFirstTimeUseOrgDetails();

                //dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
                //dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 5);
                //dispatcherTimer.Start();

                multiSiteAccessFile = new DispatcherTimer();
                multiSiteAccessFile.Tick += new EventHandler(multiSiteAccessFile_Tick);
                multiSiteAccessFile.Interval = new TimeSpan(0, 2, 0, 0);
                //multiSiteAccessFile.Interval = new TimeSpan(0, 0, 2, 0);
                multiSiteAccessFile.Start();
                cmbOrgName.Focus();

                ////ENABLE 'Super Admin' Login Link
                if (!_currentSAMode)
                {
                    lblLinkToSuperAdmin.Visibility = Visibility.Visible;
                    lblLinkToSuperAdmin.Content = "Login as NORMAL User...";
                    _currentSAMode = true;

                    rwOrgName.Height = new GridLength(0);
                    cmbOrgName.IsEnabled = false;
                    cmbOrgName.Text = "";

                    txtUserName.Background = Brushes.LightGreen;
                    txtUserName.Text = "SUPER ADMIN";
                    txtUserName.IsEnabled = false;
                    txtPassword.Password = "";
                    txtPassword.Focus();

                    btnUserLogin.IsEnabled = false;
                    btnUserLogin.Visibility = Visibility.Hidden;
                    btnSALogin.IsEnabled = true;
                    btnSALogin.Visibility = Visibility.Visible;
                }
                else
                {
                    lblLinkToSuperAdmin.Visibility = Visibility.Hidden;
                    lblLinkToSuperAdmin.Content = "Click here to Login as SUPER ADMIN...";
                    _currentSAMode = false;

                    rwOrgName.Height = GridLength.Auto;
                    cmbOrgName.IsEnabled = true;
                    cmbOrgName.Text = "";

                    txtUserName.Background = Brushes.White;
                    txtUserName.Text = "";
                    txtUserName.IsEnabled = true;
                    txtPassword.Password = "";

                    cmbOrgName.Focus();

                    btnUserLogin.IsEnabled = true;
                    btnUserLogin.Visibility = Visibility.Visible;
                    btnSALogin.IsEnabled = false;
                    btnSALogin.Visibility = Visibility.Hidden;

                    Comman.Constants._SuperAdmin = false;
                    ConfigurationManager.AppSettings["SuperAdmin"] = "";
                    ConfigurationManager.AppSettings["IsAdminUser"] = "";
                    ConfigurationManager.AppSettings["LoggedInUser"] = "";
                    ConfigurationManager.AppSettings["LoggedUserLoginID"] = "";
                    ConfigurationManager.AppSettings["HomeClubSiteID"]="";
                }
                ////ENABLE 'Super Admin' Login Link
            }
            catch (Exception)
            {
                MessageBox.Show (
                                    "Error! Unable to Connect!\n\nReasons could be:\n\n(1) No Network Connection!\n(2) Offline DB is NOT set!\n" + 
                                    "(3) TIMEOUT EXPIRED (Or) Database Connection Settings issue!\n\n" +
                                    "Contact System Administrator!! Application will Shut Down..!!!", 
                                    this.Title, MessageBoxButton.OK, MessageBoxImage.Information
                                );
                _isAlreadyClosed = true;
                Application.Current.Shutdown();
                return;
            }
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (Comman.Constants._OfflineFlag)
            {
                if (ConfigurationManager.AppSettings["LoggedInOrgID"].ToString() != "")
                {
                    if (ConfigurationManager.AppSettings["LoggedInSiteID"].ToString() != "")
                    {
                        Comman.Constants.Popup_dispatcherTimer_Tick();
                    }
                }
            }
        }
        private void multiSiteAccessFile_Tick(object sender, EventArgs e)
        {
            if (!_multiSiteAccessFile)
            {
                if (ConfigurationManager.AppSettings["LoggedInOrgID"].ToString() != "")
                {
                    _multiSiteAccessFile = false; //RUN EVERY 2 HOURS WHEN STARTED
                    Comman.Constants.multiSiteAccessFile_Tick();
                    multiSiteAccessFile.Interval = new TimeSpan(0, 2, 0, 0);
                    //multiSiteAccessFile.Interval = new TimeSpan(0, 0, 2, 0);
                }
            }
            else
            {
                multiSiteAccessFile.Stop();
            }
        }
        private void Window_UnLoaded(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void btnUserLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //GetFirstTimeUseOrgDetails();
                if (!_FirstTimeUsage)
                {
                    this.Cursor = Cursors.Wait;
                    btnUserLogin.Cursor = Cursors.Wait;
                    cmbOrgName.Cursor = Cursors.Wait;
                    txtPassword.Cursor = Cursors.Wait;
                    txtUserName.Cursor = Cursors.Wait;

                    _userName = txtUserName.Text.ToString();
                    _password = txtPassword.Password.ToString();
                    _CompanyName = cmbOrgName.Text.ToUpper();

                    if (validateData())
                    {
                        string _encText;

                        _encText = GymMembership.Comman.Constants.passwordEncrypt(_password, GymMembership.Comman.Constants._EncryptKey);

                        DataSet dC = _userDetailsBAL.get_UserLogin(_userName, _encText, _CompanyName);
                        _loginStatus = Convert.ToInt32(dC.Tables[login_Validation_Table].Rows[0]["Resultvalue"] == DBNull.Value ? 0 : dC.Tables[login_Validation_Table].Rows[0][0]);
                        ConfigurationManager.AppSettings["LoggedInOrgID"] = dC.Tables[login_Validation_Table].Rows[0]["CompanyID"].ToString();

                        ////if (_loginStatus == 1)
                        ////{
                        ////    //if (Comman.Constants._OfflineFlag)
                        ////    //{
                        ////    //    GetDisConnectedDataSetALLTables();
                        ////    //}
                        ////}

                        //////if (_loginStatus == 0)
                        //////{
                        //////    ds = _userDetailsBAL.get_UserLogin(_userName, _password, _CompanyName);
                        //////    _loginStatus = Convert.ToInt32(ds.Tables[0].Rows[0]["Resultvalue"] == DBNull.Value ? 0 : ds.Tables[0].Rows[0][0]);
                        //////}

                        if (_loginStatus == 1)
                        {
                            //MessageBox.Show("This PC : " + System.Environment.MachineName);

                            this.Cursor = Cursors.Wait;
                            btnUserLogin.Cursor = Cursors.Wait;
                            cmbOrgName.Cursor = Cursors.Wait;
                            txtPassword.Cursor = Cursors.Wait;
                            txtUserName.Cursor = Cursors.Wait;

                            ConfigurationManager.AppSettings["CompanyName"] = _CompanyName;
                            ConfigurationManager.AppSettings["LoggedInUser"] = _userName;

                            //dC = _userDetailsBAL.Get_UserCompanySiteDetails(_userName, _encText, _CompanyName);
                            if ((Convert.ToInt32(dC.Tables.Count)) >= 1)
                            {
                                ConfigurationManager.AppSettings["LoggedInOrgID"] = dC.Tables[site_Details_Table].Rows[0]["CompanyID"].ToString();
                                ConfigurationManager.AppSettings["LoggedInSiteID"] = dC.Tables[site_Details_Table].Rows[0]["SiteID"].ToString();

                                //HOME CLUB - POS - 12/SEP/2017
                                ConfigurationManager.AppSettings["HomeClubSiteID"] = dC.Tables[site_Details_Table].Rows[0]["SiteID"].ToString();
                                //HOME CLUB - POS - 12/SEP/2017

                                ConfigurationManager.AppSettings["LoggedInSite"] = dC.Tables[site_Details_Table].Rows[0]["SiteName"].ToString().ToUpper();
                                ConfigurationManager.AppSettings["LoggedInOrg"] = dC.Tables[site_Details_Table].Rows[0]["CompanyName"].ToString().ToUpper();
                                ConfigurationManager.AppSettings["LoggedUserLoginID"] = dC.Tables[site_Details_Table].Rows[0]["LoginID"].ToString();
                                ConfigurationManager.AppSettings["AccessControl_FilePath"] = dC.Tables[site_Details_Table].Rows[0]["AccessControl_FilePath"].ToString();
                                ConfigurationManager.AppSettings["AccessControl_FileName"] = dC.Tables[site_Details_Table].Rows[0]["AccessControl_FileName"].ToString();
                                ConfigurationManager.AppSettings["LoggedUserStaffID"] = dC.Tables[site_Details_Table].Rows[0]["StaffID"].ToString();

                                //EMAIL config from SiteMaster
                                ConfigurationManager.AppSettings["SMTPHost"] = dC.Tables[site_Details_Table].Rows[0]["SMTPHost"].ToString();
                                ConfigurationManager.AppSettings["SMTPPort"] = dC.Tables[site_Details_Table].Rows[0]["SMTPPort"].ToString();
                                ConfigurationManager.AppSettings["SMTPUserName"] = dC.Tables[site_Details_Table].Rows[0]["SMTPUserName"].ToString();
                                ConfigurationManager.AppSettings["SMTPEncryptPassword"] = dC.Tables[site_Details_Table].Rows[0]["SMTPEncryptPassword"].ToString();
                                
                                //Paychoice config from SiteMaster
                                ConfigurationManager.AppSettings["PaychoiceSandboxURL"] = dC.Tables[site_Details_Table].Rows[0]["SandboxURL"].ToString();
                                ConfigurationManager.AppSettings["PaychoiceSandboxUsername"] = dC.Tables[site_Details_Table].Rows[0]["SandboxUsername"].ToString();
                                ConfigurationManager.AppSettings["PaychoiceSandboxPassword"] = dC.Tables[site_Details_Table].Rows[0]["SandboxPassword"].ToString();
                                ConfigurationManager.AppSettings["PaychoiceSecuredURL"] = dC.Tables[site_Details_Table].Rows[0]["SecureURL"].ToString();
                                ConfigurationManager.AppSettings["PaychoiceSecuredUsername"] = dC.Tables[site_Details_Table].Rows[0]["SecureUsername"].ToString();
                                ConfigurationManager.AppSettings["PaychoiceSecuredPassword"] = dC.Tables[site_Details_Table].Rows[0]["SecurePassword"].ToString();
                                ConfigurationManager.AppSettings["PaychoiceUsagePortal"] = dC.Tables[site_Details_Table].Rows[0]["ActivePortal"].ToString();

                                //Address details from SiteMaster - for POS
                                ConfigurationManager.AppSettings["selectedSiteAddr1"] = dC.Tables[site_Details_Table].Rows[0]["Addr1"].ToString();
                                ConfigurationManager.AppSettings["selectedSiteAddr2"] = dC.Tables[site_Details_Table].Rows[0]["Addr2"].ToString();
                                ConfigurationManager.AppSettings["selectedSiteState"] = dC.Tables[site_Details_Table].Rows[0]["State"].ToString();

                                ////POS details from SiteMaster
                                ConfigurationManager.AppSettings["POSOpeningBalance"] = dC.Tables[site_Details_Table].Rows[0]["POS_OpeningBalance"].ToString();
                                ConfigurationManager.AppSettings["POSReportEmail"] = dC.Tables[site_Details_Table].Rows[0]["POS_Report_Email"].ToString();
                                ConfigurationManager.AppSettings["POSStockLevelAlertEmail"] = dC.Tables[site_Details_Table].Rows[0]["POS_Stock_Level_Alert_Email"].ToString();

                                //COM PORT
                                ConfigurationManager.AppSettings["AccessControlPORT"] = dC.Tables[site_Details_Table].Rows[0]["AccessControlPORT"].ToString();

                                //FLAG for user 'Idle' / 'Timeout' EXPIRE
                                _isAppTimeOutNeverExpire = Convert.ToBoolean(dC.Tables[site_Details_Table].Rows[0]["isAppTimeOutNeverExpire"].ToString());

                                //GAS-19 TECHNOGYM PARAMETERS
                                ConfigurationManager.AppSettings["test_technoGym_URL"] = dC.Tables[site_Details_Table].Rows[0]["test_technoGym_URL"].ToString();
                                ConfigurationManager.AppSettings["test_technoGym_Facility"] = dC.Tables[site_Details_Table].Rows[0]["test_technoGym_Facility"].ToString();
                                ConfigurationManager.AppSettings["test_technoGym_UniqueID"] = dC.Tables[site_Details_Table].Rows[0]["test_technoGym_UniqueID"].ToString();
                                ConfigurationManager.AppSettings["test_technoGym_hdr_CLIENT"] = dC.Tables[site_Details_Table].Rows[0]["test_technoGym_hdr_CLIENT"].ToString();
                                ConfigurationManager.AppSettings["test_technoGym_hdr_APIKEY"] = dC.Tables[site_Details_Table].Rows[0]["test_technoGym_hdr_APIKEY"].ToString();
                                ConfigurationManager.AppSettings["test_technoGym_param_Username"] = dC.Tables[site_Details_Table].Rows[0]["test_technoGym_param_Username"].ToString();
                                ConfigurationManager.AppSettings["test_technoGym_param_Password"] = dC.Tables[site_Details_Table].Rows[0]["test_technoGym_param_Password"].ToString();

                                ConfigurationManager.AppSettings["PROD_technoGym_URL"] = dC.Tables[site_Details_Table].Rows[0]["PROD_technoGym_URL"].ToString();
                                ConfigurationManager.AppSettings["PROD_technoGym_Facility"] = dC.Tables[site_Details_Table].Rows[0]["PROD_technoGym_Facility"].ToString();
                                ConfigurationManager.AppSettings["PROD_technoGym_UniqueID"] = dC.Tables[site_Details_Table].Rows[0]["PROD_technoGym_UniqueID"].ToString();
                                ConfigurationManager.AppSettings["PROD_technoGym_hdr_CLIENT"] = dC.Tables[site_Details_Table].Rows[0]["PROD_technoGym_hdr_CLIENT"].ToString();
                                ConfigurationManager.AppSettings["PROD_technoGym_hdr_APIKEY"] = dC.Tables[site_Details_Table].Rows[0]["PROD_technoGym_hdr_APIKEY"].ToString();
                                ConfigurationManager.AppSettings["PROD_technoGym_param_Username"] = dC.Tables[site_Details_Table].Rows[0]["PROD_technoGym_param_Username"].ToString();
                                ConfigurationManager.AppSettings["PROD_technoGym_param_Password"] = dC.Tables[site_Details_Table].Rows[0]["PROD_technoGym_param_Password"].ToString();

                                ConfigurationManager.AppSettings["TechnoGym_Test_OR_Prod"] = dC.Tables[site_Details_Table].Rows[0]["TechnoGym_Test_OR_Prod"].ToString().ToUpper();
                                //GAS-19 TECHNOGYM PARAMETERS

                                //StaffPIC
                                Dashboard d = new Dashboard();
                                d.Name = "DASHBOARD";
                                d._isSessionNeverExpire = _isAppTimeOutNeverExpire;

                                if (dC.Tables[site_Details_Table].Rows[0]["StaffPic"] != DBNull.Value)
                                {
                                    byte[] data = (byte[])dC.Tables[site_Details_Table].Rows[0]["StaffPic"];
                                    d._StaffPic = data;
                                }
                                d._CompanyName = dC.Tables[site_Details_Table].Rows[0]["CompanyName"].ToString();
                                d._SiteName = dC.Tables[site_Details_Table].Rows[0]["SiteName"].ToString();
                                d._CompanyAndSiteName = d._CompanyName + " ( " + d._SiteName + " )";
                                string _staffName = dC.Tables[site_Details_Table].Rows[0]["FirstName"].ToString() + " " + dC.Tables[site_Details_Table].Rows[0]["LastName"].ToString();
                                if (_staffName.Trim() != "")
                                {
                                    d._UserName = _staffName + " ( " + _userName + " )";
                                }
                                else
                                {
                                    d._UserName =  _userName;
                                }
                                d._CurrentLoginDateTime = DateTime.Now.ToString("ddd, dd MMM yyyy hh:mm tt");

                                if (dC.Tables[site_Details_Table].Rows[0]["UserRole"].ToString() == "1")
                                {
                                    ConfigurationManager.AppSettings["IsAdminUser"] = "No";
                                }
                                else
                                {
                                    ConfigurationManager.AppSettings["IsAdminUser"] = "Yes";
                                }
                                //REDA AUTO EMAIL
                                if (dC.Tables.Count > 1)
                                {
                                    if (dC.Tables[reda_Auto_Table].Rows.Count > 0)
                                    {
                                        ConfigurationManager.AppSettings["REDAAutoEmailID"] = dC.Tables[reda_Auto_Table].Rows[0]["emailID"].ToString();
                                        ConfigurationManager.AppSettings["REDAAutoEmailEncPwd"] = dC.Tables[reda_Auto_Table].Rows[0]["encPassword"].ToString();
                                    }
                                }

                                _isAlreadyClosed = true;
                                d.Show();
                                this.ShowInTaskbar = false;
                                this.dispatcherTimer.Stop();
                                this.multiSiteAccessFile.Stop();
                                this.Hide();
                                d._Login = this;
                                d._Login._isAlreadyClosed = false;
                                //this.Close();
                            }
                        }
                        else
                        {
                            this.Cursor = Cursors.Arrow;
                            btnUserLogin.Cursor = Cursors.Hand;
                            cmbOrgName.Cursor = Cursors.IBeam;
                            txtPassword.Cursor = Cursors.IBeam;
                            txtUserName.Cursor = Cursors.IBeam;

                            MessageBox.Show("Invalid Organization OR UserName OR Password", "Login", MessageBoxButton.OK, MessageBoxImage.Information);
                            ClearLoginData_InvalidLogin();
                            cmbOrgName.Focus();
                            this.Cursor = Cursors.Arrow;
                            btnUserLogin.Cursor = Cursors.Hand;
                            this.Title = "Login";
                        }
                    }
                    this.Cursor = Cursors.Arrow;
                    btnUserLogin.Cursor = Cursors.Hand;
                    cmbOrgName.Cursor = Cursors.IBeam;
                    txtPassword.Cursor = Cursors.IBeam;
                    txtUserName.Cursor = Cursors.IBeam;
                } //if FirstTime usage
                else
                {
                    AddNewOrgNameAndUser();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error! : No Network Connection OR Check your Database Connection Settings!\n(Connection Required to fetch OFFLINE data!)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                MessageBox.Show("Error! : No Network Connection OR Check your Database Connection Settings!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                this.Cursor = Cursors.Arrow;
                btnUserLogin.Cursor = Cursors.Hand;
                cmbOrgName.Cursor = Cursors.IBeam;
                txtPassword.Cursor = Cursors.IBeam;
                txtUserName.Cursor = Cursors.IBeam;
            }
        }
        private bool validateData()
        {
            reval = true;

            if (String.IsNullOrEmpty(cmbOrgName.Text.ToString()))
            {
                MessageBox.Show("Please Enter Organization Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                cmbOrgName.Focus();
                reval = false;
                return reval;
            }
            if (cmbOrgName.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Organization Name Cannot be Numeric!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                cmbOrgName.Focus();
                reval = false;
                return reval;
            }
            if (OrgNameExistsinDB(_CompanyName) == false)
            {
                if (MessageBox.Show("Organization Name Does NOT Exist in Database! \nDO YOU WANT TO CREATE THIS ORGANIZATION?",
                    this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    _FirstTimeUsage = true;
                    AddNewOrgNameAndUser();
                    return false;
                }
                return false;
            }

            if (String.IsNullOrEmpty(txtUserName.Text.ToString()))
            {
                MessageBox.Show("Please Enter User Name", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtUserName.Focus();
                reval = false;
                return reval;
            }
            if (String.IsNullOrEmpty(txtPassword.Password.ToString()))
            {
                MessageBox.Show("Please Enter Password", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtPassword.Focus();
                reval = false;
                return reval;
            }

            return true;
        }
        private bool OrgNameExistsinDB(string _strOrgName)
        {
            try
            {
                DataSet Ds = _userDetailsBAL.OrgNameExistsinDB(_strOrgName);
                _CompanyExistsInDB = Convert.ToInt32(Ds.Tables[0].Rows[0]["Result"] == DBNull.Value ? 0 : Ds.Tables[0].Rows[0][0]);
                if (_CompanyExistsInDB == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                //MessageBox.Show("Error! No Network Connection OR Check your Database Connection Settings! \n\nConnection Required to get OFFLINE data!\n\nApplication will Shut Down..!!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                MessageBox.Show("Error! No Network Connection OR Check your Database Connection Settings!\n\nApplication will Shut Down..!!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                _isAlreadyClosed = true;
                Application.Current.Shutdown();
                return false;
            }
        }
        private void ClearLoginData()
        {
            txtUserName.Text = string.Empty;
            txtPassword.Password = string.Empty;
            //cmbOrgName.SelectedIndex = -1;
        }
        private void ClearLoginData_InvalidLogin()
        {
            txtPassword.Password = string.Empty;
            //cmbOrgName.SelectedIndex = -1;
        }
        private void btnUserLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnUserLogin_Click(sender, e);
            }
        }

        private void btnUserLogin_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
            btnUserLogin.Background = Brushes.Green;
        }

        private void btnUserLogin_MouseOver(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
            btnUserLogin.Background = Brushes.Black;
        }
        public DataSet GetOrgNameFromDB()
        {
            return _userDetailsBAL.GetOrgNameFromDB();
        }
        public void GetFirstTimeUseOrgDetails()
        {
            DataSet _OrgNames = GetOrgNameFromDB();
            if (_OrgNames.Tables[0].Rows.Count > 0)
            {
                _FirstTimeUsage = false;
            }
            else
            {
                _FirstTimeUsage = true;
            }

            //if (_OrgNames.Tables[0].Rows.Count <= 0)
            //{
            //    _FirstTimeUsage = true;
            //    MessageBox.Show("FIRST TIME USAGE OF SOFTWARE DETECTED...!! \n\nPlease Enter Your Organization Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //    txtUserName.IsEnabled = false;
            //    txtPassword.IsEnabled = false;

            //    txtUserName.Visibility = Visibility.Collapsed;
            //    txtPassword.Visibility = Visibility.Collapsed;
            //    userIcon.Visibility = Visibility.Collapsed;
            //    pwdIcon.Visibility = Visibility.Collapsed;

            //    //btnUserLogin.Margin = new Thickness(170, 108, 0, 0);
            //    //btnUserLogin.Width = 180;
            //    //btnUserLogin.Content = "Create New Org!";
            //}
            //else
            //{
            //    _FirstTimeUsage = false;
            //    txtUserName.IsEnabled = true;
            //    txtPassword.IsEnabled = true;
            //    txtUserName.Visibility = Visibility.Visible;
            //    txtPassword.Visibility = Visibility.Visible;
            //    userIcon.Visibility = Visibility.Visible;
            //    pwdIcon.Visibility = Visibility.Visible;


            //    //btnUserLogin.Margin = new Thickness(275, 108, 0, 0);
            //    //btnUserLogin.Width = 75;
            //    //btnUserLogin.Content = "Login";
            //}
        }
        private string GenerateKEYCodeForFirstTimeUse()
        {
            try
            {
                string _GeneratedKeyCode = ""; char _HrChar;
                string _DateAndTimeHr; string _Day;
                bool _boolInc = true; bool _boolDec = false;

                _Day = DateTime.Today.DayOfWeek.ToString();
                int Hour = Convert.ToInt32(DateTime.Now.Hour.ToString("00"));
                int Min = Convert.ToInt32(DateTime.Now.Minute.ToString("00"));
                if (Min > 30)
                {
                    Min = 30;
                }
                else
                {
                    Min = 0;
                }
                string _TimeHr = Hour.ToString("00") + Min.ToString("00");
                _DateAndTimeHr = Convert.ToString(DateTime.Today.Date.ToString("ddMMyyyy")) + _TimeHr;
                //Copy change from KeyCodeGen

                char[] _ToDayDate = _DateAndTimeHr.ToCharArray();
                char[] _ToDayName = _Day.ToCharArray();

                switch (_Day.ToUpper())
                {
                    case "SUNDAY":
                        _HrChar = '!';
                        break;
                    case "MONDAY":
                        _HrChar = '@';
                        break;
                    case "TUESDAY":
                        _HrChar = '#';
                        break;
                    case "WEDNESDAY":
                        _HrChar = '$';
                        break;
                    case "THURSDAY":
                        _HrChar = '%';
                        break;
                    case "FRIDAY":
                        _HrChar = '&';
                        break;
                    case "SATURDAY":
                        _HrChar = '*';
                        break;
                    default:
                        _HrChar = '=';
                        break;
                }

                int _DateLen = _ToDayDate.Length; int _DayLen = _ToDayName.Length;
                int _totLen = (_DateLen + _DayLen) * 2;
                char[] _interimGenKeyCode = new char[_totLen];

                int j = 0; int k = _DayLen - 1;
                int _ModCheck = 0;

                for (int i = 0; i < _totLen; i += 2) //Incr. by 2, to accomodate special char in each iteration!
                {
                    if ((_ModCheck % 2) == 0)
                    {
                        if (j < _DateLen)
                        {
                            _interimGenKeyCode[i] = _ToDayDate[j]; j++;
                            _interimGenKeyCode[i + 1] = _HrChar;
                            if (_boolInc)
                            {
                                if ((Convert.ToInt32(_HrChar) + 1) > 47)
                                {
                                    _HrChar = Convert.ToChar(Convert.ToInt32(_HrChar) - 1);
                                    _boolInc = false;
                                    _boolDec = true;
                                }
                                else
                                {
                                    _HrChar = Convert.ToChar(Convert.ToInt32(_HrChar) + 1);
                                    _boolInc = true;
                                    _boolDec = false;
                                }
                            }
                            else if (_boolDec)
                            {
                                if ((Convert.ToInt32(_HrChar) - 1) < 33)
                                {
                                    _HrChar = Convert.ToChar(Convert.ToInt32(_HrChar) + 1);
                                    _boolInc = true;
                                    _boolDec = false;
                                }
                                else
                                {
                                    _HrChar = Convert.ToChar(Convert.ToInt32(_HrChar) - 1);
                                    _boolInc = false;
                                    _boolDec = true;
                                }
                            }
                        }
                        else
                        {
                            if (k >= 0)
                            {
                                _interimGenKeyCode[i] = _ToDayName[k]; k--;
                                _interimGenKeyCode[i + 1] = _HrChar;
                                if (_boolInc)
                                {
                                    if ((Convert.ToInt32(_HrChar) + 1) > 47)
                                    {
                                        _HrChar = Convert.ToChar(Convert.ToInt32(_HrChar) - 1);
                                        _boolInc = false;
                                        _boolDec = true;
                                    }
                                    else
                                    {
                                        _HrChar = Convert.ToChar(Convert.ToInt32(_HrChar) + 1);
                                        _boolInc = true;
                                        _boolDec = false;
                                    }
                                }
                                else if (_boolDec)
                                {
                                    if ((Convert.ToInt32(_HrChar) - 1) < 33)
                                    {
                                        _HrChar = Convert.ToChar(Convert.ToInt32(_HrChar) + 1);
                                        _boolInc = true;
                                        _boolDec = false;
                                    }
                                    else
                                    {
                                        _HrChar = Convert.ToChar(Convert.ToInt32(_HrChar) - 1);
                                        _boolInc = false;
                                        _boolDec = true;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (k >= 0)
                        {
                            _interimGenKeyCode[i] = _ToDayName[k]; k--;
                            _interimGenKeyCode[i + 1] = _HrChar;
                            if (_boolInc)
                            {
                                if ((Convert.ToInt32(_HrChar) + 1) > 47)
                                {
                                    _HrChar = Convert.ToChar(Convert.ToInt32(_HrChar) - 1);
                                    _boolInc = false;
                                    _boolDec = true;
                                }
                                else
                                {
                                    _HrChar = Convert.ToChar(Convert.ToInt32(_HrChar) + 1);
                                    _boolInc = true;
                                    _boolDec = false;
                                }
                            }
                            else if (_boolDec)
                            {
                                if ((Convert.ToInt32(_HrChar) - 1) < 33)
                                {
                                    _HrChar = Convert.ToChar(Convert.ToInt32(_HrChar) + 1);
                                    _boolInc = true;
                                    _boolDec = false;
                                }
                                else
                                {
                                    _HrChar = Convert.ToChar(Convert.ToInt32(_HrChar) - 1);
                                    _boolInc = false;
                                    _boolDec = true;
                                }
                            }
                        }
                        else
                        {
                            if (j < _DateLen)
                            {
                                _interimGenKeyCode[i] = _ToDayDate[j]; j++;
                                _interimGenKeyCode[i + 1] = _HrChar;
                                if (_boolInc)
                                {
                                    if ((Convert.ToInt32(_HrChar) + 1) > 47)
                                    {
                                        _HrChar = Convert.ToChar(Convert.ToInt32(_HrChar) - 1);
                                        _boolInc = false;
                                        _boolDec = true;
                                    }
                                    else
                                    {
                                        _HrChar = Convert.ToChar(Convert.ToInt32(_HrChar) + 1);
                                        _boolInc = true;
                                        _boolDec = false;
                                    }
                                }
                                else if (_boolDec)
                                {
                                    if ((Convert.ToInt32(_HrChar) - 1) < 33)
                                    {
                                        _HrChar = Convert.ToChar(Convert.ToInt32(_HrChar) + 1);
                                        _boolInc = true;
                                        _boolDec = false;
                                    }
                                    else
                                    {
                                        _HrChar = Convert.ToChar(Convert.ToInt32(_HrChar) - 1);
                                        _boolInc = false;
                                        _boolDec = true;
                                    }
                                }
                            }
                        }
                    }
                    _GeneratedKeyCode = _GeneratedKeyCode + _interimGenKeyCode[i] + _interimGenKeyCode[i + 1];
                    if (i > 30) { _ModCheck++; } else { _ModCheck++; }
                }
                return _GeneratedKeyCode;
            }
            catch
            {
                return "";
            }
        }
        private void AddNewOrgNameAndUser()
        {
            if (cmbOrgName.Text.ToString().Trim() != "")
            {
                //***DO NOT DELETE THE below COMMENTED LINES
                // ENABLE BELOW CODE, FOR KEYCODE VALIDATION WHEN NEW ORG. IS CREATED... FOR RELEASE ONLY!!!
                string _KEYCode = "";
                _KEYCode = Interaction.InputBox("Enter KEY CODE  \n\n(OR) \n\nContact Your Administrator!", "KEY", "");
                if (_KEYCode.Trim() == "")
                {
                    MessageBox.Show("KEY CODE Cannot be Blank!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    _FirstTimeUsage = false;
                    return;
                }

                if (_KEYCode != GenerateKEYCodeForFirstTimeUse())
                {
                    MessageBox.Show("KEY CODE Entered is INVALID...! (Note: KEY Code is Case Sensitive!) \nContact Your Administrator!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    _FirstTimeUsage = false;
                    return;
                }
                else
                {
                    //INSERT NEW ORGNAME & DEFAULT SITE NAME IN DB..
                    MessageBox.Show("KEY CODE Matched...!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    // ENABLE ABOVE CODE, FOR KEYCODE VALIDATION WHEN NEW ORG. IS CREATED... FOR RELEASE ONLY!!!
                    //***DO NOT DELETE THE above COMMENTED LINES

                    DataSet dS = _userDetailsBAL.AddNewOrganizationName(cmbOrgName.Text.ToString().Trim().ToUpper());
                    if (Convert.ToInt32(dS.Tables[0].Rows[0]["Result"]) == 0)
                    {
                        MessageBox.Show("Organization Name NOT Added..! \n\n Please Contact System Administrator!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        ConfigurationManager.AppSettings["LoggedInOrg"] = cmbOrgName.Text.ToString().Trim().ToUpper();
                        MessageBox.Show("Organization Name Added Successfully..! \n\nPlease Create a User and Restart the Application!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        ConfigurationManager.AppSettings["LoggedInOrgID"] = Convert.ToInt32(dS.Tables[0].Rows[0]["Result"]).ToString();
                        ConfigurationManager.AppSettings["LoggedInSiteID"] = Convert.ToInt32(dS.Tables[0].Rows[0]["SiteID"]).ToString();

                        UserAdministration _newUser = new UserAdministration();
                        _newUser.Owner = this;
                        _newUser._newOrgDefaultAdminUser = true;
                        _newUser.ShowDialog();
                        MessageBox.Show("APPLICATION WILL NOW SHUT DOWN.. Please Login with the User Name created!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        //this.Close();
                        _isAlreadyClosed = true;
                        Application.Current.Shutdown();
                    }
                    return;
                } //KEY Code
            }
            else
            {
                MessageBox.Show("Organization Name Cannot be Blank!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                cmbOrgName.Focus();
                _FirstTimeUsage = false;
                return;
            }
        }
        public void GetDisConnectedDataSetALLTables() //TO WORK OFFLINE
        {
            Comman.Constants.disconnected_DataSet();
            for (int i = 0; i < Comman.Constants._DisConnectedALLTables.Tables[0].Rows.Count - 1; i++)
            {
                Comman.Constants._DisConnectedALLTables.Tables[i + 1].TableName = Comman.Constants._DisConnectedALLTables.Tables[0].Rows[i]["Name"].ToString();
            }
        }

        private void lblLinkToSuperAdmin_MouseEnter(object sender, MouseEventArgs e)
        {
            lblLinkToSuperAdmin.Foreground = Brushes.Red;
        }

        private void lblLinkToSuperAdmin_MouseLeave(object sender, MouseEventArgs e)
        {
            lblLinkToSuperAdmin.Foreground = Brushes.Yellow;
        }

        private void lblLinkToSuperAdmin_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!_currentSAMode)
            {
                lblLinkToSuperAdmin.Content = "Login as NORMAL User...";
                _currentSAMode = true;

                rwOrgName.Height = new GridLength(0);
                cmbOrgName.IsEnabled = false;
                cmbOrgName.Text = "";

                txtUserName.Background = Brushes.LightGreen;
                txtUserName.Text = "SUPER ADMIN";
                txtUserName.IsEnabled = false;
                txtPassword.Password = "";
                txtPassword.Focus();

                btnUserLogin.IsEnabled = false;
                btnUserLogin.Visibility = Visibility.Hidden;
                btnSALogin.IsEnabled = true;
                btnSALogin.Visibility = Visibility.Visible;
            }
            else
            {
                lblLinkToSuperAdmin.Content = "Click here to Login as SUPER ADMIN...";
                _currentSAMode = false;

                rwOrgName.Height = GridLength.Auto;
                cmbOrgName.IsEnabled = true;
                cmbOrgName.Text = "";

                txtUserName.Background = Brushes.White;
                txtUserName.Text = "";
                txtUserName.IsEnabled = true;
                txtPassword.Password = "";

                cmbOrgName.Focus();

                btnUserLogin.IsEnabled = true;
                btnUserLogin.Visibility = Visibility.Visible;
                btnSALogin.IsEnabled = false;
                btnSALogin.Visibility = Visibility.Hidden;

                Comman.Constants._SuperAdmin = false;
                ConfigurationManager.AppSettings["SuperAdmin"] = "";
                ConfigurationManager.AppSettings["IsAdminUser"] = "";
                ConfigurationManager.AppSettings["LoggedInUser"] = "";
                ConfigurationManager.AppSettings["LoggedUserLoginID"] = "";
            }
        }

        private void btnSALogin_Click(object sender, RoutedEventArgs e)
        {
            string _KEYCode = txtPassword.Password;

            if (_KEYCode.Trim() == "")
            {
                MessageBox.Show("Password cannot be Blank!", "Super Admin Login", MessageBoxButton.OK, MessageBoxImage.Information);
                txtPassword.Focus();
                return;
            }

            if (_KEYCode != GenerateKEYCodeForFirstTimeUse())
            {
                MessageBox.Show("SuperAdmin Password is INVALID...! \n\n(Note: Password is Case Sensitive!)", "Super Admin Login", MessageBoxButton.OK, MessageBoxImage.Information);
                txtPassword.Password = "";
                txtPassword.Focus();
                return;
            }

            Comman.Constants._SuperAdmin = true;
            ConfigurationManager.AppSettings["SuperAdmin"] = "YES";
            ConfigurationManager.AppSettings["IsAdminUser"] = "YES";
            ConfigurationManager.AppSettings["LoggedInUser"] = "SUPER ADMIN";
            ConfigurationManager.AppSettings["LoggedUserLoginID"] = "0";
            //MessageBox.Show("KEY CODE MATCHED !!!", "Super Admin Login", MessageBoxButton.OK, MessageBoxImage.Information);

            DashboardSAdmin _dSA = new DashboardSAdmin();
            _dSA.Name = "SADASHBOARD";
            _dSA._Login = this;
            _dSA._CurrentLoginDateTime = DateTime.Now.ToString("ddd, dd MMM yyyy hh:mm tt");
            _dSA._UserName = "SUPER ADMIN";
            _dSA.Show();
            this.Hide();
        }
    }
}
