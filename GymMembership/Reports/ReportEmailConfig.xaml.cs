﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using GymMembership.Comman;
using System.Data;
using System.Configuration;
using System.Collections;

namespace GymMembership.Reports
{
    /// <summary>
    /// Interaction logic for Report.xaml
    /// </summary>
    public partial class ReportEmailConfig
    {
        private int _programmeGroupId = 0;
        String way = "";
        int datasetcheck;
        string _reportName = "";
        DateTime from_date = DateTime.Now;
        DateTime to_date = DateTime.Now;
        //int z=0;
        DataSet ds = new DataSet();
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;

        ReportView rptView = new ReportView();
        DataSet _dsReportTypeList = new DataSet();
        DataTable _dtReportType = new DataTable();
        DataTable _dtReportList = new DataTable();
        DataSet _dsCurrSiteConfig = new DataSet();
        DataTable _dtCurrSiteConfig = new DataTable();

        int _ReportSeqID = 0;
        int _ReportSerialNum = 0;
        string _Frequency = "";
        int _currSite = -1;
        string _emailIDs = "";
        public ReportEmailConfig()
        {
            InitializeComponent();
        }

        public void populatedata()
        {
            //Utilities _utilites = new Utilities();
            //_utilites.populateProgramGroup(cmbgroup);
            //_utilites.bookingresourcetype(cmbresourcetype);
        }
        private int Emptydataset(DataSet ds)
        {
            int value;
            if (ds == null)
            {
                MessageBox.Show("Report Does not contain any data", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                value = 0;
            }
            else if (ds.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("Report Does not contain any data", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                value = 0;
            }
            else
            {
                value = 1;
            }
            return value;

        }
        private void SetReportList(int _RSeqID)
        {
            gvReport.ItemsSource = null;
            gvReport.Items.Clear();

            if (_dtReportList != null)
            {
                _dtReportList.DefaultView.RowFilter = "";

                if (_RSeqID > 0)
                {
                    _dtReportList.DefaultView.RowFilter = "ReportTypeID = " + _RSeqID;
                    _dtReportList.DefaultView.Sort = " ReportID ASC ";

                    if (_dtReportList.DefaultView.Count > 0)
                    {
                        gvReport.ItemsSource = _dtReportList.DefaultView;
                        gvReport.DisplayMemberPath = "ReportName";
                        gvReport.SelectedIndex = 0;
                    }
                }
            }
        }
        private void btnmemberReports_Click(object sender, RoutedEventArgs e)
        {
            btnmemberReports.Background = Brushes.Chocolate;
            btnSalesReports.Background = Brushes.Gray;
            btnProspectReports.Background = Brushes.Gray;
            btnBookingReports.Background = Brushes.Gray;
            btnFinancialReports.Background = Brushes.Gray;
            btnRetentionReports.Background = Brushes.Gray;
            btnPOSReports.Background = Brushes.Gray;

            HideControls();
            populatedata();

            _ReportSeqID = 1;
            SetReportList(_ReportSeqID);
        }
        private void btnViewReport_Click(object sender, RoutedEventArgs e)
        {
            //////REPORT EMAIL CONFIG. SETTINGS SCREEN - VIEW BUTTON NOT REQUIRED
            //if (gvReport.SelectedIndex == -1)
            //{
            //    MessageBox.Show("Please Select a Report from List!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //    return;
            //}

            //int i = gvReport.SelectedIndex;
            //_reportName = gvReport.Items[i].ToString();

            //rptView = new ReportView();

            //rptView._CompanyName = this._CompanyName;
            //rptView._UserName = this._UserName;
            //rptView._CurrentLoginDateTime = this._CurrentLoginDateTime;
            //rptView._StaffPic = this._StaffPic;
            //rptView.StaffImage.Source = StaffImage.Source;

            //switch (_reportName.ToUpper())
            //{
            //    case "MEMBER DETAILS REPORT"://1
            //        ds = getMemberReport();
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "MEMBER PAYMENTS REPORT"://2
            //        ds = getMemberPaymentReport();
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "MEMBER ONHOLD REPORT"://3
            //        MessageBox.Show("No Member is on HOLD!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //        return;
            //    case "SALES ENQUIRIES-PROSPECTS REPORT"://4
            //        ds = getProspectReport();
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "ALL MEMBERS EXCLUDING HISTORICAL MEMBERS"://5
            //        ds = getwithouthistoricalmemeber();
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "ALL MEMBERS INCLUDING HISTORICAL MEMBERS"://6
            //        ds = getAllmemeber();
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "ALL MEMBERS USING DD MEMBERSHIP"://7
            //        ds = getDirectDebitMember();
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "BILLING TRANSACTION HISTORY"://8
            //        ds = getBillingHistory();
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "BIRTHDAY LIST"://9
            //        ds = getBirthdays();
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    //case "CANCELLING MEMBERS"://10
            //    case "ARCHIVED MEMBERS":
            //        ds = CancellingMemeber();
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "CURRENT MEMBERS"://11
            //        ds = getCurrentMembers();
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "END OF DAY REPORT FOR POINT OF SALE"://12
            //        String way = "Daily";
            //        DateTime from_date = DateTime.Now;
            //        DateTime to_date = DateTime.Now;
            //        if ((dtfromdate.Text == "") && (dttodate.Text == ""))
            //        {
            //            MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //            return;
            //        }
            //        if ((dtfromdate.Text != "") && (dttodate.Text == ""))
            //        {

            //            if (way == "Daily")
            //            {
            //                from_date = DateTime.Parse(dtfromdate.Text);
            //                to_date = DateTime.Now;
            //            }
            //        }
            //        else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
            //        {
            //            DateTime FirstDay = new DateTime(DateTime.Now.Year, 1, 1);
            //            if (way == "Daily")
            //            {
            //                from_date = FirstDay;
            //                to_date = DateTime.Parse(dttodate.Text);
            //            }
            //        }
            //        else
            //        {
            //            from_date = DateTime.Parse(dtfromdate.Text);
            //            to_date = DateTime.Parse(dttodate.Text);
            //        }
            //        ds = eod_pos(from_date, to_date);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "END OF WEEK REPORT FOR POINT OF SALE"://13
            //        way = "Weekly";
            //        from_date = DateTime.Now;
            //        to_date = DateTime.Now; ;
            //        if ((dtfromdate.Text == "") && (dttodate.Text == ""))
            //        {
            //            MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //            return;
            //        }
            //        if ((dtfromdate.Text != "") && (dttodate.Text == ""))
            //        {

            //            if (way == "Daily")
            //            {
            //                from_date = DateTime.Parse(dtfromdate.Text);
            //                to_date = DateTime.Now;
            //            }
            //            else
            //            {
            //                from_date = DateTime.Parse(dtfromdate.Text);
            //                to_date = from_date.AddDays(7);
            //            }
            //        }
            //        else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
            //        {
            //            DateTime FirstDay = new DateTime(DateTime.Now.Year, 1, 1);
            //            if (way == "Daily")
            //            {
            //                from_date = FirstDay;
            //                to_date = DateTime.Parse(dttodate.Text);
            //            }
            //            else
            //            {
            //                from_date = DateTime.Parse(dttodate.Text).AddDays(-7);
            //                to_date = DateTime.Parse(dttodate.Text);
            //            }
            //        }
            //        else
            //        {
            //            from_date = DateTime.Parse(dtfromdate.Text);
            //            to_date = DateTime.Parse(dttodate.Text);
            //        }
            //        ds = eod_pos(from_date, to_date);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "DAILY SALES"://14
            //    case "DAILY SALES (MEMBERSHIP)":
            //        way = "Daily";
            //        from_date = DateTime.Now;
            //        to_date = DateTime.Now; ;
            //        if ((dtfromdate.Text == "") && (dttodate.Text == ""))
            //        {
            //            MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //            return;
            //        }
            //        if ((dtfromdate.Text != "") && (dttodate.Text == ""))
            //        {

            //            if (way == "Daily")
            //            {
            //                from_date = DateTime.Parse(dtfromdate.Text);
            //                to_date = DateTime.Now;
            //            }

            //        }
            //        else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
            //        {
            //            DateTime FirstDay = new DateTime(DateTime.Now.Year, 1, 1);
            //            if (way == "Daily")
            //            {
            //                from_date = FirstDay;
            //                to_date = DateTime.Parse(dttodate.Text);
            //            }

            //        }
            //        else
            //        {
            //            from_date = DateTime.Parse(dtfromdate.Text);
            //            to_date = DateTime.Parse(dttodate.Text);
            //        }
            //        ds = Sales(from_date, to_date);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "WEEKLY SALES"://15
            //    case "WEEKLY SALES (MEMBERSHIP)":
            //        way = "Weekly";
            //        from_date = DateTime.Now;
            //        to_date = DateTime.Now; ;
            //        if ((dtfromdate.Text == "") && (dttodate.Text == ""))
            //        {
            //            MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //            return;
            //        }
            //        if ((dtfromdate.Text != "") && (dttodate.Text == ""))
            //        {

            //            if (way == "Daily")
            //            {
            //                from_date = DateTime.Parse(dtfromdate.Text);
            //                to_date = DateTime.Now;
            //            }
            //            else
            //            {
            //                from_date = DateTime.Parse(dtfromdate.Text);
            //                to_date = from_date.AddDays(7);
            //            }

            //        }
            //        else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
            //        {
            //            DateTime FirstDay = new DateTime(DateTime.Now.Year, 1, 1);
            //            if (way == "Daily")
            //            {
            //                from_date = FirstDay;
            //                to_date = DateTime.Parse(dttodate.Text);
            //            }
            //            else
            //            {
            //                from_date = DateTime.Parse(dttodate.Text).AddDays(-7);
            //                to_date = DateTime.Parse(dttodate.Text);
            //            }

            //        }
            //        else
            //        {
            //            from_date = DateTime.Parse(dtfromdate.Text);
            //            to_date = DateTime.Parse(dttodate.Text);
            //        }
            //        ds = Sales(from_date, to_date);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "DAILY WALKIN"://16
            //        way = "Daily";
            //        from_date = DateTime.Now;
            //        to_date = DateTime.Now; ;
            //        if ((dtfromdate.Text == "") && (dttodate.Text == ""))
            //        {
            //            MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //            return;
            //        }
            //        if ((dtfromdate.Text != "") && (dttodate.Text == ""))
            //        {

            //            if (way == "Daily")
            //            {
            //                from_date = DateTime.Parse(dtfromdate.Text);
            //                to_date = DateTime.Now;
            //            }

            //        }
            //        else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
            //        {
            //            DateTime FirstDay = new DateTime(DateTime.Now.Year, 1, 1);
            //            if (way == "Daily")
            //            {
            //                from_date = FirstDay;
            //                to_date = DateTime.Parse(dttodate.Text);
            //            }

            //        }
            //        else
            //        {
            //            from_date = DateTime.Parse(dtfromdate.Text);
            //            to_date = DateTime.Parse(dttodate.Text);
            //        }
            //        ds = Walkin(from_date, to_date);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "WEEKLY WALKIN"://17
            //        way = "Weekly";
            //        from_date = DateTime.Now;
            //        to_date = DateTime.Now; ;
            //        if ((dtfromdate.Text == "") && (dttodate.Text == ""))
            //        {
            //            MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //            return;
            //        }
            //        if ((dtfromdate.Text != "") && (dttodate.Text == ""))
            //        {

            //            if (way == "Daily")
            //            {
            //                from_date = DateTime.Parse(dtfromdate.Text);
            //                to_date = DateTime.Now;
            //            }
            //            else
            //            {
            //                from_date = DateTime.Parse(dtfromdate.Text);
            //                to_date = from_date.AddDays(7);
            //            }

            //        }
            //        else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
            //        {
            //            DateTime FirstDay = new DateTime(DateTime.Now.Year, 1, 1);
            //            if (way == "Daily")
            //            {
            //                from_date = FirstDay;
            //                to_date = DateTime.Parse(dttodate.Text);
            //            }
            //            else
            //            {
            //                from_date = DateTime.Parse(dttodate.Text).AddDays(-7);
            //                to_date = DateTime.Parse(dttodate.Text);
            //            }

            //        }
            //        else
            //        {
            //            from_date = DateTime.Parse(dtfromdate.Text);
            //            to_date = DateTime.Parse(dttodate.Text);
            //        }
            //        ds = Walkin(from_date, to_date);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "DAILY CONVERSION"://18
            //        way = "Daily";
            //        from_date = DateTime.Now;
            //        to_date = DateTime.Now; ;
            //        if ((dtfromdate.Text == "") && (dttodate.Text == ""))
            //        {
            //            MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //            return;
            //        }
            //        if ((dtfromdate.Text != "") && (dttodate.Text == ""))
            //        {

            //            if (way == "Daily")
            //            {
            //                from_date = DateTime.Parse(dtfromdate.Text);
            //                to_date = DateTime.Now;
            //            }

            //        }
            //        else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
            //        {
            //            DateTime FirstDay = new DateTime(DateTime.Now.Year, 1, 1);
            //            if (way == "Daily")
            //            {
            //                from_date = FirstDay;
            //                to_date = DateTime.Parse(dttodate.Text);
            //            }

            //        }
            //        else
            //        {
            //            from_date = DateTime.Parse(dtfromdate.Text);
            //            to_date = DateTime.Parse(dttodate.Text);
            //        }
            //        ds = Conversion(from_date, to_date);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "WEEKLY CONVERSION"://19
            //        way = "Weekly";
            //        from_date = DateTime.Now;
            //        to_date = DateTime.Now; ;
            //        if ((dtfromdate.Text == "") && (dttodate.Text == ""))
            //        {
            //            MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //            return;
            //        }
            //        if ((dtfromdate.Text != "") && (dttodate.Text == ""))
            //        {

            //            if (way == "Daily")
            //            {
            //                from_date = DateTime.Parse(dtfromdate.Text);
            //                to_date = DateTime.Now;
            //            }
            //            else
            //            {
            //                from_date = DateTime.Parse(dtfromdate.Text);
            //                to_date = from_date.AddDays(7);
            //            }

            //        }
            //        else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
            //        {
            //            DateTime FirstDay = new DateTime(DateTime.Now.Year, 1, 1);
            //            if (way == "Daily")
            //            {
            //                from_date = FirstDay;
            //                to_date = DateTime.Parse(dttodate.Text);
            //            }
            //            else
            //            {
            //                from_date = DateTime.Parse(dttodate.Text).AddDays(-7);
            //                to_date = DateTime.Parse(dttodate.Text);
            //            }

            //        }
            //        else
            //        {
            //            from_date = DateTime.Parse(dtfromdate.Text);
            //            to_date = DateTime.Parse(dttodate.Text);
            //        }

            //        ds = Conversion(from_date, to_date);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "TOTAL SWIPE-IN & MANUAL CHECK-IN'S"://20
            //        way = "Daily";
            //        from_date = DateTime.Now;
            //        to_date = DateTime.Now; ;

            //        from_date = DateTime.Parse(dtfromdate.Text);
            //        to_date = DateTime.Parse(dttodate.Text);

            //        ds = Swipe_Manual_CheckIn(from_date, to_date);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "CLASS, CRECHE AND ALTITUDE BOOKING SUMMARY"://21
            //        way = "Daily";
            //        from_date = DateTime.Now;
            //        to_date = DateTime.Now; ;

            //        from_date = DateTime.Parse(dtfromdate.Text);
            //        to_date = DateTime.Parse(dttodate.Text);
                    
            //        ds = Class_Creche_Altitude(from_date, to_date);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "DAILY CANCELLATIONS"://22
            //        ds = null;
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "AFTER HOURS MEMBERS DOUBLE SWIPE"://23
            //        ds = null;
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "WEEKLY STOCKTAKE"://24
            //        ds = null;
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    default://25
            //        viewreport_sub2(_reportName);
            //        break;
            //}

            //if (_reportName != "" && datasetcheck == 1)
            //{
            //    datasetcheck = 0;
            //    rptView.lvReportView.ItemsSource = null;
            //    rptView.lvReportView.ItemsSource = ds.Tables[0].AsDataView();
            //    rptView.printds = rptView.lvReportView;
            //    rptView.Owner = this;
            //    rptView.Title = _reportName;
            //    rptView.ShowDialog();
            //    clearcontrols();
            //}
            //////REPORT EMAIL CONFIG. SETTINGS SCREEN - VIEW BUTTON NOT REQUIRED
        }
        public void viewreport_sub2(string report_name)
        {
            //Report_SubCode rsub = new Report_SubCode();
            //int status;
            //int Gender;
            //switch (report_name.ToUpper())
            //{
            //    case "CURRENT VISITING MEMBERS"://1
            //        ds = null;
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "EXPIRING MEMBERS"://2
            //    case "EXPIRY CONTRACTS":
            //        ds = Expiring_Memebers();
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "FUTURE MEMBERS"://3
            //        ds = null;
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "LOST MEMBERS"://4
            //        ds = null;
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "MEMBER MADE BOOKING"://5
            //        ds = Member_Made_Booking(DateTime.Parse(dtfromdate.Text), DateTime.Parse(dttodate.Text));
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "MEMBER NO BOOKING PERIOD"://6
            //        ds = null;
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "MEMEBER NO TRAINER"://7
            //        ds = Memeber_No_trainer();
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "MEMEBERS FROM PROSPECTS"://8
            //    case "DIRECT DEBIT UPGRADES/DOWNGRADES":
            //    case "BREAKDOWN BY TYPE":
            //        ds = null;
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "NO ACTIVE MEMBERSHIP"://9 Done by Ali @ 29/07/16
            //    //case "CANCELLED CONTRACTS": // Added by Iqbal (only this line)  30/07/16
            //        ds = NoActiveMembership();
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "CANCELLED CONTRACTS": // Added by Iqbal (only this line)  30/07/16
            //        ds = CanceledContracts();
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "MEMBERSHIP HOLD LENGTH": //10 Done by Ali @ 29/07/16
            //        status = 0;
            //        ds = MembershipHoldLength(status);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "STARTING MEMBERSHIPS": //11 Done by Ali @ 29/07/16
            //        from_date = DateTime.Parse(dtfromdate.Text);
            //        to_date = DateTime.Parse(dttodate.Text);
            //        ds = StartingMemberships(from_date, to_date);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "SUSPENDED MEMBERSHIPS"://12 //Ali
            //    case "SUSPENDED CONTRACTS": // Added by Iqbal (only this line)  30/07/16
            //        status = 0;
            //        ds = MembershipHoldLength(status);
            //        datasetcheck = Emptydataset(ds);
            //        break;
                
            //    case "NEW CONTRACTS"://13
            //        String way = "Daily";
            //        from_date = DateTime.Now;
            //        to_date = DateTime.Now;
            //        from_date = DateTime.Parse(dtfromdate.Text);
            //        to_date = DateTime.Parse(dttodate.Text);
            //        ds = NewContracts(from_date, to_date);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "RENEWED CONTRACTS"://14
            //    case "LINKED MEMBERSHIP RENEWAL":
            //    case "RE-JOIN CONTRACTS":
            //    case "NEW MEMBERS":
            //        ds = null;
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "MEMBERS WITH MULTIPLE CONTRACTS"://15
            //        ds = rsub.Member_Multiple_Contracts();
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "CURRENT CONTRACTS"://16
            //        ds = rsub.Current_Contracts();
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "LENGTH OF TIME AS A MEMBER"://17
            //        ds =rsub.LengthofMember();
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "BREAKDOWN BY GENDER": //18
            //        if (cmbGender.SelectedIndex == 0)
            //        {
            //            Gender = 3;
            //        }
            //        else if (cmbGender.SelectedIndex == 1)
            //        {
            //            Gender = 1;
            //        }
            //        else
            //        {
            //            Gender = 2;
            //        }
            //            ds = rsub.BreakdownbyGender(Gender);
            //        datasetcheck = Emptydataset(ds);
            //        //clearcontrols();
            //        break;
            //    case "ALL MEMBER BOOKINGS":
            //        from_date = DateTime.Now;
            //        to_date = DateTime.Now; ;
            //        from_date = DateTime.Parse(dtfromdate.Text);
            //        to_date = DateTime.Parse(dttodate.Text);
            //        string BookedStatus = string.Empty;
            //        //int i = 0;
            //        if (chkBooked.IsChecked == true && chkWaiting.IsChecked == true && chkCancelled.IsChecked == true)
            //        {
            //            BookedStatus = "B,W,C";
            //        }
            //        else if(chkBooked.IsChecked == true && chkWaiting.IsChecked == true)
            //        {
            //            BookedStatus = "B,W";
            //        }
            //        else if (chkBooked.IsChecked == true && chkCancelled.IsChecked == true)
            //        {
            //            BookedStatus = "B,C";
            //        }
            //        else if (chkWaiting.IsChecked == true && chkCancelled.IsChecked == true)
            //        {
            //            BookedStatus = "W,C";
            //        }
            //        else if(chkBooked.IsChecked==true)
            //        {
            //            BookedStatus = "B";

            //        }
            //        else if(chkWaiting.IsChecked == true)
            //        {
            //            BookedStatus = "W";
            //        }
            //        else if(chkCancelled.IsChecked == true)
            //        {
            //            BookedStatus = "C";
            //        }
            //        else
            //        {
            //            BookedStatus = "B,W,C";
            //        }
            //        ds = BookingDetails(from_date, to_date,BookedStatus);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "ALL PAYMENTS":
            //        from_date = DateTime.Now;
            //        to_date = DateTime.Now; ;
            //        from_date = DateTime.Parse(dtfromdate.Text);
            //        to_date = DateTime.Parse(dttodate.Text);
            //        ds = AllPayments(from_date, to_date);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "ALL PRODUCTS":
            //        status = 0;
            //        ds = Products(status);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "ALL VOIDED PAYMENTS":
            //        from_date = DateTime.Now;
            //        to_date = DateTime.Now; ;
            //        from_date = DateTime.Parse(dtfromdate.Text);
            //        to_date = DateTime.Parse(dttodate.Text);
            //        ds = AllVoidedPayments(from_date, to_date);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "ALL SALES":
            //        ds = null;
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "INVENTORY STOCK LEVEL":
            //        status = 1;
            //        ds = Products(status);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "RESOURCE SUMMARY":
            //        ds = null;
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "ACTIONS TAKEN":
            //        ds = AllTasksReport(2);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "ALL COMPLETED TASK":
            //        ds = AllTasksReport(4);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "ALL OUTSTANDING TASK":
            //        ds = AllTasksReport(1);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "ALL WORKED ON TASK":
            //        ds = AllTasksReport(2);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "CORRESPONDENCE SENT":
            //        ds = CorrespondenceSent("PROSPECT");
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "NOT VISITING":
            //        from_date = DateTime.Parse(dtfromdate.Text);
            //        to_date = DateTime.Parse(dttodate.Text);
            //        ds = NotVisited(from_date, to_date);
            //        datasetcheck = Emptydataset(ds); 
            //        break;
            //    case "VISITOR LOG":
            //         from_date = DateTime.Parse(dtfromdate.Text);
            //        to_date = DateTime.Parse(dttodate.Text);
            //        ds = VisitorLog(from_date, to_date);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "VISITS IN PERIOD":
            //        from_date = DateTime.Parse(dtfromdate.Text);
            //        to_date = DateTime.Parse(dttodate.Text);
            //        ds = Visitsinperiod(from_date, to_date);
            //        datasetcheck = Emptydataset(ds);
            //        break; 
            //    case "RECENTLY NOT VISITING":
            //        from_date = DateTime.Parse(dttodate.Text).AddDays(-21);
            //        to_date = DateTime.Parse(dttodate.Text);
            //        ds = RecentlyNotVisited(from_date, to_date);
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "VISITS PER DAY":
            //        ds = null;
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "FINANCIAL":
            //        ds = null;
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "OUTSTANDING ACCOUNTS":
            //        ds = null;
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    case "ATTENDANCE":
            //        ds = null;
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //    default:
            //        ds = null;
            //        datasetcheck = Emptydataset(ds);
            //        break;
            //}
        }
        private DataSet getMemberReport()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("GetALLMemberDetails_Report", ht);
            return dsgetM;
        }
        private DataSet getMemberPaymentReport()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("GetALLMemberPayments_Report", ht);
            return dsgetM;
        }
        private DataSet getProspectReport()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("GetALLProspects_Report", ht);
            return dsgetM;
        }
        private DataSet getwithouthistoricalmemeber()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_ExcludingHistoricalMembers", ht);
            return dsgetM;
        }
        private DataSet CancellingMemeber()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_CancellingMembers", ht);
            return dsgetM;
        }
        private DataSet getAllmemeber()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_IncludingHistoricalMembers", ht);
            return dsgetM;
        }
        private DataSet getDirectDebitMember()
        {
            //SqlHelper sql = new SqlHelper();
            //Hashtable ht = new Hashtable();
            //DataSet dsgetM = new DataSet();
            //ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            //ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            //if (cmbpaymentmode.Text == "All Payment Mode")
            //{
            //    ht.Add("@paymode", cmbpaymentmode.Text);
            //    ht.Add("@PaymentModeId", 1);
            //}
            //else if ((cmbpaymentmode.Text == "Bank Account") || (cmbpaymentmode.Text == "Credit Card"))
            //{
            //    ht.Add("@paymode", cmbpaymentmode.Text);
            //    ht.Add("@PaymentModeId", 2);
            //}
            //dsgetM = sql.ExecuteProcedure("Rpt_DDMembership", ht);
            //return dsgetM;

            return null;
        }
        private DataSet getBillingHistory()
        {
            //SqlHelper sql = new SqlHelper();
            //Hashtable ht = new Hashtable();
            //ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            //ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            //ht.Add("@fromdate", DateTime.Parse(dtfromdate.Text));
            //ht.Add("@todate", DateTime.Parse(dttodate.Text));
            //DataSet dsgetM = sql.ExecuteProcedure("Rpt_Billing_Transaction_History", ht);
            //return dsgetM;

            return null;
        }
        private DataSet getBirthdays()
        {
            //SqlHelper sql = new SqlHelper();
            //Hashtable ht = new Hashtable();
            //ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            //ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            //ht.Add("@from_date", DateTime.Parse(dtfromdateBirth.Text));
            //ht.Add("@to_date", DateTime.Parse(dttodateBirth .Text));
            //DataSet dsgetM = sql.ExecuteProcedure("Rpt_BirthdayList", ht);
            //return dsgetM;

            return null;
        }
        private DataSet getCurrentMembers()
        {
            //SqlHelper sql = new SqlHelper();
            //Hashtable ht = new Hashtable();
            //if (cmbgroup.Text != "")
            //{
            //    ht.Add("@programmegroup", Convert.ToInt32(cmbgroup.SelectedValue.ToString()));
            //}
            //else
            //{
            //    ht.Add("@programmegroup","");
            //}
            
            //ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            //ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            //ht.Add("@programme", cmbprogram.Text);
            //DataSet dsgetM = sql.ExecuteProcedure("Rpt_CurrentMembers", ht);
            //return dsgetM;

            return null;
        }
        private DataSet eod_pos(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);

            DataSet dsgetM = sql.ExecuteProcedure("POS_EOD", ht);
            return dsgetM;
        }
        private DataSet Sales(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);

            DataSet dsgetM = sql.ExecuteProcedure("Rpt_Sales", ht);
            return dsgetM;
        }
        private DataSet Walkin(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);

            DataSet dsgetM = sql.ExecuteProcedure("Rpt_Walkin", ht);
            return dsgetM;
        }
        private DataSet Conversion(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);

            DataSet dsgetM = sql.ExecuteProcedure("Rpt_Conversion", ht);
            return dsgetM;
        }
        private DataSet Swipe_Manual_CheckIn(DateTime fromdate, DateTime todate)
        {
            //SqlHelper sql = new SqlHelper();
            //Hashtable ht = new Hashtable();

            //ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            //ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            //ht.Add("@fromdate", fromdate);
            //ht.Add("@todate", todate);
            //ht.Add("@chkintype", cmbchkintype.Text);
            //DataSet dsgetM = sql.ExecuteProcedure("Rpt_Swipe_CheckIn", ht);
            //return dsgetM;

            return null;
        }
        private DataSet Class_Creche_Altitude(DateTime fromdate, DateTime todate)
        {
            //SqlHelper sql = new SqlHelper();
            //Hashtable ht = new Hashtable();

            //ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            //ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            //ht.Add("@fromdate", fromdate);
            //ht.Add("@todate", todate);
            //ht.Add("@ResourceTypeName", cmbresourcetype.Text);
            //ht.Add("@RoomOrTrainer", cmbroomortrainer.Text);
            //DataSet dsgetM = sql.ExecuteProcedure("Rpt_Class_Creche", ht);
            //return dsgetM;

            return null;
        }
        private DataSet Member_Made_Booking(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_MemberMadeBooking", ht);
            return dsgetM;
        }
        private DataSet Expiring_Memebers()
        {
            //SqlHelper sql = new SqlHelper();
            //Hashtable ht = new Hashtable();

            //if (cmbgroup.Text != "")
            //{
            //    ht.Add("@programmegroup", Convert.ToInt32(cmbgroup.SelectedValue.ToString()));
            //}
            //else
            //{
            //    ht.Add("@programmegroup", "");
            //}

            //if (cmbprogram.Text != "")
            //{
            //    ht.Add("@Programme", Convert.ToInt32(cmbprogram.SelectedValue.ToString()));
            //}
            //else
            //{
            //    ht.Add("@Programme", "");
            //}

            //ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            //ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            //ht.Add("@fromdate",DateTime.Parse(dtfromdate.Text));
            //ht.Add("@todate",DateTime.Parse(dttodate.Text));
            //DataSet dsgetM = sql.ExecuteProcedure("Rpt_ExpiringMemebers", ht);
            //return dsgetM;

            return null;
        }
        private DataSet Memeber_No_trainer()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_MemberNoTrainer", ht);
            return dsgetM;
        }
        private DataSet NoActiveMembership() //Ali 29/07/16
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_NoActiveMembership", ht);
            return dsgetM;
        }
        private DataSet CanceledContracts() //Ali 29/07/16
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_CanceledContracts", ht);
            return dsgetM;
        }
        private DataSet MembershipHoldLength(int Status)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@Status", Status);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_MembershipHoldLength", ht);
            return dsgetM;
        }
        private DataSet StartingMemberships(DateTime fromdate, DateTime todate) //Ali 29/07/16
        {
            //SqlHelper sql = new SqlHelper();
            //Hashtable ht = new Hashtable();
            //if (cmbgroup.Text != "")
            //{
            //    ht.Add("@programmegroup", Convert.ToInt32(cmbgroup.SelectedValue.ToString()));
            //}
            //else
            //{
            //    ht.Add("@programmegroup", "");
            //}

            //if (cmbprogram.Text != "")
            //{
            //    ht.Add("@Programme", Convert.ToInt32(cmbprogram.SelectedValue.ToString()));
            //}
            //else
            //{
            //    ht.Add("@Programme", "");
            //}

            //ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            //ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            //ht.Add("@fromdate", fromdate);
            //ht.Add("@todate", todate);
            //DataSet dsgetM = sql.ExecuteProcedure("Rpt_StartingMemberships", ht);
            //return dsgetM;

            return null;
        }
        private DataSet NewContracts(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);

            DataSet dsgetM = sql.ExecuteProcedure("Rpt_NewContracts", ht);
            return dsgetM;
        }
        private DataSet BookingDetails(DateTime fromdate, DateTime todate,String BookedStatus)
        {
            //SqlHelper sql = new SqlHelper();
            //Hashtable ht = new Hashtable();

            //ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            //ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            //ht.Add("@fromdate", fromdate);
            //ht.Add("@todate", todate);
            //ht.Add("@ResourceTypeName", cmbresourcetype.Text);
            //ht.Add("@RoomOrTrainer", cmbroomortrainer.Text);
            //ht.Add("@BookingStatus", BookedStatus);
            //DataSet dsgetM = sql.ExecuteProcedure("Rpt_BookingDetails", ht);
            //return dsgetM;

            return null;
        }
        private DataSet AllPayments(DateTime fromdate, DateTime todate)
        {
            //SqlHelper sql = new SqlHelper();
            //Hashtable ht = new Hashtable();

            //ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            //ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            //ht.Add("@fromdate", fromdate);
            //ht.Add("@todate", todate);
            //DataSet dsgetM = sql.ExecuteProcedure("Rpt_AllPayments", ht);
            //return dsgetM;

            return null;
        }
        private DataSet AllVoidedPayments(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_AllVoidedPayments", ht);
            return dsgetM;
        }
        private DataSet Products(int status)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@Status", Convert.ToInt32(status).ToString());
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

            DataSet dsgetM = sql.ExecuteProcedure("Rpt_Products", ht);
            return dsgetM;
        }
        private DataSet VisitorLog(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_VisitorLog", ht);
            return dsgetM;
        }
        private DataSet Visitsinperiod(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_VisitsInPeriod", ht);
            return dsgetM;
        }
        private DataSet NotVisited(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_NotVisited", ht);
            return dsgetM;
        }
        private DataSet RecentlyNotVisited(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_RecentlyNotVisited", ht);
            return dsgetM;
        }
        private DataSet AllTasksReport(int _TaskStatus = 4)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@Status", _TaskStatus);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_AllTasksReport", ht);
            return dsgetM;
        }
        private DataSet CorrespondenceSent(string _receiverType = "PROSPECT")
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@receiverType", _receiverType);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_CorrespondenceSent", ht);
            return dsgetM;
        }
        private void gvReport_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                int i = gvReport.SelectedIndex;
                if (i != -1)
                {
                    ClearFields();

                    _ReportSerialNum = Convert.ToInt32((gvReport.SelectedItem as DataRowView).Row["ReportSerialNum"].ToString());

                    if (_ReportSerialNum > 0)
                    {
                        if (_dtCurrSiteConfig != null)
                        {
                            _dtCurrSiteConfig.DefaultView.RowFilter = "";
                            if (_dtCurrSiteConfig.DefaultView.Count > 0)
                            {
                                _dtCurrSiteConfig.DefaultView.RowFilter = "ReportTypeSeqID = " + _ReportSeqID + " AND ReportSerialNum = " + _ReportSerialNum;
                                if (_dtCurrSiteConfig.DefaultView.Count > 0)
                                {
                                    //SET FREQUENCY
                                    if (_dtCurrSiteConfig.DefaultView[0]["Frequency"].ToString().Trim().ToUpper() == "D")
                                    {
                                        rbuttonDailyFreq.IsChecked = true;
                                    }
                                    else if (_dtCurrSiteConfig.DefaultView[0]["Frequency"].ToString().Trim().ToUpper() == "W")
                                    {
                                        rbuttonWeeklyFreq.IsChecked = true;
                                    }
                                    else if (_dtCurrSiteConfig.DefaultView[0]["Frequency"].ToString().Trim().ToUpper() == "M")
                                    {
                                        rbuttonMonthlyFreq.IsChecked = true;
                                    }

                                    //SET CURR/ALL SITES
                                    if (Convert.ToInt32(_dtCurrSiteConfig.DefaultView[0]["currSiteCoalesce"].ToString()) == 0)
                                    {
                                        rbuttonAllSites.IsChecked = true;
                                    }
                                    else if (Convert.ToInt32(_dtCurrSiteConfig.DefaultView[0]["currSiteCoalesce"].ToString()) == 1)
                                    {
                                        rbuttonCurrentSite.IsChecked = true;
                                    }

                                    //SET EMAIL ID
                                    txtEmailID.Text = _dtCurrSiteConfig.DefaultView[0]["emailIDs"].ToString();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string _msg = ex.Message;
            }
        }
        private void HideControls()
        {
        }
        private void cmbgroup_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            //try
            //{
            //    Utilities _utilites = new Utilities();

            //    _programmeGroupId = Convert.ToInt32(cmbgroup.SelectedValue);
            //    _utilites.get_ProgramByGroup(cmbprogram, _programmeGroupId);
            //}
            //catch (Exception ex)
            //{

            //}
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
            GetReportTypesAndList();
            GetReportEmailConfigDetails();
            btnmemberReports_Click(sender, e);
        }
        private void GetReportEmailConfigDetails()
        {
            _dsCurrSiteConfig = null;
            _dtCurrSiteConfig = null;

            SqlHelper _Sql = new SqlHelper();
            if (_Sql._ConnOpen == 0)
                return;
            
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

            _dsCurrSiteConfig = _Sql.ExecuteProcedure("GetReportEmailConfigDetails", ht);

            if (_dsCurrSiteConfig != null)
            {
                if (_dsCurrSiteConfig.Tables.Count > 0)
                {
                    _dtCurrSiteConfig = _dsCurrSiteConfig.Tables[0];
                }
            }
        }
        private void GetReportTypesAndList()
        {
            _ReportSeqID = 0;
            SqlHelper _Sql = new SqlHelper();
            if (_Sql._ConnOpen == 0)
                return;

            Hashtable ht = new Hashtable();
            _dsReportTypeList = _Sql.ExecuteProcedure("GetReportTypesAndList", ht);

            if (_dsReportTypeList != null)
            {
                if (_dsReportTypeList.Tables.Count > 0)
                {
                    _dtReportType = _dsReportTypeList.Tables[0];
                    if (_dsReportTypeList.Tables.Count > 1)
                    {
                        _dtReportList = _dsReportTypeList.Tables[1];
                    }

                    if (_dtReportType.DefaultView.Count > 0)
                    {
                        //REPORT TYPE NAMES -> ASSIGN TO LEFT PANEL BUTTONS
                        for (int i = 0; i < _dtReportType.DefaultView.Count; i++)
                        {
                            _ReportSeqID = Convert.ToInt32(_dtReportType.DefaultView[i]["ReportSeqID"]);
                            switch (_ReportSeqID)
                            {
                                case 1:
                                    lblRpTypeMember.Content = _dtReportType.DefaultView[i]["ReportTypeName"].ToString();
                                    break;
                                case 2:
                                    lblRpTypeSales.Content = _dtReportType.DefaultView[i]["ReportTypeName"].ToString();
                                    break;
                                case 3:
                                    lblRpTypeProspects.Content = _dtReportType.DefaultView[i]["ReportTypeName"].ToString();
                                    break;
                                case 4:
                                    lblRpTypeBooking.Content = _dtReportType.DefaultView[i]["ReportTypeName"].ToString();
                                    break;
                                case 5:
                                    lblRpTypeFinancial.Content = _dtReportType.DefaultView[i]["ReportTypeName"].ToString();
                                    break;
                                case 6:
                                    lblRpTypeRetention.Content = _dtReportType.DefaultView[i]["ReportTypeName"].ToString();
                                    break;
                                case 7:
                                    lblRpTypePOS.Content = _dtReportType.DefaultView[i]["ReportTypeName"].ToString();
                                    break;
                                default:
                                    break;
                            }
                        }
                        _ReportSeqID = 0;
                    }
                    
                }
            }
        }
        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;
        }
        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void clearcontrols()
        {
            //cmbresourcetype.SelectedIndex = -1;
            //cmbpaymentmode.SelectedIndex = 0;
            //cmbchkintype.SelectedIndex = 0;
            //cmbGender.SelectedIndex = 0;
            //cmbgroup.Text = "";
            //cmbprogram.Text = "";
            //cmbroomortrainer.Text = "";
            //chkBooked.IsChecked = false;
            //chkCancelled.IsChecked = false;
            //chkWaiting.IsChecked = false;
            //dtfromdate.SelectedDate = DateTime.Now.Date;
            //dttodate.SelectedDate = DateTime.Now.Date;
            //dtfromdateBirth.SelectedDate = DateTime.Now.Date;
            //dttodateBirth.SelectedDate = DateTime.Now.Date;
        }
        private void btnSalesReports_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnmemberReports.Background = Brushes.Gray;
                btnSalesReports.Background = Brushes.Chocolate;
                btnProspectReports.Background = Brushes.Gray;
                btnBookingReports.Background = Brushes.Gray;
                btnFinancialReports.Background = Brushes.Gray;
                btnRetentionReports.Background = Brushes.Gray;
                btnPOSReports.Background = Brushes.Gray;

                HideControls();
                populatedata();

                _ReportSeqID = 2;
                SetReportList(_ReportSeqID);
            }
            catch (Exception ex)
            {

            }
        }
        private void btnProspectReports_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnmemberReports.Background = Brushes.Gray;
                btnSalesReports.Background = Brushes.Gray;
                btnProspectReports.Background = Brushes.Chocolate;
                btnBookingReports.Background = Brushes.Gray;
                btnFinancialReports.Background = Brushes.Gray;
                btnRetentionReports.Background = Brushes.Gray;
                btnPOSReports.Background = Brushes.Gray;

                HideControls();
                populatedata();

                _ReportSeqID = 3;
                SetReportList(_ReportSeqID);
            }
            catch (Exception ex)
            {

            }
        }
        private void cmbresourcetype_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (cmbresourcetype.SelectedIndex != -1)
            //{
            //    SqlHelper sql = new SqlHelper();
            //    Hashtable ht = new Hashtable();
            //    int index = cmbresourcetype.SelectedIndex;
            //    int selectedresourcetype = int.Parse(cmbresourcetype.SelectedValue.ToString());
            //    //string selectedresourcetype = cmbresourcetype.Text;
            //    ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            //    ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            //    ht.Add("@ResourceTypeID", selectedresourcetype);
            //    DataSet dsresourcetype = sql.ExecuteProcedure("Booking_Room_Trainer", ht);
            //    if (dsresourcetype != null)
            //    {
            //        if (dsresourcetype.Tables[0].DefaultView != null)
            //        {
            //            try
            //            {
            //                cmbroomortrainer.ItemsSource = dsresourcetype.Tables[0].DefaultView;
            //                cmbroomortrainer.DisplayMemberPath = "RoomOrTrainer";

            //            }
            //            catch (Exception)
            //            { }
            //        }
            //    }
            //}
        }
        private void btnBookingReports_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnmemberReports.Background = Brushes.Gray;
                btnSalesReports.Background = Brushes.Gray;
                btnProspectReports.Background = Brushes.Gray;
                btnBookingReports.Background = Brushes.Chocolate;
                btnFinancialReports.Background = Brushes.Gray;
                btnRetentionReports.Background = Brushes.Gray;
                btnPOSReports.Background = Brushes.Gray;

                HideControls();
                populatedata();

                _ReportSeqID = 4;
                SetReportList(_ReportSeqID);
            }
            catch (Exception ex)
            {

            }
        }
        private void btnFinancialReports_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnmemberReports.Background = Brushes.Gray;
                btnSalesReports.Background = Brushes.Gray;
                btnProspectReports.Background = Brushes.Gray;
                btnBookingReports.Background = Brushes.Gray;
                btnFinancialReports.Background = Brushes.Chocolate;
                btnRetentionReports.Background = Brushes.Gray;
                btnPOSReports.Background = Brushes.Gray;

                HideControls();
                populatedata();

                _ReportSeqID = 5;
                SetReportList(_ReportSeqID);
            }
            catch (Exception ex)
            {

            }
        }
        private void dtfromdate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //int i = gvReport.SelectedIndex;
            //if (i != -1)
            //{
            //    if (DateTime.Parse(dtfromdate.Text) > DateTime.Parse(dttodate.Text))
            //    {
            //        dttodate.SelectedDate = dtfromdate.SelectedDate;
            //    }
            //    dttodate.DisplayDateStart = dtfromdate.SelectedDate;
            //}
        }
        private void dtfromdate_GotMouseCapture(object sender, MouseEventArgs e)
        {
            //if (!dtfromdate.IsDropDownOpen)
            //{
            //    dtfromdate.IsDropDownOpen = true;
            //    //UIElement originalElement = e.OriginalSource as UIElement;
            //    //if (originalElement != null)
            //    //{
            //    //    originalElement.ReleaseMouseCapture();
            //    //}
            //    //Mouse.Capture(null);

            //}
            //else
            //{
            //    dtfromdate.IsDropDownOpen = false;
            //}
            ////UIElement originalElement = e.OriginalSource as UIElement;
            ////if (originalElement != null)
            ////{
            ////    originalElement.ReleaseMouseCapture();
            ////    Mouse.Capture(null);
            ////}
            //if (Mouse.Captured is Calendar || Mouse.Captured is System.Windows.Controls.Primitives.CalendarItem)
            //{
            //    Mouse.Capture(null);
            //}
        }
        private void dtfromdateBirth_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //int i = gvReport.SelectedIndex;
            //if (i != -1)
            //{
            //    string _reportName = gvReport.Items[i].ToString();
            //    if (_reportName == "Birthday List")
            //    {
            //        dttodateBirth.DisplayDateEnd = new DateTime(DateTime.Now.Year, 12, 31);
            //        dtfromdateBirth.DisplayDateStart = new DateTime(DateTime.Now.Year, 1, 1);
            //        dtfromdateBirth.DisplayDateEnd = new DateTime(DateTime.Now.Year, 12, 31);
            //        if (DateTime.Parse(dtfromdateBirth.Text) > DateTime.Parse(dttodateBirth.Text))
            //        {
            //            dttodateBirth.SelectedDate = dtfromdateBirth.SelectedDate;
            //        }
            //        dttodateBirth.DisplayDateStart = dtfromdateBirth.SelectedDate;
            //    }
            //}
        }
        private void btnRetentionReports_Click(object sender, RoutedEventArgs e)
        {
            btnmemberReports.Background = Brushes.Gray;
            btnSalesReports.Background = Brushes.Gray;
            btnProspectReports.Background = Brushes.Gray;
            btnBookingReports.Background = Brushes.Gray;
            btnFinancialReports.Background = Brushes.Gray;
            btnRetentionReports.Background= Brushes.Chocolate;
            btnPOSReports.Background = Brushes.Gray;

            HideControls();
            populatedata();

            _ReportSeqID = 6;
            SetReportList(_ReportSeqID);
        }

        private void btnPOSReports_Click(object sender, RoutedEventArgs e)
        {
            btnmemberReports.Background = Brushes.Gray;
            btnSalesReports.Background = Brushes.Gray;
            btnProspectReports.Background = Brushes.Gray;
            btnBookingReports.Background = Brushes.Gray;
            btnFinancialReports.Background = Brushes.Gray;
            btnRetentionReports.Background = Brushes.Gray;
            btnPOSReports.Background = Brushes.Chocolate;

            HideControls();
            populatedata();

            _ReportSeqID = 7;
            SetReportList(_ReportSeqID);
        }

        private void btnSaveConfig_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateData())
            {
                SaveReportEmailConfigDetails();
                MessageBox.Show("Email Configurations saved!", "Auto Email Config", MessageBoxButton.OK, MessageBoxImage.Information);
                ClearFields();
                GetReportEmailConfigDetails();
                gvReport.SelectedIndex = -1;
            }
        }
        private void ClearFields()
        {
            rbuttonAllSites.IsChecked = false;
            rbuttonCurrentSite.IsChecked = false;
            txtEmailID.Text = "";
            rbuttonDailyFreq.IsChecked = false;
            rbuttonWeeklyFreq.IsChecked = false;
            rbuttonMonthlyFreq.IsChecked = false;
        }
        private void SaveReportEmailConfigDetails()
        {
            SqlHelper _Sql = new SqlHelper();
            if (_Sql._ConnOpen == 0)
                return;

            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@ReportTypeSeqID", _ReportSeqID);
            ht.Add("@ReportSerialNum", _ReportSerialNum);
            ht.Add("@Frequency", _Frequency);
            ht.Add("@currSite", _currSite);
            ht.Add("@userName", ConfigurationManager.AppSettings["LoggedInUser"].ToString());
            ht.Add("@emailIDs", _emailIDs);

            _dsCurrSiteConfig = _Sql.ExecuteProcedure("SaveReportEmailConfigDetails", ht);
        }
        private bool ValidateData()
        {
            //REPORT SELECTION VALIDATION
            if (gvReport.SelectedIndex <= -1)
            {
                System.Windows.MessageBox.Show("Please select a Report from the list!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            //FREQUENCY
            if ((rbuttonDailyFreq.IsChecked == false) && (rbuttonMonthlyFreq.IsChecked == false) && (rbuttonWeeklyFreq.IsChecked == false))
            {
                System.Windows.MessageBox.Show("Please select Frequency for auto emailing of report!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            //SITE SELECTION
            if ((rbuttonAllSites.IsChecked == false) && (rbuttonCurrentSite.IsChecked == false))
            {
                System.Windows.MessageBox.Show("Please select Site option (only for this site OR All sites) ?", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            //EMAIL ID VALIDATION
            if (txtEmailID.Text.Trim() != "")
            {
                if ((!txtEmailID.Text.Contains("@")) || (!txtEmailID.Text.Contains(".")))
                {
                    System.Windows.MessageBox.Show("Email ID not in correct Format!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtEmailID.Focus();
                    return false;
                }
                else if (txtEmailID.Text.Trim().LastIndexOf("@") != txtEmailID.Text.Trim().IndexOf("@"))
                {
                    System.Windows.MessageBox.Show("Email ID not in correct Format!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtEmailID.Focus();
                    return false;
                }
                else if (txtEmailID.Text.Trim().Length < 6)
                {
                    System.Windows.MessageBox.Show("Email ID not in correct Format!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtEmailID.Focus();
                    return false;
                }
            }
            else
            {
                System.Windows.MessageBox.Show("Email ID cannot be Blank!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtEmailID.Focus();
                return false;
            }

            //AFTER VALIDATION, SET THE CURRENT VALUES
            //FREQUENCY
            if (rbuttonDailyFreq.IsChecked == true)
            {
                _Frequency = "D";
            }
            else if (rbuttonWeeklyFreq.IsChecked == true)
            {
                _Frequency = "W";
            }
            if (rbuttonMonthlyFreq.IsChecked == true)
            {
                _Frequency = "M";
            }

            //CURRENT (OR) ALL SITES
            if (rbuttonCurrentSite.IsChecked == true)
            {
                _currSite = 1;
            }
            else if (rbuttonAllSites.IsChecked == true)
            {
                _currSite = 0;
            }

            //EMAIL IDS
            _emailIDs = txtEmailID.Text;

            return true;
        }
    }
}

