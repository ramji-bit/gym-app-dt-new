﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Windows.Xps.Packaging;
using System.Windows.Xps;
using System.Diagnostics;
using System.Windows.Navigation;
using System.Data;

namespace GymMembership.Reports
{
    /// <summary>
    /// Interaction logic for ReportView.xaml
    /// </summary>
    public partial class ReportView 
    {
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;

        //public DataSet printds = new DataSet();
        public DataGrid printds = new DataGrid();
        public int c;
        public ReportView()
        {
            InitializeComponent();
        }

        //public ReportView(DataSet dss)
        //{
        //    a = dss;
        //    int b=a.Tables[0].Rows.Count;
        //    c= a.Tables[0].Columns.Count;
        //}
        

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
          
            this.Close();
        }

        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGrid dg = lvReportView;
                dg.SelectAllCells();
                dg.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
                ApplicationCommands.Copy.Execute(null, dg);
                dg.UnselectAllCells();
                string Clipboardresult = (string)Clipboard.GetData(DataFormats.CommaSeparatedValue);
                StreamWriter swObj = new StreamWriter("exportToExcel.csv");
                swObj.WriteLine(Clipboardresult);
                swObj.Close();
                Process.Start("exportToExcel.csv");
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
            txtblkReportName.Text = this.Title;
        }
        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto(_StaffPic);
        }
        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            Print _print = new Print(printds,null,this);
        }
    }
}
