﻿using System;
using System.Data;
using System.Collections;
using System.Configuration;

namespace GymMembership.Reports
{
    public class Report_SubCode
    {
        public DataSet Member_Multiple_Contracts()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_Member_Multiple_Contracts", ht);
            return dsgetM;
        }
        public DataSet Current_Contracts()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_CurrentContracts", ht);
            return dsgetM;
        }
        public DataSet BreakdownbyGender(int Gender)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@Gender", Gender);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_BreakDownByGender", ht);
            return dsgetM;
        }
        public DataSet LengthofMember()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_LengthofTimeasaMember", ht);
            return dsgetM;
        }
    }
}
