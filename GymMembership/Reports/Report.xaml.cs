﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GymMembership.Comman;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Windows.Controls.Primitives;
namespace GymMembership.Reports
{
    /// <summary>
    /// Interaction logic for Report.xaml
    /// </summary>
    public partial class Report
    {
        private int _programmeGroupId = 0;
        String way = "";
        int datasetcheck;
        string _reportName = "";
        DateTime from_date = DateTime.Now;
        DateTime to_date = DateTime.Now;
        //int z=0;
        DataSet ds = new DataSet();
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;

        ReportView rptView = new ReportView();
        public Report()
        {
            InitializeComponent();
        }

        public void populatedata()
        {
            Utilities _utilites = new Utilities();
            _utilites.populateProgramGroup(cmbgroup);
            _utilites.bookingresourcetype(cmbresourcetype);
            _utilites.populateAllProducts(cmbProduct);
        }
        private int Emptydataset(DataSet ds)
        {
            int value;
            if (ds == null)
            {
                MessageBox.Show("Report Does not contain any data", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                value = 0;
            }
            else if (ds.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("Report Does not contain any data", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                value = 0;
            }
            else
            {
                value = 1;
            }
            return value;

        }
        private void btnmemberReports_Click(object sender, RoutedEventArgs e)
        {
            btnmemberReports.Background = Brushes.Chocolate;
            btnSalesReports.Background = Brushes.Gray;
            btnProspectReports.Background = Brushes.Gray;
            btnBookingReports.Background = Brushes.Gray;
            btnFinancialReports.Background = Brushes.Gray;
            btnRetentionReports.Background = Brushes.Gray;
            btnPOSReports.Background = Brushes.Gray;

            HideControls();
            populatedata();
            gvReport.Items.Clear();
            gvReport.Items.Add("All members excluding historical members");
            gvReport.Items.Add("All members including historical members");
            gvReport.Items.Add("All members using DD membership");
            gvReport.Items.Add("Billing Transaction History");
            gvReport.Items.Add("Birthday List");
            //gvReport.Items.Add("Cancelling Members");
            gvReport.Items.Add("Archived Members");
            gvReport.Items.Add("Current Members");
            gvReport.Items.Add("Total Swipe-in & Manual Check-in's");
            gvReport.Items.Add("Class, Creche and Altitude Booking Summary");
            //gvReport.Items.Add("Daily Cancellations");
            //gvReport.Items.Add("After hours members double swipe");
            //gvReport.Items.Add("Current Visiting Members");
            gvReport.Items.Add("Expiring Members");
            //gvReport.Items.Add("Future Members");
            //gvReport.Items.Add("Lost Members");
            gvReport.Items.Add("Member Made Booking");
            //gvReport.Items.Add("Member No Booking Period");
            gvReport.Items.Add("Memeber No Trainer");
            //gvReport.Items.Add("Memebers From Prospects");
            gvReport.Items.Add("No Active Membership"); //Ali 29/07/16
            gvReport.Items.Add("Membership Hold Length"); //Ali 29/07/16
            gvReport.Items.Add("Starting Memberships"); //Ali 29/07/16
            gvReport.Items.Add("New Contracts");      
            //gvReport.Items.Add("Renewed Contracts");
            gvReport.Items.Add("Suspended Contracts"); //count=25
            Member_Reports_Part2();
        }
        private void Member_Reports_Part2()
        {
            gvReport.Items.Add("Expiry Contracts");
            gvReport.Items.Add("Cancelled Contracts");
            //gvReport.Items.Add("Linked Membership Renewal");
            //gvReport.Items.Add("Re-Join Contracts");
            gvReport.Items.Add("Members with Multiple Contracts");
            gvReport.Items.Add("Current Contracts");
            //gvReport.Items.Add("New Members");
            gvReport.Items.Add("Suspended Memberships");
            //gvReport.Items.Add("Direct Debit Upgrades/Downgrades");
            gvReport.Items.Add("Length of Time as a Member");
            gvReport.Items.Add("Breakdown by Gender");
            //gvReport.Items.Add("Breakdown by Type");
        }
        private void btnViewReport_Click(object sender, RoutedEventArgs e)
        {
            //int datasetcheck = 1;
            //string _reportName = "";

            if (gvReport.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select a Report from List!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            int i = gvReport.SelectedIndex;
            _reportName = gvReport.Items[i].ToString();

            rptView = new ReportView();

            rptView._CompanyName = this._CompanyName;
            rptView._UserName = this._UserName;
            rptView._CurrentLoginDateTime = this._CurrentLoginDateTime;
            rptView._StaffPic = this._StaffPic;
            rptView.StaffImage.Source = StaffImage.Source;

            //DataSet ds = new DataSet();

            switch (_reportName.ToUpper())
            {
                case "MEMBER DETAILS REPORT"://1
                    ds = getMemberReport();
                    datasetcheck = Emptydataset(ds);
                    break;
                case "MEMBER PAYMENTS REPORT"://2
                    ds = getMemberPaymentReport();
                    datasetcheck = Emptydataset(ds);
                    break;
                case "MEMBER ONHOLD REPORT"://3
                    MessageBox.Show("No Member is on HOLD!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                case "SALES ENQUIRIES-PROSPECTS REPORT"://4
                    ds = getProspectReport();
                    datasetcheck = Emptydataset(ds);
                    break;
                case "ALL MEMBERS EXCLUDING HISTORICAL MEMBERS"://5
                    ds = getwithouthistoricalmemeber();
                    datasetcheck = Emptydataset(ds);
                    break;
                case "ALL MEMBERS INCLUDING HISTORICAL MEMBERS"://6
                    ds = getAllmemeber();
                    datasetcheck = Emptydataset(ds);
                    break;
                case "ALL MEMBERS USING DD MEMBERSHIP"://7
                    ds = getDirectDebitMember();
                    datasetcheck = Emptydataset(ds);
                    break;
                case "BILLING TRANSACTION HISTORY"://8
                    ds = getBillingHistory();
                    datasetcheck = Emptydataset(ds);
                    break;
                case "BIRTHDAY LIST"://9
                    ds = getBirthdays();
                    datasetcheck = Emptydataset(ds);
                    break;
                //case "CANCELLING MEMBERS"://10
                case "ARCHIVED MEMBERS":
                    ds = CancellingMemeber();
                    datasetcheck = Emptydataset(ds);
                    break;
                case "CURRENT MEMBERS"://11
                    ds = getCurrentMembers();
                    datasetcheck = Emptydataset(ds);
                    break;
                //GAS-45 NEW POS REPORT
                case "PERIODIC PRODUCT-WISE POS REPORT":
                    string way = "Daily";
                    DateTime from_date = DateTime.Now;
                    DateTime to_date = DateTime.Now;
                    if ((dtfromdate.Text == "") && (dttodate.Text == ""))
                    {
                        MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    if ((dtfromdate.Text != "") && (dttodate.Text == ""))
                    {

                        if (way == "Daily")
                        {
                            from_date = DateTime.Parse(dtfromdate.Text);
                            to_date = DateTime.Now;
                        }

                    }
                    else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
                    {
                        DateTime FirstDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        if (way == "Daily")
                        {
                            from_date = FirstDay;
                            to_date = DateTime.Parse(dttodate.Text);
                        }

                    }
                    else
                    {
                        from_date = DateTime.Parse(dtfromdate.Text);
                        to_date = DateTime.Parse(dttodate.Text);
                    }
                    int _productID = 0;
                    if (cmbProduct.SelectedIndex != -1)
                        _productID = (int)cmbProduct.SelectedValue;

                    if (_productID < 0) _productID = 0;

                    ds = Periodic_ProductWise_POS_Report(from_date, to_date, _productID);
                    datasetcheck = Emptydataset(ds);
                    break;
                //GAS-45 NEW POS REPORT
                case "END OF DAY REPORT FOR POINT OF SALE"://12
                    way = "Daily";
                    from_date = DateTime.Now;
                    to_date = DateTime.Now;
                    if ((dtfromdate.Text == "") && (dttodate.Text == ""))
                    {
                        MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    if ((dtfromdate.Text != "") && (dttodate.Text == ""))
                    {

                        if (way == "Daily")
                        {
                            from_date = DateTime.Parse(dtfromdate.Text);
                            to_date = DateTime.Now;
                        }

                    }
                    else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
                    {
                        DateTime FirstDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        if (way == "Daily")
                        {
                            from_date = FirstDay;
                            to_date = DateTime.Parse(dttodate.Text);
                        }
                    }
                    else
                    {
                        from_date = DateTime.Parse(dtfromdate.Text);
                        to_date = DateTime.Parse(dttodate.Text);
                    }
                    ds = eod_pos(from_date, to_date);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "END OF WEEK REPORT FOR POINT OF SALE"://13
                    way = "Weekly";
                    from_date = DateTime.Now;
                    to_date = DateTime.Now; ;
                    if ((dtfromdate.Text == "") && (dttodate.Text == ""))
                    {
                        MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    if ((dtfromdate.Text != "") && (dttodate.Text == ""))
                    {

                        if (way == "Daily")
                        {
                            from_date = DateTime.Parse(dtfromdate.Text);
                            to_date = DateTime.Now;
                        }
                        else
                        {
                            from_date = DateTime.Parse(dtfromdate.Text);
                            to_date = from_date.AddDays(7);
                        }
                    }
                    else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
                    {
                        DateTime FirstDay = new DateTime(DateTime.Now.Year, 1, 1);
                        if (way == "Daily")
                        {
                            from_date = FirstDay;
                            to_date = DateTime.Parse(dttodate.Text);
                        }
                        else
                        {
                            from_date = DateTime.Parse(dttodate.Text).AddDays(-7);
                            to_date = DateTime.Parse(dttodate.Text);
                        }
                    }
                    else
                    {
                        from_date = DateTime.Parse(dtfromdate.Text);
                        to_date = DateTime.Parse(dttodate.Text);
                    }
                    ds = eod_pos(from_date, to_date);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "DAILY SALES"://14
                case "DAILY SALES (MEMBERSHIP)":
                    way = "Daily";
                    from_date = DateTime.Now;
                    to_date = DateTime.Now; ;
                    if ((dtfromdate.Text == "") && (dttodate.Text == ""))
                    {
                        MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    if ((dtfromdate.Text != "") && (dttodate.Text == ""))
                    {

                        if (way == "Daily")
                        {
                            from_date = DateTime.Parse(dtfromdate.Text);
                            to_date = DateTime.Now;
                        }

                    }
                    else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
                    {
                        DateTime FirstDay = new DateTime(DateTime.Now.Year, 1, 1);
                        if (way == "Daily")
                        {
                            from_date = FirstDay;
                            to_date = DateTime.Parse(dttodate.Text);
                        }

                    }
                    else
                    {
                        from_date = DateTime.Parse(dtfromdate.Text);
                        to_date = DateTime.Parse(dttodate.Text);
                    }
                    ds = Sales(from_date, to_date);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "WEEKLY SALES"://15
                case "WEEKLY SALES (MEMBERSHIP)":
                    way = "Weekly";
                    from_date = DateTime.Now;
                    to_date = DateTime.Now; ;
                    if ((dtfromdate.Text == "") && (dttodate.Text == ""))
                    {
                        MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    if ((dtfromdate.Text != "") && (dttodate.Text == ""))
                    {

                        if (way == "Daily")
                        {
                            from_date = DateTime.Parse(dtfromdate.Text);
                            to_date = DateTime.Now;
                        }
                        else
                        {
                            from_date = DateTime.Parse(dtfromdate.Text);
                            to_date = from_date.AddDays(7);
                        }

                    }
                    else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
                    {
                        DateTime FirstDay = new DateTime(DateTime.Now.Year, 1, 1);
                        if (way == "Daily")
                        {
                            from_date = FirstDay;
                            to_date = DateTime.Parse(dttodate.Text);
                        }
                        else
                        {
                            from_date = DateTime.Parse(dttodate.Text).AddDays(-7);
                            to_date = DateTime.Parse(dttodate.Text);
                        }

                    }
                    else
                    {
                        from_date = DateTime.Parse(dtfromdate.Text);
                        to_date = DateTime.Parse(dttodate.Text);
                    }
                    ds = Sales(from_date, to_date);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "DAILY WALKIN"://16
                    way = "Daily";
                    from_date = DateTime.Now;
                    to_date = DateTime.Now; ;
                    if ((dtfromdate.Text == "") && (dttodate.Text == ""))
                    {
                        MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    if ((dtfromdate.Text != "") && (dttodate.Text == ""))
                    {

                        if (way == "Daily")
                        {
                            from_date = DateTime.Parse(dtfromdate.Text);
                            to_date = DateTime.Now;
                        }

                    }
                    else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
                    {
                        DateTime FirstDay = new DateTime(DateTime.Now.Year, 1, 1);
                        if (way == "Daily")
                        {
                            from_date = FirstDay;
                            to_date = DateTime.Parse(dttodate.Text);
                        }

                    }
                    else
                    {
                        from_date = DateTime.Parse(dtfromdate.Text);
                        to_date = DateTime.Parse(dttodate.Text);
                    }
                    ds = Walkin(from_date, to_date);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "WEEKLY WALKIN"://17
                    way = "Weekly";
                    from_date = DateTime.Now;
                    to_date = DateTime.Now; ;
                    if ((dtfromdate.Text == "") && (dttodate.Text == ""))
                    {
                        MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    if ((dtfromdate.Text != "") && (dttodate.Text == ""))
                    {

                        if (way == "Daily")
                        {
                            from_date = DateTime.Parse(dtfromdate.Text);
                            to_date = DateTime.Now;
                        }
                        else
                        {
                            from_date = DateTime.Parse(dtfromdate.Text);
                            to_date = from_date.AddDays(7);
                        }

                    }
                    else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
                    {
                        DateTime FirstDay = new DateTime(DateTime.Now.Year, 1, 1);
                        if (way == "Daily")
                        {
                            from_date = FirstDay;
                            to_date = DateTime.Parse(dttodate.Text);
                        }
                        else
                        {
                            from_date = DateTime.Parse(dttodate.Text).AddDays(-7);
                            to_date = DateTime.Parse(dttodate.Text);
                        }

                    }
                    else
                    {
                        from_date = DateTime.Parse(dtfromdate.Text);
                        to_date = DateTime.Parse(dttodate.Text);
                    }
                    ds = Walkin(from_date, to_date);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "DAILY CONVERSION"://18
                    way = "Daily";
                    from_date = DateTime.Now;
                    to_date = DateTime.Now; ;
                    if ((dtfromdate.Text == "") && (dttodate.Text == ""))
                    {
                        MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    if ((dtfromdate.Text != "") && (dttodate.Text == ""))
                    {

                        if (way == "Daily")
                        {
                            from_date = DateTime.Parse(dtfromdate.Text);
                            to_date = DateTime.Now;
                        }

                    }
                    else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
                    {
                        DateTime FirstDay = new DateTime(DateTime.Now.Year, 1, 1);
                        if (way == "Daily")
                        {
                            from_date = FirstDay;
                            to_date = DateTime.Parse(dttodate.Text);
                        }

                    }
                    else
                    {
                        from_date = DateTime.Parse(dtfromdate.Text);
                        to_date = DateTime.Parse(dttodate.Text);
                    }
                    ds = Conversion(from_date, to_date);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "WEEKLY CONVERSION"://19
                    way = "Weekly";
                    from_date = DateTime.Now;
                    to_date = DateTime.Now; ;
                    if ((dtfromdate.Text == "") && (dttodate.Text == ""))
                    {
                        MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    if ((dtfromdate.Text != "") && (dttodate.Text == ""))
                    {

                        if (way == "Daily")
                        {
                            from_date = DateTime.Parse(dtfromdate.Text);
                            to_date = DateTime.Now;
                        }
                        else
                        {
                            from_date = DateTime.Parse(dtfromdate.Text);
                            to_date = from_date.AddDays(7);
                        }

                    }
                    else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
                    {
                        DateTime FirstDay = new DateTime(DateTime.Now.Year, 1, 1);
                        if (way == "Daily")
                        {
                            from_date = FirstDay;
                            to_date = DateTime.Parse(dttodate.Text);
                        }
                        else
                        {
                            from_date = DateTime.Parse(dttodate.Text).AddDays(-7);
                            to_date = DateTime.Parse(dttodate.Text);
                        }

                    }
                    else
                    {
                        from_date = DateTime.Parse(dtfromdate.Text);
                        to_date = DateTime.Parse(dttodate.Text);
                    }
                    //ds = Walkin(from_date, to_date);
                    ds = Conversion(from_date, to_date);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "TOTAL SWIPE-IN & MANUAL CHECK-IN'S"://20
                    way = "Daily";
                    from_date = DateTime.Now;
                    to_date = DateTime.Now; ;
                    //if ((dtfromdate.Text == "") && (dttodate.Text == ""))
                    //{
                    //    MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //    return;
                    //}
                    //if(cmbchkintype.Text=="")
                    //{
                    //    MessageBox.Show("Please Select the Check In Type", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //    return;
                    //}
                    //if ((dtfromdate.Text != "") && (dttodate.Text == ""))
                    //{

                    //    if (way == "Daily")
                    //    {
                    //        from_date = DateTime.Parse(dtfromdate.Text);
                    //        to_date = DateTime.Now;
                    //    }

                    //}
                    //else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
                    //{
                    //    DateTime FirstDay = new DateTime(DateTime.Now.Year, 1, 1);
                    //    if (way == "Daily")
                    //    {
                    //        from_date = FirstDay;
                    //        to_date = DateTime.Parse(dttodate.Text);
                    //    }

                    //}
                    //else
                    //{
                        from_date = DateTime.Parse(dtfromdate.Text);
                        to_date = DateTime.Parse(dttodate.Text);
                    //}
                    ds = Swipe_Manual_CheckIn(from_date, to_date);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "CLASS, CRECHE AND ALTITUDE BOOKING SUMMARY"://21
                    way = "Daily";
                    from_date = DateTime.Now;
                    to_date = DateTime.Now; ;
                    //if ((dtfromdate.Text == "") || (dttodate.Text == "")) /*|| (cmbresourcetype.Text=="") || (cmbroomortrainer.Text=="") )*/
                    //{
                    //    MessageBox.Show("Please fill all the enteries", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //    return;
                    //}
                        from_date = DateTime.Parse(dtfromdate.Text);
                        to_date = DateTime.Parse(dttodate.Text);
                    
                    ds = Class_Creche_Altitude(from_date, to_date);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "DAILY CANCELLATIONS"://22
                    ds = null;
                    datasetcheck = Emptydataset(ds);
                    break;
                case "AFTER HOURS MEMBERS DOUBLE SWIPE"://23
                    ds = null;
                    datasetcheck = Emptydataset(ds);
                    break;
                case "WEEKLY STOCKTAKE"://24
                    ds = null;
                    datasetcheck = Emptydataset(ds);
                    break;
                default://25
                    viewreport_sub2(_reportName);
                    break;
                    //return;
                    //break;

            }
            if (_reportName != "" && datasetcheck == 1)
            {
                datasetcheck = 0;
                rptView.lvReportView.ItemsSource = null;
                rptView.lvReportView.ItemsSource = ds.Tables[0].AsDataView();
                rptView.printds = rptView.lvReportView;
                rptView.Owner = this;
                rptView.Title = _reportName;
                rptView.ShowDialog();
                clearcontrols();
            }
        }
        public void viewreport_sub2(string report_name)
        {
            Report_SubCode rsub = new Report_SubCode();
            int status;
            int Gender;
            switch (report_name.ToUpper())
            {
                case "CURRENT VISITING MEMBERS"://1
                    ds = null;
                    datasetcheck = Emptydataset(ds);
                    break;
                case "EXPIRING MEMBERS"://2
                case "EXPIRY CONTRACTS":
                    ds = Expiring_Memebers();
                    datasetcheck = Emptydataset(ds);
                    break;
                case "FUTURE MEMBERS"://3
                    ds = null;
                    datasetcheck = Emptydataset(ds);
                    break;
                case "LOST MEMBERS"://4
                    ds = null;
                    datasetcheck = Emptydataset(ds);
                    break;
                case "MEMBER MADE BOOKING"://5
                    ds = Member_Made_Booking(DateTime.Parse(dtfromdate.Text), DateTime.Parse(dttodate.Text));
                    datasetcheck = Emptydataset(ds);
                    break;
                case "MEMBER NO BOOKING PERIOD"://6
                    ds = null;
                    datasetcheck = Emptydataset(ds);
                    break;
                case "MEMEBER NO TRAINER"://7
                    ds = Memeber_No_trainer();
                    datasetcheck = Emptydataset(ds);
                    break;
                case "MEMEBERS FROM PROSPECTS"://8
                case "DIRECT DEBIT UPGRADES/DOWNGRADES":
                case "BREAKDOWN BY TYPE":
                    ds = null;
                    datasetcheck = Emptydataset(ds);
                    break;
                case "NO ACTIVE MEMBERSHIP"://9 Done by Ali @ 29/07/16
                //case "CANCELLED CONTRACTS": // Added by Iqbal (only this line)  30/07/16
                    ds = NoActiveMembership();
                    datasetcheck = Emptydataset(ds);
                    break;
                case "CANCELLED CONTRACTS": // Added by Iqbal (only this line)  30/07/16
                    ds = CanceledContracts();
                    datasetcheck = Emptydataset(ds);
                    break;
                case "MEMBERSHIP HOLD LENGTH": //10 Done by Ali @ 29/07/16
                    status = 0;
                    ds = MembershipHoldLength(status);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "STARTING MEMBERSHIPS": //11 Done by Ali @ 29/07/16
                    //if (dtfromdate.Text == "")
                    //{
                    //    MessageBox.Show("Please Select Start Date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //    return;
                    //}
                    //if (dttodate.Text == "")
                    //{
                    //    MessageBox.Show("Please Select End Date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //    return;
                    //}
                    from_date = DateTime.Parse(dtfromdate.Text);
                    to_date = DateTime.Parse(dttodate.Text);
                    ds = StartingMemberships(from_date, to_date);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "SUSPENDED MEMBERSHIPS"://12 //Ali
                case "SUSPENDED CONTRACTS": // Added by Iqbal (only this line)  30/07/16
                    status = 0;
                    ds = MembershipHoldLength(status);
                    datasetcheck = Emptydataset(ds);
                    break;
                
                case "NEW CONTRACTS"://13
                    String way = "Daily";
                    from_date = DateTime.Now;
                    to_date = DateTime.Now;
                    //if ((dtfromdate.Text == "") && (dttodate.Text == ""))
                    //{
                    //    MessageBox.Show("Please Select the date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //    return;
                    //}
                    //if ((dtfromdate.Text != "") && (dttodate.Text == ""))
                    //{

                    //    if (way == "Daily")
                    //    {
                    //        from_date = DateTime.Parse(dtfromdate.Text);
                    //        to_date = DateTime.Now;
                    //    }

                    //}
                    //else if ((dtfromdate.Text == "") && (dttodate.Text != ""))
                    //{
                    //    DateTime FirstDay = new DateTime(DateTime.Now.Year, 1, 1);
                    //    if (way == "Daily")
                    //    {
                    //        from_date = FirstDay;
                    //        to_date = DateTime.Parse(dttodate.Text);
                    //    }

                    //}
                    //else
                    //{
                        from_date = DateTime.Parse(dtfromdate.Text);
                        to_date = DateTime.Parse(dttodate.Text);
                    //}
                    ds = NewContracts(from_date, to_date);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "RENEWED CONTRACTS"://14
                case "LINKED MEMBERSHIP RENEWAL":
                case "RE-JOIN CONTRACTS":
                case "NEW MEMBERS":
                    ds = null;
                    datasetcheck = Emptydataset(ds);
                    break;
                case "MEMBERS WITH MULTIPLE CONTRACTS"://15
                    ds = rsub.Member_Multiple_Contracts();
                    datasetcheck = Emptydataset(ds);
                    break;
                case "CURRENT CONTRACTS"://16
                    ds = rsub.Current_Contracts();
                    datasetcheck = Emptydataset(ds);
                    break;
                case "LENGTH OF TIME AS A MEMBER"://17
                    ds =rsub.LengthofMember();
                    datasetcheck = Emptydataset(ds);
                    break;
                case "BREAKDOWN BY GENDER": //18
                    if (cmbGender.SelectedIndex == 0)
                    {
                        Gender = 3;
                    }
                    else if (cmbGender.SelectedIndex == 1)
                    {
                        Gender = 1;
                    }
                    else
                    {
                        Gender = 2;
                    }
                        ds = rsub.BreakdownbyGender(Gender);
                    datasetcheck = Emptydataset(ds);
                    //clearcontrols();
                    break;
                case "ALL MEMBER BOOKINGS":
                    from_date = DateTime.Now;
                    to_date = DateTime.Now; ;
                    from_date = DateTime.Parse(dtfromdate.Text);
                    to_date = DateTime.Parse(dttodate.Text);
                    string BookedStatus = string.Empty;
                    //int i = 0;
                    if (chkBooked.IsChecked == true && chkWaiting.IsChecked == true && chkCancelled.IsChecked == true)
                    {
                        BookedStatus = "B,W,C";
                    }
                    else if(chkBooked.IsChecked == true && chkWaiting.IsChecked == true)
                    {
                        BookedStatus = "B,W";
                    }
                    else if (chkBooked.IsChecked == true && chkCancelled.IsChecked == true)
                    {
                        BookedStatus = "B,C";
                    }
                    else if (chkWaiting.IsChecked == true && chkCancelled.IsChecked == true)
                    {
                        BookedStatus = "W,C";
                    }
                    else if(chkBooked.IsChecked==true)
                    {
                        BookedStatus = "B";

                    }
                    else if(chkWaiting.IsChecked == true)
                    {
                        BookedStatus = "W";
                    }
                    else if(chkCancelled.IsChecked == true)
                    {
                        BookedStatus = "C";
                    }
                    else
                    {
                        BookedStatus = "B,W,C";
                    }
                    ds = BookingDetails(from_date, to_date,BookedStatus);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "ALL PAYMENTS":
                    from_date = DateTime.Now;
                    to_date = DateTime.Now; ;
                    from_date = DateTime.Parse(dtfromdate.Text);
                    to_date = DateTime.Parse(dttodate.Text);
                    ds = AllPayments(from_date, to_date);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "ALL PRODUCTS":
                    status = 0;
                    ds = Products(status);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "ALL VOIDED PAYMENTS":
                    from_date = DateTime.Now;
                    to_date = DateTime.Now; ;
                    from_date = DateTime.Parse(dtfromdate.Text);
                    to_date = DateTime.Parse(dttodate.Text);
                    ds = AllVoidedPayments(from_date, to_date);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "ALL SALES":
                    ds = null;
                    datasetcheck = Emptydataset(ds);
                    break;
                case "INVENTORY STOCK LEVEL":
                    status = 1;
                    ds = Products(status);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "RESOURCE SUMMARY":
                    ds = null;
                    datasetcheck = Emptydataset(ds);
                    break;
                case "ACTIONS TAKEN":
                    ds = AllTasksReport(2);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "ALL COMPLETED TASK":
                    ds = AllTasksReport(4);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "ALL OUTSTANDING TASK":
                    ds = AllTasksReport(1);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "ALL WORKED ON TASK":
                    ds = AllTasksReport(2);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "CORRESPONDENCE SENT":
                    ds = CorrespondenceSent("PROSPECT");
                    datasetcheck = Emptydataset(ds);
                    break;
                case "NOT VISITING":
                    from_date = DateTime.Parse(dtfromdate.Text);
                    to_date = DateTime.Parse(dttodate.Text);
                    ds = NotVisited(from_date, to_date);
                    datasetcheck = Emptydataset(ds); 
                    break;
                case "VISITOR LOG":
                     from_date = DateTime.Parse(dtfromdate.Text);
                    to_date = DateTime.Parse(dttodate.Text);
                    ds = VisitorLog(from_date, to_date);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "VISITS IN PERIOD":
                    from_date = DateTime.Parse(dtfromdate.Text);
                    to_date = DateTime.Parse(dttodate.Text);
                    ds = Visitsinperiod(from_date, to_date);
                    datasetcheck = Emptydataset(ds);
                    break; 
                case "RECENTLY NOT VISITING":
                    from_date = DateTime.Parse(dttodate.Text).AddDays(-21);
                    to_date = DateTime.Parse(dttodate.Text);
                    ds = RecentlyNotVisited(from_date, to_date);
                    datasetcheck = Emptydataset(ds);
                    break;
                case "VISITS PER DAY":
                    ds = null;
                    datasetcheck = Emptydataset(ds);
                    break;
                case "FINANCIAL":
                    ds = null;
                    datasetcheck = Emptydataset(ds);
                    break;
                case "OUTSTANDING ACCOUNTS":
                    ds = null;
                    datasetcheck = Emptydataset(ds);
                    break;
                case "ATTENDANCE":
                    ds = null;
                    datasetcheck = Emptydataset(ds);
                    break;
                default:
                    ds = null;
                    datasetcheck = Emptydataset(ds);
                    break;
            }
            //if (report_name != "" && datasetcheck == 1)
            //{
            //    //rptView.printds = ds;
            //    datasetcheck = 0;
            //    rptView.lvReportView.ItemsSource = null;
            //    rptView.lvReportView.ItemsSource = ds.Tables[0].AsDataView();
            //    rptView.printds = rptView.lvReportView;
            //    rptView.Owner = this;
            //    rptView.ShowDialog();
            //    clearcontrols();
               
            //}
        }
        private DataSet getMemberReport()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("GetALLMemberDetails_Report", ht);
            return dsgetM;
        }
        private DataSet getMemberPaymentReport()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("GetALLMemberPayments_Report", ht);
            return dsgetM;
        }
        private DataSet getProspectReport()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("GetALLProspects_Report", ht);
            return dsgetM;
        }
        private DataSet getwithouthistoricalmemeber()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_ExcludingHistoricalMembers", ht);
            return dsgetM;
        }
        private DataSet CancellingMemeber()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_CancellingMembers", ht);
            return dsgetM;
        }
        private DataSet getAllmemeber()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_IncludingHistoricalMembers", ht);
            return dsgetM;
        }
        private DataSet getDirectDebitMember()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            DataSet dsgetM = new DataSet();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            if (cmbpaymentmode.Text == "All Payment Mode")
            {
                ht.Add("@paymode", cmbpaymentmode.Text);
                ht.Add("@PaymentModeId", 1);
            }
            else if ((cmbpaymentmode.Text == "Bank Account") || (cmbpaymentmode.Text == "Credit Card"))
            {
                ht.Add("@paymode", cmbpaymentmode.Text);
                ht.Add("@PaymentModeId", 2);
            }
            dsgetM = sql.ExecuteProcedure("Rpt_DDMembership", ht);
            return dsgetM;
        }
        private DataSet getBillingHistory()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", DateTime.Parse(dtfromdate.Text));
            ht.Add("@todate", DateTime.Parse(dttodate.Text));
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_Billing_Transaction_History", ht);
            return dsgetM;
        }
        private DataSet getBirthdays()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@from_date", DateTime.Parse(dtfromdateBirth.Text));
            ht.Add("@to_date", DateTime.Parse(dttodateBirth .Text));
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_BirthdayList", ht);
            return dsgetM;
        }
        private DataSet getCurrentMembers()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            if (cmbgroup.Text != "")
            {
                ht.Add("@programmegroup", Convert.ToInt32(cmbgroup.SelectedValue.ToString()));
            }
            else
            {
                ht.Add("@programmegroup","");
            }
            
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@programme", cmbprogram.Text);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_CurrentMembers", ht);
            return dsgetM;
        }
        private DataSet eod_pos(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);

            DataSet dsgetM = sql.ExecuteProcedure("POS_EOD", ht);
            //DataSet dsgetM = sql.ExecuteProcedure("POS_Sale_Report", ht, true);
            return dsgetM;
        }
        private DataSet Periodic_ProductWise_POS_Report(DateTime fromdate, DateTime todate, int _productID = 0)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            ht.Add("@ProductID", _productID);

            DataSet dsgetM = sql.ExecuteProcedure("Periodic_ProductWise_POS_Report", ht);
            return dsgetM;
        }
        private DataSet Sales(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);

            DataSet dsgetM = sql.ExecuteProcedure("Rpt_Sales", ht);
            return dsgetM;
        }
        private DataSet Walkin(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);

            DataSet dsgetM = sql.ExecuteProcedure("Rpt_Walkin", ht);
            return dsgetM;
        }
        private DataSet Conversion(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);

            DataSet dsgetM = sql.ExecuteProcedure("Rpt_Conversion", ht);
            return dsgetM;
        }
        private DataSet Swipe_Manual_CheckIn(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            ht.Add("@chkintype", cmbchkintype.Text);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_Swipe_CheckIn", ht);
            return dsgetM;
        }
        private DataSet Class_Creche_Altitude(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            ht.Add("@ResourceTypeName", cmbresourcetype.Text);
            ht.Add("@RoomOrTrainer", cmbroomortrainer.Text);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_Class_Creche", ht);
            return dsgetM;
        }
        private DataSet Member_Made_Booking(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_MemberMadeBooking", ht);
            return dsgetM;
        }
        private DataSet Expiring_Memebers()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            if (cmbgroup.Text != "")
            {
                ht.Add("@programmegroup", Convert.ToInt32(cmbgroup.SelectedValue.ToString()));
            }
            else
            {
                ht.Add("@programmegroup", "");
            }

            if (cmbprogram.Text != "")
            {
                ht.Add("@Programme", Convert.ToInt32(cmbprogram.SelectedValue.ToString()));
            }
            else
            {
                ht.Add("@Programme", "");
            }

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate",DateTime.Parse(dtfromdate.Text));
            ht.Add("@todate",DateTime.Parse(dttodate.Text));
            //ht.Add("@Programmegroup", cmbgroup.SelectedValue);
            //ht.Add("@Programme", cmbprogram.SelectedValue);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_ExpiringMemebers", ht);
            return dsgetM;
        }
        private DataSet Memeber_No_trainer()
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_MemberNoTrainer", ht);
            return dsgetM;
        }
        private DataSet NoActiveMembership() //Ali 29/07/16
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_NoActiveMembership", ht);
            return dsgetM;
        }
        private DataSet CanceledContracts() //Ali 29/07/16
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_CanceledContracts", ht);
            return dsgetM;
        }
        private DataSet MembershipHoldLength(int Status)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@Status", Status);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_MembershipHoldLength", ht);
            return dsgetM;
        }
        private DataSet StartingMemberships(DateTime fromdate, DateTime todate) //Ali 29/07/16
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            if (cmbgroup.Text != "")
            {
                ht.Add("@programmegroup", Convert.ToInt32(cmbgroup.SelectedValue.ToString()));
            }
            else
            {
                ht.Add("@programmegroup", "");
            }

            if (cmbprogram.Text != "")
            {
                ht.Add("@Programme", Convert.ToInt32(cmbprogram.SelectedValue.ToString()));
            }
            else
            {
                ht.Add("@Programme", "");
            }

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            //ht.Add("@programme", cmbprogram.Text);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_StartingMemberships", ht);
            return dsgetM;
        }
        private DataSet NewContracts(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);

            DataSet dsgetM = sql.ExecuteProcedure("Rpt_NewContracts", ht);
            return dsgetM;
        }
        private DataSet BookingDetails(DateTime fromdate, DateTime todate,String BookedStatus)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            ht.Add("@ResourceTypeName", cmbresourcetype.Text);
            ht.Add("@RoomOrTrainer", cmbroomortrainer.Text);
            ht.Add("@BookingStatus", BookedStatus);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_BookingDetails", ht);
            return dsgetM;
        }
        private DataSet AllPayments(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_AllPayments", ht);
            return dsgetM;
        }
        private DataSet AllVoidedPayments(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_AllVoidedPayments", ht);
            return dsgetM;
        }
        private DataSet Products(int status)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@Status", Convert.ToInt32(status).ToString());
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

            DataSet dsgetM = sql.ExecuteProcedure("Rpt_Products", ht);
            return dsgetM;
        }
        private DataSet VisitorLog(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_VisitorLog", ht);
            return dsgetM;
        }
        private DataSet Visitsinperiod(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_VisitsInPeriod", ht);
            return dsgetM;
        }
        private DataSet NotVisited(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_NotVisited", ht);
            return dsgetM;
        }
        private DataSet RecentlyNotVisited(DateTime fromdate, DateTime todate)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@fromdate", fromdate);
            ht.Add("@todate", todate);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_RecentlyNotVisited", ht);
            return dsgetM;
        }
        private DataSet AllTasksReport(int _TaskStatus = 4)
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@Status", _TaskStatus);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_AllTasksReport", ht);
            return dsgetM;
        }
        private DataSet CorrespondenceSent(string _receiverType = "PROSPECT")
        {
            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            ht.Add("@receiverType", _receiverType);
            DataSet dsgetM = sql.ExecuteProcedure("Rpt_CorrespondenceSent", ht);
            return dsgetM;
        }
        private void gvReport_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int i = gvReport.SelectedIndex;
            if (i != -1)
            {
                string report_name = gvReport.Items[i].ToString();
                clearcontrols();

                //if (report_name == "All members including historical members")
                //{
                //    lblfromdate.Visibility = Visibility.Visible;
                //    lbltodate.Visibility = Visibility.Visible;
                //    dtfromdate.Visibility = Visibility.Visible;
                //    dttodate.Visibility = Visibility.Visible;
                //    lblpaymentmodecombo.Visibility = Visibility.Collapsed;
                //    cmbpaymentmode.Visibility = Visibility.Collapsed;
                //    lblcmbgroup.Visibility = Visibility.Collapsed;
                //    lblcmbpgm.Visibility = Visibility.Collapsed;
                //    cmbgroup.Visibility = Visibility.Collapsed;
                //    cmbprogram.Visibility = Visibility.Collapsed;
                //    lblchkintype.Visibility = Visibility.Collapsed;
                //    cmbchkintype.Visibility = Visibility.Collapsed;
                //    lblresourcetype.Visibility = Visibility.Collapsed;
                //    cmbresourcetype.Visibility = Visibility.Collapsed;
                //    lblroomortrainer.Visibility = Visibility.Collapsed;
                //    cmbroomortrainer.Visibility = Visibility.Collapsed;
                //    lblGender.Visibility = Visibility.Collapsed;
                //    cmbGender.Visibility = Visibility.Collapsed;
                //    lblStatus.Visibility = Visibility.Collapsed;
                //    ChkBooking.Visibility = Visibility.Collapsed;
                //}
               if (report_name == "All members using DD membership")
                {
                    lblfromdate.Visibility = Visibility.Collapsed;
                    lbltodate.Visibility = Visibility.Collapsed;
                    dtfromdate.Visibility = Visibility.Collapsed;
                    dttodate.Visibility = Visibility.Collapsed;
                    dtfromdateBirth.Visibility = Visibility.Collapsed;
                    dttodateBirth.Visibility = Visibility.Collapsed;
                    lblpaymentmodecombo.Visibility = Visibility.Visible;
                    cmbpaymentmode.Visibility = Visibility.Visible;
                    lblcmbgroup.Visibility = Visibility.Collapsed;
                    lblcmbpgm.Visibility = Visibility.Collapsed;
                    cmbgroup.Visibility = Visibility.Collapsed;
                    cmbprogram.Visibility = Visibility.Collapsed;
                    lblchkintype.Visibility = Visibility.Collapsed;
                    cmbchkintype.Visibility = Visibility.Collapsed;
                    lblresourcetype.Visibility = Visibility.Collapsed;
                    cmbresourcetype.Visibility = Visibility.Collapsed;
                    lblroomortrainer.Visibility = Visibility.Collapsed;
                    cmbroomortrainer.Visibility = Visibility.Collapsed;
                    lblGender.Visibility = Visibility.Collapsed;
                    cmbGender.Visibility = Visibility.Collapsed;
                    lblStatus.Visibility = Visibility.Collapsed;
                    ChkBooking.Visibility = Visibility.Collapsed;
                    ChkProduct.Visibility = Visibility.Collapsed;
                }
                else if (report_name == "Birthday List")
                {
                    lblfromdate.Visibility = Visibility.Visible;
                    lbltodate.Visibility = Visibility.Visible;
                    dtfromdate.Visibility = Visibility.Collapsed;
                    dttodate.Visibility = Visibility.Collapsed;
                    dtfromdateBirth.Visibility = Visibility.Visible;
                    dttodateBirth.Visibility = Visibility.Visible;
                    lblpaymentmodecombo.Visibility = Visibility.Collapsed;
                    cmbpaymentmode.Visibility = Visibility.Collapsed;
                    lblcmbgroup.Visibility = Visibility.Collapsed;
                    lblcmbpgm.Visibility = Visibility.Collapsed;
                    cmbgroup.Visibility = Visibility.Collapsed;
                    cmbprogram.Visibility = Visibility.Collapsed;
                    lblchkintype.Visibility = Visibility.Collapsed;
                    cmbchkintype.Visibility = Visibility.Collapsed;
                    lblresourcetype.Visibility = Visibility.Collapsed;
                    cmbresourcetype.Visibility = Visibility.Collapsed;
                    lblroomortrainer.Visibility = Visibility.Collapsed;
                    cmbroomortrainer.Visibility = Visibility.Collapsed;
                    lblGender.Visibility = Visibility.Collapsed;
                    cmbGender.Visibility = Visibility.Collapsed;
                    lblStatus.Visibility = Visibility.Collapsed;
                    ChkBooking.Visibility = Visibility.Collapsed;
                    ChkProduct.Visibility = Visibility.Collapsed;

                    //dtfromdate.SelectedDate.Value.ToString("dd/MMM");
                    //dttodate.DisplayDateEnd = new DateTime(DateTime.Now.Year, 12, 31);
                    //dtfromdate.DisplayDateStart = new DateTime(DateTime.Now.Year, 1, 1);
                    //dtfromdate.DisplayDateEnd = new DateTime(DateTime.Now.Year, 12, 31);
                    //dttodate.DisplayDateStart = dtfromdate.SelectedDate;

                }
                else if ((report_name == "End of day report for point of sale") || (report_name == "End of week report for point of sale")
                    || (report_name == "Daily Sales") || (report_name == "Weekly Sales") || (report_name == "Daily Sales (Membership)") 
                    || (report_name == "Weekly Sales (Membership)")
                    || (report_name == "Daily Walkin") || (report_name == "Weekly Walkin")
                    || (report_name == "Daily Conversion") || (report_name == "Weekly Conversion")
                    || (report_name == "Member Made Booking") || (report_name== "New Contracts")
                    || (report_name == "Billing Transaction History")||(report_name=="All Payments") 
                    || (report_name == "All Voided Payments") || (report_name == "Visits In Period") 
                    || (report_name == "Not Visiting")
                    || (report_name =="Visitor Log")
                    //GAS-45 NEW POS REPORT
                    || (report_name =="Periodic Product-wise POS Report")
                    //GAS-45 NEW POS REPORT
                    )
                {

                    lblfromdate.Visibility = Visibility.Visible;
                    lbltodate.Visibility = Visibility.Visible;
                    dtfromdate.Visibility = Visibility.Visible;
                    dttodate.Visibility = Visibility.Visible;
                    dtfromdateBirth.Visibility = Visibility.Collapsed;
                    dttodateBirth.Visibility = Visibility.Collapsed;
                    lblpaymentmodecombo.Visibility = Visibility.Collapsed;
                    cmbpaymentmode.Visibility = Visibility.Collapsed;
                    lblcmbgroup.Visibility = Visibility.Collapsed;
                    lblcmbpgm.Visibility = Visibility.Collapsed;
                    cmbgroup.Visibility = Visibility.Collapsed;
                    cmbprogram.Visibility = Visibility.Collapsed;
                    lblchkintype.Visibility = Visibility.Collapsed;
                    cmbchkintype.Visibility = Visibility.Collapsed;
                    lblresourcetype.Visibility = Visibility.Collapsed;
                    cmbresourcetype.Visibility = Visibility.Collapsed;
                    lblroomortrainer.Visibility = Visibility.Collapsed;
                    cmbroomortrainer.Visibility = Visibility.Collapsed;
                    lblGender.Visibility = Visibility.Collapsed;
                    cmbGender.Visibility = Visibility.Collapsed;
                    lblStatus.Visibility = Visibility.Collapsed;
                    ChkBooking.Visibility = Visibility.Collapsed;
                    ChkProduct.Visibility = Visibility.Collapsed;
                    //GAS-45 NEW POS REPORT
                    if (report_name.ToUpper() == "PERIODIC PRODUCT-WISE POS REPORT")
                    {
                        ChkProduct.Visibility = Visibility.Visible;
                    }
                    //GAS-45 NEW POS REPORT
                }
                else if (report_name == "Current Members")
                {
                    lblfromdate.Visibility = Visibility.Collapsed;
                    lbltodate.Visibility = Visibility.Collapsed;
                    dtfromdate.Visibility = Visibility.Collapsed;
                    dttodate.Visibility = Visibility.Collapsed;
                    dtfromdateBirth.Visibility = Visibility.Collapsed;
                    dttodateBirth.Visibility = Visibility.Collapsed;
                    lblpaymentmodecombo.Visibility = Visibility.Collapsed;
                    cmbpaymentmode.Visibility = Visibility.Collapsed;
                    lblcmbgroup.Visibility = Visibility.Visible;
                    lblcmbpgm.Visibility = Visibility.Visible;
                    cmbgroup.Visibility = Visibility.Visible;
                    cmbprogram.Visibility = Visibility.Visible;
                    lblchkintype.Visibility = Visibility.Collapsed;
                    cmbchkintype.Visibility = Visibility.Collapsed;
                    lblresourcetype.Visibility = Visibility.Collapsed;
                    cmbresourcetype.Visibility = Visibility.Collapsed;
                    lblroomortrainer.Visibility = Visibility.Collapsed;
                    cmbroomortrainer.Visibility = Visibility.Collapsed;
                    lblGender.Visibility = Visibility.Collapsed;
                    cmbGender.Visibility = Visibility.Collapsed;
                    lblStatus.Visibility = Visibility.Collapsed;
                    ChkBooking.Visibility = Visibility.Collapsed;
                    ChkProduct.Visibility = Visibility.Collapsed;
                }
                else if (report_name == "Total Swipe-in & Manual Check-in's")
                {
                    lblfromdate.Visibility = Visibility.Visible;
                    lbltodate.Visibility = Visibility.Visible;
                    dtfromdate.Visibility = Visibility.Visible;
                    dttodate.Visibility = Visibility.Visible;
                    dtfromdateBirth.Visibility = Visibility.Collapsed;
                    dttodateBirth.Visibility = Visibility.Collapsed;
                    lblpaymentmodecombo.Visibility = Visibility.Collapsed;
                    cmbpaymentmode.Visibility = Visibility.Collapsed;
                    lblcmbgroup.Visibility = Visibility.Collapsed;
                    lblcmbpgm.Visibility = Visibility.Collapsed;
                    cmbgroup.Visibility = Visibility.Collapsed;
                    cmbprogram.Visibility = Visibility.Collapsed;
                    lblchkintype.Visibility = Visibility.Visible;
                    cmbchkintype.Visibility = Visibility.Visible;
                    lblresourcetype.Visibility = Visibility.Collapsed;
                    cmbresourcetype.Visibility = Visibility.Collapsed;
                    lblroomortrainer.Visibility = Visibility.Collapsed;
                    cmbroomortrainer.Visibility = Visibility.Collapsed;
                    lblGender.Visibility = Visibility.Collapsed;
                    cmbGender.Visibility = Visibility.Collapsed;
                    lblStatus.Visibility = Visibility.Collapsed;
                    ChkBooking.Visibility = Visibility.Collapsed;
                    ChkProduct.Visibility = Visibility.Collapsed;
                }
                else if (report_name == "Class, Creche and Altitude Booking Summary")
                {
                    lblfromdate.Visibility = Visibility.Visible;
                    lbltodate.Visibility = Visibility.Visible;
                    dtfromdate.Visibility = Visibility.Visible;
                    dttodate.Visibility = Visibility.Visible;
                    dtfromdateBirth.Visibility = Visibility.Collapsed;
                    dttodateBirth.Visibility = Visibility.Collapsed;
                    lblpaymentmodecombo.Visibility = Visibility.Collapsed;
                    cmbpaymentmode.Visibility = Visibility.Collapsed;
                    lblcmbgroup.Visibility = Visibility.Collapsed;
                    lblcmbpgm.Visibility = Visibility.Collapsed;
                    cmbgroup.Visibility = Visibility.Collapsed;
                    cmbprogram.Visibility = Visibility.Collapsed;
                    lblchkintype.Visibility = Visibility.Collapsed ;
                    cmbchkintype.Visibility = Visibility.Collapsed ;
                    lblresourcetype.Visibility = Visibility.Visible;
                    cmbresourcetype.Visibility = Visibility.Visible ;
                    lblroomortrainer.Visibility = Visibility.Visible ;
                    cmbroomortrainer.Visibility = Visibility.Visible ;
                    lblGender.Visibility = Visibility.Collapsed;
                    cmbGender.Visibility = Visibility.Collapsed;
                    lblStatus.Visibility = Visibility.Collapsed;
                    ChkBooking.Visibility = Visibility.Collapsed;
                    ChkProduct.Visibility = Visibility.Collapsed;
                }
                else if((report_name=="Expiring Members") || (report_name == "Starting Memberships") || (report_name== "Expiry Contracts"))
                {
                    lblfromdate.Visibility = Visibility.Visible;
                    lbltodate.Visibility = Visibility.Visible;
                    dtfromdate.Visibility = Visibility.Visible;
                    dttodate.Visibility = Visibility.Visible;
                    lblcmbgroup.Visibility = Visibility.Visible;
                    lblcmbpgm.Visibility = Visibility.Visible;
                    cmbgroup.Visibility = Visibility.Visible;
                    cmbprogram.Visibility = Visibility.Visible;

                    dtfromdateBirth.Visibility = Visibility.Collapsed;
                    dttodateBirth.Visibility = Visibility.Collapsed;
                    lblpaymentmodecombo.Visibility = Visibility.Collapsed;
                    cmbpaymentmode.Visibility = Visibility.Collapsed;
                    lblchkintype.Visibility = Visibility.Collapsed;
                    cmbchkintype.Visibility = Visibility.Collapsed;
                    lblresourcetype.Visibility = Visibility.Collapsed;
                    cmbresourcetype.Visibility = Visibility.Collapsed;
                    lblroomortrainer.Visibility = Visibility.Collapsed;
                    cmbroomortrainer.Visibility = Visibility.Collapsed;
                    lblGender.Visibility = Visibility.Collapsed;
                    cmbGender.Visibility = Visibility.Collapsed;
                    lblStatus.Visibility = Visibility.Collapsed;
                    ChkBooking.Visibility = Visibility.Collapsed;
                    ChkProduct.Visibility = Visibility.Collapsed;
                }
                else if (report_name == "Breakdown by Gender")
                {
                    lblfromdate.Visibility = Visibility.Collapsed;
                    lbltodate.Visibility = Visibility.Collapsed;
                    dtfromdate.Visibility = Visibility.Collapsed;
                    dttodate.Visibility = Visibility.Collapsed;
                    dtfromdateBirth.Visibility = Visibility.Collapsed;
                    dttodateBirth.Visibility = Visibility.Collapsed;
                    lblpaymentmodecombo.Visibility = Visibility.Collapsed;
                    cmbpaymentmode.Visibility = Visibility.Collapsed;
                    lblcmbgroup.Visibility = Visibility.Collapsed;
                    lblcmbpgm.Visibility = Visibility.Collapsed;
                    cmbgroup.Visibility = Visibility.Collapsed;
                    cmbprogram.Visibility = Visibility.Collapsed;
                    lblchkintype.Visibility = Visibility.Collapsed;
                    cmbchkintype.Visibility = Visibility.Collapsed;
                    lblresourcetype.Visibility = Visibility.Collapsed;
                    cmbresourcetype.Visibility = Visibility.Collapsed;
                    lblroomortrainer.Visibility = Visibility.Collapsed;
                    cmbroomortrainer.Visibility = Visibility.Collapsed;
                    lblGender.Visibility = Visibility.Visible;
                    cmbGender.Visibility = Visibility.Visible;
                    lblStatus.Visibility = Visibility.Collapsed;
                    ChkBooking.Visibility = Visibility.Collapsed;
                    ChkProduct.Visibility = Visibility.Collapsed;
                }

                else if(report_name== "All Member Bookings")
                {
                    lblfromdate.Visibility = Visibility.Visible;
                    lbltodate.Visibility = Visibility.Visible;
                    dtfromdate.Visibility = Visibility.Visible;
                    dttodate.Visibility = Visibility.Visible;
                    dtfromdateBirth.Visibility = Visibility.Collapsed;
                    dttodateBirth.Visibility = Visibility.Collapsed;
                    lblpaymentmodecombo.Visibility = Visibility.Collapsed;
                    cmbpaymentmode.Visibility = Visibility.Collapsed;
                    lblcmbgroup.Visibility = Visibility.Collapsed;
                    lblcmbpgm.Visibility = Visibility.Collapsed;
                    cmbgroup.Visibility = Visibility.Collapsed;
                    cmbprogram.Visibility = Visibility.Collapsed;
                    lblchkintype.Visibility = Visibility.Collapsed;
                    cmbchkintype.Visibility = Visibility.Collapsed;
                    lblresourcetype.Visibility = Visibility.Visible;
                    cmbresourcetype.Visibility = Visibility.Visible;
                    lblroomortrainer.Visibility = Visibility.Visible;
                    cmbroomortrainer.Visibility = Visibility.Visible;
                    lblGender.Visibility = Visibility.Collapsed;
                    cmbGender.Visibility = Visibility.Collapsed;
                    lblStatus.Visibility = Visibility.Visible;
                    ChkBooking.Visibility = Visibility.Visible;
                    ChkProduct.Visibility = Visibility.Collapsed;
                }

                else
                {
                    lblfromdate.Visibility = Visibility.Collapsed;
                    lbltodate.Visibility = Visibility.Collapsed;
                    dtfromdate.Visibility = Visibility.Collapsed;
                    dttodate.Visibility = Visibility.Collapsed;
                    dtfromdateBirth.Visibility = Visibility.Collapsed;
                    dttodateBirth.Visibility = Visibility.Collapsed;
                    lblpaymentmodecombo.Visibility = Visibility.Collapsed;
                    cmbpaymentmode.Visibility = Visibility.Collapsed;
                    lblcmbgroup.Visibility = Visibility.Collapsed;
                    lblcmbpgm.Visibility = Visibility.Collapsed;
                    cmbgroup.Visibility = Visibility.Collapsed;
                    cmbprogram.Visibility = Visibility.Collapsed;
                    lblchkintype.Visibility = Visibility.Collapsed;
                    cmbchkintype.Visibility = Visibility.Collapsed;
                    lblresourcetype.Visibility = Visibility.Collapsed;
                    cmbresourcetype.Visibility = Visibility.Collapsed;
                    lblroomortrainer.Visibility = Visibility.Collapsed;
                    cmbroomortrainer.Visibility = Visibility.Collapsed;
                    lblGender.Visibility = Visibility.Collapsed;
                    cmbGender.Visibility = Visibility.Collapsed;
                    lblStatus.Visibility = Visibility.Collapsed;
                    ChkBooking.Visibility = Visibility.Collapsed;
                    ChkProduct.Visibility = Visibility.Collapsed;
                }
            }

        }
        private void HideControls()
        {
            lblfromdate.Visibility = Visibility.Collapsed;
            lbltodate.Visibility = Visibility.Collapsed;
            dtfromdate.Visibility = Visibility.Collapsed;
            dttodate.Visibility = Visibility.Collapsed;
            dtfromdateBirth.Visibility = Visibility.Collapsed;
            dttodateBirth.Visibility = Visibility.Collapsed;
            lblpaymentmodecombo.Visibility = Visibility.Collapsed;
            cmbpaymentmode.Visibility = Visibility.Collapsed;
            lblcmbgroup.Visibility = Visibility.Collapsed;
            lblcmbpgm.Visibility = Visibility.Collapsed;
            cmbgroup.Visibility = Visibility.Collapsed;
            cmbprogram.Visibility = Visibility.Collapsed;
            lblchkintype.Visibility = Visibility.Collapsed;
            cmbchkintype.Visibility = Visibility.Collapsed;
            lblresourcetype.Visibility = Visibility.Collapsed;
            cmbresourcetype.Visibility = Visibility.Collapsed;
            lblroomortrainer.Visibility = Visibility.Collapsed;
            cmbroomortrainer.Visibility = Visibility.Collapsed;
            lblGender.Visibility = Visibility.Collapsed;
            cmbGender.Visibility = Visibility.Collapsed;
            lblStatus.Visibility = Visibility.Collapsed;
            ChkBooking.Visibility = Visibility.Collapsed;
            ChkProduct.Visibility = Visibility.Collapsed;
        }
        private void cmbgroup_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            try
            {
                Utilities _utilites = new Utilities();

                _programmeGroupId = Convert.ToInt32(cmbgroup.SelectedValue);
                _utilites.get_ProgramByGroup(cmbprogram, _programmeGroupId);
            }
            catch (Exception ex)
            {

            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            dtfromdate.SelectedDate = DateTime.Now.Date;
            dttodate.SelectedDate = DateTime.Now.Date;
            dtfromdateBirth.SelectedDate = DateTime.Now.Date;
            dttodateBirth.SelectedDate = DateTime.Now.Date;
            dttodate.DisplayDateStart = DateTime.Now.Date;

            dttodateBirth.DisplayDateStart = DateTime.Now.Date; //new DateTime(DateTime.Now.Year, 1, 1);
            dttodateBirth.DisplayDateEnd = new DateTime(DateTime.Now.Year, 12, 31);
            dtfromdateBirth.DisplayDateStart = new DateTime(DateTime.Now.Year, 1, 1);
            dtfromdateBirth.DisplayDateEnd = new DateTime(DateTime.Now.Year, 12, 31);
            btnmemberReports_Click(sender, e);
            cmbpaymentmode.SelectedIndex = 0;
            cmbchkintype.SelectedIndex = 0;
            cmbGender.SelectedIndex = 0;
            lblfromdate.Visibility = Visibility.Collapsed;
            lbltodate.Visibility = Visibility.Collapsed;
            dtfromdate.Visibility = Visibility.Collapsed;
            dttodate.Visibility = Visibility.Collapsed;
            dtfromdateBirth.Visibility = Visibility.Collapsed;
            dttodateBirth.Visibility = Visibility.Collapsed;
            lblpaymentmodecombo.Visibility = Visibility.Collapsed;
            cmbpaymentmode.Visibility = Visibility.Collapsed;
            lblcmbgroup.Visibility = Visibility.Collapsed;
            lblcmbpgm.Visibility = Visibility.Collapsed;
            cmbgroup.Visibility = Visibility.Collapsed;
            cmbprogram.Visibility = Visibility.Collapsed;
            lblchkintype.Visibility = Visibility.Collapsed;
            cmbchkintype.Visibility = Visibility.Collapsed;
            lblresourcetype.Visibility = Visibility.Collapsed;
            cmbresourcetype.Visibility = Visibility.Collapsed;
            lblroomortrainer.Visibility = Visibility.Collapsed;
            cmbroomortrainer.Visibility = Visibility.Collapsed;
            lblGender.Visibility = Visibility.Collapsed;
            cmbGender.Visibility = Visibility.Collapsed;
            lblStatus.Visibility = Visibility.Collapsed;
            ChkBooking.Visibility = Visibility.Collapsed;
            ChkProduct.Visibility = Visibility.Collapsed;

            SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
        }
        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto(_StaffPic);
        }
        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void clearcontrols()
        {
            //dtfromdate.Text = "";
            //dttodate.Text = "";
            //cmbpaymentmode.Text = "";
            cmbresourcetype.SelectedIndex = -1;
            cmbpaymentmode.SelectedIndex = 0;
            cmbchkintype.SelectedIndex = 0;
            cmbGender.SelectedIndex = 0;
            cmbgroup.Text = "";
            cmbprogram.Text = "";
            //cmbchkintype.Text = "";
            // cmbresourcetype.Text = "";
            cmbroomortrainer.Text = "";
            //cmbGender.Text = "";
            chkBooked.IsChecked = false;
            chkCancelled.IsChecked = false;
            chkWaiting.IsChecked = false;
            dtfromdate.SelectedDate = DateTime.Now.Date;
            dttodate.SelectedDate = DateTime.Now.Date;
            dtfromdateBirth.SelectedDate = DateTime.Now.Date;
            dttodateBirth.SelectedDate = DateTime.Now.Date;
        }
        private void btnSalesReports_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnmemberReports.Background = Brushes.Gray;
                btnSalesReports.Background = Brushes.Chocolate;
                btnProspectReports.Background = Brushes.Gray;
                btnBookingReports.Background = Brushes.Gray;
                btnFinancialReports.Background = Brushes.Gray;
                btnRetentionReports.Background = Brushes.Gray;
                btnPOSReports.Background = Brushes.Gray;

                HideControls();
                populatedata();
                gvReport.Items.Clear();
                //gvReport.Items.Add("End of day report for point of sale");
                //gvReport.Items.Add("End of week report for point of sale");
                //gvReport.Items.Add("Daily Sales");
                //gvReport.Items.Add("Weekly Sales");
                gvReport.Items.Add("Daily Sales (Membership)");
                gvReport.Items.Add("Weekly Sales (Membership)");
                //gvReport.Items.Add("Weekly Stocktake");
            }
            catch (Exception ex)
            {

            }
        }
        private void btnProspectReports_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnmemberReports.Background = Brushes.Gray;
                btnSalesReports.Background = Brushes.Gray;
                btnProspectReports.Background = Brushes.Chocolate;
                btnBookingReports.Background = Brushes.Gray;
                btnFinancialReports.Background = Brushes.Gray;
                btnRetentionReports.Background = Brushes.Gray;
                btnPOSReports.Background = Brushes.Gray;

                HideControls();
                populatedata();
                gvReport.Items.Clear();
                gvReport.Items.Add("Daily Walkin");
                gvReport.Items.Add("Weekly Walkin");
                gvReport.Items.Add("Daily Conversion");
                gvReport.Items.Add("Weekly Conversion");
            }
            catch (Exception ex)
            {

            }
        }
        private void cmbresourcetype_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbresourcetype.SelectedIndex != -1)
            {
                SqlHelper sql = new SqlHelper();
                Hashtable ht = new Hashtable();
                int index = cmbresourcetype.SelectedIndex;
                int selectedresourcetype = int.Parse(cmbresourcetype.SelectedValue.ToString());
                //string selectedresourcetype = cmbresourcetype.Text;
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                ht.Add("@ResourceTypeID", selectedresourcetype);
                DataSet dsresourcetype = sql.ExecuteProcedure("Booking_Room_Trainer", ht);
                if (dsresourcetype != null)
                {
                    if (dsresourcetype.Tables[0].DefaultView != null)
                    {
                        try
                        {
                            cmbroomortrainer.ItemsSource = dsresourcetype.Tables[0].DefaultView;
                            cmbroomortrainer.DisplayMemberPath = "RoomOrTrainer";

                        }
                        catch (Exception)
                        { }
                    }
                }
            }
        }
        private void btnBookingReports_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnmemberReports.Background = Brushes.Gray;
                btnSalesReports.Background = Brushes.Gray;
                btnProspectReports.Background = Brushes.Gray;
                btnBookingReports.Background = Brushes.Chocolate;
                btnFinancialReports.Background = Brushes.Gray;
                btnRetentionReports.Background = Brushes.Gray;
                btnPOSReports.Background = Brushes.Gray;

                HideControls();
                populatedata();
                gvReport.Items.Clear();
                gvReport.Items.Add("All Member Bookings");
                //gvReport.Items.Add("Resource Summary");
            }
            catch (Exception ex)
            {

            }
        }
        private void btnFinancialReports_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnmemberReports.Background = Brushes.Gray;
                btnSalesReports.Background = Brushes.Gray;
                btnProspectReports.Background = Brushes.Gray;
                btnBookingReports.Background = Brushes.Gray;
                btnFinancialReports.Background = Brushes.Chocolate;
                btnRetentionReports.Background = Brushes.Gray;
                btnPOSReports.Background = Brushes.Gray;

                HideControls();
                populatedata();
                gvReport.Items.Clear();
                //gvReport.Items.Add("All Adjustments");
                gvReport.Items.Add("All Payments");
                //gvReport.Items.Add("All Products");
                //gvReport.Items.Add("All Sales");
                gvReport.Items.Add("All Voided Payments");
                //gvReport.Items.Add("Inventory Stock Level");
            }
            catch (Exception ex)
            {

            }
        }
        private void dtfromdate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            int i = gvReport.SelectedIndex;
            if (i != -1)
            {
                if (DateTime.Parse(dtfromdate.Text) > DateTime.Parse(dttodate.Text))
                {
                    dttodate.SelectedDate = dtfromdate.SelectedDate;
                }
                dttodate.DisplayDateStart = dtfromdate.SelectedDate;
            }
        }
        private void dtfromdate_GotMouseCapture(object sender, MouseEventArgs e)
        {
            //if (!dtfromdate.IsDropDownOpen)
            //{
            //    dtfromdate.IsDropDownOpen = true;
            //    //UIElement originalElement = e.OriginalSource as UIElement;
            //    //if (originalElement != null)
            //    //{
            //    //    originalElement.ReleaseMouseCapture();
            //    //}
            //    //Mouse.Capture(null);

            //}
            //else
            //{
            //    dtfromdate.IsDropDownOpen = false;
            //}
            ////UIElement originalElement = e.OriginalSource as UIElement;
            ////if (originalElement != null)
            ////{
            ////    originalElement.ReleaseMouseCapture();
            ////    Mouse.Capture(null);
            ////}
            //if (Mouse.Captured is Calendar || Mouse.Captured is System.Windows.Controls.Primitives.CalendarItem)
            //{
            //    Mouse.Capture(null);
            //}
        }
        private void dtfromdateBirth_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //dtfromdateBirth.DisplayDateStart = dtfromdate.SelectedDate;

            //if (z != 1)
            //{
                //DateTime from_date = DateTime.Parse(dtfromdate.Text);
                //int year = from_date.Year;
                //int i = gvReport.SelectedIndex;
                int i = gvReport.SelectedIndex;
                if (i != -1)
                {
                    string _reportName = gvReport.Items[i].ToString();
                    if (_reportName == "Birthday List")
                    {
                        dttodateBirth.DisplayDateEnd = new DateTime(DateTime.Now.Year, 12, 31);
                        dtfromdateBirth.DisplayDateStart = new DateTime(DateTime.Now.Year, 1, 1);
                        dtfromdateBirth.DisplayDateEnd = new DateTime(DateTime.Now.Year, 12, 31);
                        if (DateTime.Parse(dtfromdateBirth.Text) > DateTime.Parse(dttodateBirth.Text))
                        {
                            dttodateBirth.SelectedDate = dtfromdateBirth.SelectedDate;
                        }
                        dttodateBirth.DisplayDateStart = dtfromdateBirth.SelectedDate;
                    }
                }
            }
        private void btnRetentionReports_Click(object sender, RoutedEventArgs e)
        {
            btnmemberReports.Background = Brushes.Gray;
            btnSalesReports.Background = Brushes.Gray;
            btnProspectReports.Background = Brushes.Gray;
            btnBookingReports.Background = Brushes.Gray;
            btnFinancialReports.Background = Brushes.Gray;
            btnRetentionReports.Background= Brushes.Chocolate;
            btnPOSReports.Background = Brushes.Gray;

            HideControls();
            populatedata();
            gvReport.Items.Clear();
            gvReport.Items.Add("Actions Taken");
            gvReport.Items.Add("All Completed Task");
            gvReport.Items.Add("All Outstanding Task");
            gvReport.Items.Add("All Worked On Task");
            gvReport.Items.Add("Correspondence Sent");
            gvReport.Items.Add("Not Visiting");
            gvReport.Items.Add("Recently Not Visiting");
            gvReport.Items.Add("Visitor Log");
            gvReport.Items.Add("Visits In Period");
            gvReport.Items.Add("Visits Per day");
            //gvReport.Items.Add("Financial");
            //gvReport.Items.Add("Outstanding Accounts");
            //gvReport.Items.Add("Attendance");
        }

        private void btnPOSReports_Click(object sender, RoutedEventArgs e)
        {
            btnmemberReports.Background = Brushes.Gray;
            btnSalesReports.Background = Brushes.Gray;
            btnProspectReports.Background = Brushes.Gray;
            btnBookingReports.Background = Brushes.Gray;
            btnFinancialReports.Background = Brushes.Gray;
            btnRetentionReports.Background = Brushes.Gray;
            btnPOSReports.Background = Brushes.Chocolate;

            HideControls();
            populatedata();
            gvReport.Items.Clear();
            gvReport.Items.Add("All Products");
            gvReport.Items.Add("Inventory Stock Level");
            gvReport.Items.Add("End of day report for point of sale");
            gvReport.Items.Add("End of week report for point of sale");
            gvReport.Items.Add("Periodic Product-wise POS Report");
        }
    }
}

