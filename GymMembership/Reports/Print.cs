﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Shapes;
using System.IO;
using System.IO.Packaging;
using System.Windows.Xps.Packaging;
using System.Windows.Xps;
using System.Data;
using System.Configuration;

using GymMembership.Pointofsale;
using GymMembership.Comman;

namespace GymMembership.Reports
{
    class Print
    {
        private double printAreaWidth = 970;
        private double printAreaHeight = 816;
        public DataGrid printds = new DataGrid();
        public DataGrid _footerds = new DataGrid();

        public bool _frmPOS = false;
        public bool _frmPOSMain = false;
        public bool _frmPOSInventory = false;
        public bool _frmEndOfShift = false;

        public string CompanyName;
        public int _InvNo;

        public string Address_1 = ConfigurationManager.AppSettings["selectedSiteAddr1"].ToString().ToUpper(); // "Address 1";
        public string Address_2 = ConfigurationManager.AppSettings["selectedSiteAddr2"].ToString().ToUpper() + ", " + ConfigurationManager.AppSettings["selectedSiteState"].ToString().ToUpper(); // "Address 2";

        public string TotalSale, Card, Cash, Change, TotalSaleP, OpeningCashBalance, ClosingCashBalance, _discountPer = null, _discountAmt = null, _discSymbol = "%";
        bool _POS_EndOfShift = false;

        public Print
                    (
                        DataGrid _print, DataGrid _footer = null, Window _wOwner = null, bool _fromPOS = false,
                        string  _CompanyName=null,string _Cash=null,string _Change=null,string _Card=null,
                        string _TotalSale=null, int _invNo = 0, bool _fromPOSMain = false, string _TotalSaleP = null, 
                        string _OpeningCashBalance = null, string _ClosingCashBalance = null, bool _EndOfShift = false, 
                        bool _fromPOSInventory = false, string _discountPercent = null, string _discountedAmount = null
                        , string _pdiscSymbol = null
                    )
        {
            try
            {
                printds = _print;
                _footerds = _footer;

                _frmPOS = _fromPOS;
                _frmPOSMain = _fromPOSMain;
                _frmPOSInventory = _fromPOSInventory;
                _POS_EndOfShift = _EndOfShift;

                _discountPer = _discountPercent;
                _discountAmt = _discountedAmount;
                if (_pdiscSymbol != null)
                    _discSymbol = _pdiscSymbol;

                CompanyName = _CompanyName;
                TotalSale = _TotalSale;
                Card = _Card;
                Cash = _Cash;
                Change = _Change;
                TotalSaleP = _TotalSaleP;
                _InvNo = _invNo;
                OpeningCashBalance = _OpeningCashBalance;
                ClosingCashBalance = _ClosingCashBalance;

                //////Create a FlowDocument dynamically.
                ////string tempFileName = System.IO.Path.GetRandomFileName();
                ////File.Delete(tempFileName);
                string tempFileName = Constants.AppPath + "\\" + DateTime.Now.ToString("yyyyMMddhhmmsstt") + "PrintPreview.qyo";
                File.Delete(tempFileName);

                // Create a FlowDocument dynamically.
                if (_fromPOS)
                {
                    printAreaWidth = 275;
                    printAreaHeight = 750;
                }
                FlowDocument doc = CreateFlowDocument(printAreaWidth, printAreaHeight);

                using (XpsDocument xpsDocument = new XpsDocument(tempFileName, FileAccess.ReadWrite))
                {
                    XpsDocumentWriter writer = XpsDocument.CreateXpsDocumentWriter(xpsDocument);
                    writer.Write(((IDocumentPaginatorSource)doc).DocumentPaginator);

                    PrintPreview previewWindow = new PrintPreview
                    {
                        Document = xpsDocument.GetFixedDocumentSequence()
                    };
                    if (_wOwner != null)
                    {
                        previewWindow.Owner = _wOwner;
                    }
                    previewWindow.ShowDialog();
                }

                File.Delete(tempFileName);
            }
            catch (Exception ex)
            {
                string xMsg = ex.Message;
                if (_POS_EndOfShift)
                {
                    Constants.AddToCommunicationLog(0, "INTERNAL", "*** FILE PREVIEW CREATION ERROR ***", ex.Message, "NONE", 0);
                }
            }
        }

        private FlowDocument CreateFlowDocument(double pageWidth, double pageHeight)
        {
            // Create a FlowDocument
            FlowDocument doc = new FlowDocument();
            doc.PageWidth = pageWidth;
            doc.PageHeight = pageHeight;
            doc.ColumnWidth = doc.PageWidth;
            doc.FontFamily = new FontFamily("Arial");
            doc.FontSize = 12;

            if (_frmPOS)
                doc.FontSize = 10;

            if ((_frmPOSMain) || (_frmEndOfShift))
                doc.FontSize = 12.5;

            Table myTable = new Table();
            TableRowGroup rg = new TableRowGroup();

            myTable.BorderThickness = new Thickness(1);
            myTable.BorderBrush = new SolidColorBrush(Colors.Black);
          
            AddHeader(rg, printds);
            if (_frmPOS)
            {
                BlockUIContainer Topblock = new BlockUIContainer(MakeASale_TopCommands());
                doc.Blocks.Add(Topblock);
                AddData_POS(rg, printds);
            }
            else if((_frmPOSMain) || (_frmPOSInventory))
            {
                BlockUIContainer Topblock = new BlockUIContainer(MakeASale_POS_TopCommands());
                doc.Blocks.Add(Topblock);
                AddData(rg, printds);
            }
            else
            {
                AddData(rg, printds);
            }

            if (_frmPOS) //POS - MAKE A SALE - RECEIPT/TAX. INVOICE
            {
                myTable.RowGroups.Add(rg);
                doc.Blocks.Add(myTable);
                BlockUIContainer Bottomblock = new BlockUIContainer(MakeASale_BottomCommands());
                doc.Blocks.Add(Bottomblock);
            }
            else if (_frmPOSMain) //SALE REPORT FROM POS MAIN SCREEN OR END OF SHIFT
            {
                myTable.RowGroups.Add(rg);
                doc.Blocks.Add(myTable);
                BlockUIContainer Bottomblock = new BlockUIContainer(MakeASale_POS_BottomCommands());
                doc.Blocks.Add(Bottomblock);
            }
            else
            {
                myTable.RowGroups.Add(rg);
                doc.Blocks.Add(myTable);
            }


            //BlockUIContainer block = new BlockUIContainer(AddBottomCommands());
            //doc.Blocks.Add(block);

            if (_footerds != null)
            {
                myTable = new Table();
                rg = new TableRowGroup();

                AddHeader(rg, _footerds);
                AddData(rg, _footerds);
                myTable.RowGroups.Add(rg);

                doc.Blocks.Add(myTable);
            }

            return doc;
        }
        private void AddHeader(TableRowGroup rg)
        {
            int j = 0;
            Grid grid = this.GetGrid();
            for (int i = 0; i < printds.Columns.Count; i++)
            {
                grid.Background = new SolidColorBrush(Colors.DarkGray);

                grid.Height = 50;
                if (printds.Columns[i].Visibility == Visibility.Visible)
                {
                    this.AddContentToGrid(grid, printds.Columns[i].Header.ToString(), j, true, HorizontalAlignment.Center, VerticalAlignment.Center);
                    j++;
                }

                if (printds.Columns.Count > 0)
                {
                    var rect = new Rectangle { Fill = new SolidColorBrush(Colors.Black), Height = 1, VerticalAlignment = System.Windows.VerticalAlignment.Bottom };
                    grid.Children.Add(rect);
                    Grid.SetColumnSpan(rect, 15);
                }
            }
            rg.Rows.Add(this.GetRowWithData(grid));
        }

        private void AddHeader(TableRowGroup rg, DataGrid _printds)
        {
            int j = 0;
            Grid grid = this.GetGrid();
            for (int i = 0; i < _printds.Columns.Count; i++)
            {
                grid.Background = new SolidColorBrush(Colors.DarkGray);

                grid.Height = 34; //50
                if (_printds.Columns[i].Visibility == Visibility.Visible)
                {
                    this.AddContentToGrid(grid, _printds.Columns[i].Header.ToString(), j, true, HorizontalAlignment.Center, VerticalAlignment.Center);
                    j++;
                }

                if (_printds.Columns.Count > 0)
                {
                    var rect = new Rectangle { Fill = new SolidColorBrush(Colors.Black), Height = 1, VerticalAlignment = System.Windows.VerticalAlignment.Bottom };
                    grid.Children.Add(rect);
                    Grid.SetColumnSpan(rect, 15);
                }
            }
            rg.Rows.Add(this.GetRowWithData(grid));
        }
        /// <summary>
        /// Method to add data list to print content
        /// </summary>
        /// <param name="rg"></param>
        private void AddData(TableRowGroup rg)
        {
            int j = 0;
            for (int i = 0; i < this.printds.Items.Count; i++)
            {

                Grid grid = this.GetGrid();
                int idx = 0;
                for (j = 0; j < this.printds.Columns.Count;)
                {
                    if (printds.Columns[j].Visibility == Visibility.Visible)
                    {
                        //this.AddContentToGrid(grid, (this.printds.Items[i] as DataRowView).Row[printds.Columns[j].Header.ToString()], j);
                        this.AddContentToGrid(grid, (this.printds.Items[i] as DataRowView).Row[j].ToString(), idx);
                        idx++;
                    }
                    j++;
                }

                if (i < this.printds.Items.Count - 1)
                {
                    var rect = new Rectangle { Fill = new SolidColorBrush(Colors.Black), Height = 1, VerticalAlignment = System.Windows.VerticalAlignment.Bottom };
                    grid.Children.Add(rect);
                    Grid.SetColumnSpan(rect, 15);
                }
                rg.Rows.Add(this.GetRowWithData(grid));
            }
        }

        private void AddData(TableRowGroup rg, DataGrid _printds)
        {
            int j = 0;
            if (_POS_EndOfShift)
            {
                for (int i = 0; i < _printds.Items.Count; i++)
                {

                    Grid grid = this.GetGrid();
                    int idx = 0;
                    for (j = 0; j < _printds.Columns.Count;)
                    {
                        if (_printds.Columns[j].Visibility == Visibility.Visible)
                        {
                            //this.AddContentToGrid(grid, (_printds.Items[i] as DataRowView).Row[j].ToString(), idx);
                            _printds.CurrentColumn = _printds.Columns[j];
                            string _bound = ((_printds.CurrentColumn as DataGridBoundColumn).Binding as Binding).Path.Path.ToString();
                            AddContentToGrid(grid, (_printds.Items[i] as DataRowView).Row[_bound].ToString(), idx);
                            idx++;
                        }
                        j++;
                    }

                    if (i < _printds.Items.Count - 1)
                    {
                        var rect = new Rectangle { Fill = new SolidColorBrush(Colors.Black), Height = 1, VerticalAlignment = System.Windows.VerticalAlignment.Bottom };
                        grid.Children.Add(rect);
                        Grid.SetColumnSpan(rect, 15);
                    }
                    rg.Rows.Add(this.GetRowWithData(grid));
                }
            }
            else
            {
                for (int i = 0; i < _printds.Items.Count; i++)
                {
                    Grid grid = this.GetGrid();
                    int idx = 0;
                    for (j = 0; j < _printds.Columns.Count;)
                    {
                        if (_printds.Columns[j].Visibility == Visibility.Visible)
                        {
                            //this.AddContentToGrid(grid, (_printds.Items[i] as DataRowView).Row[j].ToString(), idx);
                            _printds.CurrentColumn = _printds.Columns[j];
                            string _bound = ((_printds.CurrentColumn as DataGridBoundColumn).Binding as Binding).Path.Path.ToString();
                            AddContentToGrid(grid, (_printds.Items[i] as DataRowView).Row[_bound].ToString(), idx);
                            idx++;
                        }
                        j++;
                    }

                    if (i < _printds.Items.Count - 1)
                    {
                        var rect = new Rectangle { Fill = new SolidColorBrush(Colors.Black), Height = 1, VerticalAlignment = System.Windows.VerticalAlignment.Bottom };
                        grid.Children.Add(rect);
                        Grid.SetColumnSpan(rect, 15);
                    }
                    rg.Rows.Add(this.GetRowWithData(grid));
                }
            }
        }

        private void AddData_POS(TableRowGroup rg, DataGrid _printds)
        {
            int j = 0;
            for (int i = 0; i < _printds.Items.Count; i++)
            {
                _printds.CurrentItem = _printds.Items[i];
                Grid grid = this.GetGrid();
                int idx = 0;
                for (j = 0; j < _printds.Columns.Count;)
                {
                    if (_printds.Columns[j].Visibility == Visibility.Visible)
                    {
                        _printds.CurrentColumn = _printds.Columns[j];
                        string _bound = ((_printds.CurrentColumn as DataGridBoundColumn).Binding as Binding).Path.Path.ToString();
                        object _content;
                        MakeaSale.myItem _myItem = (_printds.CurrentItem as MakeaSale.myItem);
                        switch (_bound.ToUpper())
                        {
                            case "PRODUCTID":
                                _content = _myItem.ProductID;
                                break;
                            case "PRODUCT":
                                _content = _myItem.Product;
                                break;
                            case "QUANTITY":
                                _content = _myItem.Quantity;
                                break;
                            case "CURRSYM":
                                _content = _myItem.CurrSym;
                                break;
                            case "TOTAL":
                                _content = _myItem.Total;
                                break;
                            case "UNITPRICE":
                                _content = _myItem.UnitPrice;
                                break;
                            default:
                                _content = "";
                            break;
                        }
                        AddContentToGrid(grid, _content, idx,true,HorizontalAlignment.Left, VerticalAlignment.Top, 0.3);
                        idx++;
                    }
                    j++;
                }

                if (i < _printds.Items.Count - 1)
                {
                    var rect = new Rectangle { Fill = new SolidColorBrush(Colors.Black), Height = 1, VerticalAlignment = System.Windows.VerticalAlignment.Bottom };
                    grid.Children.Add(rect);
                    Grid.SetColumnSpan(rect, 15);
                }

                rg.Rows.Add(this.GetRowWithData(grid));
            }
        }

        private Grid MakeASale_TopCommands()
        {
            Grid grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

            var lbl = new Label() { Content = CompanyName, FontWeight = FontWeights.Bold ,HorizontalContentAlignment=HorizontalAlignment.Center,FontSize=16, FontFamily= new FontFamily("Castellar")};
            grid.Children.Add(lbl);
            Grid.SetColumnSpan(lbl, 4);

            lbl = new Label() { Content = Address_1, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Center, FontSize = 10 };
            grid.Children.Add(lbl);
            Grid.SetRow(lbl, 1);
            Grid.SetColumnSpan(lbl, 4);

            lbl = new Label() { Content = Address_2, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Center, FontSize = 10 };
            grid.Children.Add(lbl);
            Grid.SetRow(lbl, 2);
            Grid.SetColumnSpan(lbl, 4);

            lbl = new Label() { Content = "Point Of Sale", FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Center ,FontStyle=FontStyles.Italic, FontSize = 15, FontFamily=new FontFamily("Brush Script MT")};
            grid.Children.Add(lbl);
            Grid.SetRow(lbl, 3);
            Grid.SetColumnSpan(lbl, 4);

            lbl = new Label() { Content = " *** TAX INVOICE *** ", FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Center, FontSize = 12 };
            grid.Children.Add(lbl);
            Grid.SetRow(lbl, 4);
            Grid.SetColumnSpan(lbl, 4);

            lbl = new Label() { Content = "Date/Time : " + DateTime.Now.ToString("dd MMM yyyy hh:mm:ss tt"), FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Left,FontSize=11 };
            grid.Children.Add(lbl);
            Grid.SetRow(lbl, 5);
            Grid.SetColumnSpan(lbl, 4);

            lbl = new Label() { Content = "Invoice No.: " + _InvNo.ToString(), FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Left, FontSize = 11 };
            grid.Children.Add(lbl);
            Grid.SetRow(lbl, 6);
            Grid.SetColumnSpan(lbl, 4);

            return grid;
        }
        private Grid MakeASale_POS_TopCommands()
        {
            string _reportTitle = "";

            if (_frmPOSMain)
            {
                _reportTitle = "Sale Report";
            }
            else if (_frmPOSInventory)
            {
                _reportTitle = "Inventory Report";
            }

            Grid grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

            var lbl = new Label() { Content = CompanyName, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Center, FontSize = 18, FontFamily = new FontFamily("Castellar") };
            grid.Children.Add(lbl);
            Grid.SetColumnSpan(lbl, 4);
            
            //"Brush Script MT"
            lbl = new Label() { Content = _reportTitle, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Center, FontStyle = FontStyles.Italic, FontSize = 15, FontFamily = new FontFamily("Cooper Black")};
            grid.Children.Add(lbl);
            Grid.SetRow(lbl, 2);
            Grid.SetColumnSpan(lbl, 4);

            lbl = new Label() { Content = "Report Date/Time : " + DateTime.Now.ToString("dd MMM yyyy hh:mm:ss tt"), FontWeight = FontWeights.Normal, HorizontalContentAlignment = HorizontalAlignment.Left, FontStyle = FontStyles.Normal, FontSize = 11, FontFamily = new FontFamily("Arial") };
            grid.Children.Add(lbl);
            Grid.SetRow(lbl, 4);
            Grid.SetColumnSpan(lbl, 4);

            return grid;
        }
        private Grid MakeASale_BottomCommands()
        {
            double _footerFontSize = 12;

            Grid grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength() });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength() });
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());

            var lbl = new Label() { Content = "Total Sale Amt", FontWeight = FontWeights.Bold,FontSize=_footerFontSize};
            grid.Children.Add(lbl);

            lbl = new Label() { Content = TotalSale, FontWeight = FontWeights.Bold,FontSize = _footerFontSize, HorizontalAlignment = HorizontalAlignment.Right };
            grid.Children.Add(lbl);
            Grid.SetColumn(lbl, 1);

            if ((_discountAmt != null) && (_discountPer != null))
            {
                lbl = new Label() { Content = "Discount", FontWeight = FontWeights.Bold, FontSize = _footerFontSize };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 1);

                string _DiscountLineItem = _discountPer + "%";
                if (_discSymbol == "%")
                    _DiscountLineItem = _discountPer + "%";
                else if (_discSymbol == "$")
                    _DiscountLineItem = "$ " + _discountPer;

                lbl = new Label() { Content = _DiscountLineItem , FontWeight = FontWeights.Bold, FontSize = _footerFontSize, HorizontalAlignment = HorizontalAlignment.Right };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 1);
                Grid.SetColumn(lbl, 1);

                lbl = new Label() { Content = "Nett Sale Amt", FontWeight = FontWeights.Bold, FontSize = _footerFontSize };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 2);

                lbl = new Label() { Content = _discountAmt, FontWeight = FontWeights.Bold, FontSize = _footerFontSize, HorizontalAlignment = HorizontalAlignment.Right };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 2);
                Grid.SetColumn(lbl, 1);

                lbl = new Label() { Content = "Cash", FontWeight = FontWeights.Bold, FontSize = _footerFontSize };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 3);

                lbl = new Label() { Content = Cash, FontWeight = FontWeights.Bold, FontSize = _footerFontSize, HorizontalAlignment = HorizontalAlignment.Right };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 3);
                Grid.SetColumn(lbl, 1);

                lbl = new Label() { Content = "Card", FontWeight = FontWeights.Bold, FontSize = _footerFontSize };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 4);
                lbl = new Label() { Content = Card, FontWeight = FontWeights.Bold, FontSize = _footerFontSize, HorizontalAlignment = HorizontalAlignment.Right };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 4);
                Grid.SetColumn(lbl, 1);

                lbl = new Label() { Content = "Change", FontWeight = FontWeights.Bold, FontSize = _footerFontSize };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 5);
                lbl = new Label() { Content = Change, FontWeight = FontWeights.Bold, FontSize = _footerFontSize, HorizontalAlignment = HorizontalAlignment.Right };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 5);
                Grid.SetColumn(lbl, 1);
            }
            else
            {
                lbl = new Label() { Content = "Cash", FontWeight = FontWeights.Bold, FontSize = _footerFontSize };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 1);

                lbl = new Label() { Content = Cash, FontWeight = FontWeights.Bold, FontSize = _footerFontSize, HorizontalAlignment = HorizontalAlignment.Right };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 1);
                Grid.SetColumn(lbl, 1);

                lbl = new Label() { Content = "Card", FontWeight = FontWeights.Bold, FontSize = _footerFontSize };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 2);
                lbl = new Label() { Content = Card, FontWeight = FontWeights.Bold, FontSize = _footerFontSize, HorizontalAlignment = HorizontalAlignment.Right };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 2);
                Grid.SetColumn(lbl, 1);

                lbl = new Label() { Content = "Change", FontWeight = FontWeights.Bold, FontSize = _footerFontSize };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 3);
                lbl = new Label() { Content = Change, FontWeight = FontWeights.Bold, FontSize = _footerFontSize, HorizontalAlignment = HorizontalAlignment.Right };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 3);
                Grid.SetColumn(lbl, 1);
            }
            return grid;
        }
        private Grid MakeASale_POS_BottomCommands()
        {
            double _footerFontSize = 12.5;

            Grid grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength() });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength() });

            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());
            var lbl = new Label();

            if (OpeningCashBalance != null)
            {
                OpeningCashBalance = Convert.ToDecimal(OpeningCashBalance).ToString("#0.#0");

                lbl = new Label() { Content = "Opening Cash Balance :", FontWeight = FontWeights.Bold, FontSize = _footerFontSize };
                grid.Children.Add(lbl);

                lbl = new Label() { Content = OpeningCashBalance, FontWeight = FontWeights.Bold, FontSize = _footerFontSize, HorizontalAlignment = HorizontalAlignment.Right };
                grid.Children.Add(lbl);
                Grid.SetColumn(lbl, 1);
            }

            lbl = new Label() { Content = "Total Sale :", FontWeight = FontWeights.Bold, FontSize = _footerFontSize };
            grid.Children.Add(lbl);
            Grid.SetRow(lbl, 1);

            lbl = new Label() { Content = TotalSaleP, FontWeight = FontWeights.Bold, FontSize = _footerFontSize, HorizontalAlignment = HorizontalAlignment.Right };
            grid.Children.Add(lbl);
            Grid.SetRow(lbl, 1);
            Grid.SetColumn(lbl, 1);

            lbl = new Label() { Content = "Total Payment Received :", FontWeight = FontWeights.Bold, FontSize = _footerFontSize };
            grid.Children.Add(lbl);
            Grid.SetRow(lbl, 2);

            lbl = new Label() { Content = TotalSale, FontWeight = FontWeights.Bold, FontSize = _footerFontSize, HorizontalAlignment = HorizontalAlignment.Right };
            grid.Children.Add(lbl);
            Grid.SetRow(lbl, 2);
            Grid.SetColumn(lbl, 1);
            
            lbl = new Label() { Content = "Cash : ", FontWeight = FontWeights.Bold, FontSize = _footerFontSize };
            grid.Children.Add(lbl);
            Grid.SetRow(lbl, 3);

            lbl = new Label() { Content = Cash, FontWeight = FontWeights.Bold, FontSize = _footerFontSize, HorizontalAlignment = HorizontalAlignment.Right };
            grid.Children.Add(lbl);
            Grid.SetRow(lbl, 3);
            Grid.SetColumn(lbl, 1);
            
            lbl = new Label() { Content = "Card : ", FontWeight = FontWeights.Bold, FontSize = _footerFontSize };
            grid.Children.Add(lbl);
            Grid.SetRow(lbl, 4);

            lbl = new Label() { Content = Card, FontWeight = FontWeights.Bold, FontSize = _footerFontSize, HorizontalAlignment = HorizontalAlignment.Right };
            grid.Children.Add(lbl);
            Grid.SetRow(lbl, 4);
            Grid.SetColumn(lbl, 1);

            if (ClosingCashBalance != null)
            {
                ClosingCashBalance = Convert.ToDecimal(ClosingCashBalance).ToString("#0.#0");

                lbl = new Label() { Content = "Closing Cash Balance : ", FontWeight = FontWeights.Bold, FontSize = _footerFontSize };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 5);

                lbl = new Label() { Content = ClosingCashBalance, FontWeight = FontWeights.Bold, FontSize = _footerFontSize, HorizontalAlignment = HorizontalAlignment.Right };
                grid.Children.Add(lbl);
                Grid.SetRow(lbl, 5);
                Grid.SetColumn(lbl, 1);
            }

            return grid;
        }

        /// <summary>
        /// Add bottom commands like debit, credit and balance to the last of print records
        /// </summary>
        /// <returns></returns>
        private Grid AddBottomCommands()
        {
            Grid grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });

            return grid;
        }

        private TableRow GetRowWithData(Grid grid)
        {
            BlockUIContainer block = new BlockUIContainer(grid);
            TableCell cell = new TableCell();
            cell.Blocks.Add(block);
            TableRow row = new TableRow();
            row.Cells.Add(cell);
            return row;
        }

        /// <summary>
        /// Returns a grid with columns formatted
        /// </summary>
        /// <returns></returns>
        private Grid GetGrid()
        {
            Grid grid = new Grid();
            for (int i = 0; i < printds.Columns.Count; i++)
            {
                if (printds.Columns[i].Visibility == Visibility.Visible)
                {
                    grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(10, GridUnitType.Star) });
                }
            }
            //grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(110) });
            //grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(80) });
            //grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(80) });
            //grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(80) });
            //grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(140) });
            //grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(20) });
            //grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            //grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(200) });
            return grid;
        }

        /// <summary>
        /// Add content to grid
        /// </summary>
        /// <param name="grid">Grid to add content</param>
        /// <param name="content">content can be any value type</param>
        /// <param name="columnIndex">column index of grid</param>
        /// <param name="isAddRightBorder">true or false, to add border in the right or not</param>
        /// <param name="contentAlignment">horizontal alignment of content</param>
        private void AddContentToGrid(Grid grid, object content, int columnIndex, bool isAddRightBorder = true, HorizontalAlignment contentAlignment = HorizontalAlignment.Left, VerticalAlignment vercontentAlignment = VerticalAlignment.Top, double _cellThickness = 1)
        {
            string _Text = content == null ? string.Empty : content.ToString();
            if (checkNumeric(_Text))
            {
                contentAlignment = HorizontalAlignment.Right;
            }
            var lbl = new TextBlock() { Text = content == null ? string.Empty : content.ToString(), HorizontalAlignment = contentAlignment, VerticalAlignment = vercontentAlignment, TextWrapping = TextWrapping.Wrap, Margin = new Thickness(5, 0, 5, 0) };
            grid.Children.Add(lbl);
            Grid.SetColumn(lbl, columnIndex);
            if (isAddRightBorder)
            {
                var rect = new Rectangle { Fill = new SolidColorBrush(Colors.Black), Width = _cellThickness, HorizontalAlignment = HorizontalAlignment.Right };
                grid.Children.Add(rect);
                Grid.SetColumn(rect, columnIndex);
            }
        }

        private bool checkNumeric(string _Text)
        {
            try
            {
                if (Regex.IsMatch(_Text, @"^\d{1,9}(\.\d{1,2})?$"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
