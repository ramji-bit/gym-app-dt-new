﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Windows.Data;
using System.Globalization;

namespace GymMembership.Comman
{
    public class FormUtilities
    {
        public bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }

    
    public class TextBoxMaskBehavior
    {
        public enum MaskType
        {
            Any,
            Integer,
            Decimal
        }
        public static MaskType GetMask(DependencyObject obj)
        {
            return (MaskType)obj.GetValue(MaskProperty);
        }

        public static void SetMask(DependencyObject obj, MaskType value)
        {
            obj.SetValue(MaskProperty, value);
        }

        public static readonly DependencyProperty MaskProperty =
           DependencyProperty.RegisterAttached(
           "Mask",
           typeof(MaskType),
           typeof(TextBoxMaskBehavior),
           new FrameworkPropertyMetadata(MaskChangedCallback)
           );

        private static void MaskChangedCallback(DependencyObject d,
                            DependencyPropertyChangedEventArgs e)
        {
            // ...
        }
    }

}
