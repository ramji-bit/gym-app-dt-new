﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows;
using System.Security.Cryptography;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;

using GymMembership.MemberInfo;
using GymMembership.Pointofsale;
using Paychoice_Payment;

namespace GymMembership.Comman
{
    public class Constants
    {
        public const string DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
        public const string _EncryptKey = "GymAppElumina";
        public static DataSet _DisConnectedALLTables;
        public static SqlDataAdapter _DisConnectedDataAdapter;
        public static SqlDataAdapter []_DisConnectedDataAdapter_Tables;
        public static string[] _TableNames;
        public static bool _OfflineFlag = true;
        public static string AppPath = System.AppDomain.CurrentDomain.BaseDirectory;
        public const int _maxDHDefer = 1; //2;
        public static bool _OfflineDB = false;
        public static bool _emailBeingSent = false;
        public static bool _dishonourConnMsg = false;

        public static string _appVersion = "";
        //BOOKING HOURS & SLOTS
        public static int _BookingSlotBlockHr = 0;
        public static int _BookingSlotBlockMin = 30;
        public static int _BookingSlotBlockSec = 0;

        public static int _BookingSlotStartHr = 5;
        public static int _BookingSlotStartMin = 0;
        public static int _BookingSlotStartSec = 0;

        public static int _BookingSlotEndHr = 21;
        public static int _BookingSlotEndMin = 0;
        public static int _BookingSlotEndSec = 0;
        public static DateTime _DH_FirstRun_DateTime = DateTime.Now;
        public static bool _SuperAdmin = false;
        //BOOKING HOURS & SLOTS

        public static MailMessage mail = new MailMessage();
        public static SmtpClient client = new SmtpClient();
        public static string passwordEncrypt(string inText, string key)
        {
            //Encrypting a string
            byte[] bytesBuff = Encoding.Unicode.GetBytes(inText);
            using (Aes aes = Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                aes.Key = crypto.GetBytes(32);
                aes.IV = crypto.GetBytes(16);
                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }
                    inText = Convert.ToBase64String(mStream.ToArray());
                }
            }
            return inText;
        }
        public static string passwordDecrypt(string cryptTxt, string key)
        {
            //Decrypting a string
            cryptTxt = cryptTxt.Replace(" ", "+");
            byte[] bytesBuff = Convert.FromBase64String(cryptTxt);
            using (Aes aes = Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                aes.Key = crypto.GetBytes(32);
                aes.IV = crypto.GetBytes(16);
                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }
                    cryptTxt = Encoding.Unicode.GetString(mStream.ToArray());
                }
            }
            return cryptTxt;
        }
        public static int Create_ACCESS_DAT_File(bool _isReset = false, int _MemberNo = 0, string _isMultiple = "", string _datCONTENT="")
        {
            //usp_AccessControl_MembersByCompanySite := Stored Procedure
            try
            {
                string Content = ""; string _Delimiter = "|";
                string filePath; string fileName = "";
                string filePathDAT = "";

                SqlHelper _Sql = new SqlHelper();
                if (_Sql._ConnOpen == 0) return -1;

                Hashtable ht = new Hashtable();
                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                //if (_isMultiple.Trim().ToUpper() == "MULTIPLE")
                //{
                //    _SiteID = 0;
                //}

                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID );
                ht.Add("@MemberNo", _MemberNo);
                ht.Add("@multiSite", _isMultiple);

                DataSet _dsAccessControl = _Sql.ExecuteProcedure("usp_AccessControl_MembersByCompanySite", ht, true);

                if (_dsAccessControl != null)
                {
                    if (_dsAccessControl.Tables.Count > 0)
                    {
                        filePathDAT = ConfigurationManager.AppSettings["AccessControl_FilePath"].ToString();
                        filePath = AppPath;

                        if (filePathDAT.Trim() != "")
                        {
                            fileName = ConfigurationManager.AppSettings["AccessControl_FileName"].ToString();
                            if (!fileName.EndsWith(".DAT"))
                            {
                                fileName = "ACCESS.WIP";
                                filePath = filePath + "ACCESS.WIP";
                                filePathDAT = filePathDAT + "\\" + "ACCESS.DAT"; ;
                            }
                            else
                            {
                                filePathDAT = filePathDAT + "\\" + fileName;
                                filePath = filePath + fileName + ".WIP";
                            }
                        }
                        else
                        {
                            filePathDAT = filePath;
                            filePath = filePath + "ACCESS.WIP";
                            filePathDAT = filePath + "ACCESS.DAT";
                        }
                        System.IO.FileInfo file = new System.IO.FileInfo(filePath);
                        file.Directory.Create(); // If the directory already exists, this method does nothing.
                        Content = "";
                        System.IO.File.WriteAllText(file.FullName, Content);
                        System.IO.StreamWriter _DatFile = new System.IO.StreamWriter(file.FullName, true);

                        if (_isReset)
                        {
                            Content = "P";
                            Content = Content + _Delimiter;
                            Content = Content + "RESET";
                            _DatFile.WriteLine(Content);
                        }

                        if (_dsAccessControl.Tables[0].DefaultView != null)
                        {
                            if (_dsAccessControl.Tables[0].DefaultView.Count > 0)
                            {
                                //FOR SINGLE MEMBER, IF 'DENY' EXISTS
                                if ((_MemberNo != 0) && (_datCONTENT.Trim() != ""))
                                {
                                    _DatFile.WriteLine(_datCONTENT);

                                    ht = new Hashtable();
                                    _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                                    ht.Add("@CompanyID", _CompanyID);
                                    ht.Add("@datContent", _datCONTENT);

                                    _Sql.ExecuteQuery("DenyAccessCard_Insert", ht);
                                }
                                //BEGIN LOOP FOR ALL MEMBERS
                                for (int i = 0; i < _dsAccessControl.Tables[0].DefaultView.Count; i++)
                                {
                                    Content = "P"; Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["cardNumber"].ToString(); Content = Content + _Delimiter;
                                    Content = Content + "C"; Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["MemberName"].ToString(); Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["AccessType"].ToString(); Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["MemberNo"].ToString(); Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["MemberNo"].ToString(); Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["AccessStartDt"].ToString(); Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["AccessStartTime"].ToString(); Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["AccessEndDt"].ToString(); Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["AccessEndTime"].ToString();

                                    _DatFile.WriteLine(Content);
                                }
                            }
                            ////DENY ACCESS FILE REPLICATION
                            if (_dsAccessControl.Tables.Count > 1)
                            {
                                for (int i = 0; i < _dsAccessControl.Tables[1].DefaultView.Count; i++)
                                {
                                    Content = _dsAccessControl.Tables[1].Rows[i]["datContent"].ToString();
                                    _DatFile.WriteLine(Content);
                                }
                            }
                            ////DENY ACCESS FILE REPLICATION
                            //END LOOP FOR ALL MEMBERS
                            _DatFile.Close();
                            if (file.Length > 0)
                                file.CopyTo(filePathDAT, true);

                            file.Delete();
                            return 1;
                        }
                        else
                        {
                            return -1;
                        }
                    }
                    else
                    {
                        return -1;
                    }
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! : \n\n" + ex.Message, "Access DAT File", MessageBoxButton.OK, MessageBoxImage.Information);
                return 0;
            }
        }
        public static int Create_ACCESS_DAT_File_NETWORK(bool _isReset = false, int _MemberNo = 0, string _isMultiple = "", string _datCONTENT = "")
        {
            //usp_AccessControl_MembersByCompanySite := Stored Procedure
            try
            {
                string Content = ""; string _Delimiter = "|";
                string filePath; string fileName = "";
                string filePathDAT = "";

                SqlHelper _Sql = new SqlHelper();
                if (_Sql._ConnOpen == 0) return -1;

                Hashtable ht = new Hashtable();
                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                //if (_isMultiple.Trim().ToUpper() == "MULTIPLE")
                //{
                //    _SiteID = 0;
                //}

                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                ht.Add("@MemberNo", _MemberNo);
                ht.Add("@multiSite", _isMultiple);

                DataSet _dsAccessControl = _Sql.ExecuteProcedure("usp_AccessControl_MembersByCompanySite", ht);

                if (_dsAccessControl != null)
                {
                    if (_dsAccessControl.Tables.Count > 0)
                    {
                        //USE NETWORK SHARED PATH HERE.. INSTEAD OF CURRENT LOCAL PATH FOR .DAT FILE
                        filePathDAT = ConfigurationManager.AppSettings["AccessControl_FilePath"].ToString();
                        filePath = AppPath;
                        //USE NETWORK SHARED PATH HERE.. INSTEAD OF CURRENT LOCAL PATH FOR .DAT FILE

                        if (filePathDAT.Trim() != "")
                        {
                            fileName = ConfigurationManager.AppSettings["AccessControl_FileName"].ToString();
                            if (!fileName.EndsWith(".DAT"))
                            {
                                fileName = "ACCESS.WIP";
                                filePath = filePath + "ACCESS.WIP";
                                filePathDAT = filePathDAT + "\\" + "ACCESS.DAT"; ;
                            }
                            else
                            {
                                filePathDAT = filePathDAT + "\\" + fileName;
                                filePath = filePath + fileName + ".WIP";
                            }
                        }
                        else
                        {
                            filePathDAT = filePath;
                            filePath = filePath + "ACCESS.WIP";
                            filePathDAT = filePath + "ACCESS.DAT";
                        }
                        System.IO.FileInfo file = new System.IO.FileInfo(filePath);
                        file.Directory.Create(); // If the directory already exists, this method does nothing.
                        Content = "";
                        System.IO.File.WriteAllText(file.FullName, Content);
                        System.IO.StreamWriter _DatFile = new System.IO.StreamWriter(file.FullName, true);

                        if (_isReset)
                        {
                            Content = "P";
                            Content = Content + _Delimiter;
                            Content = Content + "RESET";
                            _DatFile.WriteLine(Content);
                        }

                        if (_dsAccessControl.Tables[0].DefaultView != null)
                        {
                            if (_dsAccessControl.Tables[0].DefaultView.Count > 0)
                            {
                                //FOR SINGLE MEMBER, IF 'DENY' EXISTS
                                if ((_MemberNo != 0) && (_datCONTENT.Trim() != ""))
                                {
                                    _DatFile.WriteLine(_datCONTENT);

                                    ht = new Hashtable();
                                    _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                                    ht.Add("@CompanyID", _CompanyID);
                                    ht.Add("@datContent", _datCONTENT);

                                    _Sql.ExecuteQuery("DenyAccessCard_Insert", ht);
                                }
                                //BEGIN LOOP FOR ALL MEMBERS
                                for (int i = 0; i < _dsAccessControl.Tables[0].DefaultView.Count; i++)
                                {
                                    Content = "P"; Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["cardNumber"].ToString(); Content = Content + _Delimiter;
                                    Content = Content + "C"; Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["MemberName"].ToString(); Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["AccessType"].ToString(); Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["MemberNo"].ToString(); Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["MemberNo"].ToString(); Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["AccessStartDt"].ToString(); Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["AccessStartTime"].ToString(); Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["AccessEndDt"].ToString(); Content = Content + _Delimiter;
                                    Content = Content + _dsAccessControl.Tables[0].Rows[i]["AccessEndTime"].ToString();

                                    _DatFile.WriteLine(Content);
                                }
                            }
                            ////DENY ACCESS FILE REPLICATION
                            if (_dsAccessControl.Tables.Count > 1)
                            {
                                for (int i = 0; i < _dsAccessControl.Tables[1].DefaultView.Count; i++)
                                {
                                    Content = _dsAccessControl.Tables[1].Rows[i]["datContent"].ToString();
                                    _DatFile.WriteLine(Content);
                                }
                            }
                            ////DENY ACCESS FILE REPLICATION
                            //END LOOP FOR ALL MEMBERS
                            _DatFile.Close();
                            if (file.Length > 0)
                                file.CopyTo(filePathDAT, true);

                            file.Delete();
                            return 1;
                        }
                        else
                        {
                            return -1;
                        }
                    }
                    else
                    {
                        return -1;
                    }
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! : \n\n" + ex.Message, "Access DAT File", MessageBoxButton.OK, MessageBoxImage.Information);
                return 0;
            }
        }

        public static int Create_ACCESS_DENY_File(string _DATContent, bool _isReset = false, string _oldCardNumber = "")
        {
            try
            {
                string Content = ""; string _Delimiter = "|";
                string filePath; string fileName = "";
                string filePathDAT = "";

                filePathDAT = ConfigurationManager.AppSettings["AccessControl_FilePath"].ToString();
                filePath = AppPath;
                if (filePathDAT.Trim() != "")
                {
                    fileName = ConfigurationManager.AppSettings["AccessControl_FileName"].ToString();
                    if (!fileName.EndsWith(".DAT"))
                    {
                        fileName = "ACCESS.WIP";
                        filePath = filePath + "ACCESS.WIP";
                        filePathDAT = filePathDAT + "\\" + "ACCESS.DAT"; ;
                    }
                    else
                    {
                        filePathDAT = filePathDAT + "\\" + fileName;
                        filePath = filePath + fileName + ".WIP";
                    }
                }
                else
                {
                    filePathDAT = filePath;
                    filePath = filePath + "ACCESS.WIP";
                    filePathDAT = filePath + "ACCESS.DAT";
                }
                System.IO.FileInfo file = new System.IO.FileInfo(filePath);
                file.Directory.Create(); // If the directory already exists, this method does nothing.
                Content = "";
                System.IO.File.WriteAllText(file.FullName, Content);
                System.IO.StreamWriter _DatFile = new System.IO.StreamWriter(file.FullName, true);

                if (_isReset)
                {
                    Content = "P";
                    Content = Content + _Delimiter;
                    Content = Content + "RESET";
                    _DatFile.WriteLine(Content);
                }

                _DatFile.WriteLine(_DATContent);
                _DatFile.Close();
                file.CopyTo(filePathDAT, true);
                file.Delete();

                SqlHelper _Sql = new SqlHelper();
                if (_Sql._ConnOpen == 0) return -1;

                Hashtable ht = new Hashtable();
                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@datContent", _DATContent);
                ht.Add("@oldCardNumber", _oldCardNumber);

                _Sql.ExecuteQuery("DenyAccessCard_Insert", ht);
                
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public static string Create_ACCESS_DOOR_File(string _DoorToOpen)
        {
            try
            {
                string Content = _DoorToOpen;
                string filePath;
                
                filePath = ConfigurationManager.AppSettings["AccessControl_FilePath"].ToString();
                if (filePath.Trim() != "")
                {
                    filePath = filePath + "\\" + ConfigurationManager.AppSettings["AccessControl_FileName"].ToString();
                    if (!filePath.EndsWith(".DAT"))
                    {
                        filePath = filePath + "\\" + "DOOR.DAT";
                    }
                }
                else
                {
                    filePath = "C:\\ACCESS_CONTROL_FILES";
                    filePath = filePath + "\\" + "ACCESS.DAT";
                }
                System.IO.FileInfo file = new System.IO.FileInfo(filePath);
                file.Directory.Create(); // If the directory already exists, this method does nothing.
                Content = "";
                System.IO.File.WriteAllText(file.FullName, Content);
                System.IO.StreamWriter _DatFile = new System.IO.StreamWriter(file.FullName, true);

                Content = _DoorToOpen;
                _DatFile.WriteLine(Content);
                _DatFile.Close();

                return "1";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        public static void Popup_dispatcherTimer_Tick(int _MemberID = 0, int _MemberSiteID = 0)
        {
            SqlHelper _Sql = new SqlHelper();
            if (_Sql._ConnOpen == 0) return;

            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            ht.Add("@MemberID", _MemberID);
            ht.Add("@MemberSiteID", _MemberSiteID);

            DataSet ds = _Sql.ExecuteProcedure("GetMemberCheckInPopUp", ht);

            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    if (ds.Tables[0].DefaultView.Count > 0)
                    {
                        string _MemberName = ds.Tables[0].Rows[0]["MemberName"].ToString().ToUpper();
                        string _CardNumber = ds.Tables[0].Rows[0]["CardNumber"].ToString().ToUpper() + " (" + ds.Tables[0].Rows[0]["AccessType"].ToString().ToUpper() + ")";

                        CheckInPopUp _chkPopUp = new CheckInPopUp(); // _MemberName, _CardNumber);
                        if (ds.Tables[0].Rows[0]["MemberPic"] != DBNull.Value)
                        {
                            _chkPopUp._data = (byte[])ds.Tables[0].Rows[0]["MemberPic"];
                        }
                        _chkPopUp._MemberName = ds.Tables[0].Rows[0]["MemberName"].ToString().ToUpper();
                        _chkPopUp._CardNumber = ds.Tables[0].Rows[0]["CardNumber"].ToString().ToUpper() + " (" + ds.Tables[0].Rows[0]["AccessType"].ToString().ToUpper() + ")";
                        _chkPopUp.Topmost = true;
                        _chkPopUp.lblCardNumber.Content = _CardNumber;
                        _chkPopUp.lblMemberName.Content = _MemberName;
                        _chkPopUp.Show();
                        System.Threading.Thread.Sleep(3000);
                        _chkPopUp.Close();

                        for (int i = 0; i < Application.Current.Windows.Count; i++)
                        {
                            if (Application.Current.Windows[i].Name.ToUpper() == "DASHBOARD")
                            {
                                DateTime dt = DateTime.Now;
                                string s = ds.Tables[0].Rows[0]["MemberName"].ToString().ToUpper() + ", " + ds.Tables[0].Rows[0]["AccessType"].ToString().ToUpper() + ", " + dt.ToString("hh:mm tt");
                                (Application.Current.Windows[i] as Navigation.Dashboard).lstMemberCheckIn.Items.Insert(0,s);
                                //(Application.Current.Windows[i] as Navigation.Dashboard).lstMemberCheckIn.ScrollIntoView((Application.Current.Windows[i] as Navigation.Dashboard).lstMemberCheckIn.Items[(Application.Current.Windows[i] as Navigation.Dashboard).lstMemberCheckIn.Items.Count - 1]);
                                break;
                            }
                        }

                        ht.Clear();
                        ht.Add("@ID", Convert.ToInt32(ds.Tables[0].Rows[0]["ID"].ToString()));
                        ht.Add("@MemberId", 0);
                        ht.Add("@CompanyID", _CompanyID);
                        ht.Add("@SiteID", _SiteID);
                        ht.Add("@Programme", 0);
                        ht.Add("@CheckInDate", DateTime.Today.Date);
                        ht.Add("@Door", "");
                        ht.Add("@CheckInPopup", 1);

                        ds = _Sql.ExecuteProcedure("SaveCheckInDetails", ht);
                    }
                }
            }
        }

        public static void disHonourCheck_Tick(string _subscriptionGUID = "")
        {
            //////Refresh Payment Status Every 90 mins; & Change Access Type (DH) if Payment is 'Dishonoured' & Change Original Access if payment approved!
            SqlHelper _Sql = new SqlHelper();
            //if (_Sql._ConnOpen == 0) return;

            if (_Sql._ConnOpen == 0)
            {
                if (!_dishonourConnMsg)
                {
                    MessageBox.Show("Network / Internet Connectivity required for DDC Status Check!", "Status Check", MessageBoxButton.OK, MessageBoxImage.Information);
                    _dishonourConnMsg = true;
                }
                return;
            }

            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
            Hashtable ht = new Hashtable();

            ////REMOVE THESE LINES AFTER TESTING
            if (_SiteID == 2)
            {
                _SiteID = 2;
            }
            ////REMOVE THESE LINES AFTER TESTING

            ht.Clear();
            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            ht.Add("@Updated", 0);
            ht.Add("@SubscriptionGUID", _subscriptionGUID);

            DataSet _dsDH_Check = _Sql.ExecuteProcedure("GetLastPaymentItem_For_DisHonour_Check", ht);

            if (_dsDH_Check != null)
            {
                if (_dsDH_Check.Tables.Count > 0)
                {
                    if (_dsDH_Check.Tables[0].DefaultView.Count > 0)
                    {
                        Paychoice_Parameter _parameter = new Paychoice_Parameter();
                        string _PaymentScheduleItemGUID = "", _SubscriptionGUID = "", _CustomerGUID = "";

                        for (int i = 0; i < _dsDH_Check.Tables[0].DefaultView.Count; i++)
                        {
                            _CustomerGUID = _dsDH_Check.Tables[0].DefaultView[i]["CustomerGUID"].ToString();
                            _SubscriptionGUID = _dsDH_Check.Tables[0].DefaultView[i]["SubscriptionGUID"].ToString();

                            //<<CHECK IF SUBSCRIPTION GUID IS IN THE 'ACTIVE' LIST BEFORE CALLING PAYMENT SCHEDULE STATUS
                            List<string> _ActivesubscriptionList = _parameter.List_Subscriptions_Active(_CustomerGUID);
                            if (_ActivesubscriptionList != null)
                            {
                                if (_ActivesubscriptionList.Count > 0)
                                {
                                    if (_ActivesubscriptionList.Contains(_SubscriptionGUID))
                                    {
                                        _PaymentScheduleItemGUID = _dsDH_Check.Tables[0].DefaultView[i]["PaymentScheduleItemGUID"].ToString();

                                        string _ret = _parameter.Update_Single_Payment_Schedule_Item_Status(_PaymentScheduleItemGUID, _SubscriptionGUID, _CustomerGUID);
                                        if (_ret.Trim().ToUpper().StartsWith("FAIL"))
                                            break;
                                    }
                                }
                            }
                            //<<CHECK IF SUBSCRIPTION GUID IS IN THE 'ACTIVE' LIST BEFORE CALLING PAYMENT SCHEDULE STATUS
                        }
                    }
                }
            }

            //UPDATE STATUS: IF DISHONOURED, CHANGE ACCESS TYPE to 'DH'
            ht.Clear();
            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            ht.Add("@Updated", 1);

            int _dsDH_Check_Rows = _Sql.ExecuteQuery("GetLastPaymentItem_For_DisHonour_Check", ht);
            //if (_dsDH_Check_Rows > 0)
            //{
            //Create_ACCESS_DAT_File(false, -1, "MULTIPLE");
            //}
            //UPDATE STATUS: IF DISHONOURED, CHANGE ACCESS TYPE to 'DH'

            //UPDATE STATUS: IF APPROVED, CHANGE ACCESS TYPE TO ORIGINAL
            ht.Clear();
            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            ht.Add("@Updated", 2);

            _dsDH_Check_Rows = _Sql.ExecuteQuery("GetLastPaymentItem_For_DisHonour_Check", ht);
            //if (_dsDH_Check_Rows > 0)
            //{
            //Create_ACCESS_DAT_File(false, -2, "MULTIPLE");
            //}
            //UPDATE STATUS: IF APPROVED, CHANGE ACCESS TYPE TO ORIGINAL

            //////Refresh Payment Status Every 90 mins; & Change Access Type (DH) if Payment is 'Dishonoured' & Change Original Access if payment approved!

            ////IF EXPIRED; CHECK M/S END DATE & ARCHIVE IN P.CHOICE
            ht = new Hashtable();
            ht.Clear();
            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);

            _dsDH_Check = _Sql.ExecuteProcedure("GetExpiredMSForArchival", ht);

            if (_dsDH_Check != null)
            {
                if (_dsDH_Check.Tables.Count > 0)
                {
                    if (_dsDH_Check.Tables[0].DefaultView.Count > 0)
                    {
                        Paychoice_Parameter _parameter = new Paychoice_Parameter();
                        string _SubscriptionGUID = "";

                        for (int i = 0; i < _dsDH_Check.Tables[0].DefaultView.Count; i++)
                        {
                            _SubscriptionGUID = _dsDH_Check.Tables[0].DefaultView[i]["Subscription_GUID"].ToString();
                            _parameter.Archive_Subscription(_SubscriptionGUID);

                            ht.Clear();
                            ht.Add("@CompanyID", _CompanyID);
                            ht.Add("@SiteID", _SiteID);
                            ht.Add("@SubscriptionGUID", _SubscriptionGUID);
                            _Sql.ExecuteProcedure("UpdateMSasArchived", ht);
                        }
                    }
                }
            }
            ////IF EXPIRED; CHECK M/S END DATE & ARCHIVE IN P.CHOICE
        }
        public static void expireCheck_Tick()
        {
            ////////IF MEMBERSHIP HAS ALREADY EXPIRED; i.e. END DATE < CURR.DATE THEN UPDATE MEMBERSHIP 'STATUS' (EXPIRED)
            SqlHelper _Sql = new SqlHelper();
            if (_Sql._ConnOpen == 0) return;

            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
            Hashtable ht = new Hashtable();

            ht.Clear();
            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);

            int _dsDH_Check = _Sql.ExecuteQuery("CheckEndDate_ExpireMembership", ht);
            ////////IF MEMBERSHIP HAS ALREADY EXPIRED; i.e. END DATE < CURR.DATE THEN UPDATE MEMBERSHIP 'STATUS' (EXPIRED)
        }
        public static void expireTaskAlert_Tick()
        {
            ////////IF MEMBERSHIP IS ABOUT TO EXPIRE (IN 3 DAYS) - UPDATE TASK
            SqlHelper _Sql = new SqlHelper();
            if (_Sql._ConnOpen == 0) return;

            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
            Hashtable ht = new Hashtable();

            ht.Clear();
            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);

            int _dsDH_Check = _Sql.ExecuteQuery("Membership_Expiry_Task_Alert", ht);
            ////////IF MEMBERSHIP IS ABOUT TO EXPIRE (IN 3 DAYS) - UPDATE TASK
        }
        public static void multiSiteAccessFile_Tick()
        {
            ////////IF ACCESS FILE GENERATED; FOR SINGLE MEMBER - IN ONE SITE, COPY IN ALL SITES
            Create_ACCESS_DAT_File(false, -2, "REPLICATE");
            ////////IF ACCESS FILE GENERATED; FOR SINGLE MEMBER - IN ONE SITE, COPY IN ALL SITES
        }
        public static ClockInCheck POS_CheckClockIn(ClockInCheck _retClockInCheck)
        {
            _retClockInCheck._notClockedIn = true;
            _retClockInCheck._clockedInNotClockedOut = false;
            _retClockInCheck._clockedInAndClockedOut = false;

            int _POS_CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _POS_SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
            int _POS_StaffID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedUserLoginID"].ToString());
            string _POS_StaffUser = ConfigurationManager.AppSettings["LoggedInUser"].ToString();

            DateTime _currDate = DateTime.Now;

            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return _retClockInCheck;

            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _POS_CompanyID);
            ht.Add("@SiteID", _POS_SiteID);
            ht.Add("@StaffID", _POS_StaffID);
            ht.Add("@DateIn", _currDate);
            ht.Add("@StaffUser", _POS_StaffUser);

            //DataSet _dsResult = sql.ExecuteProcedure("POS_Check_ClockIn_For_User", ht);
            DataSet _dsResult = sql.ExecuteProcedure("POS_Check_ClockIn_For_Site", ht);
            int _resultRows = 0;
            if (_dsResult != null)
            {
                if (_dsResult.Tables.Count > 0)
                {
                    _resultRows = Convert.ToInt32(_dsResult.Tables[0].Rows[0]["Result"].ToString());
                }
            }

            if (_resultRows > 0)
            {
                _retClockInCheck._notClockedIn = false;
                _retClockInCheck._clockedInNotClockedOut = true;
                _retClockInCheck._clockedInAndClockedOut = false;

                //_dsResult = sql.ExecuteProcedure("POS_Check_ClockOut_For_User", ht);
                _dsResult = sql.ExecuteProcedure("POS_Check_ClockOut_For_Site", ht);
                if (_dsResult != null)
                {
                    if (_dsResult.Tables.Count > 0)
                    {
                        _resultRows = Convert.ToInt32(_dsResult.Tables[0].Rows[0]["Result"].ToString());
                    }
                }

                if (_resultRows > 0)
                {
                    _retClockInCheck._notClockedIn = false;
                    _retClockInCheck._clockedInNotClockedOut = false;
                    _retClockInCheck._clockedInAndClockedOut = true;

                    return _retClockInCheck;
                }
                else
                {
                    return _retClockInCheck;
                }
            }
            else
            {
                _retClockInCheck._notClockedIn = true;
                _retClockInCheck._clockedInNotClockedOut = false;
                _retClockInCheck._clockedInAndClockedOut = false;

                return _retClockInCheck;
            }
        }
        public static int POS_ClockIn_User(decimal _openBalance)
        {
            int _POS_CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _POS_SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
            int _POS_StaffID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedUserLoginID"].ToString());
            string _POS_StaffUser = ConfigurationManager.AppSettings["LoggedInUser"].ToString();

            DateTime _currDate = DateTime.Now;

            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return 0;

            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _POS_CompanyID);
            ht.Add("@SiteID", _POS_SiteID);
            ht.Add("@StaffID", _POS_StaffID);
            ht.Add("@DateIn", _currDate);
            ht.Add("@StaffUser", _POS_StaffUser);
            ht.Add("@ClockInBalance", _openBalance);
            ht.Add("@appVersion", _appVersion);

            //int _resultRows = sql.ExecuteQuery("POS_ClockIn_User_For_Today", ht);
            int _resultRows = sql.ExecuteQuery("POS_ClockIn_Site_For_Today", ht);
            return _resultRows;
        }
        public static bool DH_Status_Check_FirstRun()
        {
            int _POS_CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _POS_SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            DateTime _currDate = DateTime.Now;

            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return true;

            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _POS_CompanyID);
            ht.Add("@SiteID", _POS_SiteID);
            ht.Add("@DH_DateTime", _currDate);

            DataSet _resultRows = sql.ExecuteProcedure("DH_StatusCheck_Login_FirstRun", ht);
            if (_resultRows != null)
            {
                if (_resultRows.Tables.Count > 0)
                {
                    if (Convert.ToInt32(_resultRows.Tables[0].DefaultView[0]["Result"].ToString()) == 0)
                    {
                        return false;
                    }
                    else
                    {
                        _DH_FirstRun_DateTime = Convert.ToDateTime(_resultRows.Tables[0].DefaultView[0]["FirstRunDateTime"]);
                        return true;
                    }
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        public static bool DH_Status_Check_FirstRun_Company()
        {
            int _POS_CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _POS_SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            DateTime _currDate = DateTime.Now;

            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return true;

            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _POS_CompanyID);
            ht.Add("@SiteID", _POS_SiteID);
            ht.Add("@DH_DateTime", _currDate);

            DataSet _resultRows = sql.ExecuteProcedure("DH_StatusCheck_Login_FirstRun_Company", ht);
            if (_resultRows != null)
            {
                if (_resultRows.Tables.Count > 0)
                {
                    if (Convert.ToInt32(_resultRows.Tables[0].DefaultView[0]["Result"].ToString()) == 0)
                    {
                        return false;
                    }
                    else
                    {
                        _DH_FirstRun_DateTime = Convert.ToDateTime(_resultRows.Tables[0].DefaultView[0]["FirstRunDateTime"]);
                        return true;
                    }
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        public static bool DH_Status_Check_SecondRun()
        {
            int _POS_CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _POS_SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            DateTime _currDate = DateTime.Now;

            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return true;

            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _POS_CompanyID);
            ht.Add("@SiteID", _POS_SiteID);
            ht.Add("@DH_DateTime", _currDate);

            DataSet _resultRows = sql.ExecuteProcedure("DH_StatusCheck_Login_SecondRun", ht);
            if (_resultRows != null)
            {
                if (_resultRows.Tables.Count > 0)
                {
                    if (Convert.ToInt32(_resultRows.Tables[0].DefaultView[0]["Result"].ToString()) == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        public static bool DH_Status_Check_SecondRun_Company()
        {
            int _POS_CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _POS_SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            DateTime _currDate = DateTime.Now;

            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return true;

            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _POS_CompanyID);
            ht.Add("@SiteID", _POS_SiteID);
            ht.Add("@DH_DateTime", _currDate);

            DataSet _resultRows = sql.ExecuteProcedure("DH_StatusCheck_Login_SecondRun_Company", ht);
            if (_resultRows != null)
            {
                if (_resultRows.Tables.Count > 0)
                {
                    if (Convert.ToInt32(_resultRows.Tables[0].DefaultView[0]["Result"].ToString()) == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        public static void DH_StatusCheck_FirstRun_Update()
        {
            int _POS_CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _POS_SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
            string _POS_StaffUser = ConfigurationManager.AppSettings["LoggedInUser"].ToString();

            DateTime _currDate = DateTime.Now;

            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return;

            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _POS_CompanyID);
            ht.Add("@SiteID", _POS_SiteID);
            ht.Add("@DH_DateTime", _currDate);
            ht.Add("@UserName", _POS_StaffUser);
            ht.Add("@FirstRunAppVersion", _appVersion);

            sql.ExecuteProcedure("DH_StatusCheck_FirstRun_Update", ht);
        }
        public static void DH_StatusCheck_SecondRun_Update()
        {
            int _POS_CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _POS_SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
            string _POS_StaffUser = ConfigurationManager.AppSettings["LoggedInUser"].ToString();

            DateTime _currDate = DateTime.Now;

            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return;

            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _POS_CompanyID);
            ht.Add("@SiteID", _POS_SiteID);
            ht.Add("@DH_DateTime", _currDate);
            ht.Add("@UserName", _POS_StaffUser);
            ht.Add("@SecondRunAppVersion", _appVersion);

            sql.ExecuteProcedure("DH_StatusCheck_SecondRun_Update", ht);
        }
        public static DataSet GetAllStaffDetails()
        {
            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return null;

            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);

            DataSet ds = sql.ExecuteProcedure("StaffRoster_GetAllStaffNames", ht);
            return ds;
        }
        public static void AddToCommunicationLog(int _receiverID, string _receiverType, string _commSubject, string _commBody, string _receiverEmailID, int _commStatus = 1)
        {
            try
            {
                if (_receiverID >= 0)
                {
                    //GET COMMUNICATION LOG
                    int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                    int _staffID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedUserStaffID"].ToString());
                    string _staffName = ConfigurationManager.AppSettings["LoggedInUser"].ToString().ToUpper();

                    Hashtable ht = new Hashtable();
                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@SiteID", _SiteID);
                    ht.Add("@receiverID", _receiverID);
                    ht.Add("@receiverType", _receiverType);
                    ht.Add("@commType", "EMAIL");
                    ht.Add("@staffID", _staffID);
                    ht.Add("@staffName", _staffName);
                    ht.Add("@commSubject", _commSubject);
                    ht.Add("@commBody", _commBody);
                    ht.Add("@receiverEmailID", _receiverEmailID);
                    ht.Add("@commStatus", _commStatus);
                    ht.Add("@dateSent", DateTime.Now);

                    SqlHelper _sql = new SqlHelper();
                    //do
                    //{
                    //} while (_sql._ConnOpen == 0);
                    if (_sql._ConnOpen == 0)
                        return;

                    _sql.ExecuteQuery("SaveCommunicationHistory", ht);
                }
            }
            catch (Exception ex)
            {
                string _exMsg = ex.Message;
            }
        }
        public static void disconnected_DataSet()
        {
            try
            {
                SqlHelper _Sql = new SqlHelper();
                if (_Sql._ConnOpen == 0) return;

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                Hashtable ht = new Hashtable();

                ht.Add("@CompanyID", _CompanyID);

                //_DisConnectedDataAdapter  = _Sql.getDisConnectedDataAdapter("Get_All_Tables_Disconnected", ht);
                 _DisConnectedDataAdapter = new SqlDataAdapter();
                _DisConnectedDataAdapter = _Sql.getDisConnectedDataAdapter("Get_All_Tables_Disconnected_NEW", ht);
                _DisConnectedALLTables = new DataSet();
                _DisConnectedDataAdapter.Fill(_DisConnectedALLTables);
                _DisConnectedDataAdapter.Dispose();

                ////*********UNCOMMENT LINES FOR OFF-LINE
                //_OfflineFlag = false;
                ////*********UNCOMMENT LINES FOR OFF-LINE

                //if (_DisConnectedALLTables != null)
                //{
                //    for (int i = 0; i < _DisConnectedALLTables.Tables[0].Rows.Count; i++)
                //    {
                //        string _TableName = _DisConnectedALLTables.Tables[0].Rows[i][0].ToString();
                //        string _Query = "SELECT * FROM " + _TableName;
                //        _TableNames[i] = _TableName;
                //        _DisConnectedDataAdapter_Tables[i] = _Sql.ExecuteQueryReturnDataAdapter(_Query);
                //    }
                //    _OfflineFlag = false;
                //}
                //else
                //{
                //    _OfflineFlag = true;
                //}
            }
            catch (Exception ex)
            {
                _OfflineFlag = true;
                return;
            }
        }

        public static void _HideDashBoard()
        {
            for (int i = 0; i < Application.Current.Windows.Count; i++)
            {
                if (Application.Current.Windows[i].Name.ToUpper() == "DASHBOARD")
                {
                    Application.Current.Windows[i].Hide();
                    break;
                }
            }
        }
        public static void _ShowDashBoard()
        {
            for (int i = 0; i < Application.Current.Windows.Count; i++)
            {
                if (Application.Current.Windows[i].Name.ToUpper() == "DASHBOARD")
                {
                    Application.Current.Windows[i].Show();
                    break;
                }
            }
        }
        public static BitmapImage _getBitMapImage(Bitmap _bitmap)
        {
            BitmapImage bitmapImage = new BitmapImage();
            using (MemoryStream memory = new MemoryStream())
            {
                _bitmap.Save(memory, ImageFormat.Png);
                memory.Position = 0;
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
            }
            return bitmapImage;
        }
        public static void sendEmail(string strFrom, string strTo, string strSubject, string strBody, string _filePath, string _fromDisplayName, string _emailType)
        {
            try
            {
                client = new SmtpClient();
                client.UseDefaultCredentials = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                if (_fromDisplayName.Trim() == "") _fromDisplayName = "POS End Of Shift Report";
                if (_emailType.Trim() == "") _emailType = "INTERNAL";

                ////////////////////////////////////////// DO NOT CHANGE THE BELOW LINES ////////////////////////////////////////
                client.Port = 587;
                client.Host = "SMTP.GMAIL.COM";
                string _strFrom = "noreply.reda@gmail.com";
                string _strDecryptPassWord = passwordDecrypt("fMKQ+IINHCVpOzskqoKffaT7hdtbE+xxX1g5Ybpw+6AATKV9AxcNhvAZ9rZL7uD/",
                                                        Comman.Constants._EncryptKey);
                if ((ConfigurationManager.AppSettings["REDAAutoEmailID"].ToString().Trim() != "") &&
                    (ConfigurationManager.AppSettings["REDAAutoEmailEncPwd"].ToString().Trim() != ""))
                {
                    _strFrom = ConfigurationManager.AppSettings["REDAAutoEmailID"].ToString().Trim();
                    _strDecryptPassWord = passwordDecrypt(ConfigurationManager.AppSettings["REDAAutoEmailEncPwd"].ToString(),
                                                            Comman.Constants._EncryptKey);
                }
                ////////////////////////////////////////// DO NOT CHANGE THE BELOW LINES ////////////////////////////////////////

                strFrom = _strFrom;
                MailAddress _mFrom = new MailAddress(_strFrom, _fromDisplayName);
                MailAddress _mTo = new MailAddress(strTo);

                mail = new MailMessage(_mFrom, _mTo);
                mail.To.Add(strTo);

                mail.IsBodyHtml = true;
                System.Net.NetworkCredential objSMTPUserInfo = new System.Net.NetworkCredential(_strFrom, _strDecryptPassWord);
                client.Credentials = objSMTPUserInfo;
                client.EnableSsl = true;
                mail.Subject = strSubject;

                if (mail.IsBodyHtml == true)
                {
                    strBody = strBody.Replace("\n", "<br>");
                }
                else
                {
                    strBody = strBody.Replace("<br>", "\n");
                }
                mail.Body = strBody;
                if (_filePath != "")
                {
                    Attachment _fileAttachment = new Attachment(_filePath);
                    mail.Attachments.Add(_fileAttachment);
                }
                client.Send(mail);
                //client.SendMailAsync(mail);

                mail.Dispose();
                AddToCommunicationLog(0, _emailType, strSubject, strBody, strTo, 1);
                _emailBeingSent = false;
            }
            catch (Exception ex)
            {
                mail.Dispose();

                try
                {
                    client = new SmtpClient();
                    client.UseDefaultCredentials = true;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;

                    if (_fromDisplayName.Trim() == "") _fromDisplayName = "POS End Of Shift Report";
                    if (_emailType.Trim() == "") _emailType = "INTERNAL";

                    ////////////////////////////////////////// DO NOT CHANGE THE BELOW LINES ////////////////////////////////////////
                    client.Port = 587;
                    client.Host = "SMTP.GMAIL.COM";
                    string _strFrom = "noreply.reda@gmail.com";
                    string _strDecryptPassWord = passwordDecrypt("fMKQ+IINHCVpOzskqoKffaT7hdtbE+xxX1g5Ybpw+6AATKV9AxcNhvAZ9rZL7uD/",
                                                            Comman.Constants._EncryptKey);
                    if ((ConfigurationManager.AppSettings["REDAAutoEmailID"].ToString().Trim() != "") &&
                        (ConfigurationManager.AppSettings["REDAAutoEmailEncPwd"].ToString().Trim() != ""))
                    {
                        _strFrom = ConfigurationManager.AppSettings["REDAAutoEmailID"].ToString().Trim();
                        _strDecryptPassWord = passwordDecrypt(ConfigurationManager.AppSettings["REDAAutoEmailEncPwd"].ToString(),
                                                                Comman.Constants._EncryptKey);
                    }
                    ////////////////////////////////////////// DO NOT CHANGE THE BELOW LINES ////////////////////////////////////////

                    strFrom = _strFrom;
                    MailAddress _mFrom = new MailAddress(_strFrom, _fromDisplayName);
                    MailAddress _mTo = new MailAddress(strTo);

                    mail = new MailMessage(_mFrom, _mTo);
                    mail.To.Add(strTo);

                    mail.IsBodyHtml = true;
                    System.Net.NetworkCredential objSMTPUserInfo = new System.Net.NetworkCredential(_strFrom, _strDecryptPassWord);
                    client.Credentials = objSMTPUserInfo;
                    client.EnableSsl = true;
                    mail.Subject = strSubject;

                    if (mail.IsBodyHtml == true)
                    {
                        strBody = strBody.Replace("\n", "<br>");
                    }
                    else
                    {
                        strBody = strBody.Replace("<br>", "\n");
                    }
                    mail.Body = strBody;
                    if (_filePath != "")
                    {
                        Attachment _fileAttachment = new Attachment(_filePath);
                        mail.Attachments.Add(_fileAttachment);
                    }
                    client.Send(mail);
                    //client.SendMailAsync(mail);

                    mail.Dispose();
                    AddToCommunicationLog(0, _emailType, strSubject, strBody, strTo, 1);
                    _emailBeingSent = false;
                }
                catch (Exception x)
                {
                    try
                    {
                        AddToCommunicationLog(0, _emailType, "*** EMAIL ERROR ***", ex.Message, strTo, 0);
                        _emailBeingSent = false;
                        mail.Dispose();
                        MessageBox.Show("Email Send Error..!\n\n" + x.Message, "End Of Shift", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    catch (Exception x1)
                    {
                        string s = x1.Message;
                        MessageBox.Show("Email Send Error..!\n\n" + x1.Message, "End Of Shift", MessageBoxButton.OK, MessageBoxImage.Information);
                        Comman.Constants._emailBeingSent = false;
                    }
                }
            }
        }
        ////// MIME - CONTENT TYPES
        //        {".bmp", "image/bmp"},
        //        {".gif", "image/gif"},
        //        {".jpeg", "image/jpeg"},
        //        {".jpg", "image/jpeg"},
        //        {".png", "image/png"},
        //        {".tif", "image/tiff"},
        //        {".tiff", "image/tiff"},
        //        {".doc", "application/msword"},
        //        {".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
        //        {".pdf", "application/pdf"},
        //        {".ppt", "application/vnd.ms-powerpoint"},
        //        {".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
        //        {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
        //        {".xls", "application/vnd.ms-excel"},
        //        {".csv", "text/csv"},
        //        {".xml", "text/xml"},
        //        {".txt", "text/plain"},
        //        {".zip", "application/zip"},
        //        {".ogg", "application/ogg"},
        //        {".mp3", "audio/mpeg"},
        //        {".wma", "audio/x-ms-wma"},
        //        {".wav", "audio/x-wav"},
        //        {".wmv", "audio/x-ms-wmv"},
        //        {".swf", "application/x-shockwave-flash"},
        //        {".avi", "video/avi"},
        //        {".mp4", "video/mp4"},
        //        {".mpeg", "video/mpeg"},
        //        {".mpg", "video/mpeg"},
        //        {".qt", "video/quicktime"}
    }
}
