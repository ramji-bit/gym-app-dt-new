﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Threading.Tasks;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Windows.Controls;
using System.Windows;

using GymMembership.BAL;

namespace GymMembership.Comman
{
    public class Utilities 
    {
        //commenting the below Class instances. This will slow down for all screens. Whenever required, only in that
        //method these need to be instantiated!!

        //MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
        //CompanySiteDetailsBAL _CSDetailsBAL = new CompanySiteDetailsBAL();
        //Product _utilProd = new Product();
        //SettingsBAL _settingsBal = new SettingsBAL();
        //BookingBAL _bookingBAL = new BookingBAL();
        //TaskBAL _taskBAL = new TaskBAL();
        public void populatePersonalTrainer(ComboBox cmbPersonalTrainer)
        {
            MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
            DataSet ds = _memberDetailsBal.get_PersonalTrainer();
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbPersonalTrainer.ItemsSource = ds.Tables[0].DefaultView;
                        cmbPersonalTrainer.DisplayMemberPath = "TrainerName";
                        cmbPersonalTrainer.SelectedValuePath = "StaffID";
                        cmbPersonalTrainer.SelectedIndex = 0;
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void populatePersonalTrainer_TaskFilter(ComboBox cmbPersonalTrainer, int _Flag = 0)
        {
            MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
            DataSet ds = _memberDetailsBal.get_PersonalTrainer_TaskFilter(_Flag);
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbPersonalTrainer.ItemsSource = ds.Tables[0].DefaultView;
                        cmbPersonalTrainer.DisplayMemberPath = "TrainerName";
                        cmbPersonalTrainer.SelectedValuePath = "StaffID";
                        cmbPersonalTrainer.SelectedIndex = 0;
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void populateAllActiveStaff(ComboBox cmbPersonalTrainer)
        {
            MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
            DataSet ds = _memberDetailsBal.get_AllActiveStaff();
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbPersonalTrainer.ItemsSource = ds.Tables[0].DefaultView;
                        cmbPersonalTrainer.DisplayMemberPath = "TrainerName";
                        cmbPersonalTrainer.SelectedValuePath = "StaffID";
                        cmbPersonalTrainer.SelectedIndex = 0;
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void populateInvolvementType(ComboBox cmbInvolvementType)
        {
            MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
            DataSet ds = _memberDetailsBal.get_InvolvementType();
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbInvolvementType.ItemsSource = ds.Tables[0].DefaultView;
                        cmbInvolvementType.DisplayMemberPath = "InvolvementTypeName";
                        cmbInvolvementType.SelectedValuePath = "InvolvementTypeID";
                        cmbInvolvementType.SelectedIndex = 0;
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void populateProgramGroup(ComboBox cmbMS_ProgramGroup)
        {
            MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
            DataSet ds = _memberDetailsBal.get_ProgramGroup();
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbMS_ProgramGroup.ItemsSource = ds.Tables[0].DefaultView;
                        cmbMS_ProgramGroup.DisplayMemberPath = "GroupsName";
                        cmbMS_ProgramGroup.SelectedValuePath = "GroupsID";
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void populateAccessTypes(ComboBox cmbAccessType)
        {
            MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
            DataSet ds = _memberDetailsBal.get_AccessTypes();
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbAccessType.ItemsSource = ds.Tables[0].DefaultView;
                        cmbAccessType.DisplayMemberPath = "AccessTypeName";
                        cmbAccessType.SelectedValuePath = "ID";
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void populateALLProgramGroup(ComboBox cmbMS_ProgramGroup)
        {
            MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
            DataSet ds = _memberDetailsBal.get_ALLProgramGroup();
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbMS_ProgramGroup.ItemsSource = ds.Tables[0].DefaultView;
                        cmbMS_ProgramGroup.DisplayMemberPath = "GroupsName";
                        cmbMS_ProgramGroup.SelectedValuePath = "GroupsID";
                        cmbMS_ProgramGroup.SelectedIndex = 0;
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void populateProgram(ComboBox cmbMS_Program)
        {
            MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
            DataSet ds = _memberDetailsBal.get_Program();
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbMS_Program.ItemsSource = ds.Tables[0].DefaultView;
                        cmbMS_Program.DisplayMemberPath = "ProgrammeName";
                        cmbMS_Program.SelectedValuePath = "ProgrammeID";
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void populateProgramFindMember (ComboBox cmbMS_Program)
        {
            MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
            DataSet ds = _memberDetailsBal.get_ProgramFindMember();
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbMS_Program.ItemsSource = ds.Tables[0].DefaultView;
                        cmbMS_Program.DisplayMemberPath = "ProgrammeName";
                        cmbMS_Program.SelectedValuePath = "ProgrammeID";
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void get_ProgramByGroup(ComboBox cmbProgram, int ProgramGroupId, int _Status = 1)
        {
            MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
            DataSet ds = _memberDetailsBal.get_ProgramByGroup(ProgramGroupId, _Status);
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbProgram.ItemsSource = ds.Tables[0].DefaultView;
                        cmbProgram.DisplayMemberPath = "ProgrammeName";
                        cmbProgram.SelectedValuePath = "ProgrammeID";
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void populateBusinessPromotionDetails(ComboBox cmbJoiningDetailsClientManager)
        {
            MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
            DataSet ds = _memberDetailsBal.get_BusinessPromotionDetails();
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbJoiningDetailsClientManager.ItemsSource = ds.Tables[0].DefaultView;
                        cmbJoiningDetailsClientManager.DisplayMemberPath = "PromotionDetails";
                        cmbJoiningDetailsClientManager.SelectedValuePath = "PromotionId";
                        cmbJoiningDetailsClientManager.SelectedIndex = 0;
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void populateOrgCompanyName (ComboBox cmbOrgname)
        {
            CompanySiteDetailsBAL _CSDetailsBAL = new CompanySiteDetailsBAL();
            DataSet ds = _CSDetailsBAL.get_OrgCompanyName();
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbOrgname.ItemsSource = ds.Tables[0].DefaultView;
                        cmbOrgname.DisplayMemberPath = "CompanyName";
                        cmbOrgname.SelectedValuePath = "CompanyID";
                        cmbOrgname.SelectedIndex = -1;
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void populateSiteNamebyCompany(ComboBox cmbSiteName, string _CompanyName)
        {
            CompanySiteDetailsBAL _CSDetailsBAL = new CompanySiteDetailsBAL();
            DataSet ds = _CSDetailsBAL.get_SitebyCompanyName(_CompanyName);
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbSiteName.ItemsSource = ds.Tables[0].DefaultView;
                        cmbSiteName.DisplayMemberPath = "SiteName";
                        cmbSiteName.SelectedValuePath = "SiteID";
                        cmbSiteName.SelectedIndex = -1;
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void populateProductTypes(ComboBox cmbProdType)
        {
            Product _utilProd = new Product();
            DataSet ds = _utilProd.getProductTypes();
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbProdType.ItemsSource = ds.Tables[0].DefaultView;
                        cmbProdType.DisplayMemberPath = "ProductTypeName";
                        cmbProdType.SelectedValuePath = "ProductTypeID";
                        cmbProdType.SelectedIndex = -1;
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void populateProductTypes_Inventory(ComboBox cmbProdType)
        {
            Product _utilProd = new Product();
            DataSet ds = _utilProd.getProductTypes_Inventory();
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbProdType.ItemsSource = ds.Tables[0].DefaultView;
                        cmbProdType.DisplayMemberPath = "ProductTypeName";
                        cmbProdType.SelectedValuePath = "ProductTypeID";
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void populateAllProducts(ComboBox cmbProd)
        {
            Product _utilProd = new Product();
            DataSet ds = _utilProd.getAllProducts();
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbProd.ItemsSource = ds.Tables[0].DefaultView;
                        cmbProd.DisplayMemberPath = "TypeAndProduct";
                        cmbProd.SelectedValuePath = "ProductID";
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void populateProductsByType(ComboBox cmbProducts, string _ProdType, ComboBox cmbProdSalePrice, ComboBox cmbProdCostPrice)
        {
            Product _utilProd = new Product();
            DataSet ds = _utilProd.getProductsbyType(_ProdType);
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbProducts.ItemsSource = ds.Tables[0].DefaultView;
                        cmbProducts.DisplayMemberPath = "ProductName";
                        cmbProducts.SelectedValuePath = "ProductID";
                        cmbProducts.SelectedIndex = -1;

                        cmbProdCostPrice.ItemsSource = ds.Tables[0].DefaultView;
                        cmbProdCostPrice.DisplayMemberPath = "ProductCostPrice";
                        cmbProdCostPrice.SelectedIndex = -1;

                        cmbProdSalePrice.ItemsSource = ds.Tables[0].DefaultView;
                        cmbProdSalePrice.DisplayMemberPath = "ProductSalePrice";
                        cmbProdSalePrice.SelectedIndex = -1;
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void get_MembershipGroupById(ComboBox cmbPGGroupName, int GroupId)
        {
            SettingsBAL _settingsBal = new SettingsBAL();
            DataSet ds = _settingsBal.get_MembershipGroupById(GroupId);
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbPGGroupName.ItemsSource = ds.Tables[0].DefaultView;
                        cmbPGGroupName.DisplayMemberPath = "GroupName";
                        cmbPGGroupName.SelectedValuePath = "GroupID";
                        cmbPGGroupName.SelectedIndex = 0;
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void PopulatePaymentType(ComboBox cmbMSPaymentMtd)
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                DataSet ds = _memberDetailsBal.get_PaymentType();

                if (ds != null)
                {
                    if (ds.Tables[0] != null)
                    {
                        cmbMSPaymentMtd.ItemsSource = ds.Tables[0].DefaultView;
                        cmbMSPaymentMtd.DisplayMemberPath = "PaymentType";
                        cmbMSPaymentMtd.SelectedValuePath = "PaymentTypeId";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR!", MessageBoxButton.OK, MessageBoxImage.Hand);
            }

        }
        public void PopulateInvoiceNoByMemberId(ComboBox cmbMSForaInvoice, int MemberId)
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                DataSet ds = _memberDetailsBal.get_InvoiceNoByMemberId(MemberId);

                if (ds != null)
                {
                    if (ds.Tables[0] != null)
                    {
                        cmbMSForaInvoice.ItemsSource = ds.Tables[0].DefaultView;
                        cmbMSForaInvoice.DisplayMemberPath = "InvoiceNumber";
                        cmbMSForaInvoice.SelectedValuePath = "InvoiceNumber";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR!", MessageBoxButton.OK, MessageBoxImage.Hand);
            }

        }
        public void populateMemberNumber(ComboBox cmbMemberNo, int _SiteID = 0)
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                DataSet ds = _memberDetailsBal.get_ALLMemberDetails(_SiteID);
                if (ds != null)
                {
                    if (ds.Tables[0] != null)
                    {
                        cmbMemberNo.ItemsSource = ds.Tables[0].DefaultView;
                        cmbMemberNo.DisplayMemberPath = "MemberNo";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR!", MessageBoxButton.OK, MessageBoxImage.Hand);
            }
        }
        public void populateMemberName(ComboBox cmbMemberNo)
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                DataSet ds = _memberDetailsBal.get_ALLMemberDetails();
                if (ds != null)
                {
                    if (ds.Tables[0] != null)
                    {
                        cmbMemberNo.ItemsSource = ds.Tables[0].DefaultView;
                        cmbMemberNo.DisplayMemberPath = "MemberName";
                        cmbMemberNo.SelectedValuePath = "MemberNo";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR!", MessageBoxButton.OK, MessageBoxImage.Hand);
            }
        }
        public void PopulateResourceTypes(ComboBox cmbResourceType)
        {
            try
            {
                BookingBAL _bookingBAL = new BookingBAL();
                DataSet ds = _bookingBAL.get_ResourceTypes();
                if (ds != null)
                {
                    if (ds.Tables[0] != null)
                    {
                        if (ds.Tables[0].DefaultView.Count > 0)
                        {
                            cmbResourceType.ItemsSource = ds.Tables[0].DefaultView;
                            cmbResourceType.DisplayMemberPath = "ResourceTypeName";
                            cmbResourceType.SelectedValuePath = "ResourceTypeID";

                            cmbResourceType.SelectedIndex = 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR!", MessageBoxButton.OK, MessageBoxImage.Hand);
            }
        }
        public void populateSupplierName(ComboBox cmb_SupplierName)
        {
            SettingsBAL _settingsBal = new SettingsBAL();
            DataSet ds = _settingsBal.get_SupplierName();
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmb_SupplierName.ItemsSource = ds.Tables[0].DefaultView;
                        cmb_SupplierName.DisplayMemberPath = "SupplierName";
                        cmb_SupplierName.SelectedValuePath = "SupplierID";
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void PopulateTaskType(ComboBox cmbTaskType, int TaskTypeId)
        {
            try
            {
                TaskBAL _taskBAL = new TaskBAL();
                DataSet ds = _taskBAL.get_TasKType(TaskTypeId);
                if (ds != null)
                {
                    if (ds.Tables[0] != null)
                    {
                        cmbTaskType.ItemsSource = ds.Tables[0].DefaultView;
                        cmbTaskType.DisplayMemberPath = "TaskTypeName";
                        cmbTaskType.SelectedValuePath = "TaskTypeId";
                        //cmbTaskType.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR!", MessageBoxButton.OK, MessageBoxImage.Hand);
            }
        }
        public void populateResourceTypeById(ComboBox cmb_ResourceType)
        {
            SettingsBAL _settingsBal = new SettingsBAL();
            DataSet ds = _settingsBal.get_ResourceType();
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmb_ResourceType.ItemsSource = ds.Tables[0].DefaultView;
                        cmb_ResourceType.DisplayMemberPath = "ResourceTypeName";
                        cmb_ResourceType.SelectedValuePath = "ResourceTypeID";
                    }
                    catch (Exception)
                    { }
                }
            }
        }

        public void get_ResourcesByID(ComboBox cmbResources, int ResourceTypeId)
        {
            SettingsBAL _settingsBal = new SettingsBAL();
            DataSet ds = _settingsBal.get_ResourceById(ResourceTypeId);
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbResources.ItemsSource = ds.Tables[0].DefaultView;
                        cmbResources.DisplayMemberPath = "FacilityName";
                        cmbResources.SelectedValuePath = "FacilityId";
                    }
                    catch (Exception)
                    { }
                }
            }
        }

        public void populateBillingCompany(ComboBox cmbBillingCompany)
        {
            try
            {
                BillingBAL _billingBAL = new BillingBAL();
                DataSet ds = _billingBAL.get_BillingCompany();
                if (ds != null)
                {
                    if (ds.Tables[0] != null)
                    {
                        cmbBillingCompany.ItemsSource = ds.Tables[0].DefaultView;
                        cmbBillingCompany.DisplayMemberPath = "BillingCompanyName";
                        cmbBillingCompany.SelectedValuePath = "BillingCompanyId";
                        cmbBillingCompany.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR!", MessageBoxButton.OK, MessageBoxImage.Hand);
            }
        }
        public void populateBankCardType(ComboBox cbCC_Type)
        {
            try
            {
                BillingBAL _billingBAL = new BillingBAL();
                DataSet ds = _billingBAL.get_BankCardType();

                if (ds != null)
                {
                    if (ds.Tables[0] != null)
                    {
                        cbCC_Type.ItemsSource = ds.Tables[0].DefaultView;
                        cbCC_Type.DisplayMemberPath = "CardTypeName";
                        cbCC_Type.SelectedValuePath = "CardTypeId";
                        cbCC_Type.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR!", MessageBoxButton.OK, MessageBoxImage.Hand);
            }
        }

        public void GetActiveUserNamesUAdmin(ComboBox cmbUserName)
        {
            try
            {
                SettingsBAL _SettingsBal = new SettingsBAL();
                DataSet ds = _SettingsBal.GetActiveUserNamesUAdmin();

                if (ds != null)
                {
                    if (ds.Tables[0] != null)
                    {
                        cmbUserName.ItemsSource = ds.Tables[0].DefaultView;
                        cmbUserName.DisplayMemberPath = "Name";
                        cmbUserName.SelectedValuePath = "LoginID";
                        cmbUserName.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR!", MessageBoxButton.OK, MessageBoxImage.Hand);
            }
        }
        public void GetAllContactTypes(ListBox lstContactTypes)
        {
            try
            {
                SettingsBAL _SettingsBal = new SettingsBAL();
                DataSet ds = _SettingsBal.GetAllContactTypes();

                if (ds != null)
                {
                    if (ds.Tables[0] != null)
                    {
                        if (ds.Tables[0].DefaultView.Count > 0)
                        {
                            lstContactTypes.ItemsSource = ds.Tables[0].DefaultView;
                            lstContactTypes.DisplayMemberPath = "ContactTypeName";
                            lstContactTypes.SelectedValuePath = "ContactTypeID";
                            lstContactTypes.SelectedIndex = 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR!", MessageBoxButton.OK, MessageBoxImage.Hand);
            }
        }
        public void GetAllContactTypes(ComboBox cmbContactTypes)
        {
            try
            {
                SettingsBAL _SettingsBal = new SettingsBAL();
                DataSet ds = _SettingsBal.GetAllContactTypes();

                if (ds != null)
                {
                    if (ds.Tables[0] != null)
                    {
                        if (ds.Tables[0].DefaultView.Count > 0)
                        {
                            cmbContactTypes.ItemsSource = ds.Tables[0].DefaultView;
                            cmbContactTypes.DisplayMemberPath = "ContactTypeName";
                            cmbContactTypes.SelectedValuePath = "ContactTypeID";
                            cmbContactTypes.SelectedIndex = 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR!", MessageBoxButton.OK, MessageBoxImage.Hand);
            }
        }
        public void LoadEmailTemplates(ComboBox cmbEmailTemplate)
        {
            try
            {
                cmbEmailTemplate.ItemsSource = null;

                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                DataSet _dsEmailTemplates = sql.ExecuteProcedure("GetEmailTemplates", ht);

                if (_dsEmailTemplates != null)
                {
                    if (_dsEmailTemplates.Tables[0].DefaultView != null)
                    {
                        cmbEmailTemplate.ItemsSource = _dsEmailTemplates.Tables[0].AsDataView();
                        cmbEmailTemplate.DisplayMemberPath = "EmailTemplateName";
                        cmbEmailTemplate.SelectedValuePath = "ID";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! : " + ex.Message, "ERROR!", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        public DataSet LoadEmailTemplateByID(int _templateID)
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
                ht.Add("@TemplateID", _templateID);

                DataSet _dsEmailTemplate = sql.ExecuteProcedure("GetEmailTemplateByID", ht);
                return _dsEmailTemplate;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! : " + ex.Message, "ERROR!", MessageBoxButton.OK, MessageBoxImage.Information);
                return null;
            }
        }
        public void populateActiveMembership(ComboBox cmbMembership, int _MemberId, int _CompanyId, int _SiteId)
        {
            MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
            DataSet ds = _memberDetailsBal.get_ActiveMembership(_MemberId, _CompanyId, _SiteId);
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbMembership.ItemsSource = ds.Tables[0].DefaultView;
                        cmbMembership.DisplayMemberPath = "ProgrammeName";
                        cmbMembership.SelectedValuePath = "ProgrammeID";
                        cmbMembership.SelectedIndex = -1;
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        public void bookingresourcetype(ComboBox cmbresourcetype)
        {
            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return;

            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            DataSet dsresourcetype = sql.ExecuteProcedure("Booking_Resource_Typre", ht);
            if (dsresourcetype != null)
            {
                if (dsresourcetype.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbresourcetype.ItemsSource = dsresourcetype.Tables[0].DefaultView;
                        cmbresourcetype.DisplayMemberPath = "ResourceTypeName";
                        cmbresourcetype.SelectedValuePath = "ResourceTypeID";
                    }
                    catch (Exception)
                    { }
                }
            }
        }
    }
}
