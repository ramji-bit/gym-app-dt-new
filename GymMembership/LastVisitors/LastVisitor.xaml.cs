﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Data;
using System.Configuration;

using GymMembership.BAL;
using GymMembership.Comman;

namespace GymMembership.LastVisitors
{
    /// <summary>
    /// Interaction logic for LastVisitor.xaml
    /// </summary>
    public partial class LastVisitor 
    {
        public LastVisitor()
        {
            InitializeComponent();
        }
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;

        DataSet _dsLastVisitors;
        DataSet _dsAccessRights = new DataSet();
        int _ModuleTable = 0; int _SubModuleTable = 1;
        bool _showMemberAcccess = false; bool _completeTaskAccess = false; bool _cancelTaskAccess = false;

        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto(_StaffPic);
        }

        private void loadStaffPhoto(byte[] _StaffPic)
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }
        private void loadMemberPhoto(byte[] _Pic)
        {
            if (_Pic != null)
            {
                if (_Pic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_Pic, 0, _Pic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    MemImage.Source = bi;
                }
                else
                {
                     MemImage.Source = new BitmapImage(new Uri(@"..\Images\User_Image.png", UriKind.RelativeOrAbsolute));
                }
            }
        }
        public void SetAccessRightsDS(DataSet dS)
        {
            _dsAccessRights = dS;
        }
        private void EnableDisableControls_AccessRights()
        {
            btnLVShowMember.IsEnabled = false;
            btnLVShowMember.Opacity = 0.5;

            btnLVCompleteTask.IsEnabled = false;
            btnLVCompleteTask.Opacity = 0.5;

            btnLVCancelTask.IsEnabled = false;
            btnLVCancelTask.Opacity = 0.5;

            if (_dsAccessRights != null)
            {
                if (_dsAccessRights.Tables.Count > 0)
                {
                    _dsAccessRights.Tables[_ModuleTable].DefaultView.RowFilter = "";
                    _dsAccessRights.Tables[_SubModuleTable].DefaultView.RowFilter = "";

                    if (_dsAccessRights.Tables[_SubModuleTable].DefaultView.Count > 0)
                    {
                        _dsAccessRights.Tables[_SubModuleTable].DefaultView.RowFilter = "ModuleName = 'LAST VISITORS' AND ACTIVE = 1";
                        for (int i = 0; i < _dsAccessRights.Tables[_SubModuleTable].DefaultView.Count; i++)
                        {
                            switch (_dsAccessRights.Tables[_SubModuleTable].DefaultView[i]["SubModuleName"].ToString().ToUpper())
                            {
                                case "SHOW MEMBER":
                                    btnLVShowMember.IsEnabled = true;
                                    btnLVShowMember.Opacity = 1.00;
                                    _showMemberAcccess = true;
                                    break;
                                case "COMPLETE TASK":
                                    btnLVCompleteTask.IsEnabled = true;
                                    btnLVCompleteTask.Opacity = 1.00;
                                    _completeTaskAccess = true;
                                    break;
                                case "CANCEL TASK":
                                    btnLVCancelTask.IsEnabled = true;
                                    btnLVCancelTask.Opacity = 1.00;
                                    _cancelTaskAccess = true;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        }
        private void LoadDoors()
        {
            cmbLVDoor.ItemsSource = null;
            Hashtable ht = new Hashtable();

            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return;

            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            ht.Clear();
            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);

            DataSet _dsDoors = sql.ExecuteProcedure("GetDoorsForCompanySite", ht);
            if (_dsDoors != null)
            {
                if (_dsDoors.Tables.Count > 0)
                {
                    if (_dsDoors.Tables[0].DefaultView.Count > 0)
                    {
                        cmbLVDoor.ItemsSource = _dsDoors.Tables[0].DefaultView;
                        cmbLVDoor.DisplayMemberPath = "DoorName";
                        cmbLVDoor.SelectedValuePath = "DoorCodeNo";
                    }
                }
            }
        }
        private void LastVisitors_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Mouse.OverrideCursor = Cursors.Wait;
                SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
                EnableDisableControls_AccessRights();
                LoadDoors();

                MemberDetailsBAL _memberBAL = new MemberDetailsBAL();
                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                _dsLastVisitors = _memberBAL.GetCheckInDetails(0, _CompanyID, _SiteID, 0);

                if (_dsLastVisitors != null)
                {
                    if (_dsLastVisitors.Tables.Count > 0)
                    {
                        if (_dsLastVisitors.Tables[0].DefaultView.Count > 0)
                        {
                            gvVisitInfo.ItemsSource = _dsLastVisitors.Tables[0].DefaultView;
                        }
                    }
                }
                Mouse.OverrideCursor = null;
            }
            catch(Exception ex)
            {
                string _exMsg = ex.Message;
            }
        }

        private void btnDashboardQuit_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
 
            try
            {
                //this.Close();
                //Closing event called
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
       
    }

        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            Constants._ShowDashBoard();
            this.Close();
        }

        private void cmbLVDoor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbLVDoor.SelectedIndex != -1)
            {
                gvVisitInfo.ItemsSource = null;
                _dsLastVisitors.Tables[0].DefaultView.RowFilter = "";
                string _DoorName = (cmbLVDoor.SelectedItem as DataRowView).Row["DoorName"].ToString();
                _dsLastVisitors.Tables[0].DefaultView.RowFilter = "Door = '" + _DoorName + "'";
                gvVisitInfo.ItemsSource = _dsLastVisitors.Tables[0].DefaultView;
                MemImage.Source = new BitmapImage(new Uri(@"..\Images\User_Image.png", UriKind.RelativeOrAbsolute));
            }
        }

        private void gvVisitInfo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (gvVisitInfo.SelectedIndex >= 0)
                {
                    int i = gvVisitInfo.SelectedIndex;
                    DataRowView _dRowView = _dsLastVisitors.Tables[0].DefaultView[i] as DataRowView;
                    if (_dRowView.Row["MemberOrProspect"].ToString().ToUpper() == "VISITOR")
                    {
                        btnLVCancelTask.IsEnabled = false;
                        btnLVCompleteTask.IsEnabled = false;
                        btnLVShowMember.IsEnabled = false;
                        MemImage.Source = new BitmapImage(new Uri(@"..\Images\User_Image.png", UriKind.RelativeOrAbsolute));
                    }
                    else
                    {
                        if (_cancelTaskAccess)
                            btnLVCancelTask.IsEnabled = true;
                        if (_completeTaskAccess)
                            btnLVCompleteTask.IsEnabled = true;
                        if (_showMemberAcccess)
                            btnLVShowMember.IsEnabled = true;

                        loadMemberPhoto((byte[])_dRowView.Row["Pic"]);
                    }
                }
            }
            catch(Exception ex)
            {
                string _exMsg = ex.Message;
            }
        }
    }
}
