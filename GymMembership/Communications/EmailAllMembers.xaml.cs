﻿using System;
using System.Collections;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Data;
using System.Configuration;

using GymMembership.Comman;
using GymMembership.MemberInfo;
using GymMembership.Task;

namespace GymMembership.Communications
{
    /// <summary>
    /// Interaction logic for EmailAllMembers.xaml
    /// </summary>
    public partial class EmailAllMembers 
    {
        public EmailAllMembers()
        {
            InitializeComponent();
        }
        DataSet dsEmailRecipient = new DataSet();

        public string _MemberName;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public bool _CanOpenMemberShipNewMember = false;
        public bool ExistingMember = false;
        public int GetMemberDetailsMemberId; public long CardNumber;
        public DataSet _dSAccessRights;
        public DataTable dt = new DataTable();
        public bool _isfromDashboardComm = false;

        public int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
        public int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            if (_isfromDashboardComm)
            {
                listBox.Visibility = Visibility.Hidden;
            }
            else
            {
                listBox.Visibility = Visibility.Visible;
            }
            //loadStaffPhoto(_StaffPic);
        }
        private void loadStaffPhoto(byte[] _StaffPic)
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }
        private void btnBulkEmail_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (validateData())
                {
                    SendBulkEmail eMailComm = new SendBulkEmail();
                    eMailComm.Owner = this.Owner;
                    eMailComm._isfromDashboardComm = _isfromDashboardComm;
                    eMailComm.SetRecipientDT(GetRecipientList());
                    
                    eMailComm._MemberName = _MemberName;
                    eMailComm._CurrentLoginDateTime = _CurrentLoginDateTime;
                    eMailComm._CompanyName = _CompanyName;
                    eMailComm._SiteName = _SiteName;
                    eMailComm._UserName = _UserName;
                    eMailComm._StaffPic = _StaffPic;
                    eMailComm.StaffImage.Source = StaffImage.Source;
                    eMailComm._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    eMailComm.ExistingMember = ExistingMember;
                    eMailComm.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    eMailComm._dSAccessRights = _dSAccessRights;
                    eMailComm.dt = dt;

                    this.Close();
                    eMailComm.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! : " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private void BulkEmail_Loaded(object sender, RoutedEventArgs e)
        {
            SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic); 
            LoadEmailMemberList();
            if (_isfromDashboardComm)
            {
                listBox.Visibility = Visibility.Collapsed;
                colLeftPanel.Width = new GridLength(15);
            }
            else
            {
                listBox.Visibility = Visibility.Visible;
            }
        }
        private DataTable GetRecipientList()
        {
            DataTable dT = new DataTable("EmailRecipients");
            dT.Columns.Add("EmailID", typeof(string));
            dT.Columns.Add("MemberID", typeof(int));
            dT.Columns.Add("MemberName", typeof(string));

            dsEmailRecipient.Tables[0].DefaultView.RowFilter = "";
            for (int i = 0; i < dsEmailRecipient.Tables[0].DefaultView.Count; i++)
            {
                if (dsEmailRecipient.Tables[0].Rows[i]["CheckInd"].ToString() == "1")
                {
                    DataRow dR = dT.NewRow();
                    dR["EmailID"] = dsEmailRecipient.Tables[0].Rows[i]["EmailID"].ToString();
                    dR["MemberID"] = Convert.ToInt32(dsEmailRecipient.Tables[0].Rows[i]["MemberID"].ToString());
                    dR["MemberName"] = dsEmailRecipient.Tables[0].Rows[i]["Name"].ToString();

                    dT.Rows.Add(dR);
                }
            }
            return dT;
        }
        private bool validateData()
        {
            for (int i = 0; i < dsEmailRecipient.Tables[0].Rows.Count; i++)
            {
                if (dsEmailRecipient.Tables[0].Rows[i]["CheckInd"].ToString() == "1")
                {
                    return true;
                }
            }
            MessageBox.Show("Please Select atleast 1 Member to Send Email!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            return false;
        }
        private void LoadEmailMemberList()
        {
            try
            {
                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return;

                gvEmail.ItemsSource = null;

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                dsEmailRecipient = sql.ExecuteProcedure("getEmailRecipientList", ht);

                if (dsEmailRecipient != null)
                {
                    if (dsEmailRecipient.Tables[0].DefaultView != null)
                    {
                        gvEmail.ItemsSource = dsEmailRecipient.Tables[0].AsDataView();

                        dsEmailRecipient.Tables[0].DefaultView.RowFilter = "";
                        dsEmailRecipient.Tables[0].DefaultView.RowFilter = "CheckInd = 1";
                        lblEmailTotalRecord.Content = "Total Number of Record Selected : " + dsEmailRecipient.Tables[0].DefaultView.Count;
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! : " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void Select_UnChecked(object sender, RoutedEventArgs e)
        {
            int i = gvEmail.SelectedIndex;
            dsEmailRecipient.Tables[0].DefaultView.RowFilter = "";
            if (i >= 0)
            {
                dsEmailRecipient.Tables[0].Rows[i]["CheckInd"] = "0";
            }

            dsEmailRecipient.Tables[0].DefaultView.RowFilter = "CheckInd = 1";
            lblEmailTotalRecord.Content = "Total Number of Record Selected : " + dsEmailRecipient.Tables[0].DefaultView.Count;
        }
        private void Select_Checked(object sender, RoutedEventArgs e)
        {
            int i = gvEmail.SelectedIndex;
            dsEmailRecipient.Tables[0].DefaultView.RowFilter = "";
            if (i >= 0)
            {
                dsEmailRecipient.Tables[0].Rows[i]["CheckInd"] = "1";
            }

            dsEmailRecipient.Tables[0].DefaultView.RowFilter = "CheckInd = 1";
            lblEmailTotalRecord.Content = "Total Number of Record Selected : " + dsEmailRecipient.Tables[0].DefaultView.Count;
        }

        private void btnEmailShowMember_Click(object sender, RoutedEventArgs e)
        {
            int i; int _MemberID;
            MemberDetails mD = new MemberDetails();
            string _MemberName;

            i = gvEmail.SelectedIndex;
            if (i != -1)
            {
                _MemberID = Convert.ToInt32(dsEmailRecipient.Tables[0].Rows[i]["MemberID"].ToString());
                _MemberName = dsEmailRecipient.Tables[0].Rows[i]["Name"].ToString();

                mD.Owner = this;
                mD.GetMemberDetailsMemberId = _MemberID;
                mD._MemberName = _MemberName;
                mD.MemberDetails_AccessRights(_dSAccessRights);
                mD.ShowDialog();
                //this.Close();
            }
            else
            {
                MessageBox.Show("Please Select a Member from the List!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
        }
        public void SetAccessRights(DataSet dS)
        {
            _dSAccessRights = dS;
        }

        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberDetails mD = new MemberDetails();
                mD.Owner = this.Owner;

                mD._CurrentLoginDateTime = _CurrentLoginDateTime;
                mD._CompanyName = _CompanyName;
                mD._SiteName = _SiteName;
                mD._UserName = _UserName;
                mD._StaffPic = _StaffPic;
                mD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mD.ExistingMember = ExistingMember;
                mD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mD._MemberName = _MemberName;
                mD.Member_Image_Img.Source = Member_Image_Img.Source;
                mD.StaffImage.Source = StaffImage.Source;
                mD._dSAccessRights = _dSAccessRights;
                mD.dt = this.dt;

                this.Close();
                mD.ShowDialog();
            }
        }


        private void btnShowMembership_Click(object sender, RoutedEventArgs e)
        {
            if (!ExistingMember)
            {
                if (_CanOpenMemberShipNewMember)
                {
                    Memberships _mDetails = new Memberships();
                    _mDetails.ExistingMember = ExistingMember;
                    _mDetails.CardNumber = CardNumber;
                    _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    _mDetails._MemberName = _MemberName;
                    _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    _mDetails._CompanyName = _CompanyName;
                    _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                    _mDetails._SiteName = _SiteName;
                    _mDetails._StaffPic = _StaffPic;
                    _mDetails._UserName = _UserName;
                    _mDetails._CompanyID = _CompanyID;
                    _mDetails._SiteID = _SiteID;
                    _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                    _mDetails.StaffImage.Source = StaffImage.Source;
                    _mDetails._dSAccessRights = _dSAccessRights;
                    _mDetails.dt = this.dt;
                    _mDetails.Owner = this.Owner;

                    this.Close();
                    _mDetails.ShowDialog();
                }
                else
                {
                    System.Windows.MessageBox.Show("New Member Details have to be Created First!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            else
            {
                Memberships _mDetails = new Memberships();
                _mDetails.ExistingMember = ExistingMember;
                _mDetails.CardNumber = CardNumber;
                _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _mDetails._MemberName = _MemberName;
                _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _mDetails._CompanyName = _CompanyName;
                _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                _mDetails._SiteName = _SiteName;
                _mDetails._StaffPic = _StaffPic;
                _mDetails._UserName = _UserName;
                _mDetails._CompanyID = _CompanyID;
                _mDetails._SiteID = _SiteID;
                _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                _mDetails.StaffImage.Source = StaffImage.Source;
                _mDetails._dSAccessRights = _dSAccessRights;
                _mDetails.dt = this.dt;
                _mDetails.Owner = this.Owner;

                this.Close();
                _mDetails.ShowDialog();
            }
        }

        private void btnAccounts_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberAccounts mAccounts = new MemberAccounts();
                mAccounts.MemberID = GetMemberDetailsMemberId;
                mAccounts._CurrentLoginDateTime = _CurrentLoginDateTime;
                mAccounts._CompanyName = _CompanyName;
                mAccounts._SiteName = _SiteName;
                mAccounts._UserName = _UserName;
                mAccounts._StaffPic = _StaffPic;
                mAccounts._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mAccounts.ExistingMember = ExistingMember;
                mAccounts.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mAccounts._MemberName = _MemberName;
                mAccounts.Member_Image_Img.Source = Member_Image_Img.Source;
                mAccounts.StaffImage.Source = StaffImage.Source;
                mAccounts._dSAccessRights = _dSAccessRights;
                mAccounts.dt = this.dt;

                mAccounts.Owner = this.Owner;
                this.Cursor = System.Windows.Input.Cursors.Wait;
                btnAccounts.Cursor = System.Windows.Input.Cursors.Wait;
                this.Close();
                mAccounts.ShowDialog();
            }
        }

        private void btnMemberBillingDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ExistingMember)
                {
                    this.Cursor = System.Windows.Input.Cursors.Wait;
                    PaymentDetails PD = new PaymentDetails(GetMemberDetailsMemberId, _CompanyID, _SiteID, dt);
                    this.Cursor = System.Windows.Input.Cursors.Arrow;

                    PD._CurrentLoginDateTime = _CurrentLoginDateTime;
                    PD._CompanyName = _CompanyName;
                    PD._SiteName = _SiteName;
                    PD._UserName = _UserName;
                    PD._StaffPic = _StaffPic;
                    PD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    PD.ExistingMember = ExistingMember;
                    PD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    PD._MemberName = _MemberName;
                    PD.Member_Image_Img.Source = Member_Image_Img.Source;
                    PD.StaffImage.Source = StaffImage.Source;
                    PD._dSAccessRights = _dSAccessRights;
                    PD.dt = this.dt;

                    PD.Owner = this.Owner;
                    this.Close();
                    PD.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnMemberExtraDetails_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                ExtraDetails extra = new ExtraDetails();
                extra.strExtraDetailsMemberID = GetMemberDetailsMemberId; //Convert.ToInt32(txtMemberNumber.Text.ToString().Trim());

                extra._CurrentLoginDateTime = _CurrentLoginDateTime;
                extra._CompanyName = _CompanyName;
                extra._SiteName = _SiteName;
                extra._UserName = _UserName;
                extra._StaffPic = _StaffPic;
                extra._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                extra.ExistingMember = ExistingMember;
                extra.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                extra._MemberName = _MemberName;
                extra.Member_Image_Img.Source = Member_Image_Img.Source;
                extra.StaffImage.Source = StaffImage.Source;
                extra._dSAccessRights = _dSAccessRights;
                extra.dt = this.dt;

                extra.Owner = this.Owner;
                this.Close();
                extra.ShowDialog();
            }
        }

        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                Tasks Tsk = new Tasks();
                Tsk.strMemberNo = GetMemberDetailsMemberId; //Convert.ToInt32(txtMemberNumber.Text);
                Tsk.Owner = this.Owner;
                Tsk.SetAccessRightsDS(_dSAccessRights);

                Tsk._CurrentLoginDateTime = _CurrentLoginDateTime;
                Tsk._CompanyName = _CompanyName;
                Tsk._SiteName = _SiteName;
                Tsk._UserName = _UserName;
                Tsk._StaffPic = _StaffPic;
                Tsk._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                Tsk.ExistingMember = ExistingMember;
                Tsk.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                Tsk._MemberName = _MemberName;
                Tsk.Member_Image_Img.Source = Member_Image_Img.Source;
                Tsk.StaffImage.Source = StaffImage.Source;
                Tsk._dSAccessRights = _dSAccessRights;
                Tsk.dt = this.dt;

                this.Close();

                Tsk.ShowDialog();
            }
        }

        private void btnCommunications_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Communication _Comm = new Communication();
                _Comm.Owner = this.Owner;
                _Comm.SetAccessRightsDS(_dSAccessRights);
                this.Cursor = System.Windows.Input.Cursors.Arrow;

                _Comm._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Comm._CompanyName = _CompanyName;
                _Comm._SiteName = _SiteName;
                _Comm._UserName = _UserName;
                _Comm._StaffPic = _StaffPic;
                _Comm._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Comm.ExistingMember = ExistingMember;
                _Comm.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Comm._MemberName = _MemberName;
                _Comm.Member_Image_Img.Source = Member_Image_Img.Source;
                _Comm.StaffImage.Source = StaffImage.Source;
                _Comm._dSAccessRights = _dSAccessRights;
                _Comm.dt = this.dt;

                this.Close();

                _Comm.ShowDialog();
            }
        }

        private void btnNotes_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Notes _Notes = new Notes();
                _Notes.Owner = this.Owner;

                _Notes._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Notes._CompanyName = _CompanyName;
                _Notes._SiteName = _SiteName;
                _Notes._UserName = _UserName;
                _Notes._StaffPic = _StaffPic;
                _Notes._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Notes.ExistingMember = ExistingMember;
                _Notes.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Notes._MemberName = _MemberName;
                _Notes.Member_Image_Img.Source = Member_Image_Img.Source;
                _Notes.StaffImage.Source = StaffImage.Source;
                _Notes._dSAccessRights = _dSAccessRights;
                _Notes.dt = this.dt;

                this.Cursor = System.Windows.Input.Cursors.Arrow;
                this.Close();
                _Notes.ShowDialog();
            }
        }

        private void btnPersonalTraining_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                PersonalTraining _pt = new PersonalTraining();
                _pt.Owner = this.Owner;

                _pt._CurrentLoginDateTime = _CurrentLoginDateTime;
                _pt._CompanyName = _CompanyName;
                _pt._SiteName = _SiteName;
                _pt._UserName = _UserName;
                _pt._StaffPic = _StaffPic;
                _pt._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _pt.ExistingMember = ExistingMember;
                _pt.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _pt._MemberName = _MemberName;
                _pt.Member_Image_Img.Source = Member_Image_Img.Source;
                _pt.StaffImage.Source = StaffImage.Source;
                _pt._dSAccessRights = _dSAccessRights;
                _pt.dt = this.dt;

                this.Close();
                _pt.ShowDialog();
            }
        }

        private void btnbacktoCommunication_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
