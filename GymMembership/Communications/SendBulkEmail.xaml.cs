﻿using System;
using System.Windows.Input;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Windows.Controls;
using System.Data;
using System.Net.Mail;
using System.Net;
using System.Configuration;

using GymMembership.Comman;
using GymMembership.MemberInfo;
using GymMembership.Task;

namespace GymMembership.Communications
{
    /// <summary>
    /// Interaction logic for SendBulkEmail.xaml
    /// </summary>
    public partial class SendBulkEmail 
    {
        public SendBulkEmail()
        {
            InitializeComponent();
            //message box
        }
        DataTable _dTRecipientList = new DataTable();
        DataSet _dsEmailTemplates = new DataSet();
        
        bool _EmailSendError = false; string _EmailErrorMsg;

        public string _MemberName;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public bool _CanOpenMemberShipNewMember = false;
        public bool ExistingMember = false;
        public int GetMemberDetailsMemberId; public long CardNumber;
        public DataSet _dSAccessRights;
        public DataTable dt = new DataTable();
        public bool _isfromDashboardComm = false;
        public int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
        public int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            if (_isfromDashboardComm)
            {
                listBox.Visibility = Visibility.Hidden;
            }
            else
            {
                listBox.Visibility = Visibility.Visible;
            }
            //loadStaffPhoto(_StaffPic);
        }
        private void loadStaffPhoto(byte[] _StaffPic)
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        public void SetRecipientDT(DataTable dT)
        {
            _dTRecipientList = dT;
        }
        public void sendEmail(string strFrom, string strTo, string strSubject, string strBody, int _Index)
        {
            try
            {
                strSubject = strSubject.Replace("{MEMBERNAME}", _dTRecipientList.Rows[_Index]["MemberName"].ToString());
                strBody = strBody.Replace("{MEMBERNAME}", _dTRecipientList.Rows[_Index]["MemberName"].ToString());
                MailMessage mail = new MailMessage(strFrom, strTo);
                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                
                //////////////DO NOT CHANGE THE BELOW LINES //////////////
                client.Port = 587;
                client.Host = "SMTP.GMAIL.COM";
                string _strFrom = "noreply.reda@gmail.com";
                string _strDecryptPassWord = Comman.Constants.passwordDecrypt("fMKQ+IINHCVpOzskqoKffaT7hdtbE+xxX1g5Ybpw+6AATKV9AxcNhvAZ9rZL7uD/",
                                                        Comman.Constants._EncryptKey);
                if ((ConfigurationManager.AppSettings["REDAAutoEmailID"].ToString().Trim() != "") &&
                    (ConfigurationManager.AppSettings["REDAAutoEmailEncPwd"].ToString().Trim() != ""))
                {
                    _strFrom = ConfigurationManager.AppSettings["REDAAutoEmailID"].ToString().Trim();
                    _strDecryptPassWord = Comman.Constants.passwordDecrypt(ConfigurationManager.AppSettings["REDAAutoEmailEncPwd"].ToString(),
                                                            Comman.Constants._EncryptKey);
                }
                //////////////DO NOT CHANGE THE ABOVE LINES //////////////

                System.Net.NetworkCredential objSMTPUserInfo = new System.Net.NetworkCredential
                            (_strFrom, _strDecryptPassWord);
                client.Credentials = objSMTPUserInfo;
                client.EnableSsl = true;
                mail.Subject = strSubject;
                mail.Body = strBody;
                client.Send(mail);
            }
            catch (Exception ex)
            {
                _EmailErrorMsg = ex.Message;
                _EmailSendError = true;
            }
        }
        public void sendSMS(string strFrom, string strTo, string strSubject, string strBody, int _Index)
        {
            try
            {
                strTo = strTo + "@airtel.in"; //For Testing Only!
                MailMessage mail = new MailMessage(strFrom, strTo);
                SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["SMTPHost"].ToString());
                client.UseDefaultCredentials = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"].ToString());
                client.Host = ConfigurationManager.AppSettings["SMTPHost"].ToString();
                string _strFrom = ConfigurationManager.AppSettings["SMTPUserName"].ToString();
                string _strDecryptPassWord = Constants.passwordDecrypt(ConfigurationManager.AppSettings["SMTPEncryptPassword"].ToString(), Constants._EncryptKey);
                System.Net.NetworkCredential objSMTPUserInfo = new System.Net.NetworkCredential
                            (_strFrom, _strDecryptPassWord);
                client.Credentials = objSMTPUserInfo;
                client.EnableSsl = true;
                mail.Subject = strSubject;
                mail.Body = strBody;
                client.Send(mail);
            }
            catch (Exception ex)
            {
                _EmailErrorMsg = ex.Message;
                _EmailSendError = true;
            }
        }
        private void SendBulkEmail_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
                if (_isfromDashboardComm)
                {
                    listBox.Visibility = Visibility.Collapsed;
                    colLeftPanel.Width = new GridLength(15);
                }
                else
                {
                    listBox.Visibility = Visibility.Visible;
                }
                Utilities _Util = new Utilities();

                cmbRecipients.ItemsSource = null;
                if (_dTRecipientList != null)
                {
                    cmbRecipients.ItemsSource = _dTRecipientList.DefaultView;
                    cmbRecipients.DisplayMemberPath = "EmailID";
                    cmbRecipients.SelectedValuePath = "MemberID";
                }
                //txtFromEmail.Text = ConfigurationManager.AppSettings["SMTPUserName"].ToString();
                txtFromEmail.Text = ConfigurationManager.AppSettings["REDAAutoEmailID"].ToString();
                if (txtFromEmail.Text.Trim() != "") { txtFromEmail.IsEnabled = false; }

                _Util.LoadEmailTemplates(cmbEmailTemplate);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! : " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void cmbEmailTemplate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int _templateID = Convert.ToInt32(cmbEmailTemplate.SelectedValue.ToString());
            Utilities _Util = new Utilities();

            if (_templateID != -1)
            {
                DataSet ds = _Util.LoadEmailTemplateByID(_templateID);
                if (ds != null)
                {
                    if (ds.Tables[0].DefaultView != null)
                    {
                        txtSubject.Text = ds.Tables[0].Rows[0]["EmailSubject"].ToString();
                        txtBulkEmailBody.Text = ds.Tables[0].Rows[0]["EmailBodyText"].ToString();
                    }
                }
            }
        }

        private void btnSend_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;
                btnSend.Cursor = Cursors.Wait;

                if (cmbEmailTemplate.SelectedIndex == -1)
                {
                    MessageBox.Show("Please Select Email Template!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Cursor = Cursors.Arrow;
                    btnSend.Cursor = Cursors.Hand;
                    return;
                }
                if (txtSubject.Text.Trim() == "")
                {
                    MessageBox.Show("Subject of Email Cannot be Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Cursor = Cursors.Arrow;
                    btnSend.Cursor = Cursors.Hand;
                    return;
                }
                if (txtBulkEmailBody.Text.Trim() == "")
                {
                    MessageBox.Show("Body of Email Cannot be Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Cursor = Cursors.Arrow;
                    btnSend.Cursor = Cursors.Hand;
                    return;
                }
                if (cmbRecipients.Items.Count <= 0)
                {
                    MessageBox.Show("There are NO Recipients email in the list!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Cursor = Cursors.Arrow;
                    btnSend.Cursor = Cursors.Hand;
                    return;
                }

                this.Cursor = Cursors.Wait;
                for (int i = 0; i < cmbRecipients.Items.Count; i++)
                {
                    sendEmail(txtFromEmail.Text, (cmbRecipients.Items[i] as DataRowView).Row["EmailID"].ToString(), txtSubject.Text, txtBulkEmailBody.Text, i);
                    //sendSMS(txtFromEmail.Text, (cmbRecipients.Items[i] as DataRowView).Row["EmailID"].ToString(), txtSubject.Text, txtBulkEmailBody.Text, i);
                }

                if (!_EmailSendError)
                {
                    MessageBox.Show("Email successfully Sent!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    clearFields();
                    this.Cursor = Cursors.Arrow;
                    btnSend.Cursor = Cursors.Hand;
                }
                else
                {
                    MessageBox.Show("Email Error! " + _EmailErrorMsg, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Cursor = Cursors.Arrow;
                    btnSend.Cursor = Cursors.Hand;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! : " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private void clearFields()
        {
            txtBulkEmailBody.Text = "";
            txtSubject.Text = "";
            //cmbEmailTemplate.SelectedIndex = -1;
        }

        private void btnbacktoEmailtoAllMembership_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberDetails mD = new MemberDetails();
                mD.Owner = this.Owner;

                mD._CurrentLoginDateTime = _CurrentLoginDateTime;
                mD._CompanyName = _CompanyName;
                mD._SiteName = _SiteName;
                mD._UserName = _UserName;
                mD._StaffPic = _StaffPic;
                mD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mD.ExistingMember = ExistingMember;
                mD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mD._MemberName = _MemberName;
                mD.Member_Image_Img.Source = Member_Image_Img.Source;
                mD.StaffImage.Source = StaffImage.Source;
                mD._dSAccessRights = _dSAccessRights;
                mD.dt = this.dt;

                this.Close();
                mD.ShowDialog();
            }
        }


        private void btnShowMembership_Click(object sender, RoutedEventArgs e)
        {
            if (!ExistingMember)
            {
                if (_CanOpenMemberShipNewMember)
                {
                    Memberships _mDetails = new Memberships();
                    _mDetails.ExistingMember = ExistingMember;
                    _mDetails.CardNumber = CardNumber;
                    _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    _mDetails._MemberName = _MemberName;
                    _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    _mDetails._CompanyName = _CompanyName;
                    _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                    _mDetails._SiteName = _SiteName;
                    _mDetails._StaffPic = _StaffPic;
                    _mDetails._UserName = _UserName;
                    _mDetails._CompanyID = _CompanyID;
                    _mDetails._SiteID = _SiteID;
                    _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                    _mDetails.StaffImage.Source = StaffImage.Source;
                    _mDetails._dSAccessRights = _dSAccessRights;
                    _mDetails.dt = this.dt;
                    _mDetails.Owner = this.Owner;

                    this.Close();
                    _mDetails.ShowDialog();
                }
                else
                {
                    System.Windows.MessageBox.Show("New Member Details have to be Created First!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            else
            {
                Memberships _mDetails = new Memberships();
                _mDetails.ExistingMember = ExistingMember;
                _mDetails.CardNumber = CardNumber;
                _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _mDetails._MemberName = _MemberName;
                _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _mDetails._CompanyName = _CompanyName;
                _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                _mDetails._SiteName = _SiteName;
                _mDetails._StaffPic = _StaffPic;
                _mDetails._UserName = _UserName;
                _mDetails._CompanyID = _CompanyID;
                _mDetails._SiteID = _SiteID;
                _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                _mDetails.StaffImage.Source = StaffImage.Source;
                _mDetails._dSAccessRights = _dSAccessRights;
                _mDetails.dt = this.dt;
                _mDetails.Owner = this.Owner;

                this.Close();
                _mDetails.ShowDialog();
            }
        }

        private void btnAccounts_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberAccounts mAccounts = new MemberAccounts();
                mAccounts.MemberID = GetMemberDetailsMemberId;
                mAccounts._CurrentLoginDateTime = _CurrentLoginDateTime;
                mAccounts._CompanyName = _CompanyName;
                mAccounts._SiteName = _SiteName;
                mAccounts._UserName = _UserName;
                mAccounts._StaffPic = _StaffPic;
                mAccounts._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mAccounts.ExistingMember = ExistingMember;
                mAccounts.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mAccounts._MemberName = _MemberName;
                mAccounts.Member_Image_Img.Source = Member_Image_Img.Source;
                mAccounts.StaffImage.Source = StaffImage.Source;
                mAccounts._dSAccessRights = _dSAccessRights;
                mAccounts.dt = this.dt;

                mAccounts.Owner = this.Owner;
                this.Cursor = System.Windows.Input.Cursors.Wait;
                btnAccounts.Cursor = System.Windows.Input.Cursors.Wait;
                this.Close();
                mAccounts.ShowDialog();
            }
        }

        private void btnMemberBillingDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ExistingMember)
                {
                    this.Cursor = System.Windows.Input.Cursors.Wait;
                    PaymentDetails PD = new PaymentDetails(GetMemberDetailsMemberId, _CompanyID, _SiteID, dt);
                    this.Cursor = System.Windows.Input.Cursors.Arrow;

                    PD._CurrentLoginDateTime = _CurrentLoginDateTime;
                    PD._CompanyName = _CompanyName;
                    PD._SiteName = _SiteName;
                    PD._UserName = _UserName;
                    PD._StaffPic = _StaffPic;
                    PD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    PD.ExistingMember = ExistingMember;
                    PD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    PD._MemberName = _MemberName;
                    PD.Member_Image_Img.Source = Member_Image_Img.Source;
                    PD.StaffImage.Source = StaffImage.Source;
                    PD._dSAccessRights = _dSAccessRights;
                    PD.dt = this.dt;

                    PD.Owner = this.Owner;
                    this.Close();
                    PD.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnMemberExtraDetails_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                ExtraDetails extra = new ExtraDetails();
                extra.strExtraDetailsMemberID = GetMemberDetailsMemberId; //Convert.ToInt32(txtMemberNumber.Text.ToString().Trim());

                extra._CurrentLoginDateTime = _CurrentLoginDateTime;
                extra._CompanyName = _CompanyName;
                extra._SiteName = _SiteName;
                extra._UserName = _UserName;
                extra._StaffPic = _StaffPic;
                extra._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                extra.ExistingMember = ExistingMember;
                extra.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                extra._MemberName = _MemberName;
                extra.Member_Image_Img.Source = Member_Image_Img.Source;
                extra.StaffImage.Source = StaffImage.Source;
                extra._dSAccessRights = _dSAccessRights;
                extra.dt = this.dt;

                extra.Owner = this.Owner;
                this.Close();
                extra.ShowDialog();
            }
        }

        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                Tasks Tsk = new Tasks();
                Tsk.strMemberNo = GetMemberDetailsMemberId; //Convert.ToInt32(txtMemberNumber.Text);
                Tsk.Owner = this.Owner;
                Tsk.SetAccessRightsDS(_dSAccessRights);

                Tsk._CurrentLoginDateTime = _CurrentLoginDateTime;
                Tsk._CompanyName = _CompanyName;
                Tsk._SiteName = _SiteName;
                Tsk._UserName = _UserName;
                Tsk._StaffPic = _StaffPic;
                Tsk._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                Tsk.ExistingMember = ExistingMember;
                Tsk.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                Tsk._MemberName = _MemberName;
                Tsk.Member_Image_Img.Source = Member_Image_Img.Source;
                Tsk.StaffImage.Source = StaffImage.Source;
                Tsk._dSAccessRights = _dSAccessRights;
                Tsk.dt = this.dt;

                this.Close();

                Tsk.ShowDialog();
            }
        }

        private void btnCommunications_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Communication _Comm = new Communication();
                _Comm.Owner = this.Owner;
                _Comm.SetAccessRightsDS(_dSAccessRights);
                this.Cursor = System.Windows.Input.Cursors.Arrow;

                _Comm._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Comm._CompanyName = _CompanyName;
                _Comm._SiteName = _SiteName;
                _Comm._UserName = _UserName;
                _Comm._StaffPic = _StaffPic;
                _Comm._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Comm.ExistingMember = ExistingMember;
                _Comm.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Comm._MemberName = _MemberName;
                _Comm.Member_Image_Img.Source = Member_Image_Img.Source;
                _Comm.StaffImage.Source = StaffImage.Source;
                _Comm._dSAccessRights = _dSAccessRights;
                _Comm.dt = this.dt;

                this.Close();

                _Comm.ShowDialog();
            }
        }

        private void btnNotes_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Notes _Notes = new Notes();
                _Notes.Owner = this.Owner;

                _Notes._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Notes._CompanyName = _CompanyName;
                _Notes._SiteName = _SiteName;
                _Notes._UserName = _UserName;
                _Notes._StaffPic = _StaffPic;
                _Notes._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Notes.ExistingMember = ExistingMember;
                _Notes.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Notes._MemberName = _MemberName;
                _Notes.Member_Image_Img.Source = Member_Image_Img.Source;
                _Notes.StaffImage.Source = StaffImage.Source;
                _Notes._dSAccessRights = _dSAccessRights;
                _Notes.dt = this.dt;

                this.Cursor = System.Windows.Input.Cursors.Arrow;
                this.Close();
                _Notes.ShowDialog();
            }
        }

        private void btnPersonalTraining_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                PersonalTraining _pt = new PersonalTraining();
                _pt.Owner = this.Owner;

                _pt._CurrentLoginDateTime = _CurrentLoginDateTime;
                _pt._CompanyName = _CompanyName;
                _pt._SiteName = _SiteName;
                _pt._UserName = _UserName;
                _pt._StaffPic = _StaffPic;
                _pt._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _pt.ExistingMember = ExistingMember;
                _pt.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _pt._MemberName = _MemberName;
                _pt.Member_Image_Img.Source = Member_Image_Img.Source;
                _pt.StaffImage.Source = StaffImage.Source;
                _pt._dSAccessRights = _dSAccessRights;
                _pt.dt = this.dt;

                this.Close();
                _pt.ShowDialog();
            }
        }
    }
}
