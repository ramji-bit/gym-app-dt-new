﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Windows;

using GymMembership.Comman;

namespace GymMembership
{
    public class SqlHelper
    {
        string ConnectionString = string.Empty;
        public static SqlConnection con;
        public static OleDbConnection OLEcon;
        public int _ConnOpen = 0;
        public SqlHelper()
        {
            string UserID = "";
            string Password = "";

            try
            {
                if (!Constants._OfflineDB)
                {
                    UserID = ConfigurationManager.AppSettings["DBUser"].ToString();
                    Password = Constants.passwordDecrypt(ConfigurationManager.AppSettings["DBPassword"].ToString(), Constants._EncryptKey);
                    ConnectionString = ConfigurationManager.ConnectionStrings["GymMembershipDB"].ConnectionString;
                    ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;
                }
                else
                {
                    //UserID = ConfigurationManager.AppSettings["OfflineDBUser"].ToString();
                    //Password = Constants.passwordDecrypt(ConfigurationManager.AppSettings["OfflineDBPassword"].ToString(), Constants._EncryptKey);
                    //ConnectionString = ConfigurationManager.ConnectionStrings["offlineDB"].ConnectionString;

                    ////ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;
                    ////Constants._OfflineDB = true;

                    //ConfigurationManager.AppSettings["OfflineFlag"] = "true";

                    UserID = ConfigurationManager.AppSettings["DBUser"].ToString();
                    Password = Constants.passwordDecrypt(ConfigurationManager.AppSettings["DBPassword"].ToString(), Constants._EncryptKey);
                    ConnectionString = ConfigurationManager.ConnectionStrings["GymMembershipDB"].ConnectionString;
                    ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;
                }

                con = new SqlConnection(ConnectionString);
                con.Open();

                if (con.State == ConnectionState.Closed)
                {
                    //_ConnOpen = 0;
                    //UserID = ConfigurationManager.AppSettings["OfflineDBUser"].ToString();
                    //Password = Constants.passwordDecrypt(ConfigurationManager.AppSettings["OfflineDBPassword"].ToString(), Constants._EncryptKey);
                    //ConnectionString = ConfigurationManager.ConnectionStrings["offlineDB"].ConnectionString;
                    ////ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;

                    //con = new SqlConnection(ConnectionString);
                    //con.Open();
                    ////Constants._OfflineDB = true;
                    //ConfigurationManager.AppSettings["OfflineFlag"] = "true";

                    UserID = ConfigurationManager.AppSettings["DBUser"].ToString();
                    Password = Constants.passwordDecrypt(ConfigurationManager.AppSettings["DBPassword"].ToString(), Constants._EncryptKey);
                    ConnectionString = ConfigurationManager.ConnectionStrings["GymMembershipDB"].ConnectionString;
                    ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;

                    con = new SqlConnection(ConnectionString);
                    con.Open();
                }
                else
                {
                    _ConnOpen = 1;
                }
                //}
            }
            catch(Exception ex)
            {
                try
                {
                    string _ex = ex.Message;
                    _ConnOpen = 0;

                    if ((_ex.ToUpper().Trim().StartsWith("CONNECTION TIMEOUT")) || (_ex.ToUpper().Trim().Contains("TIMED OUT")))
                    //if (con.State == ConnectionState.Closed)
                    {
                        UserID = ConfigurationManager.AppSettings["DBUser"].ToString();
                        Password = Constants.passwordDecrypt(ConfigurationManager.AppSettings["DBPassword"].ToString(), Constants._EncryptKey);
                        ConnectionString = ConfigurationManager.ConnectionStrings["GymMembershipDB"].ConnectionString;
                        ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;
                    }
                    else
                    {
                        //UserID = ConfigurationManager.AppSettings["OfflineDBUser"].ToString();
                        //Password = Constants.passwordDecrypt(ConfigurationManager.AppSettings["OfflineDBPassword"].ToString(), Constants._EncryptKey);
                        //ConnectionString = ConfigurationManager.ConnectionStrings["offlineDB"].ConnectionString;
                        ////ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;

                        UserID = ConfigurationManager.AppSettings["DBUser"].ToString();
                        Password = Constants.passwordDecrypt(ConfigurationManager.AppSettings["DBPassword"].ToString(), Constants._EncryptKey);
                        ConnectionString = ConfigurationManager.ConnectionStrings["GymMembershipDB"].ConnectionString;
                        ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;
                    }
                        con = new SqlConnection(ConnectionString);
                        con.Open();

                        _ConnOpen = 1;
                        ////Constants._OfflineDB = true;
                        //ConfigurationManager.AppSettings["OfflineFlag"] = "true";
                }
                catch(Exception ex1)
                {
                    string _msg = ex1.Message;
                    _ConnOpen = 0;
                }
            }
        }

        public int SetConnection()
        {
            try
            {
                if (ConnectionString == string.Empty)
                {
                    if (!Constants._OfflineDB)
                    {
                        string UserID = ConfigurationManager.AppSettings["DBUser"].ToString();
                        string Password = Constants.passwordDecrypt(ConfigurationManager.AppSettings["DBPassword"].ToString(), Constants._EncryptKey);
                        ConnectionString = ConfigurationManager.ConnectionStrings["GymMembershipDB"].ConnectionString;
                        ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;
                    }
                    else
                    {
                        //string UserID = ConfigurationManager.AppSettings["OfflineDBUser"].ToString();
                        //string Password = Constants.passwordDecrypt(ConfigurationManager.AppSettings["OfflineDBPassword"].ToString(), Constants._EncryptKey);
                        //ConnectionString = ConfigurationManager.ConnectionStrings["offlineDB"].ConnectionString;
                        //ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;
                        //Constants._OfflineDB = true;
                        //ConfigurationManager.AppSettings["OfflineFlag"] = "true";

                        string UserID = ConfigurationManager.AppSettings["DBUser"].ToString();
                        string Password = Constants.passwordDecrypt(ConfigurationManager.AppSettings["DBPassword"].ToString(), Constants._EncryptKey);
                        ConnectionString = ConfigurationManager.ConnectionStrings["GymMembershipDB"].ConnectionString;
                        ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;
                    }
                }
                con = new SqlConnection(ConnectionString);
                con.Open();
                if (con.State == ConnectionState.Open)
                {
                    return 1;
                }
                else
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        //string UserID = ConfigurationManager.AppSettings["OfflineDBUser"].ToString();
                        //string Password = Constants.passwordDecrypt(ConfigurationManager.AppSettings["OfflineDBPassword"].ToString(), Constants._EncryptKey);
                        //ConnectionString = ConfigurationManager.ConnectionStrings["offlineDB"].ConnectionString;
                        //ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;

                        //con = new SqlConnection(ConnectionString);
                        //con.Open();
                        //Constants._OfflineDB = true;
                        //ConfigurationManager.AppSettings["OfflineFlag"] = "true";

                        string UserID = ConfigurationManager.AppSettings["DBUser"].ToString();
                        string Password = Constants.passwordDecrypt(ConfigurationManager.AppSettings["DBPassword"].ToString(), Constants._EncryptKey);
                        ConnectionString = ConfigurationManager.ConnectionStrings["GymMembershipDB"].ConnectionString;
                        ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;

                        con = new SqlConnection(ConnectionString);
                        con.Open();
                    }
                    if (con.State == ConnectionState.Open)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch(Exception)
            {
                try
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        //string UserID = ConfigurationManager.AppSettings["OfflineDBUser"].ToString();
                        //string Password = Constants.passwordDecrypt(ConfigurationManager.AppSettings["OfflineDBPassword"].ToString(), Constants._EncryptKey);
                        //ConnectionString = ConfigurationManager.ConnectionStrings["offlineDB"].ConnectionString;
                        //ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;

                        //con = new SqlConnection(ConnectionString);
                        //con.Open();

                        string UserID = ConfigurationManager.AppSettings["DBUser"].ToString();
                        string Password = Constants.passwordDecrypt(ConfigurationManager.AppSettings["DBPassword"].ToString(), Constants._EncryptKey);
                        ConnectionString = ConfigurationManager.ConnectionStrings["GymMembershipDB"].ConnectionString;
                        ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;

                        con = new SqlConnection(ConnectionString);
                        con.Open();
                    }
                    if (con.State == ConnectionState.Open)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                catch(Exception x1)
                {
                    return 0;
                }
            }
        }

        public DataSet ExecuteQueryReturnDataSet(string QueryString)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = 7;
                SqlDataAdapter da = new SqlDataAdapter();
                DataSet ds = new DataSet();
                cmd.CommandText = QueryString;
                int conState ;
                if ((con == null) || (con.State == ConnectionState.Closed))
                {
                    conState = SetConnection();
                }
                else
                { conState = 1; }

                if (conState == 0) { return null; }

                cmd.Connection = con;

                da.SelectCommand = cmd;
                da.Fill(ds);
                return ds;
            }
            catch (SqlException Exception)
            {
                //MessageBox.Show(Exception.Message.ToString());
                return null;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message.ToString());
                return null;
            }
        }

        public SqlDataAdapter ExecuteQueryReturnDataAdapter(string QueryString)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = 7;
                SqlDataAdapter da = new SqlDataAdapter();
                DataSet ds = new DataSet();
                cmd.CommandText = QueryString;
                int conState;
                if ((con == null) || (con.State == ConnectionState.Closed))
                {
                    conState = SetConnection();
                }
                else
                { conState = 1; }

                if (conState == 0) { return null; }

                cmd.Connection = con;

                da.SelectCommand = cmd;
                da.Fill(ds);
                return da;
            }
            catch (SqlException Exception)
            {
                //MessageBox.Show(Exception.Message.ToString());
                return null;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message.ToString());
                return null;
            }
        }
        public DataSet ExecuteProcedure(string procName, Hashtable parms, bool _forTimeout = false)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                if (_forTimeout)
                { cmd.CommandTimeout = 14; }
                else
                {
                    cmd.CommandTimeout = 7;
                }
                SqlDataAdapter da = new SqlDataAdapter();
                DataSet ds = new DataSet();
                cmd.CommandText = procName;
                cmd.CommandType = CommandType.StoredProcedure;
                int conState;
                if ((con == null) || (con.State == ConnectionState.Closed))
                {
                    conState = SetConnection();
                }
                else
                { conState = 1; }

                if (conState == 0) { return null; }

                cmd.Connection = con;
                if (parms.Count > 0)
                {
                    foreach (DictionaryEntry de in parms)
                    {
                        cmd.Parameters.AddWithValue(de.Key.ToString(), de.Value);
                    }
                }
                da.SelectCommand = cmd;
                da.Fill(ds);
                return ds;
            }
            catch (SqlException Exception)
            {
                MessageBox.Show(Exception.Message.ToString());
                return null;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message.ToString());

                return null;
            }
        }
        public SqlDataAdapter getDisConnectedDataAdapter(string procName, Hashtable parms)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = 7;
                SqlDataAdapter da = new SqlDataAdapter();
                DataSet ds = new DataSet();
                cmd.CommandText = procName;
                cmd.CommandType = CommandType.StoredProcedure;
                int conState;
                if ((con == null) || (con.State == ConnectionState.Closed))
                {
                    conState = SetConnection();
                }
                else
                { conState = 1; }

                if (conState == 0) { return null; }

                cmd.Connection = con;
                if (parms.Count > 0)
                {
                    foreach (DictionaryEntry de in parms)
                    {
                        cmd.Parameters.AddWithValue(de.Key.ToString(), de.Value);
                    }
                }
                da.SelectCommand = cmd;
                return da;
            }
            catch (SqlException Exception)
            {
                //MessageBox.Show(Exception.Message.ToString());
                return null;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message.ToString());
                return null;
            }
        }
        public int ExecuteQuery(string procName, Hashtable parms)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = 7;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = procName;
                if (parms.Count > 0)
                {
                    foreach (DictionaryEntry de in parms)
                    {
                        cmd.Parameters.AddWithValue(de.Key.ToString(), de.Value);
                    }
                }
                int conState;
                if ((con == null) || (con.State == ConnectionState.Closed))
                {
                    conState = SetConnection();
                }
                else
                { conState = 1; }

                if (conState == 0) { return 0; }
                cmd.Connection = con;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                int result = cmd.ExecuteNonQuery();
                return result;
            }
            catch (SqlException Exception)
            {
                //MessageBox.Show(Exception.Message.ToString());
                return 0;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message.ToString());
                return 0;
            }
        }
        public int ExecuteQuerywithOutputparams(SqlCommand cmd)
        {
            try {
                int conState;
                if ((con == null) || (con.State == ConnectionState.Closed))
                {
                    conState = SetConnection();
                }
                else
                { conState = 1; }

                if (conState == 0) { return 0; }
                cmd.Connection = con;
                cmd.CommandTimeout = 7;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                int result = cmd.ExecuteNonQuery();
                return result;
            }
            catch (SqlException Exception)
            {
                //MessageBox.Show(Exception.Message.ToString());
                return 0;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message.ToString());
                return 0;
            }
        }
        public int ExecuteQueryWithOutParam(string procName, Hashtable parms)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = 7;
                SqlParameter sqlparam = new SqlParameter();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = procName;
                if (parms.Count > 0)
                {
                    foreach (DictionaryEntry de in parms)
                    {
                        if (de.Key.ToString().Contains("_out"))
                        {
                            sqlparam = new SqlParameter(de.Key.ToString(), de.Value);
                            sqlparam.DbType = DbType.Int32;
                            sqlparam.Direction = ParameterDirection.Output;
                            cmd.Parameters.Add(sqlparam);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue(de.Key.ToString(), de.Value);
                        }
                    }
                }
                int conState;
                if ((con == null) || (con.State == ConnectionState.Closed))
                {
                    conState = SetConnection();
                }
                else
                { conState = 1; }

                if (conState == 0) { return 0; }

                cmd.Connection = con;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                int result = cmd.ExecuteNonQuery();
                if (sqlparam != null)
                    result = Convert.ToInt32(sqlparam.SqlValue.ToString());
                return result;
            }
            catch (SqlException Exception)
            {
                //MessageBox.Show(Exception.Message.ToString());
                return 0;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message.ToString());
                return 0;
            }
        }
    }
}
