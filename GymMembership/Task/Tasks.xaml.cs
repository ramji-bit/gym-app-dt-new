﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Input;
using System.Data;

using GymMembership.BAL;
using GymMembership.MemberInfo;
using GymMembership.Comman;
using GymMembership.Communications;
using System.Configuration;

namespace GymMembership.Task
{
    /// <summary>
    /// Interaction logic for Tasks.xaml
    /// </summary>
    public partial class Tasks 
    {
        public string _MemberName;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public bool _CanOpenMemberShipNewMember = false;
        public bool ExistingMember = false;
        public int GetMemberDetailsMemberId; public long CardNumber;
        public DataSet _dSAccessRights;// = new DataSet();
        public DataTable dt = new DataTable();
        public bool _invokedFromDashboard = false;
        public int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
        public int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

        //TaskBAL _taskBAL = new TaskBAL();
        int _ModuleTable = 0; int _SubModuleTable = 1;
        string _MEMBERorPROSPECT = "";

        private int _MemberNo = 0;
        private string strQry = "";
        private int i;
        private int _memberID = 0;
        private int _TaskId = 0;
        private int _filterTaskId = 0;
        private int _staffId = 0;
        private string _staffActionTaken;
        private int _addActionSucess = 0;
        private int _taskStatus = 0;
        private int _assignedStaffId = 0;
        private DateTime _taskExpireDate;
        private bool reval;
        private int _userRole = 0;
        private int _adminStaffId = 0;
        bool _isProspectTask = false;

        bool _wLoaded = false;
        DataSet ds = new DataSet();

        public Tasks()
        {
            InitializeComponent();
        }
        public int strMemberNo = 0;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Mouse.OverrideCursor = Cursors.Wait;
                if (_invokedFromDashboard)
                {
                    listBox.Visibility = Visibility.Collapsed;
                    col.Width = new GridLength(26);
                }
                else
                {
                    //listBox.Visibility = Visibility.Visible;
                    listBox.Visibility = Visibility.Collapsed;
                    col.Width = new GridLength(26);
                }
                SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
                EnableDisableControls_AccessRights();
                populateData();
                _wLoaded = true;
                this.IsMaxRestoreButtonEnabled = false;
                this.IsMinButtonEnabled = false;
                this.IsWindowDraggable = false;
                Mouse.OverrideCursor = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }
        public void populateData()
        {
            try
            {
                Utilities _utilities = new Utilities();
                _utilities.PopulateTaskType(cmbFilterTask, _TaskId);
                _utilities.populatePersonalTrainer_TaskFilter(cmbFilterByStaff);
                cmbFilterByStaff.SelectedIndex = -1;

                //_utilities.populatePersonalTrainer(cmbAssignTo);
                _utilities.populatePersonalTrainer_TaskFilter(cmbAssignTo, 1);
                cmbAssignTo.SelectedIndex = -1;

                ClearData();
                GetStaffId();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private void GetStaffId()
        {
            try
            {
                string _UserName = ConfigurationManager.AppSettings["LoggedInUser"];
                TaskBAL _taskBAL = new TaskBAL();
                DataSet StaffDs = _taskBAL.get_StaffIdByUserName(_UserName);

                if (StaffDs != null)
                {
                    if (StaffDs.Tables[0].DefaultView != null)
                    {
                        if (StaffDs.Tables[0].Rows.Count > 0)
                        {
                            _userRole = Convert.ToInt32(StaffDs.Tables[0].Rows[0]["userRole"].ToString());

                            if ((_userRole == 2) && (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES"))
                            {
                                _staffId = Convert.ToInt32(StaffDs.Tables[0].Rows[0]["StaffId"].ToString());
                                //_adminStaffId = _staffId;
                                btnNewTask.Visibility = Visibility.Visible;
                                cmbFilterByStaff.IsEnabled = true;
                                cmbAssignTo.IsEnabled = true;

                                LoadTaskList(_filterTaskId, _adminStaffId);
                            }
                            else
                            {
                                _staffId = Convert.ToInt32(StaffDs.Tables[0].Rows[0]["StaffId"].ToString());
                                _adminStaffId = _staffId;
                                btnNewTask.Visibility = Visibility.Hidden;
                                cmbFilterByStaff.IsEnabled = false;
                                cmbAssignTo.IsEnabled = false;
                                LoadTaskList(_filterTaskId, _adminStaffId);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }


        }

        private void LoadMemberTask()
        {
            try
            {
                if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
                    _staffId = 0;

                LoadTaskList(_filterTaskId, _staffId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }

        }

        public void setMemberNo(int s)
        {
            strMemberNo = s;
        }

        //private void DefineUser()
        //{
        //    try
        //    {
        //        if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
        //        {
        //            btnNewTask.Visibility = Visibility.Visible;
        //            cmbFilterByStaff.IsEnabled = true;
        //            cmbAssignTo.IsEnabled = true;
        //            LoadMemberTask();
        //        }
        //        else
        //        {
        //            btnNewTask.Visibility = Visibility.Hidden;
        //            cmbFilterByStaff.IsEnabled = false;
        //            cmbAssignTo.IsEnabled = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message).ToString();
        //    }

        //}

        private void grdTask_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                int i = grdTask.SelectedIndex;

                if (i > -1)
                {
                    _MEMBERorPROSPECT = (grdTask.Items[i] as DataRowView).Row["MemberOrProspect"].ToString().ToUpper();

                    DataSet ds = new DataSet();
                    _MemberNo = Convert.ToInt32((grdTask.Items[i] as DataRowView).Row["MemberID"].ToString());
                    _TaskId = Convert.ToInt32((grdTask.Items[i] as DataRowView).Row["ID"].ToString());
                    TaskBAL _taskBAL = new TaskBAL();
                    string _TaskStatus = (grdTask.Items[i] as DataRowView).Row["TaskStatus"].ToString();

                    if (_MEMBERorPROSPECT.ToUpper().Trim() == "MEMBER")
                    {
                        ds = _taskBAL.get_TaskByMemberId(_MemberNo, _TaskId);
                        btnCancelTask.IsEnabled = true;
                        btnCompleteTask.IsEnabled = true;
                        btnTaskHistory.IsEnabled = true;
                        btnNewTask.IsEnabled = true;
                        _isProspectTask = false;

                        if ((_TaskStatus.Trim().ToUpper() == "CANCELED") || (_TaskStatus.Trim().ToUpper() == "COMPLETED") || (_TaskStatus.Trim().ToUpper() == ""))
                        {
                            btnCancelTask.IsEnabled = false;
                            btnCompleteTask.IsEnabled = false;
                            btnTaskHistory.IsEnabled = false;
                        }
                    }
                    else if (_MEMBERorPROSPECT.ToUpper().Trim() == "PROSPECT")
                    {
                        ds = _taskBAL.get_TaskByProspectId(_MemberNo, _TaskId);
                        btnCancelTask.IsEnabled = false;
                        btnCompleteTask.IsEnabled = false;
                        btnTaskHistory.IsEnabled = false;
                        btnNewTask.IsEnabled = false;
                        _isProspectTask = true;
                    }
                    else
                    {
                        ds = null;
                    }

                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            lblName.Content = ds.Tables[0].Rows[0]["FirstName"].ToString() + ", " + ds.Tables[0].Rows[0]["LastName"].ToString();
                            txtMemberNo.Text = ds.Tables[0].Rows[0]["MemberID"].ToString();
                            txtTaskNotes.Text = ds.Tables[0].Rows[0]["TaskDescription"].ToString();
                            txtActions.Text = ds.Tables[0].Rows[0]["Actions"].ToString();
                            dpExpiresOn.Text = ds.Tables[0].Rows[0]["ExpiredDt"].ToString();
                            txtHomePhone.Text = ds.Tables[0].Rows[0]["HomePhone"].ToString();
                            txtCellPhone.Text = ds.Tables[0].Rows[0]["MobilePhone"].ToString();

                            cmbFilterTask.SelectedValue = ds.Tables[0].Rows[0]["TaskType"].ToString();
                            if (ds.Tables[0].Rows[0]["AssignedTo"].ToString() != "0")
                            {
                                cmbAssignTo.SelectedValue = ds.Tables[0].Rows[0]["AssignedTo"].ToString();
                            }
                            else
                            {
                                cmbAssignTo.SelectedIndex = -1;
                            }
                            EnableTaskButtons();
                        }
                        else
                        {
                            ClearData();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private void btnShowMember_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                i = grdTask.SelectedIndex;

                if (i != -1)
                {
                    MemberDetails mD = new MemberDetails();
                    _memberID = Convert.ToInt32(txtMemberNo.Text);
                    mD.GetMemberDetailsMemberId = _memberID;
                    mD._MemberName = _MemberName;
                    mD.Owner = this;
                    mD.ShowDialog();
                    this.Close();
                }
                else
                {
                    MessageBox.Show(" Please Select an Existing Member from the List!", this.Title, MessageBoxButton.OK, MessageBoxImage.Stop);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }


        }

        private void btnNewTask_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddTask _addTask = new AddTask();
                _addTask._invokedFromDashboard = _invokedFromDashboard;
                _addTask.ExistingMember = ExistingMember;
                _addTask.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _addTask._MemberName = _MemberName;
                _addTask._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _addTask._CompanyName = _CompanyName;
                _addTask._CurrentLoginDateTime = _CurrentLoginDateTime;
                _addTask._SiteName = _SiteName;
                _addTask._StaffPic = _StaffPic;
                _addTask._UserName = _UserName;
                _addTask._CompanyID = _CompanyID;
                _addTask._SiteID = _SiteID;
                _addTask.Member_Image_Img.Source = Member_Image_Img.Source;
                _addTask.StaffImage.Source = StaffImage.Source;
                _addTask._dSAccessRights = _dSAccessRights;
                _addTask.dt = this.dt;
                _addTask.Owner = this.Owner;

                this.Close();
                _addTask.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private void cmbFilterTask_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cmbFilterTask.SelectedIndex != -1)
                {
                    _filterTaskId = 0;
                    if (cmbFilterTask.SelectedValue != null)
                        _filterTaskId = Convert.ToInt32(cmbFilterTask.SelectedValue.ToString());

                    if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
                        _staffId = 0;

                    LoadTaskList(_filterTaskId, _staffId);
                    ClearTextData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }


        }

        private void cmbFilterByStaff_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cmbFilterByStaff.SelectedIndex != -1)
                {
                    _staffId = Convert.ToInt32(cmbFilterByStaff.SelectedValue.ToString());

                    //if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
                    //    _staffId = 0;

                    _filterTaskId = 0;
                    ClearTextData();
                    LoadTaskList(_filterTaskId, _staffId);

                    if (cmbFilterByStaff.SelectedIndex == 0)
                    {
                        cmbAssignTo.SelectedIndex = -1;
                        cmbAssignTo.Text = "";
                    }
                    else
                    {
                        cmbAssignTo.SelectedValue = cmbFilterByStaff.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }

        }

        private void LoadTaskList(int _filterTaskId, int StaffId)
        {
            try
            {
                TaskBAL _taskBAL = new TaskBAL();
                DataSet Taskds = _taskBAL.get_TaskByStaffId(_filterTaskId, StaffId);

                if (Taskds != null)
                {
                    if (Taskds.Tables[0].DefaultView != null)
                    {
                        if (Taskds.Tables[0].Rows.Count > 0)
                        {
                            grdTask.ItemsSource = Taskds.Tables[0].AsDataView();
                        }
                        else
                        {
                            grdTask.ItemsSource = null;
                            grdTask.Items.Clear();
                        }
                    }
                    else
                    {
                        grdTask.ItemsSource = null;
                        grdTask.Items.Clear();
                    }
                }
                else
                {
                    grdTask.ItemsSource = null;
                    grdTask.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private bool validateData()
        {
            reval = true;

            if (string.IsNullOrEmpty(txtActions.Text.ToString()))
            {
                MessageBox.Show("Please Enter Action Taken").ToString();
                txtActions.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(dpExpiresOn.Text.ToString()))
            {
                MessageBox.Show("Please Enter Expired Date").ToString();
                dpExpiresOn.Focus();
                reval = false;
                return reval;
            }
            return true;
        }

        private void btnCompleteTask_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // value has to update from Enum for Complete
                //Status 4 --> Completed
                if (validateData())
                {
                    _taskStatus = 4;
                    ActionTaken(_taskStatus);
                    ClearTextData();
                    DisableTaskButtons();
                    GetStaffId();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private void btnCancelTask_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                // value has to update from Enum for cancel
                //Status 3-- > Cancel
                if (validateData())
                {
                    _taskStatus = 3;
                    ActionTaken(_taskStatus);
                    ClearTextData();
                    DisableTaskButtons();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }

        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Status 2-- > Followup
                if (validateData())
                {
                    _taskStatus = 2;
                    ActionTaken(_taskStatus);
                    ClearTextData();
                    DisableTaskButtons();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private void ActionTaken(int TaskStatus)
        {
            try
            {
                _staffActionTaken = txtActions.Text.ToString().Trim();
                _taskExpireDate = Convert.ToDateTime(dpExpiresOn.Text.ToString().Trim());
                TaskBAL _taskBAL = new TaskBAL();
                if (_isProspectTask)
                {
                    _addActionSucess = _taskBAL.add_ProspectStaffTask(_TaskId, _MemberNo, _staffId, TaskStatus, _taskExpireDate, _staffActionTaken);
                }
                else
                {
                    _addActionSucess = _taskBAL.add_StaffTask(_TaskId, _MemberNo, _staffId, TaskStatus, _taskExpireDate, _staffActionTaken);
                }

                if (_addActionSucess != 0)
                {
                    MessageBox.Show("Task Details Updated...!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private void cmbAssignTo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                //if (cmbAssignTo.SelectedIndex != -1)
                //{
                //    _assignedStaffId = Convert.ToInt32(cmbAssignTo.SelectedValue.ToString());

                //    TaskBAL _taskBAL = new TaskBAL();
                //    _taskBAL.change_StaffTask(_TaskId, _assignedStaffId);
                //    ///////THIS SHOULD BE CALLED IN 'SAVE' CLICK, IF STAFF CHANGED
                //    //LoadTaskList(_filterTaskId, _staffId);
                //    //ClearData();
                //    ///////THIS SHOULD BE CALLED IN 'SAVE' CLICK, IF STAFF CHANGED
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }

        }

        private void ClearData()
        {
            try
            {
                txtCellPhone.Text = string.Empty;
                txtHomePhone.Text = string.Empty;
                lblName.Content = "";

                txtOwing.Text = string.Empty;
                txtMemberNo.Text = string.Empty;
                txtLastVisit.Text = string.Empty;
                txtTaskNotes.Text = string.Empty;
                txtTaskNotes.Text = string.Empty;
                dpDueDate.Text = string.Empty;
                dpExpiresOn.Text = string.Empty;

                if (_wLoaded)
                {
                    cmbFilterTask.SelectedIndex = -1; //0
                    cmbFilterByStaff.SelectedIndex = -1; //0
                    cmbAssignTo.SelectedIndex = -1; //0
                }
                DisableTaskButtons();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }

        }

        private void ClearTextData()
        {
            try
            {
                txtCellPhone.Text = string.Empty;
                txtHomePhone.Text = string.Empty;
                lblName.Content = "";

                txtOwing.Text = string.Empty;
                txtMemberNo.Text = string.Empty;
                txtLastVisit.Text = string.Empty;
                txtTaskNotes.Text = string.Empty;
                txtTaskNotes.Text = string.Empty;
                dpDueDate.Text = string.Empty;
                dpExpiresOn.Text = string.Empty;
                txtActions.Text = "";
                DisableTaskButtons();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private void DisableTaskButtons()
        {
            try
            {
                btnTaskHistory.IsEnabled = false;
                btnShowMember.IsEnabled = false;
                btnCancelTask.IsEnabled = false;
                btnCompleteTask.IsEnabled = false;
                btnTaskHistory.IsEnabled = false;
                btnSave.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }

        }

        private void EnableTaskButtons()
        {
            try
            {
                btnTaskHistory.IsEnabled = true;
                btnShowMember.IsEnabled = true;
                btnCancelTask.IsEnabled = true;
                btnCompleteTask.IsEnabled = true;
                btnTaskHistory.IsEnabled = true;
                btnSave.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }

        }
        private void btnTaskHistory_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TaskHistory _taskHistory = new TaskHistory();
                _taskHistory.TaskHistoryStaffId = _staffId;
                _taskHistory.TaskHistoryMemberId = _MemberNo;
                _taskHistory.TaskId = _TaskId;
                _taskHistory._MEMBERorPROSPECT = _MEMBERorPROSPECT;
                _taskHistory._invokedFromDashboard = _invokedFromDashboard;
                _taskHistory.ExistingMember = ExistingMember;
                _taskHistory.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _taskHistory._MemberName = _MemberName;
                _taskHistory._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _taskHistory._CompanyName = _CompanyName;
                _taskHistory._CurrentLoginDateTime = _CurrentLoginDateTime;
                _taskHistory._SiteName = _SiteName;
                _taskHistory._StaffPic = _StaffPic;
                _taskHistory._UserName = _UserName;
                _taskHistory._CompanyID = _CompanyID;
                _taskHistory._SiteID = _SiteID;
                _taskHistory.Member_Image_Img.Source = Member_Image_Img.Source;
                _taskHistory.StaffImage.Source = StaffImage.Source;
                _taskHistory._dSAccessRights = _dSAccessRights;
                _taskHistory.dt = this.dt;
                _taskHistory.Owner = this.Owner;

                this.Close();
                _taskHistory.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public void SetAccessRightsDS(DataSet dS)
        {
            _dSAccessRights = new DataSet();
            _dSAccessRights = dS;
        }
        private void EnableDisableControls_AccessRights()
        {
            btnShowMember.IsEnabled = false;
            btnShowMember.Opacity = 0.5;

            btnCancelTask.IsEnabled = false;
            btnCancelTask.Opacity = 0.5;

            btnCompleteTask.IsEnabled = false;
            btnCompleteTask.Opacity = 0.5;

            btnTaskHistory.IsEnabled = false;
            btnTaskHistory.Opacity = 0.5;

            btnNewTask.IsEnabled = false;
            btnNewTask.Opacity = 0.5;

            btnSave.IsEnabled = false;
            btnSave.Opacity = 0.5;

            _dSAccessRights.Tables[_ModuleTable].DefaultView.RowFilter = "";
            _dSAccessRights.Tables[_SubModuleTable].DefaultView.RowFilter = "";

            if (_dSAccessRights.Tables[_SubModuleTable].DefaultView.Count > 0)
            {
                _dSAccessRights.Tables[_SubModuleTable].DefaultView.RowFilter = "ModuleName = 'TASK' AND ACTIVE = 1";
                for (int i = 0; i < _dSAccessRights.Tables[_SubModuleTable].DefaultView.Count; i++)
                {
                    switch (_dSAccessRights.Tables[_SubModuleTable].DefaultView[i]["SubModuleName"].ToString().ToUpper())
                    {
                        case "ADD TASK":
                            btnNewTask.IsEnabled = true;
                            btnNewTask.Opacity = 1.00;
                            break;
                        case "SHOW MEMBER":
                            btnShowMember.IsEnabled = true;
                            btnShowMember.Opacity = 1.00;
                            break;
                        case "CANCEL TASK":
                            btnCancelTask.IsEnabled = true;
                            btnCancelTask.Opacity = 1.00;
                            break;
                        case "COMPLETE TASK":
                            btnCompleteTask.IsEnabled = true;
                            btnCompleteTask.Opacity = 1.00;
                            break;
                        case "TASK HISTORY":
                            btnTaskHistory.IsEnabled = true;
                            btnTaskHistory.Opacity = 1.00;
                            break;
                        case "SAVE":
                            btnSave.IsEnabled = true;
                            btnSave.Opacity = 1.00;
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void btnShowMembership_Click(object sender, RoutedEventArgs e)
        {
            if (!ExistingMember)
            {
                if (_CanOpenMemberShipNewMember)
                {
                    Memberships _mDetails = new Memberships();
                    _mDetails.ExistingMember = ExistingMember;
                    _mDetails.CardNumber = CardNumber;
                    _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    _mDetails._MemberName = _MemberName;
                    _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    _mDetails._CompanyName = _CompanyName;
                    _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                    _mDetails._SiteName = _SiteName;
                    _mDetails._StaffPic = _StaffPic;
                    _mDetails._UserName = _UserName;
                    _mDetails._CompanyID = _CompanyID;
                    _mDetails._SiteID = _SiteID;
                    _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                    _mDetails.StaffImage.Source = StaffImage.Source;
                    _mDetails._dSAccessRights = _dSAccessRights;
                    _mDetails.dt = this.dt;
                    _mDetails.Owner = this.Owner;

                    this.Close();
                    _mDetails.ShowDialog();
                }
                else
                {
                    System.Windows.MessageBox.Show("New Member Details have to be Created First!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            else
            {
                Memberships _mDetails = new Memberships();
                _mDetails.ExistingMember = ExistingMember;
                _mDetails.CardNumber = CardNumber;
                _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _mDetails._MemberName = _MemberName;
                _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _mDetails._CompanyName = _CompanyName;
                _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                _mDetails._SiteName = _SiteName;
                _mDetails._StaffPic = _StaffPic;
                _mDetails._UserName = _UserName;
                _mDetails._CompanyID = _CompanyID;
                _mDetails._SiteID = _SiteID;
                _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                _mDetails.StaffImage.Source = StaffImage.Source;
                _mDetails._dSAccessRights = _dSAccessRights;
                _mDetails.dt = this.dt;
                _mDetails.Owner = this.Owner;

                this.Close();
                _mDetails.ShowDialog();
            }
        }

        private void btnAccounts_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberAccounts mAccounts = new MemberAccounts();
                mAccounts.MemberID = GetMemberDetailsMemberId;
                mAccounts._CurrentLoginDateTime = _CurrentLoginDateTime;
                mAccounts._CompanyName = _CompanyName;
                mAccounts._SiteName = _SiteName;
                mAccounts._UserName = _UserName;
                mAccounts._StaffPic = _StaffPic;
                mAccounts._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mAccounts.ExistingMember = ExistingMember;
                mAccounts.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mAccounts._MemberName = _MemberName;
                mAccounts.Member_Image_Img.Source = Member_Image_Img.Source;
                mAccounts.StaffImage.Source = StaffImage.Source;
                mAccounts._dSAccessRights = _dSAccessRights;
                mAccounts.dt = this.dt;

                mAccounts.Owner = this.Owner;
                this.Cursor = System.Windows.Input.Cursors.Wait;
                btnAccounts.Cursor = System.Windows.Input.Cursors.Wait;
                this.Close();
                mAccounts.ShowDialog();
            }
        }

        private void btnMemberBillingDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ExistingMember)
                {
                    this.Cursor = System.Windows.Input.Cursors.Wait;
                    PaymentDetails PD = new PaymentDetails(GetMemberDetailsMemberId, _CompanyID, _SiteID, dt);
                    this.Cursor = System.Windows.Input.Cursors.Arrow;

                    PD._CurrentLoginDateTime = _CurrentLoginDateTime;
                    PD._CompanyName = _CompanyName;
                    PD._SiteName = _SiteName;
                    PD._UserName = _UserName;
                    PD._StaffPic = _StaffPic;
                    PD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    PD.ExistingMember = ExistingMember;
                    PD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    PD._MemberName = _MemberName;
                    PD.Member_Image_Img.Source = Member_Image_Img.Source;
                    PD.StaffImage.Source = StaffImage.Source;
                    PD._dSAccessRights = _dSAccessRights;
                    PD.dt = this.dt;

                    PD.Owner = this.Owner;
                    this.Close();
                    PD.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnMemberExtraDetails_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                ExtraDetails extra = new ExtraDetails();
                extra.strExtraDetailsMemberID = GetMemberDetailsMemberId; //Convert.ToInt32(txtMemberNumber.Text.ToString().Trim());

                extra._CurrentLoginDateTime = _CurrentLoginDateTime;
                extra._CompanyName = _CompanyName;
                extra._SiteName = _SiteName;
                extra._UserName = _UserName;
                extra._StaffPic = _StaffPic;
                extra._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                extra.ExistingMember = ExistingMember;
                extra.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                extra._MemberName = _MemberName;
                extra.Member_Image_Img.Source = Member_Image_Img.Source;
                extra.StaffImage.Source = StaffImage.Source;
                extra._dSAccessRights = _dSAccessRights;
                extra.dt = this.dt;

                extra.Owner = this.Owner;
                this.Close();
                extra.ShowDialog();
            }
        }

        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {
        ////SAME WINDOW; NO ACTION
        }

        private void btnCommunications_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Communication _Comm = new Communication();
                _Comm.Owner = this.Owner;
                _Comm.SetAccessRightsDS(_dSAccessRights);
                this.Cursor = System.Windows.Input.Cursors.Arrow;

                _Comm._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Comm._CompanyName = _CompanyName;
                _Comm._SiteName = _SiteName;
                _Comm._UserName = _UserName;
                _Comm._StaffPic = _StaffPic;
                _Comm._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Comm.ExistingMember = ExistingMember;
                _Comm.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Comm._MemberName = _MemberName;
                _Comm.Member_Image_Img.Source = Member_Image_Img.Source;
                _Comm.StaffImage.Source = StaffImage.Source;
                _Comm._dSAccessRights = _dSAccessRights;
                _Comm.dt = this.dt;

                this.Close();

                _Comm.ShowDialog();
            }
        }

        private void btnNotes_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Notes _Notes = new Notes();
                _Notes.Owner = this.Owner;

                _Notes._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Notes._CompanyName = _CompanyName;
                _Notes._SiteName = _SiteName;
                _Notes._UserName = _UserName;
                _Notes._StaffPic = _StaffPic;
                _Notes._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Notes.ExistingMember = ExistingMember;
                _Notes.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Notes._MemberName = _MemberName;
                _Notes.Member_Image_Img.Source = Member_Image_Img.Source;
                _Notes.StaffImage.Source = StaffImage.Source;
                _Notes._dSAccessRights = _dSAccessRights;
                _Notes.dt = this.dt;

                this.Cursor = System.Windows.Input.Cursors.Arrow;
                this.Close();
                _Notes.ShowDialog();
            }
        }

        private void btnPersonalTraining_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                PersonalTraining _pt = new PersonalTraining();
                _pt.Owner = this.Owner;

                _pt._CurrentLoginDateTime = _CurrentLoginDateTime;
                _pt._CompanyName = _CompanyName;
                _pt._SiteName = _SiteName;
                _pt._UserName = _UserName;
                _pt._StaffPic = _StaffPic;
                _pt._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _pt.ExistingMember = ExistingMember;
                _pt.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _pt._MemberName = _MemberName;
                _pt.Member_Image_Img.Source = Member_Image_Img.Source;
                _pt.StaffImage.Source = StaffImage.Source;
                _pt._dSAccessRights = _dSAccessRights;
                _pt.dt = this.dt;

                this.Close();
                _pt.ShowDialog();
            }
        }

        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberDetails mD = new MemberDetails();
                mD.Owner = this.Owner;

                mD._CurrentLoginDateTime = _CurrentLoginDateTime;
                mD._CompanyName = _CompanyName;
                mD._SiteName = _SiteName;
                mD._UserName = _UserName;
                mD._StaffPic = _StaffPic;
                mD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mD.ExistingMember = ExistingMember;
                mD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mD._MemberName = _MemberName;
                mD.Member_Image_Img.Source = Member_Image_Img.Source;
                mD.StaffImage.Source = StaffImage.Source;
                mD._dSAccessRights = _dSAccessRights;
                mD.dt = this.dt;

                this.Close();
                mD.ShowDialog();
            }
        }
        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;

            if (_MemberName != null)
            {
                if (_MemberName.Trim() != "")
                {
                    Heading.Text = Heading.Text + " - " + _MemberName;
                }
            }
            //loadStaffPhoto(_StaffPic);
        }
        private void loadStaffPhoto(byte[] _StaffPic)
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }
    }
}
