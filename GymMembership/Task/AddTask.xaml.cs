﻿using System;
using System.Collections;
using System.Windows;
using System.Configuration;
using System.IO;
using System.Windows.Media.Imaging;
using System.Data;

using GymMembership.Comman;
using GymMembership.BAL;
using GymMembership.MemberInfo;
using GymMembership.Communications;


namespace GymMembership.Task
{
    /// <summary>
    /// Interaction logic for AddTask.xaml
    /// </summary>
    public partial class AddTask
    {
        public string _MemberName;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public bool _CanOpenMemberShipNewMember = false;
        public bool ExistingMember = false;
        public int GetMemberDetailsMemberId; public long CardNumber;
        public DataSet _dSAccessRights;// = new DataSet();
        public DataTable dt = new DataTable();

        public bool _invokedFromDashboard = false;
        public bool _fromProspect = false;
        public int _ProspectID = 0;

        public int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
        public int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
        int _MemberDetailsDataTable = 0; int _MemberShipDataTable = 1;
        DataSet _ds = new DataSet();

        FormUtilities _formUtilities = new FormUtilities();
        TaskBAL _taskBAL = new TaskBAL();
        DataSet AddTaskTypeDs = new DataSet();
        private int _taskTypeId = 0;
        private int _taskMemberId = 0;
        private int _staffTaskTypeId = 0;
        private string _assignedStaffName = string.Empty;
        private string _staffTaskTypeDescription = string.Empty;
        private DateTime _staffTaskTypeDate;
        private int _assignedStaff = 0;
        private bool reval;
        private int _addTaskSuccess = 0;
        private int _taskStatus = 0;
        public AddTask()

        {
            InitializeComponent();
        }
        private void AddTask_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_invokedFromDashboard)
                {
                    listBox.Visibility = Visibility.Collapsed;
                    col.Width = new GridLength(26);
                }
                else
                {
                    listBox.Visibility = Visibility.Visible;
                }

                if (!_fromProspect)
                {
                    tbBack.Text = "Back to Dashboard";
                    txtAddTaskMember.Focus();
                }
                else
                {
                    tbBack.Text = "Back To Prospect";
                    colAddTaskList0.Width = new GridLength(0, GridUnitType.Star);
                    colAddTaskList1.Width = new GridLength(0, GridUnitType.Star);
                }

                populateData();
                SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
            }
            catch (Exception ex)
            {
                if (!ex.Message.ToString().ToUpper().Contains("(DLL)"))
                {
                    MessageBox.Show("Add Task Form Load Error! " + ex.Message, this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
                }
            }
        }

        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto(_StaffPic);
        }
        private void loadStaffPhoto(byte[] _StaffPic)
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }

        public void SetAccessRightsDS(DataSet dS)
        {
            _dSAccessRights = new DataSet();
            _dSAccessRights = dS;
        }

        private void btnAddTaskCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void populateData()
        {
            try
            {
                Utilities _utilities = new Utilities();
                _utilities.PopulateTaskType(cmbTaskType, _taskTypeId);
                _utilities.populatePersonalTrainer(cmbStaffAssigned);
                ClearNewTaskData();
                if (!_fromProspect)
                {
                    LoadMemberListFromDBOnLoad();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void LoadMemberListFromDBOnLoad()
        {
            _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            SqlHelper sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            _ds = new DataSet();
            _ds = sql.ExecuteProcedure("GetMemberDetailsFindMember", ht);
            gvTaskMemberDetails.ItemsSource = null;

            _ds.Tables[_MemberDetailsDataTable].DefaultView.RowFilter = "";
            _ds.Tables[_MemberDetailsDataTable].DefaultView.RowFilter = "Status = 'Active'";
            gvTaskMemberDetails.ItemsSource = _ds.Tables[_MemberDetailsDataTable].DefaultView;
        }
        private void btnShowMember_Click(object sender, RoutedEventArgs e)
        {
            if (txtAddTaskMember.Text.Trim() != "")
            {
                _ds.Tables[_MemberDetailsDataTable].DefaultView.RowFilter = "";
                _ds.Tables[_MemberDetailsDataTable].DefaultView.RowFilter = "(LastName LIKE '%" + txtAddTaskMember.Text.Trim().Replace("'","''") 
                        + "%' OR FirstName LIKE '%" + txtAddTaskMember.Text.Trim().Replace("'","''") + "%' OR MemberNo LIKE '%" 
                        + txtAddTaskMember.Text.Trim().Replace("'", "''") + "%' OR CardNumber LIKE '%" + txtAddTaskMember.Text.Trim().Replace("'", "''") + "%')";
                gvTaskMemberDetails.ItemsSource = _ds.Tables[_MemberDetailsDataTable].DefaultView;
            }
        }
        private bool validateData()
        {
            reval = true;


            if (cmbTaskType.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select a Task Type!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                cmbTaskType.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtTaskDescription.Text.ToString()))
            {
                MessageBox.Show("Please Enter Task Description", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtTaskDescription.Focus();
                reval = false;
                return reval;
            }


            if (cldrAddTaskDt.SelectedDate.ToString().Trim() == string.Empty)
            {
                MessageBox.Show("Please select Due Date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                reval = false;
                return reval;

            }
            if (cmbStaffAssigned.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Staff", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                cmbStaffAssigned.Focus();
                reval = false;
                return reval;
            }

            if (!_fromProspect)
            {
                if (gvTaskMemberDetails.SelectedIndex == -1)
                {
                    MessageBox.Show("Select a Member from List!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    gvTaskMemberDetails.Focus();
                    reval = false;
                    return reval;
                }
            }

            return true;
        }

        private void ClearNewTaskData()
        {
            txtAddTaskMember.Text = string.Empty;
            cmbTaskType.SelectedIndex = 0;
            txtTaskDescription.Text = string.Empty;
            cldrAddTaskDt.SelectedDate = (DateTime)cldrAddTaskDt.DisplayDate;
            cmbStaffAssigned.SelectedIndex = 0;
            _assignedStaffName = string.Empty;
            
            this.gvTaskMemberDetails.ItemsSource = null;

        }

        private void btnAddTaskOK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (validateData())
                {
                    if (!_fromProspect)
                    {
                        _taskMemberId = Convert.ToInt32((gvTaskMemberDetails.Items[gvTaskMemberDetails.SelectedIndex] as DataRowView).Row["MemberNo"].ToString());
                    }
                    else
                    {
                        _taskMemberId = _ProspectID;
                    }
                    _staffTaskTypeId = Convert.ToInt32(cmbTaskType.SelectedValue.ToString());
                    _staffTaskTypeDescription = txtTaskDescription.Text.ToString().Trim();

                    if (cldrAddTaskDt.SelectedDate.ToString().Trim() == string.Empty)
                    {
                        _staffTaskTypeDate = (DateTime)cldrAddTaskDt.DisplayDate;
                    }
                    else
                    {
                        _staffTaskTypeDate = (DateTime)cldrAddTaskDt.SelectedDate;
                    }

                    _assignedStaff = Convert.ToInt32(cmbStaffAssigned.SelectedValue.ToString());
                    _assignedStaffName = cmbStaffAssigned.Text.ToString();
                    _taskStatus = 1;
                    if (_fromProspect)
                    {
                        _addTaskSuccess = _taskBAL.add_ProspectTaskToStaff(_taskMemberId, _staffTaskTypeId, _staffTaskTypeDescription, _staffTaskTypeDate, _assignedStaff, _taskStatus);
                    }
                    else
                    {
                        _addTaskSuccess = _taskBAL.add_TaskToStaff(_taskMemberId, _staffTaskTypeId, _staffTaskTypeDescription, _staffTaskTypeDate, _assignedStaff, _taskStatus);
                    }

                    if (_addTaskSuccess != 0)
                    {
                        MessageBox.Show("Task Added to : " + _assignedStaffName.ToString(), this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        ClearNewTaskData();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnbacktoTask_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberDetails mD = new MemberDetails();
                mD.Owner = this.Owner;

                mD._CurrentLoginDateTime = _CurrentLoginDateTime;
                mD._CompanyName = _CompanyName;
                mD._SiteName = _SiteName;
                mD._UserName = _UserName;
                mD._StaffPic = _StaffPic;
                mD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mD.ExistingMember = ExistingMember;
                mD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mD._MemberName = _MemberName;
                mD.Member_Image_Img.Source = Member_Image_Img.Source;
                mD.StaffImage.Source = StaffImage.Source;
                mD._dSAccessRights = _dSAccessRights;
                mD.dt = this.dt;

                this.Close();
                mD.ShowDialog();
            }
        }


        private void btnShowMembership_Click(object sender, RoutedEventArgs e)
        {
            if (!ExistingMember)
            {
                if (_CanOpenMemberShipNewMember)
                {
                    Memberships _mDetails = new Memberships();
                    _mDetails.ExistingMember = ExistingMember;
                    _mDetails.CardNumber = CardNumber;
                    _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    _mDetails._MemberName = _MemberName;
                    _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    _mDetails._CompanyName = _CompanyName;
                    _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                    _mDetails._SiteName = _SiteName;
                    _mDetails._StaffPic = _StaffPic;
                    _mDetails._UserName = _UserName;
                    _mDetails._CompanyID = _CompanyID;
                    _mDetails._SiteID = _SiteID;
                    _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                    _mDetails.StaffImage.Source = StaffImage.Source;
                    _mDetails._dSAccessRights = _dSAccessRights;
                    _mDetails.dt = this.dt;
                    _mDetails.Owner = this.Owner;

                    this.Close();
                    _mDetails.ShowDialog();
                }
                else
                {
                    System.Windows.MessageBox.Show("New Member Details have to be Created First!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            else
            {
                Memberships _mDetails = new Memberships();
                _mDetails.ExistingMember = ExistingMember;
                _mDetails.CardNumber = CardNumber;
                _mDetails.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _mDetails._MemberName = _MemberName;
                _mDetails._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _mDetails._CompanyName = _CompanyName;
                _mDetails._CurrentLoginDateTime = _CurrentLoginDateTime;
                _mDetails._SiteName = _SiteName;
                _mDetails._StaffPic = _StaffPic;
                _mDetails._UserName = _UserName;
                _mDetails._CompanyID = _CompanyID;
                _mDetails._SiteID = _SiteID;
                _mDetails.Member_Image_Img.Source = Member_Image_Img.Source;
                _mDetails.StaffImage.Source = StaffImage.Source;
                _mDetails._dSAccessRights = _dSAccessRights;
                _mDetails.dt = this.dt;
                _mDetails.Owner = this.Owner;

                this.Close();
                _mDetails.ShowDialog();
            }
        }

        private void btnAccounts_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                MemberAccounts mAccounts = new MemberAccounts();
                mAccounts.MemberID = GetMemberDetailsMemberId;
                mAccounts._CurrentLoginDateTime = _CurrentLoginDateTime;
                mAccounts._CompanyName = _CompanyName;
                mAccounts._SiteName = _SiteName;
                mAccounts._UserName = _UserName;
                mAccounts._StaffPic = _StaffPic;
                mAccounts._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                mAccounts.ExistingMember = ExistingMember;
                mAccounts.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                mAccounts._MemberName = _MemberName;
                mAccounts.Member_Image_Img.Source = Member_Image_Img.Source;
                mAccounts.StaffImage.Source = StaffImage.Source;
                mAccounts._dSAccessRights = _dSAccessRights;
                mAccounts.dt = this.dt;

                mAccounts.Owner = this.Owner;
                this.Cursor = System.Windows.Input.Cursors.Wait;
                btnAccounts.Cursor = System.Windows.Input.Cursors.Wait;
                this.Close();
                mAccounts.ShowDialog();
            }
        }

        private void btnMemberBillingDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ExistingMember)
                {
                    this.Cursor = System.Windows.Input.Cursors.Wait;
                    PaymentDetails PD = new PaymentDetails(GetMemberDetailsMemberId, _CompanyID, _SiteID, dt);
                    this.Cursor = System.Windows.Input.Cursors.Arrow;

                    PD._CurrentLoginDateTime = _CurrentLoginDateTime;
                    PD._CompanyName = _CompanyName;
                    PD._SiteName = _SiteName;
                    PD._UserName = _UserName;
                    PD._StaffPic = _StaffPic;
                    PD._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                    PD.ExistingMember = ExistingMember;
                    PD.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                    PD._MemberName = _MemberName;
                    PD.Member_Image_Img.Source = Member_Image_Img.Source;
                    PD.StaffImage.Source = StaffImage.Source;
                    PD._dSAccessRights = _dSAccessRights;
                    PD.dt = this.dt;

                    PD.Owner = this.Owner;
                    this.Close();
                    PD.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnMemberExtraDetails_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                ExtraDetails extra = new ExtraDetails();
                extra.strExtraDetailsMemberID = GetMemberDetailsMemberId; //Convert.ToInt32(txtMemberNumber.Text.ToString().Trim());

                extra._CurrentLoginDateTime = _CurrentLoginDateTime;
                extra._CompanyName = _CompanyName;
                extra._SiteName = _SiteName;
                extra._UserName = _UserName;
                extra._StaffPic = _StaffPic;
                extra._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                extra.ExistingMember = ExistingMember;
                extra.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                extra._MemberName = _MemberName;
                extra.Member_Image_Img.Source = Member_Image_Img.Source;
                extra.StaffImage.Source = StaffImage.Source;
                extra._dSAccessRights = _dSAccessRights;
                extra.dt = this.dt;

                extra.Owner = this.Owner;
                this.Close();
                extra.ShowDialog();
            }
        }

        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                Tasks Tsk = new Tasks();
                Tsk.strMemberNo = GetMemberDetailsMemberId; //Convert.ToInt32(txtMemberNumber.Text);
                Tsk.Owner = this.Owner;
                Tsk.SetAccessRightsDS(_dSAccessRights);

                Tsk._CurrentLoginDateTime = _CurrentLoginDateTime;
                Tsk._CompanyName = _CompanyName;
                Tsk._SiteName = _SiteName;
                Tsk._UserName = _UserName;
                Tsk._StaffPic = _StaffPic;
                Tsk._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                Tsk.ExistingMember = ExistingMember;
                Tsk.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                Tsk._MemberName = _MemberName;
                Tsk.Member_Image_Img.Source = Member_Image_Img.Source;
                Tsk.StaffImage.Source = StaffImage.Source;
                Tsk._dSAccessRights = _dSAccessRights;
                Tsk.dt = this.dt;

                this.Close();

                Tsk.ShowDialog();
            }
        }

        private void btnCommunications_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Communication _Comm = new Communication();
                _Comm.Owner = this.Owner;
                _Comm.SetAccessRightsDS(_dSAccessRights);
                this.Cursor = System.Windows.Input.Cursors.Arrow;

                _Comm._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Comm._CompanyName = _CompanyName;
                _Comm._SiteName = _SiteName;
                _Comm._UserName = _UserName;
                _Comm._StaffPic = _StaffPic;
                _Comm._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Comm.ExistingMember = ExistingMember;
                _Comm.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Comm._MemberName = _MemberName;
                _Comm.Member_Image_Img.Source = Member_Image_Img.Source;
                _Comm.StaffImage.Source = StaffImage.Source;
                _Comm._dSAccessRights = _dSAccessRights;
                _Comm.dt = this.dt;

                this.Close();

                _Comm.ShowDialog();
            }
        }

        private void btnNotes_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                Notes _Notes = new Notes();
                _Notes.Owner = this.Owner;

                _Notes._CurrentLoginDateTime = _CurrentLoginDateTime;
                _Notes._CompanyName = _CompanyName;
                _Notes._SiteName = _SiteName;
                _Notes._UserName = _UserName;
                _Notes._StaffPic = _StaffPic;
                _Notes._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _Notes.ExistingMember = ExistingMember;
                _Notes.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _Notes._MemberName = _MemberName;
                _Notes.Member_Image_Img.Source = Member_Image_Img.Source;
                _Notes.StaffImage.Source = StaffImage.Source;
                _Notes._dSAccessRights = _dSAccessRights;
                _Notes.dt = this.dt;

                this.Cursor = System.Windows.Input.Cursors.Arrow;
                this.Close();
                _Notes.ShowDialog();
            }
        }

        private void btnPersonalTraining_Click(object sender, RoutedEventArgs e)
        {
            if (ExistingMember)
            {
                PersonalTraining _pt = new PersonalTraining();
                _pt.Owner = this.Owner;

                _pt._CurrentLoginDateTime = _CurrentLoginDateTime;
                _pt._CompanyName = _CompanyName;
                _pt._SiteName = _SiteName;
                _pt._UserName = _UserName;
                _pt._StaffPic = _StaffPic;
                _pt._CanOpenMemberShipNewMember = _CanOpenMemberShipNewMember;
                _pt.ExistingMember = ExistingMember;
                _pt.GetMemberDetailsMemberId = GetMemberDetailsMemberId;
                _pt._MemberName = _MemberName;
                _pt.Member_Image_Img.Source = Member_Image_Img.Source;
                _pt.StaffImage.Source = StaffImage.Source;
                _pt._dSAccessRights = _dSAccessRights;
                _pt.dt = this.dt;

                this.Close();
                _pt.ShowDialog();
            }
        }

    }
}
