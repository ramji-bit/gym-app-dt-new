﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GymMembership.DAL;
using GymMembership.DTO;
using GymMembership.MemberInfo;
using System.Windows.Forms;

namespace GymMembership.BAL
{
    public class BillingBAL
    {
        BillingDAL _billingDAL = new BillingDAL();

        public DataSet get_BillingHistoryByMemberId(int MemberId)
        {
            try
            {
                return _billingDAL.get_BillingHistoryByMemberId(MemberId);
            }
            catch (Exception)
            {
                return null;
            }
        }


        public DataSet get_BillingCompany()
        {
            try
            {
                return _billingDAL.get_BillingCompany();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet get_BankDetailsByMemberId(int MemberId)
        {
            try
            {
                return _billingDAL.get_BankDetailsByMemberId(MemberId);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet get_BankCardType()
        {
            try
            {
                return _billingDAL.get_BankCardType();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int SaveBankDetails(MemberBankDetailsDTO memberBankDetailsDTO)
        {
            try
            {
                return _billingDAL.SaveBankDetails(memberBankDetailsDTO);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public DataSet get_PaymentDetails(int MemberId)
        {
            try
            {
                return _billingDAL.Get_PaymentDetails(MemberId);
            }
            catch (Exception)
            {
                return null;
            }
        }


    }
}
