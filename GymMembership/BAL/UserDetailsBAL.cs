﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GymMembership.DAL;
using GymMembership.DTO;

namespace GymMembership.BAL
{
    public class UserDetailsBAL
    {
        UserDetailsDAL _userDetailsDAL = new UserDetailsDAL();

        public DataSet get_UserLogin(string UserName,string Password, string CompanyName)
        {
            return _userDetailsDAL.get_UserLogin(UserName, Password, CompanyName);
        }

        public DataSet Get_UserCompanySiteDetails(string UName, string Pwd, string CompanyName)
        {
            return _userDetailsDAL.Get_UserCompanySiteDetails(UName, Pwd, CompanyName);
        }
        public DataSet OrgNameExistsinDB(string _strOrgName)
        {
            return _userDetailsDAL.OrgNameExistsinDB(_strOrgName);
        }
        public DataSet CheckIfUserAlreadyExists(string CompanyName, string SiteName, string UserName)
        {
            return _userDetailsDAL.CheckIfUserAlreadyExists(CompanyName, SiteName, UserName);
        }
        public DataSet AddNEWLoginAndStaffDetails(UserLoginDTO _UserLoginDTO)
        {
            return _userDetailsDAL.AddNEWLoginAndStaffDetails(_UserLoginDTO);
        }
        public int UpdateLoginAndStaffDetails(UserLoginDTO _UserLoginDTO)
        {
            return _userDetailsDAL.UpdateLoginAndStaffDetails(_UserLoginDTO);
        }
        public DataSet GetOrgNameFromDB()
        {
            return _userDetailsDAL.GetOrgNameFromDB();
        }
        public DataSet AddNewOrganizationName( string _OrgName)
        {
            return _userDetailsDAL.AddNewOrganizationName(_OrgName);
        }
        public int UA_AddDefaultAccessRightsOnLoginCreation(int LoginID, int AccessRights, int CompanyID, int SiteID)
        {
            return _userDetailsDAL.UA_AddDefaultAccessRightsOnLoginCreation(LoginID, AccessRights, CompanyID, SiteID);
        }
    }
}
