﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GymMembership.DAL;
using GymMembership.DTO;
using GymMembership.MemberInfo;
using System.Windows.Forms;

namespace GymMembership.BAL
{
    public class SettingsBAL
    {
        SettingsDAL _settingDetailsDAL = new SettingsDAL();
        public DataSet get_MembershipGroupById(int GroupId)
        {
            try
            {
                return _settingDetailsDAL.get_MembershipGroupById(GroupId);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int add_ProgrammeGroups(int ProgrammeGroupId, string GroupName, string Abbreviation, int GroupMembershipCard, int GroupStatus)
        {
            try
            {
                return _settingDetailsDAL.add_ProgrammeGroups(ProgrammeGroupId, GroupName, Abbreviation, GroupMembershipCard, GroupStatus);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int addNew_ProgrammeGroups(string GroupName, string Abbreviation, int GroupMembershipCard, int GroupStatus)
        {
            try
            {
                return _settingDetailsDAL.addNew_ProgrammeGroups(GroupName, Abbreviation, GroupMembershipCard, GroupStatus);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int saveEdit_ProgrammeDetails(int ProgramId, string ProgrammeName, decimal ProgrammePrice, int ProgrammeLength, int ProgrammeDuration, int NoOfVisits, decimal SignUpFee, string Conditions, int ProgrammeStatus, DateTime ProgrammeSdate, bool _isUpFront = false, bool _isNeverExpire = false)
        {
            try
            {
                return _settingDetailsDAL.saveEdit_ProgrammeDetails(ProgramId, ProgrammeName, ProgrammePrice, ProgrammeLength, ProgrammeDuration, NoOfVisits, SignUpFee, Conditions, ProgrammeStatus, ProgrammeSdate, _isUpFront, _isNeverExpire);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int saveNew_ProgrammeDetails(int ProgrammeGroupId, string ProgrammeName, decimal ProgrammePrice, int ProgrammeLength, int ProgrammeDuration, int NoOfVisits, decimal SignUpFee, string Conditions, int ProgrammeStatus, DateTime ProgrammeSdate, bool _isUpFront = false, bool _isNeverExpire = false)
        {
            try
            {
                return _settingDetailsDAL.saveNew_ProgrammeDetails(ProgrammeGroupId, ProgrammeName, ProgrammePrice, ProgrammeLength, ProgrammeDuration, NoOfVisits, SignUpFee, Conditions, ProgrammeStatus, ProgrammeSdate, _isUpFront, _isNeverExpire);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        // 24/3/2016
        public int saveNew_productType(string ProductType)
        {
            try
            {
                return _settingDetailsDAL.saveNew_productType(ProductType);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int saveEdit_productType(int ProductTypeId, string ProductType, int Active = 1)
        {
            try
            {
                return _settingDetailsDAL.saveEdit_productType(ProductTypeId, ProductType, Active);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public int saveEdit_ProductDetails(int ProductId, string ProductName, Decimal ProductCostPrice, Decimal ProductSellingPrice, int Active = 1, int ReOrderQty = 0, string alertEmail = "")
        {
            try
            {
                return _settingDetailsDAL.saveEdit_ProductDetails(ProductId, ProductName, ProductCostPrice, ProductSellingPrice, Active, ReOrderQty, alertEmail);
            }
            catch (Exception)
            {
                return 0;
            }
        }


        public int saveNew_ProductDetails(int ProductTypeId, string ProductName, decimal ProductCostPrice, decimal ProductSellingPrice, int ReOrderQty = 0, string alertEmail = "")
        {
            try
            {
                return _settingDetailsDAL.saveNew_ProductDetails(ProductTypeId, ProductName, ProductCostPrice, ProductSellingPrice, ReOrderQty, alertEmail);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public DataSet get_SupplierName()
        {
            try
            {
                return _settingDetailsDAL.getSupplierName();
            }
            catch (Exception)
            {
                return null;
            }
        }


        public DataSet get_SupplierDetails(int SupplierId)
        {
            try
            {
                return _settingDetailsDAL.get_SupplierDetails(SupplierId);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int saveEdit_SupplierDetails(int SupplierId, string SupplierName, string MobileNo, string PhoneNo, string FaxNo, string Email, string Website, string Notes, string ContactPerson)
        {
            try
            {
                return _settingDetailsDAL.saveEdit_SupplierDetails(SupplierId, SupplierName, MobileNo, PhoneNo, FaxNo, Email, Website, Notes, ContactPerson);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int saveNew_SupplierDetails(string SupplierName, string MobileNo, string PhoneNo, string FaxNo, string Email, string Website, string ContactPerson, string Notes)
        {
            try
            {
                return _settingDetailsDAL.saveNew_SupplierDetails(SupplierName, MobileNo, PhoneNo, FaxNo, Email, Website, ContactPerson, Notes);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public DataSet get_ResourceType()
        {
            try
            {
                return _settingDetailsDAL.getResourceType();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet get_ResourceById(int ResourceTypeId)
        {
            try
            {
                return _settingDetailsDAL.get_ResourceById(ResourceTypeId);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet get_ResourceByFacilityId(int ResourceId)
        {
            try
            {
                return _settingDetailsDAL.get_ResourceFacilityId(ResourceId);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int saveNew_ResourcesDetails(BookingResourceDTO _bookingResourceDTO)
        {
            try
            {
                return _settingDetailsDAL.saveNew_ResourcesDetails(_bookingResourceDTO);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int saveEdit_ResourcesDetails(string _FacilityName, int _FacilityStatus, int _ResourceId, int _MaxMembers = 0, int _MaxWaitList = 0)
        {
            try
            {
                return _settingDetailsDAL.saveEdit_ResourcesDetails(_FacilityName, _FacilityStatus, _ResourceId, _MaxMembers, _MaxWaitList);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public DataSet GetActiveUserNamesUAdmin()
        {
            try
            {
                return _settingDetailsDAL.GetActiveUserNamesUAdmin();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet GetAllContactTypes()
        {
            try
            {
                return _settingDetailsDAL.GetAllContactTypes();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
