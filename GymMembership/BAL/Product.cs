﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;
using System.Configuration;

namespace GymMembership.BAL
{
    public class Product
    {
        public DataSet getProductsbyType(string _prodType)
        {
            try
            {
                DataSet ds;

                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                if (Comman.Constants._OfflineFlag)
                {
                    Hashtable ht = new Hashtable();
                    ht.Add("@ProductType", _prodType);
                    ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                    //ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                    ds = sql.ExecuteProcedure("GetProductsbyType", ht);
                }
                else
                {
                    ds = new DataSet();
                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("ProductName", typeof(string));
                    dtResult.Columns.Add("ProductID", typeof(int));
                    dtResult.Columns.Add("CurrQuantity", typeof(int));
                    dtResult.Columns.Add("ProductSalePrice", typeof(decimal));
                    dtResult.Columns.Add("ProductCostPrice", typeof(decimal));

                    var result = from Products in Comman.Constants._DisConnectedALLTables.Tables["Product"].AsEnumerable()
                                 join ProductTypes in Comman.Constants._DisConnectedALLTables.Tables["ProductType"].AsEnumerable()
                                 on Products.Field<int>("ProductTypeID") equals ProductTypes.Field<int>("ProductTypeID")
                                 where (Products.Field<int>("CompanyID") == _CompanyID
                                 && ProductTypes.Field<int>("CompanyID") == _CompanyID
                                 && ProductTypes.Field<string>("ProductTypeName").ToUpper() == _prodType.ToUpper())

                                 select Products;

                    foreach (var v in result)
                    {
                        DataRow dr = dtResult.NewRow();
                        dr["ProductName"] = v.Field<string>("ProductName");
                        dr["ProductID"] = v.Field<int>("ProductID");
                        dr["CurrQuantity"] = v.Field<int>("CurrQuantity");
                        dr["ProductSalePrice"] = v.Field<decimal>("ProductSalePrice");
                        dr["ProductCostPrice"] = v.Field<decimal>("ProductCostPrice");

                        dtResult.Rows.Add(dr);
                    }
                    ds.Tables.Add(dtResult);
                }
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        public DataSet getAllProducts()
        {
            try
            {
                DataSet ds  = new DataSet();

                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                if (Comman.Constants._OfflineFlag)
                {
                    Hashtable ht = new Hashtable();
                    ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                    //ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                    ds = sql.ExecuteProcedure("GetAllProducts", ht);
                }
                return ds;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }
        public DataSet getAllProducts_Active()
        {
            try
            {
                DataSet ds = new DataSet();

                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                if (Comman.Constants._OfflineFlag)
                {
                    Hashtable ht = new Hashtable();
                    ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                    //ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                    ds = sql.ExecuteProcedure("GetAllProducts_Active", ht);
                }
                return ds;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }
        public DataSet getProductsbyType_Active(string _prodType)
        {
            try
            {
                DataSet ds;

                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                if (Comman.Constants._OfflineFlag)
                {
                    Hashtable ht = new Hashtable();
                    ht.Add("@ProductType", _prodType);
                    ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
                    ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));

                    ds = sql.ExecuteProcedure("GetProductsbyType_Active", ht);
                }
                else
                {
                    ds = new DataSet();
                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("ProductName", typeof(string));
                    dtResult.Columns.Add("ProductID", typeof(int));
                    dtResult.Columns.Add("CurrQuantity", typeof(int));
                    dtResult.Columns.Add("ProductSalePrice", typeof(decimal));
                    dtResult.Columns.Add("ProductCostPrice", typeof(decimal));

                    var result = from Products in Comman.Constants._DisConnectedALLTables.Tables["Product"].AsEnumerable()
                                 join ProductTypes in Comman.Constants._DisConnectedALLTables.Tables["ProductType"].AsEnumerable()
                                 on Products.Field<int>("ProductTypeID") equals ProductTypes.Field<int>("ProductTypeID")
                                 where (Products.Field<int>("CompanyID") == _CompanyID
                                 && ProductTypes.Field<int>("CompanyID") == _CompanyID
                                 && ProductTypes.Field<string>("ProductTypeName").ToUpper() == _prodType.ToUpper())

                                 select Products;

                    foreach (var v in result)
                    {
                        DataRow dr = dtResult.NewRow();
                        dr["ProductName"] = v.Field<string>("ProductName");
                        dr["ProductID"] = v.Field<int>("ProductID");
                        dr["CurrQuantity"] = v.Field<int>("CurrQuantity");
                        dr["ProductSalePrice"] = v.Field<decimal>("ProductSalePrice");
                        dr["ProductCostPrice"] = v.Field<decimal>("ProductCostPrice");

                        dtResult.Rows.Add(dr);
                    }
                    ds.Tables.Add(dtResult);
                }
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        public DataSet getProductTypes()
        {
            try
            {
                DataSet ds;

                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                if (Comman.Constants._OfflineFlag)
                {
                    Hashtable ht = new Hashtable();
                    ht.Add("@CompanyID", _CompanyID);
                    //ht.Add("@SiteID", _SiteID);

                    ds = sql.ExecuteProcedure("GetProductTypes", ht);
                }
                else
                {
                    ds = new DataSet();
                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("ProductTypeName", typeof(string));
                    dtResult.Columns.Add("ProductTypeID", typeof(int));

                    var result = from ProductTypes in Comman.Constants._DisConnectedALLTables.Tables["ProductType"].AsEnumerable()
                                 where (ProductTypes.Field<int>("CompanyID") == _CompanyID)

                                 select ProductTypes;

                    foreach (var v in result)
                    {
                        DataRow dr = dtResult.NewRow();
                        dr["ProductTypeName"] = v.Field<string>("ProductTypeName");
                        dr["ProductTypeID"] = v.Field<int>("ProductTypeID");
                        dtResult.Rows.Add(dr);
                    }
                    ds.Tables.Add(dtResult);
                }
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        public DataSet getProductTypes_Inventory()
        {
            try
            {
                DataSet ds;

                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                if (Comman.Constants._OfflineFlag)
                {
                    Hashtable ht = new Hashtable();
                    ht.Add("@CompanyID", _CompanyID);
                    //ht.Add("@SiteID", _SiteID);

                    ds = sql.ExecuteProcedure("GetProductTypes_Inventory", ht);
                }
                else
                {
                    ds = null;
                }
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        public DataSet getProductTypes_Active()
        {
            try
            {
                DataSet ds;

                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return null;

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                if (Comman.Constants._OfflineFlag)
                {
                    Hashtable ht = new Hashtable();
                    ht.Add("@CompanyID", _CompanyID);
                    //ht.Add("@SiteID", _SiteID);

                    ds = sql.ExecuteProcedure("GetProductTypes_Active", ht);
                }
                else
                {
                    ds = new DataSet();
                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("ProductTypeName", typeof(string));
                    dtResult.Columns.Add("ProductTypeID", typeof(int));

                    var result = from ProductTypes in Comman.Constants._DisConnectedALLTables.Tables["ProductType"].AsEnumerable()
                                 where (ProductTypes.Field<int>("CompanyID") == _CompanyID)

                                 select ProductTypes;

                    foreach (var v in result)
                    {
                        DataRow dr = dtResult.NewRow();
                        dr["ProductTypeName"] = v.Field<string>("ProductTypeName");
                        dr["ProductTypeID"] = v.Field<int>("ProductTypeID");
                        dtResult.Rows.Add(dr);
                    }
                    ds.Tables.Add(dtResult);
                }
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
    }
}
