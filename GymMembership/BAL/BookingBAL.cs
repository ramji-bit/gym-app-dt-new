﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GymMembership.DAL;
using GymMembership.DTO;
using GymMembership.MemberInfo;
using System.Windows.Forms;

namespace GymMembership.BAL
{
    public class BookingBAL
    {
        BookingDAL _bookingDAL = new BookingDAL();
        MemberDetailsDAL _memberDetailsDal = new MemberDetailsDAL();


        public DataSet get_BookingMembersDetails(string MemberLastName)
        {
            try
            {
                return _bookingDAL.get_BookingMembersDetails(MemberLastName);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public int addBooking(BookingTransDTO _bookingDTO)
        {
            try
            {
                return _bookingDAL.addBooking(_bookingDTO);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public DataSet getBooking(int _CompanyID, int _SiteID, int _ResourceTypeID, DateTime _Date, int _SlotID = 0, int _FacilityID = 0)
        { 
            try
            {
                return _bookingDAL.getBooking(_CompanyID, _SiteID, _ResourceTypeID, _Date, _SlotID, _FacilityID);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataSet get_BookingResourceTrainer()
        {
            try
            {
                return _bookingDAL.get_BookingResourceTrainer();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet get_Facilities()
        {
            try
            {
                return _bookingDAL.get_Facilities();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet getFacilitySlots(int _facilityID)
        {
            try
            {
                return _bookingDAL.getFacilitySlots(_facilityID);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet get_ResourceTypes()
        {
            try
            {
                return _bookingDAL.get_ResourceTypes();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
