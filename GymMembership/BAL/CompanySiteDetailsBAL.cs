﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using GymMembership.DAL;

namespace GymMembership.BAL
{
    class CompanySiteDetailsBAL
    {
        CompanySiteDetailsDAL _CSDetailsDAL = new CompanySiteDetailsDAL();
        public DataSet get_OrgCompanyName()
        {
            try
            {
                return _CSDetailsDAL.get_OrgCompanyName();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataSet get_SitebyCompanyName(string _CompanyName)
        {
            try
            {
                return _CSDetailsDAL.get_SitebyCompanyName(_CompanyName);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
