﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GymMembership.Pointofsale;
using System.Data;
using GymMembership.DAL;

namespace GymMembership.BAL
{
    public class PointOfSaleBAL
    {
        PointOfSaleDAL _PointOfSaleDAL = new PointOfSaleDAL();
        public DataSet getPOSMemberDetails(string MemberLastName)
        {
            try
            {
                return _PointOfSaleDAL.getPOSMemberDetails(MemberLastName);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public int SavePOSInvoiceDetails(DataTable addInvoiceDetails)
        {
            try
            {
                return _PointOfSaleDAL.SavePOSInvoiceDetails(addInvoiceDetails);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int raise_POS_Invoice (
                                        DateTime InvoiceDate, int InvoiceNo, decimal InvoiceAmount, decimal AmountPaid, 
                                        decimal OutstandingAmt, int MemberId, string InvoiceStatus, string FieldNameType, 
                                        decimal _Cash = 0, decimal _Card = 0, decimal _Change = 0, 
                                        decimal _InvAmtExcDiscount = 0m, int _discountPercent = 0,
                                        decimal _discountDollar = 0m
                                     )
        {
            try
            {
                return _PointOfSaleDAL.raise_POS_Invoice (
                                                            InvoiceDate, InvoiceNo, InvoiceAmount, AmountPaid, 
                                                            OutstandingAmt, MemberId, InvoiceStatus, FieldNameType, 
                                                            _Cash, _Card, _Change, 
                                                            _InvAmtExcDiscount, _discountPercent, _discountDollar
                                                         );
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
