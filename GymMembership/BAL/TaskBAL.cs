﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GymMembership.DAL;
using GymMembership.DTO;
using GymMembership.MemberInfo;
using System.Windows.Forms;

namespace GymMembership.BAL
{
    public class TaskBAL
    {
        TaskDAL _taskDAL = new TaskDAL();

        public DataSet get_TaskByMemberId(int MemberId,int TaskId)
        {
            try
            {
                return _taskDAL.get_TaskByMemberId(MemberId,TaskId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet get_TaskByProspectId(int MemberId, int TaskId)
        {
            try
            {
                return _taskDAL.get_TaskByProspectId(MemberId, TaskId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet get_TasKType(int TaskTypeId)
        {
            try
            {
                return _taskDAL.get_TasKType(TaskTypeId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public int add_TaskToStaff(int MemberId, int TaskType, string TaskDecription, DateTime TaskAssgnDt, int StaffAssgnTo, int TasKStatus)
        {
            try
            {
                return _taskDAL.add_TaskToStaff(MemberId, TaskType, TaskDecription, TaskAssgnDt, StaffAssgnTo, TasKStatus);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }
        public int add_ProspectTaskToStaff(int ProspectID, int TaskType, string TaskDecription, DateTime TaskAssgnDt, int StaffAssgnTo, int TasKStatus)
        {
            try
            {
                return _taskDAL.add_ProspectTaskToStaff(ProspectID, TaskType, TaskDecription, TaskAssgnDt, StaffAssgnTo, TasKStatus);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }
        }
        public DataSet get_TaskByStaffId(int TaskId, int StaffId)
        {
            try
            {
                return _taskDAL.get_TaskByStaffId(TaskId, StaffId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public int add_StaffTask(int TaskId, int MemberId, int StaffId, int TaskStaus, DateTime TaskExpireDate, string StaffActionTaken)
        {
            try
            {
                return _taskDAL.add_StaffTask(TaskId, MemberId, StaffId, TaskStaus, TaskExpireDate, StaffActionTaken);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int add_ProspectStaffTask(int TaskId, int MemberId, int StaffId, int TaskStaus, DateTime TaskExpireDate, string StaffActionTaken)
        {
            try
            {
                return _taskDAL.add_ProspectStaffTask(TaskId, MemberId, StaffId, TaskStaus, TaskExpireDate, StaffActionTaken);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int change_StaffTask(int TaskId, int ChangedStaffId)
        {
            try
            {
                return _taskDAL.change_StaffTask(TaskId, ChangedStaffId);
            }
            catch (Exception)
            {
                return 0;
            }


        }

        public DataSet get_TaskHistoryByMember(int StaffId, int MemberId)
        {
            try
            {
                return _taskDAL.get_TaskHistoryByMember(StaffId, MemberId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet get_TaskHistoryByProspect(int StaffId, int MemberId, int _TaskID)
        {
            try
            {
                return _taskDAL.get_TaskHistoryByProspect(StaffId, MemberId, _TaskID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public DataSet get_StaffIdByUserName(string UserName)
        {
            try
            {
                return _taskDAL.get_StaffIdByUserName(UserName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
    }
}
