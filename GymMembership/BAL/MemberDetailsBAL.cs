﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GymMembership.DAL;
using GymMembership.DTO;
using GymMembership.MemberInfo;
using System.Windows.Forms;

namespace GymMembership.BAL
{
    public class MemberDetailsBAL
    {
        MemberDetailsDAL _memberDetailsDal = new MemberDetailsDAL();

        public int add_MemberDetails(MemberDetailsDTO memberDetailsDTO, int _Update = 0)
        {
            try
            {
                return _memberDetailsDal.add_MemberDetails(memberDetailsDTO, _Update);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public DataSet get_AllActiveStaff()
        {
            try
            {
                return _memberDetailsDal.get_AllActiveStaff();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataSet get_PersonalTrainer()
        {
            try
            {
                return _memberDetailsDal.get_PersonalTrainer();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataSet get_PersonalTrainer_TaskFilter(int _Flag = 0)
        {
            try
            {
                return _memberDetailsDal.get_PersonalTrainer_TaskFilter(_Flag);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataSet CheckIfDuplicateCardNo(int _CompanyID, int _SiteID, string _CardNo, string _MemberNo)
        {
            try
            {
                return _memberDetailsDal.CheckIfDuplicateCardNo(_CompanyID, _SiteID, _CardNo, _MemberNo);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataSet get_InvolvementType()
        {
            try
            {
                return _memberDetailsDal.get_InvolvementType();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet get_ProgramGroup()
        {
            try
            {
                return _memberDetailsDal.get_ProgramGroup();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataSet get_AccessTypes()
        {
            try
            {
                return _memberDetailsDal.get_AccessTypes();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataSet get_ALLProgramGroup()
        {
            try
            {
                return _memberDetailsDal.get_ALLProgramGroup();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataSet get_Program()
        {
            try
            {
                return _memberDetailsDal.get_Program();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataSet get_ProgramFindMember()
        {
            try
            {
                return _memberDetailsDal.get_ProgramFindMember();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataSet get_ProgramByGroup(int ProgramGroupId, int _Status = 1)
        {
            try
            {
                return _memberDetailsDal.get_ProgramByGroup(ProgramGroupId, _Status);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet get_ProgrammeDetails(int ProgramId)
        {
            try
            {
                return _memberDetailsDal.get_ProgrammeDetails(ProgramId);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet get_CardNo()
        {
            try
            {
                return _memberDetailsDal.get_CardNo();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet get_MemberNo()
        {
            try
            {
                return _memberDetailsDal.get_MemberNo();
            }
            catch (Exception)
            {
                return null;
            }
        }


        public int add_Membership(DataTable addMembership)//,decimal FullCost, string Condition, int CardNo, int MemberNo)
        {
            try
            {
                return _memberDetailsDal.add_Membership(addMembership);//,FullCost, Condition, CardNo, MemberNo);

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        public DataSet get_MembershipOnExistingMember(int MemberId)
        {
            try
            {
                return _memberDetailsDal.get_MembershipOnExistingMember(MemberId);
            }
            catch (Exception)
            {
                return null;
            }
        }


        public int add_MemberExtraInfo(MembersAddlInfoDTO membersAddlInfoDTO)
        {
            try
            {
                return _memberDetailsDal.add_MemberExtraInfo(membersAddlInfoDTO);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public DataSet get_BusinessPromotionDetails()
        {
            try
            {
                return _memberDetailsDal.get_BusinessPromotionDetails();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet get_ExtraDetailsByMemberId(int MemberId)
        {
            try
            {
                return _memberDetailsDal.get_ExtraDetailsByMemberId(MemberId);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataSet get_LastUsedNo(string FieldName)
        {
            try
            {
                return _memberDetailsDal.get_LastUsedNo(FieldName);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataSet get_LastUsedNoByCompanySite(string FieldName, int CompanyID, int SiteID)
        {
            try
            {
                return _memberDetailsDal.get_LastUsedNoForCompanySite(FieldName, CompanyID, SiteID);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataSet get_PaymentType()
        {
            try
            {
                return _memberDetailsDal.get_PaymentType();
            }
            catch (Exception)
            {
                return null;
            }
        }

        //public DataSet get_PaymentDetailsByMemberId(int MemberId)
        //{
        //    try
        //    {
        //        return _memberDetailsDal.get_PaymentDetailsByMemberId(MemberId);
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}
        public DataSet get_PendingInvoiceDetailsByMemberID(int MemberId, char _isProd)
        {
            try
            {
                return _memberDetailsDal.get_PendingInvoiceDetailsByMemberID(MemberId, _isProd);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataSet get_ALLMemberDetails(int _SiteID = 0)
        {
            try
            {
                return _memberDetailsDal.get_ALLMemberDetails(_SiteID);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int add_MembershipPayments(DateTime PaymentDate, int PaymentMode, decimal PaidAmount, string PaymentNotes, string PaymentInvoiceNo, int InvoiceDetailsID)
        {
            try
            {
                return _memberDetailsDal.add_MembershipPayments(PaymentDate, PaymentMode, PaidAmount, PaymentNotes, PaymentInvoiceNo, InvoiceDetailsID);
            }
            catch (Exception)
            {
                return 0;
            }
        }


        public DataSet get_InvoiceNoByMemberId(int MemberId)
        {
            try
            {
                return _memberDetailsDal.get_InvoiceNoByMemberId(MemberId);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int raise_Invoice(DateTime InvoiceDate, int InvoiceNo, decimal InvoiceAmount, decimal AmountPaid, decimal OutstandingAmt, int MemberId, string InvoiceStatus, string FieldNameType)
        {
            try
            {
                return _memberDetailsDal.raise_Invoice(InvoiceDate, InvoiceNo, InvoiceAmount, AmountPaid, OutstandingAmt, MemberId, InvoiceStatus, FieldNameType);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public DataSet get_AllMemberDetailsAndAlsoByMemberId(int MemberId)
        {
            try
            {
                return _memberDetailsDal.get_AllMemberDetailsAndAlsoByMemberId(MemberId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public int AddInvoiceDetails(DataTable addInvoiceDetails)
        {
            try
            {
                return _memberDetailsDal.AddInvoiceDetails(addInvoiceDetails);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int AddMemberHold
            (
                int HoldId, DateTime StartDate, DateTime EndDate, string Reason, int FeeType, int MemberId, 
                int CompanyId, bool EndHoldMember, bool ProrataMember, int Status, Decimal FeeAmount, int ProgramId, bool IsFreeTime, int MSUniqueID = 0
            )
        {
            try
            {
                return _memberDetailsDal.AddMemberHold(HoldId, StartDate, EndDate, Reason, FeeType, MemberId, CompanyId, EndHoldMember, ProrataMember, Status, FeeAmount, ProgramId, IsFreeTime, MSUniqueID);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public int MemberHoldImmediate(int _CompanyID, int _SiteID, int _MemberNo, int _ProgramID, int _Immediate = 0)
        {
            try
            {
                return _memberDetailsDal.MemberHoldImmediate(_CompanyID, _SiteID, _MemberNo, _ProgramID, _Immediate);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public DataSet get_MemberHoldDetails(int MemberId, int CompanyId, int ProgramID = 0, int MSUniqueID = 0)
        {
            try
            {
                return _memberDetailsDal.get_MemberHoldDetails(MemberId, CompanyId, ProgramID, MSUniqueID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        public int delete_MemberHoldDetails(int HoldId, int CompanyId, int HoldStatus)
        {
            try
            {
                return _memberDetailsDal.delete_MemberHoldDetails(HoldId, CompanyId, HoldStatus);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int add_RemoveMemberHoldDays(int NoOfDays, int HoldId, int CompanyId)
        {
            try
            {
                return _memberDetailsDal.add_RemoveMemberHoldDays(NoOfDays, HoldId, CompanyId);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public DataSet get_FreeTimeDetails(int HoldId)
        {
            try
            {
                return _memberDetailsDal.get_FreeTimeDetails(HoldId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }
        public DataSet get_ActiveMembership(int MemberId, int CompanyId, int SiteId)
        {
            try
            {
                return _memberDetailsDal.Get_ActiveMembership(MemberId, CompanyId, SiteId);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int Update_usedVisits(int MemberId, int CompanyId, int SiteId, int UsedVisits, int ProgrammeId)
        {
            try
            {
                return _memberDetailsDal.Update_UsedVisits(MemberId, CompanyId, SiteId, UsedVisits, ProgrammeId);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public DataSet get_VisitsCount(int MemberId, int CompanyId, int SiteId, int ProgrammeId)
        {
            try
            {
                return _memberDetailsDal.Get_VisitsCount(MemberId, CompanyId, SiteId, ProgrammeId);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet SaveCheckInDetails(int MemberId, int CompanyId, int SiteId, int ProgrammeId, DateTime CheckInDate, string _Door)
        {
            try
            {
                return _memberDetailsDal.SaveCheckInDetails(MemberId, CompanyId, SiteId, ProgrammeId, CheckInDate, _Door);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet GetCheckInDetails(int MemberId, int CompanyId, int SiteId, int ProgrammeId)
        {
            try
            {
                return _memberDetailsDal.GetCheckInDetails(MemberId, CompanyId, SiteId, ProgrammeId);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int SaveUpdateProspects_SaveCheckIn(ProspectsDTO _ProspectDTO)
        {
            try
            {
                return _memberDetailsDal.SaveUpdateProspects_SaveCheckIn(_ProspectDTO);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public int SaveUpdateProspects_SaveOnly(ProspectsDTO _ProspectDTO)
        {
            try
            {
                return _memberDetailsDal.SaveUpdateProspects_SaveOnly(_ProspectDTO);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public DataSet GetNotesForMember(int _CompanyID, int _SiteID, int _MemberNo)
        {
            try
            {
                return _memberDetailsDal.GetNotesForMember(_CompanyID, _SiteID, _MemberNo);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataSet GetFormsForMember(int _CompanyID, int _SiteID, int _MemberNo)
        {
            try
            {
                return _memberDetailsDal.GetFormsForMember(_CompanyID, _SiteID, _MemberNo);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int SaveFormsForMember(int _CompanyID, int _SiteID, int _MemberID, DataTable _dtForms)
        {
            try
            {
                return _memberDetailsDal.SaveFormsForMember(_CompanyID, _SiteID, _MemberID, _dtForms);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int RemoveFormsForMember(int _CompanyID, int _SiteID, int _MemberID)
        {
            try
            {
                return _memberDetailsDal.RemoveFormsForMember(_CompanyID, _SiteID, _MemberID);
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}