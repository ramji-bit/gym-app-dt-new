﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GymMembership.Pointofsale
{
    /// <summary>
    /// Interaction logic for AddSale.xaml
    /// </summary>
    public partial class AddSale 
    {
        public AddSale()
        {
            InitializeComponent();
        }

        private void txt_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private static bool IsTextAllowed_UnitPrice(string text)
        {
            Regex regex = new Regex("[^0-9.]"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtQuantity.Text = "1";
            //txtQuantity.Focus();
            txtUnitPrice.Focus();
        }

        private void MetroWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) || (e.Key == Key.Return))
            {
                btnSave_Click(sender, e);
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //VALIDATE -> set .Owner values & quit
                if (txtQuantity.Text == "")
                {
                    Console.Beep(4000, 100);
                    return;
                }
                else if ( Convert.ToInt32(txtQuantity.Text) > 0 )
                {
                    if (txtUnitPrice.Text.Trim() == "")
                    {
                        Console.Beep(4000, 100);
                        return;
                    }
                    else if ( Convert.ToDecimal(txtUnitPrice.Text) > 0 )
                    {
                        (this.Owner as MakeaSale)._isAddSale = true;
                        (this.Owner as MakeaSale)._addSaleQty = Convert.ToInt32(txtQuantity.Text);
                        (this.Owner as MakeaSale)._addSaleUnitPrice = Convert.ToDecimal(txtUnitPrice.Text);
                        (this.Owner as MakeaSale)._addSaleTotPrice = Convert.ToDecimal(txtQuantity.Text) * Convert.ToDecimal(txtUnitPrice.Text);

                        this.Close();
                    }
                    else
                    {
                        Console.Beep(4000, 100);
                        return;
                    }
                }
                else
                {
                    Console.Beep(4000, 100);
                    return;
                }
            }
            catch(Exception)
            {

            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            //set .Owner values to null & quit
            (this.Owner as MakeaSale)._isAddSale = false;
            (this.Owner as MakeaSale)._addSaleQty = 0;
            (this.Owner as MakeaSale)._addSaleUnitPrice = 0m;
            (this.Owner as MakeaSale)._addSaleTotPrice = 0m;

            this.Close();
        }

        private void txtUnitPrice_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            bool _isAllowedDec = false;
            _isAllowedDec = !IsTextAllowed_UnitPrice(e.Text);
            if (!_isAllowedDec)
            {
                if (e.Text == "." && txtUnitPrice.Text.Contains("."))
                {
                    Console.Beep(4000, 100);
                    _isAllowedDec = true;
                }
                else if (txtUnitPrice.Text.Contains("."))
                {
                    if (txtUnitPrice.Text.Substring(txtUnitPrice.Text.IndexOf(".") + 1).Length >= 2)
                    {
                        Console.Beep(4000, 100);
                        _isAllowedDec = true;
                    }
                }
                else
                {
                    if ((txtUnitPrice.Text.Length >= 3) && (e.Text != "."))
                    {
                        Console.Beep(4000, 100);
                        _isAllowedDec = true;
                    }
                }
            }
            e.Handled = _isAllowedDec;
        }

        private void btnMinus_Click(object sender, RoutedEventArgs e)
        {
            if (txtQuantity.Text != "")
            {
                int _Qty = Convert.ToInt32(txtQuantity.Text);
                _Qty--;
                if (_Qty <= 0)
                {
                    Console.Beep(4000, 100);
                    txtQuantity.Text = "1";
                }
                else
                {
                    Console.Beep(4000, 100);
                    txtQuantity.Text = _Qty.ToString();
                }
            }
            else
            {
                Console.Beep(4000, 100);
                txtQuantity.Text = "1";
            }
        }

        private void btnPlus_Click(object sender, RoutedEventArgs e)
        {
            if (txtQuantity.Text != "")
            {
                int _Qty = Convert.ToInt32(txtQuantity.Text);
                _Qty++;
                if (_Qty > 999)
                {
                    Console.Beep(4000, 100);
                    txtQuantity.Text = "999";
                }
                else
                {
                    Console.Beep(4000, 100);
                    txtQuantity.Text = _Qty.ToString();
                }
            }
            else
            {
                Console.Beep(4000, 100);
                txtQuantity.Text = "1";
            }
        }
    }
}
