﻿using System;
using System.Collections;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Configuration;
using System.Windows.Input;
using System.Data;
using Microsoft.VisualBasic;

namespace GymMembership.Pointofsale
{
    /// <summary>
    /// Interaction logic for DoaStocktake.xaml
    /// </summary>
    public partial class DoaStocktake 
    {
        ClockInCheck _ClockInCheck = new ClockInCheck();
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        DataSet _dsAccessRights = new DataSet();
        DataSet _dsStock = new DataSet();
        static int _StockDateTable = 0;
        static int _StockDetailsTable = 1;
        bool _windowFullyLoaded = false;
        public DoaStocktake()
        {
            InitializeComponent();
        }

        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto(_StaffPic);
        }

        private void btnDoaStocktakeClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void SetAccessRightsDS(DataSet dS)
        {
            _dsAccessRights = dS;
        }


        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnPOS_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            POS _POS = new POS();
            _POS.Owner = this;
            _POS.SetAccessRightsDS(_dsAccessRights);
            this.Cursor = Cursors.Arrow;
            _POS._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _POS._CompanyName = this._CompanyName;
            _POS._SiteName = this._SiteName;
            _POS._UserName = this._UserName;
            _POS._StaffPic = this._StaffPic;
            _POS.StaffImage.Source = this.StaffImage.Source;
            _POS.Owner = this.Owner;
            this.Close();
            _POS.ShowDialog();
        }

        private void btnAddNewStock_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            AddNewStock _AddNewStock = new AddNewStock();
            _AddNewStock.Owner = this;
            _AddNewStock.SetAccessRightsDS(_dsAccessRights);
            this.Cursor = Cursors.Arrow;
            _AddNewStock._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _AddNewStock._CompanyName = this._CompanyName;
            _AddNewStock._SiteName = this._SiteName;
            _AddNewStock._UserName = this._UserName;
            _AddNewStock._StaffPic = this._StaffPic;
            _AddNewStock.StaffImage.Source = this.StaffImage.Source;
            _AddNewStock.Owner = this.Owner;
            this.Close();
            _AddNewStock.ShowDialog();
        }

        private void btnPOSMakeASale_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _ClockInCheck = Comman.Constants.POS_CheckClockIn(_ClockInCheck);

                if (!_ClockInCheck._notClockedIn)
                {
                    if (_ClockInCheck._clockedInNotClockedOut)
                    {
                        //GAS - 27 POS Home Club : 19/SEP/2017
                        if (ConfigurationManager.AppSettings["SuperAdmin"].ToString().ToUpper().Trim() != "YES")
                        {
                            if
                            (
                                ConfigurationManager.AppSettings["LoggedInSiteID"].ToString().ToUpper().Trim() !=
                                ConfigurationManager.AppSettings["HomeClubSiteID"].ToString().ToUpper().Trim()
                            )
                            {
                                MessageBoxResult _HomeClubResult =
                                MessageBox.Show
                                    (
                                        "Your selected site is different to the Home site!" +
                                        "\nWould you like to still continue with POS transaction - Yes or No?" +
                                        "\n\n(CLICK 'YES' TO PROCEED, 'NO' TO EXIT!)",
                                        this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No
                                    );
                                if (
                                        _HomeClubResult == MessageBoxResult.No || _HomeClubResult == MessageBoxResult.Cancel
                                   )
                                {
                                    return;
                                }
                            }
                        }
                        //GAS - 27 POS Home Club : 19/SEP/2017

                        this.Cursor = Cursors.Wait;
                        MakeaSale makeaSale = new MakeaSale();
                        makeaSale.Owner = this;
                        makeaSale.SetAccessRightsDS(_dsAccessRights);
                        this.Cursor = Cursors.Arrow;
                        makeaSale._CurrentLoginDateTime = this._CurrentLoginDateTime;
                        makeaSale._CompanyName = _CompanyName;
                        makeaSale._SiteName = this._SiteName;
                        makeaSale._UserName = this._UserName;
                        makeaSale._StaffPic = this._StaffPic;
                        makeaSale.StaffImage.Source = this.StaffImage.Source;
                        makeaSale.ShowDialog();
                    }
                    else if (_ClockInCheck._clockedInAndClockedOut)
                    {
                        MessageBox.Show("End-Of-Shift has been completed for Today!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }

                }
                else
                {
                    decimal _clockInBalance = -1;
                    string _strClockInBalance = null;

                    _strClockInBalance = ConfigurationManager.AppSettings["POSOpeningBalance"].ToString();
                    if (_strClockInBalance == "")
                        _strClockInBalance = "0.00";

                    //_strClockInBalance = Interaction.InputBox("Enter Opening Cash Balance : ", "POS Clock-In");
                    //if ((_strClockInBalance == "") || (_strClockInBalance == null))
                    //{
                    //    MessageBox.Show("No Amount entered!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //    return;
                    //}
                    //if (!Regex.IsMatch(_strClockInBalance, @"^\d{1,7}(\.\d{1,2})?$"))
                    //{
                    //    MessageBox.Show("Amount should be Numeric! (upto 2 decimal places)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //    return;
                    //}

                    _clockInBalance = Convert.ToDecimal(_strClockInBalance);
                    if (_clockInBalance < 0)
                    {
                        MessageBox.Show("Amount CANNOT be less than Zero!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }

                    int _result = Comman.Constants.POS_ClockIn_User(_clockInBalance);

                    if (_result > 0)
                    {
                        //GAS - 27 POS Home Club : 19/SEP/2017
                        if (ConfigurationManager.AppSettings["SuperAdmin"].ToString().ToUpper().Trim() != "YES")
                        {
                            if
                            (
                                ConfigurationManager.AppSettings["LoggedInSiteID"].ToString().ToUpper().Trim() !=
                                ConfigurationManager.AppSettings["HomeClubSiteID"].ToString().ToUpper().Trim()
                            )
                            {
                                MessageBoxResult _HomeClubResult =
                                MessageBox.Show
                                    (
                                        "Your selected site is different to the Home site!" +
                                        "\nWould you like to still continue with POS transaction - Yes or No?" +
                                        "\n\n(CLICK 'YES' TO PROCEED, 'NO' TO EXIT!)",
                                        this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No
                                    );
                                if (
                                        _HomeClubResult == MessageBoxResult.No || _HomeClubResult == MessageBoxResult.Cancel
                                   )
                                {
                                    return;
                                }
                            }
                        }
                        //GAS - 27 POS Home Club : 19/SEP/2017

                        this.Cursor = Cursors.Wait;
                        MakeaSale makeaSale = new MakeaSale();
                        makeaSale.Owner = this;
                        makeaSale.SetAccessRightsDS(_dsAccessRights);
                        this.Cursor = Cursors.Arrow;
                        makeaSale._CurrentLoginDateTime = this._CurrentLoginDateTime;
                        makeaSale._CompanyName = _CompanyName;
                        makeaSale._SiteName = this._SiteName;
                        makeaSale._UserName = this._UserName;
                        makeaSale._StaffPic = this._StaffPic;
                        makeaSale.StaffImage.Source = this.StaffImage.Source;
                        makeaSale.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Error in Staff Clock-In!", this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }
        
        private void btnViewInventory_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            ViewInventory vI = new ViewInventory();
            vI.Owner = this;
            vI.SetAccessRightsDS(_dsAccessRights);
            this.Cursor = Cursors.Arrow;
            vI._CurrentLoginDateTime = this._CurrentLoginDateTime;
            vI._CompanyName = this._CompanyName;
            vI._SiteName = this._SiteName;
            vI._UserName = this._UserName;
            vI._StaffPic = this._StaffPic;
            vI.StaffImage.Source = this.StaffImage.Source;
            vI.Owner = this.Owner;
            this.Close();
            vI.ShowDialog();
        }

        private void btnDoaStocktake_Click(object sender, RoutedEventArgs e)
        {
            //SAME WINDOW; NO ACTION;
        }

        private void DoaStocktake_Loaded(object sender, RoutedEventArgs e)
        {
            SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
            GetStockAddedDates();
            btnDoaStocktakeSave.IsEnabled = false;
            _windowFullyLoaded = true;
        }
        private void GetStockAddedDates(string _Year = "", string _Month = "")
        {
            try
            {
                gvDoaStocktakeDates.ItemsSource = null;
                gvDoaStocktakeDates.Items.Clear();

                gvDoaStocktakeReason.ItemsSource = null;
                gvDoaStocktakeReason.Items.Clear();

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return;

                Hashtable ht = new Hashtable();

                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);

                if (_Year.Trim() != "")
                    ht.Add("@YearFilter", _Year);
                if (_Month.Trim() != "")
                    ht.Add("@MonthFilter", _Month);

                _dsStock = new DataSet();
                _dsStock = sql.ExecuteProcedure("GetStockAddedDetails", ht);

                if (_dsStock != null)
                {
                    if (_dsStock.Tables.Count > 0)
                    {
                        if (_dsStock.Tables[_StockDateTable].DefaultView.Count > 0)
                        {
                            gvDoaStocktakeDates.ItemsSource = _dsStock.Tables[_StockDateTable].DefaultView;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                string _msg = ex.Message;
            }
        }
        private void btnDoaStocktakeEdit_Click(object sender, RoutedEventArgs e)
        {
            if (gvDoaStocktakeDates.SelectedIndex != -1)
            {
                gvDoaStocktakeReason.IsReadOnly = false;
                gvDoaStocktakeReason.CanUserAddRows = false;
                gvDoaStocktakeReason.Background = System.Windows.Media.Brushes.Wheat;
                if (gvDoaStocktakeReason.Items.Count > 0)
                {
                    gvDoaStocktakeReason.SelectedIndex = 0;
                }
                btnDoaStocktakeSave.IsEnabled = true;
            }
            else
            {
                gvDoaStocktakeReason.IsReadOnly = true;
                gvDoaStocktakeReason.CanUserAddRows = false;
                gvDoaStocktakeReason.Background = System.Windows.Media.Brushes.White;
                MessageBox.Show("Select a Stock-Added-Date to View / Edit details!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                btnDoaStocktakeSave.IsEnabled = false;
            }
        }

        private void gvDoaStocktakeDates_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (gvDoaStocktakeDates.SelectedIndex != -1)
            {
                gvDoaStocktakeReason.ItemsSource = null;

                string _uStockTranID = (gvDoaStocktakeDates.SelectedItem as DataRowView).Row["uStockTranID"].ToString();
                if (_dsStock.Tables.Count > 1)
                {
                    _dsStock.Tables[_StockDetailsTable].DefaultView.RowFilter = "";
                    _dsStock.Tables[_StockDetailsTable].DefaultView.RowFilter = "uStockTranID = '" + _uStockTranID + "'";
                    gvDoaStocktakeReason.ItemsSource = _dsStock.Tables[_StockDetailsTable].DefaultView;
                }
                gvDoaStocktakeReason.IsReadOnly = true;
                gvDoaStocktakeReason.CanUserAddRows = false;
                gvDoaStocktakeReason.Background = System.Windows.Media.Brushes.White;
                btnDoaStocktakeSave.IsEnabled = false;
            }
        }

        private void chkCurrentMonth_Checked(object sender, RoutedEventArgs e)
        {
            if (_windowFullyLoaded)
            {
                GetStockAddedDates("", "");
                btnDoaStocktakeSave.IsEnabled = false;
            }
        }

        private void chkCurrentMonth_Unchecked(object sender, RoutedEventArgs e)
        {
            if (_windowFullyLoaded)
            {
                GetStockAddedDates("1900", "01");
                btnDoaStocktakeSave.IsEnabled = false;
            }
        }
        
        private void btnDoaStocktakeSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SqlHelper _sql = new SqlHelper();
                if (_sql._ConnOpen == 0)
                    return;

                if (_dsStock.Tables[_StockDetailsTable].DefaultView.Count > 0)
                {
                    Hashtable ht = new Hashtable();
                    int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                    string _Staff = ConfigurationManager.AppSettings["LoggedInUser"].ToString();
                    bool _isAtleastOneItemUpdated = false;
                    //FOR EACH ROW; IF DATA IS CHANGED, UPDATE IN STOCK & INVENTORY TABLES
                    for (int i = 0; i < _dsStock.Tables[_StockDetailsTable].DefaultView.Count; i++)
                    {
                        ht.Clear();

                        DataRowView _dV = (_dsStock.Tables[_StockDetailsTable].DefaultView[i] as DataRowView);
                        string _uStockTranID = _dV["uStockTranID"].ToString();
                        int _ProductTypeID = Convert.ToInt32(_dV["ProductTypeID"].ToString());
                        int _ProductID = Convert.ToInt32(_dV["ProductID"].ToString());
                        int _newQty = Convert.ToInt32(_dV["QuantityAdded"].ToString());
                        int _oldQty = Convert.ToInt32(_dV["OldQuantity"].ToString());  

                        ht.Add("@CompanyID", _CompanyID);
                        ht.Add("@SiteID", _SiteID);
                        ht.Add("@uStockTranID", _uStockTranID);
                        ht.Add("@Staff", _Staff);
                        ht.Add("@ProductTypeID", _ProductTypeID);
                        ht.Add("@ProductID", _ProductID);
                        ht.Add("@NewQuantity", _newQty);
                        ht.Add("@OldQuantity", _oldQty);

                        _sql.ExecuteQuery("Update_Edit_Product_Stock", ht);
                        if (_newQty != _oldQty)
                        {
                            _isAtleastOneItemUpdated = true;
                        }
                    }
                    if (_isAtleastOneItemUpdated)
                    {
                        MessageBox.Show("Stock Details Updated !!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        if (chkCurrentMonth.IsChecked == true)
                        {
                            GetStockAddedDates("", "");
                        }
                        else
                        {
                            GetStockAddedDates("1900", "01");
                        }
                        btnDoaStocktakeSave.IsEnabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string _exMsg = ex.Message;
            }
        }
    }
}
