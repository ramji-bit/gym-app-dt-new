﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GymMembership.Pointofsale
{
    /// <summary>
    /// Interaction logic for Discount.xaml
    /// </summary>
    public partial class Discount 
    {
        public Discount()
        {
            InitializeComponent();
        }

        private void txt_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            bool _isAllowed = false;
            //e.Handled = !IsTextAllowed(e.Text);
            _isAllowed = !IsTextAllowed(e.Text);
            if (!_isAllowed)
            {
                if (txtDiscountPercent.Text != "")
                {
                    if ((Convert.ToInt32(txtDiscountPercent.Text + e.Text) > 100) || (Convert.ToInt32(txtDiscountPercent.Text + e.Text) < 0))
                    {
                        Console.Beep(4000, 100);
                        _isAllowed = true;
                    }
                }
            }
            e.Handled = _isAllowed;
        }

        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtDiscountPercent.Focus();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (txtDiscountPercent.Text.Trim() == "")
            {
                //MessageBox.Show("Enter Discount %", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                Console.Beep(4000, 500);
                txtDiscountPercent.Focus();
                return;
            }
            
            if ((Convert.ToInt32(txtDiscountPercent.Text) > 100) || (Convert.ToInt32(txtDiscountPercent.Text) < 0))
            {
                //MessageBox.Show("Discount % should be between < 0 > AND < 100 >", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                Console.Beep(4000, 100);
                txtDiscountPercent.Focus();
                return;
            }

            if (Convert.ToInt32(txtDiscountPercent.Text) == 0)
            {
                (this.Owner as MakeaSale)._discountPercent = 0;
                (this.Owner as MakeaSale)._isDiscountApplied = false;
            }
            else
            {
                (this.Owner as MakeaSale)._discountPercent = Convert.ToInt32(txtDiscountPercent.Text);
                (this.Owner as MakeaSale)._isDiscountApplied = true;
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            (this.Owner as MakeaSale)._discountPercent = 0;
            (this.Owner as MakeaSale)._isDiscountApplied = false;
            this.Close();
        }

        private void MetroWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) || (e.Key == Key.Return))
            {
                btnSave_Click(sender, e);
            }
        }
    }
}
