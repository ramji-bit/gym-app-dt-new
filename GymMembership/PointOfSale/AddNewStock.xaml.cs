﻿using System;
using System.Collections;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Configuration;
using System.Windows.Input;
using Microsoft.VisualBasic;
using System.Data;

using GymMembership.Comman;

namespace GymMembership.Pointofsale
{
    /// <summary>
    /// Interaction logic for AddNewStock.xaml
    /// </summary>
    public partial class AddNewStock 
    {
        ClockInCheck _ClockInCheck = new ClockInCheck();
        DataSet ds = new DataSet();
        DataSet _dsAccessRights = new DataSet();
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;

        public AddNewStock()
        {
            InitializeComponent();
        }

        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto(_StaffPic);
        }


        private void btnAddNewStockClose_Click(object sender, RoutedEventArgs e)
        {
            //if (MessageBox.Show("Edit in Progress.. Do you want to Quit?",this.Title,MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            //{
                this.Close();
            //}
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);

            cmbAddNewStockFilterbyYear.Items.Add("Filter By Year");
            cmbAddNewStockFilterbyYear.Items.Add(Convert.ToString(DateTime.Today.Year));
            cmbAddNewStockFilterbyYear.Items.Add(Convert.ToString(DateTime.Today.Year - 1));
            cmbAddNewStockFilterbyYear.Items.Add(Convert.ToString(DateTime.Today.Year - 2));
            cmbAddNewStockFilterbyYear.Items.Add(Convert.ToString(DateTime.Today.Year - 3));
            cmbAddNewStockFilterbyYear.SelectedIndex = 0;

            cmbAddNewStockFilterbyMonth.Items.Add("Filter By Month");
            cmbAddNewStockFilterbyMonth.Items.Add("January");
            cmbAddNewStockFilterbyMonth.Items.Add("February"); cmbAddNewStockFilterbyMonth.Items.Add("March");
            cmbAddNewStockFilterbyMonth.Items.Add("April"); cmbAddNewStockFilterbyMonth.Items.Add("May");
            cmbAddNewStockFilterbyMonth.Items.Add("June"); cmbAddNewStockFilterbyMonth.Items.Add("July");
            cmbAddNewStockFilterbyMonth.Items.Add("August"); cmbAddNewStockFilterbyMonth.Items.Add("September");
            cmbAddNewStockFilterbyMonth.Items.Add("October"); cmbAddNewStockFilterbyMonth.Items.Add("November");
            cmbAddNewStockFilterbyMonth.Items.Add("December");
            cmbAddNewStockFilterbyMonth.SelectedIndex = 0;

            cmbAddNewStockFilterbySupplier.Items.Add("Filter by Supplier");
            cmbAddNewStockFilterbySupplier.SelectedIndex = 0;

            ClearAndLoad();
        }
        private void LoadSuppliers()
        {
            //int _CompanyID =  Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            //int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            Utilities _Util = new Utilities();
            _Util.populateSupplierName(cmbAddNewStockSupplier);
        }
        public void SetAccessRightsDS(DataSet dS)
        {
            _dsAccessRights = dS;
        }


        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //private void btnPOSMakeASale_Click(object sender, RoutedEventArgs e)
        //{

        //}

        //private void btnAddNewStock_Click(object sender, RoutedEventArgs e)
        //{

        //}

        //private void btnViewInventory_Click(object sender, RoutedEventArgs e)
        //{

        //}

        //private void btnDoaStocktake_Click(object sender, RoutedEventArgs e)
        //{

        //}

        private void btnPOS_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            POS _POS = new POS();
            _POS.Owner = this;
            _POS.SetAccessRightsDS(_dsAccessRights);
            this.Cursor = Cursors.Arrow;
            _POS._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _POS._CompanyName = this._CompanyName;
            _POS._SiteName = this._SiteName;
            _POS._UserName = this._UserName;
            _POS._StaffPic = this._StaffPic;
            _POS.StaffImage.Source = this.StaffImage.Source;
            _POS.Owner = this.Owner;
            this.Close();
            _POS.ShowDialog();
        }


        private void btnAddNewStock_Click(object sender, RoutedEventArgs e)
        {
            //SAME WINDOW; NO ACTION;
        }

        private void btnPOSMakeASale_Click(object sender, RoutedEventArgs e)
        {
            //this.Cursor = Cursors.Wait;
            //MakeaSale makeaSale = new MakeaSale();
            //makeaSale.Owner = this;
            //makeaSale.SetAccessRightsDS(_dsAccessRights);
            //this.Cursor = Cursors.Arrow;
            //makeaSale._CurrentLoginDateTime = this._CurrentLoginDateTime;
            //makeaSale._CompanyName = this._CompanyName;
            //makeaSale._SiteName = this._SiteName;
            //makeaSale._UserName = this._UserName;
            //makeaSale._StaffPic = this._StaffPic;
            //makeaSale.StaffImage.Source = this.StaffImage.Source;
            //makeaSale.Owner = this.Owner;
            //this.Close();
            //makeaSale.ShowDialog();
            try
            {
                _ClockInCheck = Comman.Constants.POS_CheckClockIn(_ClockInCheck);

                if (!_ClockInCheck._notClockedIn)
                {
                    if (_ClockInCheck._clockedInNotClockedOut)
                    {
                        //GAS - 27 POS Home Club : 19/SEP/2017
                        if (ConfigurationManager.AppSettings["SuperAdmin"].ToString().ToUpper().Trim() != "YES")
                        {
                            if
                            (
                                ConfigurationManager.AppSettings["LoggedInSiteID"].ToString().ToUpper().Trim() !=
                                ConfigurationManager.AppSettings["HomeClubSiteID"].ToString().ToUpper().Trim()
                            )
                            {
                                MessageBoxResult _HomeClubResult =
                                MessageBox.Show
                                    (
                                        "Your selected site is different to the Home site!" +
                                        "\nWould you like to still continue with POS transaction - Yes or No?" +
                                        "\n\n(CLICK 'YES' TO PROCEED, 'NO' TO EXIT!)",
                                        this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No
                                    );
                                if (
                                        _HomeClubResult == MessageBoxResult.No || _HomeClubResult == MessageBoxResult.Cancel
                                   )
                                {
                                    return;
                                }
                            }
                        }
                        //GAS - 27 POS Home Club : 19/SEP/2017

                        this.Cursor = Cursors.Wait;
                        MakeaSale makeaSale = new MakeaSale();
                        makeaSale.Owner = this;
                        makeaSale.SetAccessRightsDS(_dsAccessRights);
                        this.Cursor = Cursors.Arrow;
                        makeaSale._CurrentLoginDateTime = this._CurrentLoginDateTime;
                        makeaSale._CompanyName = _CompanyName;
                        makeaSale._SiteName = this._SiteName;
                        makeaSale._UserName = this._UserName;
                        makeaSale._StaffPic = this._StaffPic;
                        makeaSale.StaffImage.Source = this.StaffImage.Source;
                        makeaSale.ShowDialog();
                    }
                    else if (_ClockInCheck._clockedInAndClockedOut)
                    {
                        MessageBox.Show("End-Of-Shift has been completed for Today!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }

                }
                else
                {
                    decimal _clockInBalance = -1;
                    string _strClockInBalance = null;

                    _strClockInBalance = ConfigurationManager.AppSettings["POSOpeningBalance"].ToString();
                    if (_strClockInBalance == "")
                        _strClockInBalance = "0.00";

                    //_strClockInBalance = Interaction.InputBox("Enter Opening Cash Balance : ", "POS Clock-In");
                    //if ((_strClockInBalance == "") || (_strClockInBalance == null))
                    //{
                    //    MessageBox.Show("No Amount entered!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //    return;
                    //}
                    //if (!Regex.IsMatch(_strClockInBalance, @"^\d{1,7}(\.\d{1,2})?$"))
                    //{
                    //    MessageBox.Show("Amount should be Numeric! (upto 2 decimal places)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //    return;
                    //}
                    _clockInBalance = Convert.ToDecimal(_strClockInBalance);
                    if (_clockInBalance < 0)
                    {
                        MessageBox.Show("Amount CANNOT be less than Zero!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }

                    int _result = Comman.Constants.POS_ClockIn_User(_clockInBalance);

                    if (_result > 0)
                    {
                        //GAS - 27 POS Home Club : 19/SEP/2017
                        if (ConfigurationManager.AppSettings["SuperAdmin"].ToString().ToUpper().Trim() != "YES")
                        {
                            if
                            (
                                ConfigurationManager.AppSettings["LoggedInSiteID"].ToString().ToUpper().Trim() !=
                                ConfigurationManager.AppSettings["HomeClubSiteID"].ToString().ToUpper().Trim()
                            )
                            {
                                MessageBoxResult _HomeClubResult =
                                MessageBox.Show
                                    (
                                        "Your selected site is different to the Home site!" +
                                        "\nWould you like to still continue with POS transaction - Yes or No?" +
                                        "\n\n(CLICK 'YES' TO PROCEED, 'NO' TO EXIT!)",
                                        this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No
                                    );
                                if (
                                        _HomeClubResult == MessageBoxResult.No || _HomeClubResult == MessageBoxResult.Cancel
                                   )
                                {
                                    return;
                                }
                            }
                        }
                        //GAS - 27 POS Home Club : 19/SEP/2017

                        this.Cursor = Cursors.Wait;
                        MakeaSale makeaSale = new MakeaSale();
                        makeaSale.Owner = this;
                        makeaSale.SetAccessRightsDS(_dsAccessRights);
                        this.Cursor = Cursors.Arrow;
                        makeaSale._CurrentLoginDateTime = this._CurrentLoginDateTime;
                        makeaSale._CompanyName = _CompanyName;
                        makeaSale._SiteName = this._SiteName;
                        makeaSale._UserName = this._UserName;
                        makeaSale._StaffPic = this._StaffPic;
                        makeaSale.StaffImage.Source = this.StaffImage.Source;
                        makeaSale.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Error in Staff Clock-In!", this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }
        
        private void btnViewInventory_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            ViewInventory vI = new ViewInventory();
            vI.Owner = this;
            vI.SetAccessRightsDS(_dsAccessRights);
            this.Cursor = Cursors.Arrow;
            vI._CurrentLoginDateTime = this._CurrentLoginDateTime;
            vI._CompanyName = this._CompanyName;
            vI._SiteName = this._SiteName;
            vI._UserName = this._UserName;
            vI._StaffPic = this._StaffPic;
            vI.StaffImage.Source = this.StaffImage.Source;
            vI.Owner = this.Owner;
            this.Close();
            vI.ShowDialog();
        }

        private void btnDoaStocktake_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            DoaStocktake dStock = new DoaStocktake();
            dStock.Owner = this;
            dStock.SetAccessRightsDS(_dsAccessRights);
            this.Cursor = Cursors.Arrow;
            dStock._CurrentLoginDateTime = this._CurrentLoginDateTime;
            dStock._CompanyName = this._CompanyName;
            dStock._SiteName = this._SiteName;
            dStock._UserName = this._UserName;
            dStock._StaffPic = this._StaffPic;
            dStock.StaffImage.Source = this.StaffImage.Source;
            dStock.Owner = this.Owner;
            this.Close();
            dStock.ShowDialog();
        }

        private void btnAddNewStockAdd_Click(object sender, RoutedEventArgs e)
        {
        }
        private void LoadInventoryDetails()
        {
            int _POS_CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _POS_SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return;

            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _POS_CompanyID);
            ht.Add("@SiteID", _POS_SiteID);

            ds = new DataSet();
            ds = sql.ExecuteProcedure("GetProductInventoryDetails_Stock", ht);
            if (ds != null)
            {
                gvAddNewStockSerial.ItemsSource = null;
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].DefaultView.Count > 0)
                    {
                        gvAddNewStockSerial.ItemsSource = ds.Tables[0].DefaultView;
                    }
                }
            }
        }

        private void btnAddNewStockSave_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateData())
            {
                ds.AcceptChanges();
                DataTable _dt = ds.Tables[0].DefaultView.ToTable();
                _dt.DefaultView.RowFilter = "CurrQuantity > 0";
                DataTable _dtUpdate = _dt.DefaultView.ToTable();

                int _POS_CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _POS_SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                int _POS_SupplierID = Convert.ToInt32(cmbAddNewStockSupplier.SelectedValue);
                string _POS_Invoice_Ref = txtAddNewStockInvoiceNo.Text;

                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return;

                Hashtable ht = new Hashtable();
                string uStockTranID = DateTime.Now.ToString("yyyyMMddhhmmss");

                ht.Add("@CompanyID", _POS_CompanyID);
                ht.Add("@SiteID", _POS_SiteID);
                ht.Add("@SupplierID", _POS_SupplierID);
                ht.Add("@InvoiceRef", _POS_Invoice_Ref);
                ht.Add("@ProductStockTable", _dtUpdate);
                ht.Add("@uStockTranID", uStockTranID);

                DataSet _dsStock = new DataSet();
                _dsStock = sql.ExecuteProcedure("Update_Product_Stock", ht);

                for (int i = 0; i < _dt.DefaultView.Count; i++)
                {
                    if (Convert.ToInt32(_dt.DefaultView[i]["CurrQuantity"].ToString()) > 0)
                    {
                        int _ProductTypeID = Convert.ToInt32(_dt.DefaultView[i]["ProductTypeID"].ToString());
                        int _ProductID = Convert.ToInt32(_dt.DefaultView[i]["ProductID"].ToString());
                        int _Quantity = Convert.ToInt32(_dt.DefaultView[i]["CurrQuantity"].ToString());

                        ht.Clear();

                        ht.Add("@CompanyID", _POS_CompanyID);
                        ht.Add("@SiteID", _POS_SiteID);
                        ht.Add("@ProductTypeID", _ProductTypeID);
                        ht.Add("@ProductID", _ProductID);
                        ht.Add("@Quantity", _Quantity);

                        _dsStock = new DataSet();
                        _dsStock = sql.ExecuteProcedure("Update_Product_Inventory", ht);
                    }
                }

                MessageBox.Show("Stock Details Added!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                ClearAndLoad();
            }
        }
        private void ClearAndLoad()
        {
            txtAddNewStockInvoiceNo.Text = "";
            LoadSuppliers();
            LoadInventoryDetails();

            txtAddNewStockPurachaseAt.IsEnabled = false;
            txtAddNewStockPurachaseAt.Text = DateTime.Now.ToString("dd MMM yyyy hh:mm:ss tt");
            cmbAddNewStockSupplier.SelectedIndex = -1;
        }
        private bool ValidateData()
        {
            if (cmbAddNewStockSupplier.SelectedIndex == -1)
            {
                MessageBox.Show("Select Supplier Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            if (txtAddNewStockInvoiceNo.Text == "")
            {
                MessageBox.Show("Enter Invoice (or) Reference No. / Notes", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            return true;
        }
    }

}
