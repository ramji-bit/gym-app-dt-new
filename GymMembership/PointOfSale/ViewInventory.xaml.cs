﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Data;
using System.Windows.Input;
using System.Configuration;
using System.Collections;
using Microsoft.VisualBasic;

using GymMembership.Comman;
using GymMembership.Reports;

namespace GymMembership.Pointofsale
{
    /// <summary>
    /// Interaction logic for ViewInventory.xaml
    /// </summary>
    public partial class ViewInventory 
    {
        ClockInCheck _ClockInCheck = new ClockInCheck();
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        DataSet _dsAccessRights = new DataSet();
        
        DataSet ds = new DataSet();

        public ViewInventory()
        {
            InitializeComponent();
        }

        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto(_StaffPic);
        }

        private void btnInventoryClose_Click(object sender, RoutedEventArgs e)
        {
                this.Close();
        }
        
        private void cmbInventoryProdType_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (cmbInventoryProdType.SelectedIndex != -1)
            {
                if (cmbInventoryProdType.SelectedIndex == 0)
                {
                    gvInventoryPurchaseDetails.ItemsSource = null;
                    ds.Tables[0].DefaultView.RowFilter = "";
                    gvInventoryPurchaseDetails.ItemsSource = ds.Tables[0].DefaultView;
                }
                else
                {
                    int _PrdTypeID = Convert.ToInt32(cmbInventoryProdType.SelectedValue);

                    gvInventoryPurchaseDetails.ItemsSource = null;
                    ds.Tables[0].DefaultView.RowFilter = "";
                    ds.Tables[0].DefaultView.RowFilter = "ProductTypeID = " + _PrdTypeID;

                    gvInventoryPurchaseDetails.ItemsSource = ds.Tables[0].DefaultView;
                }
            }
        }

        public void SetAccessRightsDS(DataSet dS)
        {
            _dsAccessRights = dS;
        }

        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //private void btnPOSMakeASale_Click(object sender, RoutedEventArgs e)
        //{

        //}

        //private void btnAddNewStock_Click(object sender, RoutedEventArgs e)
        //{

        //}

        //private void btnViewInventory_Click(object sender, RoutedEventArgs e)
        //{

        //}

        //private void btnDoaStocktake_Click(object sender, RoutedEventArgs e)
        //{

        //}

        private void btnPOS_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            POS _POS = new POS();
            _POS.Owner = this;
            _POS.SetAccessRightsDS(_dsAccessRights);
            this.Cursor = Cursors.Arrow;
            _POS._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _POS._CompanyName = this._CompanyName;
            _POS._SiteName = this._SiteName;
            _POS._UserName = this._UserName;
            _POS._StaffPic = this._StaffPic;
            _POS.StaffImage.Source = this.StaffImage.Source;
            _POS.Owner = this.Owner;
            this.Close();
            _POS.ShowDialog();
        }


        private void btnAddNewStock_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            AddNewStock _AddNewStock = new AddNewStock();
            _AddNewStock.Owner = this;
            _AddNewStock.SetAccessRightsDS(_dsAccessRights);
            this.Cursor = Cursors.Arrow;
            _AddNewStock._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _AddNewStock._CompanyName = this._CompanyName;
            _AddNewStock._SiteName = this._SiteName;
            _AddNewStock._UserName = this._UserName;
            _AddNewStock._StaffPic = this._StaffPic;
            _AddNewStock.StaffImage.Source = this.StaffImage.Source;
            _AddNewStock.Owner = this.Owner;
            this.Close();
            _AddNewStock.ShowDialog();
        }

        private void btnPOSMakeASale_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _ClockInCheck = Comman.Constants.POS_CheckClockIn(_ClockInCheck);

                if (!_ClockInCheck._notClockedIn)
                {
                    if (_ClockInCheck._clockedInNotClockedOut)
                    {
                        //GAS - 27 POS Home Club : 19/SEP/2017
                        if (ConfigurationManager.AppSettings["SuperAdmin"].ToString().ToUpper().Trim() != "YES")
                        {
                            if
                            (
                                ConfigurationManager.AppSettings["LoggedInSiteID"].ToString().ToUpper().Trim() !=
                                ConfigurationManager.AppSettings["HomeClubSiteID"].ToString().ToUpper().Trim()
                            )
                            {
                                MessageBoxResult _HomeClubResult =
                                MessageBox.Show
                                    (
                                        "Your selected site is different to the Home site!" +
                                        "\nWould you like to still continue with POS transaction - Yes or No?" +
                                        "\n\n(CLICK 'YES' TO PROCEED, 'NO' TO EXIT!)",
                                        this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No
                                    );
                                if (
                                        _HomeClubResult == MessageBoxResult.No || _HomeClubResult == MessageBoxResult.Cancel
                                   )
                                {
                                    return;
                                }
                            }
                        }
                        //GAS - 27 POS Home Club : 19/SEP/2017

                        this.Cursor = Cursors.Wait;
                        MakeaSale makeaSale = new MakeaSale();
                        makeaSale.Owner = this;
                        makeaSale.SetAccessRightsDS(_dsAccessRights);
                        this.Cursor = Cursors.Arrow;
                        makeaSale._CurrentLoginDateTime = this._CurrentLoginDateTime;
                        makeaSale._CompanyName = _CompanyName;
                        makeaSale._SiteName = this._SiteName;
                        makeaSale._UserName = this._UserName;
                        makeaSale._StaffPic = this._StaffPic;
                        makeaSale.StaffImage.Source = this.StaffImage.Source;
                        makeaSale.ShowDialog();
                    }
                    else if (_ClockInCheck._clockedInAndClockedOut)
                    {
                        MessageBox.Show("End-Of-Shift has been completed for Today!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
                else
                {
                    decimal _clockInBalance = -1;
                    string _strClockInBalance = null;

                    _strClockInBalance = ConfigurationManager.AppSettings["POSOpeningBalance"].ToString();
                    if (_strClockInBalance == "")
                        _strClockInBalance = "0.00";

                    //_strClockInBalance = Interaction.InputBox("Enter Opening Cash Balance : ", "POS Clock-In");
                    //if ((_strClockInBalance == "") || (_strClockInBalance == null))
                    //{
                    //    MessageBox.Show("No Amount entered!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //    return;
                    //}
                    //if (!Regex.IsMatch(_strClockInBalance, @"^\d{1,7}(\.\d{1,2})?$"))
                    //{
                    //    MessageBox.Show("Amount should be Numeric! (upto 2 decimal places)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //    return;
                    //}
                    _clockInBalance = Convert.ToDecimal(_strClockInBalance);
                    if (_clockInBalance < 0)
                    {
                        MessageBox.Show("Amount CANNOT be less than Zero!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }

                    int _result = Comman.Constants.POS_ClockIn_User(_clockInBalance);

                    if (_result > 0)
                    {
                        //GAS - 27 POS Home Club : 19/SEP/2017
                        if (ConfigurationManager.AppSettings["SuperAdmin"].ToString().ToUpper().Trim() != "YES")
                        {
                            if
                            (
                                ConfigurationManager.AppSettings["LoggedInSiteID"].ToString().ToUpper().Trim() !=
                                ConfigurationManager.AppSettings["HomeClubSiteID"].ToString().ToUpper().Trim()
                            )
                            {
                                MessageBoxResult _HomeClubResult =
                                MessageBox.Show
                                    (
                                        "Your selected site is different to the Home site!" +
                                        "\nWould you like to still continue with POS transaction - Yes or No?" +
                                        "\n\n(CLICK 'YES' TO PROCEED, 'NO' TO EXIT!)",
                                        this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No
                                    );
                                if (
                                        _HomeClubResult == MessageBoxResult.No || _HomeClubResult == MessageBoxResult.Cancel
                                   )
                                {
                                    return;
                                }
                            }
                        }
                        //GAS - 27 POS Home Club : 19/SEP/2017

                        this.Cursor = Cursors.Wait;
                        MakeaSale makeaSale = new MakeaSale();
                        makeaSale.Owner = this;
                        makeaSale.SetAccessRightsDS(_dsAccessRights);
                        this.Cursor = Cursors.Arrow;
                        makeaSale._CurrentLoginDateTime = this._CurrentLoginDateTime;
                        makeaSale._CompanyName = _CompanyName;
                        makeaSale._SiteName = this._SiteName;
                        makeaSale._UserName = this._UserName;
                        makeaSale._StaffPic = this._StaffPic;
                        makeaSale.StaffImage.Source = this.StaffImage.Source;
                        makeaSale.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Error in Staff Clock-In!", this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }
        
        private void btnViewInventory_Click(object sender, RoutedEventArgs e)
        {
            //SAME WINDOW; NO ACTION;

            //this.Cursor = Cursors.Wait;
            //ViewInventory vI = new ViewInventory();
            //vI.Owner = this;
            //vI.SetAccessRightsDS(_dsAccessRights);
            //this.Cursor = Cursors.Arrow;
            //vI._CurrentLoginDateTime = this._CurrentLoginDateTime;
            //vI._CompanyName = this._CompanyName;
            //vI._SiteName = this._SiteName;
            //vI._UserName = this._UserName;
            //vI._StaffPic = this._StaffPic;
            //vI.StaffImage.Source = this.StaffImage.Source;
            //vI.Owner = this.Owner;
            //this.Close();
            //vI.ShowDialog();
        }

        private void btnDoaStocktake_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            DoaStocktake dStock = new DoaStocktake();
            dStock.Owner = this;
            dStock.SetAccessRightsDS(_dsAccessRights);
            this.Cursor = Cursors.Arrow;
            dStock._CurrentLoginDateTime = this._CurrentLoginDateTime;
            dStock._CompanyName = this._CompanyName;
            dStock._SiteName = this._SiteName;
            dStock._UserName = this._UserName;
            dStock._StaffPic = this._StaffPic;
            dStock.StaffImage.Source = this.StaffImage.Source;
            dStock.Owner = this.Owner;
            this.Close();
            dStock.ShowDialog();
        }

        private void ViewInventory_Loaded(object sender, RoutedEventArgs e)
        {
            SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
            Utilities _Util = new Utilities();
            _Util.populateProductTypes_Inventory(cmbInventoryProdType);

            LoadInventoryDetails();
            cmbInventoryProdType.SelectedIndex = 0;

            if (ConfigurationManager.AppSettings["isAdminUser"].ToString().Trim().ToUpper() == "YES")
            {
                btnInventoryReset.IsEnabled = true;
            }
            else
            {
                btnInventoryReset.IsEnabled = false;
            }
        }
        private void LoadInventoryDetails()
        {
            int _POS_CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _POS_SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

            SqlHelper sql = new SqlHelper();
            if (sql._ConnOpen == 0) return;

            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _POS_CompanyID);
            ht.Add("@SiteID", _POS_SiteID);
            gvInventoryPurchaseDetails.ItemsSource = null;

            ds = new DataSet();
            ds = sql.ExecuteProcedure("GetProductInventoryDetails", ht);
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].DefaultView.Count > 0)
                    {
                        gvInventoryPurchaseDetails.ItemsSource = ds.Tables[0].DefaultView;
                    }
                }
            }
        }

        private void btnInventoryPrint_Click(object sender, RoutedEventArgs e)
        {
            if (gvInventoryPurchaseDetails.Items.Count > 0)
            {
                Print _print = new Print(gvInventoryPurchaseDetails, null, this, false, _CompanyName,null,null,null,null,0,false,null,null,null,false,true);
            }
        }

        private void btnInventoryReset_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("This will reset current Quantity to 0 (Zero) for ALL Products!\n\nAre you sure to Reset ?","View Inventory",MessageBoxButton.YesNo,MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                //RESET QTY TO ZERO := ProcName = ResetInventoryQtyToZero

                int _POS_CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _POS_SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return;

                Hashtable ht = new Hashtable();

                ht.Add("@CompanyID", _POS_CompanyID);
                ht.Add("@SiteID", _POS_SiteID);

                sql.ExecuteQuery("ResetInventoryQtyToZero", ht);
                LoadInventoryDetails();
                cmbInventoryProdType.SelectedIndex = 0;
            }
        }
    }
}
