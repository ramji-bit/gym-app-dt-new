﻿using System;
using System.Linq;
using System.Data;
using GymMembership.BAL;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Collections;
using System.Configuration;
using System.Windows.Input;
using System.IO;
using System.Windows.Media;
using System.Diagnostics;

using System.Net.Mail;

using GymMembership.Navigation;
using GymMembership.Comman;
using GymMembership.Reports;
using Microsoft.VisualBasic;

namespace GymMembership.Pointofsale
{
    /// <summary>
    /// Interaction logic for MakeaSale.xaml
    /// </summary>
    public partial class MakeaSale
    {
        enum _CashOrQty
        {
            None,
            Quantity,
            Cash,
            Card
        };

        bool _isViewInCalc = true;
        PointOfSaleBAL _PointofSaleBAL = new PointOfSaleBAL();
        string _ProdType;
        string _Product;

        MailMessage mail = new MailMessage();
        SmtpClient client = new SmtpClient();

        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _CompanyNameOnly = "";
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        DataSet _dsAccessRights = new DataSet();
        int _CurrProdAvailQuantity = 0;

        _CashOrQty Flag;
        int _qtyLength = 2;
        int _amtDollarsLength = 4; int _amtCentsLength = 2;
        bool _isPrintReceipt = true;
        bool _WindowLoaded = false;
        bool _isQuantityDefault = true;

        DataGrid _printGrid = new DataGrid();
        DataTable _dtEmailData = new DataTable();
        int _POSSummaryTable = 0; int _POSDetailsTable = 1; int _POSClockInOutTable = 2;
        decimal _TotalPaidAmt = 0m;
        decimal _TotalCashAmt = 0m;
        decimal _TotalCardAmt = 0m;
        decimal _TotalSaleAmt = 0m;

        //DISCOUNT
        public string _strClockInBalance = null;
        public decimal _clockOutBalance = -1;
        public int _discountPercent = 0;
        public bool _isDiscountApplied = false;
        bool _isDiscPercent = false;
        bool _isDiscDollar = false;
        public decimal _discountDollar = 0m;

        //ADD SALE
        public bool _isAddSale = false;
        public int _addSaleQty = 0;
        public decimal _addSaleUnitPrice = 0m;
        public decimal _addSaleTotPrice = 0m;

        string _TotalPaidAmt_s = "";
        string _TotalCashAmt_s = "";
        string _TotalCardAmt_s = "";
        string _TotalSaleAmt_s = "";
        string _OpeningCashBalance = null;
        string _ClosingCashBalance = null;

        public MakeaSale()
        {
            InitializeComponent();
            this.Closing += new CancelEventHandler(MakeASale_Closing);

            for (int i = 0; i < Application.Current.Windows.Count; i++)
            {
                if (Application.Current.Windows[i].Name.ToUpper() == "DASHBOARD")
                {
                    (Application.Current.Windows[i] as Navigation.Dashboard)._isDishonourCheckEnabled = false;
                    break;
                }
            }
        }
        void MakeASale_Closing(object sender, CancelEventArgs e)
        {
            #region "commented code for Closing Balance/THIS WILL BE HANDLED IN 'END OF SHIFT' BUTTON"
            //if (MessageBox.Show("Do you want to CLOCK-OUT Now?\n\n[Once you Clock-Out]", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            //{
            //    _strClockInBalance = Interaction.InputBox("Enter Closing Cash Balance : ", "POS Clock-Out");
            //    if ((_strClockInBalance == "") || (_strClockInBalance == null))
            //    {
            //        MessageBox.Show("No Amount entered!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //        e.Cancel = true;
            //        return;
            //    }
            //    if (!Regex.IsMatch(_strClockInBalance, @"^\d{1,7}(\.\d{1,2})?$"))
            //    {
            //        MessageBox.Show("Amount should be Numeric! (upto 2 decimal places)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //        e.Cancel = true;
            //        return;
            //    }
            //    _clockOutBalance = Convert.ToDecimal(_strClockInBalance);
            //    if (_clockOutBalance < 0)
            //    {
            //        MessageBox.Show("Amount CANNOT be less than Zero!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //        e.Cancel = true;
            //        return;
            //    }
            //    int _result = ClockOut_User(_clockOutBalance);
            //}
            //else
            //{
            //    e.Cancel = true;
            //    return;
            //}
            #endregion
            for (int i = 0; i < Application.Current.Windows.Count; i++)
            {
                if (Application.Current.Windows[i].Name.ToUpper() == "DASHBOARD")
                {
                    (Application.Current.Windows[i] as Navigation.Dashboard)._isDishonourCheckEnabled = true;
                    break;
                }
            }
        }
        private int ClockOut_User(decimal _openBalance)
        {
            try
            {
                int _POS_CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _POS_SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                int _POS_StaffID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedUserLoginID"].ToString());
                string _POS_StaffUser = ConfigurationManager.AppSettings["LoggedInUser"].ToString();

                DateTime _currDate = DateTime.Now;

                SqlHelper sql = new SqlHelper();
                if (sql._ConnOpen == 0) return 0;

                Hashtable ht = new Hashtable();

                ht.Add("@CompanyID", _POS_CompanyID);
                ht.Add("@SiteID", _POS_SiteID);
                ht.Add("@StaffID", _POS_StaffID);
                ht.Add("@DateIn", _currDate);
                ht.Add("@StaffUser", _POS_StaffUser);
                ht.Add("@ClockOutBalance", _openBalance);
                ht.Add("@appVersion", Comman.Constants._appVersion);

                //int _resultRows = sql.ExecuteQuery("POS_ClockOut_User_For_Today", ht);
                int _resultRows = sql.ExecuteQuery("POS_ClockOut_Site_For_Today", ht);
                return _resultRows;
            }
            catch (Exception ex)
            {
                Comman.Constants.AddToCommunicationLog(0, "INTERNAL", "*** RETRIEVE ERROR (ON CLOCK-OUT) ***", ex.Message, "NONE", 0);
                Comman.Constants._emailBeingSent = false;
                return 0;
            }
        }
        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto(_StaffPic);
        }

        public class myItem
        {
            public string Product { get; set; }
            public string UnitPrice { get; set; }
            public string Quantity { get; set; }
            public string CurrSym { get; set; }
            public string Total { get; set; }
            public int ProductID { get; set; }
            public int currStock { get; set; }

            public static explicit operator myItem(DataRowView v)
            {
                throw new NotImplementedException();
            }
        }

        Product Prd = new Product();
        int[] ProdCurrQty = new int[100];
        decimal[] ProdSalePrice = new decimal[100];
        object ProdTotalPrice = "";

        string _AutoGenerateInvoiceNo = "InvoiceNo";
        int _CompanyID, _SiteID;

        private void btnPRODTYPE_Click(object sender, RoutedEventArgs e)
        {
            Console.Beep(4000, 100);
            Flag = _CashOrQty.Quantity;
            _ProdType = (((sender as System.Windows.Controls.Button).Content as System.Windows.Controls.StackPanel).Children[0] as System.Windows.Controls.TextBlock).Text;
            DataSet ds = Prd.getProductsbyType_Active(_ProdType);
            LoadProducts(ds);
            LoadProducts_Buttons(ds);
            Flag = _CashOrQty.Quantity;
        }

        private void btnPROD_Click(object sender, RoutedEventArgs e)
        {
            _Product = (((sender as System.Windows.Controls.Button).Content as System.Windows.Controls.StackPanel).Children[0] as System.Windows.Controls.TextBlock).Text;
            
            //cmbProduct.Text = _Product;
            SetProductIndex(_Product);
            if (cmbProduct.SelectedIndex < 0) return;

            Flag = _CashOrQty.Quantity;
            txtQuantity.CaretIndex = txtQuantity.Text.Length;
            btnQuantity.Background = Brushes.DarkGreen;
            btnCard.Background = Brushes.LightGreen;
            btnCash.Background = Brushes.LightGreen;
            grdCalculation.IsEnabled = true;

            if (Flag == _CashOrQty.Cash)
            {
                txtQuantity.Text = "0";
            }
            else if (Flag == _CashOrQty.Quantity)
            {
                txtQuantity.Text = "1";
                _isQuantityDefault = true;
            }
            btnConfirmForInvoice_Click(sender, e);

            gbMakeaSaleProductToSelect.Visibility = Visibility.Hidden;
            gbMakeaSaleProducts.Visibility = Visibility.Visible;
        }
        private void SetProductIndex(string _Product)
        {
            for (int i = 0; i < cmbProduct.Items.Count; i++)
            {
                if ((cmbProduct.Items[i] as DataRowView).Row["ProductName"].ToString().ToUpper().Trim() == _Product.Trim().ToUpper())
                {
                    cmbProduct.SelectedIndex = i;
                    break;
                }
            }
        }
        private void btnConfirmForInvoice_Click(object sender, RoutedEventArgs e)
        {
            if (txtQuantity.Text.Trim() == "")
            {
                //System.Windows.MessageBox.Show("Enter Sale Quantity/Nos. for this Product!",this.Title,MessageBoxButton.OK,MessageBoxImage.Stop);
                //txtQuantity.Focus();
                return;
            }

            if (!txtQuantity.Text.All(c => Char.IsNumber(c)))
            {
                //System.Windows.MessageBox.Show("Enter Quantity in Numbers..!", this.Title, MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }

            if (Convert.ToInt32(txtQuantity.Text.Trim()) <= 0)
            {
                //System.Windows.MessageBox.Show("Quantity Cannot be Zero OR Less !!", this.Title, MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }

            if (cmbProduct.SelectedIndex == -1)
            {
                //System.Windows.MessageBox.Show("Please Select a Product!", this.Title, MessageBoxButton.OK, MessageBoxImage.Stop);
                //cmbProduct.Focus();
                return;
            }
            if (_CurrProdAvailQuantity < Convert.ToInt32(txtQuantity.Text))
            {
                //AVAILABLE QUANTITY; LESS THAN SELECTED QUANTITY
                if (_CurrProdAvailQuantity <= 0)
                {
                    MessageBox.Show("Product OUT-OF-STOCK !", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Stock is LESS than Quantity Requested !", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                }
                return;
            }

            object _UnitPrice = ProdSalePrice[cmbProduct.SelectedIndex];
            ProdTotalPrice = Convert.ToString(ProdSalePrice[cmbProduct.SelectedIndex] * (Convert.ToInt32(txtQuantity.Text)));
            myItem invItem = new myItem { UnitPrice = _UnitPrice.ToString(), Product = cmbProduct.Text.ToString(), Quantity = txtQuantity.Text, CurrSym = "$", Total = (ProdTotalPrice.ToString()), ProductID = Convert.ToInt32(cmbProduct.SelectedValue), currStock = _CurrProdAvailQuantity };

            AddItemAndCalculateTotal(invItem);

            if (Flag == _CashOrQty.Cash)
            {
                txtQuantity.Text = "0";
            }
            else if (Flag == _CashOrQty.Quantity)
            {
                txtQuantity.Text = "1";
            }
        }

        void AddItemAndCalculateTotal(myItem invItem)
        {
            if (CheckIfProductAlreadySelected(invItem.ProductID))
            {
                //MessageBox.Show("Product Already Selected for Invoice!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                Console.Beep(4000, 100);
                Console.Beep(4000, 100);
                cmbProduct.SelectedIndex = -1;
                return;
            }

            double Amount = Convert.ToDouble(txtTotalSaleAmt.Text);
            Amount = Amount + Convert.ToDouble(invItem.Total);
            txtTotalSaleAmt.Text = Amount.ToString("0.00"); //need to Format to 2 decimal places

            lviewInvoice.Items.Add(invItem);
            lviewInvoice.ScrollIntoView(lviewInvoice.Items[lviewInvoice.Items.Count - 1]);
            lviewInvoice.SelectedIndex = lviewInvoice.Items.Count - 1;
        }
        void AddItemAndCalculateTotal(myItem invItem, int _insertIndex)
        {
            double Amount = Convert.ToDouble(txtTotalSaleAmt.Text);
            Amount = Amount + Convert.ToDouble(invItem.Total);
            txtTotalSaleAmt.Text = Amount.ToString("0.00"); //need to Format to 2 decimal places

            lviewInvoice.Items.Insert(_insertIndex, invItem);
            lviewInvoice.ScrollIntoView(lviewInvoice.Items[_insertIndex]);
            lviewInvoice.SelectedIndex = _insertIndex;
        }
        private bool CheckIfProductAlreadySelected(int _prodID)
        {
            if (lviewInvoice.Items.Count > 0)
            {
                for (int i = 0; i < lviewInvoice.Items.Count; i++)
                {
                    myItem _checkItem = new myItem();
                    _checkItem = (myItem)lviewInvoice.Items[i];
                    if (_checkItem.ProductID == _prodID)
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                return false;
            }
        }

        private void LoadProducts(DataSet ds)
        {
            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                ProdCurrQty[i] = Convert.ToInt32(ds.Tables[0].Rows[i]["CurrQuantity"]);
                ProdSalePrice[i] = Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]);
            }
            cmbProduct.ItemsSource = null;
            cmbProduct.Items.Clear();
            cmbProduct.ItemsSource = ds.Tables[0].DefaultView;
            cmbProduct.DisplayMemberPath = "ProductName";
            cmbProduct.SelectedValuePath = "ProductID";
            //if (cmbProduct.Items.Count >= 1) { cmbProduct.SelectedIndex = 0; }
        }
        private void HideOtherProductTypeButtons()
        {
            btnGeneral.Visibility = Visibility.Hidden;
            btnEquipment.Visibility = Visibility.Hidden;
            btnDrink.Visibility = Visibility.Hidden;
            btnFood.Visibility = Visibility.Hidden;
            btnOther.Visibility = Visibility.Hidden;
            btnPrdTypeR1C6.Visibility = Visibility.Hidden;
            btnPrdTypeR2C1.Visibility = Visibility.Hidden;
            btnPrdTypeR2C2.Visibility = Visibility.Hidden;
            btnPrdTypeR2C3.Visibility = Visibility.Hidden;
            btnPrdTypeR2C4.Visibility = Visibility.Hidden;
            btnPrdTypeR2C5.Visibility = Visibility.Hidden;
            btnPrdTypeR2C6.Visibility = Visibility.Hidden;
            btnPrdTypeR3C1.Visibility = Visibility.Hidden;
            btnPrdTypeR3C2.Visibility = Visibility.Hidden;
            btnPrdTypeR3C3.Visibility = Visibility.Hidden;
            btnPrdTypeR3C4.Visibility = Visibility.Hidden;
            btnPrdTypeR3C5.Visibility = Visibility.Hidden;
            btnPrdTypeR3C6.Visibility = Visibility.Hidden;
            btnPrdTypeR4C1.Visibility = Visibility.Hidden;
            btnPrdTypeR4C2.Visibility = Visibility.Hidden;
            btnPrdTypeR4C3.Visibility = Visibility.Hidden;
            btnPrdTypeR4C4.Visibility = Visibility.Hidden;
            btnPrdTypeR4C5.Visibility = Visibility.Hidden;
            btnPrdTypeR4C6.Visibility = Visibility.Hidden;
            btnPrdTypeR5C1.Visibility = Visibility.Hidden;
            btnPrdTypeR5C2.Visibility = Visibility.Hidden;
            btnPrdTypeR5C3.Visibility = Visibility.Hidden;
            btnPrdTypeR5C4.Visibility = Visibility.Hidden;
            btnPrdTypeR5C5.Visibility = Visibility.Hidden;
            btnPrdTypeR5C6.Visibility = Visibility.Hidden;
        }
        private void HideOtherProductButtons()
        {
            btnPGeneral.Visibility = Visibility.Hidden;
            btnPEquipment.Visibility = Visibility.Hidden;
            btnPDrink.Visibility = Visibility.Hidden;
            btnPFood.Visibility = Visibility.Hidden;
            btnPOther.Visibility = Visibility.Hidden;
            btnPrdR1C6.Visibility = Visibility.Hidden;
            btnPrdR2C1.Visibility = Visibility.Hidden;
            btnPrdR2C2.Visibility = Visibility.Hidden;
            btnPrdR2C3.Visibility = Visibility.Hidden;
            btnPrdR2C4.Visibility = Visibility.Hidden;
            btnPrdR2C5.Visibility = Visibility.Hidden;
            btnPrdR2C6.Visibility = Visibility.Hidden;
            btnPrdR3C1.Visibility = Visibility.Hidden;
            btnPrdR3C2.Visibility = Visibility.Hidden;
            btnPrdR3C3.Visibility = Visibility.Hidden;
            btnPrdR3C4.Visibility = Visibility.Hidden;
            btnPrdR3C5.Visibility = Visibility.Hidden;
            btnPrdR3C6.Visibility = Visibility.Hidden;
            btnPrdR4C1.Visibility = Visibility.Hidden;
            btnPrdR4C2.Visibility = Visibility.Hidden;
            btnPrdR4C3.Visibility = Visibility.Hidden;
            btnPrdR4C4.Visibility = Visibility.Hidden;
            btnPrdR4C5.Visibility = Visibility.Hidden;
            btnPrdR4C6.Visibility = Visibility.Hidden;
            btnPrdR5C1.Visibility = Visibility.Hidden;
            btnPrdR5C2.Visibility = Visibility.Hidden;
            btnPrdR5C3.Visibility = Visibility.Hidden;
            btnPrdR5C4.Visibility = Visibility.Hidden;
            btnPrdR5C5.Visibility = Visibility.Hidden;
            btnPrdR5C6.Visibility = Visibility.Hidden;
        }

        private void LoadProducts_Buttons(DataSet ds)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                //Member_Image_Img.Source = new BitmapImage(new Uri(@"..\Images\User_Image.png", UriKind.RelativeOrAbsolute));
                HideOtherProductButtons();

                gbMakeaSaleProducts.Visibility = Visibility.Hidden;
                gbMakeaSaleProductToSelect.Visibility = Visibility.Visible;

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (i > 29)
                    {
                        break;
                    }

                    ProdCurrQty[i] = Convert.ToInt32(ds.Tables[0].Rows[i]["CurrQuantity"]);
                    ProdSalePrice[i] = Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]);
                    _Product = ds.Tables[0].Rows[i]["ProductName"].ToString();

                    switch (i)
                    {
                        case 0:
                            btnPGeneralContent.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPGeneralPrice.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPGeneral.Visibility = Visibility.Visible;
                            btnPGeneral.IsEnabled = true;
                            btnPGeneral.ToolTip = btnPGeneralContent.Text;
                            break;
                        case 1:
                            btnPEquipmentContent.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPEquipmentPrice.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPEquipment.Visibility = Visibility.Visible;
                            btnPEquipment.IsEnabled = true;
                            btnPEquipment.ToolTip = btnPEquipmentContent.Text;
                            break;
                        case 2:
                            btnPDrinkContent.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPDrinkPrice.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPDrink.Visibility = Visibility.Visible;
                            btnPDrink.IsEnabled = true;
                            btnPDrink.ToolTip = btnPDrinkContent.Text;
                            break;
                        case 3:
                            btnPFoodContent.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPFoodPrice.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPFood.Visibility = Visibility.Visible;
                            btnPFood.IsEnabled = true;
                            btnPFood.ToolTip = btnPFoodContent.Text;
                            break;
                        case 4:
                            btnPOtherContent.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPOtherPrice.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPOther.Visibility = Visibility.Visible;
                            btnPOther.IsEnabled = true;
                            btnPOther.ToolTip = btnPOtherContent.Text;
                            break;
                        case 5:
                            btnPrdR1C6Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR1C6Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR1C6.Visibility = Visibility.Visible;
                            btnPrdR1C6.IsEnabled = true;
                            btnPrdR1C6.Background = System.Windows.Media.Brushes.Violet;
                            btnPrdR1C6.ToolTip = btnPrdR1C6Content.Text;
                            break;
                        case 6:
                            btnPrdR2C1Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR2C1Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR2C1.Visibility = Visibility.Visible;
                            btnPrdR2C1.IsEnabled = true;
                            btnPrdR2C1.Background = System.Windows.Media.Brushes.Green;
                            btnPrdR2C1.ToolTip = btnPrdR2C1Content.Text;
                            break;
                        case 7:
                            btnPrdR2C2Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR2C2Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR2C2.Visibility = Visibility.Visible;
                            btnPrdR2C2.IsEnabled = true;
                            btnPrdR2C2.Background = System.Windows.Media.Brushes.Green;
                            btnPrdR2C2.ToolTip = btnPrdR2C2Content.Text;
                            break;
                        case 8:
                            btnPrdR2C3Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR2C3Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR2C3.Visibility = Visibility.Visible;
                            btnPrdR2C3.IsEnabled = true;
                            btnPrdR2C3.Background = System.Windows.Media.Brushes.DarkViolet;
                            btnPrdR2C3.ToolTip = btnPrdR2C3Content.Text;
                            break;
                        case 9:
                            btnPrdR2C4Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR2C4Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR2C4.Visibility = Visibility.Visible;
                            btnPrdR2C4.IsEnabled = true;
                            btnPrdR2C4.Background = System.Windows.Media.Brushes.Orange;
                            btnPrdR2C4.ToolTip = btnPrdR2C4Content.Text;
                            break;
                        case 10:
                            btnPrdR2C5Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR2C5Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR2C5.Visibility = Visibility.Visible;
                            btnPrdR2C5.IsEnabled = true;
                            btnPrdR2C5.Background = System.Windows.Media.Brushes.OrangeRed;
                            btnPrdR2C5.ToolTip = btnPrdR2C5Content.Text;
                            break;
                        case 11:
                            btnPrdR2C6Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR2C6Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR2C6.Visibility = Visibility.Visible;
                            btnPrdR2C6.IsEnabled = true;
                            btnPrdR2C6.Background = System.Windows.Media.Brushes.OrangeRed;
                            btnPrdR2C6.ToolTip = btnPrdR2C6Content.Text;
                            break;
                        case 12:
                            btnPrdR3C1Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR3C1Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR3C1.Visibility = Visibility.Visible;
                            btnPrdR3C1.IsEnabled = true;
                            btnPrdR3C1.Background = System.Windows.Media.Brushes.Green;
                            btnPrdR3C1.ToolTip = btnPrdR3C1Content.Text;
                            break;
                        case 13:
                            btnPrdR3C2Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR3C2Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR3C2.Visibility = Visibility.Visible;
                            btnPrdR3C2.IsEnabled = true;
                            btnPrdR3C2.Background = System.Windows.Media.Brushes.MidnightBlue;
                            btnPrdR3C2.ToolTip = btnPrdR3C2Content.Text;
                            break;
                        case 14:
                            btnPrdR3C3Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR3C3Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR3C3.Visibility = Visibility.Visible;
                            btnPrdR3C3.IsEnabled = true;
                            btnPrdR3C3.Background = System.Windows.Media.Brushes.DarkViolet;
                            btnPrdR3C3.ToolTip = btnPrdR3C3Content.Text;
                            break;
                        case 15:
                            btnPrdR3C4Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR3C4Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR3C4.Visibility = Visibility.Visible;
                            btnPrdR3C4.IsEnabled = true;
                            btnPrdR3C4.Background = System.Windows.Media.Brushes.Orange;
                            btnPrdR3C4.ToolTip = btnPrdR3C4Content.Text;
                            break;
                        case 16:
                            btnPrdR3C5Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR3C5Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR3C5.Visibility = Visibility.Visible;
                            btnPrdR3C5.IsEnabled = true;
                            btnPrdR3C5.Background = System.Windows.Media.Brushes.OrangeRed;
                            btnPrdR3C5.ToolTip = btnPrdR3C5Content.Text;
                            break;
                        case 17:
                            btnPrdR3C6Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR3C6Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR3C6.Visibility = Visibility.Visible;
                            btnPrdR3C6.IsEnabled = true;
                            btnPrdR3C6.Background = System.Windows.Media.Brushes.OrangeRed;
                            btnPrdR3C6.ToolTip = btnPrdR3C6Content.Text;
                            break;
                        case 18:
                            btnPrdR4C1Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR4C1Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR4C1.Visibility = Visibility.Visible;
                            btnPrdR4C1.IsEnabled = true;
                            btnPrdR4C1.Background = System.Windows.Media.Brushes.Green;
                            btnPrdR4C1.ToolTip = btnPrdR4C1Content.Text;
                            break;
                        case 19:
                            btnPrdR4C2Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR4C2Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR4C2.Visibility = Visibility.Visible;
                            btnPrdR4C2.IsEnabled = true;
                            btnPrdR4C2.Background = System.Windows.Media.Brushes.CornflowerBlue;
                            btnPrdR4C2.ToolTip = btnPrdR4C2Content.Text;
                            break;
                        case 20:
                            btnPrdR4C3Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR4C3Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR4C3.Visibility = Visibility.Visible;
                            btnPrdR4C3.IsEnabled = true;
                            btnPrdR4C3.Background = System.Windows.Media.Brushes.DarkViolet;
                            btnPrdR4C3.ToolTip = btnPrdR4C3Content.Text;
                            break;
                        case 21:
                            btnPrdR4C4Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR4C4Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR4C4.Visibility = Visibility.Visible;
                            btnPrdR4C4.IsEnabled = true;
                            btnPrdR4C4.Background = System.Windows.Media.Brushes.Orange;
                            btnPrdR4C4.ToolTip = btnPrdR4C4Content.Text;
                            break;
                        case 22:
                            btnPrdR4C5Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR4C5Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR4C5.Visibility = Visibility.Visible;
                            btnPrdR4C5.IsEnabled = true;
                            btnPrdR4C5.Background = System.Windows.Media.Brushes.OrangeRed;
                            btnPrdR4C5.ToolTip = btnPrdR4C5Content.Text;
                            break;
                        case 23:
                            btnPrdR4C6Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR4C6Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR4C6.Visibility = Visibility.Visible;
                            btnPrdR4C6.IsEnabled = true;
                            btnPrdR4C6.Background = System.Windows.Media.Brushes.OrangeRed;
                            btnPrdR4C6.ToolTip = btnPrdR4C6Content.Text;
                            break;
                        case 24:
                            btnPrdR5C1Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR5C1Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR5C1.Visibility = Visibility.Visible;
                            btnPrdR5C1.IsEnabled = true;
                            btnPrdR5C1.Background = System.Windows.Media.Brushes.Chocolate;
                            btnPrdR5C1.ToolTip = btnPrdR5C1Content.Text;
                            break;
                        case 25:
                            btnPrdR5C2Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR5C2Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR5C2.Visibility = Visibility.Visible;
                            btnPrdR5C2.IsEnabled = true;
                            btnPrdR5C2.Background = System.Windows.Media.Brushes.Green;
                            btnPrdR5C2.ToolTip = btnPrdR5C2Content.Text;
                            break;
                        case 26:
                            btnPrdR5C3Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR5C3Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR5C3.Visibility = Visibility.Visible;
                            btnPrdR5C3.IsEnabled = true;
                            btnPrdR5C3.Background = System.Windows.Media.Brushes.DarkViolet;
                            btnPrdR5C3.ToolTip = btnPrdR5C3Content.Text;
                            break;
                        case 27:
                            btnPrdR5C4Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR5C4Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR5C4.Visibility = Visibility.Visible;
                            btnPrdR5C4.IsEnabled = true;
                            btnPrdR5C4.Background = System.Windows.Media.Brushes.Orange;
                            btnPrdR5C4.ToolTip = btnPrdR5C4Content.Text;
                            break;
                        case 28:
                            btnPrdR5C5Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR5C5Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR5C5.Visibility = Visibility.Visible;
                            btnPrdR5C5.IsEnabled = true;
                            btnPrdR5C5.Background = System.Windows.Media.Brushes.OrangeRed;
                            btnPrdR5C5.ToolTip = btnPrdR5C5Content.Text;
                            break;
                        case 29:
                            btnPrdR5C6Content.Text = ds.Tables[0].Rows[i]["ProductName"].ToString();
                            btnPrdR5C6Price.Text = "$ " + Convert.ToDecimal(ds.Tables[0].Rows[i]["ProductSalePrice"]).ToString("##0.#0");
                            btnPrdR5C6.Visibility = Visibility.Visible;
                            btnPrdR5C6.IsEnabled = true;
                            btnPrdR5C6.Background = System.Windows.Media.Brushes.OrangeRed;
                            btnPrdR5C6.ToolTip = btnPrdR5C6Content.Text;
                            break;
                    }
                }
            }
            else
            {
                System.Windows.MessageBox.Show("No PRODUCTS Defined for this Product Type - in Settings!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void cmbProduct_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (cmbProduct.SelectedIndex != -1)
            {
                _CurrProdAvailQuantity = Convert.ToInt32((cmbProduct.SelectedItem as DataRowView).Row["CurrQuantity"].ToString());
            }

            if (Flag == _CashOrQty.Cash)
            {
                txtQuantity.Text = "0";
            }
            else if (Flag == _CashOrQty.Quantity)
            {
                txtQuantity.Text = "1";
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            Console.Beep(4000, 100);
            ItemClear();
        }
        private void btnMakeaSaleCancelSale_Click(object sender, RoutedEventArgs e)
        {
            Console.Beep(4000, 100);
            Console.Beep(4000, 100);
            ItemClear();
            MakeASaleClear();
        }
        private void gvMakeaSaleMember_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            int i; string _DisplayName, _DisplayMemberNo;
            i = gvMakeaSaleMember.SelectedIndex;
            if (i != -1)
            {
                _DisplayName = ((gvMakeaSaleMember.Items[i] as DataRowView).Row.ItemArray[0].ToString() + " " + (gvMakeaSaleMember.Items[i] as DataRowView).Row.ItemArray[1].ToString());
                _DisplayMemberNo = (gvMakeaSaleMember.Items[i] as DataRowView).Row.ItemArray[2].ToString();

                txtMakeaSaleSoldTo.Text = _DisplayName;
                txtMakeaSaleInSoldTo1.Text = _DisplayMemberNo;
            }
            else
            {
                txtMakeaSaleSoldTo.Text = "NON-MEMBER";
                txtMakeaSaleInSoldTo1.Text = "0";
            }
        }

        private void txtMakeaSaleSearch_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            string _MemberLastName = txtMakeaSaleSearch.Text.Trim();
            gvMakeaSaleMember.ItemsSource = null;
            gvMakeaSaleMember.ItemsSource = _PointofSaleBAL.getPOSMemberDetails(_MemberLastName).Tables[0].AsDataView();
        }

        private void btnMakeaSaleClear_Click(object sender, RoutedEventArgs e)
        {
            MakeASaleClear();
        }

        private void btnMakeaSaleCompleteSale_Click(object sender, RoutedEventArgs e)
        {
            Console.Beep(4000, 100);
            if (lviewInvoice.Items.Count <= 0)
            {
                //MessageBox.Show("Please Select Atleast 1 Item to Invoice!", this.Title, MessageBoxButton.OK, MessageBoxImage.Stop);
                Console.Beep(4000, 100);
                return;
            }

            if ((txtTotalSaleAmt.Text.Trim() == "") || (txtTotalSaleAmt.Text.Trim() == "0.00"))
            {
                if ((txtTotalSaleAmt.Text.Trim() == "0.00") && (!_isDiscountApplied))
                {
                    //MessageBox.Show("Sale Amount Cannot be Zero!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Stop);
                    Console.Beep(4000, 100);
                    return;
                }
                else if (txtTotalSaleAmt.Text.Trim() == "")
                {
                    //MessageBox.Show("Sale Amount Cannot be Zero!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Stop);
                    Console.Beep(4000, 100);
                    return;
                }
            }

            //if (Convert.ToDecimal(txtPaid.Text) <= 0)
            //{
            //    MessageBox.Show("Payment Incomplete !!", this.Title, MessageBoxButton.OK, MessageBoxImage.Stop);
            //    return;
            //}

            if (Convert.ToDecimal(txtDue.Text) > 0)
            {
                Console.Beep(4000, 100);
                MessageBox.Show("Payment Incomplete !!", this.Title, MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }

            if (Convert.ToDouble(txtCard.Text) > Convert.ToDouble(txtTotalSaleAmt.Text))
            {
                Console.Beep(4000, 100);
                MessageBox.Show("Card Payment more than Sale Amount !!", this.Title, MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }

            if (gvMakeaSaleMember.SelectedIndex == -1)
            {
                txtMakeaSaleSoldTo.Text = "NON-MEMBER";
                txtMakeaSaleInSoldTo1.Text = "0";
            }

            {
                int InvNo = RAISE_POS_INVOICE();
                if (InvNo != -1)
                {
                    RAISE_POS_INVOICEDETAILS(InvNo);
                }

                if (_isPrintReceipt)
                {
                    string _discPer = null;
                    string _discAmt = null;
                    string _discSym = null;
                    string _TotSale = txtTotalSaleAmt.Text;

                    if (_isDiscountApplied)
                    {
                        if (_isDiscPercent)
                        {
                            _discPer = _discountPercent.ToString("#0.#0");
                            _discSym = "%";
                        }
                        else if (_isDiscDollar)
                        {
                            _discPer = _discountDollar.ToString("#0.#0");
                            _discSym = "$";
                        }
                        _discAmt = txtTotalSaleAmt.Text;
                        _TotSale = txtDiscount.Text;
                    }
                    Reports.Print print = new Reports.Print(
                                                                lviewInvoice, null, this, true, _CompanyName, txtPaid.Text,
                                                                txtBalance.Text, txtCard.Text, _TotSale, InvNo, false, null,
                                                                null, null, false, false, _discPer, _discAmt, _discSym
                                                            );
                }
                //MessageBox.Show("SALE COMPLETE!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                ItemClear();
                MakeASaleClear();
            }
        }

        private void MakeASale_Loaded(object sender, RoutedEventArgs e)
        {
            _WindowLoaded = true;
            lblDiscount.Visibility = Visibility.Collapsed;
            txtDiscount.Visibility = Visibility.Collapsed;

            HideOtherProductTypeButtons();

            gvMakeaSaleMember.ItemsSource = null;
            gvMakeaSaleMember.Items.Clear();

            SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
            gvMakeaSaleMember.ItemsSource = null;
            gvMakeaSaleMember.ItemsSource = _PointofSaleBAL.getPOSMemberDetails("").Tables[0].AsDataView();
            //txtTotalSaleAmt.Text = "0.00";

            txtMakeaSaleSoldTo.Text = "NON-MEMBER";
            txtMakeaSaleInSoldTo1.Text = "0";

            _isPrintReceipt = true;

            gbMakeaSaleProducts.Visibility = Visibility.Visible;
            gbMakeaSaleProductToSelect.Visibility = Visibility.Hidden;

            if (ConfigurationManager.AppSettings["isAdminUser"].ToString().ToUpper() == "YES")
            {
                colAddSale.Width = new GridLength(9, GridUnitType.Star);
                colDiscount.Width = new GridLength(9, GridUnitType.Star);
                colDiscountDollar.Width = new GridLength(9, GridUnitType.Star);
                btnMakeaSaleAddsale.Visibility = Visibility.Visible;
                btnMakeaSaleDiscount.Visibility = Visibility.Visible;
                btnMakeaSaleDiscountDollar.Visibility = Visibility.Visible;
            }
            else
            {
                colAddSale.Width = new GridLength(0, GridUnitType.Star);
                colDiscount.Width = new GridLength(0, GridUnitType.Star);
                colDiscountDollar.Width = new GridLength(0, GridUnitType.Star);
                btnMakeaSaleAddsale.Visibility = Visibility.Collapsed;
                btnMakeaSaleDiscount.Visibility = Visibility.Collapsed;
                btnMakeaSaleDiscountDollar.Visibility = Visibility.Collapsed;
            }
            btnMakeaSaleAddsale.IsEnabled = btnMakeaSaleDiscount.IsEnabled = btnMakeaSaleDiscountDollar.IsEnabled =
                (ConfigurationManager.AppSettings["isAdminUser"].ToString().ToUpper() == "YES");

            if (btnMakeaSaleAddsale.IsEnabled)
            {
                btnMakeaSaleAddsale.Opacity = 1;
                btnMakeaSaleDiscount.Opacity = 1;
                btnMakeaSaleDiscountDollar.Opacity = 1;
            }
            else
            {
                btnMakeaSaleAddsale.Opacity = 0.5;
                btnMakeaSaleDiscount.Opacity = 0.5;
                btnMakeaSaleDiscountDollar.Opacity = 0.5;
            }

            Hashtable ht = new Hashtable();
            ht.Add("@CompanyID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString()));
            //ht.Add("@SiteID", Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()));
            SqlHelper sH = new SqlHelper();
            if (sH._ConnOpen == 0) return;

            DataSet ds = sH.ExecuteProcedure("GetProductTypes_Active", ht);
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            //if (i > 11)
                            //if (i > 7)
                            if (i > 29)
                            {
                                break;
                            }
                            switch (i)
                            {
                                case 0:
                                    btnGeneralContent.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnGeneral.Visibility = Visibility.Visible;
                                    btnGeneral.IsEnabled = true;
                                    btnGeneral.ToolTip = btnGeneralContent.Text;
                                    break;
                                case 1:
                                    btnEquipmentContent.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnEquipment.Visibility = Visibility.Visible;
                                    btnEquipment.IsEnabled = true;
                                    btnEquipment.ToolTip = btnEquipmentContent.Text;
                                    break;
                                case 2:
                                    btnDrinkContent.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnDrink.Visibility = Visibility.Visible;
                                    btnDrink.IsEnabled = true;
                                    btnDrink.ToolTip = btnDrinkContent.Text;
                                    break;
                                case 3:
                                    btnFoodContent.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnFood.Visibility = Visibility.Visible;
                                    btnFood.IsEnabled = true;
                                    btnFood.ToolTip = btnFoodContent.Text;
                                    break;
                                case 4:
                                    btnOtherContent.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnOther.Visibility = Visibility.Visible;
                                    btnOther.IsEnabled = true;
                                    btnOther.ToolTip = btnOtherContent.Text;
                                    break;
                                case 5:
                                    btnPrdTypeR1C6Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR1C6.Visibility = Visibility.Visible;
                                    btnPrdTypeR1C6.IsEnabled = true;
                                    btnPrdTypeR1C6.Background = System.Windows.Media.Brushes.Violet;
                                    btnPrdTypeR1C6.ToolTip = btnPrdTypeR1C6Content.Text;
                                    break;
                                case 6:
                                    btnPrdTypeR2C1Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR2C1.Visibility = Visibility.Visible;
                                    btnPrdTypeR2C1.IsEnabled = true;
                                    btnPrdTypeR2C1.Background = System.Windows.Media.Brushes.Chocolate;
                                    btnPrdTypeR2C1.ToolTip = btnPrdTypeR2C1Content.Text;
                                    break;
                                case 7:
                                    btnPrdTypeR2C2Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR2C2.Visibility = Visibility.Visible;
                                    btnPrdTypeR2C2.IsEnabled = true;
                                    btnPrdTypeR2C2.Background = System.Windows.Media.Brushes.Green;
                                    btnPrdTypeR2C2.ToolTip = btnPrdTypeR2C2Content.Text;
                                    break;
                                case 8:
                                    btnPrdTypeR2C3Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR2C3.Visibility = Visibility.Visible;
                                    btnPrdTypeR2C3.IsEnabled = true;
                                    btnPrdTypeR2C3.Background = System.Windows.Media.Brushes.DarkViolet;
                                    btnPrdTypeR2C3.ToolTip = btnPrdTypeR2C3Content.Text;
                                    break;
                                case 9:
                                    btnPrdTypeR2C4Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR2C4.Visibility = Visibility.Visible;
                                    btnPrdTypeR2C4.IsEnabled = true;
                                    btnPrdTypeR2C4.Background = System.Windows.Media.Brushes.Orange;
                                    btnPrdTypeR2C4.ToolTip = btnPrdTypeR2C4Content.Text;
                                    break;
                                case 10:
                                    btnPrdTypeR2C5Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR2C5.Visibility = Visibility.Visible;
                                    btnPrdTypeR2C5.IsEnabled = true;
                                    btnPrdTypeR2C5.Background = System.Windows.Media.Brushes.OrangeRed;
                                    btnPrdTypeR2C5.ToolTip = btnPrdTypeR2C5Content.Text;
                                    break;
                                case 11:
                                    btnPrdTypeR2C6Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR2C6.Visibility = Visibility.Visible;
                                    btnPrdTypeR2C6.IsEnabled = true;
                                    btnPrdTypeR2C6.Background = System.Windows.Media.Brushes.DarkOrange;
                                    btnPrdTypeR2C6.ToolTip = btnPrdTypeR2C6Content.Text;
                                    break;
                                case 12:
                                    btnPrdTypeR3C1Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR3C1.Visibility = Visibility.Visible;
                                    btnPrdTypeR3C1.IsEnabled = true;
                                    btnPrdTypeR3C1.Background = System.Windows.Media.Brushes.DarkViolet;
                                    btnPrdTypeR3C1.ToolTip = btnPrdTypeR3C1Content.Text;
                                    break;
                                case 13:
                                    btnPrdTypeR3C2Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR3C2.Visibility = Visibility.Visible;
                                    btnPrdTypeR3C2.IsEnabled = true;
                                    btnPrdTypeR3C2.Background = System.Windows.Media.Brushes.Chocolate;
                                    btnPrdTypeR3C2.ToolTip = btnPrdTypeR3C2Content.Text;
                                    break;
                                case 14:
                                    btnPrdTypeR3C3Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR3C3.Visibility = Visibility.Visible;
                                    btnPrdTypeR3C3.IsEnabled = true;
                                    btnPrdTypeR3C3.Background = System.Windows.Media.Brushes.CornflowerBlue;
                                    btnPrdTypeR3C3.ToolTip = btnPrdTypeR3C3Content.Text;
                                    break;
                                case 15:
                                    btnPrdTypeR3C4Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR3C4.Visibility = Visibility.Visible;
                                    btnPrdTypeR3C4.IsEnabled = true;
                                    btnPrdTypeR3C4.Background = System.Windows.Media.Brushes.DarkViolet;
                                    btnPrdTypeR3C4.ToolTip = btnPrdTypeR3C4Content.Text;
                                    break;
                                case 16:
                                    btnPrdTypeR3C5Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR3C5.Visibility = Visibility.Visible;
                                    btnPrdTypeR3C5.IsEnabled = true;
                                    btnPrdTypeR3C5.Background = System.Windows.Media.Brushes.Chocolate;
                                    btnPrdTypeR3C5.ToolTip = btnPrdTypeR3C5Content.Text;
                                    break;
                                case 17:
                                    btnPrdTypeR3C6Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR3C6.Visibility = Visibility.Visible;
                                    btnPrdTypeR3C6.IsEnabled = true;
                                    btnPrdTypeR3C6.Background = System.Windows.Media.Brushes.CornflowerBlue;
                                    btnPrdTypeR3C6.ToolTip = btnPrdTypeR3C6Content.Text;
                                    break;
                                case 18:
                                    btnPrdTypeR4C1Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR4C1.Visibility = Visibility.Visible;
                                    btnPrdTypeR4C1.IsEnabled = true;
                                    btnPrdTypeR4C1.Background = System.Windows.Media.Brushes.DarkViolet;
                                    btnPrdTypeR4C1.ToolTip = btnPrdTypeR4C1Content.Text;
                                    break;
                                case 19:
                                    btnPrdTypeR4C2Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR4C2.Visibility = Visibility.Visible;
                                    btnPrdTypeR4C2.IsEnabled = true;
                                    btnPrdTypeR4C2.Background = System.Windows.Media.Brushes.Orange;
                                    btnPrdTypeR4C2.ToolTip = btnPrdTypeR4C2Content.Text;
                                    break;
                                case 20:
                                    btnPrdTypeR4C3Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR4C3.Visibility = Visibility.Visible;
                                    btnPrdTypeR4C3.IsEnabled = true;
                                    btnPrdTypeR4C3.Background = System.Windows.Media.Brushes.OrangeRed;
                                    btnPrdTypeR4C3.ToolTip = btnPrdTypeR4C3Content.Text;
                                    break;
                                case 21:
                                    btnPrdTypeR4C4Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR4C4.Visibility = Visibility.Visible;
                                    btnPrdTypeR4C4.IsEnabled = true;
                                    btnPrdTypeR4C4.Background = System.Windows.Media.Brushes.DarkOrange;
                                    btnPrdTypeR4C4.ToolTip = btnPrdTypeR4C4Content.Text;
                                    break;
                                case 22:
                                    btnPrdTypeR4C5Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR4C5.Visibility = Visibility.Visible;
                                    btnPrdTypeR4C5.IsEnabled = true;
                                    btnPrdTypeR4C5.Background = System.Windows.Media.Brushes.CornflowerBlue;
                                    btnPrdTypeR4C5.ToolTip = btnPrdTypeR4C5Content.Text;
                                    break;
                                case 23:
                                    btnPrdTypeR4C6Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR4C6.Visibility = Visibility.Visible;
                                    btnPrdTypeR4C6.IsEnabled = true;
                                    btnPrdTypeR4C6.Background = System.Windows.Media.Brushes.Chocolate;
                                    btnPrdTypeR4C6.ToolTip = btnPrdTypeR4C6Content.Text;
                                    break;
                                case 24:
                                    btnPrdTypeR5C1Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR5C1.Visibility = Visibility.Visible;
                                    btnPrdTypeR5C1.IsEnabled = true;
                                    btnPrdTypeR5C1.Background = System.Windows.Media.Brushes.DarkViolet;
                                    btnPrdTypeR5C1.ToolTip = btnPrdTypeR5C1Content.Text;
                                    break;
                                case 25:
                                    btnPrdTypeR5C2Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR5C2.Visibility = Visibility.Visible;
                                    btnPrdTypeR5C2.IsEnabled = true;
                                    btnPrdTypeR5C2.Background = System.Windows.Media.Brushes.Violet;
                                    btnPrdTypeR5C2.ToolTip = btnPrdTypeR5C2Content.Text;
                                    break;
                                case 26:
                                    btnPrdTypeR5C3Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR5C3.Visibility = Visibility.Visible;
                                    btnPrdTypeR5C3.IsEnabled = true;
                                    btnPrdTypeR5C3.Background = System.Windows.Media.Brushes.DarkSeaGreen;
                                    btnPrdTypeR5C3.ToolTip = btnPrdTypeR5C3Content.Text;
                                    break;
                                case 27:
                                    btnPrdTypeR5C4Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR5C4.Visibility = Visibility.Visible;
                                    btnPrdTypeR5C4.IsEnabled = true;
                                    btnPrdTypeR5C4.Background = System.Windows.Media.Brushes.Green;
                                    btnPrdTypeR5C4.ToolTip = btnPrdTypeR5C4Content.Text;
                                    break;
                                case 28:
                                    btnPrdTypeR5C5Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR5C5.Visibility = Visibility.Visible;
                                    btnPrdTypeR5C5.IsEnabled = true;
                                    btnPrdTypeR5C5.Background = System.Windows.Media.Brushes.DarkViolet;
                                    btnPrdTypeR5C5.ToolTip = btnPrdTypeR5C5Content.Text;
                                    break;
                                case 29:
                                    btnPrdTypeR5C6Content.Text = ds.Tables[0].Rows[i]["ProductTypeName"].ToString();
                                    btnPrdTypeR5C6.Visibility = Visibility.Visible;
                                    btnPrdTypeR5C6.IsEnabled = true;
                                    btnPrdTypeR5C6.Background = System.Windows.Media.Brushes.Orange;
                                    btnPrdTypeR5C6.ToolTip = btnPrdTypeR5C6Content.Text;
                                    break;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("No PRODUCT TYPES / PRODUCTS Defined in Settings!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("No PRODUCT TYPES / PRODUCTS Defined in Settings!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            else
            {
                MessageBox.Show("No PRODUCT TYPES / PRODUCTS Defined in Settings!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnClearLastItem_Click(object sender, RoutedEventArgs e)
        {
            Console.Beep(4000, 100);
            Flag = _CashOrQty.None;
            ClearLastItem();
        }

        private void ClearLastItem(bool _fromKeyPadEnter = false, bool _selItem = false)
        {
            int i = -1;
            bool _isLastItemAddSale = false;
            if (!_fromKeyPadEnter)
            {
                if (!_selItem)
                {
                    i = lviewInvoice.Items.Count - 1;
                }
                else
                {
                    i = lviewInvoice.SelectedIndex;
                }
            }
            else
            {
                i = lviewInvoice.SelectedIndex;
            }
            if (i >= 0)
            {
                myItem invItem = new myItem();
                invItem = (myItem)lviewInvoice.Items[i];
                if (invItem.Product.ToString().ToUpper().Trim() == "ADD SALE" )
                {
                    _isLastItemAddSale = true;
                }
                else
                {
                    _isLastItemAddSale = false;
                }

                //if (_fromKeyPadEnter && _isLastItemAddSale)
                //    return;
                if (_fromKeyPadEnter && _isDiscountApplied)
                    return;

                if (_fromKeyPadEnter && Flag == _CashOrQty.Quantity)
                {
                    if (invItem.currStock < Convert.ToInt32(txtQuantity.Text) && invItem.currStock > 0)
                    {
                        MessageBox.Show("Stock is LESS than Quantity Requested! <OR> Product Out-Of-Stock!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }

                if (!_fromKeyPadEnter)
                {
                    Flag = _CashOrQty.None;
                    txtQuantity.Text = "";
                    if (Convert.ToDecimal(txtCard.Text) >= Convert.ToDecimal(txtTotalSaleAmt.Text))
                    {
                        txtPaid.Text = "0.00";
                        txtCard.Text = "0.00";
                    }
                }

                if (lviewInvoice.Items.Count == 1)
                {
                    txtTotalSaleAmt.Text = "0.00";
                    if (!_fromKeyPadEnter)
                    {
                        if (Convert.ToDecimal(txtCard.Text) >= Convert.ToDecimal(txtTotalSaleAmt.Text))
                        {
                            txtPaid.Text = "0.00";
                            txtCard.Text = "0.00";
                        }
                    }
                    invItem = new myItem();
                    invItem = (myItem)lviewInvoice.Items[i];
                    if (invItem.Product.ToUpper().Trim() == "ADD SALE")
                    {
                        _isAddSale = false;
                    }
                }
                else
                {
                    invItem = new myItem();
                    invItem = (myItem)lviewInvoice.Items[i];
                    txtTotalSaleAmt.Text = (Convert.ToDecimal(txtTotalSaleAmt.Text) - Convert.ToDecimal(invItem.Total)).ToString();

                    if (invItem.Product.ToUpper().Trim() == "ADD SALE" && !_fromKeyPadEnter)
                    {
                        _isAddSale = false;
                    }
                }
                //lviewInvoice.Items.Remove(lviewInvoice.Items[i]);

                if (!_fromKeyPadEnter)
                {
                    cmbProduct.SelectedIndex = -1;
                    lviewInvoice.Items.RemoveAt(i);
                }
                else
                {
                    object _UnitPrice = invItem.UnitPrice;
                    ProdTotalPrice = Convert.ToString(Convert.ToDecimal(_UnitPrice) * (Convert.ToInt32(txtQuantity.Text)));
                    int _currStock = invItem.currStock;
                    int _productID = invItem.ProductID;
                    string _productName = invItem.Product;
                    if (txtQuantity.Text != "")
                    {
                        if (_currStock < Convert.ToInt32(txtQuantity.Text))
                        {
                            if (_currStock <= 0)
                            {
                                MessageBox.Show("Product OUT-OF-STOCK !", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                            else
                            {
                                MessageBox.Show("Stock is LESS than Quantity Requested !", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                            return;
                        }
                    }
                    lviewInvoice.Items.RemoveAt(i);
                    myItem invItem1 = new myItem { UnitPrice = _UnitPrice.ToString(), Product = _productName.ToString(), Quantity = txtQuantity.Text, CurrSym = "$", Total = (ProdTotalPrice.ToString()), ProductID = _productID, currStock = _currStock };
                    AddItemAndCalculateTotal(invItem1, i);

                    if (Flag == _CashOrQty.Cash)
                    {
                        txtQuantity.Text = "0";
                    }
                    else if (Flag == _CashOrQty.Quantity)
                    {
                        txtQuantity.Text = "1";
                    }
                }
                lviewInvoice.Items.Refresh();
            }
        }

        private void chkPrintReceipt_Checked(object sender, RoutedEventArgs e)
        {
            _isPrintReceipt = true;
        }
        private void chkPrintReceipt_UnChecked(object sender, RoutedEventArgs e)
        {
            _isPrintReceipt = false;
        }
        private int RAISE_POS_INVOICE()
        {
            try
            {
                int _memberInvoiceNo = GetMembershipInvoiceNo();
                if (_memberInvoiceNo != -1)
                {
                    DateTime _currentDate = DateTime.Now;

                    decimal _invoiceAmount = Convert.ToDecimal(txtTotalSaleAmt.Text.ToString());
                    decimal _amountPaid = 0m;
                    decimal _amountOutStanding = Convert.ToDecimal(_invoiceAmount - _amountPaid);

                    decimal _Cash = Convert.ToDecimal(txtPaid.Text);
                    decimal _Card = Convert.ToDecimal(txtCard.Text);
                    decimal _Change = Convert.ToDecimal(txtBalance.Text);
                    decimal _InvAmtExcDiscount = Convert.ToDecimal(txtDiscount.Text);
                    if (_discountPercent < 0) _discountPercent = 0;

                    int _invoiceMemberId = Convert.ToInt32(txtMakeaSaleInSoldTo1.Text.ToString().Trim());
                    string _invoiceStatus = "Paid";
                    _PointofSaleBAL.raise_POS_Invoice
                                                (_currentDate, _memberInvoiceNo, _invoiceAmount, _amountPaid, _amountOutStanding, 
                                                    _invoiceMemberId, _invoiceStatus, _AutoGenerateInvoiceNo, _Cash, _Card, _Change
                                                    , _InvAmtExcDiscount, _discountPercent
                                                    , _discountDollar
                                                );
                    return _memberInvoiceNo;
                }
                return -1;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString(), this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return -1;
            }
        }
        private void RAISE_POS_INVOICEDETAILS(int _InvNo)
        {
            DataTable _POSInvoiceTable = new DataTable("POSInvoiceDetails");

            _POSInvoiceTable.Columns.Add("InvoiceNo", typeof(int));
            _POSInvoiceTable.Columns.Add("MemberNo", typeof(int));
            _POSInvoiceTable.Columns.Add("InvoiceDetails", typeof(string));
            _POSInvoiceTable.Columns.Add("Amount", typeof(decimal));
            _POSInvoiceTable.Columns.Add("Status", typeof(string));
            _POSInvoiceTable.Columns.Add("ProdSale", typeof(char));
            _POSInvoiceTable.Columns.Add("ProdID", typeof(int));
            _POSInvoiceTable.Columns.Add("ProdQty", typeof(int));

            for (int i = 0; i < lviewInvoice.Items.Count; i++)
            {
                DataRow dR = _POSInvoiceTable.NewRow();

                myItem _item = new myItem();
                _item = (myItem)lviewInvoice.Items[i];

                dR["InvoiceNo"] = _InvNo;
                dR["MemberNo"] = Convert.ToInt32(txtMakeaSaleInSoldTo1.Text.ToString());
                dR["InvoiceDetails"] = _item.Product.ToString();
                dR["Amount"] =  Convert.ToDecimal(_item.Total.ToString());
                //dR["Status"] = "Pending";
                dR["Status"] = "Paid";
                dR["ProdSale"] = 'Y';
                dR["ProdID"] = Convert.ToInt32(_item.ProductID.ToString());
                dR["ProdQty"] = Convert.ToInt32(_item.Quantity.ToString());

                _POSInvoiceTable.Rows.Add(dR);
            }
            _PointofSaleBAL.SavePOSInvoiceDetails(_POSInvoiceTable);
        }
        private int GetMembershipInvoiceNo()
        {
            try
            {
                int _invoiceNo;

                _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                DataSet ds = _memberDetailsBal.get_LastUsedNoByCompanySite(_AutoGenerateInvoiceNo, _CompanyID, _SiteID);
                if (ds != null)
                {
                    _invoiceNo = Convert.ToInt32(ds.Tables[0].Rows[0]["LastUsedNo"].ToString().Trim());
                    return _invoiceNo;
                }
                else
                {
                    System.Windows.MessageBox.Show("No Data! (Invoice Number AutoGenerated)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return -1;
                }
            }
            catch (Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return -1;
            }
        }
        private void MakeASaleClear()
        {
            Console.Beep(4000, 100);
            txtMakeaSaleSoldTo.Text = "NON-MEMBER";
            txtMakeaSaleInSoldTo1.Text = "0";
            txtMakeaSaleSearch.Text = "";
            gvMakeaSaleMember.SelectedIndex = -1;
        }

        public void SetAccessRightsDS(DataSet dS)
        {
            _dsAccessRights = dS;
        }


        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        
        private void btnPOS_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            POS _POS = new POS();
            _POS.SetAccessRightsDS(_dsAccessRights);
            this.Cursor = Cursors.Arrow;
            _POS._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _POS._CompanyName = this._CompanyName;
            _POS._SiteName = this._SiteName;
            _POS._UserName = this._UserName;
            _POS._StaffPic = this._StaffPic;
            _POS.StaffImage.Source = this.StaffImage.Source;
            _POS.Owner = this.Owner;
            this.Close();
            _POS.ShowDialog();
        }


        private void btnAddNewStock_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            AddNewStock _AddNewStock = new AddNewStock();
            _AddNewStock.SetAccessRightsDS(_dsAccessRights);
            this.Cursor = Cursors.Arrow;
            _AddNewStock._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _AddNewStock._CompanyName = this._CompanyName;
            _AddNewStock._SiteName = this._SiteName;
            _AddNewStock._UserName = this._UserName;
            _AddNewStock._StaffPic = this._StaffPic;
            _AddNewStock.StaffImage.Source = this.StaffImage.Source;
            _AddNewStock.Owner = this.Owner;
            this.Close();
            _AddNewStock.ShowDialog();
        }

        private void btnPOSMakeASale_Click(object sender, RoutedEventArgs e)
        {
            //SAME WINDOW; NO ACTION;
        }

        private void btnViewInventory_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            ViewInventory vI = new ViewInventory();
            vI.SetAccessRightsDS(_dsAccessRights);
            this.Cursor = Cursors.Arrow;
            vI._CurrentLoginDateTime = this._CurrentLoginDateTime;
            vI._CompanyName = this._CompanyName;
            vI._SiteName = this._SiteName;
            vI._UserName = this._UserName;
            vI._StaffPic = this._StaffPic;
            vI.StaffImage.Source = this.StaffImage.Source;
            vI.Owner = this.Owner;
            this.Close();
            vI.ShowDialog();
        }

        private void btnDoaStocktake_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            DoaStocktake dStock = new DoaStocktake();
            dStock.SetAccessRightsDS(_dsAccessRights);
            this.Cursor = Cursors.Arrow;
            dStock._CurrentLoginDateTime = this._CurrentLoginDateTime;
            dStock._CompanyName = this._CompanyName;
            dStock._SiteName = this._SiteName;
            dStock._UserName = this._UserName;
            dStock._StaffPic = this._StaffPic;
            dStock.StaffImage.Source = this.StaffImage.Source;
            dStock.Owner = this.Owner;
            this.Close();
            dStock.ShowDialog();
        }

        private void btnBackToProdType_Click(object sender, RoutedEventArgs e)
        {
            Console.Beep(4000, 100);
            gbMakeaSaleProductToSelect.Visibility = Visibility.Hidden;
            gbMakeaSaleProducts.Visibility = Visibility.Visible;
        }

        private void btnMakeaSalePrintReceipt_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void ItemClear()
        {
            cmbProduct.ItemsSource = null;
            cmbProduct.Items.Clear();
            lviewInvoice.Items.Clear();
            txtTotalSaleAmt.Text = "0.00";
            txtPaid.Text = "0.00";
            txtCard.Text = "0.00";

            txtDiscount.Text = "0.00";
            lblDiscount.Visibility = Visibility.Collapsed;
            txtDiscount.Visibility = Visibility.Collapsed;
            gbMakeaSaleProducts.IsEnabled = true;
            gbMakeaSaleProductToSelect.IsEnabled = true;

            btnMakeaSaleDiscount.Content = "Disc. %";
            btnMakeaSaleDiscount.FontSize = 17;
            btnMakeaSaleDiscountDollar.Content = "Disc. $";
            btnMakeaSaleDiscountDollar.FontSize = 17;

            btnMakeaSaleAddsale.IsEnabled = true;
            btnMakeaSaleAddsale.Opacity = 1.0;
            btnMakeaSaleDiscount.IsEnabled = true;
            btnMakeaSaleDiscount.Opacity = 1.0;
            btnMakeaSaleDiscountDollar.IsEnabled = true;
            btnMakeaSaleDiscountDollar.Opacity = 1.0;

            Flag = _CashOrQty.Quantity;

            if (Flag == _CashOrQty.Cash)
            {
                txtQuantity.Text = "0";
            }
            else if (Flag == _CashOrQty.Quantity)
            {
                txtQuantity.Text = "0";
            }
            txtQuantity.CaretIndex = txtQuantity.Text.Length;
            btnQuantity.Background = Brushes.DarkGreen;
            btnCard.Background = Brushes.LightGreen;
            btnCash.Background = Brushes.LightGreen;

            grdCalculation.IsEnabled = false;
            _isDiscountApplied = false;
            _isAddSale = false;
            _discountPercent = 0;
        }

        private void btnCustomerSearch_Click(object sender, RoutedEventArgs e)
        {
            Console.Beep(4000, 100);
            bdCalculation.Visibility = Visibility.Hidden;
            _isViewInCalc = false;
            gbgvMakeaSaleMember.Visibility = Visibility.Visible;
        }

        private void btnOne_Click(object sender, RoutedEventArgs e)
        {
            _onKeyPadPress(1);
        }

        private void btnTwo_Click(object sender, RoutedEventArgs e)
        {
            _onKeyPadPress(2);
        }

        private void txtQuantity_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (_WindowLoaded)
            {
                //if ((Flag == _CashOrQty.Cash) || (Flag == _CashOrQty.Card))
                if (Flag == _CashOrQty.Cash)
                {
                    double _Change, _Paid, _Card;
                    _Card = Convert.ToDouble(txtCard.Text);
                    if (_Card >= Convert.ToDouble(txtTotalSaleAmt.Text))
                    {
                        txtPaid.Text = "0.00";
                        txtQuantity.Text = "0";
                        return;
                    }
                    if (txtQuantity.Text.Trim() == "")
                    {
                        _Paid = 0;
                    }
                    else
                    {
                        _Paid = Convert.ToDouble(txtQuantity.Text);
                    }

                    if (_Paid >= Convert.ToDouble(txtTotalSaleAmt.Text))
                    {
                        txtCard.Text = "0.00";
                        _Card = 0;
                    }
                    _Change = (_Paid + _Card) - Convert.ToDouble(txtTotalSaleAmt.Text);

                    txtPaid.Text = _Paid.ToString("#####0.#0");
                    if (_Change >= 0)
                    {
                        if (_Card >= Convert.ToDouble(txtTotalSaleAmt.Text))
                        {
                            _Change = 0;
                        }
                        txtBalance.Text = Convert.ToDouble(_Change).ToString("#####0.#0");
                        txtDue.Text = 0.ToString("#####0.#0");
                        txtDue.Foreground = Brushes.Black;
                        txtBalance.Foreground = Brushes.Red;
                    }
                    else
                    {
                        _Change = _Change * -1;
                        txtDue.Text = Convert.ToDouble(_Change).ToString("#####0.#0");
                        txtBalance.Text = 0.ToString("#####0.#0");
                        txtDue.Foreground = Brushes.Red;
                        txtBalance.Foreground = Brushes.Black;
                    }
                }
                else if (Flag == _CashOrQty.Card)
                {
                    double _Change, _Paid, _Card;
                    _Paid = Convert.ToDouble(txtPaid.Text);

                    if (txtQuantity.Text.Trim() == "")
                    {
                        _Card = 0;
                    }
                    else
                    {
                        _Card = Convert.ToDouble(txtQuantity.Text);
                    }

                    if (_Card <= Convert.ToDouble(txtTotalSaleAmt.Text))
                    {
                        _Change = (_Card + _Paid) - Convert.ToDouble(txtTotalSaleAmt.Text);
                    }
                    else
                    {
                        _Change = 0;
                        txtPaid.Text = "0.00";
                    }
                    txtCard.Text = _Card.ToString("#####0.#0");

                    if (_Change >= 0)
                    {
                        txtBalance.Text = Convert.ToDouble(_Change).ToString("#####0.#0");
                        txtDue.Text = 0.ToString("#####0.#0");
                        txtDue.Foreground = Brushes.Black;
                        txtBalance.Foreground = Brushes.Red;
                    }
                    else
                    {
                        _Change = _Change * -1;
                        txtDue.Text = Convert.ToDouble(_Change).ToString("#####0.#0");
                        txtBalance.Text = 0.ToString("#####0.#0");
                        txtDue.Foreground = Brushes.Red;
                        txtBalance.Foreground = Brushes.Black;
                    }
                }
            }
        }

        private void btnThree_Click(object sender, RoutedEventArgs e)
        {
            _onKeyPadPress(3);
        }

        private void btnFour_Click(object sender, RoutedEventArgs e)
        {
            _onKeyPadPress(4);
        }

        private void btnFive_Click(object sender, RoutedEventArgs e)
        {
            _onKeyPadPress(5);
        }

        private void btnSix_Click(object sender, RoutedEventArgs e)
        {
            _onKeyPadPress(6);
        }

        private void btnSeven_Click(object sender, RoutedEventArgs e)
        {
            _onKeyPadPress(7);
        }

        private void btnEight_Click(object sender, RoutedEventArgs e)
        {
            _onKeyPadPress(8);
        }

        private void btnNine_Click(object sender, RoutedEventArgs e)
        {
            _onKeyPadPress(9);
        }

        private void btnClearQty_Click(object sender, RoutedEventArgs e)
        {
            Console.Beep(4000, 100);
            txtQuantity.Text = "0";
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Console.Beep(4000, 100);
            if ((txtQuantity.Text != "0") && (txtQuantity.Text != ""))
            {
                txtQuantity.Text = txtQuantity.Text.Substring(0, txtQuantity.Text.Length - 1);
                if (txtQuantity.Text == "")
                {
                    txtQuantity.Text = "0";
                }
                if (txtQuantity.Text.Substring(txtQuantity.Text.Length - 1, 1) == ".")
                {
                    txtQuantity.Text = txtQuantity.Text.Remove(txtQuantity.Text.Length - 1);
                }
            }
            else
            {
                if (txtQuantity.Text == "")
                {
                    txtQuantity.Text = "0";
                }
            }
        }

        private void btnDot_Click(object sender, RoutedEventArgs e)
        {
            if ((Flag == _CashOrQty.Cash) || (Flag == _CashOrQty.Card))
            {
                Console.Beep(4000, 100);
                if (!txtQuantity.Text.Contains("."))
                {
                    txtQuantity.Text = txtQuantity.Text + '.';
                }
            }
            else
            {
                Console.Beep(4000, 100);
                Console.Beep(4000, 100);
            }
        }

        private void btnQuantity_Click(object sender, RoutedEventArgs e)
        {
            Console.Beep(4000, 100);
            btnCash.Background = Brushes.LightGreen;
            btnQuantity.Background = Brushes.DarkGreen;
            btnCard.Background = Brushes.LightGreen;
            Flag = _CashOrQty.Quantity;
            //txtQuantity.Text = "1";
            txtQuantity.Text = "";
        }

        private void btnZero_Click(object sender, RoutedEventArgs e)
        {
            _onKeyPadPress(0);
        }

        private void txtQuantity_KeyDown(object sender, KeyEventArgs e)
        {
            if (_isViewInCalc)
            {
                switch (e.Key)
                {
                    case Key.OemPeriod:
                    case Key.Decimal:
                        btnDot_Click(sender, e);
                        break;
                    case Key.NumPad0:
                    case Key.D0:
                        btnZero_Click(sender, e);
                        break;
                    case Key.NumPad1:
                    case Key.D1:
                        btnOne_Click(sender, e);
                        break;
                    case Key.NumPad2:
                    case Key.D2:
                        btnTwo_Click(sender, e);
                        break;
                    case Key.NumPad3:
                    case Key.D3:
                        btnThree_Click(sender, e);
                        break;
                    case Key.NumPad4:
                    case Key.D4:
                        btnFour_Click(sender, e);
                        break;
                    case Key.NumPad5:
                    case Key.D5:
                        btnFive_Click(sender, e);
                        break;
                    case Key.NumPad6:
                    case Key.D6:
                        btnSix_Click(sender, e);
                        break;
                    case Key.NumPad7:
                    case Key.D7:
                        btnSeven_Click(sender, e);
                        break;
                    case Key.NumPad8:
                    case Key.D8:
                        btnEight_Click(sender, e);
                        break;
                    case Key.NumPad9:
                    case Key.D9:
                        btnNine_Click(sender, e);
                        break;
                    case Key.Back:
                        btnDelete_Click(sender, e);
                        break;
                    case Key.Enter:
                    case Key.LineFeed:
                        btnEnter.Focus();
                        btnEnter_Click(sender, e);
                        break;
                    default:
                        break;
                }
            }
        }

        private void btnBackToCalc_Click(object sender, RoutedEventArgs e)
        {
            Console.Beep(4000, 100);
            gbgvMakeaSaleMember.Visibility = Visibility.Hidden;
            bdCalculation.Visibility = Visibility.Visible;
            _isViewInCalc = true;
        }

        private void txtTotalSaleAmt_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (_WindowLoaded)
            {
                double _Change, _Paid, _Card;

                if (txtPaid.Text.Trim() == "")
                    _Paid = 0;
                else
                    _Paid = Convert.ToDouble(txtPaid.Text);

                _Card = Convert.ToDouble(txtCard.Text);
                _Change = (_Paid + _Card) - Convert.ToDouble(txtTotalSaleAmt.Text);

                if (_Card >= Convert.ToDouble(txtTotalSaleAmt.Text))
                    _Change = 0;

                if (_Change >= 0)
                {
                    txtBalance.Text = Convert.ToDouble(_Change).ToString("#####0.#0");
                    txtDue.Text = 0.ToString("#####0.#0");
                    txtBalance.Foreground = Brushes.Red;
                    txtDue.Foreground = Brushes.Black;
                }
                else
                {
                    _Change = _Change * -1;
                    txtDue.Text = Convert.ToDouble(_Change).ToString("#####0.#0");
                    txtBalance.Text = 0.ToString("#####0.#0");
                    txtBalance.Foreground = Brushes.Black;
                    txtDue.Foreground = Brushes.Red;
                }
                if (!_isDiscountApplied)
                {
                    txtDiscount.Text = txtTotalSaleAmt.Text;
                }
            }
        }

        private void btnEnter_Click(object sender, RoutedEventArgs e)
        {
            if (!_isDiscountApplied)
            {
                if (Flag == _CashOrQty.Quantity)
                {
                    //if ((txtQuantity.Text.Trim() != "") && (txtQuantity.Text.Trim() != "0") && !_isQuantityDefault && cmbProduct.SelectedIndex != -1)
                    if ((txtQuantity.Text.Trim() != "") && (txtQuantity.Text.Trim() != "0") && !_isQuantityDefault )
                    {
                        ClearLastItem(true);
                        txtQuantity.Text = "";
                        _isQuantityDefault = true;
                    }
                }
            }
            else
            {
                Console.Beep(4000, 100);
                Console.Beep(4000, 100);
            }
        }

        private void txtMakeaSaleSoldTo_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (_WindowLoaded)
                lblMemberName.Content = txtMakeaSaleSoldTo.Text;
        }

        private void txtPaid_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            //if (_WindowLoaded)
            //{
            //    if (Flag == _CashOrQty.Card)
            //    {
            //        double _Change, _Paid;
            //        if (txtQuantity.Text.Trim() == "")
            //        {
            //            _Change = 0 - Convert.ToDouble(txtTotalSaleAmt.Text);
            //            _Paid = 0;
            //        }
            //        else
            //        {
            //            _Change = Convert.ToDouble(txtQuantity.Text) - Convert.ToDouble(txtTotalSaleAmt.Text);
            //            _Paid = Convert.ToDouble(txtQuantity.Text);
            //        }

            //        txtPaid.Text = _Paid.ToString("#####0.#0");
            //        if (_Change >= 0)
            //        {
            //            txtBalance.Text = Convert.ToDouble(_Change).ToString("#####0.#0");
            //            txtDue.Text = 0.ToString("#####0.#0");
            //            txtDue.Foreground = Brushes.Black;
            //        }
            //        else
            //        {
            //            _Change = _Change * -1;
            //            txtDue.Text = Convert.ToDouble(_Change).ToString("#####0.#0");
            //            txtBalance.Text = 0.ToString("#####0.#0");
            //            txtDue.Foreground = Brushes.Red;
            //        }
            //    }
            //}
        }

        private void btnCash_Click(object sender, RoutedEventArgs e)
        {
            Console.Beep(4000, 100);
            if (lviewInvoice.Items.Count > 0)
            {
                Flag = _CashOrQty.Cash;
                btnCash.Background = Brushes.DarkGreen;
                btnQuantity.Background = Brushes.LightGreen;
                btnCard.Background = Brushes.LightGreen;
                if (txtPaid.Text == "" || txtPaid.Text == "0.00")
                {
                    txtQuantity.Text = "0";
                }
                else
                {
                    txtQuantity.Text = txtPaid.Text;
                }
            }
        }

        private void btnCard_Click(object sender, RoutedEventArgs e)
        {
            Console.Beep(4000, 100);
            if (lviewInvoice.Items.Count > 0)
            {
                Flag = _CashOrQty.Card;
                btnCash.Background = Brushes.LightGreen;
                btnQuantity.Background = Brushes.LightGreen;
                btnCard.Background = Brushes.DarkGreen;
                if (txtCard.Text == "" || txtCard.Text == "0.00")
                {
                    txtQuantity.Text = "0";
                }
                else
                {
                    txtQuantity.Text = txtCard.Text;
                }
            }
        }

        private void btnMakeaSaleClockOut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //GAS - 27 POS Home Club : 19/SEP/2017
                if (ConfigurationManager.AppSettings["SuperAdmin"].ToString().ToUpper().Trim() != "YES")
                {
                    if
                        (
                            ConfigurationManager.AppSettings["LoggedInSiteID"].ToString().ToUpper().Trim() !=
                            ConfigurationManager.AppSettings["HomeClubSiteID"].ToString().ToUpper().Trim()
                        )
                    {
                        MessageBoxResult _HomeClubResult =
                        MessageBox.Show
                            (
                                "Your selected site is different to the Home site!" +
                                "\nWould you like to still continue with POS End-Of-Shift - Yes or No?" +
                                "\n\n(CLICK 'YES' TO PROCEED, 'NO' TO EXIT!)",
                                this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No
                            );
                        if (
                                _HomeClubResult == MessageBoxResult.No || _HomeClubResult == MessageBoxResult.Cancel
                           )
                        {
                            return;
                        }
                    }
                }
                //GAS - 27 POS Home Club : 19/SEP/2017

                ClockInCheck _ClockInCheck = new ClockInCheck();
                _ClockInCheck = Comman.Constants.POS_CheckClockIn(_ClockInCheck);

                if (!_ClockInCheck._clockedInAndClockedOut)
                {
                    Console.Beep(4000, 100);
                    //if (MessageBox.Show("Do you want to CLOCK-OUT Now?", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    //{
                    ////CLOCK-OUT END-OF-SHIFT; TO CAPTURE DENOMINATION & CALCULATE CLOSING BALANCE
                    EndOfShift _endOfShift = new EndOfShift();
                    _endOfShift.Owner = this;
                    _endOfShift.ShowDialog();
                    ////CLOCK-OUT END-OF-SHIFT; TO CAPTURE DENOMINATION & CALCULATE CLOSING BALANCE

                    //_strClockInBalance = Interaction.InputBox("Enter Closing Cash Balance : ", "POS - End Of Shift");

                    if ((_strClockInBalance == "") || (_strClockInBalance == null))
                    {
                        //MessageBox.Show("No Amount entered! (OR) Canceled!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    if (!Regex.IsMatch(_strClockInBalance, @"^\d{1,7}(\.\d{1,2})?$"))
                    {
                        MessageBox.Show("Amount should be Numeric! (upto 2 decimal places)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    _clockOutBalance = Convert.ToDecimal(_strClockInBalance);
                    if (_clockOutBalance < 0)
                    {
                        MessageBox.Show("Amount CANNOT be less than Zero!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    Comman.Constants._emailBeingSent = true;
                    int _result = ClockOut_User(_clockOutBalance);

                    if (_result != 0)
                    {
                        //PRINT REPORT HERE..
                        Comman.Constants._emailBeingSent = true;
                        InitializeDataGrid();
                        ShowPOSSaleReport(DateTime.Now, DateTime.Now);
                        if (_printGrid.Items.Count > 0)
                        {
                            if (_OpeningCashBalance.Trim() == "") _OpeningCashBalance = null;
                            if (_ClosingCashBalance.Trim() == "") _ClosingCashBalance = null;

                            Print _print = new Print(_printGrid, null, this, false, _CompanyName, _TotalCashAmt_s,
                                    null, _TotalCardAmt_s, _TotalPaidAmt_s, 0, true, _TotalSaleAmt_s, _OpeningCashBalance, _ClosingCashBalance, true, false);

                            this.Hide();
                            Mouse.OverrideCursor = Cursors.Wait;
                            PleaseWait _plsWait = new PleaseWait("Sending Email ... Please Wait...!");
                            _plsWait.Name = "PLEASEWAIT";
                            _plsWait.Show();
                            AddFooterToEmailReport();
                            CreateCSVandSendMail();
                            _plsWait.Close();
                            Mouse.OverrideCursor = null;
                            Comman.Constants._emailBeingSent = false;
                        }
                        else
                        {
                            this.Hide();
                            Mouse.OverrideCursor = Cursors.Wait;
                            PleaseWait _plsWait = new PleaseWait("Sending Email ... Please Wait...!");
                            _plsWait.Name = "PLEASEWAIT";
                            _plsWait.Show();
                            AddFooterToEmailReport();
                            CreateCSVandSendMail();
                            _plsWait.Close();
                            Mouse.OverrideCursor = null;
                            Comman.Constants._emailBeingSent = false;
                        }
                    }
                    else
                    {
                        Comman.Constants.AddToCommunicationLog(0, "INTERNAL", "*** NO REPORT DATA (ON CLOCK-OUT) ***", "REPORT CONTAINS 0 DATA (CLOCK-OUT RETURNED 0 ROWS!)", "NONE", 0);
                    }
                    Comman.Constants._emailBeingSent = false;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("End-Of-Shift has been completed for Today!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            catch(Exception ex1)
            {
                string _xMsg = ex1.Message;
                Comman.Constants.AddToCommunicationLog(0, "INTERNAL", "*** PREVIEW ERROR (ON EOS CLICK) ***", ex1.Message, "NONE", 0);
                Comman.Constants._emailBeingSent = false;
            }
        }
        void AddFooterToEmailReport()
        {
            try
            {
                if ((_OpeningCashBalance == null) || (_OpeningCashBalance == ""))
                {
                    _OpeningCashBalance = "0.00";
                }
                else
                {
                    _OpeningCashBalance = Convert.ToDecimal(_OpeningCashBalance).ToString("#0.#0");
                }
                if ((_ClosingCashBalance == null) || (_ClosingCashBalance == ""))
                {
                    _ClosingCashBalance = "0.00";
                }
                else
                {
                    _ClosingCashBalance = Convert.ToDecimal(_ClosingCashBalance).ToString("#0.#0");
                }
                _printGrid.CanUserAddRows = true;
                DataRow _dR = _dtEmailData.NewRow();
                _dR["SaleDate"] = "";
                _dR["Change"] = "";
                _dtEmailData.Rows.Add(_dR);

                _dR = _dtEmailData.NewRow();
                _dR["SaleDate"] = "Opening Balance (Cash) :";
                _dR["Change"] = _OpeningCashBalance;
                _dtEmailData.Rows.Add(_dR);

                _dR = _dtEmailData.NewRow();
                _dR["SaleDate"] = "Total Sale :";
                _dR["Change"] = _TotalSaleAmt_s;
                _dtEmailData.Rows.Add(_dR);

                _dR = _dtEmailData.NewRow();
                _dR["SaleDate"] = "Total Payment Received :";
                _dR["Change"] = _TotalPaidAmt_s;
                _dtEmailData.Rows.Add(_dR);

                _dR = _dtEmailData.NewRow();
                _dR["SaleDate"] = "Total Cash transaction :";
                _dR["Change"] = _TotalCashAmt_s;
                _dtEmailData.Rows.Add(_dR);

                _dR = _dtEmailData.NewRow();
                _dR["SaleDate"] = "Total Card transaction :";
                _dR["Change"] = _TotalCardAmt_s;
                _dtEmailData.Rows.Add(_dR);

                _dR = _dtEmailData.NewRow();
                _dR["SaleDate"] = "Closing Balance (Cash) :";
                _dR["Change"] = _ClosingCashBalance;
                _dtEmailData.Rows.Add(_dR);
            }
            catch(Exception ex)
            {
                Comman.Constants.AddToCommunicationLog(0, "INTERNAL", "*** EMAIL REPORT FOOTER ERROR ***", ex.Message, "NONE", 0);
                Comman.Constants._emailBeingSent = false;
            }
        }

        void CreateCSVandSendMail()
        {
            try
            {
                string _emailFrom = "";
                string _emailTo = ConfigurationManager.AppSettings["POSReportEmail"].ToString(); //"ramji@eluminaelearning.com.au";
                string _emailSubject = _CompanyName + " - *** POS End Of Shift Report *** ";
                _CompanyNameOnly = _CompanyName.Substring(0, _CompanyName.IndexOf("(") - 1);

                string _emailBody = "\nOrganization : " + _CompanyNameOnly
                                + "\nSite Name    : " + _SiteName
                                + "\n\nPOS End of Shift Report as of Date / Time: " + DateTime.Now.ToString("dd MMM yyyy / hh:mm tt")
                                + "\n\n\n*** This is an automated email from 'REDA' Management Software ***";

                if (true)
                {
                    string _filePath = "";
                    if (_printGrid.Items.Count > 0)
                    {
                    }
                    else
                    {
                        _emailSubject = _CompanyName + " - *** POS End Of Shift Report *** ";
                        _emailBody = "\nOrganization : " + _CompanyNameOnly
                                        + "\nSite Name    : " + _SiteName
                                        + "\n\nPOS End of Shift Report as of Date / Time: " + DateTime.Now.ToString("dd MMM yyyy / hh:mm tt")
                                        + "\n\n\n                      *** THERE ARE NO TRANSACTIONS FOR TODAY ***"
                                        + "\n\n\n*** This is an automated email from 'REDA' Management Software ***";
                    }

                    _printGrid.ItemsSource = _dtEmailData.DefaultView;
                    _filePath = Comman.Constants.AppPath + "\\" + DateTime.Now.ToString("yyyyMMddhhmmsstt") + "_POSEndOfShift.csv";
                    DataGrid dg = _printGrid;
                    dg.SelectAllCells();
                    dg.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
                    ApplicationCommands.Copy.Execute(null, dg);
                    dg.UnselectAllCells();
                    string Clipboardresult = (string)Clipboard.GetData(DataFormats.CommaSeparatedValue);
                    StreamWriter swObj = new StreamWriter(_filePath);
                    swObj.WriteLine(Clipboardresult);
                    swObj.Close();

                    if (_emailTo.Trim() == "")
                    {
                        _emailTo = Interaction.InputBox("Enter email-ID to send the report!", "POS Report");
                        if (_emailTo.Trim() != "")
                        {
                            sendEmail_POS(_emailFrom, _emailTo, _emailSubject, _emailBody, _filePath);
                        }
                        else
                        {
                            Comman.Constants.AddToCommunicationLog(0, "INTERNAL", "*** Email ADDRESS error ***","Email ID not available! or User input BLANK", "NONE", 0);
                        }
                    }
                    else
                    {
                        sendEmail_POS(_emailFrom, _emailTo, _emailSubject, _emailBody, _filePath);
                    }
                    
                    if (_filePath.Trim() != "")
                    {
                        File.Delete(_filePath);
                    }
                }
            }
            catch (Exception x)
            {
                string _msg = x.Message;
                MessageBox.Show("File Creation Error..!\n\n" + _msg, "End Of Shift", MessageBoxButton.OK, MessageBoxImage.Information);
                Comman.Constants.AddToCommunicationLog(0, "INTERNAL", "*** FILE (.CSV) CREATION ERROR ***", x.Message, "NONE", 0);
                Comman.Constants._emailBeingSent = false;
            }
        }

        public void sendEmail_POS(string strFrom, string strTo, string strSubject, string strBody, string _filePath)
        {
            try
            {
                client = new SmtpClient();
                client.UseDefaultCredentials = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                //////////////DO NOT CHANGE THE BELOW LINES //////////////
                client.Port = 587;
                client.Host = "SMTP.GMAIL.COM";
                string _strFrom = "noreply.reda@gmail.com";
                string _strDecryptPassWord = Comman.Constants.passwordDecrypt("fMKQ+IINHCVpOzskqoKffaT7hdtbE+xxX1g5Ybpw+6AATKV9AxcNhvAZ9rZL7uD/",
                                                        Comman.Constants._EncryptKey);
                if ((ConfigurationManager.AppSettings["REDAAutoEmailID"].ToString().Trim() != "") &&
                    (ConfigurationManager.AppSettings["REDAAutoEmailEncPwd"].ToString().Trim() != ""))
                {
                    _strFrom = ConfigurationManager.AppSettings["REDAAutoEmailID"].ToString().Trim();
                    _strDecryptPassWord = Comman.Constants.passwordDecrypt(ConfigurationManager.AppSettings["REDAAutoEmailEncPwd"].ToString(),
                                                            Comman.Constants._EncryptKey);
                }
                //////////////DO NOT CHANGE THE ABOVE LINES //////////////

                strFrom = _strFrom;
                
                //CHECK IF EMAIL ADDRESS FOR STARTING OR ENDING COMMAs
                if (strTo.Trim() != "")
                {
                    if (strTo.Trim().StartsWith(","))
                    {
                        strTo = strTo.Trim().Substring(1);
                    }
                    if (strTo.Trim().EndsWith(","))
                    {
                        strTo = strTo.Substring(0, strTo.Trim().Length - 1);
                    }
                }
                //CHECK IF EMAIL ADDRESS FOR STARTING OR ENDING COMMAs

                MailAddress _mFrom = new MailAddress(_strFrom, "POS End Of Shift Report");
                MailAddress _mTo = new MailAddress(strTo);

                mail = new MailMessage (_mFrom, _mTo);
                mail.To.Add(strTo);

                mail.IsBodyHtml = true;
                System.Net.NetworkCredential objSMTPUserInfo = new System.Net.NetworkCredential(_strFrom, _strDecryptPassWord);
                client.Credentials = objSMTPUserInfo;
                client.EnableSsl = true;
                mail.Subject = strSubject;
                if (mail.IsBodyHtml == true)
                {
                    strBody = strBody.Replace("\n", "<br>");
                }
                else
                {
                    strBody = strBody.Replace("<br>", "\n");
                }
                mail.Body = strBody;
                if (_filePath != "")
                {
                    Attachment _fileAttachment = new Attachment(_filePath);
                    mail.Attachments.Add(_fileAttachment);
                }
                client.Send(mail);
                //client.SendMailAsync(mail);

                mail.Dispose();
                Comman.Constants.AddToCommunicationLog(0, "INTERNAL", strSubject, strBody, strTo, 1);
                Comman.Constants._emailBeingSent = false;
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error Sending Email!\nDetails: " + ex.Message +"\n\nAttempting (thru REDA default email) again...","End Of Shift",MessageBoxButton.OK,MessageBoxImage.Error);
                mail.Dispose();

                try
                {
                    client = new SmtpClient();
                    client.UseDefaultCredentials = true;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    
                    //////////////DO NOT CHANGE THE BELOW LINES //////////////
                    client.Port = 587;
                    client.Host = "SMTP.GMAIL.COM";
                    string _strFrom = "noreply.reda@gmail.com";
                    string _strDecryptPassWord = Comman.Constants.passwordDecrypt("fMKQ+IINHCVpOzskqoKffaT7hdtbE+xxX1g5Ybpw+6AATKV9AxcNhvAZ9rZL7uD/",
                                                            Comman.Constants._EncryptKey);
                    if ((ConfigurationManager.AppSettings["REDAAutoEmailID"].ToString().Trim() != "") &&
                        (ConfigurationManager.AppSettings["REDAAutoEmailEncPwd"].ToString().Trim() != ""))
                    {
                        _strFrom = ConfigurationManager.AppSettings["REDAAutoEmailID"].ToString().Trim();
                        _strDecryptPassWord = Comman.Constants.passwordDecrypt(ConfigurationManager.AppSettings["REDAAutoEmailEncPwd"].ToString(),
                                                                Comman.Constants._EncryptKey);
                    }
                    //////////////DO NOT CHANGE THE ABOVE LINES //////////////

                    strFrom = _strFrom;
                    MailAddress _mFrom = new MailAddress(_strFrom, "POS End Of Shift Report");
                    MailAddress _mTo = new MailAddress(strTo);

                    mail = new MailMessage(_mFrom, _mTo);
                    mail.To.Add(strTo);

                    mail.IsBodyHtml = true;
                    System.Net.NetworkCredential objSMTPUserInfo = new System.Net.NetworkCredential(_strFrom, _strDecryptPassWord);
                    client.Credentials = objSMTPUserInfo;
                    client.EnableSsl = true;
                    mail.Subject = strSubject;
                    if (mail.IsBodyHtml == true)
                    {
                        strBody = strBody.Replace("\n", "<br>");
                    }
                    else
                    {
                        strBody = strBody.Replace("<br>", "\n");
                    }
                    mail.Body = strBody;
                    if (_filePath != "")
                    {
                        Attachment _fileAttachment = new Attachment(_filePath);
                        mail.Attachments.Add(_fileAttachment);
                    }
                    client.Send(mail);
                    //client.SendMailAsync(mail);

                    mail.Dispose();

                    Comman.Constants.AddToCommunicationLog(0, "INTERNAL", strSubject, strBody, strTo, 1);
                    Comman.Constants._emailBeingSent = false;
                }
                catch(Exception x)
                {
                    try
                    {
                        Comman.Constants.AddToCommunicationLog(0, "INTERNAL", "*** EMAIL ERROR ***", ex.Message, strTo, 0);
                        Comman.Constants._emailBeingSent = false;
                        mail.Dispose();
                        MessageBox.Show("Email Send Error..!\n\n" + x.Message, "End Of Shift", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    catch(Exception x1)
                    {
                        string s = x1.Message;
                        MessageBox.Show("Email Send Error..!\n\n" + x1.Message, "End Of Shift", MessageBoxButton.OK, MessageBoxImage.Information);
                        Comman.Constants._emailBeingSent = false;
                    }
                }
            }
        }

        void InitializeDataGrid()
        {
            _printGrid = new DataGrid();
            _printGrid.CanUserAddRows = false;

            DataGridTextColumn dataColumn = new DataGridTextColumn();
            dataColumn.Header = "Date";
            dataColumn.Binding = new Binding("SaleDate");
            _printGrid.Columns.Add(dataColumn);

            dataColumn = new DataGridTextColumn();
            dataColumn.Header = "Staff";
            dataColumn.Binding = new Binding("Staff");
            _printGrid.Columns.Add(dataColumn);

            dataColumn = new DataGridTextColumn();
            dataColumn.Header = "Sale Amt";
            //dataColumn.Binding = new Binding("SaleAmt");
            dataColumn.Binding = new Binding("SaleActAmt");
            _printGrid.Columns.Add(dataColumn);

            //<Discount> <"Disc.%">
            dataColumn = new DataGridTextColumn();
            dataColumn.Header = "Disc.%";
            dataColumn.Binding = new Binding("Discount");
            _printGrid.Columns.Add(dataColumn);

            dataColumn = new DataGridTextColumn();
            dataColumn.Header = "Disc.$";
            dataColumn.Binding = new Binding("DiscDollar");
            _printGrid.Columns.Add(dataColumn);

            dataColumn = new DataGridTextColumn();
            dataColumn.Header = "Paid Amt";
            dataColumn.Binding = new Binding("SalePAmt");
            _printGrid.Columns.Add(dataColumn);

            dataColumn = new DataGridTextColumn();
            dataColumn.Header = "InvNo";
            dataColumn.Binding = new Binding("SaleInvNo");
            dataColumn.Visibility = Visibility.Hidden;
            _printGrid.Columns.Add(dataColumn);

            dataColumn = new DataGridTextColumn();
            dataColumn.Header = "Pay Mode";
            dataColumn.Binding = new Binding("SalePMode");
            _printGrid.Columns.Add(dataColumn);

            dataColumn = new DataGridTextColumn();
            dataColumn.Header = "Cash";
            dataColumn.Binding = new Binding("Cash");
            _printGrid.Columns.Add(dataColumn);

            dataColumn = new DataGridTextColumn();
            dataColumn.Header = "Card";
            dataColumn.Binding = new Binding("Card");
            _printGrid.Columns.Add(dataColumn);

            dataColumn = new DataGridTextColumn();
            dataColumn.Header = "Change Given";
            dataColumn.Binding = new Binding("Change");
            _printGrid.Columns.Add(dataColumn);

            dataColumn = new DataGridTextColumn();
            dataColumn.Header = "Member Name";
            dataColumn.Binding = new Binding("SaleMember");
            _printGrid.Columns.Add(dataColumn);

            dataColumn = new DataGridTextColumn();
            dataColumn.Header = "Sale Description";
            dataColumn.Binding = new Binding("SaleDesc");
            dataColumn.Visibility = Visibility.Hidden;
            _printGrid.Columns.Add(dataColumn);
        }
        private void ShowPOSSaleReport(DateTime _fromDate, DateTime _toDate)
        {
            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
            
            //string _userName = ConfigurationManager.AppSettings["LoggedInUser"].ToString();
            //int _userLoginID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedUserLoginID"].ToString());

            string _userName = "";
            int _userLoginID = 0;
            //if (ConfigurationManager.AppSettings["isAdminUser"].ToString().Trim().ToUpper() == "YES")
            //{
            //    _userName = "";
            //    _userLoginID = 0;
            //}

            SqlHelper _sql = new SqlHelper();
            if (_sql._ConnOpen == 0) return;

            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            ht.Add("@FromDate", _fromDate);
            ht.Add("@ToDate", _toDate);
            ht.Add("@Staff", _userName);
            ht.Add("@StaffLoginID", _userLoginID);

            DataSet _dsPOS = _sql.ExecuteProcedure("POS_Sale_Report", ht, true);
            _printGrid.ItemsSource = null;

            if (_dsPOS != null)
            {
                if (_dsPOS.Tables.Count > 0)
                {
                    _dtEmailData = _dsPOS.Tables[_POSSummaryTable];
                    _TotalPaidAmt = 0;
                    _TotalCashAmt = 0;
                    _TotalCardAmt = 0;
                    _TotalSaleAmt = 0;

                    _TotalPaidAmt_s = _TotalPaidAmt.ToString("#0.#0");
                    _TotalCashAmt_s = _TotalCashAmt.ToString("#0.#0");
                    _TotalCardAmt_s = _TotalCardAmt.ToString("#0.#0");
                    _TotalSaleAmt_s = _TotalSaleAmt.ToString("#0.#0");

                    if (_dsPOS.Tables[_POSSummaryTable].DefaultView.Count > 0)
                    {
                        _printGrid.ItemsSource = _dsPOS.Tables[_POSSummaryTable].DefaultView;

                        DataTable _dtPOS = _dsPOS.Tables[_POSSummaryTable];
                        _TotalPaidAmt = 0;
                        _TotalCashAmt = 0;
                        _TotalCardAmt = 0;
                        _TotalSaleAmt = 0;

                        for (int i = 0; i < _dtPOS.DefaultView.Count; i++)
                        {
                            _TotalSaleAmt = _TotalSaleAmt + Convert.ToDecimal(_dtPOS.DefaultView[i]["SaleAmt"].ToString());
                            _TotalPaidAmt = _TotalPaidAmt + Convert.ToDecimal(_dtPOS.DefaultView[i]["SalePAmt"].ToString());
                            _TotalCashAmt = _TotalCashAmt + Convert.ToDecimal(_dtPOS.DefaultView[i]["Cash"].ToString()) - Convert.ToDecimal(_dtPOS.DefaultView[i]["Change"].ToString());
                            _TotalCardAmt = _TotalCardAmt + Convert.ToDecimal(_dtPOS.DefaultView[i]["Card"].ToString());
                        }
                        _TotalPaidAmt_s = _TotalPaidAmt.ToString("#0.#0");
                        _TotalCashAmt_s = _TotalCashAmt.ToString("#0.#0");
                        _TotalCardAmt_s = _TotalCardAmt.ToString("#0.#0");
                        _TotalSaleAmt_s = _TotalSaleAmt.ToString("#0.#0");
                    }

                    if (_dsPOS.Tables[_POSClockInOutTable].DefaultView.Count > 0)
                    {
                        _OpeningCashBalance = _dsPOS.Tables[_POSClockInOutTable].DefaultView[0]["OpeningBalance"].ToString();
                        _ClosingCashBalance = _dsPOS.Tables[_POSClockInOutTable].DefaultView[0]["ClosingBalance"].ToString();
                    }
                }
                else
                {
                    _dtEmailData = new DataTable();
                    _dtEmailData.Columns.Add("SaleDate", typeof(string));
                    _dtEmailData.Columns.Add("Change", typeof(string));
                }
            }
            else
            {
                _dtEmailData = new DataTable();
                _dtEmailData.Columns.Add("SaleDate", typeof(string));
                _dtEmailData.Columns.Add("Change", typeof(string));
            }
        }

        private void btnMakeaSaleAddsale_Click(object sender, RoutedEventArgs e)
        {
            if (!_isAddSale)
            {
                Console.Beep(4000, 100);
                AddSale _addSale = new AddSale();
                _addSale.Owner = this;
                _addSale.ShowDialog();

                if (_isAddSale)
                {
                    //ADD SALE ITEM
                    myItem invItem = new myItem {
                                                    UnitPrice = _addSaleUnitPrice.ToString("#0.#0"), Product = "Add Sale",
                                                    Quantity = _addSaleQty.ToString("#0"), CurrSym = "$", Total = _addSaleTotPrice.ToString("#0.#0"),
                                                    ProductID = -1, currStock = int.MaxValue
                                                };
                    AddItemAndCalculateTotal(invItem);

                    Flag = _CashOrQty.Quantity;
                    txtQuantity.CaretIndex = txtQuantity.Text.Length;
                    btnQuantity.Background = Brushes.DarkGreen;
                    btnCard.Background = Brushes.LightGreen;
                    btnCash.Background = Brushes.LightGreen;
                    grdCalculation.IsEnabled = true;

                    if (Flag == _CashOrQty.Cash)
                    {
                        txtQuantity.Text = "0";
                    }
                    else if (Flag == _CashOrQty.Quantity)
                    {
                        txtQuantity.Text = "1";
                        _isQuantityDefault = true;
                    }
                }
            }
            else
            {
                Console.Beep(4000, 100);
            }
        }

        private void btnMakeaSaleDiscount_Click(object sender, RoutedEventArgs e)
        {
            if (!_isDiscountApplied)
            {
                if (txtTotalSaleAmt.Text != "")
                {
                    if (Convert.ToDecimal(txtTotalSaleAmt.Text) > 0)
                    {
                        Console.Beep(4000, 100);
                        btnBackToProdType_Click(sender, e);

                        Discount _discount = new Discount();
                        _discount.Owner = this;
                        _discount.ShowDialog();
                        if ((_discountPercent > 0) && (_isDiscountApplied))
                        {
                            _isDiscPercent = true;
                            _isDiscDollar = false;
                            lblDiscount.Visibility = Visibility.Visible;
                            txtDiscount.Visibility = Visibility.Visible;
                            btnMakeaSaleDiscount.FontSize = 13;
                            btnMakeaSaleDiscount.Content = "Cancel Discount";

                            gbMakeaSaleProducts.IsEnabled = false;
                            gbMakeaSaleProductToSelect.IsEnabled = false;
                            btnClear.IsEnabled = false;
                            btnClearLastItem.IsEnabled = false;
                            btnClearSelItem.IsEnabled = false;
                            btnMakeaSaleAddsale.IsEnabled = false;
                            btnMakeaSaleAddsale.Opacity = 0.5;
                            btnMakeaSaleDiscountDollar.IsEnabled = false;
                            btnMakeaSaleDiscountDollar.Opacity = 0.5;

                            txtDiscount.Text = txtTotalSaleAmt.Text;
                            decimal _discountAmt = Convert.ToDecimal(txtTotalSaleAmt.Text);
                            decimal _percentUnit = (Convert.ToDecimal(_discountPercent) / 100m);
                            _discountAmt = _discountAmt - (_percentUnit * _discountAmt);
                            _discountAmt = Math.Round(_discountAmt, 2, MidpointRounding.AwayFromZero);
                            _discountAmt = Math.Round(_discountAmt, 1, MidpointRounding.AwayFromZero);

                            txtTotalSaleAmt.Text = _discountAmt.ToString("#0.#0");
                        }
                    }
                    else
                    {
                        Console.Beep(4000, 100);
                        Console.Beep(4000, 100);
                        btnMakeaSaleDiscountDollar.IsEnabled = true;
                        btnMakeaSaleDiscountDollar.Opacity = 1.0;
                    }
                }
                else
                {
                    Console.Beep(4000, 100);
                    Console.Beep(4000, 100);
                    btnMakeaSaleDiscountDollar.IsEnabled = true;
                    btnMakeaSaleDiscountDollar.Opacity = 1.0;
                }
            }
            else
            {
                Console.Beep(4000, 100);
                Console.Beep(4000, 100);
                _isDiscountApplied = false;
                _isDiscPercent = false;
                _isDiscDollar = false;

                lblDiscount.Visibility = Visibility.Collapsed;
                txtDiscount.Visibility = Visibility.Collapsed;
                btnMakeaSaleDiscount.FontSize = 17;
                btnMakeaSaleDiscount.Content = "Disc. %";
                txtTotalSaleAmt.Text = txtDiscount.Text;

                gbMakeaSaleProducts.IsEnabled = true;
                gbMakeaSaleProductToSelect.IsEnabled = true;
                btnClear.IsEnabled = true;
                btnClearLastItem.IsEnabled = true;
                btnClearSelItem.IsEnabled = true;
                btnMakeaSaleAddsale.IsEnabled = true;
                btnMakeaSaleAddsale.Opacity = 1.0;
                btnMakeaSaleDiscountDollar.IsEnabled = true;
                btnMakeaSaleDiscountDollar.Opacity = 1.0;
            }
        }

        private void btnMakeaSaleDiscountDollar_Click(object sender, RoutedEventArgs e)
        {
            if (!_isDiscountApplied)
            {
                if (txtTotalSaleAmt.Text != "")
                {
                    if (Convert.ToDecimal(txtTotalSaleAmt.Text) > 0)
                    {
                        Console.Beep(4000, 100);
                        DiscountDollar _discount = new DiscountDollar();
                        _discount.Owner = this;
                        _discount.ShowDialog();
                        if ((_discountDollar > 0m) && (_isDiscountApplied))
                        {
                            decimal _discountAmt = Convert.ToDecimal(txtTotalSaleAmt.Text);
                            _discountAmt = _discountAmt - _discountDollar;
                            if (_discountAmt >= 0m)
                            {
                                _isDiscPercent = false;
                                _isDiscDollar = true;
                                lblDiscount.Visibility = Visibility.Visible;
                                txtDiscount.Visibility = Visibility.Visible;
                                btnMakeaSaleDiscountDollar.FontSize = 13;
                                btnMakeaSaleDiscountDollar.Content = "Cancel Discount";

                                gbMakeaSaleProducts.IsEnabled = false;
                                gbMakeaSaleProductToSelect.IsEnabled = false;
                                btnClear.IsEnabled = false;
                                btnClearLastItem.IsEnabled = false;
                                btnClearSelItem.IsEnabled = false;

                                btnMakeaSaleAddsale.IsEnabled = false;
                                btnMakeaSaleAddsale.Opacity = 0.5;
                                btnMakeaSaleDiscount.IsEnabled = false;
                                btnMakeaSaleDiscount.Opacity = 0.5;

                                txtDiscount.Text = txtTotalSaleAmt.Text;

                                txtTotalSaleAmt.Text = _discountAmt.ToString("#0.#0");
                            }
                            else
                            {
                                Console.Beep(4000, 100);
                                Console.Beep(4000, 100);
                                btnMakeaSaleDiscount.IsEnabled = true;
                                btnMakeaSaleDiscount.Opacity = 1.0;
                                _isDiscountApplied = false;
                            }
                        }
                    }
                    else
                    {
                        Console.Beep(4000, 100);
                        Console.Beep(4000, 100);
                        btnMakeaSaleDiscount.IsEnabled = true;
                        btnMakeaSaleDiscount.Opacity = 1.0;
                    }
                }
                else
                {
                    Console.Beep(4000, 100);
                    Console.Beep(4000, 100);
                    btnMakeaSaleDiscount.IsEnabled = true;
                    btnMakeaSaleDiscount.Opacity = 1.0;
                }
            }
            else
            {
                Console.Beep(4000, 100);
                Console.Beep(4000, 100);
                _isDiscountApplied = false;
                _isDiscPercent = false;
                _isDiscDollar = false;
                lblDiscount.Visibility = Visibility.Collapsed;
                txtDiscount.Visibility = Visibility.Collapsed;
                btnMakeaSaleDiscountDollar.FontSize = 17;
                btnMakeaSaleDiscountDollar.Content = "Disc. $";
                txtTotalSaleAmt.Text = txtDiscount.Text;

                gbMakeaSaleProducts.IsEnabled = true;
                gbMakeaSaleProductToSelect.IsEnabled = true;
                btnClear.IsEnabled = true;
                btnClearLastItem.IsEnabled = true;
                btnClearSelItem.IsEnabled = true;
                btnMakeaSaleAddsale.IsEnabled = true;
                btnMakeaSaleAddsale.Opacity = 1.0;
                btnMakeaSaleDiscount.IsEnabled = true;
                btnMakeaSaleDiscount.Opacity = 1.0;
            }
        }

        private void lviewInvoice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Console.Beep(4000, 100);
            Flag = _CashOrQty.Quantity;
            txtQuantity.CaretIndex = txtQuantity.Text.Length;
            btnQuantity.Background = Brushes.DarkGreen;
            btnCard.Background = Brushes.LightGreen;
            btnCash.Background = Brushes.LightGreen;
            grdCalculation.IsEnabled = true;
        }

        private void btnClearSelItem_Click(object sender, RoutedEventArgs e)
        {
            Console.Beep(4000, 100);
            Flag = _CashOrQty.None;
            ClearLastItem(false, true);
        }

        private void _onKeyPadPress (int _KeyParam)
        {
            Console.Beep(4000, 100);
            if ((!_isDiscountApplied) || (_isDiscountApplied && Flag != _CashOrQty.Quantity))
            {
                var _Key = _KeyParam;
                if (txtQuantity.Text.Equals("0") || _isQuantityDefault)
                {
                    txtQuantity.Text = string.Empty;
                    if (_isQuantityDefault) _isQuantityDefault = false;
                }
                if (Flag == _CashOrQty.Quantity)
                {
                    if (_qtyLength > txtQuantity.Text.Length)
                    {
                        txtQuantity.Text = txtQuantity.Text + _Key;
                    }
                }
                else if ((Flag == _CashOrQty.Cash) || (Flag == _CashOrQty.Card))
                {
                    if (txtQuantity.Text.Contains("."))
                    {
                        if (_amtCentsLength > txtQuantity.Text.Substring(txtQuantity.Text.IndexOf(".")).Length - 1)
                        {
                            txtQuantity.Text = txtQuantity.Text + _Key;
                        }
                    }
                    else
                    {
                        if (_amtDollarsLength > txtQuantity.Text.Length)
                        {
                            txtQuantity.Text = txtQuantity.Text + _Key;
                        }
                    }
                }
            }
            else
            {

            }
        }
    }
}