﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymMembership.Pointofsale
{
    public class ClockInCheck
    {
        public bool _notClockedIn { get; set; } = true;
        public bool _clockedInNotClockedOut { get; set; } = false;
        public bool _clockedInAndClockedOut { get; set; } = false;
    }
}
