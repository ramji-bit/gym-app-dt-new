﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace GymMembership.Pointofsale
{
    /// <summary>
    /// Interaction logic for DiscountDollar.xaml
    /// </summary>
    public partial class DiscountDollar
    {
        public DiscountDollar()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (txtDiscountDollar.Text.Trim() == "")
            {
                //MessageBox.Show("Enter Discount %", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                Console.Beep(4000, 500);
                txtDiscountDollar.Focus();
                return;
            }

            if (Convert.ToDecimal(txtDiscountDollar.Text) < 0)
            {
                //MessageBox.Show("Discount % should be between < 0 > AND < 100 >", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                Console.Beep(4000, 100);
                txtDiscountDollar.Focus();
                return;
            }

            if (Convert.ToDecimal(txtDiscountDollar.Text) == 0)
            {
                (this.Owner as MakeaSale)._discountDollar = 0m;
                (this.Owner as MakeaSale)._isDiscountApplied = false;
            }
            else
            {
                (this.Owner as MakeaSale)._discountDollar = Convert.ToDecimal(txtDiscountDollar.Text);
                (this.Owner as MakeaSale)._isDiscountApplied = true;
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            (this.Owner as MakeaSale)._discountDollar = 0m;
            (this.Owner as MakeaSale)._isDiscountApplied = false;
            this.Close();
        }

        private void txt_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            bool _isAllowedDec = false;
            _isAllowedDec = !IsTextAllowed_UnitPrice(e.Text);
            if (!_isAllowedDec)
            {
                if (e.Text == "." && txtDiscountDollar.Text.Contains("."))
                {
                    Console.Beep(4000, 100);
                    _isAllowedDec = true;
                }
                else if (txtDiscountDollar.Text.Contains("."))
                {
                    if (txtDiscountDollar.Text.Substring(txtDiscountDollar.Text.IndexOf(".") + 1).Length >= 2)
                    {
                        Console.Beep(4000, 100);
                        _isAllowedDec = true;
                    }
                }
                else
                {
                    if ((txtDiscountDollar.Text.Length >= 4) && (e.Text != ".")) // >= 4
                    {
                        Console.Beep(4000, 100);
                        _isAllowedDec = true;
                    }
                }
            }
            e.Handled = _isAllowedDec;
        }

        private static bool IsTextAllowed_UnitPrice(string text)
        {
            Regex regex = new Regex("[^0-9.]"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtDiscountDollar.Focus();
        }

        private void MetroWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) || (e.Key == Key.Return))
            {
                btnSave_Click(sender, e);
            }
        }
    }
}