﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GymMembership.Pointofsale
{
    /// <summary>
    /// Interaction logic for EndOfShift.xaml
    /// </summary>
    public partial class EndOfShift 
    {
        public EndOfShift()
        {
            InitializeComponent();
        }
        
        private void txt_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            txt100DCount.Focus();
            txtCashAmount.Text = "0.00";
        }

        private void txtDenominationCount_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                int _Count = 0;
                decimal _TotalValue = 0m; decimal _denominationValue = 0m;
                decimal _dollarCentDecimal = 1;
                string _Name = (sender as System.Windows.Controls.TextBox).Name;
                TextBox _txtSender = (sender as System.Windows.Controls.TextBox);
                
                TextBox _txtValue = new TextBox();

                switch (_Name.ToUpper())
                {
                    //DOLLAR DENOMINATIONS

                    case "TXT100DCOUNT":
                        _txtValue = txt100DValue;
                        _denominationValue = 100m;
                        _dollarCentDecimal = 1;
                        break;
                    case "TXT50DCOUNT":
                        _txtValue = txt50DValue;
                        _denominationValue = 50m;
                        _dollarCentDecimal = 1;
                        break;
                    case "TXT20DCOUNT":
                        _txtValue = txt20DValue;
                        _denominationValue = 20m;
                        _dollarCentDecimal = 1;
                        break;
                    case "TXT10DCOUNT":
                        _txtValue = txt10DValue;
                        _denominationValue = 10m;
                        _dollarCentDecimal = 1;
                        break;
                    case "TXT5DCOUNT":
                        _txtValue = txt5DValue;
                        _denominationValue = 5m;
                        _dollarCentDecimal = 1;
                        break;
                    case "TXT2DCOUNT":
                        _txtValue = txt2DValue;
                        _denominationValue = 2m;
                        _dollarCentDecimal = 1;
                        break;
                    case "TXT1DCOUNT":
                        _txtValue = txt1DValue;
                        _denominationValue = 1m;
                        _dollarCentDecimal = 1;
                        break;

                    //CENTS DENOMINATIONS
                    case "TXT50CCOUNT":
                        _txtValue = txt50cValue;
                        _denominationValue = 50m;
                        _dollarCentDecimal = 0.01m;
                        break;
                    case "TXT20CCOUNT":
                        _txtValue = txt20cValue;
                        _denominationValue = 20m;
                        _dollarCentDecimal = 0.01m;
                        break;
                    case "TXT10CCOUNT":
                        _txtValue = txt10cValue;
                        _denominationValue = 10m;
                        _dollarCentDecimal = 0.01m;
                        break;
                    case "TXT5CCOUNT":
                        _txtValue = txt5cValue;
                        _denominationValue = 5m;
                        _dollarCentDecimal = 0.01m;
                        break;

                    default:
                        _txtValue = txtCashAmount;
                        _denominationValue = 0m;
                        _dollarCentDecimal = 1;
                        break;
                }

                if (_txtSender.Text.Trim() != "")
                {
                    _Count = Convert.ToInt32(_txtSender.Text.Trim());
                    _TotalValue = Convert.ToDecimal(_Count) * _denominationValue * _dollarCentDecimal;
                }
                else
                {
                    _TotalValue = 0m;
                }

                _txtValue.Text = _TotalValue.ToString("#0.#0");
                Calculate_Total_CashAmount();
            }
            catch (Exception)
            {

            }
        }
        private void Calculate_Total_CashAmount()
        {
            decimal _TotalCashAmount = 0m;
            
            //DOLLAR DENOMINATIONS
            if (txt100DValue.Text.Trim() != "")
            {
                _TotalCashAmount = _TotalCashAmount + Convert.ToDecimal(txt100DValue.Text.Trim());
            }
            else
            {
                _TotalCashAmount = _TotalCashAmount + 0m;
            }

            if (txt50DValue.Text.Trim() != "")
            {
                _TotalCashAmount = _TotalCashAmount + Convert.ToDecimal(txt50DValue.Text.Trim());
            }
            else
            {
                _TotalCashAmount = _TotalCashAmount + 0m;
            }

            if (txt20DValue.Text.Trim() != "")
            {
                _TotalCashAmount = _TotalCashAmount + Convert.ToDecimal(txt20DValue.Text.Trim());
            }
            else
            {
                _TotalCashAmount = _TotalCashAmount + 0m;
            }

            if (txt10DValue.Text.Trim() != "")
            {
                _TotalCashAmount = _TotalCashAmount + Convert.ToDecimal(txt10DValue.Text.Trim());
            }
            else
            {
                _TotalCashAmount = _TotalCashAmount + 0m;
            }

            if (txt5DValue.Text.Trim() != "")
            {
                _TotalCashAmount = _TotalCashAmount + Convert.ToDecimal(txt5DValue.Text.Trim());
            }
            else
            {
                _TotalCashAmount = _TotalCashAmount + 0m;
            }

            if (txt2DValue.Text.Trim() != "")
            {
                _TotalCashAmount = _TotalCashAmount + Convert.ToDecimal(txt2DValue.Text.Trim());
            }
            else
            {
                _TotalCashAmount = _TotalCashAmount + 0m;
            }

            if (txt1DValue.Text.Trim() != "")
            {
                _TotalCashAmount = _TotalCashAmount + Convert.ToDecimal(txt1DValue.Text.Trim());
            }
            else
            {
                _TotalCashAmount = _TotalCashAmount + 0m;
            }

            //CENTS DENOMINATIONS
            if (txt50cValue.Text.Trim() != "")
            {
                _TotalCashAmount = _TotalCashAmount + Convert.ToDecimal(txt50cValue.Text.Trim());
            }
            else
            {
                _TotalCashAmount = _TotalCashAmount + 0m;
            }

            if (txt20cValue.Text.Trim() != "")
            {
                _TotalCashAmount = _TotalCashAmount + Convert.ToDecimal(txt20cValue.Text.Trim());
            }
            else
            {
                _TotalCashAmount = _TotalCashAmount + 0m;
            }

            if (txt10cValue.Text.Trim() != "")
            {
                _TotalCashAmount = _TotalCashAmount + Convert.ToDecimal(txt10cValue.Text.Trim());
            }
            else
            {
                _TotalCashAmount = _TotalCashAmount + 0m;
            }

            if (txt5cValue.Text.Trim() != "")
            {
                _TotalCashAmount = _TotalCashAmount + Convert.ToDecimal(txt5cValue.Text.Trim());
            }
            else
            {
                _TotalCashAmount = _TotalCashAmount + 0m;
            }

            txtCashAmount.Text = _TotalCashAmount.ToString("#0.#0");
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (Convert.ToDecimal(txtCashAmount.Text) > 0)
            {
                (Owner as MakeaSale)._strClockInBalance = txtCashAmount.Text;
                (Owner as MakeaSale)._clockOutBalance = Convert.ToDecimal(txtCashAmount.Text);
                this.Close();
            }
            else
            {
                //System.Media.SystemSounds.Beep.Play();
                //System.Media.SystemSounds.Asterisk.Play();
                //System.Media.SystemSounds.Exclamation.Play();
                //System.Media.SystemSounds.Question.Play();
                //System.Media.SystemSounds.Hand.Play();
                Console.Beep(4000, 500);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            (Owner as MakeaSale)._strClockInBalance = null;
            (Owner as MakeaSale)._clockOutBalance = -1;
            this.Close();
        }
    }
}
