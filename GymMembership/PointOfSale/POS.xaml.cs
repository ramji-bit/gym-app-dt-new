﻿using System;
using System.IO;
using System.Windows;
using System.Collections;
using System.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Configuration;
using System.Windows.Controls;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;

using GymMembership.Reports;

namespace GymMembership.Pointofsale
{
    /// <summary>
    /// Interaction logic for POS.xaml
    /// </summary>
    public partial class POS 
    {
        ClockInCheck _ClockInCheck = new ClockInCheck();
        private int printAreaWidth = 970;
        private int printAreaHeight = 816;
        //public DataGrid printds = new DataGrid();
        public DataSet printds = new DataSet();

        decimal _TotalPaidAmt = 0m;
        decimal _TotalCashAmt = 0m;
        decimal _TotalCardAmt = 0m;
        decimal _TotalSaleAmt = 0m;

        string _OpeningCashBalance = null;
        string _ClosingCashBalance = null;
        public POS()
        {
            InitializeComponent();
        }
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        DataSet _dsAccessRights = new DataSet();
        int _ModuleTable = 0; int _SubModuleTable = 1;
        int _POSSummaryTable = 0; int _POSDetailsTable = 1; int _POSClockInOutTable = 2;
        DataSet _dsPOS = new DataSet();

        private void SetTopPanelDetails(string _CompanyName, string _UserName, string _CurrentLoginDateTime, byte[] _StaffPic)
        {
            lblOrgName.Content = _CompanyName;
            lblUserName.Content = _UserName;
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto(_StaffPic);
        }

        private void loadStaffPhoto(byte[] _StaffPic)
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }

        private void btnAddNewStock_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            AddNewStock _AddNewStock = new AddNewStock();
            _AddNewStock.Owner = this;
            _AddNewStock.SetAccessRightsDS(_dsAccessRights);
            this.Cursor = Cursors.Arrow;
            _AddNewStock._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _AddNewStock._CompanyName = this._CompanyName;
            _AddNewStock._SiteName = this._SiteName;
            _AddNewStock._UserName = this._UserName;
            _AddNewStock._StaffPic = this._StaffPic;
            _AddNewStock.StaffImage.Source = this.StaffImage.Source;
            _AddNewStock.Owner = this.Owner;
            this.Close();
            _AddNewStock.ShowDialog();
        }
        
        private void btnPOSMakeASale_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _ClockInCheck = Comman.Constants.POS_CheckClockIn(_ClockInCheck);

                if (!_ClockInCheck._notClockedIn)
                {
                    if (_ClockInCheck._clockedInNotClockedOut)
                    {
                        //GAS - 27 POS Home Club : 19/SEP/2017
                        if (ConfigurationManager.AppSettings["SuperAdmin"].ToString().ToUpper().Trim() != "YES")
                        {
                            if
                            (
                                ConfigurationManager.AppSettings["LoggedInSiteID"].ToString().ToUpper().Trim() !=
                                ConfigurationManager.AppSettings["HomeClubSiteID"].ToString().ToUpper().Trim()
                            )
                            {
                                MessageBoxResult _HomeClubResult =
                                MessageBox.Show
                                    (
                                        "Your selected site is different to the Home site!" +
                                        "\nWould you like to still continue with POS transaction - Yes or No?" +
                                        "\n\n(CLICK 'YES' TO PROCEED, 'NO' TO EXIT!)",
                                        this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No
                                    );
                                if (
                                        _HomeClubResult == MessageBoxResult.No || _HomeClubResult == MessageBoxResult.Cancel
                                   )
                                {
                                    return;
                                }
                            }
                        }
                        //GAS - 27 POS Home Club : 19/SEP/2017

                        this.Cursor = Cursors.Wait;
                        MakeaSale makeaSale = new MakeaSale();
                        makeaSale.Owner = this;
                        makeaSale.SetAccessRightsDS(_dsAccessRights);
                        this.Cursor = Cursors.Arrow;
                        makeaSale._CurrentLoginDateTime = this._CurrentLoginDateTime;
                        makeaSale._CompanyName = _CompanyName;
                        makeaSale._SiteName = this._SiteName;
                        makeaSale._UserName = this._UserName;
                        makeaSale._StaffPic = this._StaffPic;
                        makeaSale.StaffImage.Source = this.StaffImage.Source;
                        makeaSale.ShowDialog();
                    }
                    else if (_ClockInCheck._clockedInAndClockedOut)
                    {
                        MessageBox.Show("End-Of-Shift has been completed for Today!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
                else
                {
                    decimal _clockInBalance = -1;
                    string _strClockInBalance = null;

                    _strClockInBalance = ConfigurationManager.AppSettings["POSOpeningBalance"].ToString();
                    if (_strClockInBalance == "")
                        _strClockInBalance = "0.00";

                    //_strClockInBalance = Interaction.InputBox("Enter Opening Cash Balance : ", "POS Clock-In");
                    //if ((_strClockInBalance == "") || (_strClockInBalance == null))
                    //{
                    //    MessageBox.Show("No Amount entered!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //    return;
                    //}
                    //if (!Regex.IsMatch(_strClockInBalance, @"^\d{1,7}(\.\d{1,2})?$"))
                    //{
                    //    MessageBox.Show("Amount should be Numeric! (upto 2 decimal places)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //    return;
                    //}
                    _clockInBalance = Convert.ToDecimal(_strClockInBalance);
                    if (_clockInBalance < 0)
                    {
                        MessageBox.Show("Amount CANNOT be less than Zero!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }

                    int _result = Comman.Constants.POS_ClockIn_User(_clockInBalance);

                    if (_result > 0)
                    {
                        //GAS - 27 POS Home Club : 19/SEP/2017
                        if (ConfigurationManager.AppSettings["SuperAdmin"].ToString().ToUpper().Trim() != "YES")
                        {
                            if
                            (
                                ConfigurationManager.AppSettings["LoggedInSiteID"].ToString().ToUpper().Trim() !=
                                ConfigurationManager.AppSettings["HomeClubSiteID"].ToString().ToUpper().Trim()
                            )
                            {
                                MessageBoxResult _HomeClubResult =
                                MessageBox.Show
                                    (
                                        "Your selected site is different to the Home site!" +
                                        "\nWould you like to still continue with POS transaction - Yes or No?" +
                                        "\n\n(CLICK 'YES' TO PROCEED, 'NO' TO EXIT!)",
                                        this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No
                                    );
                                if (
                                        _HomeClubResult == MessageBoxResult.No || _HomeClubResult == MessageBoxResult.Cancel
                                   )
                                {
                                    return;
                                }
                            }
                        }
                        //GAS - 27 POS Home Club : 19/SEP/2017

                        this.Cursor = Cursors.Wait;
                        MakeaSale makeaSale = new MakeaSale();
                        makeaSale.Owner = this;
                        makeaSale.SetAccessRightsDS(_dsAccessRights);
                        this.Cursor = Cursors.Arrow;
                        makeaSale._CurrentLoginDateTime = this._CurrentLoginDateTime;
                        makeaSale._CompanyName = _CompanyName;
                        makeaSale._SiteName = this._SiteName;
                        makeaSale._UserName = this._UserName;
                        makeaSale._StaffPic = this._StaffPic;
                        makeaSale.StaffImage.Source = this.StaffImage.Source;
                        makeaSale.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Error in Staff Clock-In!", this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }
        
        private void btnViewInventory_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            ViewInventory vI = new ViewInventory();
            vI.Owner = this;
            vI.SetAccessRightsDS(_dsAccessRights);
            this.Cursor = Cursors.Arrow;
            vI._CurrentLoginDateTime = this._CurrentLoginDateTime;
            vI._CompanyName = this._CompanyName;
            vI._SiteName = this._SiteName;
            vI._UserName = this._UserName;
            vI._StaffPic = this._StaffPic;
            vI.StaffImage.Source = this.StaffImage.Source;
            vI.Owner = this.Owner;
            this.Close();
            vI.ShowDialog();
        }

        private void btnDoaStocktake_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            DoaStocktake dStock = new DoaStocktake();
            dStock.Owner = this;
            dStock.SetAccessRightsDS(_dsAccessRights);
            this.Cursor = Cursors.Arrow;
            dStock._CurrentLoginDateTime = this._CurrentLoginDateTime;
            dStock._CompanyName = this._CompanyName;
            dStock._SiteName = this._SiteName;
            dStock._UserName = this._UserName;
            dStock._StaffPic = this._StaffPic;
            dStock.StaffImage.Source = this.StaffImage.Source;
            dStock.Owner = this.Owner;
            this.Close();
            dStock.ShowDialog();
        }
        public void SetAccessRightsDS(DataSet dS)
        {
            _dsAccessRights = dS;
        }
        private void EnableDisableControls_AccessRights()
        {
            btnAddNewStock.IsEnabled = false;
            btnAddNewStock.Opacity = 0.5;

            btnDoaStocktake.IsEnabled = false;
            btnDoaStocktake.Opacity = 0.5;

            btnPOSMakeASale.IsEnabled = false;
            btnPOSMakeASale.Opacity = 0.5;

            btnViewInventory.IsEnabled = false;
            btnViewInventory.Opacity = 0.5;

            _dsAccessRights.Tables[_ModuleTable].DefaultView.RowFilter = "";
            _dsAccessRights.Tables[_SubModuleTable].DefaultView.RowFilter = "";

            if (_dsAccessRights.Tables[_SubModuleTable].DefaultView.Count > 0)
            {
                _dsAccessRights.Tables[_SubModuleTable].DefaultView.RowFilter = "ModuleName = 'POINT OF SALE' AND ACTIVE = 1";
                for (int i = 0; i < _dsAccessRights.Tables[_SubModuleTable].DefaultView.Count; i++)
                {
                    switch (_dsAccessRights.Tables[_SubModuleTable].DefaultView[i]["SubModuleName"].ToString().ToUpper())
                    {
                        case "MAKE A SALE":
                            btnPOSMakeASale.IsEnabled = true;
                            btnPOSMakeASale.Opacity = 1.00;
                            break;
                        case "ADD NEW STOCK":
                            btnAddNewStock.IsEnabled = true;
                            btnAddNewStock.Opacity = 1.00;
                            break;
                        case "VIEW INVENTORY":
                            btnViewInventory.IsEnabled = true;
                            btnViewInventory.Opacity = 1.00;
                            break;
                        case "DO A STOCK TABLE":
                            btnDoaStocktake.IsEnabled = true;
                            btnDoaStocktake.Opacity = 1.00;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void POS_Loaded(object sender, RoutedEventArgs e)
        {
            SetTopPanelDetails(_CompanyName, _UserName, _CurrentLoginDateTime, _StaffPic);
            EnableDisableControls_AccessRights();

            dtFrom.DisplayDate = DateTime.Now.Date;
            dtFrom.SelectedDate = DateTime.Now.Date;
            dtTo.DisplayDate = DateTime.Now.Date;
            dtTo.SelectedDate = DateTime.Now.Date;

            if (ConfigurationManager.AppSettings["isAdminUser"].ToString().Trim().ToUpper() == "YES")
            {
                chkAllStaff.IsChecked = true;
                chkAllStaff.Visibility = Visibility.Hidden;
            }
            else
            {
                chkAllStaff.IsChecked = true;
                chkAllStaff.Visibility = Visibility.Hidden;
            }

            ShowPOSSaleReport(dtFrom.DisplayDate,dtTo.DisplayDate);
        }

        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnPOS_Click(object sender, RoutedEventArgs e)
        {
            //SAME WINDOW; NO ACTION;

            //this.Cursor = Cursors.Wait;
            //POS _POS = new POS();
            //_POS.Owner = this;
            //_POS.SetAccessRightsDS(_dsAccessRights);
            //this.Cursor = Cursors.Arrow;
            //_POS._CurrentLoginDateTime = this._CurrentLoginDateTime;
            //_POS._CompanyName = this._CompanyName;
            //_POS._SiteName = this._SiteName;
            //_POS._UserName = this._UserName;
            //_POS._StaffPic = this._StaffPic;
            //_POS.StaffImage.Source = this.StaffImage.Source;
            //_POS.Owner = this.Owner;
            //this.Close();
            //_POS.ShowDialog();
        }

        private void btnShowReport_Click(object sender, RoutedEventArgs e)
        {
            DateTime _fromDate, _toDate;

            if (dtFrom.SelectedDate == null)
            {
                _fromDate = dtFrom.DisplayDate;
            }
            else
            {
                _fromDate = (DateTime)dtFrom.SelectedDate;
            }

            if (dtTo.SelectedDate == null)
            {
                _toDate = dtTo.DisplayDate;
            }
            else
            {
                _toDate = (DateTime)dtTo.SelectedDate;
            }

            if (ValidateDate(_fromDate,_toDate))
            {
                gvItemList.ItemsSource = null;
                ShowPOSSaleReport(_fromDate,_toDate);
            }
        }

        private void ShowPOSSaleReport(DateTime _fromDate, DateTime _toDate)
        {
            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
            string _userName = ConfigurationManager.AppSettings["LoggedInUser"].ToString();
            int _userLoginID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedUserLoginID"].ToString());

            try
            {
                if (chkAllStaff.IsChecked == true)
                {
                    _userName = "";
                    _userLoginID = 0;
                }

                SqlHelper _sql = new SqlHelper();
                Hashtable ht = new Hashtable();

                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                ht.Add("@FromDate", _fromDate);
                ht.Add("@ToDate", _toDate);
                ht.Add("@Staff", _userName);
                ht.Add("@StaffLoginID", _userLoginID);

                _TotalPaidAmt = 0;
                _TotalCashAmt = 0;
                _TotalCardAmt = 0;
                _TotalSaleAmt = 0;
                txtTotalPayment.Text = _TotalPaidAmt.ToString("#0.#0");
                txtTotalCash.Text = _TotalCashAmt.ToString("#0.#0");
                txtTotalCard.Text = _TotalCardAmt.ToString("#0.#0");
                txtTotalSale.Text = _TotalSaleAmt.ToString("#0.#0");

                _dsPOS = _sql.ExecuteProcedure("POS_Sale_Report", ht, true);
                gvPOSTransactions.ItemsSource = null;
                btnSendEmail.IsEnabled = false;

                if (_dsPOS != null)
                {
                    if (_dsPOS.Tables.Count > 0)
                    {
                        if (_dsPOS.Tables[_POSSummaryTable].DefaultView.Count > 0)
                        {
                            btnSendEmail.IsEnabled = true;
                            gvPOSTransactions.ItemsSource = _dsPOS.Tables[_POSSummaryTable].DefaultView;

                            DataTable _dtPOS = _dsPOS.Tables[_POSSummaryTable];
                            _TotalPaidAmt = 0;
                            _TotalCashAmt = 0;
                            _TotalCardAmt = 0;
                            _TotalSaleAmt = 0;

                            for (int i = 0; i < _dtPOS.DefaultView.Count; i++)
                            {
                                _TotalSaleAmt = _TotalSaleAmt + Convert.ToDecimal(_dtPOS.DefaultView[i]["SaleAmt"].ToString());
                                _TotalPaidAmt = _TotalPaidAmt + Convert.ToDecimal(_dtPOS.DefaultView[i]["SalePAmt"].ToString());
                                _TotalCashAmt = _TotalCashAmt + Convert.ToDecimal(_dtPOS.DefaultView[i]["Cash"].ToString()) - Convert.ToDecimal(_dtPOS.DefaultView[i]["Change"].ToString());
                                _TotalCardAmt = _TotalCardAmt + Convert.ToDecimal(_dtPOS.DefaultView[i]["Card"].ToString());
                            }
                            txtTotalPayment.Text = _TotalPaidAmt.ToString("#0.#0");
                            txtTotalCash.Text = _TotalCashAmt.ToString("#0.#0");
                            txtTotalCard.Text = _TotalCardAmt.ToString("#0.#0");
                            txtTotalSale.Text = _TotalSaleAmt.ToString("#0.#0");
                        }

                        if (_dsPOS.Tables[_POSClockInOutTable].DefaultView.Count > 0)
                        {
                            _OpeningCashBalance = _dsPOS.Tables[_POSClockInOutTable].DefaultView[0]["OpeningBalance"].ToString();
                            _ClosingCashBalance = _dsPOS.Tables[_POSClockInOutTable].DefaultView[0]["ClosingBalance"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                string msg = e.Message;
            }
        }

        private bool ValidateDate(DateTime _fromDate, DateTime _toDate)
        {
            if (_fromDate.Date > _toDate.Date)
            {
                MessageBox.Show("From Date Cannot be later than To Date!", "Point Of Sale", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            if (_toDate.Date > DateTime.Now.Date)
            {
                MessageBox.Show("To Date Cannot be future Date!", "Point Of Sale", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            return true;
        }

        private void gvPOSTransactions_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            int i = gvPOSTransactions.SelectedIndex;
            
            if (i != -1)
            {
                gvItemList.ItemsSource = null;
                _dsPOS.Tables[_POSDetailsTable].DefaultView.RowFilter = "";
                int _selInvoiceNo = Convert.ToInt32(_dsPOS.Tables[_POSSummaryTable].DefaultView[i]["SaleInvNo"].ToString());
                DataTable _dtPOSDetails = _dsPOS.Tables[_POSDetailsTable];
                _dtPOSDetails.DefaultView.RowFilter = "InvoiceNo = " + _selInvoiceNo;
                gvItemList.ItemsSource = _dtPOSDetails.DefaultView;
            }
        }
        
        private void btnPrintReport_Click(object sender, RoutedEventArgs e)
        {
            if (gvPOSTransactions.Items.Count > 0)
            {
                printds = _dsPOS;
                if (_OpeningCashBalance != null)
                    if (_OpeningCashBalance.Trim() == "")
                            _OpeningCashBalance = null;

                if (_ClosingCashBalance != null)
                    if (_ClosingCashBalance.Trim() == "")
                        _ClosingCashBalance = null;

                Print _print = new Print(gvPOSTransactions, null, this, false, _CompanyName, txtTotalCash.Text,
                        null,txtTotalCard.Text,txtTotalPayment.Text,0,true, txtTotalSale.Text, _OpeningCashBalance, _ClosingCashBalance);
            }
        }

        private void btnSendEmail_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
