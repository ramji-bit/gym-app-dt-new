﻿
using System.Windows;
using System.Collections.Generic;
using System.Windows.Controls;
using GymMembership.Accounts;
using GymMembership.BAL;
using GymMembership.DTO;
using GymMembership.Comman;
using System.Linq;
using System;
using System.Data;
using System.Windows.Input;
using System.Windows.Data;
using System.Windows.Controls.Primitives;
using System.ComponentModel;
using GymMembership.Task;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Windows.Media.Imaging;
using System.IO;


namespace GymMembership.Settings
{
    /// <summary>
    /// Interaction logic for Programme.xaml
    /// </summary>
    public partial class Programme 
    {
        private int _programGroupId=0;
        private int _programId = 0;
        private string _programmeName;
        private decimal _programmePrice;
        private int _programmeLength;
        private bool _programmeStatus;
        private int _NoOfVisits;
        private decimal _programmeSignUpFee;
        private string _programmeConditions;
        private int _programmeStatusCheck;
        private DateTime _dpProgrammePSDate;
        private int _addSuccess;
        private int _newSuccess;
        bool _isEdit = false;
        private bool reval;
        private int Duration = 0;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public Login _Login;
        Utilities _utilites = new Utilities();

        bool _isUpFront = false;
        bool _isNeverExpire = false;

        public Programme()
        {
            InitializeComponent();
        }
        
        private void btnProgrammeCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void cmbProgrammePG_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _programGroupId = Convert.ToInt32(cmbProgrammePG.SelectedValue);
            _utilites.get_ProgramByGroup(cmbProgrammeProgramme, _programGroupId,0);
            if(cmbProgrammePG.SelectedIndex>=0)
            {
                cmbProgrammeProgramme.IsEnabled = true;
                btnProgrammeNew.IsEnabled = true;
            }
            else
            {
                cmbProgrammeProgramme.IsEnabled = false;
                btnProgrammeNew.IsEnabled = false;
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            populateData();
            //this.Title = "CompanyDetails";
            lblOrgName.Content = _CompanyName.ToString().Replace("_", "__");
            lblUserName.Content = _UserName.ToString().Replace("_", "__");
            //lblSiteName.Content = _SiteName.ToString().Replace("_", "__");
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto();
        }

        private void loadStaffPhoto()
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }

        public void populateData()
        {
            try
            {
                _utilites.populateProgramGroup(cmbProgrammePG);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        public void clearData()
        {
            txtProgrammePP.Text = string.Empty;
            dpProgrammePSDate.Text = string.Empty;
            dpProgrammePEDate.Text = string.Empty;
            txtProgrammeConditions.Text = string.Empty;
            txtProgrammePSignUpFee.Text = string.Empty;
            txtNoOfVisits.Text = string.Empty;
            txtProgrammePL.Text = string.Empty;
            cmbProgrammePD.Text = string.Empty;
            chkProgrammePS.IsChecked = false;
        }

        public void disableData()
        {
            btnProgrammeEdit.IsEnabled = false;
            btnProgrammeNew.IsEnabled = false;
            btnProgrammeSave.IsEnabled = false;
            txtProgrammePP.IsEnabled = false;
            dpProgrammePSDate.IsEnabled = false;
            txtProgrammeConditions.IsEnabled = false;
            txtProgrammePSignUpFee.IsEnabled = false;
            txtProgrammePL.IsEnabled = false;
            cmbProgrammePD.IsEnabled = false;
            txtNoOfVisits.IsEnabled = false;
            chkProgrammePS.IsEnabled = false;
            grdRButtonUPNE.IsEnabled = false;
            txtProgrammeProgramme.Visibility = Visibility.Hidden;
            cmbProgrammeProgramme.Visibility = Visibility.Visible;
            cmbProgrammePG.IsEnabled = true;
        }
        public void enableData()
        {
            txtProgrammePP.IsEnabled = true;
            dpProgrammePSDate.IsEnabled = true;
            dpProgrammePEDate.IsEnabled = false;
            txtProgrammeConditions.IsEnabled = true;
            txtProgrammePSignUpFee.IsEnabled = true;
            txtProgrammePL.IsEnabled = true;
            cmbProgrammePD.IsEnabled = true;
            txtNoOfVisits.IsEnabled = true;
            chkProgrammePS.IsEnabled = true;
            grdRButtonUPNE.IsEnabled = true;
            txtProgrammeProgramme.Visibility = Visibility.Visible;
            cmbProgrammeProgramme.Visibility = Visibility.Hidden;
        }

        private void cmbProgrammeProgramme_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                MemberDetailsBAL _memberDetailsBal = new MemberDetailsBAL();
                _programId = Convert.ToInt32(cmbProgrammeProgramme.SelectedValue);
                DataSet ds = _memberDetailsBal.get_ProgrammeDetails(_programId);

                if(_programId!=0)
                {
                    txtProgrammePP.Text = Convert.ToDecimal (ds.Tables[0].Rows[0]["ProgrammePrice"]).ToString().Trim();
                    dpProgrammePSDate.Text= ds.Tables[1].Rows[0]["ProgrammeSdate"].ToString().Trim();
                    dpProgrammePEDate.Text = ds.Tables[1].Rows[0]["ProgrammeEdate"].ToString().Trim();
                    txtProgrammeConditions.Text= ds.Tables[0].Rows[0]["Conditions"].ToString().Trim();
                    txtProgrammePSignUpFee.Text = Convert.ToDecimal( ds.Tables[0].Rows[0]["SignUpFee"]).ToString().Trim();
                    txtNoOfVisits.Text= ds.Tables[0].Rows[0]["NoOfVisits"].ToString().Trim();
                    txtProgrammePL.Text = ds.Tables[0].Rows[0]["ProgrammeLength"].ToString().Trim();
                    cmbProgrammePD.SelectedValue = Convert.ToInt32(ds.Tables[0].Rows[0]["ProgrammeDuration"].ToString().Trim());

                    ////RR:CR-UPFRONT-NEVEREXPIRE-03AUG2016
                    rbUpfront.IsChecked = Convert.ToBoolean(ds.Tables[0].Rows[0]["isUpFront"].ToString());
                    rbNeverExpire.IsChecked = Convert.ToBoolean(ds.Tables[0].Rows[0]["isNeverExpire"].ToString());
                    if ((rbUpfront.IsChecked == false) && (rbNeverExpire.IsChecked == false))
                    {
                        rbNoneRegular.IsChecked = true;
                    }
                    ////RR:CR-UPFRONT-NEVEREXPIRE-03AUG2016

                    if (Convert.ToInt32(ds.Tables[0].Rows[0]["ProgrammeStatus"]) == 1)
                    {
                        chkProgrammePS.IsChecked = true;
                    }
                    else
                    {
                        chkProgrammePS.IsChecked = false;
                    }
                    btnProgrammeEdit.IsEnabled = true;
                }
                else
                {
                    clearData();
                    disableData();

                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
            }
        }

        private void btnProgrammeEdit_Click(object sender, RoutedEventArgs e)
        {
            enableData();
            cmbProgrammePG.IsEnabled = false;
            txtProgrammeProgramme.Text = cmbProgrammeProgramme.Text;
            btnProgrammeSave.IsEnabled = true;
            btnProgrammeEdit.IsEnabled = false;
            _isEdit = true;
        }


        private void btnProgrammeNew_Click(object sender, RoutedEventArgs e)
        {
            clearData();
            disableData();
            enableData();
            txtProgrammeProgramme.Focus();
            btnProgrammeEdit.IsEnabled = false;
            btnProgrammeSave.IsEnabled = true;
            cmbProgrammePG.IsEnabled = false;
            txtProgrammeProgramme.Text = string.Empty;
            _isEdit = false;
        }

        private void btnProgrammeSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SettingsBAL _settingDetailsBAL = new SettingsBAL();
                string _existingProdName;
                if (validateData())
                {
                    if (_isEdit)
                    {
                        CheckDateAndCalculateEndDate();
                    }

                    _programGroupId = Convert.ToInt32(cmbProgrammePG.SelectedValue);
                    _programmeName = txtProgrammeProgramme.Text.ToString().Trim();
                    _programmePrice = Convert.ToDecimal(txtProgrammePP.Text.ToString().Trim());
                    _programmeLength = Convert.ToInt32(txtProgrammePL.Text.ToString().Trim());
                    _NoOfVisits = Convert.ToInt32(txtNoOfVisits.Text.ToString().Trim());
                    Duration = Convert.ToInt32(cmbProgrammePD.SelectedValue.ToString());
                    _programmeSignUpFee = Convert.ToDecimal(txtProgrammePSignUpFee.Text.ToString().Trim());
                    _programmeConditions = txtProgrammeConditions.Text.ToString().Trim();
                    _programmeStatus = Convert.ToBoolean(chkProgrammePS.IsChecked.ToString());
                    _dpProgrammePSDate = Convert.ToDateTime(dpProgrammePSDate.ToString().Trim());

                    if (_programmeStatus == true)
                    {
                        _programmeStatusCheck = 1;
                    }
                    else
                    {
                        _programmeStatusCheck = 0;
                    }

                    _isUpFront = Convert.ToBoolean(rbUpfront.IsChecked.ToString());
                    _isNeverExpire = Convert.ToBoolean(rbNeverExpire.IsChecked.ToString());

                    if (_isEdit)
                    {
                        int _programmeId = Convert.ToInt32(cmbProgrammeProgramme.SelectedValue.ToString().Trim());
                        _programId = Convert.ToInt32(cmbProgrammeProgramme.SelectedValue.ToString().Trim());

                        int j = cmbProgrammeProgramme.SelectedIndex;

                        for (int i = 0; i < cmbProgrammeProgramme.Items.Count; i++)
                        {
                            //cmbProgrammeProgramme.SelectedIndex = i;
                            //_existingProdName = cmbProgrammeProgramme.Text;
                            _existingProdName = (cmbProgrammeProgramme.Items[i] as DataRowView).Row["ProgrammeName"].ToString();
                            if ((_programmeName.ToUpper() == _existingProdName.ToUpper()) && (i != j)) //&& (_programmeId != Convert.ToInt32(cmbProgrammeProgramme.SelectedValue.ToString().Trim())))
                            {
                                MessageBox.Show("Membership Already Exists!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                btnProgrammeSave.IsEnabled = false;
                                clearData();
                                disableData();
                                btnProgrammeNew.IsEnabled = true;
                                _utilites.get_ProgramByGroup(cmbProgrammeProgramme, _programGroupId);
                                return;
                            }
                        }

                        _addSuccess = _settingDetailsBAL.saveEdit_ProgrammeDetails(_programmeId, _programmeName, _programmePrice, _programmeLength, Duration, _NoOfVisits, _programmeSignUpFee, _programmeConditions, _programmeStatusCheck, _dpProgrammePSDate,_isUpFront, _isNeverExpire);
                        if (_addSuccess != 0)
                        {
                            MessageBox.Show("Membership Updated Sucessfully", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            cmbProgrammePG.Visibility = Visibility.Visible;
                            populateData();
                            cmbProgrammeProgramme.Visibility = Visibility.Visible;
                            btnProgrammeSave.IsEnabled = false;
                            cmbProgrammePG.IsEnabled = true;
                        }

                        else
                        {
                            MessageBox.Show("Membership Not added",this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
                        }
                    }

                    else
                    {
                        if (cmbProgrammeProgramme.Items.Count > 0)
                        {
                            for (int i = 0; i < cmbProgrammeProgramme.Items.Count; i++)
                            {
                                //cmbProgrammeProgramme.SelectedIndex = i;
                                //_existingProdName = cmbProgrammeProgramme.Text;
                                _existingProdName = (cmbProgrammeProgramme.Items[i] as DataRowView).Row["ProgrammeName"].ToString();
                                if (_programmeName.ToUpper() == _existingProdName.ToUpper())
                                {
                                    MessageBox.Show("Membership Already Exists! Please enter a New Membership!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                    return;
                                }
                            }
                        }
                        _newSuccess = _settingDetailsBAL.saveNew_ProgrammeDetails(_programGroupId, _programmeName, _programmePrice, _programmeLength, Duration, _NoOfVisits, _programmeSignUpFee, _programmeConditions, _programmeStatusCheck, _dpProgrammePSDate, _isUpFront, _isNeverExpire);

                        if (_newSuccess != 0)
                        {
                            MessageBox.Show("Membership Added Sucessfully", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            populateData();
                            cmbProgrammeProgramme.Visibility = Visibility.Visible;
                            btnProgrammeSave.IsEnabled = false;
                            cmbProgrammePG.IsEnabled = true;
                            clearData();
                            disableData();
                        }

                        else
                        {
                            MessageBox.Show("Membership Not added", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                } 
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private bool validateData()
        {
            reval = true;
            FormUtilities _formUtilities = new FormUtilities();

            if (string.IsNullOrEmpty(txtProgrammeProgramme.Text.ToString()))
            {
                MessageBox.Show("Please Enter Membership Name", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtProgrammeProgramme.Focus();
                reval = false;
                return reval;
            }
          
            if (string.IsNullOrEmpty(txtProgrammePP.Text.ToString()))
            {
                MessageBox.Show("Please Enter Membership Price.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtProgrammePP.Focus();
                reval = false;
                return reval;
            }

            if (!Regex.IsMatch(txtProgrammePP.Text, @"^\d{1,7}(\.\d{1,2})?$"))
            {
                MessageBox.Show("Please Enter Membership Price in Numbers.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtProgrammePP.Focus();
                reval = false;
                txtProgrammePP.Text = string.Empty;
                return reval;
            }

            if (string.IsNullOrEmpty(txtProgrammePL.Text.ToString()))
            {
                MessageBox.Show("Please Enter Membership Length", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtProgrammePL.Focus();
                reval = false;
                return reval;
            }

            if (!txtProgrammePL.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Please Enter Membership Length in Numbers", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtProgrammePL.Focus();
                reval = false;
                txtProgrammePL.Text = string.Empty;
                return reval;
            }

            if (string.IsNullOrEmpty(cmbProgrammePD.Text.ToString()))
            {
                MessageBox.Show("Please Select Membership Duration", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                cmbProgrammePD.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(dpProgrammePSDate.Text.ToString()))
            {
                MessageBox.Show("Please select Membership Start Date", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                dpProgrammePSDate.Focus();
                reval = false;
                return reval;
            }

            if (_formUtilities.CheckDate(dpProgrammePSDate.Text) == false)
            {
                MessageBox.Show("Please Enter Date in dd/mm/yyyy format", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                dpProgrammePSDate.Focus();
                reval = false;
                return reval;
            }

            if (string.IsNullOrEmpty(txtNoOfVisits.Text.ToString()))
            {
                MessageBox.Show("Please Enter No Of Visits", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtNoOfVisits.Focus();
                reval = false;
                return reval;
            }

            if (!txtNoOfVisits.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Please Enter No Of Visits in Numbers", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtNoOfVisits.Focus();
                reval = false;
                txtNoOfVisits.Text = string.Empty;
                return reval;

            }

            if (string.IsNullOrEmpty(txtProgrammePSignUpFee.Text.ToString()))
            {
                MessageBox.Show("Please Enter Sign Up Fee", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtProgrammePSignUpFee.Focus();
                reval = false;
                return reval;
            }

            if (!Regex.IsMatch(txtProgrammePSignUpFee.Text, @"^\d{1,7}(\.\d{1,2})?$"))
            {
                MessageBox.Show("Please Enter Sign Up Fee in Numbers", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtProgrammePSignUpFee.Focus();
                reval = false;
                txtProgrammePSignUpFee.Text = string.Empty;
                return reval;
            }

            if (string.IsNullOrEmpty(txtProgrammeConditions.Text.ToString()))
            {
                MessageBox.Show("Please Enter Notes", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtProgrammeConditions.Focus();
                reval = false;
                return reval;
            }
            
            return true;
        }

        private void StartDate_LostFocus(object sender, RoutedEventArgs e)
        {
            CheckDateAndCalculateEndDate();
        }
        private void CheckDateAndCalculateEndDate()
        {
            //if (dpProgrammePSDate.Text.Trim() != "")
            if ((dpProgrammePSDate.Text.Trim() != "") && (rbNeverExpire.IsChecked != true))
            {
                FormUtilities _formUtilities = new FormUtilities();
                if (_formUtilities.CheckDate(dpProgrammePSDate.Text) == true)
                {
                    if (txtProgrammePL.Text.Trim() != "")
                    {
                        if (txtProgrammePL.Text.All(c => Char.IsNumber(c)))
                        {
                            if (Convert.ToInt32(txtProgrammePL.Text.Trim()) > 0)
                            {
                                int _pgLength = Convert.ToInt32(txtProgrammePL.Text.Trim());
                                if (cmbProgrammePD.Text.Trim() != "")
                                {
                                    DateTime _Sdt = Convert.ToDateTime(dpProgrammePSDate.Text);
                                    DateTime _Edt;
                                    switch (cmbProgrammePD.Text.Trim().ToUpper())
                                    {
                                        case "YEAR":
                                            _Edt = _Sdt.AddYears(_pgLength);
                                            break;
                                        case "MONTH":
                                            _Edt = _Sdt.AddMonths(_pgLength);
                                            break;
                                        case "WEEK":
                                            _Edt = _Sdt.AddDays(_pgLength * 7);
                                            break;
                                        case "DAY":
                                            _Edt = _Sdt.AddDays(_pgLength);
                                            break;
                                        default:
                                            return;
                                    }
                                    dpProgrammePEDate.Text = _Edt.Date.ToString();
                                }
                            }
                        }
                    }
                }
            }
            else if (rbNeverExpire.IsChecked == true)
            {
                dpProgrammePEDate.Text = "31/12/9999";
            }
        }
        private void btnContactTypes1_Click(object sender, RoutedEventArgs e)
        {
            CompanyDetails _CompanySiteDetails = new CompanyDetails();
            _CompanySiteDetails._CompanyName = this._CompanyName;
            _CompanySiteDetails._UserName = this._UserName;
            _CompanySiteDetails._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _CompanySiteDetails._StaffPic = this._StaffPic;
            _CompanySiteDetails.StaffImage.Source = StaffImage.Source;
            _CompanySiteDetails.Owner = this.Owner;

            this.Close();
            _CompanySiteDetails.ShowDialog();
        }

        private void btnUserAdministration_Click(object sender, RoutedEventArgs e)
        {
            if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
            {
                UserAdministration useradmin = new UserAdministration();
                useradmin._CompanyName = this._CompanyName;
                useradmin._UserName = this._UserName;
                useradmin._CurrentLoginDateTime = this._CurrentLoginDateTime;
                useradmin._StaffPic = this._StaffPic;
                useradmin.StaffImage.Source = StaffImage.Source;
                useradmin.Owner = this.Owner;

                this.Close();
                useradmin.ShowDialog();
            }
            else
            {
                MessageBox.Show("You MUST be Admin user to access this Screen!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnProductsTypes_Click(object sender, RoutedEventArgs e)
        {

            ProductType prdType = new ProductType();
            prdType._CompanyName = this._CompanyName;
            prdType._UserName = this._UserName;
            prdType._CurrentLoginDateTime = this._CurrentLoginDateTime;
            prdType._StaffPic = this._StaffPic;
            prdType.StaffImage.Source = StaffImage.Source;
            prdType.Owner = this.Owner;

            this.Close();
            prdType.ShowDialog();
        }

        private void btnManageProducts_Click(object sender, RoutedEventArgs e)
        {
            Products products = new Products();
            products._CompanyName = this._CompanyName;
            products._UserName = this._UserName;
            products._CurrentLoginDateTime = this._CurrentLoginDateTime;
            products._StaffPic = this._StaffPic;
            products.StaffImage.Source = StaffImage.Source;
            products.Owner = this.Owner;

            this.Close();
            products.ShowDialog();
        }

        private void btnProgrammeGSetup_Click(object sender, RoutedEventArgs e)
        {
            ProgrammeGroup PrgGroup = new ProgrammeGroup();
            PrgGroup._CompanyName = this._CompanyName;
            PrgGroup._UserName = this._UserName;
            PrgGroup._CurrentLoginDateTime = this._CurrentLoginDateTime;
            PrgGroup._StaffPic = this._StaffPic;
            PrgGroup.StaffImage.Source = StaffImage.Source;
            PrgGroup.Owner = this.Owner;

            this.Close();
            PrgGroup.ShowDialog();
        }

        private void btnAddEditProgrammes_Click(object sender, RoutedEventArgs e)
        {
            //SAME WINDOW; NO ACTION

            //Programme Prg = new Programme();
            //Prg._CompanyName = this._CompanyName;
            //Prg._UserName = this._UserName;
            //Prg._CurrentLoginDateTime = this._CurrentLoginDateTime;
            //Prg._StaffPic = this._StaffPic;
            //Prg.StaffImage.Source = StaffImage.Source;
            //Prg.Owner = this.Owner;

            //this.Close();
            //Prg.ShowDialog();
        }

        private void btnSupplierSetup_Click(object sender, RoutedEventArgs e)
        {
            Supplier _Supplier = new Supplier();
            _Supplier._CompanyName = this._CompanyName;
            _Supplier._UserName = this._UserName;
            _Supplier._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Supplier._StaffPic = this._StaffPic;
            _Supplier.StaffImage.Source = StaffImage.Source;
            _Supplier.Owner = this.Owner;

            this.Close();
            _Supplier.ShowDialog();
        }

        private void btnBookingResources_Click(object sender, RoutedEventArgs e)
        {
            Settings.Resources _Res = new Settings.Resources();
            _Res._CompanyName = this._CompanyName;
            _Res._UserName = this._UserName;
            _Res._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Res._StaffPic = this._StaffPic;
            _Res.StaffImage.Source = StaffImage.Source;
            _Res.Owner = this.Owner;

            this.Close();
            _Res.ShowDialog();
        }

        private void btnUserAccessRights_Click(object sender, RoutedEventArgs e)
        {
            StaffAccess _StaffAccess = new StaffAccess();
            _StaffAccess._CompanyName = this._CompanyName;
            _StaffAccess._UserName = this._UserName;
            _StaffAccess._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _StaffAccess._StaffPic = this._StaffPic;
            _StaffAccess.StaffImage.Source = StaffImage.Source;
            _StaffAccess.Owner = this.Owner;

            this.Close();
            _StaffAccess.ShowDialog();
        }

        private void btnEmailTSetup_Click(object sender, RoutedEventArgs e)
        {
            EmailTemplateSettings _eMailTemplate = new EmailTemplateSettings();
            _eMailTemplate._CompanyName = this._CompanyName;
            _eMailTemplate._UserName = this._UserName;
            _eMailTemplate._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _eMailTemplate._StaffPic = this._StaffPic;
            _eMailTemplate.StaffImage.Source = StaffImage.Source;
            _eMailTemplate.Owner = this.Owner;

            this.Close();
            _eMailTemplate.ShowDialog();
        }


        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDashboardQuit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
                //Closing event called
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnContactTypes_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
