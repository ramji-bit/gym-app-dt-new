﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GymMembership.Accounts;
using System.Configuration;

namespace GymMembership.Settings
{
    /// <summary>
    /// Interaction logic for Setting.xaml
    /// </summary>
    public partial class Setting : Window
    {
        public Setting()
        {
            InitializeComponent();
        }

        private void btnUserAdministration_Click(object sender, RoutedEventArgs e)
        {
            if (ConfigurationManager.AppSettings["IsAdminUser"] == "Yes")
            { 
                UserAdministration useradmin = new UserAdministration();
                useradmin.Owner = this;
                useradmin.ShowDialog();
            }
            else
            {
                MessageBox.Show("You MUST be Admin user to access this Screen!");
                return;
            }
        }

        private void btnProductsTypes_Click(object sender, RoutedEventArgs e)
        {
            ProductType prdType = new ProductType();
            prdType.Owner = this;
            prdType.ShowDialog();
        }

        private void btnManageProducts_Click(object sender, RoutedEventArgs e)
        {
            Products products = new Products();
            products.Owner = this;
            products.ShowDialog();
        }

        private void btnProgrammeGSetup_Click(object sender, RoutedEventArgs e)
        {
            ProgrammeGroup PrgGroup = new ProgrammeGroup();
            PrgGroup.Owner = this;
            PrgGroup.ShowDialog();
        }

        private void btnAddEditProgrammes_Click(object sender, RoutedEventArgs e)
        {
            Programme Prg = new Programme();
            Prg.Owner = this;
            Prg.ShowDialog();
        }

        private void btnSupplierSetup_Click(object sender, RoutedEventArgs e)
        {
            Supplier _Supplier = new Supplier();
            _Supplier.Owner = this;
            _Supplier.ShowDialog();
        }

        private void btnBookingResources_Click(object sender, RoutedEventArgs e)
        {
            Settings.Resources _Res = new Settings.Resources();
            _Res.Owner = this;
            _Res.ShowDialog();
        }

        private void btnUserAccessRights_Click(object sender, RoutedEventArgs e)
        {
            StaffAccess _StaffAccess = new StaffAccess();
            _StaffAccess.Owner = this;
            _StaffAccess.ShowDialog();
        }
        private void btnContactTypes_Click(object sender, RoutedEventArgs e)
        {
            CompanyDetails _CompanySiteDetails = new CompanyDetails();
            _CompanySiteDetails.Owner = this;
            _CompanySiteDetails.ShowDialog();
        }

        private void btnEmailTSetup_Click(object sender, RoutedEventArgs e)
        {
            EmailTemplateSettings _eMailTemplate = new EmailTemplateSettings();
            _eMailTemplate.Owner = this;

            _eMailTemplate.ShowDialog();
        }
    }
}
