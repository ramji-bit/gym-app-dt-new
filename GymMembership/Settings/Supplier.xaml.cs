﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GymMembership.Comman;
using GymMembership.BAL;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;
using GymMembership.Accounts;
using System.Windows.Media.Imaging;
using System.IO;
namespace GymMembership.Settings
{
    /// <summary>
    /// Interaction logic for Supplier.xaml
    /// </summary>
    public partial class Supplier 
    {
        public Supplier()
        {
            InitializeComponent();

        }
        private int _SupplierId;
        private bool _isEdit = false;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public Login _Login;
        Utilities _util = new Utilities();
        SettingsBAL _settingDetailsBAL = new SettingsBAL();

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _util.populateSupplierName(cmbSupplierName);
            if(cmbSupplierName.Items.Count>0)
            {
                cmbSupplierName.IsEnabled = true;
            }
            else
            {
                cmbSupplierName.IsEnabled = false;
            }
            //this.Title = "CompanyDetails";
            lblOrgName.Content = _CompanyName.ToString().Replace("_", "__");
            lblUserName.Content = _UserName.ToString().Replace("_", "__");
            //lblSiteName.Content = _SiteName.ToString().Replace("_", "__");
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto();
        }

        private void loadStaffPhoto()
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }


        private void cmbSupplierName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                _SupplierId = Convert.ToInt32(cmbSupplierName.SelectedValue);
                DataSet ds = _settingDetailsBAL.get_SupplierDetails(_SupplierId);
                if (_SupplierId != 0)
                {
                    txtSupplierName.Text = ds.Tables[0].Rows[0]["SupplierName"].ToString().Trim();
                    txtSupplierPhoneNo.Text = ds.Tables[0].Rows[0]["PhoneNo"].ToString().Trim();
                    txtSupplierContactPerson.Text = ds.Tables[0].Rows[0]["ContactPerson"].ToString().Trim();
                    txtSupplierEmail.Text = ds.Tables[0].Rows[0]["Email"].ToString().Trim();
                    txtSupplierFaxNo.Text = ds.Tables[0].Rows[0]["FaxNo"].ToString().Trim();
                    txtSupplierMobileNo.Text = ds.Tables[0].Rows[0]["MobileNo"].ToString().Trim();
                    txtSupplierWebsite.Text = ds.Tables[0].Rows[0]["Website"].ToString().Trim();
                    txtSupplierNotes.Text = ds.Tables[0].Rows[0]["Notes"].ToString().Trim();
                    btnSupplierEdit.IsEnabled = true;
                    Disable();
                    btnSupplierEdit.IsEnabled = true;
                    btnSupplierSave.IsEnabled = false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
           
        }

        private void btnSupplierCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void ClearData()
        {
            txtSupplierName.Text = string.Empty;
            txtSupplierPhoneNo.Text = string.Empty;
            txtSupplierContactPerson.Text = string.Empty;
            txtSupplierEmail.Text = string.Empty;
            txtSupplierFaxNo.Text = string.Empty;
            txtSupplierMobileNo.Text = string.Empty;
            txtSupplierWebsite.Text = string.Empty;
            txtSupplierNotes.Text = string.Empty;
        }

        public void Enable()
        {
            txtSupplierName.IsEnabled = true;
            txtSupplierPhoneNo.IsEnabled = true;
            txtSupplierContactPerson.IsEnabled = true;
            txtSupplierEmail.IsEnabled = true;
            txtSupplierFaxNo.IsEnabled = true;
            txtSupplierMobileNo.IsEnabled = true;
            txtSupplierWebsite.IsEnabled = true;
            txtSupplierNotes.IsEnabled = true;
            btnSupplierSave.IsEnabled = true;
        }

        public void Disable()
        {
            txtSupplierName.IsEnabled = false;
            txtSupplierPhoneNo.IsEnabled = false;
            txtSupplierContactPerson.IsEnabled = false;
            txtSupplierEmail.IsEnabled = false;
            txtSupplierFaxNo.IsEnabled = false;
            txtSupplierMobileNo.IsEnabled = false;
            txtSupplierWebsite.IsEnabled = false;
            txtSupplierNotes.IsEnabled = false;
            btnSupplierSave.IsEnabled = false;
        }

        private void btnSupplierNew_Click(object sender, RoutedEventArgs e)
        {
            txtSupplierName.Focus();
            cmbSupplierName.SelectedValue = 0;
            btnSupplierEdit.IsEnabled = false;
            Enable();
            ClearData();
            _isEdit = false;
        }

        private void btnSupplierEdit_Click(object sender, RoutedEventArgs e)
        {
            Enable();
            btnSupplierEdit.IsEnabled = false;
            _isEdit = true;
        }

        private void btnSupplierSave_Click(object sender, RoutedEventArgs e)
        {
            string _SupplierName;
            string _PhoneNo;
            string _MobileNo;
            string _FaxNo;
            string _ContactPerson;
            string _Notes;
            string _Website;
            string _Email;
            string _existingSupplierName;
            int _addSuccess;

            try
            {
                if (txtSupplierName.Text.Trim() == "")
                {
                    MessageBox.Show("Please enter Supplier Name", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtSupplierName.Focus();
                    return;
                }
                else if (txtSupplierPhoneNo.Text.Trim() == "")
                {
                    MessageBox.Show("Please enter Phone No", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtSupplierPhoneNo.Focus();
                    return;
                }
                else if (Regex.IsMatch(txtSupplierPhoneNo.Text, @"[^0-9.-]+"))
                {
                    txtSupplierPhoneNo.Text = "";
                    MessageBox.Show("Please enter Numbers only", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtSupplierPhoneNo.Focus();
                    return;
                }
                //else if (txtSupplierWebsite.Text.Trim() == "")
                //{
                //    MessageBox.Show("Please enter a Website", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //    return;
                //}
                //else if (!txtSupplierWebsite.Text.Contains(".com"))
                //{
                //    MessageBox.Show("Enter Website Address in Correct Format ", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //    return;
                //}
                //else if (txtSupplierEmail.Text.Trim() == "")
                //{
                //    MessageBox.Show("Please enter a Email Id", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //    return;
                //}
                //else if (!txtSupplierEmail.Text.Contains("@"))
                //{
                //    MessageBox.Show("InValid Email", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //    return;
                //}
                //else if (!txtSupplierEmail.Text.Contains(".com"))
                //{
                //    MessageBox.Show("InValid Email", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //    return;
                //}
                //else if (txtSupplierFaxNo.Text.Trim() == "")
                //{
                //    MessageBox.Show("Please enter a Fax No", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //    return;
                //}
                //else if (Regex.IsMatch(txtSupplierFaxNo.Text, @"[^0-9.-]+"))
                //{
                //    txtSupplierFaxNo.Text = "";
                //    MessageBox.Show("Please enter a Numbers only", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //    txtSupplierFaxNo.Focus();
                //    return;
                //}

                else if (txtSupplierContactPerson.Text.Trim() == "")
                {
                    MessageBox.Show("Please enter Contact Person", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtSupplierContactPerson.Focus();
                    return;
                }
                else if (txtSupplierMobileNo.Text.Trim() == "")
                {
                    MessageBox.Show("Please enter a Mobile No", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtSupplierMobileNo.Focus();
                    return;
                }
                else if (Regex.IsMatch(txtSupplierMobileNo.Text, @"[^0-9.-]+"))
                {
                    txtSupplierMobileNo.Text = "";
                    MessageBox.Show("Please enter only Numbers for Mobile", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtSupplierMobileNo.Focus();
                    return;
                }

                if (!_isEdit)
                {
                    _SupplierName = txtSupplierName.Text.ToString().Trim();
                    _PhoneNo = txtSupplierPhoneNo.Text.ToString().Trim();
                    _MobileNo = txtSupplierMobileNo.Text.ToString().Trim();
                    _FaxNo = txtSupplierFaxNo.Text.ToString().Trim();
                    _Email = txtSupplierEmail.Text.ToString().Trim();
                    _Website = txtSupplierWebsite.Text.ToString().Trim();
                    _ContactPerson = txtSupplierContactPerson.Text.ToString().Trim();
                    _Notes = txtSupplierNotes.Text.ToString().Trim();

                    if (cmbSupplierName.Items.Count > 0)
                    {
                        for (int i = 0; i < cmbSupplierName.Items.Count; i++)
                        {
                            //cmbSupplierName.SelectedIndex = i;
                            _existingSupplierName = (cmbSupplierName.Items[i] as DataRowView).Row["SupplierName"].ToString();

                            if (_SupplierName.ToUpper() == _existingSupplierName.ToUpper())
                            {
                                MessageBox.Show("Supplier Name Already Exists! Please enter a New Supplier Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                btnSupplierNew.IsEnabled = true;
                                btnSupplierSave.IsEnabled = true;
                                return;
                            }
                        }
                    }
                    _addSuccess = _settingDetailsBAL.saveNew_SupplierDetails(_SupplierName, _MobileNo, _PhoneNo, _FaxNo, _Email, _Website, _ContactPerson, _Notes);
                    if (_addSuccess != 0)
                    {
                        MessageBox.Show("New Supplier Name Added ! " + _SupplierName, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        _util.populateSupplierName(cmbSupplierName);
                        cmbSupplierName.IsEnabled = true;
                        btnSupplierEdit.IsEnabled = false;
                        ClearData();
                        Disable();
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Error! Supplier Not Added!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
                else
                {
                    int _SupplierIdCheck = Convert.ToInt32(cmbSupplierName.SelectedValue);
                    _SupplierName = txtSupplierName.Text.ToString().Trim();
                    _PhoneNo = txtSupplierPhoneNo.Text.ToString().Trim();
                    _MobileNo = txtSupplierMobileNo.Text.ToString().Trim();
                    _FaxNo = txtSupplierFaxNo.Text.ToString().Trim();
                    _Email = txtSupplierEmail.Text.ToString().Trim();
                    _Website = txtSupplierWebsite.Text.ToString().Trim();
                    _ContactPerson = txtSupplierContactPerson.Text.ToString().Trim();
                    _Notes = txtSupplierNotes.Text.ToString().Trim();
                    int j = cmbSupplierName.SelectedIndex;
                    for (int i = 0; i < cmbSupplierName.Items.Count; i++)
                    {
                        //cmbSupplierName.SelectedIndex = i;
                        _existingSupplierName = (cmbSupplierName.Items[i] as DataRowView).Row["SupplierName"].ToString();

                        if ((_SupplierName.ToUpper() == _existingSupplierName.ToUpper()) && (i != j))// && (_SupplierIdCheck != Convert.ToInt32(cmbSupplierName.SelectedValue.ToString().Trim())))
                        {
                            MessageBox.Show("Supplier Name Type Already Exists!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            btnSupplierNew.IsEnabled = true;
                            btnSupplierSave.IsEnabled = true;
                            return;
                        }
                    }

                    _addSuccess = _settingDetailsBAL.saveEdit_SupplierDetails(_SupplierIdCheck, _SupplierName, _MobileNo, _PhoneNo,  _FaxNo, _Email, _Website, _ContactPerson, _Notes);
                    MessageBox.Show("Supplier Name Updated ! " + _SupplierName, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    _util.populateSupplierName(cmbSupplierName);
                    btnSupplierEdit.IsEnabled = false;
                    ClearData();
                    Disable();
                    return;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }
        private void btnContactTypes1_Click(object sender, RoutedEventArgs e)
        {
            CompanyDetails _CompanySiteDetails = new CompanyDetails();
            _CompanySiteDetails._CompanyName = this._CompanyName;
            _CompanySiteDetails._UserName = this._UserName;
            _CompanySiteDetails._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _CompanySiteDetails._StaffPic = this._StaffPic;
            _CompanySiteDetails.StaffImage.Source = StaffImage.Source;
            _CompanySiteDetails.Owner = this.Owner;

            this.Close();
            _CompanySiteDetails.ShowDialog();
        }

        private void btnUserAdministration_Click(object sender, RoutedEventArgs e)
        {
            if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
            {
                UserAdministration useradmin = new UserAdministration();
                useradmin._CompanyName = this._CompanyName;
                useradmin._UserName = this._UserName;
                useradmin._CurrentLoginDateTime = this._CurrentLoginDateTime;
                useradmin._StaffPic = this._StaffPic;
                useradmin.StaffImage.Source = StaffImage.Source;
                useradmin.Owner = this.Owner;

                this.Close();
                useradmin.ShowDialog();
            }
            else
            {
                MessageBox.Show("You MUST be Admin user to access this Screen!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnProductsTypes_Click(object sender, RoutedEventArgs e)
        {

            ProductType prdType = new ProductType();
            prdType._CompanyName = this._CompanyName;
            prdType._UserName = this._UserName;
            prdType._CurrentLoginDateTime = this._CurrentLoginDateTime;
            prdType._StaffPic = this._StaffPic;
            prdType.StaffImage.Source = StaffImage.Source;
            prdType.Owner = this.Owner;

            this.Close();
            prdType.ShowDialog();
        }

        private void btnManageProducts_Click(object sender, RoutedEventArgs e)
        {
            Products products = new Products();
            products._CompanyName = this._CompanyName;
            products._UserName = this._UserName;
            products._CurrentLoginDateTime = this._CurrentLoginDateTime;
            products._StaffPic = this._StaffPic;
            products.StaffImage.Source = StaffImage.Source;
            products.Owner = this.Owner;

            this.Close();
            products.ShowDialog();
        }

        private void btnProgrammeGSetup_Click(object sender, RoutedEventArgs e)
        {
            ProgrammeGroup PrgGroup = new ProgrammeGroup();
            PrgGroup._CompanyName = this._CompanyName;
            PrgGroup._UserName = this._UserName;
            PrgGroup._CurrentLoginDateTime = this._CurrentLoginDateTime;
            PrgGroup._StaffPic = this._StaffPic;
            PrgGroup.StaffImage.Source = StaffImage.Source;
            PrgGroup.Owner = this.Owner;

            this.Close();
            PrgGroup.ShowDialog();
        }

        private void btnAddEditProgrammes_Click(object sender, RoutedEventArgs e)
        {
            Programme Prg = new Programme();
            Prg._CompanyName = this._CompanyName;
            Prg._UserName = this._UserName;
            Prg._CurrentLoginDateTime = this._CurrentLoginDateTime;
            Prg._StaffPic = this._StaffPic;
            Prg.StaffImage.Source = StaffImage.Source;
            Prg.Owner = this.Owner;

            this.Close();
            Prg.ShowDialog();
        }

        private void btnSupplierSetup_Click(object sender, RoutedEventArgs e)
        {
            ////SAME WINDOW NO ACTION;

            //Supplier _Supplier = new Supplier();
            //_Supplier._CompanyName = this._CompanyName;
            //_Supplier._UserName = this._UserName;
            //_Supplier._CurrentLoginDateTime = this._CurrentLoginDateTime;
            //_Supplier._StaffPic = this._StaffPic;
            //_Supplier.StaffImage.Source = StaffImage.Source;
            //_Supplier.Owner = this.Owner;

            //this.Close();
            //_Supplier.ShowDialog();
        }

        private void btnBookingResources_Click(object sender, RoutedEventArgs e)
        {
            Settings.Resources _Res = new Settings.Resources();
            _Res._CompanyName = this._CompanyName;
            _Res._UserName = this._UserName;
            _Res._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Res._StaffPic = this._StaffPic;
            _Res.StaffImage.Source = StaffImage.Source;
            _Res.Owner = this.Owner;

            this.Close();
            _Res.ShowDialog();
        }

        private void btnUserAccessRights_Click(object sender, RoutedEventArgs e)
        {
            StaffAccess _StaffAccess = new StaffAccess();
            _StaffAccess._CompanyName = this._CompanyName;
            _StaffAccess._UserName = this._UserName;
            _StaffAccess._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _StaffAccess._StaffPic = this._StaffPic;
            _StaffAccess.StaffImage.Source = StaffImage.Source;
            _StaffAccess.Owner = this.Owner;

            this.Close();
            _StaffAccess.ShowDialog();
        }

        private void btnEmailTSetup_Click(object sender, RoutedEventArgs e)
        {
            EmailTemplateSettings _eMailTemplate = new EmailTemplateSettings();
            _eMailTemplate._CompanyName = this._CompanyName;
            _eMailTemplate._UserName = this._UserName;
            _eMailTemplate._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _eMailTemplate._StaffPic = this._StaffPic;
            _eMailTemplate.StaffImage.Source = StaffImage.Source;
            _eMailTemplate.Owner = this.Owner;

            this.Close();
            _eMailTemplate.ShowDialog();
        }

        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDashboardQuit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
                //Closing event called
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnContactTypes_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
