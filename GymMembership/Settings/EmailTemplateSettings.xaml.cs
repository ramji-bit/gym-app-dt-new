﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration;
using System.Data;
using System.Windows.Threading;
using System.ComponentModel;
using System.IO;
using GymMembership.Accounts;
using GymMembership.Comman;

namespace GymMembership.Settings
{
    /// <summary>
    /// Interaction logic for EmailTemplateSettings.xaml
    /// </summary>
    public partial class EmailTemplateSettings 
    {
        public EmailTemplateSettings()
        {
            InitializeComponent();
        }
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public Login _Login;

        bool _isEdit = false; int _isUpdate = 0;
        int _templateID;
        int _CompanyID, _SiteID;
        private void EmailTemplate_Loaded(object sender, RoutedEventArgs e)
        {
            Utilities _Util = new Utilities();
            _Util.LoadEmailTemplates(cmbTemplate);

            txtBody.IsEnabled = false;
            txtSubject.IsEnabled = false;
            txtTemplateName.IsEnabled = false;
            //this.Title = "CompanyDetails";
            lblOrgName.Content = _CompanyName.ToString().Replace("_", "__");
            lblUserName.Content = _UserName.ToString().Replace("_", "__");
            //lblSiteName.Content = _SiteName.ToString().Replace("_", "__");
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto();
        }
        private void loadStaffPhoto()
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }


        private void cmbTemplate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbTemplate.SelectedIndex >= 0)
            {
                _templateID = Convert.ToInt32(cmbTemplate.SelectedValue.ToString());
                txtTemplateName.Text = (cmbTemplate.Items[cmbTemplate.SelectedIndex] as DataRowView).Row["EmailTemplateName"].ToString();

                PopulateTemplateDetails(_templateID);
            }
        }
        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            cmbTemplate.SelectedIndex = -1;
            txtTemplateName.Text = "";
            _isEdit = false; _isUpdate = 0;
            txtBody.Text = "";
            txtSubject.Text = "";

            txtBody.IsEnabled = true;
            txtSubject.IsEnabled = true;
            txtTemplateName.IsEnabled = true;
            txtTemplateName.Focus();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (validateData())
            {
                SqlHelper sql = new SqlHelper();
                Hashtable ht = new Hashtable();
                _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                string _UserName = ConfigurationManager.AppSettings["LoggedInUser"].ToString();

                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                ht.Add("@TemplateID", _templateID);
                ht.Add("@EmailTemplateName", txtTemplateName.Text.Trim().ToUpper());
                ht.Add("@EmailSubject", txtSubject.Text.Trim());
                ht.Add("@EmailBodyText", txtBody.Text.Trim());
                ht.Add("@UserName", _UserName);
                ht.Add("@isUpdate", _isUpdate);

                DataSet _dsEmailTemplate = new DataSet();
                _dsEmailTemplate = sql.ExecuteProcedure("SaveUpdateEmailTemplate", ht);

                if (Convert.ToInt32(_dsEmailTemplate.Tables[0].Rows[0]["Result"]) != 0)
                {
                    //SUCCESS
                    MessageBox.Show("Email Template Saved Successfully!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Close();
                }
                else
                {
                    //FAILED
                    MessageBox.Show("Email Template Save Failed!",this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
                    return;
                }
            }
        }
        private bool validateData()
        {
            if (txtTemplateName.Text.Trim() == "")
            {
                MessageBox.Show("Enter a Template Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtTemplateName.Focus();
                return false;
            }
            if (!_isEdit)
            {
                for (int i = 0; i < cmbTemplate.Items.Count; i++)
                {
                    string s = (cmbTemplate.Items[i] as DataRowView).Row["EmailTemplateName"].ToString().ToUpper();
                    if (txtTemplateName.Text.Trim().ToUpper() == s)
                    {
                        MessageBox.Show("Email Template Name Already Exists! Please Enter a Different Name!");
                        txtTemplateName.Text = "";
                        txtTemplateName.Focus();
                        return false;
                    }
                }
            }
            else
            {
                int _sIndex = cmbTemplate.SelectedIndex;
                for (int i = 0; i < cmbTemplate.Items.Count; i++)
                {
                    if (i != _sIndex)
                    {
                        string s = (cmbTemplate.Items[i] as DataRowView).Row["EmailTemplateName"].ToString().ToUpper();
                        if (txtTemplateName.Text.Trim().ToUpper() == s)
                        {
                            MessageBox.Show("Email Template Name Already Exists! Please Enter a Different Name!");
                            txtTemplateName.Text = "";
                            txtTemplateName.Focus();
                            return false;
                        }
                    }
                }
            }

            if (txtSubject.Text.Trim() == "")
            {
                MessageBox.Show("Enter Email Subject!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtSubject.Focus();
                return false;
            }

            if (txtBody.Text.Trim() == "")
            {
                MessageBox.Show("Enter Body of the Email!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtBody.Focus();
                return false;
            }
            return true;
        }
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (_templateID <= 0)
            {
                MessageBox.Show("Select an Email Template to Edit!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                cmbTemplate.Focus();
                txtBody.IsEnabled = false;
                txtSubject.IsEnabled = false;
                txtTemplateName.IsEnabled = false;
                return;
            }

            _isEdit = true;
            _isUpdate = 1;
            txtBody.IsEnabled = true;
            txtSubject.IsEnabled = true;
            txtTemplateName.IsEnabled = true;
            txtTemplateName.Focus();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void PopulateTemplateDetails(int _templateID)
        { 
            txtBody.IsEnabled = false;
            txtSubject.IsEnabled = false;
            txtTemplateName.IsEnabled = false;
            Utilities _Util = new Utilities();

            if (_templateID != -1)
            {
                DataSet ds = _Util.LoadEmailTemplateByID(_templateID);
                if (ds != null)
                {
                    if (ds.Tables[0].DefaultView != null)
                    {
                        txtSubject.Text = ds.Tables[0].Rows[0]["EmailSubject"].ToString();
                        txtBody.Text = ds.Tables[0].Rows[0]["EmailBodyText"].ToString();
                    }
                }
            }
        }


        private void btnContactTypes1_Click(object sender, RoutedEventArgs e)
        {
            CompanyDetails _CompanySiteDetails = new CompanyDetails();
            _CompanySiteDetails._CompanyName = this._CompanyName;
            _CompanySiteDetails._UserName = this._UserName;
            _CompanySiteDetails._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _CompanySiteDetails._StaffPic = this._StaffPic;
            _CompanySiteDetails.StaffImage.Source = StaffImage.Source;
            _CompanySiteDetails.Owner = this.Owner;

            this.Close();
            _CompanySiteDetails.ShowDialog();
        }

        private void btnUserAdministration_Click(object sender, RoutedEventArgs e)
        {
            if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
            {
                UserAdministration useradmin = new UserAdministration();
                useradmin._CompanyName = this._CompanyName;
                useradmin._UserName = this._UserName;
                useradmin._CurrentLoginDateTime = this._CurrentLoginDateTime;
                useradmin._StaffPic = this._StaffPic;
                useradmin.StaffImage.Source = StaffImage.Source;
                useradmin.Owner = this.Owner;

                this.Close();
                useradmin.ShowDialog();
            }
            else
            {
                MessageBox.Show("You MUST be Admin user to access this Screen!");
                return;
            }
        }

        private void btnProductsTypes_Click(object sender, RoutedEventArgs e)
        {

            ProductType prdType = new ProductType();
            prdType._CompanyName = this._CompanyName;
            prdType._UserName = this._UserName;
            prdType._CurrentLoginDateTime = this._CurrentLoginDateTime;
            prdType._StaffPic = this._StaffPic;
            prdType.StaffImage.Source = StaffImage.Source;
            prdType.Owner = this.Owner;

            this.Close();
            prdType.ShowDialog();
        }

        private void btnManageProducts_Click(object sender, RoutedEventArgs e)
        {
            Products products = new Products();
            products._CompanyName = this._CompanyName;
            products._UserName = this._UserName;
            products._CurrentLoginDateTime = this._CurrentLoginDateTime;
            products._StaffPic = this._StaffPic;
            products.StaffImage.Source = StaffImage.Source;
            products.Owner = this.Owner;

            this.Close();
            products.ShowDialog();
        }

        private void btnProgrammeGSetup_Click(object sender, RoutedEventArgs e)
        {
            ProgrammeGroup PrgGroup = new ProgrammeGroup();
            PrgGroup._CompanyName = this._CompanyName;
            PrgGroup._UserName = this._UserName;
            PrgGroup._CurrentLoginDateTime = this._CurrentLoginDateTime;
            PrgGroup._StaffPic = this._StaffPic;
            PrgGroup.StaffImage.Source = StaffImage.Source;
            PrgGroup.Owner = this.Owner;

            this.Close();
            PrgGroup.ShowDialog();
        }

        private void btnAddEditProgrammes_Click(object sender, RoutedEventArgs e)
        {
            Programme Prg = new Programme();
            Prg._CompanyName = this._CompanyName;
            Prg._UserName = this._UserName;
            Prg._CurrentLoginDateTime = this._CurrentLoginDateTime;
            Prg._StaffPic = this._StaffPic;
            Prg.StaffImage.Source = StaffImage.Source;
            Prg.Owner = this.Owner;

            this.Close();
            Prg.ShowDialog();
        }

        private void btnSupplierSetup_Click(object sender, RoutedEventArgs e)
        {
            Supplier _Supplier = new Supplier();
            _Supplier._CompanyName = this._CompanyName;
            _Supplier._UserName = this._UserName;
            _Supplier._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Supplier._StaffPic = this._StaffPic;
            _Supplier.StaffImage.Source = StaffImage.Source;
            _Supplier.Owner = this.Owner;

            this.Close();
            _Supplier.ShowDialog();
        }

        private void btnBookingResources_Click(object sender, RoutedEventArgs e)
        {
            Settings.Resources _Res = new Settings.Resources();
            _Res._CompanyName = this._CompanyName;
            _Res._UserName = this._UserName;
            _Res._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Res._StaffPic = this._StaffPic;
            _Res.StaffImage.Source = StaffImage.Source;
            _Res.Owner = this.Owner;

            this.Close();
            _Res.ShowDialog();
        }

        private void btnUserAccessRights_Click(object sender, RoutedEventArgs e)
        {
            StaffAccess _StaffAccess = new StaffAccess();
            _StaffAccess._CompanyName = this._CompanyName;
            _StaffAccess._UserName = this._UserName;
            _StaffAccess._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _StaffAccess._StaffPic = this._StaffPic;
            _StaffAccess.StaffImage.Source = StaffImage.Source;
            _StaffAccess.Owner = this.Owner;

            this.Close();
            _StaffAccess.ShowDialog();
        }

        private void btnEmailTSetup_Click(object sender, RoutedEventArgs e)
        {
            //SAME WINDOW; NO ACTION;

            //EmailTemplateSettings _eMailTemplate = new EmailTemplateSettings();
            //_eMailTemplate._CompanyName = this._CompanyName;
            //_eMailTemplate._UserName = this._UserName;
            //_eMailTemplate._CurrentLoginDateTime = this._CurrentLoginDateTime;
            //_eMailTemplate._StaffPic = this._StaffPic;
            //_eMailTemplate.Owner = this.Owner;

            //this.Close();
            //_eMailTemplate.ShowDialog();
        }


        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnContactTypes_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnDashboardQuit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
                //Closing event called
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

    }
}
