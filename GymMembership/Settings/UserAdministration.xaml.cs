﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Data;
using System.ComponentModel;
using System.Configuration;
using System.Windows.Media.Imaging;
using System.IO;

using GymMembership.Comman;
using GymMembership.BAL;
using GymMembership.MemberInfo;
using GymMembership.DTO;
using GymMembership.Accounts;
using WebEye.Controls.Wpf;

namespace GymMembership.Settings
{
    /// <summary>
    /// Interaction logic for UserAdministration.xaml
    /// </summary>
    public partial class UserAdministration 
    {
        //Utilities _uTil = new Utilities();
        //UserDetailsBAL _userBAL = new UserDetailsBAL();
        
        FormUtilities _FormUtil = new FormUtilities();
        UserLoginDTO _userLoginDTO = new UserLoginDTO();

        bool _IsImageEdited = false;
        public bool _IsCameraStarted = false; bool _IsCaptured = false; public bool _IsWebCamConnected = false;
        List<WebCameraId> cameras;

        int _SiteIDUAdmin;
        int _UserRole = 0; int _Gender = 0; int _StaffRoleID = 0;
        int _StaffID = 0; int _LoginID = 0; bool _UserStatus;
        bool _isEdit = false; bool _isMember = false; 
        public bool _newOrgDefaultAdminUser = false;
        //webCam _webcam = new webCam();
        string fileNamePath_MemberPhoto = "";
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public Login _Login;
        public UserAdministration()
        {
            InitializeComponent();
            this.Closing += new CancelEventHandler(UserAdmin_Closing);
        }
        void UserAdmin_Closing(object sender, CancelEventArgs e)
        {
            if (_newOrgDefaultAdminUser)
            {
                if ( System.Windows.MessageBox.Show("New Organization is being Set Up! \n\nIf Close You may not be able to access the Application!" +
                         "\n\nAre You Sure to Close?",this.Title,MessageBoxButton.YesNo,MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (_IsCameraStarted)
                    {
                        //_webcam.Stop();
                        //_webcam = null;

                        if (!_IsCaptured)
                        { _webCamMS.StopCapture(); }
                    }
                    return;
                }
                e.Cancel = true;
                return;
            }
            else
            {
                if (_IsCameraStarted)
                {
                    //_webcam.Stop();
                    //_webcam = null;
                    if (!_IsCaptured)
                    { _webCamMS.StopCapture(); }
                }
                return;
            }
        }
        private void btnDetailsCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        
        private void chkDetailsIsMember_Checked(object sender, RoutedEventArgs e)
        {
            cmbDetailsMemberID.Visibility = Visibility.Visible;
            lblDetailsMemberID.Visibility = Visibility.Visible;
            if (!_isEdit)
            {
                cmbDetailsMemberID.SelectedIndex = -1;
            }
        }
        private void chkDetailsIsMember_UnChecked(object sender, RoutedEventArgs e)
        {
            cmbDetailsMemberID.Visibility = Visibility.Hidden;
            lblDetailsMemberID.Visibility = Visibility.Hidden;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cameras = new List<WebCameraId>(_webCamMS.GetVideoCaptureDevices());
            if (cameras.Count <= 0) { _IsWebCamConnected = false; } else { _IsWebCamConnected = true; }

            ClearALLDetails();
            LoadDetails();
            if (_newOrgDefaultAdminUser == true)
            {
                rbtnUadminRoleAdmin.IsEnabled = false;
                rbtnUadminRoleAdmin.IsChecked = true;
                rbtnUadminRoleUser.IsChecked = false;
                rbtnUadminRoleUser.IsEnabled = false;
                chkDetailsIsMember.Visibility = Visibility.Hidden;
                btnDetailsCancel.IsEnabled = false;

                listBox.IsEnabled = false;
            }
            else
            {
                rbtnUadminRoleAdmin.IsEnabled = true;
                rbtnUadminRoleAdmin.IsChecked = false;
                rbtnUadminRoleUser.IsChecked = true;
                rbtnUadminRoleUser.IsEnabled = true;
                chkDetailsIsMember.Visibility = Visibility.Visible;
                btnDetailsCancel.IsEnabled = true;
                listBox.IsEnabled = true;

                //this.Title = "CompanyDetails";
                lblOrgName.Content = _CompanyName.ToString().Replace("_", "__");
                lblUserName.Content = _UserName.ToString().Replace("_", "__");
                //lblSiteName.Content = _SiteName.ToString().Replace("_", "__");
                lblLoggedInTime.Content = _CurrentLoginDateTime;
                //loadStaffPhoto();
            }
        }

        private void loadStaffPhoto()
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }

        private void btnCheckLoginAvailability_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbUadminCompany.SelectedIndex == -1)
                {
                    System.Windows.MessageBox.Show("Please Select Organization!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    cmbUadminCompany.Focus();
                    return;
                }
                if (cmbUadminSite.SelectedIndex == -1)
                {
                    System.Windows.MessageBox.Show("Please Select Site/Branch!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    cmbUadminSite.Focus();
                    return;
                }
                if (txtUadminLoginName.Text.Trim() == "")
                {
                    System.Windows.MessageBox.Show("Enter Login Name", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtUadminLoginName.Focus();
                    return;
                }
                CheckAndLoadExistingUser(false);
            }
            catch(Exception ex)
            {
                System.Windows.MessageBox.Show("Error ! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private void CheckAndLoadExistingUser(bool _fromExistingUserSelection)
        {
            string _CompanyName = cmbUadminCompany.Text.ToUpper().Trim();
            string _SiteName = cmbUadminSite.Text.ToUpper().Trim();
            string _LoginName;

            if (!_fromExistingUserSelection)
            {
                _LoginName = txtUadminLoginName.Text.ToUpper().Trim();
            }
            else
            {
                _LoginName = (cmbUadminLoginName.SelectedItem as DataRowView).Row["UserName"].ToString();
                txtUadminLoginName.Text = _LoginName;
            }

            UserDetailsBAL _userBAL = new UserDetailsBAL();
            DataSet ds = _userBAL.CheckIfUserAlreadyExists(_CompanyName, _SiteName, _LoginName);
            if (Convert.ToInt32(ds.Tables[0].Rows[0]["Result"] == DBNull.Value ? 0 : ds.Tables[0].Rows[0][0]) == 1)
            {
                if (!_fromExistingUserSelection)
                {
                    if (System.Windows.MessageBox.Show("User Already Exists for this Organization! Do you want to fetch the Details?", "User Administration", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    {
                        ClearDetails();
                        txtUadminLoginName.Text = "";
                        txtUadminLoginName.IsEnabled = true;
                        txtUadminLoginName.Focus();
                        ds = null;
                        return;
                    }
                    else
                    {
                        ShowExistingUserDetails(ds);
                    }
                }
                else
                {
                    ShowExistingUserDetails(ds);
                }
            }
            else
            {
                if (!_fromExistingUserSelection)
                {
                    System.Windows.MessageBox.Show("User Name is Available!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    btnCheckLoginAvailability.IsEnabled = false;
                    chkIsActive.IsChecked = true;
                    chkIsActive.IsEnabled = true;
                    chkTimeOutNeverExpire.IsChecked = false;
                    chkTimeOutNeverExpire.IsEnabled = true;

                    ClearDetails();
                    txtUadminPassword.IsEnabled = true;
                    if (_newOrgDefaultAdminUser == true)
                    {
                        rbtnUadminRoleAdmin.IsEnabled = false;
                        rbtnUadminRoleAdmin.IsChecked = true;
                        rbtnUadminRoleUser.IsChecked = false;
                        rbtnUadminRoleUser.IsEnabled = false;
                    }
                    else
                    {
                        rbtnUadminRoleAdmin.IsEnabled = true;
                        rbtnUadminRoleAdmin.IsChecked = false;
                        rbtnUadminRoleUser.IsChecked = true;
                        rbtnUadminRoleUser.IsEnabled = true;
                    }
                    gbDetails.IsEnabled = true;

                    StaffPic.IsEnabled = true;
                    btnCapture.IsEnabled = true;
                    btnStartCamera.IsEnabled = true;

                    txtUadminPassword.Focus();
                    txtUadminLoginName.IsEnabled = false;
                }
            }
        }
        private void rbtnUadminRoleAdmin_Checked(object sender, RoutedEventArgs e)
        {
            _UserRole = 2;
        }

        private void rbtnUadminRoleUser_Checked(object sender, RoutedEventArgs e)
        {
            _UserRole = 1;
        }

        private void ClearDetails()
        {
            btnDetailsAdd.Content = "Add"; _isEdit = false;
            txtUadminPassword.Password = "";
            txtDetailsLastName.Text = ""; txtDetailsFirstName.Text = "";txtDetailsDOB.Text = "";
            txtDetailsStreet.Text = ""; txtDetailsSuburb.Text = ""; txtDetailsMobilePhone.Text = "";
            txtDetailsHomePhone.Text = ""; txtDetailsPostalCode.Text = "";
            txtDetailsEmailID.Text = "";
            txtUadminPassword.IsEnabled = false;
            rbtnUadminRoleUser.IsEnabled = false;
            rbtnUadminRoleAdmin.IsEnabled = false;
            chkDetailsIsMember.IsChecked = false;
        }

        private void ClearALLDetails()
        {
            btnDetailsAdd.Content = "Add";_isEdit = false;
            txtUadminLoginName.IsEnabled = true;

            cmbUadminSite.IsEnabled = true;
            cmbUadminSite.SelectedIndex = -1;

            cmbUadminLoginName.SelectedIndex = -1;
            cmbUadminLoginName.Text = "";

            cmbUadminLoginName.Visibility = Visibility.Hidden;
            txtUadminLoginName.Visibility = Visibility.Visible;
            btnCheckLoginAvailability.Visibility = Visibility.Visible;
            btnCheckLoginNewUser.Visibility = Visibility.Hidden;

            txtUadminLoginName.Text = "";
            txtUadminPassword.Password = "";
            txtDetailsLastName.Text = ""; txtDetailsFirstName.Text = ""; txtDetailsDOB.Text = "";
            txtDetailsStreet.Text = ""; txtDetailsSuburb.Text = ""; txtDetailsMobilePhone.Text = "";
            txtDetailsHomePhone.Text = ""; txtDetailsPostalCode.Text = "";
            txtDetailsEmailID.Text = "";

            lblDetailsMemberID.Visibility = Visibility.Hidden;
            cmbDetailsMemberID.Visibility = Visibility.Hidden;
            txtUadminPassword.IsEnabled = false;
            rbtnUadminRoleUser.IsEnabled = false;
            rbtnUadminRoleAdmin.IsEnabled = false;
            btnCheckLoginAvailability.IsEnabled = true;
            cmbDetailsStaffRole.SelectedIndex = -1;
            chkDetailsIsMember.IsChecked = false;
            chkIsActive.IsChecked = true;
            chkTimeOutNeverExpire.IsChecked = false;

            gbDetails.IsEnabled = false;

            StaffPic.IsEnabled = false;
            btnCapture.IsEnabled = false;
            btnStartCamera.IsEnabled = false;

            StaffPic_Img.Source = new BitmapImage(new Uri(@"..\Images\User_Image.png", UriKind.RelativeOrAbsolute));
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            ClearALLDetails();
        }

        private void btnDetailsAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ValidateDetails())
                {
                    SetUserLoginDTO();
                    if (_isEdit)
                    {
                        //call UPDATE EXISTING USER DETAILS
                        _userLoginDTO.LoginID = _LoginID;
                        _userLoginDTO.StaffID = _StaffID;

                        int Result;
                        UserDetailsBAL _userBAL = new UserDetailsBAL();
                        Result = _userBAL.UpdateLoginAndStaffDetails(_userLoginDTO);
                        if (Result == 0)
                        {
                            System.Windows.MessageBox.Show("Error! User details NOT Updated!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            ClearALLDetails();
                            return;
                        }
                        else
                        {
                            System.Windows.MessageBox.Show(" User details Updated Successfully!",this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
                            //ClearALLDetails();
                            //return;
                            this.Close();
                        }
                    }
                    else
                    {
                        //call INSERT NEW USER DETAILS
                        _userLoginDTO.LoginID = 0;
                        _userLoginDTO.StaffID = 0;
                        _userLoginDTO.UserStatus = 1;
                        
                        UserDetailsBAL _userBAL = new UserDetailsBAL();
                        DataSet ds = _userBAL.AddNEWLoginAndStaffDetails(_userLoginDTO);
                        int Result = Convert.ToInt32( ds.Tables[0].Rows[0]["Result"].ToString());
                        if (Result == 0)
                        {
                            System.Windows.MessageBox.Show( "Error! User details NOT added! Please refer log for more details..", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            ClearALLDetails();
                            return;
                        }
                        else
                        {
                            //ADD CODE TO INSERT MODULES/SUBMODULES
                            int _LoginID = Result;
                            int _AccessStatus; int _CompanyID;
                            if (rbtnUadminRoleAdmin.IsChecked == true)
                            {
                                _AccessStatus = 1;
                            }
                            else
                            {
                                _AccessStatus = 0;
                            }
                            _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                            int _AccessResult = _userBAL.UA_AddDefaultAccessRightsOnLoginCreation(_LoginID, _AccessStatus, _CompanyID, _SiteIDUAdmin);
                            if (_AccessResult == 0)
                            {
                                System.Windows.MessageBox.Show(" User Added Successfully! \n\nHowever Access Rights Errored..!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                            }
                            else
                            {
                                System.Windows.MessageBox.Show(" User Added Successfully!",this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
                            }
                            //ClearALLDetails();
                            //return;
                            _newOrgDefaultAdminUser = false;
                            this.Close();
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                System.Windows.MessageBox.Show("Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private bool ValidateDetails()
        {
            if (cmbUadminCompany.SelectedIndex ==-1)
            {
                System.Windows.MessageBox.Show("Please Select Organization Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                cmbUadminCompany.Focus();
                return false;
            }

            if (cmbUadminSite.SelectedIndex == -1)
            {
                System.Windows.MessageBox.Show("Please Select a Branch/Site!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                cmbUadminSite.Focus();
                return false;
            }
            if (txtUadminLoginName.Text.Trim()=="")
            {
                System.Windows.MessageBox.Show("Please Enter User Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtUadminLoginName.Focus();
                return false;
            }
            if (txtUadminPassword.Password.Trim() == "")
            {
                System.Windows.MessageBox.Show("Password cannot be Empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtUadminPassword.Focus();
                return false;
            }
            if ((rbtnUadminRoleAdmin.IsChecked == false) && (rbtnUadminRoleUser.IsChecked == false))
            {
                System.Windows.MessageBox.Show("Please Select User Role!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                rbtnUadminRoleUser.Focus();
                return false;
            }
            if (txtDetailsLastName.Text.Trim() == "")
            {
                System.Windows.MessageBox.Show("Last Name cannot be empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtDetailsLastName.Focus();
                return false;
            }
            if (txtDetailsFirstName.Text.Trim() == "")
            {
                System.Windows.MessageBox.Show("First Name cannot be empty!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtDetailsFirstName.Focus();
                return false;
            }
            if (txtDetailsDOB.Text.Trim() == "")
            {
                //System.Windows.MessageBox.Show("Date of Birth cannot be empty!",this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
                //txtDetailsDOB.Focus();
                //return false;
            }
            else
            {
                if (!_FormUtil.CheckDate(txtDetailsDOB.Text.Trim()))
                {
                    System.Windows.MessageBox.Show("Date of Birth - Date Format Incorrect!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtDetailsDOB.Focus();
                    return false;
                }
            }
            //if (txtDetailsStreet.Text.Trim() == "")
            //{
            //    System.Windows.MessageBox.Show("Street cannot be empty!",this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
            //    txtDetailsStreet.Focus();
            //    return false;
            //}
            //if (txtDetailsSuburb.Text.Trim() == "")
            //{
            //    System.Windows.MessageBox.Show("Suburb cannot be empty!",this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
            //    txtDetailsSuburb.Focus();
            //    return false;
            //}
            //if ((rbtnDetailsMale.IsChecked == false) && (rbtnDetailsFemale.IsChecked == false))
            //{
            //    System.Windows.MessageBox.Show("Please Select Gender!",this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
            //    rbtnDetailsMale.Focus();
            //    return false;
            //}
            //if ((txtDetailsMobilePhone.Text.Trim() == "") && (txtDetailsHomePhone.Text.Trim() == ""))
            //{
            //    System.Windows.MessageBox.Show("Either Home Phone OR Mobile Phone is mandatory!",this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
            //    txtDetailsHomePhone.Focus();
            //    return false;
            //}
            if (txtDetailsMobilePhone.Text.Trim() != "")
            {
                if (!txtDetailsMobilePhone.Text.All(c => Char.IsNumber(c)))
                {
                    System.Windows.MessageBox.Show("Please Enter Mobile Phone in Numeric!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtDetailsMobilePhone.Focus();
                    return false;
                }
            }

            ///// HOME PHONE REMOVED - TO ACCOMODATE 'EMAIL ID' FOR STAFF : 26 JUN 2017
            //if (txtDetailsHomePhone.Text.Trim() != "")
            //{
            //    if (!txtDetailsHomePhone.Text.All(c => Char.IsNumber(c)))
            //    {
            //        System.Windows.MessageBox.Show("Please Enter Home Phone in Numeric!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            //        txtDetailsHomePhone.Focus();
            //        return false;
            //    }
            //}

            if (txtDetailsEmailID.Text.Trim() != "")
            {
                if ((!txtDetailsEmailID.Text.Contains("@")) || (!txtDetailsEmailID.Text.Contains(".")))
                {
                    System.Windows.MessageBox.Show("Email ID not in correct Format!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtDetailsEmailID.Focus();
                    return false;
                }
                else if(txtDetailsEmailID.Text.Trim().LastIndexOf("@") != txtDetailsEmailID.Text.Trim().IndexOf("@"))
                {
                    System.Windows.MessageBox.Show("Email ID not in correct Format!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtDetailsEmailID.Focus();
                    return false;
                }
                else if (txtDetailsEmailID.Text.Trim().Length < 6)
                {
                    System.Windows.MessageBox.Show("Email ID not in correct Format!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtDetailsEmailID.Focus();
                    return false;
                }
            }
            ///// HOME PHONE REMOVED - TO ACCOMODATE 'EMAIL ID' FOR STAFF : 26 JUN 2017
            if (txtDetailsPostalCode.Text.Trim() == "")
            {
                //System.Windows.MessageBox.Show("Postal Code cannot be empty!",this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
                //txtDetailsPostalCode.Focus();
                //return false;
                txtDetailsPostalCode.Text = "0";
            }
            else
            {
                if (txtDetailsPostalCode.Text.Trim() != "")
                {
                    if (!txtDetailsPostalCode.Text.All(c => Char.IsNumber(c)))
                    {
                        System.Windows.MessageBox.Show("Please Enter Postal Code in Numbers.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        txtDetailsPostalCode.Focus();
                        return false;
                    }
                }
            }
            if (cmbDetailsStaffRole.SelectedIndex == -1)
            {
                System.Windows.MessageBox.Show("Please Select a Staff Role!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                cmbDetailsStaffRole.Focus();
                return false;
            }
            if (chkDetailsIsMember.IsChecked == true)
            {
                if (cmbDetailsMemberID.SelectedIndex == -1)
                {
                    System.Windows.MessageBox.Show("Please Select the Member Number, if this Staff is a member...", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    cmbDetailsMemberID.Focus();
                    return false;
                }
            }
            return true;
        }

        private void LoadDetails()
        {
            cmbUadminCompany.Items.Clear();
            cmbUadminCompany.Items.Add(ConfigurationManager.AppSettings["LoggedInOrg"]);
            cmbUadminCompany.SelectedIndex = 0;
        }

        public void SetUserLoginDTO()
        {
            _userLoginDTO.SiteID = _SiteIDUAdmin;
            _userLoginDTO.CompanyName = cmbUadminCompany.Text.ToUpper().Trim();
            _userLoginDTO.SiteName = cmbUadminSite.Text.ToUpper().Trim();
            _userLoginDTO.UserName = txtUadminLoginName.Text.Trim();
            //_userLoginDTO.Password = txtUadminPassword.Password;
            _userLoginDTO.Password = Constants.passwordEncrypt(txtUadminPassword.Password, Constants._EncryptKey);

            if (rbtnUadminRoleAdmin.IsChecked == true) { _UserRole = 2; }
            if (rbtnUadminRoleUser.IsChecked == true) { _UserRole = 1; }
            _userLoginDTO.UserRole = _UserRole;

            _userLoginDTO.LastName = txtDetailsLastName.Text.Trim().ToUpper();
            _userLoginDTO.FirstName = txtDetailsFirstName.Text.Trim().ToUpper();
            _userLoginDTO.DateOfBirth = txtDetailsDOB.Text.Trim();
            _userLoginDTO.Street = txtDetailsStreet.Text.Trim().ToUpper();

            if (rbtnDetailsMale.IsChecked == true) { _Gender = 1; }
            if (rbtnDetailsFemale.IsChecked == true) { _Gender = 2; }
            _userLoginDTO.Gender = _Gender;

            _userLoginDTO.Suburb = txtDetailsSuburb.Text.Trim().ToUpper();

            _userLoginDTO.HomePhone = txtDetailsHomePhone.Text.Trim();
            _userLoginDTO.EmailID = txtDetailsEmailID.Text.Trim();

            _userLoginDTO.MobilePhone = txtDetailsMobilePhone.Text.Trim();
            _userLoginDTO.PostalCode = Convert.ToInt32(txtDetailsPostalCode.Text.Trim());
            

            if (cmbDetailsStaffRole.SelectedIndex == 0) { _StaffRoleID = 1; }
            if (cmbDetailsStaffRole.SelectedIndex == 1) { _StaffRoleID = 2; }
            _userLoginDTO.StaffRoleID = _StaffRoleID;

            if (chkDetailsIsMember.IsChecked == true)
            {
                _isMember = true;
                _userLoginDTO.IsMember = 1;
                _userLoginDTO.MemberID = Convert.ToInt32(cmbDetailsMemberID.Text);
            }
            else
            {
                _isMember = false;
                _userLoginDTO.IsMember = 0;
                _userLoginDTO.MemberID = 0;
            }

            if (chkIsActive.IsChecked == true)
            {
                _userLoginDTO.UserStatus = 1;
            }
            else
            {
                _userLoginDTO.UserStatus = 0;
            }

            if (chkTimeOutNeverExpire.IsChecked == true)
            {
                _userLoginDTO.isAppTimeOutNeverExpire = true;
            }
            else
            {
                _userLoginDTO.isAppTimeOutNeverExpire = false;
            }

            if ((fileNamePath_MemberPhoto != null) && (fileNamePath_MemberPhoto != ""))
            {
                FileStream fs = new FileStream(fileNamePath_MemberPhoto, FileMode.Open, FileAccess.Read);
                byte[] data = new byte[fs.Length];
                //if (Convert.ToInt32(fs.Length) > 250000)
                //{
                //    //System.Windows.MessageBox.Show("FILE SIZE EXCEEDS ALLOWED LIMIT! \nPlease Select a different Photo to Upload!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                //    return;
                //}
                fs.Read(data, 0, System.Convert.ToInt32(fs.Length));
                fs.Close();
                _userLoginDTO.StaffPic = data;

                StaffPic_Img.Source = new BitmapImage(new Uri(fileNamePath_MemberPhoto, UriKind.RelativeOrAbsolute));
                _IsImageEdited = true;
            }
        }

        private void ShowExistingUserDetails(DataSet ds)
        {
            try
            {
                btnCheckLoginAvailability.IsEnabled = false;
                gbDetails.IsEnabled = true;

                StaffPic.IsEnabled = true;
                btnCapture.IsEnabled = true;
                btnStartCamera.IsEnabled = true;

                btnDetailsAdd.Content = "Update"; _isEdit = true;
                txtUadminLoginName.IsEnabled = false;
                txtUadminPassword.IsEnabled = true;
                rbtnUadminRoleUser.IsEnabled = true;
                rbtnUadminRoleAdmin.IsEnabled = true;

                //display details
                _StaffID = Convert.ToInt32(ds.Tables[0].Rows[0]["StaffID"]);
                _LoginID = Convert.ToInt32(ds.Tables[0].Rows[0]["LoginID"]);
                cmbUadminSite.Text = ds.Tables[0].Rows[0]["SiteName"].ToString();
                cmbUadminSite.IsEnabled = false;

                _UserRole = Convert.ToInt32(ds.Tables[0].Rows[0]["UserRole"]);

                //txtUadminPassword.Password =  ds.Tables[0].Rows[0]["Password"].ToString();
                txtUadminPassword.Password = Constants.passwordDecrypt(ds.Tables[0].Rows[0]["Password"].ToString(),Constants._EncryptKey);

                if (_UserRole == 1)
                {
                    rbtnUadminRoleUser.IsChecked = true;
                }
                else
                {
                    rbtnUadminRoleAdmin.IsChecked = true;
                }
                txtDetailsFirstName.Text = ds.Tables[0].Rows[0]["FirstName"].ToString();
                txtDetailsLastName.Text = ds.Tables[0].Rows[0]["LastName"].ToString();
                txtDetailsDOB.Text = ds.Tables[0].Rows[0]["DateofBirth"].ToString();
                txtDetailsStreet.Text = ds.Tables[0].Rows[0]["Street"].ToString();
                txtDetailsSuburb.Text = ds.Tables[0].Rows[0]["Suburb"].ToString();

                txtDetailsHomePhone.Text = ds.Tables[0].Rows[0]["HomePhone"].ToString();
                txtDetailsEmailID.Text = ds.Tables[0].Rows[0]["EmailID"].ToString();

                txtDetailsMobilePhone.Text = ds.Tables[0].Rows[0]["MobilePhone"].ToString();
                txtDetailsPostalCode.Text= ds.Tables[0].Rows[0]["PostalCode"].ToString();

                _isMember = Convert.ToBoolean(ds.Tables[0].Rows[0]["isMember"]);
                chkDetailsIsMember.IsChecked = _isMember;
                _UserStatus = Convert.ToBoolean(ds.Tables[0].Rows[0]["UserStatus"]);
                chkIsActive.IsChecked = _UserStatus;
                chkTimeOutNeverExpire.IsChecked = Convert.ToBoolean(ds.Tables[0].Rows[0]["isAppTimeOutNeverExpire"]);

                if (ConfigurationManager.AppSettings["LoggedInUser"].ToUpper().Trim() == txtUadminLoginName.Text.Trim().ToUpper())
                {
                    chkIsActive.IsEnabled = false;
                }
                else
                {
                    chkIsActive.IsEnabled = true;
                }
                if (_isMember == true)
                {
                    cmbDetailsMemberID.Visibility = Visibility.Visible;
                    cmbDetailsMemberID.Text = ds.Tables[0].Rows[0]["MemberID"].ToString();
                }

                if (Convert.ToInt32(ds.Tables[0].Rows[0]["Gender"]) == 1)
                {
                    rbtnDetailsMale.IsChecked = true;
                }
                else
                {
                    rbtnDetailsFemale.IsChecked = true;
                }

                if (Convert.ToInt32(ds.Tables[0].Rows[0]["StaffRoleID"]) == 2)
                {
                    cmbDetailsStaffRole.SelectedIndex = 1;
                }
                else
                {
                    cmbDetailsStaffRole.SelectedIndex = 0;
                }

                if (ds.Tables[0].Rows[0]["StaffPic"] != DBNull.Value)
                {
                    byte[] data = (byte[])ds.Tables[0].Rows[0]["StaffPic"];
                    if (data.Length > 4)
                    {
                        MemoryStream strm = new MemoryStream();
                        strm.Write(data, 0, data.Length);
                        strm.Position = 0;

                        System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                        BitmapImage bi = new BitmapImage();
                        bi.BeginInit();
                        MemoryStream ms = new MemoryStream();
                        img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                        ms.Seek(0, SeekOrigin.Begin);
                        bi.StreamSource = ms;
                        bi.EndInit();

                        StaffPic_Img.Source = bi;
                        _userLoginDTO.StaffPic = data; //(byte[])ds.Tables[0].Rows[0]["StaffPic"];
                    }
                    else
                    {
                        StaffPic_Img.Source = new BitmapImage(new Uri(@"..\Images\User_Image.png", UriKind.RelativeOrAbsolute));
                    }
                }
                else
                {
                    StaffPic_Img.Source = new BitmapImage(new Uri(@"..\Images\User_Image.png", UriKind.RelativeOrAbsolute));
                }
            }
            catch(Exception ex)
            {
                System.Windows.MessageBox.Show("Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void cmbUadminCompany_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Utilities _uTil = new Utilities();
            _uTil.populateSiteNamebyCompany(cmbUadminSite, cmbUadminCompany.SelectedItem.ToString());
            _uTil.populateSiteNamebyCompany(cmbUadminChangeToSite, cmbUadminCompany.SelectedItem.ToString());
        }

        private void Change_Staff_Photo(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                OpenFileDialog commonOpenFileDialog = new OpenFileDialog();
                commonOpenFileDialog.Filter = "Image Files|*.jpeg;*.jpg;*.png;*.bmp;*.tif;*.gif;|JPEG Files|*.jpg;*.jpeg|PNG Files|*.png|BMP Files|*.bmp|TIF Files|*.tif|GIF Files|*.gif";
                commonOpenFileDialog.Title = "Select Member Profile PHOTO...";
                commonOpenFileDialog.Multiselect = false;
                commonOpenFileDialog.ShowDialog();

                fileNamePath_MemberPhoto = commonOpenFileDialog.FileName;
                if (fileNamePath_MemberPhoto != "")
                {
                    FileStream fs = new FileStream(fileNamePath_MemberPhoto, FileMode.Open, FileAccess.Read);
                    byte[] data = new byte[fs.Length];
                    if (Convert.ToInt32(fs.Length) > 250000)
                    {
                        System.Windows.MessageBox.Show("FILE SIZE EXCEEDS ALLOWED LIMIT! \nPlease Select a different Photo to Upload!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                    fs.Read(data, 0, System.Convert.ToInt32(fs.Length));
                    fs.Close();
                    _userLoginDTO.StaffPic = data;

                    StaffPic_Img.Source = new BitmapImage(new Uri(fileNamePath_MemberPhoto, UriKind.RelativeOrAbsolute));
                    _IsImageEdited = true;
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error! NOT a Valid Image File! \nError Details: ( " + ex.Message + " )", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private void Change_Staff_Photo_USB()
        {
            try
            {
                if (fileNamePath_MemberPhoto != "")
                {
                    FileStream fs = new FileStream(fileNamePath_MemberPhoto, FileMode.Open, FileAccess.Read);
                    byte[] data = new byte[fs.Length];

                    fs.Read(data, 0, System.Convert.ToInt32(fs.Length));
                    fs.Close();
                    _userLoginDTO.StaffPic = data;

                    StaffPic_Img.Source = new BitmapImage(new Uri(fileNamePath_MemberPhoto, UriKind.RelativeOrAbsolute));
                    _IsImageEdited = true;
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error! NOT a Valid Image File! \nError Details: ( " + ex.Message + " )", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private void btnStartCamera_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!_IsCameraStarted)
                {
                    //_webcam = new webCam();
                    //_webcam.InitializeWebCam(ref StaffPic_Img);
                    //_webcam.Start();
                    //_IsCameraStarted = true;
                    if (!_IsWebCamConnected)
                    {
                        cameras = new List<WebCameraId>(_webCamMS.GetVideoCaptureDevices());
                        if (cameras.Count <= 0) { _IsWebCamConnected = false; } else { _IsWebCamConnected = true; }
                    }

                    if (_IsWebCamConnected)
                    {
                        StaffPic_Img.Visibility = Visibility.Hidden;
                        _webCamMS.Visibility = Visibility.Visible;
                        _IsCameraStarted = true;
                        _webCamMS.StartCapture(cameras[0]);
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("No WebCam Detected!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("WebCam may not be properly Installed! Unable to start WebCam!\n\n(Details: " + ex.Message + " )", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //_webcam = null;
            }
        }

        private void btnCapture_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (_IsCameraStarted)
                //{
                //    StaffPic_Img.Source = StaffPic_Img.Source;
                //    Helper.SaveImageCapture((BitmapSource)StaffPic_Img.Source);
                //    _IsImageEdited = true;
                //    fileNamePath_MemberPhoto = Comman.Constants.AppPath + "\\Member.JPG";
                //    _webcam.Stop();
                //    _webcam = null;
                //    _IsCaptured = true;
                //    _IsCameraStarted = false;
                //}
                if (_IsCameraStarted)
                {
                    StaffPic_Img.Source = Constants._getBitMapImage(_webCamMS.GetCurrentImage());
                    fileNamePath_MemberPhoto = Comman.Constants.AppPath + "\\" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".JPG";
                    Helper.SaveImageCapture((BitmapSource)StaffPic_Img.Source,fileNamePath_MemberPhoto);

                    _IsImageEdited = true;
                    _IsCaptured = true;
                    _IsCameraStarted = false;

                    StaffPic_Img.Visibility = Visibility.Visible;
                    _webCamMS.Visibility = Visibility.Hidden;
                    _webCamMS.StopCapture();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("WebCam may not be properly Installed! Unable to capture Image!\n\n(Details: " + ex.Message + " )", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //_webcam = null;
            }
        }

        private void btnContactTypes1_Click(object sender, RoutedEventArgs e)
        {
            CompanyDetails _CompanySiteDetails = new CompanyDetails();
            _CompanySiteDetails._CompanyName = this._CompanyName;
            _CompanySiteDetails._UserName = this._UserName;
            _CompanySiteDetails._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _CompanySiteDetails._StaffPic = this._StaffPic;
            _CompanySiteDetails.StaffImage.Source = StaffImage.Source;
            _CompanySiteDetails.Owner = this.Owner;

            this.Close();
            _CompanySiteDetails.ShowDialog();
        }

        private void btnUserAdministration_Click(object sender, RoutedEventArgs e)
        {
            ////SAME WINDOW; NO ACTION

            //if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
            //{
            //    UserAdministration useradmin = new UserAdministration();
            //    useradmin._CompanyName = this._CompanyName;
            //    useradmin._UserName = this._UserName;
            //    useradmin._CurrentLoginDateTime = this._CurrentLoginDateTime;
            //    useradmin._StaffPic = this._StaffPic;
            //    useradmin.StaffImage.Source = StaffImage.Source;
            //    useradmin.Owner = this.Owner;

            //    this.Close();
            //    useradmin.ShowDialog();
            //}
            //else
            //{
            //    MessageBox.Show("You MUST be Admin user to access this Screen!",this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
            //    return;
            //}
        }

        private void btnProductsTypes_Click(object sender, RoutedEventArgs e)
        {

            ProductType prdType = new ProductType();
            prdType._CompanyName = this._CompanyName;
            prdType._UserName = this._UserName;
            prdType._CurrentLoginDateTime = this._CurrentLoginDateTime;
            prdType._StaffPic = this._StaffPic;
            prdType.StaffImage.Source = StaffImage.Source;
            prdType.Owner = this.Owner;

            this.Close();
            prdType.ShowDialog();
        }

        private void btnManageProducts_Click(object sender, RoutedEventArgs e)
        {
            Products products = new Products();
            products._CompanyName = this._CompanyName;
            products._UserName = this._UserName;
            products._CurrentLoginDateTime = this._CurrentLoginDateTime;
            products._StaffPic = this._StaffPic;
            products.StaffImage.Source = StaffImage.Source;
            products.Owner = this.Owner;

            this.Close();
            products.ShowDialog();
        }

        private void btnProgrammeGSetup_Click(object sender, RoutedEventArgs e)
        {
            ProgrammeGroup PrgGroup = new ProgrammeGroup();
            PrgGroup._CompanyName = this._CompanyName;
            PrgGroup._UserName = this._UserName;
            PrgGroup._CurrentLoginDateTime = this._CurrentLoginDateTime;
            PrgGroup._StaffPic = this._StaffPic;
            PrgGroup.StaffImage.Source = StaffImage.Source;
            PrgGroup.Owner = this.Owner;

            this.Close();
            PrgGroup.ShowDialog();
        }

        private void btnAddEditProgrammes_Click(object sender, RoutedEventArgs e)
        {
            Programme Prg = new Programme();
            Prg._CompanyName = this._CompanyName;
            Prg._UserName = this._UserName;
            Prg._CurrentLoginDateTime = this._CurrentLoginDateTime;
            Prg._StaffPic = this._StaffPic;
            Prg.StaffImage.Source = StaffImage.Source;
            Prg.Owner = this.Owner;

            this.Close();
            Prg.ShowDialog();
        }

        private void btnSupplierSetup_Click(object sender, RoutedEventArgs e)
        {
            Supplier _Supplier = new Supplier();
            _Supplier._CompanyName = this._CompanyName;
            _Supplier._UserName = this._UserName;
            _Supplier._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Supplier._StaffPic = this._StaffPic;
            _Supplier.StaffImage.Source = StaffImage.Source;
            _Supplier.Owner = this.Owner;

            this.Close();
            _Supplier.ShowDialog();
        }

        private void btnBookingResources_Click(object sender, RoutedEventArgs e)
        {
            Settings.Resources _Res = new Settings.Resources();
            _Res._CompanyName = this._CompanyName;
            _Res._UserName = this._UserName;
            _Res._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Res._StaffPic = this._StaffPic;
            _Res.StaffImage.Source = StaffImage.Source;
            _Res.Owner = this.Owner;

            this.Close();
            _Res.ShowDialog();
        }

        private void btnUserAccessRights_Click(object sender, RoutedEventArgs e)
        {
            StaffAccess _StaffAccess = new StaffAccess();
            _StaffAccess._CompanyName = this._CompanyName;
            _StaffAccess._UserName = this._UserName;
            _StaffAccess._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _StaffAccess._StaffPic = this._StaffPic;
            _StaffAccess.StaffImage.Source = StaffImage.Source;
            _StaffAccess.Owner = this.Owner;

            this.Close();
            _StaffAccess.ShowDialog();
        }

        private void btnEmailTSetup_Click(object sender, RoutedEventArgs e)
        {
            EmailTemplateSettings _eMailTemplate = new EmailTemplateSettings();
            _eMailTemplate._CompanyName = this._CompanyName;
            _eMailTemplate._UserName = this._UserName;
            _eMailTemplate._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _eMailTemplate._StaffPic = this._StaffPic;
            _eMailTemplate.StaffImage.Source = StaffImage.Source;
            _eMailTemplate.Owner = this.Owner;

            this.Close();
            _eMailTemplate.ShowDialog();
        }

        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDashboardQuit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
                //Closing event called
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void cmbUadminsite_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            try
            {
                Utilities _uTil = new Utilities();
                _SiteIDUAdmin = Convert.ToInt32(cmbUadminSite.SelectedValue);
                _uTil.populateMemberNumber(cmbDetailsMemberID, _SiteIDUAdmin);

                LoadUserNames();

                cmbUadminLoginName.Visibility = Visibility.Visible;
                txtUadminLoginName.Text = "";
                txtUadminLoginName.Visibility = Visibility.Hidden;
                btnCheckLoginAvailability.Visibility = Visibility.Hidden;
                btnCheckLoginNewUser.Visibility = Visibility.Visible;
                btnDetailsAdd.IsEnabled = false;
            }
            catch(Exception)
            {

            }
        }
        private void LoadUserNames()
        {
            int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            int _SiteID = _SiteIDUAdmin;

            SqlHelper _sql = new SqlHelper();
            Hashtable ht = new Hashtable();

            ht.Add("@CompanyID", _CompanyID);
            ht.Add("@SiteID", _SiteID);
            cmbUadminLoginName.ItemsSource = null;
            cmbUadminLoginName.Items.Clear();

            DataSet _dsUsers  = _sql.ExecuteProcedure("GetAllUsersUAdmin", ht);
            if (_dsUsers != null)
            {
                if (_dsUsers.Tables.Count > 0)
                {
                    if (_dsUsers.Tables[0].DefaultView.Count > 0)
                    {
                        cmbUadminLoginName.ItemsSource = _dsUsers.Tables[0].DefaultView;
                        cmbUadminLoginName.SelectedValuePath = "LoginID";
                        cmbUadminLoginName.DisplayMemberPath = "Name";
                    }
                }
            }
            cmbUadminLoginName.SelectedIndex = -1;
            cmbUadminLoginName.Text = "";
    }

        private void btnContactTypes_Click(object sender, RoutedEventArgs e)
        {

        }

        private void cmbUadminLoginName_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (cmbUadminLoginName.SelectedIndex != -1)
            {
                CheckAndLoadExistingUser(true);
                _isEdit = true;
                btnDetailsAdd.IsEnabled = true;
            }
        }

        private void btnCheckLoginNewUser_Click(object sender, RoutedEventArgs e)
        {
            ClearDetails();

            cmbUadminLoginName.Visibility = Visibility.Hidden;
            txtUadminLoginName.Text = "";
            txtUadminLoginName.IsEnabled = true;

            txtUadminLoginName.Visibility = Visibility.Visible;
            btnCheckLoginAvailability.Visibility = Visibility.Visible;
            btnCheckLoginAvailability.IsEnabled = true;

            btnCheckLoginNewUser.Visibility = Visibility.Hidden;
            txtUadminLoginName.Focus();
            _isEdit = false;
            StaffPic_Img.Source = new BitmapImage(new Uri(@"..\Images\User_Image.png", UriKind.RelativeOrAbsolute));
            btnDetailsAdd.IsEnabled = true;
        }
    }
}
