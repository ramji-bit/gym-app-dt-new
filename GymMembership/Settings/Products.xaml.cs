﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration;
using System.Data;
using System.Windows.Threading;
using System.ComponentModel;
using System.IO;
using GymMembership.Comman;
using GymMembership.BAL;
using System.Text.RegularExpressions;
using GymMembership.Accounts;

namespace GymMembership.Settings
{
    /// <summary>
    /// Interaction logic for Products.xaml
    /// </summary>
    public partial class Products 
    {
        Utilities _util = new Utilities();
        ComboBox cmbProdSalePrice = new ComboBox();
        ComboBox cmbProdCostPrice = new ComboBox();
        private bool _isEdit = false;
        //Product _Prod = new Product();
        //SettingsBAL _settingDetailsBAL = new SettingsBAL();
        //private int _productId = 0;
        //private int _productTypeId = 0;
        //private Decimal _productCostPrice;
        //private Decimal _productSellingPrice;
        //private string _productName;
        //private string _existingProdName;
        //private int _addSuccess;

        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public Login _Login;
        DataSet ds = new DataSet();
        public Products()
        {
            InitializeComponent();
        }

        private void btnProductDetailsCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _util.populateProductTypes(cmbProductType);
            txtProductDetailsSellingPrice.IsEnabled = false;

            txtAlertEmail.IsEnabled = false;
            txtReOrderQty.IsEnabled = false;

            txtProductDetailsName.IsEnabled = false;
            chkProductDetailsStatus.IsEnabled = false;
            txtProductDetailsCostPrice.IsEnabled = false;
            btnProductDetailsSave.IsEnabled = false;
            btnProductDetailsEdit.IsEnabled = false;
            btnProductDetailsNew.IsEnabled = false;
            //this.Title = "CompanyDetails";
            lblOrgName.Content = _CompanyName.ToString().Replace("_", "__");
            lblUserName.Content = _UserName.ToString().Replace("_", "__");
            //lblSiteName.Content = _SiteName.ToString().Replace("_", "__");
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto();
        }
        private void loadStaffPhoto()
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }
        private void populateProductsByType(string _prodType)
        {
            Product _utilProd = new Product();
            
            ds = _utilProd.getProductsbyType(_prodType);
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbProduct.ItemsSource = ds.Tables[0].DefaultView;
                        cmbProduct.DisplayMemberPath = "ProductName";
                        cmbProduct.SelectedValuePath = "ProductID";
                        cmbProduct.SelectedIndex = 0;

                        cmbProdCostPrice.ItemsSource = ds.Tables[0].DefaultView;
                        cmbProdCostPrice.DisplayMemberPath = "ProductCostPrice";
                        cmbProdCostPrice.SelectedIndex = 0;

                        cmbProdSalePrice.ItemsSource = ds.Tables[0].DefaultView;
                        cmbProdSalePrice.DisplayMemberPath = "ProductSalePrice";
                        cmbProdSalePrice.SelectedIndex = 0;
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        private void cmbProductType_LostFocus(object sender, RoutedEventArgs e)
        {
            //_util.populateProductsByType(cmbProduct, cmbProductType.Text, cmbProdSalePrice, cmbProdCostPrice);
            string _prodType = (cmbProductType.SelectedItem as DataRowView).Row["ProductTypeName"].ToString();
            populateProductsByType(_prodType);

            cmbProduct.IsEnabled = true;
            txtProductDetailsSellingPrice.Text = string.Empty;
            txtProductDetailsName.Text = string.Empty;
            txtProductDetailsCostPrice.Text = string.Empty;
        }
        
        private void btnProductDetailsNew_Click(object sender, RoutedEventArgs e)
        {
            _isEdit = false;
            btnProductDetailsEdit.IsEnabled = false;
            cmbProduct.SelectedIndex = -1;
            //cmbProduct.IsEnabled = false;
            txtProductDetailsName.Text = string.Empty;
            txtProductDetailsCostPrice.Text = string.Empty;
            txtProductDetailsSellingPrice.Text = string.Empty;
            txtProductDetailsName.IsEnabled = true;
            
            txtProductDetailsCostPrice.IsEnabled = true;
            txtProductDetailsSellingPrice.IsEnabled = true;

            txtAlertEmail.IsEnabled = true;
            txtReOrderQty.IsEnabled = true;

            btnProductDetailsSave.IsEnabled = true;
            txtProductDetailsName.Focus();
            btnProductDetailsNew.IsEnabled = false;

            chkProductDetailsStatus.IsEnabled = false;
            chkProductDetailsStatus.IsChecked = true;
            txtReOrderQty.Text = "";
        }

        private void btnProductDetailsEdit_Click(object sender, RoutedEventArgs e)
        {
            btnProductDetailsNew.IsEnabled = true;
            txtProductDetailsName.IsEnabled = true;
            chkProductDetailsStatus.IsEnabled = true;
            btnProductDetailsEdit.IsEnabled = false;
            txtProductDetailsCostPrice.IsEnabled = true;
            txtProductDetailsSellingPrice.IsEnabled = true;

            txtAlertEmail.IsEnabled = true;
            txtReOrderQty.IsEnabled = true;

            btnProductDetailsSave.IsEnabled = true;
            _isEdit = true;
        }

        private void cmbProduct_LostFocus(object sender, RoutedEventArgs e)
        {
            cmbProdSalePrice.SelectedIndex = cmbProdCostPrice.SelectedIndex = cmbProduct.SelectedIndex;
            txtProductDetailsName.Text = cmbProduct.Text;
            txtProductDetailsCostPrice.Text =cmbProdCostPrice.Text;
            txtProductDetailsSellingPrice.Text = cmbProdSalePrice.Text;
            txtProductDetailsSellingPrice.IsEnabled = false;

            txtAlertEmail.IsEnabled = false;
            txtReOrderQty.IsEnabled = false;

            txtProductDetailsName.IsEnabled = false;
            chkProductDetailsStatus.IsEnabled = false;

            txtProductDetailsCostPrice.IsEnabled = false;
            if(cmbProductType.SelectedIndex >= 0)
            {
                btnProductDetailsNew.IsEnabled = true;
                if (cmbProduct.SelectedIndex >= 0)
                {
                    btnProductDetailsEdit.IsEnabled = true;
                }
                else
                { 
                    btnProductDetailsEdit.IsEnabled = false;
                }
                    
            }
            else
            {
                btnProductDetailsNew.IsEnabled = false;
            }
        }

        private void cmbProductType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbProductType.SelectedIndex != -1)
            {
                btnProductDetailsSave.IsEnabled = false;
                txtProductDetailsSellingPrice.IsEnabled = false;

                txtAlertEmail.IsEnabled = false;
                txtReOrderQty.IsEnabled = false;

                txtProductDetailsName.IsEnabled = false;
                chkProductDetailsStatus.IsEnabled = false;
                txtProductDetailsCostPrice.IsEnabled = false;
                btnProductDetailsNew.IsEnabled = true;
                btnProductDetailsEdit.IsEnabled = false;
                txtProductDetailsSellingPrice.Text = string.Empty;
                txtProductDetailsName.Text = string.Empty;
                txtProductDetailsCostPrice.Text = string.Empty;

                //_util.populateProductsByType(cmbProduct, cmbProductType.Text, cmbProdSalePrice, cmbProdCostPrice);
                string _prodType = (cmbProductType.SelectedItem as DataRowView).Row["ProductTypeName"].ToString();
                populateProductsByType(_prodType);

                //cmbProduct.IsEnabled = true;
                //txtProductDetailsSellingPrice.Text = string.Empty;
                //txtProductDetailsName.Text = string.Empty;
                //txtProductDetailsCostPrice.Text = string.Empty;
            }
        }

        public void cleanup()
        {
            txtProductDetailsSellingPrice.Text = string.Empty;
            txtProductDetailsName.Text = string.Empty;
            txtProductDetailsCostPrice.Text = string.Empty;
            txtProductDetailsName.IsEnabled = false;
            chkProductDetailsStatus.IsEnabled = false;
            txtProductDetailsCostPrice.IsEnabled = false;
            txtProductDetailsSellingPrice.IsEnabled = false;

            txtAlertEmail.IsEnabled = false;
            txtReOrderQty.IsEnabled = false;

            btnProductDetailsSave.IsEnabled = false;
            btnProductDetailsNew.IsEnabled = true;
            btnProductDetailsEdit.IsEnabled = false;
            cmbProduct.IsEnabled = true;

            //_util.populateProductsByType(cmbProduct, cmbProductType.Text, cmbProdSalePrice, cmbProdCostPrice);
            string _prodType = (cmbProductType.SelectedItem as DataRowView).Row["ProductTypeName"].ToString();
            populateProductsByType(_prodType);
        }

        private void btnProductDetailsSave_Click(object sender, RoutedEventArgs e)
        {
            int _productId = 0;
            int _productTypeId = 0;
            decimal _productCostPrice;
            decimal _productSellingPrice;
            string _productName;
            string _existingProdName;
            int _addSuccess;

            try
            {
                SettingsBAL _settingDetailsBAL = new SettingsBAL();
                if (txtProductDetailsName.Text.Trim() == "")
                {
                    MessageBox.Show("Please enter a Product Name", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProductDetailsName.Focus();
                    return;
                }
                else if (Regex.IsMatch(txtProductDetailsName.Text, @"[^a-zA-Z0-9\s]"))
                {
                    MessageBox.Show("Don't Use Special Characters", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProductDetailsName.Focus();
                    return;
                }
                else if (txtProductDetailsCostPrice.Text.Trim() == "")
                {
                    MessageBox.Show("Please enter a Cost Price", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProductDetailsCostPrice.Focus();
                    return;
                }
                else if (!Regex.IsMatch(txtProductDetailsCostPrice.Text, @"^\d{1,7}(\.\d{1,2})?$"))
                {
                    txtProductDetailsCostPrice.Text = "";
                    MessageBox.Show("Please enter a Numbers only (upto 2 decimal places)",this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
                    txtProductDetailsCostPrice.Focus();
                    return;
                }
                else if(Convert.ToDecimal(txtProductDetailsCostPrice.Text.Trim()) <= 0m)
                {
                    txtProductDetailsCostPrice.Text = "";
                    MessageBox.Show("Cost Price Cannot be Zero OR Less!",this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
                    txtProductDetailsCostPrice.Focus();
                    return;
                }
                else if (txtProductDetailsSellingPrice.Text.Trim() == "")
                {
                    MessageBox.Show("Please enter a Selling Price", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProductDetailsSellingPrice.Focus();
                    return;
                }
                else if (!Regex.IsMatch(txtProductDetailsSellingPrice.Text, @"^\d{1,7}(\.\d{1,2})?$"))
                {
                    txtProductDetailsSellingPrice.Text = "";
                    MessageBox.Show("Please enter a Numbers only (upto 2 decimal places)",this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
                    txtProductDetailsSellingPrice.Focus();
                    return;
                }
                else if (Convert.ToDecimal(txtProductDetailsSellingPrice.Text.Trim()) <= 0m)
                {
                    txtProductDetailsSellingPrice.Text = "";
                    MessageBox.Show("Selling Price Cannot be Zero OR Less!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProductDetailsSellingPrice.Focus();
                    return;
                }

                if (txtReOrderQty.Text == "")
                {
                    txtReOrderQty.Text = "0";
                }
                else
                {
                    if (!Regex.IsMatch(txtReOrderQty.Text, @"^\d{1,2}?$"))
                    {
                        txtReOrderQty.Text = "";
                        MessageBox.Show("Product re-Order Quantity MUST be numeric!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        txtReOrderQty.Focus();
                        return;
                    }
                }
                if (!_isEdit)
                {
                    _productTypeId = Convert.ToInt32(cmbProductType.SelectedValue.ToString().Trim());
                    _productName = txtProductDetailsName.Text.ToString().Trim();
                    _productCostPrice = Convert.ToDecimal(txtProductDetailsCostPrice.Text.ToString().Trim());
                    _productSellingPrice = Convert.ToDecimal(txtProductDetailsSellingPrice.Text.ToString().Trim());
                    if (cmbProduct.Items.Count > 0)
                    {
                        for (int i = 0; i < cmbProduct.Items.Count; i++)
                        {
                            //cmbProduct.SelectedIndex = i;
                            //_existingProdName = cmbProduct.Text;
                            _existingProdName = (cmbProduct.Items[i] as DataRowView).Row["ProductName"].ToString();

                            if (_productName.ToUpper() == _existingProdName.ToUpper())
                            {
                                MessageBox.Show("Product Type Already Exists! Please enter a New ProductName!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                btnProductDetailsNew.IsEnabled = true;
                                btnProductDetailsSave.IsEnabled = true;
                                return;
                            }
                        }
                    }
                    _addSuccess = _settingDetailsBAL.saveNew_ProductDetails(_productTypeId, _productName, _productCostPrice, _productSellingPrice, Convert.ToInt32(txtReOrderQty.Text));
                    if (_addSuccess != 0)
                    {
                        MessageBox.Show("New ProductName Added ! " + _productName, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        cleanup();
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Error! Product Not Added!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
                else
                {
                    _productId = Convert.ToInt32(cmbProduct.SelectedValue.ToString().Trim());
                    _productName = txtProductDetailsName.Text.ToString().Trim();
                    _productCostPrice = Convert.ToDecimal(txtProductDetailsCostPrice.Text.ToString().Trim());
                    _productSellingPrice = Convert.ToDecimal(txtProductDetailsSellingPrice.Text.ToString().Trim());
                    int j = cmbProduct.SelectedIndex;

                    for (int i = 0; i < cmbProduct.Items.Count; i++)
                    {
                        //cmbProduct.SelectedIndex = i;
                        //_existingProdName = cmbProduct.Text;
                        _existingProdName = (cmbProduct.Items[i] as DataRowView).Row["ProductName"].ToString();

                        if ((_productName.ToUpper() == _existingProdName.ToUpper()) && (i!=j)) //&& (_productId != Convert.ToInt32(cmbProduct.SelectedValue.ToString().Trim())))
                        {
                            MessageBox.Show("Product Type Already Exists!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            btnProductDetailsNew.IsEnabled = true;
                            btnProductDetailsSave.IsEnabled = true;
                            return;
                        }
                    }
                    if (chkProductDetailsStatus.IsChecked == true)
                    {
                        _addSuccess = _settingDetailsBAL.saveEdit_ProductDetails(_productId, _productName, _productCostPrice, _productSellingPrice, 1, Convert.ToInt32(txtReOrderQty.Text));
                    }
                    else
                    {
                        _addSuccess = _settingDetailsBAL.saveEdit_ProductDetails(_productId, _productName, _productCostPrice, _productSellingPrice, 0, Convert.ToInt32(txtReOrderQty.Text));
                    }
                    
                    MessageBox.Show("ProductName Updated ! " + _productName, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    cleanup();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void cmbProduct_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbProduct.SelectedIndex != -1)
            {
                btnProductDetailsSave.IsEnabled = false;

                cmbProdSalePrice.SelectedIndex = cmbProdCostPrice.SelectedIndex = cmbProduct.SelectedIndex;
                //txtProductDetailsName.Text = cmbProduct.Text;
                txtProductDetailsName.Text = (cmbProduct.SelectedItem as DataRowView).Row["ProductName"].ToString();
                txtProductDetailsCostPrice.Text = (cmbProduct.SelectedItem as DataRowView).Row["ProductCostPrice"].ToString();
                txtProductDetailsSellingPrice.Text = (cmbProduct.SelectedItem as DataRowView).Row["ProductSalePrice"].ToString();

                txtReOrderQty.Text = (cmbProduct.SelectedItem as DataRowView).Row["ReOrderQty"].ToString();
                txtAlertEmail.Text = (cmbProduct.SelectedItem as DataRowView).Row["alertEmail"].ToString();

                txtProductDetailsSellingPrice.IsEnabled = false;

                txtAlertEmail.IsEnabled = false;
                txtReOrderQty.IsEnabled = false;

                txtProductDetailsName.IsEnabled = false;
                txtProductDetailsCostPrice.IsEnabled = false;

                if (cmbProductType.SelectedIndex >= 0)
                {
                    btnProductDetailsNew.IsEnabled = true;
                    if (cmbProduct.SelectedIndex >= 0)
                    {
                        btnProductDetailsEdit.IsEnabled = true;
                    }
                    else
                    {
                        btnProductDetailsEdit.IsEnabled = false;
                    }

                }
                else
                {
                    btnProductDetailsNew.IsEnabled = false;
                }

                if ((cmbProduct.SelectedItem as DataRowView).Row["Active"].ToString() == "1")
                {
                    chkProductDetailsStatus.IsChecked = true;
                }
                else
                {
                    chkProductDetailsStatus.IsChecked = false;
                }
                chkProductDetailsStatus.IsEnabled = false;
            }
        }


        private void btnContactTypes1_Click(object sender, RoutedEventArgs e)
        {
            CompanyDetails _CompanySiteDetails = new CompanyDetails();
            _CompanySiteDetails._CompanyName = this._CompanyName;
            _CompanySiteDetails._UserName = this._UserName;
            _CompanySiteDetails._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _CompanySiteDetails._StaffPic = this._StaffPic;
            _CompanySiteDetails.StaffImage.Source = StaffImage.Source;
            _CompanySiteDetails.Owner = this.Owner;

            this.Close();
            _CompanySiteDetails.ShowDialog();
        }

        private void btnUserAdministration_Click(object sender, RoutedEventArgs e)
        {
            if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
            {
                UserAdministration useradmin = new UserAdministration();
                useradmin._CompanyName = this._CompanyName;
                useradmin._UserName = this._UserName;
                useradmin._CurrentLoginDateTime = this._CurrentLoginDateTime;
                useradmin._StaffPic = this._StaffPic;
                useradmin.StaffImage.Source = StaffImage.Source;
                useradmin.Owner = this.Owner;

                this.Close();
                useradmin.ShowDialog();
            }
            else
            {
                MessageBox.Show("You MUST be Admin user to access this Screen!");
                return;
            }
        }

        private void btnProductsTypes_Click(object sender, RoutedEventArgs e)
        {

            ProductType prdType = new ProductType();
            prdType._CompanyName = this._CompanyName;
            prdType._UserName = this._UserName;
            prdType._CurrentLoginDateTime = this._CurrentLoginDateTime;
            prdType._StaffPic = this._StaffPic;
            prdType.StaffImage.Source = StaffImage.Source;
            prdType.Owner = this.Owner;

            this.Close();
            prdType.ShowDialog();
        }

        private void btnManageProducts_Click(object sender, RoutedEventArgs e)
        {
            //SAME WINDOW; NO ACTION;

            //Products products = new Products();
            //products._CompanyName = this._CompanyName;
            //products._UserName = this._UserName;
            //products._CurrentLoginDateTime = this._CurrentLoginDateTime;
            //products._StaffPic = this._StaffPic;
            //products.StaffImage.Source = StaffImage.Source;
            //products.Owner = this.Owner;

            //this.Close();
            //products.ShowDialog();
        }

        private void btnProgrammeGSetup_Click(object sender, RoutedEventArgs e)
        {
            ProgrammeGroup PrgGroup = new ProgrammeGroup();
            PrgGroup._CompanyName = this._CompanyName;
            PrgGroup._UserName = this._UserName;
            PrgGroup._CurrentLoginDateTime = this._CurrentLoginDateTime;
            PrgGroup._StaffPic = this._StaffPic;
            PrgGroup.StaffImage.Source = StaffImage.Source;
            PrgGroup.Owner = this.Owner;

            this.Close();
            PrgGroup.ShowDialog();
        }

        private void btnAddEditProgrammes_Click(object sender, RoutedEventArgs e)
        {
            Programme Prg = new Programme();
            Prg._CompanyName = this._CompanyName;
            Prg._UserName = this._UserName;
            Prg._CurrentLoginDateTime = this._CurrentLoginDateTime;
            Prg._StaffPic = this._StaffPic;
            Prg.StaffImage.Source = StaffImage.Source;
            Prg.Owner = this.Owner;

            this.Close();
            Prg.ShowDialog();
        }

        private void btnSupplierSetup_Click(object sender, RoutedEventArgs e)
        {
            Supplier _Supplier = new Supplier();
            _Supplier._CompanyName = this._CompanyName;
            _Supplier._UserName = this._UserName;
            _Supplier._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Supplier._StaffPic = this._StaffPic;
            _Supplier.StaffImage.Source = StaffImage.Source;
            _Supplier.Owner = this.Owner;

            this.Close();
            _Supplier.ShowDialog();
        }

        private void btnBookingResources_Click(object sender, RoutedEventArgs e)
        {
            Settings.Resources _Res = new Settings.Resources();
            _Res._CompanyName = this._CompanyName;
            _Res._UserName = this._UserName;
            _Res._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Res._StaffPic = this._StaffPic;
            _Res.StaffImage.Source = StaffImage.Source;
            _Res.Owner = this.Owner;

            this.Close();
            _Res.ShowDialog();
        }

        private void btnUserAccessRights_Click(object sender, RoutedEventArgs e)
        {
            StaffAccess _StaffAccess = new StaffAccess();
            _StaffAccess._CompanyName = this._CompanyName;
            _StaffAccess._UserName = this._UserName;
            _StaffAccess._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _StaffAccess._StaffPic = this._StaffPic;
            _StaffAccess.StaffImage.Source = StaffImage.Source;
            _StaffAccess.Owner = this.Owner;

            this.Close();
            _StaffAccess.ShowDialog();
        }

        private void btnEmailTSetup_Click(object sender, RoutedEventArgs e)
        {
            EmailTemplateSettings _eMailTemplate = new EmailTemplateSettings();
            _eMailTemplate._CompanyName = this._CompanyName;
            _eMailTemplate._UserName = this._UserName;
            _eMailTemplate._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _eMailTemplate._StaffPic = this._StaffPic;
            _eMailTemplate.StaffImage.Source = StaffImage.Source;
            _eMailTemplate.Owner = this.Owner;

            this.Close();
            _eMailTemplate.ShowDialog();
        }


        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDashboardQuit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
                //Closing event called
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnContactTypes_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
