﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration;
using System.Data;
using System.Windows.Threading;
using System.ComponentModel;
using System.IO;
using GymMembership.Comman;
using GymMembership.Accounts;
using TechnoGymAPI;

namespace GymMembership.Settings
{
    /// <summary>
    /// Interaction logic for CompanyDetails.xaml
    /// </summary>
    /// 
    public class Img
    {
        public Img(string value, Image img) { Str = value; Image = img; }
        public string Str { get; set; }
        public Image Image { get; set; }
    }

    public partial class CompanyDetails
    {
        public CompanyDetails()
        {
            InitializeComponent();
            //this.Closing += new CancelEventHandler(CompanyDetails_Closing);
        }
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public Login _Login;

        bool _IsIdle = false;
        private bool _isNewSite = false; private int _isUpdate = 1; int _isAccessRequiredfromUI = 1;
        bool _isPrevOFFLINE = false;
        int _CompanyID, _SiteID;
        int _SiteMasterTable = 0; int _AccessTypeTable = 1; int _DoorTable = 2;
        DataSet _dsSiteDetails = new DataSet();
        DataTable _dtNewSiteAccessType = new DataTable();
        DataTable _dtNewSiteDoors = new DataTable();
        bool _windowLoaded = false;
        bool _localPathFolder = true;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lblOrgName1.Content = ConfigurationManager.AppSettings["LoggedInOrg"].ToString().ToUpper();
            Utilities _uTil = new Utilities();
            _uTil.populateSiteNamebyCompany(cmbSiteName, lblOrgName1.Content.ToString());
            ClearFields();
            tabControl.SelectedIndex = 0;
            (tabControl.Items[1] as TabItem).IsEnabled = false;
            (tabControl.Items[2] as TabItem).IsEnabled = false;
            (tabControl.Items[3] as TabItem).IsEnabled = false;
            (tabControl.Items[4] as TabItem).IsEnabled = false;
            tabTechnoGym.IsEnabled = false; //MODIFY, IF TECHNOGYM DETAILS ARE COMMON FOR ALL SITES

            this.Title = "Company Details";
            lblOrgName.Content = _CompanyName.ToString().Replace("_", "__");
            lblUserName.Content = _UserName.ToString().Replace("_", "__");
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            _windowLoaded = true;
        }

        private void cmbSiteName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbSiteName.SelectedIndex != -1)
            {
                ClearFields();

                _SiteID = Convert.ToInt32(cmbSiteName.SelectedValue);
                PopulateSiteDetails(_SiteID);
                _isNewSite = false;
                _isUpdate = 1;
                grpAccess.IsEnabled = true;
                (tabControl.Items[1] as TabItem).IsEnabled = true;
                (tabControl.Items[2] as TabItem).IsEnabled = true;
                (tabControl.Items[3] as TabItem).IsEnabled = true;
                (tabControl.Items[4] as TabItem).IsEnabled = true;

                tabTechnoGym.IsEnabled = true;
                tabTechnoGym.IsEnabled = true;
                tabTechnoGym.Visibility = Visibility.Visible;
                //btnGetTechnoGymToken.IsEnabled = true;
            }
        }
        private void PopulateSiteDetails(int _siteID)
        {
            try
            {
                if (_siteID != -1)
                {
                    _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());

                    SqlHelper sql = new SqlHelper();
                    Hashtable ht = new Hashtable();

                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@SiteID", _siteID);

                    //_dsSiteDetails = sql.ExecuteProcedure("GetSiteDetails", ht);
                    _dsSiteDetails = sql.ExecuteProcedure("GetSiteDetails_NEW", ht);

                    if (_dsSiteDetails != null)
                    {
                        if (_dsSiteDetails.Tables[_SiteMasterTable].DefaultView != null)
                        {
                            //--TabControl 1
                            txtSiteName.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["SiteName"].ToString().ToUpper();
                            txtAddress1.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["Addr1"].ToString().ToUpper();
                            txtAddress2.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["Addr2"].ToString().ToUpper();
                            txtAddress3.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["Addr3"].ToString().ToUpper();
                            txtState.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["State"].ToString().ToUpper();
                            txtPhoneNumber.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["Phone"].ToString().ToUpper();
                            txtFaxNumber.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["Fax"].ToString().ToUpper();
                            txtEnquiriesEmail.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["Enq_Email"].ToString().ToUpper();
                            txtWebsite.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["website"].ToString().ToUpper();
                            txtFilePath.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["AccessControl_FilePath"].ToString().ToUpper();
                            txtFileName.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["AccessControl_FileName"].ToString().ToUpper();

                            ////RAMJI-27OCT2017
                            txtNetworkFilePath.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["DATFileNetworkPath"].ToString().Trim();
                            txtDATFileEmailID.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["datFILEalertEmail"].ToString().Trim();

                            if (_dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["DATFileisLocalPath"].ToString() == "1")
                            {
                                _localPathFolder = true;
                                if (_windowLoaded)
                                {
                                    btnToggle.IsChecked = true;
                                }
                            }
                            else
                            {
                                _localPathFolder = false;
                                if (_windowLoaded)
                                {
                                    btnToggle.IsChecked = false;
                                }
                            }
                            ////RAMJI-27OCT2017

                            //--TabControl 2
                            txtEmailSender.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["cfgEmail_Sender"].ToString().ToUpper();
                            txtHostName.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["cfgSMTPHost"].ToString().ToUpper();
                            txtPortNumber.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["cfgSMTPPort"].ToString().ToUpper();
                            txtUserName.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["cfgSMTPUserName"].ToString();
                            txtPassword.Password = Constants.passwordDecrypt(_dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["cfgSMTPPassword"].ToString(), Constants._EncryptKey);

                            //Paychoice
                            txtSandboxURL.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["SandboxURL"].ToString();
                            txtSandboxUsername.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["SandboxUsername"].ToString();
                            //txtSandboxPassword.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["SandboxPassword"].ToString();
                            txtSandboxPassword.Password = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["SandboxPassword"].ToString();
                            txtSecuredURL.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["SecureURL"].ToString();
                            txtSecuredUsername.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["SecureUsername"].ToString();
                            //txtSecuredPassword.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["SecurePassword"].ToString();
                            txtSecuredPassword.Password = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["SecurePassword"].ToString();
                            string activeportal = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["ActivePortal"].ToString();
                            
                            if (activeportal == "Sandbox")
                            {
                                rbSandboxActive.IsChecked = true;
                            }
                            else
                            {
                                rbSecuredActive.IsChecked = true;
                            }
                            lblgateway.Content = activeportal;

                            //access control connected pc/server name
                            txtAccessPCName.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["ACCESS_PCNAME"].ToString();

                            //SITE GOES OFF - ACTIVE / INACTIVE : GAS-6
                            if (Convert.ToInt32(_dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["isAccessRequired"].ToString()) == 0)
                            {
                                rbuttonInActive.IsChecked = true;
                                rbuttonActive.IsChecked = false;
                                _isPrevOFFLINE = true;
                            }
                            else
                            {
                                rbuttonActive.IsChecked = true;
                                rbuttonInActive.IsChecked = false;
                                _isPrevOFFLINE = false;
                            }
                            //SITE GOES OFF - ACTIVE / INACTIVE : GAS-6

                            txtPOSOpeningBal.Text = Convert.ToDecimal(_dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["POS_OpeningBalance"].ToString()).ToString("#0.#0");
                            txtPOSReportEmail.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["POS_Report_Email"].ToString();
                            txtPOSStockEmail.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["POS_Stock_Level_Alert_Email"].ToString();

                            //COM PORT - AccessControlCOMport
                            cmbPORT.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["AccessControlCOMport"].ToString().ToUpper();
                        }

                        //Table:1 - AccessTypes
                        if (_dsSiteDetails.Tables[_AccessTypeTable].DefaultView != null)
                        {
                            //--TabControl 3
                            lstAccessTypes.ItemsSource = _dsSiteDetails.Tables[_AccessTypeTable].DefaultView;
                            lstAccessTypes.DisplayMemberPath = "AccessTypeName";
                        }

                        //Table:2 - Doors
                        if (_dsSiteDetails.Tables[_DoorTable].DefaultView != null)
                        {
                            //--TabControl 3
                            lstDoor.ItemsSource = _dsSiteDetails.Tables[_DoorTable].DefaultView;
                            lstDoor.DisplayMemberPath = "DoorName";
                            lstDoor.SelectedValuePath = "DoorNo";
                        }

                        //TechnoGym
                        if (_dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["TechnoGym_Test_OR_Prod"].ToString().Trim().ToUpper() == "TEST")
                        {
                            rbTGtestActive.IsChecked = true;
                        }
                        else if (_dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["TechnoGym_Test_OR_Prod"].ToString().Trim().ToUpper() == "PROD")
                        {
                            rbTGPRODActive.IsChecked = true;
                        }
                        //TechnoGym - TEST url configuration
                        txtTGtestURL.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["test_technoGym_URL"].ToString().Trim();
                        txtTGtestFacility.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["test_technoGym_Facility"].ToString().Trim();
                        txtTGtestUniqueID.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["test_technoGym_UniqueID"].ToString().Trim();
                        txtTGtestHdrCLIENT.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["test_technoGym_hdr_CLIENT"].ToString().Trim();
                        txtTGtestHdrAPIKEY.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["test_technoGym_hdr_APIKEY"].ToString().Trim();
                        txtTGtestUsername.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["test_technoGym_param_Username"].ToString().Trim();
                        txtTGtestPassword.Password = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["test_technoGym_param_Password"].ToString();
                        
                        //TechnoGym - PROD url configuration
                        txtTGPRODURL.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["PROD_technoGym_URL"].ToString().Trim();
                        txtTGPRODFacility.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["PROD_technoGym_Facility"].ToString().Trim();
                        txtTGPRODUniqueID.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["PROD_technoGym_UniqueID"].ToString().Trim();
                        txtTGPRODHdrCLIENT.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["PROD_technoGym_hdr_CLIENT"].ToString().Trim();
                        txtTGPRODHdrAPIKEY.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["PROD_technoGym_hdr_APIKEY"].ToString().Trim();
                        txtTGPRODUsername.Text = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["PROD_technoGym_param_Username"].ToString().Trim();
                        txtTGPRODPassword.Password = _dsSiteDetails.Tables[_SiteMasterTable].Rows[0]["PROD_technoGym_param_Password"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error: " + ex.Message);
                return;
            }
        }
        private void ClearFields()
        {
            txtSiteName.Text = "";
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtAddress3.Text = "";
            txtState.Text = "";
            txtPhoneNumber.Text = "";
            txtFaxNumber.Text = "";
            txtEnquiriesEmail.Text = "";
            txtWebsite.Text = "";
            txtFilePath.Text = "";
            txtEmailSender.Text = "";
            txtHostName.Text = "";
            txtPortNumber.Text = "";
            txtUserName.Text = "";
            txtPassword.Password = "";

            txtNewAccessType.Text = "";
            txtFileName.Text = "";
            lstAccessTypes.ItemsSource = null;
            lstDoor.ItemsSource = null;

            txtDoorCode.Text = "";
            txtDoorName.Text = "";
            txtNewDoor.Text = "";
            cmbPORTDoor.Text = ""; txtDoorPCName.Text = "";

            txtSandboxURL.Text = "";
            txtSandboxUsername.Text = "";
            txtSecuredPassword.Password = "";
            txtSecuredURL.Text = "";
            txtSecuredUsername.Text = "";
            txtSecuredPassword.Password = "";

            txtPOSStockEmail.Text = "";
            txtPOSReportEmail.Text = "";
            txtPOSOpeningBal.Text = "0.00";
            rbuttonActive.IsChecked = true;

            //TechnoGym
            txtTGPRODFacility.Text = "";
            txtTGPRODHdrAPIKEY.Text = "";
            txtTGPRODHdrCLIENT.Text = "";
            txtTGPRODPassword.Password = "";
            txtTGPRODUniqueID.Text = "";
            txtTGPRODURL.Text = "";
            txtTGPRODUsername.Text = "";

            txtTGtestFacility.Text = "";
            txtTGtestHdrAPIKEY.Text = "";
            txtTGtestHdrCLIENT.Text = "";
            txtTGtestPassword.Password = "";
            txtTGtestUniqueID.Text = "";
            txtTGtestURL.Text = "";
            txtTGtestUsername.Text = "";
        }

        private void btnNewSite_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                cmbSiteName.SelectedIndex = -1;
                cmbPORT.SelectedIndex = -1;
                rbuttonActive.IsChecked = true;
                rbuttonInActive.IsChecked = false;
                _isPrevOFFLINE = false;

                _isNewSite = true;
                _isUpdate = 0;
                _isAccessRequiredfromUI = 1;
                _SiteID = 0;
                ClearFields();

                grpAccess.IsEnabled = true;
                (tabControl.Items[1] as TabItem).IsEnabled = true;
                (tabControl.Items[2] as TabItem).IsEnabled = true;
                (tabControl.Items[3] as TabItem).IsEnabled = true;
                (tabControl.Items[4] as TabItem).IsEnabled = true;

                _dtNewSiteAccessType = new DataTable();
                _dtNewSiteAccessType.Columns.Add("CompanyID", typeof(int));
                _dtNewSiteAccessType.Columns.Add("SiteID", typeof(int));
                _dtNewSiteAccessType.Columns.Add("AccessTypeName", typeof(string));
                lstAccessTypes.ItemsSource = _dtNewSiteAccessType.DefaultView;

                _dtNewSiteDoors = new DataTable();
                _dtNewSiteDoors.Columns.Add("CompanyID", typeof(int));
                _dtNewSiteDoors.Columns.Add("SiteID", typeof(int));
                _dtNewSiteDoors.Columns.Add("DoorName", typeof(string));
                _dtNewSiteDoors.Columns.Add("DoorCode", typeof(string));
                _dtNewSiteDoors.Columns.Add("DoorNo", typeof(int));
                
                //GAS-28 : COM Port - Multiple Doors
                _dtNewSiteDoors.Columns.Add("COMPort", typeof(string));
                _dtNewSiteDoors.Columns.Add("AccPCNAME", typeof(string));
                //GAS-28 : COM Port - Multiple Doors

                lstDoor.ItemsSource = _dtNewSiteDoors.DefaultView;
                lstDoor.DisplayMemberPath = "DoorName";
                lstDoor.SelectedValuePath = "DoorNo";

                tabControl.SelectedIndex = 0;
                txtSiteName.Focus();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error : " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ValidateData())
                {
                    int _PortNumber = 0;
                    SqlHelper sql = new SqlHelper();
                    Hashtable ht = new Hashtable();
                    _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());

                    if (txtPortNumber.Text.Trim() != "")
                    {
                        _PortNumber = Convert.ToInt32(txtPortNumber.Text.Trim().ToUpper());
                    }
                    
                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@SiteID", _SiteID);
                    ht.Add("@SiteName", txtSiteName.Text.Trim().ToUpper());
                    ht.Add("@Addr1", txtAddress1.Text.Trim().ToUpper());
                    ht.Add("@Addr2", txtAddress2.Text.Trim().ToUpper());
                    ht.Add("@Addr3", txtAddress3.Text.Trim().ToUpper());
                    ht.Add("@State", txtState.Text.Trim().ToUpper());
                    ht.Add("@Phone", txtPhoneNumber.Text.Trim().ToUpper());
                    ht.Add("@Fax", txtFaxNumber.Text.Trim().ToUpper());
                    ht.Add("@Enq_Email", txtEnquiriesEmail.Text.Trim().ToUpper());
                    ht.Add("@website", txtWebsite.Text.Trim().ToUpper());
                    ht.Add("@AccessControl_FilePath", txtFilePath.Text.Trim().ToUpper());
                    ht.Add("@Access_FileName", txtFileName.Text.Trim().ToUpper());
                    ht.Add("@cfgEmail_Sender", txtEmailSender.Text.Trim().ToUpper());
                    ht.Add("@cfgSMTPHost", txtHostName.Text.Trim().ToUpper());
                    ht.Add("@cfgSMTPPort", _PortNumber);
                    ht.Add("@cfgSMTPUserName", txtUserName.Text.Trim());
                    ht.Add("@cfgSMTPPassword", Constants.passwordEncrypt(txtPassword.Password.Trim(), Constants._EncryptKey));
                    ht.Add("@pcsandbocurl", txtSandboxURL.Text.Trim());
                    ht.Add("@pcsandboxun", txtSandboxUsername.Text.Trim());
                    ht.Add("@pcsandboxpwd", txtSandboxPassword.Password.Trim());
                    ht.Add("@pcsecureurl", txtSecuredURL.Text.Trim());
                    ht.Add("@pcsecureun", txtSecuredUsername.Text.Trim());
                    ht.Add("@pcsecurepwd", txtSecuredPassword.Password.Trim());
                    if (rbSandboxActive.IsChecked == true)
                    {
                        ht.Add("@pcactive", "Sandbox");
                    }
                    else if (rbSecuredActive.IsChecked == true)
                    {
                        ht.Add("@pcactive", "Secure");
                    }
                    else
                    {
                        ht.Add("@pcactive", "Sandbox");
                    }
                    ht.Add("@isUpdate", _isUpdate);
                    if (_isNewSite)
                    {
                        ht.Add("@AccessTypes", _dtNewSiteAccessType);
                        ht.Add("@AccessDoors", _dtNewSiteDoors);
                    }
                    else
                    {
                        ht.Add("@AccessTypes", _dsSiteDetails.Tables[_AccessTypeTable]);
                        ht.Add("@AccessDoors", _dsSiteDetails.Tables[_DoorTable]);
                    }
                    if (txtPOSOpeningBal.Text.Trim() == "")
                        txtPOSOpeningBal.Text = "0.00";

                    ht.Add("@POS_OpeningBalance", Convert.ToDecimal(txtPOSOpeningBal.Text));
                    ht.Add("@POS_Report_Email", txtPOSReportEmail.Text.Trim());
                    ht.Add("@POS_Stock_Level_Alert_Email", txtPOSStockEmail.Text.Trim());
                    ht.Add("@ACCESS_PCNAME", txtAccessPCName.Text.Trim().ToUpper());

                    if (rbuttonActive.IsChecked == true)
                        _isAccessRequiredfromUI = 1;
                    else if (rbuttonInActive.IsChecked == true)
                        _isAccessRequiredfromUI = 0;

                    ht.Add("@isAccessRequiredfromUI", _isAccessRequiredfromUI);
                    ht.Add("@AccessControlCOMport", cmbPORT.Text.Trim().ToUpper());
                    if (_localPathFolder)
                    {
                        ht.Add("@DATFileisLocalPath", 1);
                    }
                    else
                    {
                        ht.Add("@DATFileisLocalPath", 0);
                    }
                    ////RAMJI-27OCT2017
                    ht.Add("@DATFileNetworkPath", txtNetworkFilePath.Text.Trim());
                    ht.Add("@datFILEalertEmail", txtDATFileEmailID.Text.Trim());
                    ////RAMJI-27OCT2017

                    //GAS-19: TechnoGym Parameters
                    ht.Add("@test_technoGym_URL", txtTGtestURL.Text.Trim());
					ht.Add("@test_technoGym_Facility", txtTGtestFacility.Text.Trim());
                    ht.Add("@test_technoGym_UniqueID", txtTGtestUniqueID.Text.Trim());
                    ht.Add("@test_technoGym_hdr_CLIENT", txtTGtestHdrCLIENT.Text.Trim());
                    ht.Add("@test_technoGym_hdr_APIKEY", txtTGtestHdrAPIKEY.Text.Trim());
                    ht.Add("@test_technoGym_param_Username", txtTGtestUsername.Text.Trim());
                    ht.Add("@test_technoGym_param_Password", txtTGtestPassword.Password);

                    ht.Add("@PROD_technoGym_URL", txtTGPRODURL.Text.Trim());
                    ht.Add("@PROD_technoGym_Facility", txtTGPRODFacility.Text.Trim());
                    ht.Add("@PROD_technoGym_UniqueID", txtTGPRODUniqueID.Text.Trim());
                    ht.Add("@PROD_technoGym_hdr_CLIENT", txtTGPRODHdrCLIENT.Text.Trim());
                    ht.Add("@PROD_technoGym_hdr_APIKEY", txtTGPRODHdrAPIKEY.Text.Trim());
                    ht.Add("@PROD_technoGym_param_Username", txtTGPRODUsername.Text.Trim());
                    ht.Add("@PROD_technoGym_param_Password", txtTGPRODPassword.Password);

                    if (rbTGPRODActive.IsChecked == true)
                    {
                        ht.Add("@TechnoGym_Test_OR_Prod", "PROD");
                    }
                    else
                    {
                        ht.Add("@TechnoGym_Test_OR_Prod", "TEST");
                    }
                    //GAS-19: TechnoGym Parameters

                    DataSet _dsResult = new DataSet();
                    //_dsResult = sql.ExecuteProcedure("SaveSiteDetails_new", ht);
                    _dsResult = sql.ExecuteProcedure("SaveSiteDetails_new_MULTIDOORS", ht);

                    if (Convert.ToInt32(_dsResult.Tables[_SiteMasterTable].Rows[0]["Result"]) != 0)
                    {
                        //SUCCESS
                        MessageBox.Show("Site Details Saved Successfully!\n\n(PLEASE NOTE: You must Logout AND Login again\nFor the changes to take effect)", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        if (_SiteID == Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString()))
                        {
                            ConfigurationManager.AppSettings["AccessControl_FilePath"] = txtFilePath.Text.Trim().ToUpper();
                            ConfigurationManager.AppSettings["AccessControl_FileName"] = txtFileName.Text.Trim().ToUpper();
                            ConfigurationManager.AppSettings["AccessControlPORT"] = cmbPORT.Text.ToUpper();
                        }

                        if (!_isNewSite)
                        {
                            if ((_isPrevOFFLINE) && (_isAccessRequiredfromUI == 1) && (txtAccessPCName.Text.Trim() != ""))
                            {
                                Constants.Create_ACCESS_DAT_File(false);
                            }
                        }
                        this.Close();
                    }
                    else
                    {
                        //FAILED
                        System.Windows.MessageBox.Show("Site Details Save Failed!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error : " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnFilePath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.FolderBrowserDialog commonOpenFileDialog = new System.Windows.Forms.FolderBrowserDialog();
                commonOpenFileDialog.ShowDialog();

                string fileNamePath = commonOpenFileDialog.SelectedPath;
                if (fileNamePath != "")
                {
                    txtFilePath.Text = fileNamePath;
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (txtNewAccessType.Text.Trim() == "")
            {
                System.Windows.MessageBox.Show("Please Enter an Access Type Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtNewAccessType.Focus();
                return;
            }
            if (DuplicateAccessType(txtNewAccessType.Text.Trim().ToUpper()))
            {
                System.Windows.MessageBox.Show("Access Type Already Exists!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtNewAccessType.Focus();
                return;
            }
            AddAccessType(txtNewAccessType.Text.Trim().ToUpper());
            txtNewAccessType.Text = "";
            txtNewAccessType.Focus();
        }
        private bool DuplicateAccessType(string _AccessType)
        {
            for (int i = 0; i < lstAccessTypes.Items.Count; i++)
            {
                if ((lstAccessTypes.Items[i] as DataRowView).Row["AccessTypeName"].ToString().ToUpper() == _AccessType.ToUpper())
                {
                    return true;
                }
            }
            return false;
        }
        private bool DuplicateDoor(string _DoorNo)
        {
            try
            {
                for (int i = 0; i < lstDoor.Items.Count; i++)
                {
                    if ((lstDoor.Items[i] as DataRowView).Row["DoorNo"].ToString().ToUpper() == _DoorNo.ToUpper())
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error : " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
        }
        private bool DuplicateDoorName(string _DoorName)
        {
            try
            {
                for (int i = 0; i < lstDoor.Items.Count; i++)
                {
                    if ((lstDoor.Items[i] as DataRowView).Row["DoorName"].ToString().ToUpper() == _DoorName.ToUpper())
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error : " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
        }
        private void AddAccessType(string _AccessType)
        {
            DataRow dR;
            if (_isNewSite)
            {
                dR = _dtNewSiteAccessType.NewRow();
                dR["SiteID"] = 0;
            }
            else
            {
                dR = _dsSiteDetails.Tables[_AccessTypeTable].NewRow();
                dR["SiteID"] = cmbSiteName.SelectedValue;
            }
            dR["CompanyID"] = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
            dR["AccessTypeName"] = _AccessType;

            if (_isNewSite)
            {
                _dtNewSiteAccessType.Rows.Add(dR);
                lstAccessTypes.ItemsSource = null;
                lstAccessTypes.ItemsSource = _dtNewSiteAccessType.DefaultView;
                lstAccessTypes.DisplayMemberPath = "AccessTypeName";
            }
            else
            {
                _dsSiteDetails.Tables[_AccessTypeTable].Rows.Add(dR);
                _dsSiteDetails.Tables[_AccessTypeTable].AcceptChanges();
                _dsSiteDetails.AcceptChanges();

                lstAccessTypes.ItemsSource = null;
                lstAccessTypes.ItemsSource = _dsSiteDetails.Tables[_AccessTypeTable].DefaultView;
                lstAccessTypes.DisplayMemberPath = "AccessTypeName";
            }
        }
        private void AddDoor(string _DoorNo, string _DoorCode = "R", string _DoorName = "", string _COMPort = "", string _AccPCNAME = "")
        {
            try
            {
                DataRow dR;
                if (_isNewSite)
                {
                    dR = _dtNewSiteDoors.NewRow();
                    dR["SiteID"] = 0;
                }
                else
                {
                    dR = _dsSiteDetails.Tables[_DoorTable].NewRow();
                    dR["SiteID"] = cmbSiteName.SelectedValue;
                }
                dR["CompanyID"] = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                dR["DoorNo"] = _DoorNo;
                dR["DoorCode"] = _DoorCode;
                dR["DoorName"] = _DoorName;

                //GAS-28 : COM Port - Multiple Doors
                dR["COMPort"] = _COMPort;
                dR["AccPCNAME"] = _AccPCNAME;
                //GAS-28 : COM Port - Multiple Doors

                if (_isNewSite)
                {
                    _dtNewSiteDoors.Rows.Add(dR);
                    lstDoor.ItemsSource = null;
                    lstDoor.ItemsSource = _dtNewSiteDoors.DefaultView;
                    lstDoor.DisplayMemberPath = "DoorName";
                    lstDoor.SelectedValuePath = "DoorNo";
                }
                else
                {
                    _dsSiteDetails.Tables[_DoorTable].Rows.Add(dR);
                    _dsSiteDetails.Tables[_DoorTable].AcceptChanges();
                    _dsSiteDetails.AcceptChanges();

                    lstDoor.ItemsSource = null;
                    lstDoor.ItemsSource = _dsSiteDetails.Tables[_DoorTable].DefaultView;
                    lstDoor.DisplayMemberPath = "DoorName";
                    lstDoor.SelectedValuePath = "DoorNo";
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error : " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (lstAccessTypes.SelectedItems.Count <= 0)
            {
                System.Windows.MessageBox.Show("Select an Access Type to Remove!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                lstAccessTypes.Focus();
                return;
            }
            string _remAccessTypeName = (lstAccessTypes.SelectedItem as DataRowView).Row["AccessTypeName"].ToString().ToUpper();

            if (_isNewSite)
            {
                for (int i = _dtNewSiteAccessType.Rows.Count - 1; i >= 0; i--)
                {
                    if (_dtNewSiteAccessType.Rows[i]["AccessTypeName"].ToString().ToUpper() == _remAccessTypeName)
                    {
                        _dtNewSiteAccessType.Rows[i].Delete();
                        _dtNewSiteAccessType.AcceptChanges();
                        break;
                    }
                }
                lstAccessTypes.ItemsSource = null;
                lstAccessTypes.ItemsSource = _dtNewSiteAccessType.DefaultView;
            }
            else
            {
                for (int i = _dsSiteDetails.Tables[_AccessTypeTable].Rows.Count - 1; i >= 0; i--)
                {
                    if (_dsSiteDetails.Tables[_AccessTypeTable].Rows[i]["AccessTypeName"].ToString().ToUpper() == _remAccessTypeName)
                    {
                        _dsSiteDetails.Tables[_AccessTypeTable].Rows[i].Delete();
                        _dsSiteDetails.Tables[_AccessTypeTable].AcceptChanges();
                        _dsSiteDetails.AcceptChanges();

                        break;
                    }
                }
                lstAccessTypes.ItemsSource = null;
                lstAccessTypes.ItemsSource = _dsSiteDetails.Tables[_AccessTypeTable].DefaultView;
            }
            txtNewAccessType.Text = "";
            txtNewAccessType.Focus();
        }

        private void btnAddDoor_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtNewDoor.Text.Trim() == "")
                {
                    System.Windows.MessageBox.Show("Please Enter a Door Number!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    txtNewDoor.Focus();
                    return;
                }
                if (!txtNewDoor.Text.All(c => Char.IsNumber(c)))
                {
                    System.Windows.MessageBox.Show("Please enter Numeric value!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    txtNewDoor.Text = "";
                    txtNewDoor.Focus();
                    return;
                }
                if (DuplicateDoor(txtNewDoor.Text.Trim().ToUpper()))
                {
                    System.Windows.MessageBox.Show("Door Already Exists!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    txtNewDoor.Focus();
                    return;
                }
                if (txtDoorCode.Text.Trim() == "")
                {
                    System.Windows.MessageBox.Show("Please Enter a Door Code (e.g \"R\" - to be used in AccessControl) !", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    txtDoorCode.Focus();
                    return;
                }
                if (txtDoorName.Text.Trim() == "")
                {
                    System.Windows.MessageBox.Show("Please Enter a Door Name / Description!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    txtDoorName.Focus();
                    return;
                }
                if (DuplicateDoorName(txtDoorName.Text.Trim().ToUpper()))
                {
                    System.Windows.MessageBox.Show("Door Name Already Exists!!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    txtDoorName.Focus();
                    return;
                }

                AddDoor
                        (
                            txtNewDoor.Text.Trim().ToUpper(), txtDoorCode.Text.Trim().ToUpper(), 
                            txtDoorName.Text.Trim().ToUpper(), cmbPORTDoor.Text.ToUpper().Trim(), 
                            txtDoorPCName.Text.ToUpper().Trim()
                        );

                txtNewDoor.Text = ""; txtDoorCode.Text = ""; txtDoorName.Text = "";
                cmbPORTDoor.Text = ""; txtDoorPCName.Text = "";
                txtNewDoor.Focus();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error : " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnRemoveDoor_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lstDoor.SelectedItems.Count <= 0)
                {
                    System.Windows.MessageBox.Show("Select a Door to Remove!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    lstDoor.Focus();
                    return;
                }
                string _remDoorNo = (lstDoor.SelectedItem as DataRowView).Row["DoorNo"].ToString().ToUpper();

                if (_isNewSite)
                {
                    for (int i = _dtNewSiteDoors.Rows.Count - 1; i >= 0; i--)
                    {
                        if (_dtNewSiteDoors.Rows[i]["DoorNo"].ToString().ToUpper() == _remDoorNo)
                        {
                            _dtNewSiteDoors.Rows[i].Delete();
                            _dtNewSiteDoors.AcceptChanges();
                            break;
                        }
                    }
                    lstDoor.ItemsSource = null;
                    lstDoor.ItemsSource = _dtNewSiteDoors.DefaultView;
                }
                else
                {
                    for (int i = _dsSiteDetails.Tables[_DoorTable].Rows.Count - 1; i >= 0; i--)
                    {
                        if (_dsSiteDetails.Tables[_DoorTable].Rows[i]["DoorNo"].ToString().ToUpper() == _remDoorNo)
                        {
                            _dsSiteDetails.Tables[_DoorTable].Rows[i].Delete();
                            _dsSiteDetails.Tables[_DoorTable].AcceptChanges();
                            _dsSiteDetails.AcceptChanges();
                            break;
                        }
                    }
                    lstDoor.ItemsSource = null;
                    lstDoor.ItemsSource = _dsSiteDetails.Tables[_DoorTable].DefaultView;
                }

                txtNewDoor.Text = ""; txtDoorName.Text = ""; txtDoorCode.Text = "";
                cmbPORTDoor.Text = ""; txtDoorPCName.Text = "";
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error : " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private bool ValidateData()
        {
            if (txtSiteName.Text.Trim() == "")
            {
                System.Windows.MessageBox.Show("Please Enter Site Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                tabControl.SelectedIndex = 0;
                txtSiteName.Focus();
                return false;
            }

            if (_isNewSite)
            {
                for (int i = 0; i < cmbSiteName.Items.Count; i++)
                {
                    string s = (cmbSiteName.Items[i] as DataRowView).Row["SiteName"].ToString().ToUpper();
                    if (txtSiteName.Text.Trim().ToUpper() == s)
                    {
                        System.Windows.MessageBox.Show("Site Name Already Exists! Please Enter a Different Site Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        tabControl.SelectedIndex = 0;
                        txtSiteName.Focus();
                        return false;
                    }
                }
            }
            else
            {
                int _sIndex = cmbSiteName.SelectedIndex;
                for (int i = 0; i < cmbSiteName.Items.Count; i++)
                {
                    if (i != _sIndex)
                    {
                        string s = (cmbSiteName.Items[i] as DataRowView).Row["SiteName"].ToString().ToUpper();
                        if (txtSiteName.Text.Trim().ToUpper() == s)
                        {
                            System.Windows.MessageBox.Show("Site Name Already Exists! Please Enter a Different Site Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            tabControl.SelectedIndex = 0;
                            txtSiteName.Focus();
                            return false;
                        }
                    }
                }
            }
            if (txtFilePath.Text.Trim() == "")
            {
                System.Windows.MessageBox.Show("Please Select Folder Path to save Access Control Files!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
            if (txtFileName.Text.Trim() == "")
            {
                System.Windows.MessageBox.Show("Please Specify Access Control File Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
            if (txtPOSOpeningBal.Text.Trim() == "")
            {
                txtPOSOpeningBal.Text = "0.00";
            }
            else if (!Regex.IsMatch(txtPOSOpeningBal.Text, @"^\d{1,7}(\.\d{1,2})?$"))
            {
                System.Windows.MessageBox.Show("Opening Balance should be Numeric!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtPOSOpeningBal.Text = "0.00";
                return false;
            }
            return true;
        }

        private void btnContactTypes1_Click(object sender, RoutedEventArgs e)
        {
            //SAME WINDOW; NO ACTION;

            //CompanyDetails _CompanySiteDetails = new CompanyDetails();
            //_CompanySiteDetails._CompanyName = this._CompanyName;
            //_CompanySiteDetails._UserName = this._UserName;
            //_CompanySiteDetails._CurrentLoginDateTime = this._CurrentLoginDateTime;
            //_CompanySiteDetails._StaffPic = this._StaffPic;
            //_CompanySiteDetails.StaffImage.Source = StaffImage.Source;
            //_CompanySiteDetails.Owner = this.Owner;

            //this.Close();
            //_CompanySiteDetails.ShowDialog();
        }

        private void btnUserAdministration_Click(object sender, RoutedEventArgs e)
        {
            if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
            {
                UserAdministration useradmin = new UserAdministration();
                useradmin._CompanyName = this._CompanyName;
                useradmin._UserName = this._UserName;
                useradmin._CurrentLoginDateTime = this._CurrentLoginDateTime;
                useradmin._StaffPic = this._StaffPic;
                useradmin.StaffImage.Source = StaffImage.Source;
                useradmin.Owner = this.Owner;

                this.Close();
                useradmin.ShowDialog();
            }
            else
            {
                MessageBox.Show("You MUST be Admin user to access this Screen!");
                return;
            }
        }

        private void btnProductsTypes_Click(object sender, RoutedEventArgs e)
        {
           
            ProductType prdType = new ProductType();
            prdType._CompanyName = this._CompanyName;
            prdType._UserName = this._UserName;
            prdType._CurrentLoginDateTime = this._CurrentLoginDateTime;
            prdType._StaffPic = this._StaffPic;
            prdType.StaffImage.Source = StaffImage.Source;
            prdType.Owner = this.Owner;

            this.Close();
            prdType.ShowDialog();
        }

        private void btnManageProducts_Click(object sender, RoutedEventArgs e)
        {
            Products products = new Products();
            products._CompanyName = this._CompanyName;
            products._UserName = this._UserName;
            products._CurrentLoginDateTime = this._CurrentLoginDateTime;
            products._StaffPic = this._StaffPic;
            products.StaffImage.Source = StaffImage.Source;
            products.Owner = this.Owner;

            this.Close();
            products.ShowDialog();
        }

        private void btnProgrammeGSetup_Click(object sender, RoutedEventArgs e)
        {
            ProgrammeGroup PrgGroup = new ProgrammeGroup();
            PrgGroup._CompanyName = this._CompanyName;
            PrgGroup._UserName = this._UserName;
            PrgGroup._CurrentLoginDateTime = this._CurrentLoginDateTime;
            PrgGroup._StaffPic = this._StaffPic;
            PrgGroup.StaffImage.Source = StaffImage.Source;
            PrgGroup.Owner = this.Owner;

            this.Close();
            PrgGroup.ShowDialog();
        }

        private void btnAddEditProgrammes_Click(object sender, RoutedEventArgs e)
        {
            Programme Prg = new Programme();
            Prg._CompanyName = this._CompanyName;
            Prg._UserName = this._UserName;
            Prg._CurrentLoginDateTime = this._CurrentLoginDateTime;
            Prg._StaffPic = this._StaffPic;
            Prg.StaffImage.Source = StaffImage.Source;
            Prg.Owner = this.Owner;

            this.Close();
            Prg.ShowDialog();
        }

        private void btnSupplierSetup_Click(object sender, RoutedEventArgs e)
        {
            Supplier _Supplier = new Supplier();
            _Supplier._CompanyName = this._CompanyName;
            _Supplier._UserName = this._UserName;
            _Supplier._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Supplier._StaffPic = this._StaffPic;
            _Supplier.StaffImage.Source = StaffImage.Source;
            _Supplier.Owner = this.Owner;

            this.Close();
            _Supplier.ShowDialog();
        }

        private void btnBookingResources_Click(object sender, RoutedEventArgs e)
        {
            Settings.Resources _Res = new Settings.Resources();
            _Res._CompanyName = this._CompanyName;
            _Res._UserName = this._UserName;
            _Res._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Res._StaffPic = this._StaffPic;
            _Res.StaffImage.Source = StaffImage.Source;
            _Res.Owner = this.Owner;

            this.Close();
            _Res.ShowDialog();
        }

        private void btnUserAccessRights_Click(object sender, RoutedEventArgs e)
        {
            StaffAccess _StaffAccess = new StaffAccess();
            _StaffAccess._CompanyName = this._CompanyName;
            _StaffAccess._UserName = this._UserName;
            _StaffAccess._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _StaffAccess._StaffPic = this._StaffPic;
            _StaffAccess.StaffImage.Source = StaffImage.Source;
            _StaffAccess.Owner = this.Owner;

            this.Close();
            _StaffAccess.ShowDialog();
        }

        private void btnEmailTSetup_Click(object sender, RoutedEventArgs e)
        {
            EmailTemplateSettings _eMailTemplate = new EmailTemplateSettings();
            _eMailTemplate._CompanyName = this._CompanyName;
            _eMailTemplate._UserName = this._UserName;
            _eMailTemplate._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _eMailTemplate._StaffPic = this._StaffPic;
            _eMailTemplate.StaffImage.Source = StaffImage.Source;
            _eMailTemplate.Owner = this.Owner;

            this.Close();
            _eMailTemplate.ShowDialog();
        }
        private void btnContactTypes_Click(object sender, RoutedEventArgs e)
        {
            ContactTypes _ContactTypes = new ContactTypes();
            _ContactTypes._CompanyName = this._CompanyName;
            _ContactTypes._UserName = this._UserName;
            _ContactTypes._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _ContactTypes._StaffPic = this._StaffPic;
            _ContactTypes.StaffImage.Source = StaffImage.Source;
            _ContactTypes.Owner = this.Owner;

            this.Close();
            _ContactTypes.ShowDialog();
        }
        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDashboardQuit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
                //Closing event called
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }


        private void loadStaffPhoto()
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }

        private void cmbPORT_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_windowLoaded)
            {
                int i = cmbPORT.SelectedIndex;
                if (i >= 0)
                {
                    lblPORTDetails.Visibility = Visibility.Collapsed;
                }
                else
                {
                    lblPORTDetails.Visibility = Visibility.Collapsed;
                }
            }
        }
        
        private void lstAccessTypes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (_windowLoaded)
                {
                    if (lstAccessTypes.SelectedIndex != -1)
                    {
                        txtNewAccessType.Text = (lstAccessTypes.SelectedItem as DataRowView).Row["AccessTypeName"].ToString().ToUpper().Trim();
                    }
                }
            }
            catch(Exception x)
            {
                string _msg = x.Message;
            }
        }

        private void btnToggle_Checked(object sender, RoutedEventArgs e)
        {
            if (_windowLoaded)
            {
                _localPathFolder = true;
                btnToggle.Content = "ON";
            }
        }

        private void btnToggle_Unchecked(object sender, RoutedEventArgs e)
        {
            if (_windowLoaded)
            {
                _localPathFolder = false;
                btnToggle.Content = "OFF";
            }
        }

        private void btnNetworkFilePath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.FolderBrowserDialog commonOpenFileDialog = new System.Windows.Forms.FolderBrowserDialog();
                commonOpenFileDialog.ShowDialog();

                string fileNamePath = commonOpenFileDialog.SelectedPath;
                if (fileNamePath != "")
                {
                    txtNetworkFilePath.Text = fileNamePath;
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void rbTGPRODActive_Checked(object sender, RoutedEventArgs e)
        {
            if (_windowLoaded)
            {
                lblTGtestProd.Content = "PROD";
            }
        }

        private void rbTGtestActive_Checked(object sender, RoutedEventArgs e)
        {
            if (_windowLoaded)
            {
                lblTGtestProd.Content = "TEST";
            }
        }

        private void btnGetTechnoGymToken_Click(object sender, RoutedEventArgs e)
        {
        }

        private void lstDoor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (_windowLoaded)
                {
                    if (lstDoor.SelectedIndex != -1)
                    {
                        int _doorNo = Convert.ToInt32(lstDoor.SelectedValue.ToString());
                        if (_isNewSite)
                        {
                            for (int i = 0; i < _dtNewSiteDoors.DefaultView.Count; i++)
                            {
                                if (_dtNewSiteDoors.Rows[i]["DoorNo"].ToString() == _doorNo.ToString())
                                {
                                    txtNewDoor.Text = _dtNewSiteDoors.Rows[i]["DoorNo"].ToString();
                                    txtDoorCode.Text = _dtNewSiteDoors.Rows[i]["DoorCode"].ToString();
                                    txtDoorName.Text = _dtNewSiteDoors.Rows[i]["DoorName"].ToString();

                                    //GAS-28 : COM Port - Multiple Doors
                                    cmbPORTDoor.Text = _dtNewSiteDoors.Rows[i]["COMPort"].ToString();
                                    txtDoorPCName.Text = _dtNewSiteDoors.Rows[i]["AccPCNAME"].ToString();
                                    //GAS-28 : COM Port - Multiple Doors

                                    break;
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < _dsSiteDetails.Tables[_DoorTable].DefaultView.Count; i++)
                            {
                                if (_dsSiteDetails.Tables[_DoorTable].Rows[i]["DoorNo"].ToString() == _doorNo.ToString())
                                {
                                    txtNewDoor.Text = _dsSiteDetails.Tables[_DoorTable].Rows[i]["DoorNo"].ToString();
                                    txtDoorCode.Text = _dsSiteDetails.Tables[_DoorTable].Rows[i]["DoorCode"].ToString();
                                    txtDoorName.Text = _dsSiteDetails.Tables[_DoorTable].Rows[i]["DoorName"].ToString();

                                    //GAS-28 : COM Port - Multiple Doors
                                    cmbPORTDoor.Text = _dsSiteDetails.Tables[_DoorTable].Rows[i]["COMPort"].ToString();
                                    txtDoorPCName.Text = _dsSiteDetails.Tables[_DoorTable].Rows[i]["AccPCNAME"].ToString();
                                    //GAS-28 : COM Port - Multiple Doors

                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error : " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        //void CompanyDetails_Closing(object sender, CancelEventArgs e)
        //{
        //    try
        //    {
        //        if (!_IsIdle)
        //        {
        //            if (MessageBox.Show("Are you sure to LOGOFF ?", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
        //            {
        //                if (!Comman.Constants._OfflineFlag)
        //                {
        //                    Comman.Constants._DisConnectedDataAdapter.Update(Comman.Constants._DisConnectedALLTables);
        //                }
        //                Comman.Constants._OfflineFlag = true;
        //                _Login.cmbOrgName.Text = "";
        //                _Login.txtPassword.Password = "";
        //                _Login.txtUserName.Text = "";
        //                _Login.ShowInTaskbar = true;
        //                _Login.Show();
        //                _Login.cmbOrgName.Focus();
        //            }
        //            else
        //            {
        //                e.Cancel = true;
        //                return;
        //            }
        //        }
        //        else
        //        {
        //            _Login.cmbOrgName.Text = "";
        //            _Login.txtPassword.Password = "";
        //            _Login.txtUserName.Text = "";
        //            _Login.ShowInTaskbar = true;
        //            _Login.Show();
        //            _Login.cmbOrgName.Focus();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Error! " + ex.Message);
        //        return;
        //    }
        //}
    }
}
