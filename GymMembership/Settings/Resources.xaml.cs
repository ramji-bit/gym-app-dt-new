﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Configuration;
using System.Data;
using System.IO;

using GymMembership.Comman;
using GymMembership.BAL;
using GymMembership.Accounts;
using GymMembership.DTO;

namespace GymMembership.Settings
{
    /// <summary>
    /// Interaction logic for Resources.xaml
    /// </summary>
    public partial class Resources 
    {
        public Resources()
        {
            InitializeComponent();
        }
        private int _ResourceTypeId;
        private int _ResourceId;
        private string _ResourceName;
        private int _ResourceStatusCheck;
        private int _ResourceMaxMembers; private int _ResourceMaxWaitList;
        private bool _ResourceStatus;
        private bool _IsEdit=false;
        private bool reval;
        private int _addSuccess;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public Login _Login;
        Utilities _utilities = new Utilities();
        SettingsBAL _settingsBAL = new SettingsBAL();
        DataTable _dtSlots; int _sTimeIndex = -1; int _eTimeIndex = -1; bool _isStartEdited = false; int _RowEdited = -1; bool _isEndEdited = false;
        int _Slots; int _fTable = 0; int _fSlotTable = 1; bool _isSlotsEmpty = false;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _utilities.populateResourceTypeById(cmbResourceType);
            //this.Title = "CompanyDetails";
            lblOrgName.Content = _CompanyName.ToString().Replace("_", "__");
            lblUserName.Content = _UserName.ToString().Replace("_", "__");
            //lblSiteName.Content = _SiteName.ToString().Replace("_", "__");
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto();

            gvTimeSlots.PreparingCellForEdit += new EventHandler<DataGridPreparingCellForEditEventArgs>(gvTimeSlots_PreparingCellForEdit);
        }
        void gvTimeSlots_PreparingCellForEdit(object sender, DataGridPreparingCellForEditEventArgs e)
        {
            if (e.Column is DataGridComboBoxColumn)
            {
                _RowEdited = e.Row.GetIndex();
                if (e.Column.DisplayIndex == 1) { _isStartEdited = true; _isEndEdited = false; } else { _isStartEdited = false; _isEndEdited = true; }
                ComboBox cmb = e.EditingElement as ComboBox;
                cmb.SelectionChanged += new SelectionChangedEventHandler(cmb_SelectionChanged);
            }
        }
        void cmb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (_isStartEdited)
                {
                    _sTimeIndex = (sender as ComboBox).SelectedIndex;
                    if (_sTimeIndex != -1)
                        _dtSlots.Rows[_RowEdited]["sTimeIndex"] = _sTimeIndex;
                    _isStartEdited = false;
                }
                else if (_isEndEdited)
                {
                    _eTimeIndex = (sender as ComboBox).SelectedIndex;
                    if (_eTimeIndex != -1)
                        _dtSlots.Rows[_RowEdited]["eTimeIndex"] = _eTimeIndex;
                    _isEndEdited = false;
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Please DO NOT Add extra Sessions! Max. Allowed is only : "  + txtNumSlots.Text + "\n\n(OR)\n\nPlease modify No. of Sessions in 'Details' Page", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                try
                {
                    gvTimeSlots.ItemsSource = null;
                    for (int i = _dtSlots.Rows.Count - 1; i >= _Slots; i--)
                    {
                        _dtSlots.Rows.RemoveAt(i);
                    }
                    gvTimeSlots.ItemsSource = _dtSlots.DefaultView;
                }
                catch(Exception ex1)
                {
                    MessageBox.Show(ex1.Message);
                    return;
                }
                return;
            }
        }
        private void loadStaffPhoto()
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }

        public void Disable()
        {
            btnResourceDetailsEdit.IsEnabled = false;
            btnResourceDetailsSave.IsEnabled = false;
            txtResourcesDetailsName.IsEnabled = false;
            txtResourcesDetailsName.Text = string.Empty;
            chkResourcesStatus.IsChecked = false;
            chkResourcesStatus.IsEnabled = false;

            txtMaxMembers.IsEnabled = false;
            txtMaxMembers.Text = string.Empty;
            txtMaxWaitList.IsEnabled = false;
            txtMaxWaitList.Text = string.Empty;

            txtChargePerBooking.IsEnabled = false;
            txtChargePerBooking.Text = string.Empty;
            txtNumSlots.IsEnabled = false;
            txtNumSlots.Text = string.Empty;

            gvTimeSlots.ItemsSource = null;
            gvTimeSlots.Items.Clear();
        }
        private void cmbResourceType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _ResourceTypeId = Convert.ToInt32(cmbResourceType.SelectedValue);
            _utilities.get_ResourcesByID(cmbResources, _ResourceTypeId);
            txtResourcesDetailsName.IsEnabled = false;
            txtMaxMembers.IsEnabled = false;
            txtMaxWaitList.IsEnabled = false;
            txtChargePerBooking.IsEnabled = false;
            txtNumSlots.IsEnabled = false;
            chkResourcesStatus.IsEnabled = false;
            btnResourceDetailsNew.IsEnabled = true;
        }
        private void cmbResources_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                _ResourceId = Convert.ToInt32(cmbResources.SelectedValue);
                DataSet ds = _settingsBAL.get_ResourceByFacilityId(_ResourceId);

                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    { 
                        txtResourcesDetailsName.Text = ds.Tables[_fTable].Rows[0]["FacilityName"].ToString().Trim();
                        txtMaxMembers.Text = ds.Tables[_fTable].Rows[0]["MaxMembers"].ToString().Trim();
                        txtMaxWaitList.Text = ds.Tables[_fTable].Rows[0]["MaxWaitList"].ToString().Trim();
                        txtNumSlots.Text = ds.Tables[_fTable].Rows[0]["NumTimeSlots"].ToString().Trim();
                        txtChargePerBooking.Text = ds.Tables[_fTable].Rows[0]["ChargePerBooking"].ToString().Trim();

                        if (Convert.ToInt32(ds.Tables[_fTable].Rows[0]["FacilityStatus"]) == 1)
                        {
                            chkResourcesStatus.IsChecked = true;
                        }
                        else
                        {
                            chkResourcesStatus.IsChecked = false;
                        }
                        txtResourcesDetailsName.IsEnabled = false;
                        chkResourcesStatus.IsEnabled = false;
                        btnResourceDetailsEdit.IsEnabled = true;
                        btnResourceDetailsSave.IsEnabled = false;
                        txtMaxMembers.IsEnabled = false;
                        txtMaxWaitList.IsEnabled = false;

                        txtChargePerBooking.IsEnabled = false;
                        txtNumSlots.IsEnabled = false;
                        btnNext.IsEnabled = false;
                        TimeSlots.IsEnabled = false;

                        if (ds.Tables.Count > 1)
                        {
                            gvTimeSlots.ItemsSource = ds.Tables[_fSlotTable].DefaultView;
                            _dtSlots = ds.Tables[_fSlotTable];
                            if (ds.Tables[_fSlotTable].DefaultView.Count <= 0)
                            {
                                _isSlotsEmpty = true;
                            }
                            else
                            {
                                _isSlotsEmpty = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnResourceDetailsNew_Click(object sender, RoutedEventArgs e)
        {
            cmbResources.SelectedValue = 0;
            chkResourcesStatus.IsEnabled = true;
            txtResourcesDetailsName.IsEnabled = true;
            txtResourcesDetailsName.Text = string.Empty;
            chkResourcesStatus.IsChecked =false;
            btnResourceDetailsSave.IsEnabled = true;
            btnResourceDetailsEdit.IsEnabled = false;

            txtMaxMembers.IsEnabled = true;
            txtMaxMembers.Text = string.Empty;
            txtMaxWaitList.IsEnabled = true;
            txtMaxWaitList.Text = string.Empty;

            txtChargePerBooking.IsEnabled = true;
            txtChargePerBooking.Text = string.Empty;
            txtNumSlots.IsEnabled = true;
            txtNumSlots.Text = string.Empty;
            btnNext.IsEnabled = true;

            txtResourcesDetailsName.Focus();
            _IsEdit = false;
            _isSlotsEmpty = true;
        }

        private void btnResourceDetailsCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnResourceDetailsSave_Click(object sender, RoutedEventArgs e)
        {
            ////for (int i = 0; i < _dtSlots.DefaultView.Count; i++)
            ////{
            ////    DataRowView _dv = (gvTimeSlots.Items[i] as DataRowView);
            ////    MessageBox.Show("Slot: " + _dv.Row["SlotSequence"].ToString() + " Start Time: (Index = " + _dv.Row["sTimeIndex"].ToString() + " )" + _dv.Row["StartTime"].ToString() + " End Time: (Index = " + _dv.Row["eTimeIndex"].ToString() + " )" + _dv.Row["EndTime"].ToString());
            ////}

            if (ValidateData())
            {
                if (ValidateTimeSlot())
                {
                    BookingResourceDTO _bookingResourceDTO = new BookingResourceDTO();
                    _ResourceName = txtResourcesDetailsName.Text.ToString().Trim();
                    _ResourceStatus = Convert.ToBoolean(chkResourcesStatus.IsChecked.ToString());
                    _ResourceMaxMembers = Convert.ToInt32(txtMaxMembers.Text.ToString().Trim());
                    _ResourceMaxWaitList = Convert.ToInt32(txtMaxWaitList.Text.ToString().Trim());

                    if (_ResourceStatus == true)
                    {
                        _ResourceStatusCheck = 1;
                    }
                    else
                    {
                        _ResourceStatusCheck = 0;
                    }

                    _bookingResourceDTO._ChargePerSlot = Convert.ToDecimal(txtChargePerBooking.Text);
                    _bookingResourceDTO._dtSlots = _dtSlots;
                    _bookingResourceDTO._FacilityID = 0;
                    _bookingResourceDTO._FacilityName = _ResourceName;
                    _bookingResourceDTO._FacilityStatus = Convert.ToInt32(_ResourceStatus);
                    _bookingResourceDTO._isUpdate = 0;
                    _bookingResourceDTO._MaxMembers = _ResourceMaxMembers;
                    _bookingResourceDTO._MaxWaitList = _ResourceMaxWaitList;
                    _bookingResourceDTO._NumTimeSlots = Convert.ToInt32(txtNumSlots.Text);
                    _bookingResourceDTO._ResourceTypeID = _ResourceTypeId;

                    if (!_IsEdit)
                    {
                        _bookingResourceDTO._isUpdate = 0;
                        if (cmbResources.Items.Count > 0)
                        {
                            for (int i = 0; i < cmbResources.Items.Count; i++)
                            {
                                //cmbResources.SelectedIndex = i;
                                int j = cmbResources.Items.Count - 1;
                                //string _existingName = cmbResources.Text;
                                string _existingName = (cmbResources.Items[i] as DataRowView).Row["FacilityName"].ToString();
                                string _new = _ResourceName;
                                if (_new.ToUpper() == _existingName.ToUpper())
                                {
                                    MessageBox.Show("Resource Already Exists!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                    return;
                                }
                            }
                            _addSuccess = _settingsBAL.saveNew_ResourcesDetails(_bookingResourceDTO);
                            MessageBox.Show("Resource Name Added ! " + _ResourceName, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            cmbResources.SelectedValue = 0;
                            _utilities.get_ResourcesByID(cmbResources, _ResourceTypeId);
                            Disable();
                            return;
                        }
                        else
                        {
                            _addSuccess = _settingsBAL.saveNew_ResourcesDetails(_bookingResourceDTO);
                            MessageBox.Show("Resource Name Added ! " + _ResourceName, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            cmbResources.SelectedValue = 0;
                            _utilities.get_ResourcesByID(cmbResources, _ResourceTypeId);
                            Disable();
                            return;
                        }
                    }
                    else
                    {
                        int _resourceIdCheck = Convert.ToInt32(cmbResources.SelectedValue);
                        _bookingResourceDTO._FacilityID = _resourceIdCheck;
                        _bookingResourceDTO._isUpdate = 1;

                        int j = cmbResources.SelectedIndex;
                        for (int i = 0; i < cmbResources.Items.Count; i++)
                        {
                            //cmbResources.SelectedIndex = i;
                            //string _existingName = cmbResources.Text;
                            string _existingName = (cmbResources.Items[i] as DataRowView).Row["FacilityName"].ToString();
                            string _new = _ResourceName;
                            if (_new.ToUpper() == _existingName.ToUpper() && (i != j))// && (_resourceIdCheck != Convert.ToInt32(cmbResources.SelectedValue.ToString().Trim())))
                            {
                                MessageBox.Show("Resource Already Exists!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                return;
                            }
                        }
                        _addSuccess = _settingsBAL.saveNew_ResourcesDetails(_bookingResourceDTO);
                        MessageBox.Show("Resource Name Updated ! " + _ResourceName, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        cmbResources.SelectedValue = 0;
                        _utilities.get_ResourcesByID(cmbResources, _ResourceTypeId);
                        Disable();
                    }
                    tabResource.SelectedIndex = 0;
                }
            }
        }
        private bool ValidateData()
        {
            if (txtResourcesDetailsName.Text.Trim() == "")
            {
                MessageBox.Show("Please Enter Facility Name!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtResourcesDetailsName.Focus();
                return false;
            }
            if (txtMaxMembers.Text.Trim() == "")
            {
                MessageBox.Show("Please Enter Max. No. of Members allowed!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtMaxMembers.Focus();
                return false;
            }
            if (!txtMaxMembers.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Please Enter Max. No. of Members allowed in NUMBERS!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtMaxMembers.Focus();
                return false;
            }
            if (txtMaxWaitList.Text.Trim() == "")
            {
                MessageBox.Show("Please Enter Max No. of Members that can be Waitlisted!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtMaxWaitList.Focus();
                return false;
            }
            if (!txtMaxWaitList.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Please Enter Max No. of Members that can be Waitlisted in NUMBERS!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtMaxWaitList.Focus();
                return false;
            }

            if (txtNumSlots.Text.Trim() == "")
            {
                MessageBox.Show("Please Enter No. of Slots per day!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtNumSlots.Focus();
                return false;
            }
            if (!txtNumSlots.Text.All(c => Char.IsNumber(c)))
            {
                MessageBox.Show("Please Enter No. of Slots in NUMBERS!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtNumSlots.Focus();
                return false;
            }
            if (Convert.ToInt32(txtNumSlots.Text) <= 0)
            {
                MessageBox.Show("No. of Slots CANNOT be Zero or Less!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtNumSlots.Focus();
                return false;
            }
            if (Convert.ToInt32(txtNumSlots.Text) > 4)
            {
                MessageBox.Show("No. of Slots CANNOT be more than 4!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtNumSlots.Focus();
                return false;
            }
            if (txtChargePerBooking.Text.Trim() == "")
            {
                MessageBox.Show("Enter Charge amount per Booking!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtChargePerBooking.Focus();
                return false;
            }
            if (!Regex.IsMatch(txtChargePerBooking.Text, @"^\d{1,2}(\.\d{1,2})?$"))
            {
                txtChargePerBooking.Text = "";
                MessageBox.Show("Please enter Charge in Numbers only (upto 2 decimal places)\n(OR)\nCharge should be Less than 99.99!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtChargePerBooking.Focus();
                return false;
            }
            if (Convert.ToDecimal(txtChargePerBooking.Text) == 0)
            {
                txtChargePerBooking.Text = "";
                MessageBox.Show("Charges CANNOT be Zero!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtChargePerBooking.Focus();
                return false;
            }
            return true;
        }
        private bool ValidateTimeSlot()
        {
            if (_Slots > _dtSlots.DefaultView.Count)
            {
                _isSlotsEmpty = true;
                MessageBox.Show("Session Time Slots Not Defined!\n\nClick 'Next' to Add Time Slots", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            for (int i = 0; i < _dtSlots.DefaultView.Count; i++) //check if start:time is > end:time
            {
                if (Convert.ToInt32(_dtSlots.DefaultView[i].Row["sTimeIndex"].ToString()) >= Convert.ToInt32(_dtSlots.DefaultView[i].Row["eTimeIndex"].ToString()))
                {
                    MessageBox.Show("(Session No. " + (i+1) + ") Start Time CANNOT be Greater than End Time!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    return false;
                }
            }

            for (int i = 0; i < _dtSlots.DefaultView.Count; i++) //check if Starttime of subsequent timeslot is < end:time of prevtime slot
            {
                if (i != 0)
                {
                    if (Convert.ToInt32(_dtSlots.DefaultView[i].Row["sTimeIndex"].ToString()) < Convert.ToInt32(_dtSlots.DefaultView[i-1].Row["eTimeIndex"].ToString()))
                    {
                        MessageBox.Show("(Session No. " + (i + 1) + ") Slot Time CANNOT Overlap!\n(OR)\nSlot Time NOT in Order!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        return false;
                    }
                }
            }

            return true;
        }
        private void btnResourceDetailsEdit_Click(object sender, RoutedEventArgs e)
        {
            _IsEdit = true;
            txtResourcesDetailsName.IsEnabled = true;
            chkResourcesStatus.IsEnabled = true;
            btnResourceDetailsSave.IsEnabled = true;
            btnResourceDetailsEdit.IsEnabled = false;

            txtMaxMembers.IsEnabled = true;
            txtMaxWaitList.IsEnabled = true;

            txtChargePerBooking.IsEnabled = true;
            txtNumSlots.IsEnabled = true;
            btnNext.IsEnabled = true;
        }


        private void btnContactTypes1_Click(object sender, RoutedEventArgs e)
        {
            CompanyDetails _CompanySiteDetails = new CompanyDetails();
            _CompanySiteDetails._CompanyName = this._CompanyName;
            _CompanySiteDetails._UserName = this._UserName;
            _CompanySiteDetails._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _CompanySiteDetails._StaffPic = this._StaffPic;
            _CompanySiteDetails.StaffImage.Source = StaffImage.Source;
            _CompanySiteDetails.Owner = this.Owner;

            this.Close();
            _CompanySiteDetails.ShowDialog();
        }

        private void btnUserAdministration_Click(object sender, RoutedEventArgs e)
        {
            if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
            {
                UserAdministration useradmin = new UserAdministration();
                useradmin._CompanyName = this._CompanyName;
                useradmin._UserName = this._UserName;
                useradmin._CurrentLoginDateTime = this._CurrentLoginDateTime;
                useradmin._StaffPic = this._StaffPic;
                useradmin.StaffImage.Source = StaffImage.Source;
                useradmin.Owner = this.Owner;

                this.Close();
                useradmin.ShowDialog();
            }
            else
            {
                MessageBox.Show("You MUST be Admin user to access this Screen!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnProductsTypes_Click(object sender, RoutedEventArgs e)
        {

            ProductType prdType = new ProductType();
            prdType._CompanyName = this._CompanyName;
            prdType._UserName = this._UserName;
            prdType._CurrentLoginDateTime = this._CurrentLoginDateTime;
            prdType._StaffPic = this._StaffPic;
            prdType.StaffImage.Source = StaffImage.Source;
            prdType.Owner = this.Owner;

            this.Close();
            prdType.ShowDialog();
        }

        private void btnManageProducts_Click(object sender, RoutedEventArgs e)
        {
            Products products = new Products();
            products._CompanyName = this._CompanyName;
            products._UserName = this._UserName;
            products._CurrentLoginDateTime = this._CurrentLoginDateTime;
            products._StaffPic = this._StaffPic;
            products.StaffImage.Source = StaffImage.Source;
            products.Owner = this.Owner;

            this.Close();
            products.ShowDialog();
        }

        private void btnProgrammeGSetup_Click(object sender, RoutedEventArgs e)
        {
            ProgrammeGroup PrgGroup = new ProgrammeGroup();
            PrgGroup._CompanyName = this._CompanyName;
            PrgGroup._UserName = this._UserName;
            PrgGroup._CurrentLoginDateTime = this._CurrentLoginDateTime;
            PrgGroup._StaffPic = this._StaffPic;
            PrgGroup.StaffImage.Source = StaffImage.Source;
            PrgGroup.Owner = this.Owner;

            this.Close();
            PrgGroup.ShowDialog();
        }

        private void btnAddEditProgrammes_Click(object sender, RoutedEventArgs e)
        {
            Programme Prg = new Programme();
            Prg._CompanyName = this._CompanyName;
            Prg._UserName = this._UserName;
            Prg._CurrentLoginDateTime = this._CurrentLoginDateTime;
            Prg._StaffPic = this._StaffPic;
            Prg.StaffImage.Source = StaffImage.Source;
            Prg.Owner = this.Owner;

            this.Close();
            Prg.ShowDialog();
        }

        private void btnSupplierSetup_Click(object sender, RoutedEventArgs e)
        {
            Supplier _Supplier = new Supplier();
            _Supplier._CompanyName = this._CompanyName;
            _Supplier._UserName = this._UserName;
            _Supplier._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Supplier._StaffPic = this._StaffPic;
            _Supplier.StaffImage.Source = StaffImage.Source;
            _Supplier.Owner = this.Owner;

            this.Close();
            _Supplier.ShowDialog();
        }

        private void btnBookingResources_Click(object sender, RoutedEventArgs e)
        {
            ////SAME WINDOW; NO ACTION

            //Settings.Resources _Res = new Settings.Resources();
            //_Res._CompanyName = this._CompanyName;
            //_Res._UserName = this._UserName;
            //_Res._CurrentLoginDateTime = this._CurrentLoginDateTime;
            //_Res._StaffPic = this._StaffPic;
            //_Res.StaffImage.Source = StaffImage.Source;
            //_Res.Owner = this.Owner;

            //this.Close();
            //_Res.ShowDialog();
        }

        private void btnUserAccessRights_Click(object sender, RoutedEventArgs e)
        {
            StaffAccess _StaffAccess = new StaffAccess();
            _StaffAccess._CompanyName = this._CompanyName;
            _StaffAccess._UserName = this._UserName;
            _StaffAccess._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _StaffAccess._StaffPic = this._StaffPic;
            _StaffAccess.StaffImage.Source = StaffImage.Source;
            _StaffAccess.Owner = this.Owner;

            this.Close();
            _StaffAccess.ShowDialog();
        }

        private void btnEmailTSetup_Click(object sender, RoutedEventArgs e)
        {
            EmailTemplateSettings _eMailTemplate = new EmailTemplateSettings();
            _eMailTemplate._CompanyName = this._CompanyName;
            _eMailTemplate._UserName = this._UserName;
            _eMailTemplate._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _eMailTemplate._StaffPic = this._StaffPic;
            _eMailTemplate.StaffImage.Source = StaffImage.Source;
            _eMailTemplate.Owner = this.Owner;

            this.Close();
            _eMailTemplate.ShowDialog();
        }

        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        
        private void btnDashboardQuit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
                //Closing event called
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateData())
            {
                tabResource.SelectedIndex = 1;
                TimeSlots.IsEnabled = true;
                gvTimeSlots.IsReadOnly = false;

                _Slots = Convert.ToInt32(txtNumSlots.Text);

                gvTimeSlots.ItemsSource = null;
                gvTimeSlots.Items.Clear();

                ComboBox _cmb = _getTimeSlots();

                sTime.ItemsSource = _cmb.Items;
                eTime.ItemsSource = _cmb.Items;
                _sTimeIndex = 0;
                _eTimeIndex = _cmb.Items.Count - 1;

                if (_isSlotsEmpty)
                {
                    _isSlotsEmpty = false;
                    _dtSlots = new DataTable();
                    _dtSlots.Columns.Add("SlotSequence", typeof(int));
                    _dtSlots.Columns.Add("StartTime", typeof(string));
                    _dtSlots.Columns.Add("EndTime", typeof(string));
                    _dtSlots.Columns.Add("sTimeIndex", typeof(int));
                    _dtSlots.Columns.Add("eTimeIndex", typeof(int));

                    for (int i = 0; i < _Slots; i++)
                    {
                        DataRow _dR = _dtSlots.NewRow();
                        _dR["SlotSequence"] = i + 1;
                        _dR["StartTime"] = _cmb.Items[0].ToString();
                        _dR["EndTime"] = _cmb.Items[_cmb.Items.Count - 1].ToString();
                        _dR["sTimeIndex"] = 0;
                        _dR["eTimeIndex"] = _cmb.Items.Count - 1;
                        _dtSlots.Rows.Add(_dR);
                    }
                }
                else
                {
                    if (_Slots > _dtSlots.DefaultView.Count)
                    {
                        for (int i = _dtSlots.DefaultView.Count; i < _Slots; i++)
                        {
                            DataRow _dR = _dtSlots.NewRow();
                            _dR["SlotSequence"] = i + 1;
                            _dR["StartTime"] = _cmb.Items[0].ToString();
                            _dR["EndTime"] = _cmb.Items[_cmb.Items.Count - 1].ToString();
                            _dR["sTimeIndex"] = 0;
                            _dR["eTimeIndex"] = _cmb.Items.Count - 1;
                            _dtSlots.Rows.Add(_dR);
                        }
                    }
                }
                gvTimeSlots.ItemsSource = _dtSlots.DefaultView;
            }
        }

        private ComboBox _getTimeSlots()
        {
            ComboBox _cmb = new ComboBox();
            TimeSpan tSlotBlock = new TimeSpan(Constants._BookingSlotBlockHr, Constants._BookingSlotBlockMin, Constants._BookingSlotBlockSec);
            TimeSpan tStart = new TimeSpan(Constants._BookingSlotStartHr, Constants._BookingSlotStartMin, Constants._BookingSlotStartSec);
            TimeSpan tEnd = new TimeSpan(Constants._BookingSlotEndHr, Constants._BookingSlotEndMin, Constants._BookingSlotEndSec);
            DateTime tNextSlot = new DateTime();

            double _SlotMinutes = tSlotBlock.TotalMinutes;
            double _EndMinutes = tEnd.TotalMinutes;
            double _StartMinutes = tStart.TotalMinutes;

            tNextSlot = DateTime.Today.Add(tStart);
            int NoOfSlots;
            NoOfSlots = Convert.ToInt32((_EndMinutes - _StartMinutes) / (_SlotMinutes)) + 1;

            for (int i = 0; i < NoOfSlots; i++)
            {
                _cmb.Items.Add( tNextSlot.ToString("hh:mm tt"));
                tNextSlot = tNextSlot.Add(tSlotBlock);
            }
            
            return _cmb;
        }

        private void tabResource_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tabResource.SelectedIndex == 0)
            {
                TimeSlots.IsEnabled = false;
            }
        }

        private void btnContactTypes_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
