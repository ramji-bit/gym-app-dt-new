﻿using System.Windows;
using System.Collections.Generic;
using System.Windows.Controls;
using GymMembership.Accounts;
using GymMembership.BAL;
using GymMembership.DTO;
using GymMembership.Comman;
using System.Linq;
using System;
using System.Data;
using System.Windows.Input;
using System.Windows.Data;
using System.Windows.Controls.Primitives;
using System.ComponentModel;
using GymMembership.BAL;
using System.Windows.Media.Imaging;
using System.IO;
using System.Configuration;

namespace GymMembership.Settings
{
    /// <summary>
    /// Interaction logic for ProgrammeGroup.xaml
    /// </summary>
    public partial class ProgrammeGroup 
    {
        private int _GroupId = 0;
        private string _groupName;
        private string _abbreviation;
        private bool _groupMembershipCardcheck;
        private int _groupMembershipCard;
        private bool _groupStatuscheck;
        private int _groupStatus;
        private int _addSuccess;
        bool _isEdit = false;
        private bool reval;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public Login _Login;
        Utilities _utilites = new Utilities();
        SettingsBAL _settingDetailsBAL = new SettingsBAL();
        private int _programmeGroupId;
        public ProgrammeGroup()
        {
            InitializeComponent();
            populateData();
            Disable();
        }

        private void btnPGCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //this.Title = "CompanyDetails";
            lblOrgName.Content = _CompanyName.ToString().Replace("_", "__");
            lblUserName.Content = _UserName.ToString().Replace("_", "__");
            //lblSiteName.Content = _SiteName.ToString().Replace("_", "__");
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto();
        }

        private void loadStaffPhoto()
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }


        public void Disable()
        {
            txtPGAbbreviation.IsEnabled = false;
            chkMembershipCard.IsEnabled = false;
            chkMembershipStatus.IsEnabled = false;
            btnPGSave.IsEnabled = false;
            txtPGGroupName.Visibility = Visibility.Hidden;
        }

        public void populateData()
        {
            _utilites.populateALLProgramGroup(cmbPGGroupName);
            if (cmbPGGroupName.Items.Count > 0)
            {
                cmbPGGroupName.SelectedIndex = 0;
                btnPGEdit.IsEnabled = true;
            }
            else
            {
                btnPGEdit.IsEnabled = false;
            }

        }

        private void cmbPGGroupName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                _programmeGroupId = Convert.ToInt32(cmbPGGroupName.SelectedValue);
                DataSet ds = _settingDetailsBAL.get_MembershipGroupById(_programmeGroupId);
                if (_programmeGroupId != 0)
                { 
                btnPGEdit.IsEnabled = true;
                txtPGAbbreviation.Text = ds.Tables[0].Rows[0]["Abbreviation"].ToString().Trim();
                if (Convert.ToInt32(ds.Tables[0].Rows[0]["GroupsMembershipCard"]) == 1)
                {
                    chkMembershipCard.IsChecked = true;
                }
                else
                {
                    chkMembershipCard.IsChecked = false;
                }

                if (Convert.ToInt32(ds.Tables[0].Rows[0]["GroupsStatus"]) == 1)
                {

                    chkMembershipStatus.IsChecked = true;
                }
                else
                {
                    chkMembershipStatus.IsChecked = false;
                }
            }

                else
                {
                    _utilites.populateALLProgramGroup(cmbPGGroupName);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Program Group Error",this.Title,MessageBoxButton.OK,MessageBoxImage.Information);
            }
        }

        private void btnPGEdit_Click(object sender, RoutedEventArgs e)
        {
            txtPGGroupName.Visibility = Visibility.Visible;
            cmbPGGroupName.Visibility = Visibility.Visible;
            txtPGGroupName.Text = cmbPGGroupName.Text;
            txtPGAbbreviation.IsEnabled = true;
            chkMembershipCard.IsEnabled = true;
            chkMembershipStatus.IsEnabled = true;
            btnPGSave.IsEnabled = true;
            btnPGEdit.IsEnabled = false;
            _isEdit = true;
        }

        private void btnPGSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string _existingProdName;
                if (validateData())
                {
                    _programmeGroupId = Convert.ToInt32(cmbPGGroupName.SelectedValue);
                    _groupName = txtPGGroupName.Text.ToString().Trim();
                    _abbreviation = txtPGAbbreviation.Text.ToString().Trim();
                    _groupMembershipCardcheck = Convert.ToBoolean(chkMembershipCard.IsChecked.ToString());

                    if (_groupMembershipCardcheck == true)
                    {
                        _groupMembershipCard = 1;
                    }
                    else
                    {
                        _groupMembershipCard = 0;
                    }


                    _groupStatuscheck = Convert.ToBoolean(chkMembershipStatus.IsChecked.ToString());
                    if (_groupStatuscheck == true)
                    {
                        _groupStatus = 1;
                    }
                    else
                    {
                        _groupStatus = 0;
                    }

                    if (_isEdit)
                    {
                        int _programmeGroupIdCheck = Convert.ToInt32(cmbPGGroupName.SelectedValue);
                        int j = cmbPGGroupName.SelectedIndex;
                        for (int i = 0; i < cmbPGGroupName.Items.Count; i++)
                        {
                            //cmbPGGroupName.SelectedIndex = i;
                            //_existingProdName = cmbPGGroupName.Text;
                            _existingProdName = (cmbPGGroupName.Items[i] as DataRowView).Row["GroupsName"].ToString();
                            if ((_groupName.ToUpper() == _existingProdName.ToUpper()) && (i != j)) //&& (_programmeGroupIdCheck != Convert.ToInt32(cmbPGGroupName.SelectedValue.ToString().Trim())))
                            {
                                MessageBox.Show("Programme Group Already Exists!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                Disable();
                                btnPGEdit.IsEnabled = false;
                                _utilites.populateALLProgramGroup(cmbPGGroupName);
                                return;
                            }
                        }

                        _addSuccess = _settingDetailsBAL.add_ProgrammeGroups(_programmeGroupIdCheck, _groupName, _abbreviation, _groupMembershipCard, _groupStatus);

                        if (_addSuccess != 0)
                        {
                            MessageBox.Show("Programme Group Updated Sucessfully", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            cmbPGGroupName.Visibility = Visibility.Visible;
                            txtPGGroupName.Visibility = Visibility.Hidden;
                            populateData();
                            Disable();
                            btnPGSave.IsEnabled = false;
                        }
                        else
                        {
                            MessageBox.Show(": Error : Programme Group NOT Added!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        if (cmbPGGroupName.Items.Count > 0)
                        {
                            for (int i = 0; i < cmbPGGroupName.Items.Count; i++)
                            {
                                //cmbPGGroupName.SelectedIndex = i;
                                //_existingProdName = cmbPGGroupName.Text;
                                _existingProdName = (cmbPGGroupName.Items[i] as DataRowView).Row["GroupsName"].ToString();
                                if (_groupName.ToUpper() == _existingProdName.ToUpper())
                                {
                                    MessageBox.Show("Programme Group Already Exists! Please enter a New Programme Group!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                                    return;
                                }
                            }
                        }
                        _addSuccess = _settingDetailsBAL.addNew_ProgrammeGroups(_groupName, _abbreviation, _groupMembershipCard, _groupStatus);

                        if (_addSuccess != 0)
                        {
                            MessageBox.Show("Programme Group Added Sucessfully", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            cmbPGGroupName.Visibility = Visibility.Visible;
                            txtPGGroupName.Visibility = Visibility.Hidden;
                            populateData();
                            Disable();
                            btnPGSave.IsEnabled = false;
                        }
                        else
                        {
                            MessageBox.Show("New Program not inserted", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
            public void clearProgrammeGroupData()
            {
                txtPGGroupName.Text = string.Empty;
                txtPGAbbreviation.Text = string.Empty;
                chkMembershipCard.IsChecked = false;
                chkMembershipStatus.IsChecked = false;
                cmbPGGroupName.Visibility = Visibility.Hidden;
                txtPGGroupName.Visibility = Visibility.Visible;
            }

            private void btnPGNew_Click(object sender, RoutedEventArgs e)
            {
                btnPGSave.IsEnabled = true;
            btnPGEdit.IsEnabled = false;
                clearProgrammeGroupData();
                btnPGEdit.IsEnabled = false;
                txtPGAbbreviation.IsEnabled = true;
                chkMembershipCard.IsEnabled = true;
                chkMembershipStatus.IsEnabled = true;
            _isEdit = false;
            cmbPGGroupName.Visibility = Visibility.Hidden;
            txtPGGroupName.Visibility = Visibility.Visible;
        }

        private bool validateData()
        {
            reval = true;

            if (string.IsNullOrEmpty(txtPGGroupName.Text.ToString()))
            {
                MessageBox.Show("Please Enter Programme Name.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtPGGroupName.Focus();
                reval = false;
                return reval;
            }
            if (string.IsNullOrEmpty(txtPGAbbreviation.Text.ToString()))
            {
                MessageBox.Show("Please Enter Abbreviation Name.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                txtPGAbbreviation.Focus();
                reval = false;
                return reval;
            }
            return true;
        }

        private void txtPGGroupName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(txtPGGroupName.Text.Length>0)
            { 
                if (!System.Text.RegularExpressions.Regex.IsMatch(txtPGGroupName.Text, "^[a-zA-Z]"))
                {
                    MessageBox.Show("Group Name Doesn't Accepts First Numeric Characters", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtPGGroupName.Text = string.Empty;
                }
            }
        }

        private void txtPGAbbreviation_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtPGAbbreviation.Text.Length > 0)
            {
                if (!System.Text.RegularExpressions.Regex.IsMatch(txtPGAbbreviation.Text, "^[a-zA-Z]"))
                {
                    MessageBox.Show("Abbreviation Doesn't Accepts First Numeric Characters", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtPGAbbreviation.Text = string.Empty;
                }
            }
        }


        private void btnContactTypes1_Click(object sender, RoutedEventArgs e)
        {
            CompanyDetails _CompanySiteDetails = new CompanyDetails();
            _CompanySiteDetails._CompanyName = this._CompanyName;
            _CompanySiteDetails._UserName = this._UserName;
            _CompanySiteDetails._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _CompanySiteDetails._StaffPic = this._StaffPic;
            _CompanySiteDetails.StaffImage.Source = StaffImage.Source;
            _CompanySiteDetails.Owner = this.Owner;

            this.Close();
            _CompanySiteDetails.ShowDialog();
        }

        private void btnUserAdministration_Click(object sender, RoutedEventArgs e)
        {
            if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
            {
                UserAdministration useradmin = new UserAdministration();
                useradmin._CompanyName = this._CompanyName;
                useradmin._UserName = this._UserName;
                useradmin._CurrentLoginDateTime = this._CurrentLoginDateTime;
                useradmin._StaffPic = this._StaffPic;
                useradmin.StaffImage.Source = StaffImage.Source;
                useradmin.Owner = this.Owner;

                this.Close();
                useradmin.ShowDialog();
            }
            else
            {
                MessageBox.Show("You MUST be Admin user to access this Screen!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnProductsTypes_Click(object sender, RoutedEventArgs e)
        {

            ProductType prdType = new ProductType();
            prdType._CompanyName = this._CompanyName;
            prdType._UserName = this._UserName;
            prdType._CurrentLoginDateTime = this._CurrentLoginDateTime;
            prdType._StaffPic = this._StaffPic;
            prdType.StaffImage.Source = StaffImage.Source;
            prdType.Owner = this.Owner;

            this.Close();
            prdType.ShowDialog();
        }

        private void btnManageProducts_Click(object sender, RoutedEventArgs e)
        {
            Products products = new Products();
            products._CompanyName = this._CompanyName;
            products._UserName = this._UserName;
            products._CurrentLoginDateTime = this._CurrentLoginDateTime;
            products._StaffPic = this._StaffPic;
            products.StaffImage.Source = StaffImage.Source;
            products.Owner = this.Owner;

            this.Close();
            products.ShowDialog();
        }

        private void btnProgrammeGSetup_Click(object sender, RoutedEventArgs e)
        {
            //SAME WINDOW; NO ACTION

            //ProgrammeGroup PrgGroup = new ProgrammeGroup();
            //PrgGroup._CompanyName = this._CompanyName;
            //PrgGroup._UserName = this._UserName;
            //PrgGroup._CurrentLoginDateTime = this._CurrentLoginDateTime;
            //PrgGroup._StaffPic = this._StaffPic;
            //PrgGroup.StaffImage.Source = StaffImage.Source;
            //PrgGroup.Owner = this.Owner;

            //this.Close();
            //PrgGroup.ShowDialog();
        }

        private void btnAddEditProgrammes_Click(object sender, RoutedEventArgs e)
        {
            Programme Prg = new Programme();
            Prg._CompanyName = this._CompanyName;
            Prg._UserName = this._UserName;
            Prg._CurrentLoginDateTime = this._CurrentLoginDateTime;
            Prg._StaffPic = this._StaffPic;
            Prg.StaffImage.Source = StaffImage.Source;
            Prg.Owner = this.Owner;

            this.Close();
            Prg.ShowDialog();
        }

        private void btnSupplierSetup_Click(object sender, RoutedEventArgs e)
        {
            Supplier _Supplier = new Supplier();
            _Supplier._CompanyName = this._CompanyName;
            _Supplier._UserName = this._UserName;
            _Supplier._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Supplier._StaffPic = this._StaffPic;
            _Supplier.StaffImage.Source = StaffImage.Source;
            _Supplier.Owner = this.Owner;

            this.Close();
            _Supplier.ShowDialog();
        }

        private void btnBookingResources_Click(object sender, RoutedEventArgs e)
        {
            Settings.Resources _Res = new Settings.Resources();
            _Res._CompanyName = this._CompanyName;
            _Res._UserName = this._UserName;
            _Res._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Res._StaffPic = this._StaffPic;
            _Res.StaffImage.Source = StaffImage.Source;
            _Res.Owner = this.Owner;

            this.Close();
            _Res.ShowDialog();
        }

        private void btnUserAccessRights_Click(object sender, RoutedEventArgs e)
        {
            StaffAccess _StaffAccess = new StaffAccess();
            _StaffAccess._CompanyName = this._CompanyName;
            _StaffAccess._UserName = this._UserName;
            _StaffAccess._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _StaffAccess._StaffPic = this._StaffPic;
            _StaffAccess.StaffImage.Source = StaffImage.Source;
            _StaffAccess.Owner = this.Owner;

            this.Close();
            _StaffAccess.ShowDialog();
        }

        private void btnEmailTSetup_Click(object sender, RoutedEventArgs e)
        {
            EmailTemplateSettings _eMailTemplate = new EmailTemplateSettings();
            _eMailTemplate._CompanyName = this._CompanyName;
            _eMailTemplate._UserName = this._UserName;
            _eMailTemplate._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _eMailTemplate._StaffPic = this._StaffPic;
            _eMailTemplate.StaffImage.Source = StaffImage.Source;
            _eMailTemplate.Owner = this.Owner;

            this.Close();
            _eMailTemplate.ShowDialog();
        }

        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDashboardQuit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
                //Closing event called
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnContactTypes_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
