﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration;
using System.Data;
using System.IO;
using GymMembership.Comman;
using GymMembership.Accounts;

namespace GymMembership.Settings
{
    /// <summary>
    /// Interaction logic for StaffRoster.xaml
    /// </summary>
    public partial class StaffRoster
    {
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        //public Login _Login;

        int _currentSelButton = 0;
        int _currentSelIndex = -1;
        int _currStaffCount = 0;
        int _currentPaging = 0;
        bool _moreThanEightStaff = false;
        bool _validSelection = false;

        DataTable _dtStaffGlobal;
        DataTable _dtRosterGlobal;
        string _currSelectedStaffEmailID = "";
        string _currSelectedStaffName = "";
        string _AdminUserName = "";

        double _sliderLValue;
        double _sliderUValue;

        Brush _validForeColor = Brushes.MediumAquamarine;
        Brush _invalidForeColor = Brushes.LightGray;
        Brush _validSelectedForeColor = Brushes.PaleGoldenrod;
        Brush _scrollButtonFColorActive = Brushes.MediumAquamarine;
        Brush _scrollButtonFColorInActive = Brushes.LightGray;
        Brush _scrollArrowColorActive = Brushes.Yellow;
        Brush _scrollArrowColorInActive = Brushes.DarkGray;

        bool _windowLoadComplete = false;
        int _countOfDateChecked = 0;
        public StaffRoster()
        {
            InitializeComponent();
        }
        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                lblOrgName.Content = _CompanyName.ToString().Replace("_", "__");
                lblUserName.Content = _UserName.ToString().Replace("_", "__");
                //lblSiteName.Content = _SiteName.ToString().Replace("_", "__");
                lblLoggedInTime.Content = _CurrentLoginDateTime;

                loadData();
                _windowLoadComplete = true;
            }
            catch (Exception x)
            {
                string _msg = x.Message;
                _windowLoadComplete = false;
            }
        }
        private void DisableAllButtons()
        {
            try
            {
                btnStaff1.Background = _invalidForeColor;
                btnStaff2.Background = _invalidForeColor;
                btnStaff3.Background = _invalidForeColor;
                btnStaff4.Background = _invalidForeColor;
                btnStaff5.Background = _invalidForeColor;
                btnStaff6.Background = _invalidForeColor;
                btnStaff7.Background = _invalidForeColor;
                btnStaff8.Background = _invalidForeColor;

                btnStaff1.IsEnabled = false;
                btnStaff2.IsEnabled = false;
                btnStaff3.IsEnabled = false;
                btnStaff4.IsEnabled = false;
                btnStaff5.IsEnabled = false;
                btnStaff6.IsEnabled = false;
                btnStaff7.IsEnabled = false;
                btnStaff8.IsEnabled = false;

                tblockContent1.Text = "";
                tblockContent2.Text = "";
                tblockContent3.Text = "";
                tblockContent4.Text = "";
                tblockContent5.Text = "";
                tblockContent6.Text = "";
                tblockContent7.Text = "";
                tblockContent8.Text = "";
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }
        private void LoadDates()
        {
            try
            {
                lblToday1.Content = DateTime.Today.AddDays(1).ToString("r").ToString().Substring(0, 11);
                lblToday2.Content = DateTime.Today.AddDays(2).ToString("r").ToString().Substring(0, 11);
                lblToday3.Content = DateTime.Today.AddDays(3).ToString("r").ToString().Substring(0, 11);
                lblToday4.Content = DateTime.Today.AddDays(4).ToString("r").ToString().Substring(0, 11);
                lblToday5.Content = DateTime.Today.AddDays(5).ToString("r").ToString().Substring(0, 11);
                lblToday6.Content = DateTime.Today.AddDays(6).ToString("r").ToString().Substring(0, 11);
                //lblToday6.Content = DateTime.Today.AddDays(6).ToString("dd / MMM");
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }
        private void loadData()
        {
            try
            {
                _currentSelButton = 0;
                _currentSelIndex = -1;
                _currStaffCount = 0;
                _currentPaging = 0;
                _moreThanEightStaff = false; //TOTAL DISPLAYABLE BUTTONS: 8
                _validSelection = false;
                _countOfDateChecked = 0;

                DisableAllButtons();
                LoadDates();

                SqlHelper _sql = new SqlHelper();
                if (_sql._ConnOpen == 0)
                    return;

                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                Hashtable ht = new Hashtable();
                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);

                DataSet _dsStaff = _sql.ExecuteProcedure("StaffRoster_GetAllStaffNames", ht);
                if (_dsStaff != null)
                {
                    if (_dsStaff.Tables.Count > 0)
                    {
                        if (_dsStaff.Tables.Count > 1)
                        {
                            _dtRosterGlobal = _dsStaff.Tables[1];
                        }

                        if (_dsStaff.Tables[0].DefaultView.Count > 0)
                        {
                            _dtStaffGlobal = _dsStaff.Tables[0];
                            _currStaffCount = _dtStaffGlobal.DefaultView.Count;

                            if (_currStaffCount > 8)
                            {
                                _moreThanEightStaff = true;
                                DispalyStaffNames(_dtStaffGlobal, 0, 8);
                            }
                            else
                            {
                                _moreThanEightStaff = false;
                                DispalyStaffNames(_dtStaffGlobal, 0, _currStaffCount);
                            }
                        }
                    }
                }
                if (!_moreThanEightStaff)
                {
                    btnLeftScroll.Background = _scrollButtonFColorInActive;
                    btnLeftScroll.IsEnabled = false;
                    rectLeftArrow.Fill = _scrollArrowColorInActive;

                    btnRightScroll.Background = _scrollButtonFColorInActive;
                    btnRightScroll.IsEnabled = false;
                    rectRightArrow.Fill = _scrollArrowColorInActive;
                }
                else
                {
                    btnLeftScroll.Background = _scrollButtonFColorActive;
                    btnLeftScroll.IsEnabled = true;
                    rectLeftArrow.Fill = _scrollArrowColorActive;

                    btnRightScroll.Background = _scrollButtonFColorActive;
                    btnRightScroll.IsEnabled = true;
                    rectRightArrow.Fill = _scrollArrowColorActive;
                }

                if (_currentPaging <= 0)
                {
                    btnLeftScroll.Background = _scrollButtonFColorInActive;
                    btnLeftScroll.IsEnabled = false;
                    rectLeftArrow.Fill = _scrollArrowColorInActive;
                }
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }

        private void DispalyStaffNames(DataTable _dtStaff, int _IndexStart, int _IndexCount)
        {
            try
            {
                string _FirstName = "";
                string _LastName = "";
                string _UserName = "";
                string _SiteName = "";
                string _emailID = "";

                int _modValue = _IndexStart;

                DisableAllButtons();

                if (_IndexCount > _dtStaff.DefaultView.Count)
                    _IndexCount = _dtStaff.DefaultView.Count;

                for (int i = _IndexStart; i < _IndexCount; i++)
                {
                    _modValue = i - (_currentPaging * 8);

                    _FirstName = _dtStaff.DefaultView[i]["FirstName"].ToString().ToUpper();
                    _LastName = _dtStaff.DefaultView[i]["LastName"].ToString().ToUpper();
                    _UserName = _dtStaff.DefaultView[i]["UserName"].ToString();
                    _SiteName = _dtStaff.DefaultView[i]["SiteName"].ToString();
                    _emailID = _dtStaff.DefaultView[i]["EmailID"].ToString();

                    if (_UserName.Trim() != "")
                    {
                        _UserName = "(" + _UserName + ")";
                    }
                    if (_SiteName.Trim() != "")
                    {
                        _SiteName = " - " + _SiteName.ToUpper();
                    }
                    if (_emailID.Trim() != "")
                    {
                        _emailID = "\n(" + _emailID + ")";
                    }

                    Button _btnStaff = new Button();
                    TextBlock _tblockStaff = new TextBlock();

                    bool _Valid = false;

                    switch (_modValue)
                    {
                        case 0:
                            _tblockStaff = tblockContent1;
                            _btnStaff = btnStaff1;
                            _Valid = true;
                            break;
                        case 1:
                            _tblockStaff = tblockContent2;
                            _btnStaff = btnStaff2;
                            _Valid = true;
                            break;
                        case 2:
                            _tblockStaff = tblockContent3;
                            _btnStaff = btnStaff3;
                            _Valid = true;
                            break;
                        case 3:
                            _tblockStaff = tblockContent4;
                            _btnStaff = btnStaff4;
                            _Valid = true;
                            break;
                        case 4:
                            _tblockStaff = tblockContent5;
                            _btnStaff = btnStaff5;
                            _Valid = true;
                            break;
                        case 5:
                            _tblockStaff = tblockContent6;
                            _btnStaff = btnStaff6;
                            _Valid = true;
                            break;
                        case 6:
                            _tblockStaff = tblockContent7;
                            _btnStaff = btnStaff7;
                            _Valid = true;
                            break;
                        case 7:
                            _tblockStaff = tblockContent8;
                            _btnStaff = btnStaff8;
                            _Valid = true;
                            break;
                        default:
                            _Valid = false;
                            break;
                    }
                    if (_Valid)
                    {
                        _tblockStaff.TextAlignment = TextAlignment.Center;
                        _tblockStaff.TextWrapping = TextWrapping.Wrap;
                        _tblockStaff.Text = _FirstName + " " + _LastName + " " + _UserName + _SiteName + _emailID;
                        _btnStaff.IsEnabled = true;
                        _btnStaff.Background = _validForeColor;
                    }
                }
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }
        private void loadStaffPhoto()
        {
            try
            {
                //StaffPIC
                if (_StaffPic != null)
                {
                    if (_StaffPic.Length >= 4)
                    {
                        MemoryStream strm = new MemoryStream();
                        strm.Write(_StaffPic, 0, _StaffPic.Length);
                        strm.Position = 0;

                        System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                        BitmapImage bi = new BitmapImage();
                        bi.BeginInit();
                        MemoryStream ms = new MemoryStream();
                        img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                        ms.Seek(0, SeekOrigin.Begin);
                        bi.StreamSource = ms;
                        bi.EndInit();

                        StaffImage.Source = bi;
                    }
                }
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }

        private void SetBackGroundColorForSelectedButton(int _currButton)
        {
            try
            {
                if (btnStaff1.IsEnabled)
                    btnStaff1.Background = _validForeColor;
                else
                    btnStaff1.Background = _invalidForeColor;

                if (btnStaff2.IsEnabled)
                    btnStaff2.Background = _validForeColor;
                else
                    btnStaff2.Background = _invalidForeColor;

                if (btnStaff3.IsEnabled)
                    btnStaff3.Background = _validForeColor;
                else
                    btnStaff3.Background = _invalidForeColor;

                if (btnStaff4.IsEnabled)
                    btnStaff4.Background = _validForeColor;
                else
                    btnStaff4.Background = _invalidForeColor;

                if (btnStaff5.IsEnabled)
                    btnStaff5.Background = _validForeColor;
                else
                    btnStaff5.Background = _invalidForeColor;

                if (btnStaff6.IsEnabled)
                    btnStaff6.Background = _validForeColor;
                else
                    btnStaff6.Background = _invalidForeColor;

                if (btnStaff7.IsEnabled)
                    btnStaff7.Background = _validForeColor;
                else
                    btnStaff7.Background = _invalidForeColor;

                if (btnStaff8.IsEnabled)
                    btnStaff8.Background = _validForeColor;
                else
                    btnStaff8.Background = _invalidForeColor;

                switch (_currButton)
                {
                    case 0:
                        break;
                    case 1:
                        btnStaff1.Background = _validSelectedForeColor;
                        break;
                    case 2:
                        btnStaff2.Background = _validSelectedForeColor;
                        break;
                    case 3:
                        btnStaff3.Background = _validSelectedForeColor;
                        break;
                    case 4:
                        btnStaff4.Background = _validSelectedForeColor;
                        break;
                    case 5:
                        btnStaff5.Background = _validSelectedForeColor;
                        break;
                    case 6:
                        btnStaff6.Background = _validSelectedForeColor;
                        break;
                    case 7:
                        btnStaff7.Background = _validSelectedForeColor;
                        break;
                    case 8:
                        btnStaff8.Background = _validSelectedForeColor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }
        private void btnStaff_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResetBlockSliders();

                string _currentButton = (sender as Button).Name.ToString().ToUpper();
                switch (_currentButton)
                {
                    case "BTNSTAFF1":
                        _currentSelButton = 1;
                        break;
                    case "BTNSTAFF2":
                        _currentSelButton = 2;
                        break;
                    case "BTNSTAFF3":
                        _currentSelButton = 3;
                        break;
                    case "BTNSTAFF4":
                        _currentSelButton = 4;
                        break;
                    case "BTNSTAFF5":
                        _currentSelButton = 5;
                        break;
                    case "BTNSTAFF6":
                        _currentSelButton = 6;
                        break;
                    case "BTNSTAFF7":
                        _currentSelButton = 7;
                        break;
                    case "BTNSTAFF8":
                        _currentSelButton = 8;
                        break;
                    default:
                        _currentSelButton = 0;
                        break;
                }
                _currentSelIndex = ((_currentPaging * 8) + _currentSelButton) - 1;

                if (_currentSelIndex < 0)
                {
                    _validSelection = false;
                    borderDetails.IsEnabled = false;
                    btnSaveEmail.IsEnabled = false;
                }
                else
                {
                    _validSelection = true;
                    borderDetails.IsEnabled = true;
                }

                if (_validSelection)
                {
                    SetBackGroundColorForSelectedButton(_currentSelButton);

                    //IF ROSTER EXISTS FOR SELECTED STAFF; DISPLAY SHIFT TIMES
                    DisplayStaffRosterDetails(_currentSelIndex);
                }
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }
        private void DisplayStaffRosterDetails(int _curSelIndex)
        {
            try
            {
                int _StaffID = Convert.ToInt32(_dtStaffGlobal.DefaultView[_curSelIndex]["StaffID"].ToString());
                int _StaffLoginID = Convert.ToInt32(_dtStaffGlobal.DefaultView[_curSelIndex]["LoginID"].ToString());
                _currSelectedStaffEmailID = _dtStaffGlobal.DefaultView[_curSelIndex]["EmailID"].ToString();
                _currSelectedStaffName =
                                _dtStaffGlobal.DefaultView[_curSelIndex]["FirstName"].ToString().ToUpper()
                        + " " + _dtStaffGlobal.DefaultView[_curSelIndex]["LastName"].ToString().ToUpper()
                        + " (" + _dtStaffGlobal.DefaultView[_curSelIndex]["UserName"].ToString() + ")"; 

                if (_dtRosterGlobal != null)
                {
                    DataTable _dtRosterLocal = _dtRosterGlobal;
                    _dtRosterLocal.DefaultView.RowFilter = "StaffID = " + _StaffID + " AND StaffLoginID = " + _StaffLoginID;
                    if (_dtRosterLocal.DefaultView.Count > 0)
                    {
                        for (int i = 0; i < _dtRosterLocal.DefaultView.Count; i++)
                        {
                            DateTime _RosterDate = Convert.ToDateTime(_dtRosterLocal.DefaultView[i]["RosterDate"]);
                            int _StartHR = Convert.ToInt32(_dtRosterLocal.DefaultView[i]["ShiftStartTime_HOUR"]);
                            int _StartMIN = Convert.ToInt32(_dtRosterLocal.DefaultView[i]["ShiftStartTime_MIN"]);
                            int _EndHR = Convert.ToInt32(_dtRosterLocal.DefaultView[i]["ShiftEndTime_HOUR"]);
                            int _EndMIN = Convert.ToInt32(_dtRosterLocal.DefaultView[i]["ShiftEndTime_MIN"]);
                            string _StartAMPM = " AM";
                            string _EndAMPM = " AM";

                            int _sliderLowValue = (_StartHR * 60) + _StartMIN;
                            int _sliderHighValue = (_EndHR * 60) + _EndMIN;

                            string _StartTimeDisplay;
                            if (_StartHR > 12)
                            {
                                _StartTimeDisplay = Convert.ToInt32((_StartHR - 12)).ToString("#0");
                                _StartAMPM = " PM";
                            }
                            else
                            {
                                _StartTimeDisplay = _StartHR.ToString("#0");
                            }
                            _StartTimeDisplay = _StartTimeDisplay + ":" + _StartMIN.ToString("00") + _StartAMPM;

                            string _EndTimeDisplay;
                            if (_EndHR > 12)
                            {
                                _EndTimeDisplay= Convert.ToInt32((_EndHR - 12)).ToString("#0");
                                _EndAMPM = " PM";
                            }
                            else
                            {
                                _EndTimeDisplay = _EndHR.ToString("#0");
                            }
                            _EndTimeDisplay = _EndTimeDisplay + ":" + _EndMIN.ToString("00") + _EndAMPM;

                            if (_RosterDate.Date == DateTime.Now.Date) //IF CURRENT DATE SHIFT DETAILS EXIST
                            {
                                sliderToday0.LowerValue = _sliderLowValue;
                                sliderToday0.UpperValue = _sliderHighValue;
                                lblEtTime0.Content = _EndTimeDisplay;
                                lblStTime0.Content = _StartTimeDisplay;

                                chkSel0.IsChecked = true;
                            }
                            else if (_RosterDate.Date == DateTime.Now.AddDays(1).Date) //IF CURRENT DATE + 1 SHIFT DETAILS EXIST
                            {
                                sliderToday1.LowerValue = _sliderLowValue;
                                sliderToday1.UpperValue = _sliderHighValue;
                                lblEtTime1.Content = _EndTimeDisplay;
                                lblStTime1.Content = _StartTimeDisplay;

                                chkSel1.IsChecked = true;
                            }
                            else if (_RosterDate.Date == DateTime.Now.AddDays(2).Date) //IF CURRENT DATE + 2 SHIFT DETAILS EXIST
                            {
                                sliderToday2.LowerValue = _sliderLowValue;
                                sliderToday2.UpperValue = _sliderHighValue;
                                lblEtTime2.Content = _EndTimeDisplay;
                                lblStTime2.Content = _StartTimeDisplay;

                                chkSel2.IsChecked = true;
                            }
                            else if (_RosterDate.Date == DateTime.Now.AddDays(3).Date) //IF CURRENT DATE + 3 SHIFT DETAILS EXIST
                            {
                                sliderToday3.LowerValue = _sliderLowValue;
                                sliderToday3.UpperValue = _sliderHighValue;
                                lblEtTime3.Content = _EndTimeDisplay;
                                lblStTime3.Content = _StartTimeDisplay;

                                chkSel3.IsChecked = true;
                            }
                            else if (_RosterDate.Date == DateTime.Now.AddDays(4).Date) //IF CURRENT DATE + 4 SHIFT DETAILS EXIST
                            {
                                sliderToday4.LowerValue = _sliderLowValue;
                                sliderToday4.UpperValue = _sliderHighValue;
                                lblEtTime4.Content = _EndTimeDisplay;
                                lblStTime4.Content = _StartTimeDisplay;

                                chkSel4.IsChecked = true;
                            }
                            else if (_RosterDate.Date == DateTime.Now.AddDays(5).Date) //IF CURRENT DATE + 5 SHIFT DETAILS EXIST
                            {
                                sliderToday5.LowerValue = _sliderLowValue;
                                sliderToday5.UpperValue = _sliderHighValue;
                                lblEtTime5.Content = _EndTimeDisplay;
                                lblStTime5.Content = _StartTimeDisplay;

                                chkSel5.IsChecked = true;
                            }
                            else if (_RosterDate.Date == DateTime.Now.AddDays(6).Date) //IF CURRENT DATE + 6 SHIFT DETAILS EXIST
                            {
                                sliderToday6.LowerValue = _sliderLowValue;
                                sliderToday6.UpperValue = _sliderHighValue;
                                lblEtTime6.Content = _EndTimeDisplay;
                                lblStTime6.Content = _StartTimeDisplay;

                                chkSel6.IsChecked = true;
                            }
                        }
                    }
                    _dtRosterLocal.DefaultView.RowFilter = "";
                }
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }
        private void ResetBlockSliders()
        {
            chkSel0.IsChecked = false;
            chkSel1.IsChecked = false;
            chkSel2.IsChecked = false;
            chkSel3.IsChecked = false;
            chkSel4.IsChecked = false;
            chkSel5.IsChecked = false;
            chkSel6.IsChecked = false;

            sliderToday0.LowerValue = 360;
            sliderToday0.UpperValue = 840;

            sliderToday1.LowerValue = 360;
            sliderToday1.UpperValue = 840;

            sliderToday2.LowerValue = 360;
            sliderToday2.UpperValue = 840;

            sliderToday3.LowerValue = 360;
            sliderToday3.UpperValue = 840;

            sliderToday4.LowerValue = 360;
            sliderToday4.UpperValue = 840;

            sliderToday5.LowerValue = 360;
            sliderToday5.UpperValue = 840;

            sliderToday6.LowerValue = 360;
            sliderToday6.UpperValue = 840;

            borderDetails.IsEnabled = false;

            txtNotes.Text = "";
        }
        private void btnLeftScroll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _currentPaging = _currentPaging - 1;
                _currentSelIndex = -1;
                ResetBlockSliders();

                if (_currentPaging <= 0)
                {
                    btnLeftScroll.Background = _scrollButtonFColorInActive;
                    btnLeftScroll.IsEnabled = false;
                    rectLeftArrow.Fill = _scrollArrowColorInActive;
                }
                else
                {
                    btnLeftScroll.Background = _scrollButtonFColorActive;
                    btnLeftScroll.IsEnabled = true;
                    rectLeftArrow.Fill = _scrollArrowColorActive;
                }

                if (_currStaffCount <= ((_currentPaging * 8) + 8))
                {
                    btnRightScroll.Background = _scrollButtonFColorInActive;
                    btnRightScroll.IsEnabled = false;
                    rectRightArrow.Fill = _scrollArrowColorInActive;
                }
                else
                {
                    btnRightScroll.Background = _scrollButtonFColorActive;
                    btnRightScroll.IsEnabled = true;
                    rectRightArrow.Fill = _scrollArrowColorActive;
                }
                DispalyStaffNames(_dtStaffGlobal, _currentPaging * 8, _currentPaging * 8 + 8);
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }

        private void btnRightScroll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _currentPaging = _currentPaging + 1;
                _currentSelIndex = -1;
                ResetBlockSliders();

                if (_currStaffCount <= ((_currentPaging * 8) + 8))
                {
                    btnRightScroll.Background = _scrollButtonFColorInActive;
                    btnRightScroll.IsEnabled = false;
                    rectRightArrow.Fill = _scrollArrowColorInActive;
                }
                else
                {
                    btnRightScroll.Background = _scrollButtonFColorActive;
                    btnRightScroll.IsEnabled = true;
                    rectRightArrow.Fill = _scrollArrowColorActive;
                }

                if (_currentPaging <= 0)
                {
                    btnLeftScroll.Background = _scrollButtonFColorInActive;
                    btnLeftScroll.IsEnabled = false;
                    rectLeftArrow.Fill = _scrollArrowColorInActive;
                }
                else
                {
                    btnLeftScroll.Background = _scrollButtonFColorActive;
                    btnLeftScroll.IsEnabled = true;
                    rectLeftArrow.Fill = _scrollArrowColorActive;
                }

                DispalyStaffNames(_dtStaffGlobal, _currentPaging * 8, _currentPaging * 8 + 8);
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }
        private void labelFGtoNormal()
        {
            try
            {
                lblTime5AM.Foreground = Brushes.Black;
                lblTime6AM.Foreground = Brushes.Black;
                lblTime7AM.Foreground = Brushes.Black;
                lblTime8AM.Foreground = Brushes.Black;
                lblTime9AM.Foreground = Brushes.Black;
                lblTime10AM.Foreground = Brushes.Black;
                lblTime11AM.Foreground = Brushes.Black;
                lblTime12PM.Foreground = Brushes.Black;
                lblTime1PM.Foreground = Brushes.Black;
                lblTime2PM.Foreground = Brushes.Black;
                lblTime3PM.Foreground = Brushes.Black;
                lblTime4PM.Foreground = Brushes.Black;
                lblTime5PM.Foreground = Brushes.Black;
                lblTime6PM.Foreground = Brushes.Black;
                lblTime7PM.Foreground = Brushes.Black;
                lblTime8PM.Foreground = Brushes.Black;
                lblTime9PM.Foreground = Brushes.Black;
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }
        private void sliderToday_RangeSelectionChanged(object sender, MahApps.Metro.Controls.RangeSelectionChangedEventArgs e)
        {
            try
            {
                if (_windowLoadComplete)
                {
                    MahApps.Metro.Controls.RangeSlider sliderToday = (sender as MahApps.Metro.Controls.RangeSlider);
                    Label _currStLabel = new Label();
                    Label _currEndLabel = new Label();

                    _sliderLValue = sliderToday.LowerValue;
                    _sliderUValue = sliderToday.UpperValue;

                    //TIME DISPLAY
                    int _hr = (int)(_sliderLValue / 60);
                    int _min = (int)(_sliderLValue % 60);

                    string _AMPM = " AM";
                    if (_hr >= 12) _AMPM = " PM";
                    if (_hr > 12) _hr = _hr - 12;

                    string _StTimeToDisplay = _hr.ToString("#0") + ":" + _min.ToString("00") + _AMPM;

                    _hr = (int)(_sliderUValue / 60);
                    _min = (int)(_sliderUValue % 60);

                    _AMPM = " AM";
                    if (_hr >= 12) _AMPM = " PM";
                    if (_hr > 12) _hr = _hr - 12;

                    string _EndTimeToDisplay = _hr.ToString("#0") + ":" + _min.ToString("00") + _AMPM;

                    string _sliderRow = sliderToday.Name.ToUpper();
                    switch (_sliderRow)
                    {
                        case "SLIDERTODAY0":
                            _currStLabel = lblStTime0;
                            _currEndLabel = lblEtTime0;
                            break;
                        case "SLIDERTODAY1":
                            _currStLabel = lblStTime1;
                            _currEndLabel = lblEtTime1;
                            break;
                        case "SLIDERTODAY2":
                            _currStLabel = lblStTime2;
                            _currEndLabel = lblEtTime2;
                            break;
                        case "SLIDERTODAY3":
                            _currStLabel = lblStTime3;
                            _currEndLabel = lblEtTime3;
                            break;
                        case "SLIDERTODAY4":
                            _currStLabel = lblStTime4;
                            _currEndLabel = lblEtTime4;
                            break;
                        case "SLIDERTODAY5":
                            _currStLabel = lblStTime5;
                            _currEndLabel = lblEtTime5;
                            break;
                        case "SLIDERTODAY6":
                            _currStLabel = lblStTime6;
                            _currEndLabel = lblEtTime6;
                            break;
                        default:
                            break;
                    }
                    _currStLabel.Content = _StTimeToDisplay;
                    _currEndLabel.Content = _EndTimeToDisplay;
                }
                #region "OLD CODE"
                //labelFGtoNormal();
                ////LOWER VALUE
                //switch (_hr)
                //{
                //    case 5:
                //        lblTime5AM.Foreground = Brushes.Red;
                //        break;
                //    case 6:
                //        lblTime6AM.Foreground = Brushes.Red;
                //        break;
                //    case 7:
                //        lblTime7AM.Foreground = Brushes.Red;
                //        break;
                //    case 8:
                //        lblTime8AM.Foreground = Brushes.Red;
                //        break;
                //    case 9:
                //        lblTime9AM.Foreground = Brushes.Red;
                //        break;
                //    case 10:
                //        lblTime10AM.Foreground = Brushes.Red;
                //        break;
                //    case 11:
                //        lblTime11AM.Foreground = Brushes.Red;
                //        break;
                //    case 12:
                //        lblTime12PM.Foreground = Brushes.Red;
                //        break;
                //    case 13:
                //        lblTime1PM.Foreground = Brushes.Red;
                //        break;
                //    case 14:
                //        lblTime2PM.Foreground = Brushes.Red;
                //        break;
                //    case 15:
                //        lblTime3PM.Foreground = Brushes.Red;
                //        break;
                //    case 16:
                //        lblTime4PM.Foreground = Brushes.Red;
                //        break;
                //    case 17:
                //        lblTime5PM.Foreground = Brushes.Red;
                //        break;
                //    case 18:
                //        lblTime6PM.Foreground = Brushes.Red;
                //        break;
                //    case 19:
                //        lblTime7PM.Foreground = Brushes.Red;
                //        break;
                //    case 20:
                //        lblTime8PM.Foreground = Brushes.Red;
                //        break;
                //    case 21:
                //        lblTime9PM.Foreground = Brushes.Red;
                //        break;
                //    default:
                //        break;
                //}
                ////UPPER VALUE
                //_hr = (int)(_sliderUValue / 60);
                //switch (_hr)
                //{
                //    case 5:
                //        lblTime5AM.Foreground = Brushes.Red;
                //        break;
                //    case 6:
                //        lblTime6AM.Foreground = Brushes.Red;
                //        break;
                //    case 7:
                //        lblTime7AM.Foreground = Brushes.Red;
                //        break;
                //    case 8:
                //        lblTime8AM.Foreground = Brushes.Red;
                //        break;
                //    case 9:
                //        lblTime9AM.Foreground = Brushes.Red;
                //        break;
                //    case 10:
                //        lblTime10AM.Foreground = Brushes.Red;
                //        break;
                //    case 11:
                //        lblTime11AM.Foreground = Brushes.Red;
                //        break;
                //    case 12:
                //        lblTime12PM.Foreground = Brushes.Red;
                //        break;
                //    case 13:
                //        lblTime1PM.Foreground = Brushes.Red;
                //        break;
                //    case 14:
                //        lblTime2PM.Foreground = Brushes.Red;
                //        break;
                //    case 15:
                //        lblTime3PM.Foreground = Brushes.Red;
                //        break;
                //    case 16:
                //        lblTime4PM.Foreground = Brushes.Red;
                //        break;
                //    case 17:
                //        lblTime5PM.Foreground = Brushes.Red;
                //        break;
                //    case 18:
                //        lblTime6PM.Foreground = Brushes.Red;
                //        break;
                //    case 19:
                //        lblTime7PM.Foreground = Brushes.Red;
                //        break;
                //    case 20:
                //        lblTime8PM.Foreground = Brushes.Red;
                //        break;
                //    case 21:
                //        lblTime9PM.Foreground = Brushes.Red;
                //        break;
                //    default:
                //        break;
                //}
                #endregion "OLD CODE END
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void chkSel_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                _countOfDateChecked--;
                if (_countOfDateChecked <= 0)
                {
                    _countOfDateChecked = 0;
                    btnSaveEmail.IsEnabled = false;
                }

                CheckBox _currChkBox = (sender as CheckBox);
                string _chkBoxSelected = _currChkBox.Name.ToUpper();

                switch (_chkBoxSelected)
                {
                    case "CHKSEL6":
                        if (sliderToday6 != null)
                        {
                            sliderToday6.IsEnabled = false;
                        }
                        break;
                    case "CHKSEL5":
                        if (sliderToday5 != null)
                        {
                            sliderToday5.IsEnabled = false;
                        }
                        break;
                    case "CHKSEL4":
                        if (sliderToday4 != null)
                        {
                            sliderToday4.IsEnabled = false;
                        }
                        break;
                    case "CHKSEL3":
                        if (sliderToday3 != null)
                        {
                            sliderToday3.IsEnabled = false;
                        }
                        break;
                    case "CHKSEL2":
                        if (sliderToday2 != null)
                        {
                            sliderToday2.IsEnabled = false;
                        }
                        break;
                    case "CHKSEL1":
                        if (sliderToday1 != null)
                        {
                            sliderToday1.IsEnabled = false;
                        }
                        break;
                    case "CHKSEL0":
                        if (sliderToday0 != null)
                        {
                            sliderToday0.IsEnabled = false;
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }

        private void chkSel_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                _countOfDateChecked++;
                btnSaveEmail.IsEnabled = true;

                CheckBox _currChkBox = (sender as CheckBox);
                string _chkBoxSelected = _currChkBox.Name.ToUpper();

                switch (_chkBoxSelected)
                {
                    case "CHKSEL6":
                        if (sliderToday6 != null)
                        {
                            sliderToday6.IsEnabled = true;
                        }
                        break;
                    case "CHKSEL5":
                        if (sliderToday5 != null)
                        {
                            sliderToday5.IsEnabled = true;
                        }
                        break;
                    case "CHKSEL4":
                        if (sliderToday4 != null)
                        {
                            sliderToday4.IsEnabled = true;
                        }
                        break;
                    case "CHKSEL3":
                        if (sliderToday3 != null)
                        {
                            sliderToday3.IsEnabled = true;
                        }
                        break;
                    case "CHKSEL2":
                        if (sliderToday2 != null)
                        {
                            sliderToday2.IsEnabled = true;
                        }
                        break;
                    case "CHKSEL1":
                        if (sliderToday1 != null)
                        {
                            sliderToday1.IsEnabled = true;
                        }
                        break;
                    case "CHKSEL0":
                        if (sliderToday0 != null)
                        {
                            sliderToday0.IsEnabled = true;
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception x)
            {
                string _msg = x.Message;
            }
        }

        private void btnSaveEmail_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Mouse.OverrideCursor = Cursors.Wait;
                DataTable _rosterSaveTableType = CreateSaveRosterDataTable();

                SqlHelper _Sql = new SqlHelper();
                if (_Sql._ConnOpen == 0)
                    return;

                if (_rosterSaveTableType != null)
                {
                    string _Message = "";
                    Hashtable ht = new Hashtable();

                    int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    int _SiteID = Convert.ToInt32(_dtStaffGlobal.DefaultView[_currentSelIndex]["SiteID"].ToString());
                    int _StaffID = Convert.ToInt32(_dtStaffGlobal.DefaultView[_currentSelIndex]["StaffID"].ToString());
                    int _StaffLoginID = Convert.ToInt32(_dtStaffGlobal.DefaultView[_currentSelIndex]["LoginID"].ToString());
                    int _crAdminLoginID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedUserLoginID"].ToString());

                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@PrimarySiteID", _SiteID);
                    ht.Add("@TargetSiteID", _SiteID);
                    ht.Add("@StaffID", _StaffID);
                    ht.Add("@StaffLoginID", _StaffLoginID);
                    ht.Add("@CreatedAdminID", _crAdminLoginID);
                    ht.Add("@appVersion", Constants._appVersion);
                    ht.Add("@RosterDetails", _rosterSaveTableType);

                    int _res = _Sql.ExecuteQuery("StaffRoster_SaveUpdateRosterDetails", ht);
                    if (_res > 0)
                    {
                        _Message = "Staff Roster Details Saved successfully!";
                        if (_currSelectedStaffEmailID.Trim() != "")
                        {
                            ComposeAndSendMail(_rosterSaveTableType);
                        }
                        else
                        {
                            _Message = _Message + "\n\n(Staff Email ID not available! Email NOT SENT!)";
                        }
                    }
                    else
                    {
                        _Message = "Error! While saving Roster Details!";
                    }
                    MessageBox.Show(_Message, "Staff Roster", MessageBoxButton.OK, MessageBoxImage.Information);
                    Mouse.OverrideCursor = null;

                    ResetBlockSliders();
                    loadData();
                }
                else
                {
                    MessageBox.Show("Error! While saving Roster Details!", "Staff Roster", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception x)
            {
                string _msg = x.Message;
                MessageBox.Show("Error! While saving Roster Details!", "Staff Roster", MessageBoxButton.OK, MessageBoxImage.Information);
                Mouse.OverrideCursor = null;
            }
        }
        private void ComposeAndSendMail(DataTable _rosterSaveTableType)
        {
            string _strBodyRoster = "";
            string _shiftDetails = "";

            _AdminUserName = ConfigurationManager.AppSettings["LoggedInUser"].ToString();

            _strBodyRoster = "Dear <b>" + _currSelectedStaffName + ",</b><br><br>";
            _strBodyRoster += "<b>Please find below your Shift Time / Roster details :</b><br><br>";

            for (int i = 0; i < _rosterSaveTableType.DefaultView.Count; i++)
            {
                _shiftDetails = "<b>Date : </b>" + Convert.ToDateTime(_rosterSaveTableType.DefaultView[i]["RosterDate"]).ToString("dd MMM yyyy");
                _shiftDetails += " <b>Time: </b>" + Convert.ToInt32(_rosterSaveTableType.DefaultView[i]["ShiftStartHOUR"]).ToString("#0") + ":" + Convert.ToInt32(_rosterSaveTableType.DefaultView[i]["ShiftStartMIN"]).ToString("00");
                _shiftDetails += " - To - ";
                _shiftDetails += Convert.ToInt32(_rosterSaveTableType.DefaultView[i]["ShiftEndHOUR"]).ToString("#0") + ":" + Convert.ToInt32(_rosterSaveTableType.DefaultView[i]["ShiftEndMIN"]).ToString("00");

                _strBodyRoster += _shiftDetails + "<br>";
            }

            if (txtNotes.Text.Trim() != "")
                _strBodyRoster += "<br>" + txtNotes.Text;

            _strBodyRoster += "<br><br>regards, <br>(sent by : " + _AdminUserName + ")";
            _strBodyRoster += "<br><br>*** Should you have any questions/concerns please contact your manager/admin ***";
            _strBodyRoster += "<br>*** This is an auto generated email from REDA MANAGEMENT SOFTWARE ***";

            Comman.Constants.sendEmail("", _currSelectedStaffEmailID,"REDA : STAFF ROSTER - SHIFT DETAILS",_strBodyRoster,"","REDA STAFF ROSTER","STAFF ROSTER");
        }
        private DataTable CreateSaveRosterDataTable()
        {
            bool _atleastOne = false;

            DataTable _table = new DataTable();
            DataRow _dR;

            _table.Columns.Add("RosterDate", typeof(DateTime));
            _table.Columns.Add("ShiftStartHOUR", typeof(int));
            _table.Columns.Add("ShiftStartMIN", typeof(int));
            _table.Columns.Add("ShiftEndHOUR", typeof(int));
            _table.Columns.Add("ShiftEndMIN", typeof(int));

            DateTime _rosterDate;
            int _StartHOUR;
            int _StartMIN;
            int _EndHOUR;
            int _EndMIN;

            if (chkSel0.IsChecked == true) //TODAY
            {
                _atleastOne = true;

                _rosterDate = Convert.ToDateTime(DateTime.Now.Date);
                _StartHOUR = (int)(sliderToday0.LowerValue / 60);
                _StartMIN = (int)(sliderToday0.LowerValue % 60);
                _EndHOUR = (int)(sliderToday0.UpperValue / 60);
                _EndMIN = (int)(sliderToday0.UpperValue % 60);

                _dR = _table.NewRow();
                _dR["RosterDate"] = _rosterDate;
                _dR["ShiftStartHOUR"] = _StartHOUR;
                _dR["ShiftStartMIN"] = _StartMIN;
                _dR["ShiftEndHOUR"] = _EndHOUR;
                _dR["ShiftEndMIN"] = _EndMIN;

                _table.Rows.Add(_dR);
            }
            if (chkSel1.IsChecked == true) //TODAY + 1
            {
                _atleastOne = true;

                _rosterDate = Convert.ToDateTime(DateTime.Now.AddDays(1).Date);
                _StartHOUR = (int)(sliderToday1.LowerValue / 60);
                _StartMIN = (int)(sliderToday1.LowerValue % 60);
                _EndHOUR = (int)(sliderToday1.UpperValue / 60);
                _EndMIN = (int)(sliderToday1.UpperValue % 60);

                _dR = _table.NewRow();
                _dR["RosterDate"] = _rosterDate;
                _dR["ShiftStartHOUR"] = _StartHOUR;
                _dR["ShiftStartMIN"] = _StartMIN;
                _dR["ShiftEndHOUR"] = _EndHOUR;
                _dR["ShiftEndMIN"] = _EndMIN;

                _table.Rows.Add(_dR);
            }
            if (chkSel2.IsChecked == true) //TODAY + 2
            {
                _atleastOne = true;

                _rosterDate = Convert.ToDateTime(DateTime.Now.AddDays(2).Date);
                _StartHOUR = (int)(sliderToday2.LowerValue / 60);
                _StartMIN = (int)(sliderToday2.LowerValue % 60);
                _EndHOUR = (int)(sliderToday2.UpperValue / 60);
                _EndMIN = (int)(sliderToday2.UpperValue % 60);

                _dR = _table.NewRow();
                _dR["RosterDate"] = _rosterDate;
                _dR["ShiftStartHOUR"] = _StartHOUR;
                _dR["ShiftStartMIN"] = _StartMIN;
                _dR["ShiftEndHOUR"] = _EndHOUR;
                _dR["ShiftEndMIN"] = _EndMIN;

                _table.Rows.Add(_dR);
            }
            if (chkSel3.IsChecked == true) //TODAY + 3
            {
                _atleastOne = true;

                _rosterDate = Convert.ToDateTime(DateTime.Now.AddDays(3).Date);
                _StartHOUR = (int)(sliderToday3.LowerValue / 60);
                _StartMIN = (int)(sliderToday3.LowerValue % 60);
                _EndHOUR = (int)(sliderToday3.UpperValue / 60);
                _EndMIN = (int)(sliderToday3.UpperValue % 60);

                _dR = _table.NewRow();
                _dR["RosterDate"] = _rosterDate;
                _dR["ShiftStartHOUR"] = _StartHOUR;
                _dR["ShiftStartMIN"] = _StartMIN;
                _dR["ShiftEndHOUR"] = _EndHOUR;
                _dR["ShiftEndMIN"] = _EndMIN;

                _table.Rows.Add(_dR);
            }
            if (chkSel4.IsChecked == true) //TODAY + 4
            {
                _atleastOne = true;

                _rosterDate = Convert.ToDateTime(DateTime.Now.AddDays(4).Date);
                _StartHOUR = (int)(sliderToday4.LowerValue / 60);
                _StartMIN = (int)(sliderToday4.LowerValue % 60);
                _EndHOUR = (int)(sliderToday4.UpperValue / 60);
                _EndMIN = (int)(sliderToday4.UpperValue % 60);

                _dR = _table.NewRow();
                _dR["RosterDate"] = _rosterDate;
                _dR["ShiftStartHOUR"] = _StartHOUR;
                _dR["ShiftStartMIN"] = _StartMIN;
                _dR["ShiftEndHOUR"] = _EndHOUR;
                _dR["ShiftEndMIN"] = _EndMIN;

                _table.Rows.Add(_dR);
            }
            if (chkSel5.IsChecked == true) //TODAY + 5
            {
                _atleastOne = true;

                _rosterDate = Convert.ToDateTime(DateTime.Now.AddDays(5).Date);
                _StartHOUR = (int)(sliderToday5.LowerValue / 60);
                _StartMIN = (int)(sliderToday5.LowerValue % 60);
                _EndHOUR = (int)(sliderToday5.UpperValue / 60);
                _EndMIN = (int)(sliderToday5.UpperValue % 60);

                _dR = _table.NewRow();
                _dR["RosterDate"] = _rosterDate;
                _dR["ShiftStartHOUR"] = _StartHOUR;
                _dR["ShiftStartMIN"] = _StartMIN;
                _dR["ShiftEndHOUR"] = _EndHOUR;
                _dR["ShiftEndMIN"] = _EndMIN;

                _table.Rows.Add(_dR);
            }
            if (chkSel6.IsChecked == true) //TODAY + 6
            {
                _atleastOne = true;

                _rosterDate = Convert.ToDateTime(DateTime.Now.AddDays(6).Date);
                _StartHOUR = (int)(sliderToday6.LowerValue / 60);
                _StartMIN = (int)(sliderToday6.LowerValue % 60);
                _EndHOUR = (int)(sliderToday6.UpperValue / 60);
                _EndMIN = (int)(sliderToday6.UpperValue % 60);

                _dR = _table.NewRow();
                _dR["RosterDate"] = _rosterDate;
                _dR["ShiftStartHOUR"] = _StartHOUR;
                _dR["ShiftStartMIN"] = _StartMIN;
                _dR["ShiftEndHOUR"] = _EndHOUR;
                _dR["ShiftEndMIN"] = _EndMIN;

                _table.Rows.Add(_dR);
            }

            if (!_atleastOne)
            {
                _table = null;
            }
            return _table;
        }
    }
}
