﻿using System.Windows;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Linq;
using System;
using System.Windows.Media.Imaging;
using System.IO;
using System.Data;
using System.Windows.Input;
using System.Windows.Data;
using System.Windows.Controls.Primitives;
using System.ComponentModel;
using System.Configuration;
using System.Text.RegularExpressions;

using GymMembership.Accounts;
using GymMembership.BAL;
using GymMembership.DTO;
using GymMembership.Comman;

namespace GymMembership.Settings
{
    /// <summary>
    /// Interaction logic for ProductType.xaml
    /// </summary>
    public partial class ProductType 
    {
        Utilities _Util = new Utilities();
        DataSet ds = new DataSet();

        bool _isEdit = true;
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public Login _Login;
        public ProductType()
        {
            InitializeComponent();
        }

        private void btncmbProductTypeCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //this.Title = "CompanyDetails";
            lblOrgName.Content = _CompanyName.ToString().Replace("_", "__");
            lblUserName.Content = _UserName.ToString().Replace("_", "__");
            //lblSiteName.Content = _SiteName.ToString().Replace("_", "__");
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto();

            //_Util.populateProductTypes(cmbProductType);
            populateProductTypes();
            if (cmbProductType.Items.Count > 0)
            {
                cmbProductType.SelectedIndex = 0;
                btncmbProductTypeEdit.IsEnabled = true;
            }
            else
            {
                btncmbProductTypeEdit.IsEnabled = false;
            }
            txtProductType.Text = cmbProductType.Text;
            txtProductType.IsEnabled = false;
            chkProductTypeStatus.IsEnabled = false;
            btncmbProductTypeSave.IsEnabled = false;
        }
        private void populateProductTypes()
        {
            Product _utilProd = new Product();
            ds = _utilProd.getProductTypes();
            if (ds != null)
            {
                if (ds.Tables[0].DefaultView != null)
                {
                    try
                    {
                        cmbProductType.ItemsSource = ds.Tables[0].DefaultView;
                        cmbProductType.DisplayMemberPath = "ProductTypeName";
                        cmbProductType.SelectedValuePath = "ProductTypeID";
                    }
                    catch (Exception)
                    { }
                }
            }
        }
        private void loadStaffPhoto()
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }


        private void cmbProductType_LostFocus(object sender, RoutedEventArgs e)
        {
            txtProductType.Text = cmbProductType.Text;
            txtProductType.IsEnabled = false;
            chkProductTypeStatus.IsEnabled = false;
            btncmbProductTypeSave.IsEnabled = false;
            btncmbProductTypeNew.IsEnabled = true;
            if(cmbProductType.SelectedIndex >= 0)
            {
                btncmbProductTypeEdit.IsEnabled = true;
            }
            else
            {
                btncmbProductTypeEdit.IsEnabled = false;
            }
        }

        private void btncmbProductTypeEdit_Click(object sender, RoutedEventArgs e)
        {
            txtProductType.IsEnabled = true;
            btncmbProductTypeSave.IsEnabled = true;
            btncmbProductTypeNew.IsEnabled = true;
            btncmbProductTypeEdit.IsEnabled = false;
            _isEdit = true;

            chkProductTypeStatus.IsEnabled = true;

            txtProductType.Focus();
        }
        private void btncmbProductTypeNew_Click(object sender, RoutedEventArgs e)
        {
            cmbProductType.SelectedIndex = -1;
            txtProductType.IsEnabled = true;
            chkProductTypeStatus.IsEnabled = true;
            btncmbProductTypeEdit.IsEnabled = false;
            btncmbProductTypeSave.IsEnabled = true;
            _isEdit = false;
            txtProductType.Text = "";

            chkProductTypeStatus.IsChecked = true;
            chkProductTypeStatus.IsEnabled = false;

            txtProductType.Focus();
        }

        private void btncmbProductTypeSave_Click(object sender, RoutedEventArgs e)
        {
            string _productType;
            int _newSucess;
            int _newEditSucess;
            int _productTypeId = 0;
            string _newProdtype;
            string _existingProdType;
            try
            {
                SettingsBAL _settingDetailsBAL = new SettingsBAL();
                if (txtProductType.Text.Trim() == "")
                {
                    MessageBox.Show("Please enter a Product Type", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProductType.Focus();
                    return;
                }
                else if (Regex.IsMatch(txtProductType.Text, @"[^a-zA-Z0-9\s]"))
                {
                    MessageBox.Show("Don't Use Special Characters", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    txtProductType.Focus();
                    return;
                }
                if (!_isEdit)
                {
                    //Check if ProdType already exists, and INSERT new ProdType
                    _newProdtype = txtProductType.Text.Trim();

                    for (int i = 0; i < cmbProductType.Items.Count; i++)
                    {
                        //cmbProductType.SelectedIndex = i;
                        //_existingProdType = cmbProductType.Text.Trim().ToUpper();
                        _existingProdType = (cmbProductType.Items[i] as DataRowView).Row["ProductTypeName"].ToString();
                        if ((_newProdtype.ToUpper() == _existingProdType.ToUpper()))
                        {
                            MessageBox.Show("Product Type Already Exists! Please enter a New ProductType!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            btncmbProductTypeSave.IsEnabled = true;
                            return;
                        }
                    }
                    _newSucess = _settingDetailsBAL.saveNew_productType(_newProdtype);
                    MessageBox.Show("New Product Type Added ! " + _newProdtype, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    //_Util.populateProductTypes(cmbProductType);
                    populateProductTypes();
                    txtProductType.Text = "";
                    btncmbProductTypeEdit.IsEnabled = true;
                    return;
                }
                else
                {
                    //UPDATE PRODTYPE WITH NEW NAME
                    _productTypeId = Convert.ToInt32(cmbProductType.SelectedValue.ToString().Trim());
                    _newProdtype = txtProductType.Text.ToString().Trim();
                    int _ProdTypeID = Convert.ToInt32(cmbProductType.SelectedValue.ToString());
                    int j = cmbProductType.SelectedIndex;
                    for (int i = 0; i < cmbProductType.Items.Count; i++)
                    {
                        //cmbProductType.SelectedIndex = i;
                        //_existingProdType = cmbProductType.Text.Trim().ToUpper();
                        _existingProdType = (cmbProductType.Items[i] as DataRowView).Row["ProductTypeName"].ToString();
                        if ((_newProdtype.ToUpper() == _existingProdType.ToUpper()) && (i != j)) //&& (_ProdTypeID != Convert.ToInt32(cmbProductType.SelectedValue.ToString())))
                        {
                            MessageBox.Show("Product Type Already Exists! Please enter a New ProductType!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                            btncmbProductTypeSave.IsEnabled = false;
                            txtProductType.IsEnabled = false;
                            chkProductTypeStatus.IsEnabled = false;
                            btncmbProductTypeEdit.IsEnabled = true;
                            return;
                        }
                    }
                    if (chkProductTypeStatus.IsChecked == true)
                    {
                        _newEditSucess = _settingDetailsBAL.saveEdit_productType(_productTypeId, _newProdtype);
                    }
                    else
                    {
                        _newEditSucess = _settingDetailsBAL.saveEdit_productType(_productTypeId, _newProdtype, 0);
                    }
                    MessageBox.Show("ProductType Edited ! " + _newProdtype, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                }

                //_Util.populateProductTypes(cmbProductType);
                populateProductTypes();
                if (cmbProductType.Items.Count > 0) { cmbProductType.SelectedIndex = 0; }
                txtProductType.Text = cmbProductType.Text;
                txtProductType.IsEnabled = false;
                chkProductTypeStatus.IsEnabled = false;
                btncmbProductTypeSave.IsEnabled = false;
                btncmbProductTypeEdit.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnContactTypes1_Click(object sender, RoutedEventArgs e)
        {
            CompanyDetails _CompanySiteDetails = new CompanyDetails();
            _CompanySiteDetails._CompanyName = this._CompanyName;
            _CompanySiteDetails._UserName = this._UserName;
            _CompanySiteDetails._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _CompanySiteDetails._StaffPic = this._StaffPic;
            _CompanySiteDetails.StaffImage.Source = StaffImage.Source;
            _CompanySiteDetails.Owner = this.Owner;

            this.Close();
            _CompanySiteDetails.ShowDialog();
        }

        private void btnUserAdministration_Click(object sender, RoutedEventArgs e)
        {
            if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
            {
                UserAdministration useradmin = new UserAdministration();
                useradmin._CompanyName = this._CompanyName;
                useradmin._UserName = this._UserName;
                useradmin._CurrentLoginDateTime = this._CurrentLoginDateTime;
                useradmin._StaffPic = this._StaffPic;
                useradmin.Owner = this.Owner;

                this.Close();
                useradmin.ShowDialog();
            }
            else
            {
                MessageBox.Show("You MUST be Admin user to access this Screen!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnProductsTypes_Click(object sender, RoutedEventArgs e)
        {
            //SAME WINDOW; NO ACTION

            //ProductType prdType = new ProductType();
            //prdType._CompanyName = this._CompanyName;
            //prdType._UserName = this._UserName;
            //prdType._CurrentLoginDateTime = this._CurrentLoginDateTime;
            //prdType._StaffPic = this._StaffPic;
            //prdType.Owner = this.Owner;

            //this.Close();
            //prdType.ShowDialog();
        }

        private void btnManageProducts_Click(object sender, RoutedEventArgs e)
        {
            Products products = new Products();
            products._CompanyName = this._CompanyName;
            products._UserName = this._UserName;
            products._CurrentLoginDateTime = this._CurrentLoginDateTime;
            products._StaffPic = this._StaffPic;
            products.Owner = this.Owner;

            this.Close();
            products.ShowDialog();
        }

        private void btnProgrammeGSetup_Click(object sender, RoutedEventArgs e)
        {
            ProgrammeGroup PrgGroup = new ProgrammeGroup();
            PrgGroup._CompanyName = this._CompanyName;
            PrgGroup._UserName = this._UserName;
            PrgGroup._CurrentLoginDateTime = this._CurrentLoginDateTime;
            PrgGroup._StaffPic = this._StaffPic;
            PrgGroup.Owner = this.Owner;

            this.Close();
            PrgGroup.ShowDialog();
        }

        private void btnAddEditProgrammes_Click(object sender, RoutedEventArgs e)
        {
            Programme Prg = new Programme();
            Prg._CompanyName = this._CompanyName;
            Prg._UserName = this._UserName;
            Prg._CurrentLoginDateTime = this._CurrentLoginDateTime;
            Prg._StaffPic = this._StaffPic;
            Prg.Owner = this.Owner;

            this.Close();
            Prg.ShowDialog();
        }

        private void btnSupplierSetup_Click(object sender, RoutedEventArgs e)
        {
            Supplier _Supplier = new Supplier();
            _Supplier._CompanyName = this._CompanyName;
            _Supplier._UserName = this._UserName;
            _Supplier._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Supplier._StaffPic = this._StaffPic;
            _Supplier.Owner = this.Owner;

            this.Close();
            _Supplier.ShowDialog();
        }

        private void btnBookingResources_Click(object sender, RoutedEventArgs e)
        {
            Settings.Resources _Res = new Settings.Resources();
            _Res._CompanyName = this._CompanyName;
            _Res._UserName = this._UserName;
            _Res._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Res._StaffPic = this._StaffPic;
            _Res.Owner = this.Owner;

            this.Close();
            _Res.ShowDialog();
        }

        private void btnUserAccessRights_Click(object sender, RoutedEventArgs e)
        {
            StaffAccess _StaffAccess = new StaffAccess();
            _StaffAccess._CompanyName = this._CompanyName;
            _StaffAccess._UserName = this._UserName;
            _StaffAccess._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _StaffAccess._StaffPic = this._StaffPic;
            _StaffAccess.Owner = this.Owner;

            this.Close();
            _StaffAccess.ShowDialog();
        }

        private void btnEmailTSetup_Click(object sender, RoutedEventArgs e)
        {
            EmailTemplateSettings _eMailTemplate = new EmailTemplateSettings();
            _eMailTemplate._CompanyName = this._CompanyName;
            _eMailTemplate._UserName = this._UserName;
            _eMailTemplate._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _eMailTemplate._StaffPic = this._StaffPic;
            _eMailTemplate.Owner = this.Owner;

            this.Close();
            _eMailTemplate.ShowDialog();
        }

        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDashboardQuit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
                //Closing event called
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnContactTypes_Click(object sender, RoutedEventArgs e)
        {

        }

        private void cmbProductType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbProductType.SelectedIndex != -1)
            {
                txtProductType.Text = (cmbProductType.SelectedItem as DataRowView).Row["ProductTypeName"].ToString();
                txtProductType.IsEnabled = false;
                chkProductTypeStatus.IsEnabled = false;
                btncmbProductTypeSave.IsEnabled = false;
                btncmbProductTypeNew.IsEnabled = true;
                if (cmbProductType.SelectedIndex >= 0)
                {
                    btncmbProductTypeEdit.IsEnabled = true;
                }
                else
                {
                    btncmbProductTypeEdit.IsEnabled = false;
                }

                if ((cmbProductType.SelectedItem as DataRowView).Row["Active"].ToString() == "1")
                {
                    chkProductTypeStatus.IsChecked = true;
                }
                else
                {
                    chkProductTypeStatus.IsChecked = false;
                }
            }
        }
    }
}
