﻿using System;
using System.Collections;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

using GymMembership.BAL;
using GymMembership.DAL;
using GymMembership.Comman;
using GymMembership.Accounts;
using System.Windows.Media.Imaging;
using System.IO;


namespace GymMembership.Settings
{
    /// <summary>
    /// Interaction logic for StaffAccess.xaml
    /// </summary>
    public partial class StaffAccess 
    {
        public StaffAccess()
        {
            InitializeComponent();
        }
        public string _CurrentLoginDateTime;
        public string _CompanyName;
        public string _SiteName;
        public string _UserName;
        public byte[] _StaffPic;
        public Login _Login;
        int _currentSelectedModuleID = 0; int _currentSelectedSubModuleID = 0;
        int _ModuleTable = 0; int _SubModuleTable = 1;
        DataSet _dsUserRights = new DataSet();
        DataSet _dsUsers = new DataSet();

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //Utilities _Util = new Utilities();
                //_Util.GetActiveUserNamesUAdmin(cmbUserName);

                SettingsBAL _SettingsBal = new SettingsBAL();
                _dsUsers = _SettingsBal.GetActiveUserNamesUAdmin();

                if (_dsUsers != null)
                {
                    if (_dsUsers.Tables[0] != null)
                    {
                        cmbUserName.ItemsSource = _dsUsers.Tables[0].DefaultView;
                        cmbUserName.DisplayMemberPath = "Name";
                        cmbUserName.SelectedValuePath = "LoginID";
                        cmbUserName.SelectedIndex = 0;
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            finally
            {
                cmbUserName.Focus();
            }
            //this.Title = "CompanyDetails";
            lblOrgName.Content = _CompanyName.ToString().Replace("_", "__");
            lblUserName.Content = _UserName.ToString().Replace("_", "__");
            //lblSiteName.Content = _SiteName.ToString().Replace("_", "__");
            lblLoggedInTime.Content = _CurrentLoginDateTime;
            //loadStaffPhoto();
        }

        private void loadStaffPhoto()
        {
            //StaffPIC
            if (_StaffPic != null)
            {
                if (_StaffPic.Length >= 4)
                {
                    MemoryStream strm = new MemoryStream();
                    strm.Write(_StaffPic, 0, _StaffPic.Length);
                    strm.Position = 0;

                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();

                    StaffImage.Source = bi;
                }
            }
        }

        private void cmbUserName_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            try
            {
                int _CompanyID; int _LoginID; int _SiteID;

                //>>> Form Controls START
                lvModuleActive.ItemsSource = null;
                lvModuleInActive.ItemsSource = null;
                lvSubModuleActive.ItemsSource = null;
                lvSubModuleInActive.ItemsSource = null;

                lvModuleActive.Items.Clear();
                lvModuleInActive.Items.Clear();
                lvSubModuleActive.Items.Clear();
                lvSubModuleInActive.Items.Clear();
                // >>> Form controls END

                _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                _LoginID = Convert.ToInt32(cmbUserName.SelectedValue.ToString());
                _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                SettingsDAL _SettingsDal = new SettingsDAL();
                _dsUserRights = _SettingsDal.GetAccessRightForUser(_CompanyID, _SiteID, _LoginID);

                if (_dsUserRights != null)
                {
                    if (_dsUserRights.Tables[_ModuleTable].DefaultView != null)
                    {
                        SetActiveAndInactiveModules();
                    }
                }

                //FILTER LOGIN
                _dsUsers.Tables[0].DefaultView.RowFilter = "";
                _dsUsers.Tables[0].DefaultView.RowFilter = "LoginID = " + _LoginID;
                if (_dsUsers.Tables[0].DefaultView.Count > 0)
                {
                    if ( _dsUsers.Tables[0].DefaultView[0]["UserRole"].ToString() == "2")
                    {
                        lblUserRole.Content = "** ADMIN USER **";
                        btnSave.IsEnabled = false;
                        btnModuleAdd.IsEnabled = false;
                        btnModuleRemove.IsEnabled = false;
                        btnSubModuleAdd.IsEnabled = false;
                        btnSubModuleRemove.IsEnabled = false;
                    }
                    else
                    {
                        lblUserRole.Content = "** NORMAL USER **";
                        btnSave.IsEnabled = true;
                        btnModuleAdd.IsEnabled = true;
                        btnModuleRemove.IsEnabled = true;
                        btnSubModuleAdd.IsEnabled = true;
                        btnSubModuleRemove.IsEnabled = true;
                    }
                }
                _dsUsers.Tables[0].DefaultView.RowFilter = "";
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private void lvModuleActive_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            try
            {
                lvSubModuleInActive.IsEnabled = true;
                int _intSelect; int _ModuleID;
                _intSelect = Convert.ToInt32(lvModuleActive.SelectedIndex);
                _ModuleID = Convert.ToInt32(lvModuleActive.SelectedValue);
                _currentSelectedModuleID = _ModuleID;

                SetActiveAdInactiveSubModules(_ModuleID);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private void lvModuleInActive_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                lvSubModuleInActive.IsEnabled = false;
                int _intSelect; int _ModuleID;
                _intSelect = Convert.ToInt32(lvModuleInActive.SelectedIndex);
                _ModuleID = Convert.ToInt32(lvModuleInActive.SelectedValue);
                _currentSelectedModuleID = _ModuleID;

                SetActiveAdInactiveSubModules(_ModuleID);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnModuleAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lvModuleInActive.SelectedItems.Count <= 0)
                {
                    MessageBox.Show("Select a Module from 'Inactive' List to Add!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                
                _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "ModuleID = " + _currentSelectedModuleID;
                foreach (DataRowView rowView in _dsUserRights.Tables[_ModuleTable].DefaultView)
                {
                    rowView["Active"] = 1;
                }
                _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = string.Empty;
                _dsUserRights.Tables[_ModuleTable].AcceptChanges();
                _dsUserRights.AcceptChanges();

                SetActiveAndInactiveModules();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnModuleRemove_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lvModuleActive.SelectedItems.Count <= 0)
                {
                    MessageBox.Show("Select a Module from 'Active' List to Remove!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = "ModuleID = " + _currentSelectedModuleID;
                foreach (DataRowView rowView in _dsUserRights.Tables[_ModuleTable].DefaultView)
                {
                    rowView["Active"] = 0;
                }

                //Module Inactive - all Submodules should be Inactive!
                _dsUserRights.Tables[_SubModuleTable].DefaultView.RowFilter = "ModuleID = " + _currentSelectedModuleID;
                foreach (DataRowView rowView in _dsUserRights.Tables[_SubModuleTable].DefaultView)
                {
                    rowView["Active"] = 0;
                }
                _dsUserRights.Tables[_ModuleTable].DefaultView.RowFilter = string.Empty;
                _dsUserRights.Tables[_ModuleTable].AcceptChanges();
                _dsUserRights.AcceptChanges();

                SetActiveAndInactiveModules();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnSubModuleAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lvSubModuleInActive.SelectedItems.Count <= 0)
                {
                    MessageBox.Show("Select a Sub-Module from 'Inactive' List to Add!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                _dsUserRights.Tables[_SubModuleTable].DefaultView.RowFilter = "ModuleID = " + _currentSelectedModuleID + " AND SubModuleID = " + _currentSelectedSubModuleID;
                foreach (DataRowView rowView in _dsUserRights.Tables[_SubModuleTable].DefaultView)
                {
                    rowView["Active"] = 1;
                }
                _dsUserRights.Tables[_SubModuleTable].DefaultView.RowFilter = string.Empty;
                _dsUserRights.Tables[_SubModuleTable].AcceptChanges();
                _dsUserRights.AcceptChanges();

                SetActiveAdInactiveSubModules(_currentSelectedModuleID);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }
        private void btnSubModuleRemove_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lvSubModuleActive.SelectedItems.Count <= 0)
                {
                    MessageBox.Show("Select a Sub-Module from 'Active' List to Remove!", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                _dsUserRights.Tables[_SubModuleTable].DefaultView.RowFilter = "ModuleID = " + _currentSelectedModuleID + " AND SubModuleID = " + _currentSelectedSubModuleID;
                foreach (DataRowView rowView in _dsUserRights.Tables[_SubModuleTable].DefaultView)
                {
                    rowView["Active"] = 0;
                }
                _dsUserRights.Tables[_SubModuleTable].DefaultView.RowFilter = string.Empty;
                _dsUserRights.Tables[_SubModuleTable].AcceptChanges();
                _dsUserRights.AcceptChanges();

                SetActiveAdInactiveSubModules(_currentSelectedModuleID);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            try
            { 
                if (MessageBox.Show("Edit in Progress! Are you sure to Close?", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    this.Close();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
        private void lvSubModuleActive_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvSubModuleActive.SelectedIndex != -1)
            {
                _currentSelectedSubModuleID = Convert.ToInt32(lvSubModuleActive.SelectedValue);
            }
        }
        private void lvSubModuleInActive_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvSubModuleInActive.SelectedIndex != -1)
            {
                _currentSelectedSubModuleID = Convert.ToInt32(lvSubModuleInActive.SelectedValue);
            }
        }
        private void SetActiveAndInactiveModules()
        {
            //ACTIVE MODULES
            var dv = _dsUserRights.Tables[_ModuleTable].DefaultView;
            dv.RowFilter = "Active = 1";
            DataTable newDT = dv.ToTable();

            lvModuleActive.ItemsSource = null; lvModuleActive.Items.Clear();
            lvModuleActive.ItemsSource = newDT.AsDataView();
            lvModuleActive.DisplayMemberPath = "ModuleName";
            lvModuleActive.SelectedValuePath = "ModuleID";

            //IN-ACTIVE MODULES
            dv = _dsUserRights.Tables[_ModuleTable].DefaultView;
            dv.RowFilter = "Active = 0";
            newDT = dv.ToTable();

            lvModuleInActive.ItemsSource = null; lvModuleInActive.Items.Clear();
            lvModuleInActive.ItemsSource = newDT.AsDataView();
            lvModuleInActive.DisplayMemberPath = "ModuleName";
            lvModuleInActive.SelectedValuePath = "ModuleID";
        }
        private void SetActiveAdInactiveSubModules(int _ModuleID)
        {
            //ACTIVE SUBMODULES
            var dv = _dsUserRights.Tables[_SubModuleTable].DefaultView;
            dv.RowFilter = "Active = 1 AND ModuleID = " + _ModuleID;
            DataTable newDT = dv.ToTable();

            DataTable _nDT = new DataTable("SubModules");
            _nDT.Columns.Add("SubModuleName");
            _nDT.Columns.Add("SubModuleID");

            foreach (DataRowView dRow in newDT.DefaultView)
            {
                DataRow dR = _nDT.NewRow();
                dR["SubModuleName"] = dRow["SubModuleName"].ToString();
                dR["SubModuleID"] = dRow["SubModuleID"].ToString();

                _nDT.Rows.Add(dR);
            }

            lvSubModuleActive.ItemsSource = null;
            lvSubModuleActive.Items.Clear();
            //lvSubModuleActive.ItemsSource = newDT.DefaultView;
            lvSubModuleActive.ItemsSource = _nDT.DefaultView;
            lvSubModuleActive.DisplayMemberPath = "SubModuleName";
            lvSubModuleActive.SelectedValuePath = "SubModuleID";
            lvSubModuleActive.Items.Refresh();

            //IN-ACTIVE SUBMODULES
            dv = _dsUserRights.Tables[_SubModuleTable].DefaultView;
            dv.RowFilter = "Active = 0 AND ModuleID = " + _ModuleID;
            newDT = dv.ToTable();

            _nDT = new DataTable("SubModules");
            _nDT.Columns.Add("SubModuleName");
            _nDT.Columns.Add("SubModuleID");

            foreach (DataRowView dRow in newDT.DefaultView)
            {
                DataRow dR = _nDT.NewRow();
                dR["SubModuleName"] = dRow["SubModuleName"].ToString();
                dR["SubModuleID"] = dRow["SubModuleID"].ToString();

                _nDT.Rows.Add(dR);
            }

            lvSubModuleInActive.ItemsSource = null; 
            lvSubModuleInActive.Items.Clear();
            //lvSubModuleInActive.ItemsSource = newDT.DefaultView;
            lvSubModuleInActive.ItemsSource = _nDT.DefaultView;
            lvSubModuleInActive.DisplayMemberPath = "SubModuleName";
            lvSubModuleInActive.SelectedValuePath = "SubModuleID";
            lvSubModuleInActive.Items.Refresh();
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Are You sure to apply the Changes?", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    _dsUserRights.Tables[_SubModuleTable].DefaultView.RowFilter = "";
                    int _CompanyID; //@CompanyID int,
                    int _LoginID; //@LoginID int,
                    int _SiteID;
                    string _UserName;

                    SqlHelper sql = new SqlHelper();
                    Hashtable ht = new Hashtable();

                    _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    _LoginID = Convert.ToInt32(cmbUserName.SelectedValue.ToString());
                    _UserName = ConfigurationManager.AppSettings["LoggedInUser"].ToString();
                    _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());

                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@LoginID", _LoginID);
                    ht.Add("@UserName", _UserName);
                    ht.Add("@UserAccess_Update", _dsUserRights.Tables[_SubModuleTable]);
                    ht.Add("@SiteID", _SiteID);

                    sql.ExecuteQuery("UpdateUserAccessRights", ht);

                    MessageBox.Show("User Access Rights updated sucessfully!", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: User Access Rights NOT Updated! \n\n" + ex.Message, this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                //System.GC.Collect();
                return;
            }
        }


        private void btnContactTypes1_Click(object sender, RoutedEventArgs e)
        {
            CompanyDetails _CompanySiteDetails = new CompanyDetails();
            _CompanySiteDetails._CompanyName = this._CompanyName;
            _CompanySiteDetails._UserName = this._UserName;
            _CompanySiteDetails._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _CompanySiteDetails._StaffPic = this._StaffPic;
            _CompanySiteDetails.StaffImage.Source = StaffImage.Source;
            _CompanySiteDetails.Owner = this.Owner;

            this.Close();
            _CompanySiteDetails.ShowDialog();
        }

        private void btnUserAdministration_Click(object sender, RoutedEventArgs e)
        {
            if (ConfigurationManager.AppSettings["IsAdminUser"].ToString().ToUpper() == "YES")
            {
                UserAdministration useradmin = new UserAdministration();
                useradmin._CompanyName = this._CompanyName;
                useradmin._UserName = this._UserName;
                useradmin._CurrentLoginDateTime = this._CurrentLoginDateTime;
                useradmin._StaffPic = this._StaffPic;
                useradmin.StaffImage.Source = StaffImage.Source;
                useradmin.Owner = this.Owner;

                this.Close();
                useradmin.ShowDialog();
            }
            else
            {
                MessageBox.Show("You MUST be Admin user to access this Screen!");
                return;
            }
        }

        private void btnProductsTypes_Click(object sender, RoutedEventArgs e)
        {

            ProductType prdType = new ProductType();
            prdType._CompanyName = this._CompanyName;
            prdType._UserName = this._UserName;
            prdType._CurrentLoginDateTime = this._CurrentLoginDateTime;
            prdType._StaffPic = this._StaffPic;
            prdType.StaffImage.Source = StaffImage.Source;
            prdType.Owner = this.Owner;

            this.Close();
            prdType.ShowDialog();
        }

        private void btnManageProducts_Click(object sender, RoutedEventArgs e)
        {
            Products products = new Products();
            products._CompanyName = this._CompanyName;
            products._UserName = this._UserName;
            products._CurrentLoginDateTime = this._CurrentLoginDateTime;
            products._StaffPic = this._StaffPic;
            products.StaffImage.Source = StaffImage.Source;
            products.Owner = this.Owner;

            this.Close();
            products.ShowDialog();
        }

        private void btnProgrammeGSetup_Click(object sender, RoutedEventArgs e)
        {
            ProgrammeGroup PrgGroup = new ProgrammeGroup();
            PrgGroup._CompanyName = this._CompanyName;
            PrgGroup._UserName = this._UserName;
            PrgGroup._CurrentLoginDateTime = this._CurrentLoginDateTime;
            PrgGroup._StaffPic = this._StaffPic;
            PrgGroup.StaffImage.Source = StaffImage.Source;
            PrgGroup.Owner = this.Owner;

            this.Close();
            PrgGroup.ShowDialog();
        }

        private void btnAddEditProgrammes_Click(object sender, RoutedEventArgs e)
        {
            Programme Prg = new Programme();
            Prg._CompanyName = this._CompanyName;
            Prg._UserName = this._UserName;
            Prg._CurrentLoginDateTime = this._CurrentLoginDateTime;
            Prg._StaffPic = this._StaffPic;
            Prg.StaffImage.Source = StaffImage.Source;
            Prg.Owner = this.Owner;

            this.Close();
            Prg.ShowDialog();
        }

        private void btnSupplierSetup_Click(object sender, RoutedEventArgs e)
        {
            Supplier _Supplier = new Supplier();
            _Supplier._CompanyName = this._CompanyName;
            _Supplier._UserName = this._UserName;
            _Supplier._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Supplier._StaffPic = this._StaffPic;
            _Supplier.StaffImage.Source = StaffImage.Source;
            _Supplier.Owner = this.Owner;

            this.Close();
            _Supplier.ShowDialog();
        }

        private void btnBookingResources_Click(object sender, RoutedEventArgs e)
        {
            Settings.Resources _Res = new Settings.Resources();
            _Res._CompanyName = this._CompanyName;
            _Res._UserName = this._UserName;
            _Res._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _Res._StaffPic = this._StaffPic;
            _Res.StaffImage.Source = StaffImage.Source;
            _Res.Owner = this.Owner;

            this.Close();
            _Res.ShowDialog();
        }

        private void btnUserAccessRights_Click(object sender, RoutedEventArgs e)
        {
            ////SAME WINDOW; NO ACTION

            //StaffAccess _StaffAccess = new StaffAccess();
            //_StaffAccess._CompanyName = this._CompanyName;
            //_StaffAccess._UserName = this._UserName;
            //_StaffAccess._CurrentLoginDateTime = this._CurrentLoginDateTime;
            //_StaffAccess._StaffPic = this._StaffPic;
            //_StaffAccess.StaffImage.Source = StaffImage.Source;
            //_StaffAccess.Owner = this.Owner;

            //this.Close();
            //_StaffAccess.ShowDialog();
        }

        private void btnEmailTSetup_Click(object sender, RoutedEventArgs e)
        {
            EmailTemplateSettings _eMailTemplate = new EmailTemplateSettings();
            _eMailTemplate._CompanyName = this._CompanyName;
            _eMailTemplate._UserName = this._UserName;
            _eMailTemplate._CurrentLoginDateTime = this._CurrentLoginDateTime;
            _eMailTemplate._StaffPic = this._StaffPic;
            _eMailTemplate.StaffImage.Source = StaffImage.Source;
            _eMailTemplate.Owner = this.Owner;

            this.Close();
            _eMailTemplate.ShowDialog();
        }

        private void btnbacktoDashBoard_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDashboardQuit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
                //Closing event called
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message);
                return;
            }
        }

        private void btnContactTypes_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
