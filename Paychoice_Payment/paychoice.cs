﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Windows.Forms;
using System.Data;

namespace Paychoice_Payment
{
    public class paychoice
    {
        static string customer_id = null;
        static string bank_token = null;
        static string token = null;
        static int card_identifier = 0;
        static int add_debit = 0;
        static double subscription_amount = 0;
        static string member_id_proceedure_name = "FetchMemeberID_Payment";
        static string Payment_Schedule = null;
        static string billing_cycle_unit = null;
        static string billing_cycle_unit_Fortnight;
        static int billing_cycle_length = 0;
        static int billing_cycle_length_Fortnight = 0;
        static string subscription_id;
        //Create a new User 
        public static int pay_via_bankaccount(Hashtable htaccountdetails)
        {
            try
            {
                add_debit = 0;
                string paymentmode = "Bank Account";
                int Company_id = Int32.Parse(htaccountdetails["Company_Id"].ToString());
                int Site_id = Int32.Parse(htaccountdetails["Site_Id"].ToString());
                int App_Customerid = Int32.Parse(htaccountdetails["App_Member_id"].ToString());
                int Member_ID = FetchMemberId(Company_id, Site_id, App_Customerid);

                if (Member_ID != 0)
                {
                    create_new_user(htaccountdetails, Company_id, Site_id, Member_ID, App_Customerid);
                    create_new_bank_account(htaccountdetails, paymentmode);
                    set_default_payment_method(htaccountdetails);
                    create_new_subscription(htaccountdetails);

                    add_debit = AddDirectDebit(Company_id, Site_id, paymentmode, Member_ID);
                    if (add_debit == 1)
                    {
                        successmessage();
                    }
                    else
                    {
                        failuremessage();
                    }
                }
                return add_debit;
            }
            catch (Exception ex)
            {
                try
                {
                    Hashtable htpaychoicecustomer = new Hashtable();
                    int Company_id = Int32.Parse(htaccountdetails["Company_Id"].ToString());
                    int Site_id = Int32.Parse(htaccountdetails["Site_Id"].ToString());
                    int App_Customerid = Int32.Parse(htaccountdetails["App_Member_id"].ToString());
                    int Member_ID = FetchMemberId(Company_id, Site_id, App_Customerid);

                    DataManager dal = new DataManager();
                    //while (!dal._ConnOpen)
                    //{
                    //    dal = new DataManager();
                    //}

                    htpaychoicecustomer.Add("@AppMemberID", Member_ID);
                    //dal.ExecuteNonQueryProcedure("DelPayChoiceCustomer", htpaychoicecustomer);

                    MessageBox.Show("Error creating Payment Method: Bank Account \n\nError details: " + ex.Message, "Billing & Bank", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return 0;
                }
                catch (Exception ex1)
                {
                    MessageBox.Show("Error creating Payment Method: Bank Account (Rollback)\n\nError details: " + ex1.Message, "Billing & Bank", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return 0;
                }
            }

        }
        public static int FetchMemberId(int CompanyId, int SiteID, int AppCustomerID)
        {
            Hashtable htmemeberid = new Hashtable();

            DataManager dal = new DataManager();
            //while (!dal._ConnOpen)
            //{
            //    dal = new DataManager();
            //}

            htmemeberid.Add("@CompanyID", CompanyId);
            htmemeberid.Add("@SiteID", SiteID);
            htmemeberid.Add("@Memberno", AppCustomerID);
            DataSet ds_member_ID = dal.ExecuteProcedureReader(member_id_proceedure_name, htmemeberid);
            htmemeberid.Clear();
            if (ds_member_ID != null)
            {
                if (ds_member_ID.Tables[0].DefaultView.Count > 0)
                {
                    return Int32.Parse(ds_member_ID.Tables[0].Rows[0]["MemberID"].ToString());
                }
            }
            return 0;
        }
        public static int AddDirectDebit(int CompanyId, int SiteID, string paymentmmode, int memberid)
        {
            Hashtable htdirectdebit = new Hashtable();

            DataManager dal = new DataManager();
            //while (!dal._ConnOpen)
            //{
            //    dal = new DataManager();
            //}

            Create_Subscription sub = new Create_Subscription();
            string proceedurename = "AddDirectDebit";
            string transactionstatus = "success";
            DateTime lastpaidate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-ddTHH:mmzzz")); //DateTime.Now.ToString("yyyy-MM-ddTHH:mmzzz");
            int count = 0;
            htdirectdebit.Add("@CompanyID", CompanyId);
            htdirectdebit.Add("@SiteID", SiteID);
            htdirectdebit.Add("@AppCustomerID", memberid);

            htdirectdebit.Add("@PaychoiceCustomerID", customer_id);

            htdirectdebit.Add("@payschedule", Payment_Schedule);
            if (billing_cycle_unit == "Days")
            {
                count = billing_cycle_length * 1;
            }
            else if (billing_cycle_unit == "Weeks")
            {
                count = billing_cycle_length * 7;
            }
            else if (billing_cycle_unit == "Months")
            {
                count = billing_cycle_length * 30;
            }
            else if (billing_cycle_unit == "Years")
            {
                count = billing_cycle_length * 365;
            }
            else if (billing_cycle_unit == "Fortnight")
            {
                count = billing_cycle_length * 14;
            }
            if (paymentmmode == "Bank Account")
            {
                htdirectdebit.Add("@PaymentMode", paymentmmode);
                htdirectdebit.Add("@paychoicetoken", bank_token);
                htdirectdebit.Add("@Amount", 0.00);
                transactionstatus = "processing";
                count = count + 4;
            }
            else
            {
                htdirectdebit.Add("@PaymentMode", paymentmmode);
                htdirectdebit.Add("@paychoicetoken", token);
                htdirectdebit.Add("@Amount", subscription_amount);
            }
            htdirectdebit.Add("@nextdate", DateTime.Now.AddDays(count).ToString("yyyy-MM-ddTHH:mmzzz"));
            htdirectdebit.Add("@lasttransactionstatus", transactionstatus);
            htdirectdebit.Add("@lastpaiddate", DateTime.Now.ToString("yyyy-MM-ddTHH:mmzzz"));
            int directdebit = dal.ExecuteNonQueryProcedure(proceedurename, htdirectdebit);
            htdirectdebit.Clear();
            return directdebit;
        }
        public static void successmessage()
        {
            MessageBox.Show("Direct Debit Payment Details Added Successfully", "Direct Debit", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public static void failuremessage()
        {
            MessageBox.Show("Some Problems occured handling with database", "Direct Debit", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        public static int pay_via_credit_card(Hashtable htcarddetails)
        {
            try
            {
                string paymentmode = "Credit Card";
                card_identifier = 1;
                int Company_id = Int32.Parse(htcarddetails["Company_Id"].ToString());
                int Site_id = Int32.Parse(htcarddetails["Site_Id"].ToString());
                int App_Customerid = Int32.Parse(htcarddetails["App_Member_id"].ToString());
                int Member_ID = FetchMemberId(Company_id, Site_id, App_Customerid);

                if (Member_ID != 0)
                {
                    create_new_user(htcarddetails, Company_id, Site_id, Member_ID, App_Customerid);
                    create_new_credit_card(htcarddetails, paymentmode);
                    set_cc_default_payment_method(htcarddetails);
                    create_new_subscription(htcarddetails);

                    add_debit = AddDirectDebit(Company_id, Site_id, paymentmode, Member_ID);
                    if (add_debit == 1)
                    {
                        successmessage();
                    }
                    else
                    {
                        failuremessage();
                    }
                }
                return add_debit;
            }
            catch (Exception ex)
            {
                try
                {
                    Hashtable htpaychoicecustomer = new Hashtable();
                    int Company_id = Int32.Parse(htcarddetails["Company_Id"].ToString());
                    int Site_id = Int32.Parse(htcarddetails["Site_Id"].ToString());
                    int App_Customerid = Int32.Parse(htcarddetails["App_Member_id"].ToString());
                    int Member_ID = FetchMemberId(Company_id, Site_id, App_Customerid);

                    DataManager dal = new DataManager();
                    //while (!dal._ConnOpen)
                    //{
                    //    dal = new DataManager();
                    //}


                    htpaychoicecustomer.Add("@AppMemberID", Member_ID);
                    //dal.ExecuteNonQueryProcedure("DelPayChoiceCustomer", htpaychoicecustomer);

                    MessageBox.Show("Error creating Payment Method: Credit Card \n\nError details: " + ex.Message, "Billing & Bank", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return 0;
                }
                catch (Exception ex1)
                {
                    MessageBox.Show("Error creating Payment Method: Credit Card (Rollback)\n\nError details: " + ex1.Message, "Billing & Bank", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return 0;
                }
            }
        }
        public static void create_new_user(Hashtable htdata, int companyid, int siteid, int memberid, int memberNo)
        {
            Create_User newuser = new Create_User();
            Paychoice_Parameter parameters = new Paychoice_Parameter();
            Hashtable htmemeberid = new Hashtable();
            Hashtable htpaychoicecustomer = new Hashtable();

            DataManager dal = new DataManager();
            //while (!dal._ConnOpen)
            //{
            //    dal = new DataManager();
            //}

            string account_number_reference;// = "E7833123";
            string user_procedure_name = "AddPayChoiceCustomer";

            newuser.business_name = htdata["First_Name"].ToString() + " " + htdata["Last_Name"].ToString();
            account_number_reference = "C" + companyid.ToString("00") + "-S" + siteid.ToString("00") + "-M" + memberNo.ToString("00000");

            if (card_identifier == 1)
            {
                newuser.account_number = account_number_reference;
            }
            else
            {
                //newuser.account_number = htdata["BankAccount_No"].ToString();
                newuser.account_number = account_number_reference;
            }

            newuser.first_name = htdata["First_Name"].ToString();
            newuser.last_name = htdata["Last_Name"].ToString();
            newuser.email = htdata["Email"].ToString();
            newuser.phone = htdata["Phone_No"].ToString();
            newuser.mobile = htdata["Mobile_No"].ToString();
            newuser.address_line1 = htdata["Address"].ToString();
            newuser.address_suburb = htdata["Suburb"].ToString();
            newuser.address_state = htdata["State"].ToString();
            newuser.address_postcode = htdata["PostalCode"].ToString();
            newuser.address_country = htdata["Country"].ToString();

            var create = parameters.create_user(newuser);
            customer_id = create.id;
            try
            {
                htpaychoicecustomer.Add("@AppMemberID", memberid);
                htpaychoicecustomer.Add("@AppSiteID", siteid);
                htpaychoicecustomer.Add("@CustomerGUID", customer_id);
                htpaychoicecustomer.Add("@AppCompanyID", companyid);
                int addpaychoiceuser = dal.ExecuteNonQueryProcedure(user_procedure_name, htpaychoicecustomer);
            }
            catch (Exception ex)
            { failuremessage(); }
        }
        //Create a new bank account for a customer
        public static void create_new_bank_account(Hashtable htdata, string paymentmode)
        {
            Create_Bank_Account bankaccount = new Create_Bank_Account();
            Paychoice_Parameter parmeters = new Paychoice_Parameter();
            string bsbnumber = htdata["BSB_No"].ToString();
            string payee_name = htdata["Payer_Name"].ToString();
            string bankaccountnumber = htdata["BankAccount_No"].ToString();
            bankaccount.bank_name = payee_name;
            bankaccount.bank_bsb = bsbnumber;
            bankaccount.bank_number = bankaccountnumber;

            var create = parmeters.create_bank_account(bankaccount, customer_id);

            bank_token = create.bank_token;
            try
            {
                Hashtable htpaychoicebank = new Hashtable();

                DataManager dal = new DataManager();
                //while (!dal._ConnOpen)
                //{
                //    dal = new DataManager();
                //}

                string bank_procedure = "AddPayChoicePayMode_Bank";
                htpaychoicebank.Add("@CustomerGUID", customer_id);
                htpaychoicebank.Add("@PayMode", paymentmode);
                htpaychoicebank.Add("@tokenGUID", bank_token);
                htpaychoicebank.Add("@bsb", bsbnumber);
                htpaychoicebank.Add("@payname", payee_name);
                htpaychoicebank.Add("@accountno", bankaccountnumber);
                int addpaychoicebank = dal.ExecuteNonQueryProcedure(bank_procedure, htpaychoicebank);
            }
            catch (Exception ex) { failuremessage(); }
        }

        //Set bank account as default payment method 
        public static void set_default_payment_method(Hashtable htdata)
        {
            Paychoice_Parameter parmeters = new Paychoice_Parameter();
            var set = parmeters.set_payment_method(customer_id, bank_token);
        }

        //Create new subscription
        public static void create_new_subscription(Hashtable htdata)
        {
            try
            {
                User_Created uc = new User_Created();
                Create_Subscription subscription = new Create_Subscription();
                Paychoice_Parameter parmeters = new Paychoice_Parameter();
                int Minimum_Payment_Duration = Int32.Parse(htdata["Billing_Cycle_Min_Length"].ToString());
                int Minimum_Payment_Duration_Fortnight = Minimum_Payment_Duration;

                //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017
                int _msUniqueID = Int32.Parse(htdata["msUniqueID"].ToString());
                //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017


                double amount = double.Parse(htdata["Amount"].ToString());
                //double billingamount = amount / Minimum_Payment_Duration;
                double billingamount = amount;
                billing_cycle_unit = htdata["Billing_Cycle_Unit"].ToString();
                billing_cycle_length = Int32.Parse(htdata["Billing_Cycle_Length"].ToString());

                billing_cycle_unit_Fortnight = billing_cycle_unit;
                billing_cycle_length_Fortnight = billing_cycle_length;
                Minimum_Payment_Duration_Fortnight = Minimum_Payment_Duration;

                if (billing_cycle_unit == "Fortnight")
                {
                    billing_cycle_unit_Fortnight = "Weeks";
                    billing_cycle_length_Fortnight = billing_cycle_length * 2;
                    Minimum_Payment_Duration_Fortnight = Minimum_Payment_Duration * 2;
                }

                subscription.subscription_name = htdata["Subscription_Name"].ToString();
                subscription.customer_id = customer_id;
                subscription.memebership_id = "";
                //subscription.start_date = DateTime.Now.ToString("yyyy-MM-dd");
                DateTime dTime = DateTime.Parse(htdata["Subscription_Start_Date"].ToString());
                subscription.start_date = dTime.ToString("yyyy-MM-dd");

                subscription.currency = htdata["Currency"].ToString();
                subscription.setup_fee = double.Parse(htdata["Set_Up_Fee"].ToString());
                subscription_amount = billingamount;
                subscription.billing_cycle_amount = billingamount;

                if (billing_cycle_unit == "Fortnight")
                {
                    subscription.billing_cycle_length = billing_cycle_length_Fortnight;
                    subscription.billing_cycle_unit = billing_cycle_unit_Fortnight;
                    subscription.billing_cycle_min_length = Minimum_Payment_Duration_Fortnight;
                }
                else
                {
                    subscription.billing_cycle_length = billing_cycle_length;
                    subscription.billing_cycle_unit = billing_cycle_unit;
                    subscription.billing_cycle_min_length = Minimum_Payment_Duration;
                }

                subscription.cacellation_fee = double.Parse(htdata["Cancellation_Fee"].ToString());
                subscription.charge_remain = false;
                var create = parmeters.create_subscription(subscription);
                if (create != null)
                {
                    subscription_id = create.id;

                    if (subscription_id != null)
                    {
                        Paychoice_Parameter parameter = new Paychoice_Parameter();

                        DataManager dal = new DataManager();
                        //while (!dal._ConnOpen)
                        //{
                        //    dal = new DataManager();
                        //}

                        string insert_schedule = "SUCCESS";

                        if (insert_schedule.ToUpper() == "SUCCESS")
                        {
                            Hashtable htupdatemembershipsubscription = new Hashtable();
                            htupdatemembershipsubscription.Add("@subscriptionguid", subscription_id);
                            htupdatemembershipsubscription.Add("@MemberNo", Int32.Parse(htdata["App_Member_id"].ToString()));
                            htupdatemembershipsubscription.Add("@SiteID", Int32.Parse(htdata["Site_Id"].ToString()));
                            htupdatemembershipsubscription.Add("@CompanyID", Int32.Parse(htdata["Company_Id"].ToString()));
                            htupdatemembershipsubscription.Add("@Programme", Int32.Parse(htdata["Programme_id"].ToString()));
                            //update SET-UP FEE ALONG WITH SUBSCRIPTION_ID::RR:18/07
                            htupdatemembershipsubscription.Add("@SetupFee", subscription.setup_fee);
                            htupdatemembershipsubscription.Add("@FullCost", subscription.billing_cycle_amount);
                            //update SET-UP FEE ALONG WITH SUBSCRIPTION_ID::RR:18/07

                            int updatesubscriptioninmemberships = dal.ExecuteNonQueryProcedure("UpdateSubscriptionGUIDinMemberships", htupdatemembershipsubscription);
                            htupdatemembershipsubscription.Clear();


                            Hashtable htpaychoicesubscription = new Hashtable();
                            Payment_Schedule = billing_cycle_unit;

                            string subscription_procedure = "AddPayChoiceSubscription";
                            htpaychoicesubscription.Add("@customerGUID", customer_id);
                            htpaychoicesubscription.Add("@subscriptionGuid", create.id);
                            htpaychoicesubscription.Add("@subscriptionname", subscription.subscription_name);
                            htpaychoicesubscription.Add("@archeived", create.archived);
                            htpaychoicesubscription.Add("@amount", subscription.billing_cycle_amount);
                            htpaychoicesubscription.Add("@payschedule", Payment_Schedule);
                            htpaychoicesubscription.Add("@minpayschedule", billing_cycle_unit);

                            htpaychoicesubscription.Add("@cancelamount", subscription.cacellation_fee);
                            htpaychoicesubscription.Add("@chargeremain", subscription.charge_remain);
                            htpaychoicesubscription.Add("@currency", subscription.currency);
                            htpaychoicesubscription.Add("@fixedterm", create.fixed_term);
                            htpaychoicesubscription.Add("@startdate", create.start_date);
                            htpaychoicesubscription.Add("@status", create.status);

                            htpaychoicesubscription.Add("@payscheduleUnit", billing_cycle_length);
                            htpaychoicesubscription.Add("@minpayscheduleUnit", Minimum_Payment_Duration);

                            //ADD COLUMN - SETUPFEE & PARAM, UPDATE PAYCHOICE_SUBSCRIPTION:RR:18/07/2016
                            htpaychoicesubscription.Add("@SignUpFee", subscription.setup_fee);
                            //ADD COLUMN - SETUPFEE & PARAM, UPDATE PAYCHOICE_SUBSCRIPTION:RR:18/07/2016

                            //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017
                            htpaychoicesubscription.Add("@msUniqueID", _msUniqueID);
                            //ADD COLUMN - msUNIQUEID - UPDATE PAYCHOICE_SUBSCRIPTION:RR:22/JUN/2017

                            int addsubscription = dal.ExecuteNonQueryProcedure(subscription_procedure, htpaychoicesubscription);
                        }

                        insert_schedule = parameter.Retreive_Payment_Schedule_Items(subscription_id, customer_id);
                    }
                }
            }
            catch (Exception) { }
        }
    
        
        //Add new payment schedule
        public static void add_new_payment_schedule()
        {
            try
            {
                Add_Payment_Schedule payschedule = new Add_Payment_Schedule();
                Paychoice_Parameter parmeters = new Paychoice_Parameter();
                payschedule.amount = 50;
                payschedule.schedule_date = DateTime.Now.ToString("yyyy-MM-dd");
                string add = parmeters.add_payment_schedule(payschedule);
            }
            catch (Exception ex) { }
        }
        //Create a new credit card for a customer
        public static void create_new_credit_card(Hashtable htdata, string paymentmode)
        {
            Create_Credit_Card card = new Create_Credit_Card();
            Paychoice_Parameter parmeters = new Paychoice_Parameter();
            card.card_name = htdata["CreditCard_Name"].ToString();
            card.card_number = htdata["CreditCard_No"].ToString();
            card.card_expiry_month = htdata["Expiry_Month"].ToString();
            string year_expired = htdata["Expiry_Year"].ToString();
            card.card_expiry_year = year_expired.Substring(year_expired.Length - 2);
            card.card_cvv = htdata["Cvv_No"].ToString();

            var create = parmeters.create_credit_card(card, customer_id);
            token = create.token;
            try
            {
                Hashtable htpaychoicecard = new Hashtable();

                DataManager dal = new DataManager();
                //while (!dal._ConnOpen)
                //{
                //    dal = new DataManager();
                //}

                string card_proceedure = "AddPayChoicePayMode_Card";
                htpaychoicecard.Add("@CustomerGUID", customer_id);
                htpaychoicecard.Add("@PayMode", paymentmode);
                htpaychoicecard.Add("@tokenGUID", token);
                htpaychoicecard.Add("@payname", card.card_name);
                htpaychoicecard.Add("@cardnummasked", create.masked_number);
                htpaychoicecard.Add("@cardtype", create.card_type);
                htpaychoicecard.Add("@cardexpirymonth", card.card_expiry_month);
                htpaychoicecard.Add("@cardexpiryyear", card.card_expiry_year);
                int addpaychoicecard = dal.ExecuteNonQueryProcedure(card_proceedure, htpaychoicecard);
            }
            catch (Exception ex) { }
        }
        //set credit card as a default payment method
        //cc-credit card
        public static void set_cc_default_payment_method(Hashtable htdata)
        {
            Paychoice_Parameter parmeters = new Paychoice_Parameter();

            var set = parmeters.set_cc_payment_method(customer_id, token);
            string test = set.token;
        }

        public static int CreateSubscriptionforExistingCustomer(string CustomerGuid, string selected_Payment_Mode, Hashtable htsubscriptiondata)
        {
            try
            {
                customer_id = CustomerGuid;

                DataManager dal = new DataManager();
                //while (!dal._ConnOpen)
                //{
                //    dal = new DataManager();
                //}

                Paychoice_Parameter parameter = new Paychoice_Parameter();
                Hashtable htvalidatepaymentmethode = new Hashtable();
                Create_Subscription subscription = new Create_Subscription();
                int updatesubscriptioninmemberships = 0;
                if (selected_Payment_Mode == "Bank Account")
                {
                    htvalidatepaymentmethode.Add("@CustomerGUID", CustomerGuid);
                    htvalidatepaymentmethode.Add("@paymentmode", selected_Payment_Mode);
                    DataSet dsvalidatebankaccount = dal.ExecuteProcedureReader("ValidatePaymentMode", htvalidatepaymentmethode);
                    htvalidatepaymentmethode.Clear();
                    if (dsvalidatebankaccount != null)
                    {
                        DataTable dtvalidatebankaccount = dsvalidatebankaccount.Tables[0];
                        if (dtvalidatebankaccount.Rows.Count > 0)
                        {
                            create_new_subscription(htsubscriptiondata);
                        }
                        else
                        {
                            create_new_bank_account(htsubscriptiondata, "Bank Account");
                            set_default_payment_method(htsubscriptiondata);
                            create_new_subscription(htsubscriptiondata);
                        }
                    }
                }
                else if (selected_Payment_Mode == "Credit Card")
                {
                    htvalidatepaymentmethode.Add("@CustomerGUID", CustomerGuid);
                    htvalidatepaymentmethode.Add("@paymentmode", selected_Payment_Mode);
                    DataSet dsvalidatebankaccount = dal.ExecuteProcedureReader("ValidatePaymentMode", htvalidatepaymentmethode);
                    htvalidatepaymentmethode.Clear();
                    if (dsvalidatebankaccount != null)
                    {
                        DataTable dtvalidatecreditcard = dsvalidatebankaccount.Tables[0];
                        if (dtvalidatecreditcard.Rows.Count > 0)
                        {
                            create_new_subscription(htsubscriptiondata);
                        }
                        else
                        {
                            create_new_credit_card(htsubscriptiondata, "Credit Card");
                            set_cc_default_payment_method(htsubscriptiondata);
                            create_new_subscription(htsubscriptiondata);
                        }
                    }
                    if (updatesubscriptioninmemberships == 1)
                    {
                        MessageBox.Show("Subscription Added Successfully");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in Adding Subscription" + ex.Message);
            }
            return 0;
        }
        public static string Delete_Payment_Schedule(string subscription_id, DateTime startdate)
        {
            try
            {
                string _result = "";
                Paychoice_Parameter parmeters = new Paychoice_Parameter();
                var getschedule = parmeters.Cancel_Payment_Schedule(subscription_id, startdate);
                if (getschedule == "Success")
                {
                    _result = "The Payment schedule has been deleted from " + startdate + " Successfully";
                }
                else
                {
                    _result = getschedule;
                }
                return _result;
            }
            catch (Exception ex)
            {
                return "Error Cannot change now Try after sometime\n" + ex.Message;
            }
        }
        public static string Skip_Delete_Single_Payment_Schedule(string subscriptionguid, string payment_schedule_id)
        {
            try
            {
                string _result = "";
                Paychoice_Parameter parmeters = new Paychoice_Parameter();
                var getschedule = parmeters.Skip_Delete_Single_Payment_Schedule(subscriptionguid, payment_schedule_id);
                if (getschedule == "Success")
                {
                    _result = getschedule;
                }
                else
                {
                    _result = getschedule;
                }
                return _result;
            }
            catch (Exception ex)
            {
                return "Error Cannot change now Try after sometime\n" + ex.Message;
            }
        }

        public static string Void_Dishonoured_Invoice_with_Alternate_Payment(string payment_schedule_id, string subscriptionguid, string _customerGUID)
        {
            try
            {
                string _result = "";
                Paychoice_Parameter parmeters = new Paychoice_Parameter();
                var getschedule = parmeters.Void_Dishonoured_Invoice_with_Alternate_Payment(payment_schedule_id, subscriptionguid, _customerGUID);
                if (getschedule == "Success")
                {
                    _result = getschedule;
                }
                else
                {
                    _result = getschedule;
                }
                return _result;
            }
            catch (Exception ex)
            {
                return "Error Cannot change now Try after sometime\n" + ex.Message;
            }
        }

        public static string Pay_Dishonoured_Invoice_with_Alternate_Payment(string payment_schedule_id, string subscriptionguid, string _customerGUID, double _amount)
        {
            try
            {
                string _result = "";
                Paychoice_Parameter parmeters = new Paychoice_Parameter();
                var getschedule = parmeters.Pay_Dishonoured_Invoice_with_Alternate_Payment(payment_schedule_id, subscriptionguid, _customerGUID, _amount);
                if (getschedule == "Success")
                {
                    _result = getschedule;
                }
                else
                {
                    _result = getschedule;
                }
                return _result;
            }
            catch (Exception ex)
            {
                return "Error Cannot change now Try after sometime\n" + ex.Message;
            }
        }

        public static string Amend_Single_Payment_Schedule(string subscriptionguid, string payment_schedule_id, double amount)
        {
            try
            {
                string _result = "";
                Paychoice_Parameter parmeters = new Paychoice_Parameter();
                var getschedule = parmeters.Amend_Single_Payment_Schedule(subscriptionguid, payment_schedule_id, amount);
                if (getschedule == "Success")
                {
                    _result = getschedule;
                }
                else
                {
                    _result = getschedule;
                }
                return _result;
            }
            catch (Exception ex)
            {
                return "Error Cannot change now Try after sometime\n" + ex.Message;
            }
        }
        public static void Delete_Paychoice_Customer(string customer_guid)
        {
            Paychoice_Parameter parmeters = new Paychoice_Parameter();
            var set = parmeters.Delete_Customer(customer_guid);
        }
        public static string Archive_Subscription(string subscriptionguid)
        {
            try
            {
                string _result = "";
                Paychoice_Parameter parmeters = new Paychoice_Parameter();
                var getschedule = parmeters.Archive_Subscription(subscriptionguid);
                if ((getschedule.ToUpper() == "SUCCESS") || (getschedule.ToUpper() == "TRUE"))
                {
                    _result = "SUCCESS";
                }
                else
                {
                    _result = getschedule;
                }
                return _result;
            }
            catch (Exception ex)
            {
                //return "Error Cannot change now Try after sometime\n" + ex.Message;
                string _res = "Fail " + ex.Message;
                return _res;
            }
        }
        public static string Edit_Payment_Schedule(string subscription_id, DateTime startdate, DateTime enddate, Double amount)
        {
            try
            {
                string _result = "";
                Paychoice_Parameter parmeters = new Paychoice_Parameter();

                var getschedule = parmeters.Modify_Payment_Schedule(subscription_id, startdate, enddate, amount);

                if (getschedule == "Success")
                {
                    _result = "The Payment schedule has been modified between " + startdate.Date.ToString("dd/MMM/yyyy") + " and " + enddate.Date.ToString("dd/MMM/yyyy");
                    //MessageBox.Show("The Payment schedule has been modified between " + startdate + " and " + enddate + " Successfully");
                }
                else
                {
                    _result = getschedule;
                }
                return _result;
            }
            catch (Exception ex)
            {
                string _result;
                //MessageBox.Show("Error Cannot change now Try after sometime\n" + ex.Message);
                _result = "Error Cannot change now Try after sometime\n" + ex.Message;
                return _result;
            }
        }
        public static int Update_Status_PaychoiceScheduledItems(string subscriptionguid, string _customerGUID, DateTime _prgEndDate, string _State = "ACTIVE", bool _EXPFLAG = false)
        {
            Paychoice_Parameter parameter = new Paychoice_Parameter();
            var updatestatus = "";
            if (_EXPFLAG)
            {
                updatestatus = parameter.Update_Payment_Schedule_Items_EXPIRED_PREVSTATUS(subscriptionguid, _customerGUID, _prgEndDate);
            }
            else
            {
                if (_State.Trim().ToUpper() == "ACTIVE")
                {
                    updatestatus = parameter.Update_Payment_Schedule_Items(subscriptionguid, _customerGUID);
                }
                else
                {
                    updatestatus = parameter.Update_Payment_Schedule_Items_EXPIRED(subscriptionguid, _customerGUID, _prgEndDate);
                }
                if (updatestatus == "Success")
                {
                    //MessageBox.Show("Payment Schedule Status updated");
                }
                else
                {
                    //MessageBox.Show("Problem in updating payment schedule status");
                }
            }
            return 0;
        }

        //Update Existing Bank Details
        public static void Update_new_bank_account(Hashtable htdata, string paymentmode, string _customerGUID)
        {
            Create_Bank_Account bankaccount = new Create_Bank_Account();
            Paychoice_Parameter parmeters = new Paychoice_Parameter();
            string bsbnumber = htdata["BSB_No"].ToString();
            string payee_name = htdata["Payer_Name"].ToString();
            string bankaccountnumber = htdata["BankAccount_No"].ToString();
            bankaccount.bank_name = payee_name;
            bankaccount.bank_bsb = bsbnumber;
            bankaccount.bank_number = bankaccountnumber;

            var create = parmeters.create_bank_account(bankaccount, _customerGUID);
            bank_token = create.bank_token;

            try
            {
                if (bank_token != null)
                {
                    Update_default_payment_method(_customerGUID, bank_token);

                    Hashtable htpaychoicebank = new Hashtable();

                    DataManager dal = new DataManager();
                    //while (!dal._ConnOpen)
                    //{
                    //    dal = new DataManager();
                    //}

                    string bank_procedure = "UpdatePayChoicePayMode_Bank";
                    htpaychoicebank.Add("@CustomerGUID", _customerGUID);
                    htpaychoicebank.Add("@PayMode", paymentmode);
                    htpaychoicebank.Add("@tokenGUID", bank_token);
                    htpaychoicebank.Add("@bsb", bsbnumber);
                    htpaychoicebank.Add("@payname", payee_name);
                    htpaychoicebank.Add("@accountno", bankaccountnumber);
                    int addpaychoicebank = dal.ExecuteNonQueryProcedure(bank_procedure, htpaychoicebank);
                }
            }
            catch (Exception ex) { failuremessage(); }
        }

        //Set Default for Updated Bank details
        public static void Update_default_payment_method(string _customerGUID, string _bank_Token)
        {
            Paychoice_Parameter parmeters = new Paychoice_Parameter();
            var set = parmeters.set_payment_method(_customerGUID, _bank_Token);
        }

        //Set default for Updated CC
        public static void Update_cc_default_payment_method(string _customerGUID, string _bank_Token)
        {
            Paychoice_Parameter parmeters = new Paychoice_Parameter();

            var set = parmeters.set_cc_payment_method(_customerGUID, _bank_Token);
            string test = set.token;
        }

        public static void Remove_Old_CreditCard(string _customerGUID, string _oldCCTokenGUID)
        {
            Paychoice_Parameter parmeters = new Paychoice_Parameter();
            var set = parmeters.Remove_Credit_Card(_customerGUID, _oldCCTokenGUID);
        }

        //Update Existing CC details
        public static void Update_new_credit_card(Hashtable htdata, string paymentmode, string _customerGUID, string _OldCardTokenGUID = "")
        {
            Create_Credit_Card card = new Create_Credit_Card();
            Paychoice_Parameter parmeters = new Paychoice_Parameter();
            card.card_name = htdata["CreditCard_Name"].ToString();
            card.card_number = htdata["CreditCard_No"].ToString();
            card.card_expiry_month = htdata["Expiry_Month"].ToString();
            string year_expired = htdata["Expiry_Year"].ToString();
            card.card_expiry_year = year_expired.Substring(year_expired.Length - 2);
            card.card_cvv = htdata["Cvv_No"].ToString();

            var create = parmeters.create_credit_card(card, _customerGUID);
            token = create.token;

            try
            {
                if (token != null)
                {
                    Update_cc_default_payment_method(_customerGUID, token);
                    if (_OldCardTokenGUID.Trim() != "")
                    {
                        Remove_Old_CreditCard(_customerGUID, _OldCardTokenGUID);
                    }

                    Hashtable htpaychoicecard = new Hashtable();

                    DataManager dal = new DataManager();
                    //while (!dal._ConnOpen)
                    //{
                    //    dal = new DataManager();
                    //}

                    string card_proceedure = "UpdatePayChoicePayMode_Card";
                    htpaychoicecard.Add("@CustomerGUID", _customerGUID);
                    htpaychoicecard.Add("@PayMode", paymentmode);
                    htpaychoicecard.Add("@tokenGUID", token);
                    htpaychoicecard.Add("@payname", card.card_name);
                    htpaychoicecard.Add("@cardnummasked", create.masked_number);
                    htpaychoicecard.Add("@cardtype", create.card_type);
                    htpaychoicecard.Add("@cardexpirymonth", card.card_expiry_month);
                    htpaychoicecard.Add("@cardexpiryyear", card.card_expiry_year);
                    int addpaychoicecard = dal.ExecuteNonQueryProcedure(card_proceedure, htpaychoicecard);
                }
            }
            catch (Exception ex) { }
        }
        public static double Retrieve_Customer_Balance_pc (string _customerGUID)
        {
            Paychoice_Parameter parameter = new Paychoice_Parameter();
            return parameter.Retrieve_Customer_Balance_pc_param(_customerGUID);
        }
    }
}
