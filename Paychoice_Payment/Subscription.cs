﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Paychoice_Payment
{
   public  class Subscription
    {
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("archived")]
        public string archived { get; set; }
        [JsonProperty("fixed_term")]
        public string fixed_term { get; set; }
        [JsonProperty("start_date")]
        public string start_date { get; set; }
        [JsonProperty("status")]
        public string status { get; set; }
        
    }
}
