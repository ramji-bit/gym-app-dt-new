﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Paychoice_Payment
{
    public class User_Created
    {
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("account_balance")]
        public double account_balance { get; set; }
        public double subscription_balance { get; set; }
    }
}
