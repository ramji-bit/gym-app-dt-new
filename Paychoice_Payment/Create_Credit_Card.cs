﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paychoice_Payment
{
    public class Create_Credit_Card
    {
        public string card_name { get; set; }
        public string card_number { get; set; }
        public string card_expiry_month { get; set; }
        public string card_expiry_year { get; set; }
        public string card_cvv { get; set; }
    }
}
