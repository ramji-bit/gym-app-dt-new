﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Collections;
using System.Windows;
using System.Configuration;
using System.Security.Cryptography;
using System.IO;

namespace Paychoice_Payment
{
    ///<summary>  
    /// SqlServerDAL class is the interface to the SQL Server database. All required methods for accessing the  
    /// SQL database will be provided in this class and hence acts as the Data Access Layer (DAL) between  
    /// front-end application and the SQL Server database.  
    ///</summary> 
    public class DataManager
    {
        ///<summary>  
        /// objSqlServerDAL is the single instance variable for this class  
        ///</summary>  
        private static DataManager objSqlServerDAL = null;

        ///<summary>  
        /// The object which holds the sql connection  
        ///</summary>  
        private SqlConnection objSqlConnection = null;

        ///<summary>  
        /// The constructor should be a private method for a singleton class. This constructor  
        /// will initialize the sql connection  
        ///</summary>  
        ///<param name="connString">connection string variable  
        string ConnectionString = string.Empty;
        public const string _EncryptKey = "GymAppElumina";
        public static bool _OfflineDB = false;
        public bool _ConnOpen = false;
        public DataManager()
        {
            try
            {
                string UserID;
                string Password;
                if (ConfigurationManager.AppSettings["OfflineFlag"].ToString().ToUpper() == "FALSE")
                {
                    UserID = ConfigurationManager.AppSettings["DBUser"].ToString();
                    Password = passwordDecrypt(ConfigurationManager.AppSettings["DBPassword"].ToString(), _EncryptKey);
                    ConnectionString = ConfigurationManager.ConnectionStrings["GymMembershipDB"].ConnectionString;
                    ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;
                }
                else
                {
                    //UserID = ConfigurationManager.AppSettings["OfflineDBUser"].ToString();
                    //Password = passwordDecrypt(ConfigurationManager.AppSettings["OfflineDBPassword"].ToString(), _EncryptKey);
                    //ConnectionString = ConfigurationManager.ConnectionStrings["offlineDB"].ConnectionString;
                    //ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;

                    UserID = ConfigurationManager.AppSettings["DBUser"].ToString();
                    Password = passwordDecrypt(ConfigurationManager.AppSettings["DBPassword"].ToString(), _EncryptKey);
                    ConnectionString = ConfigurationManager.ConnectionStrings["GymMembershipDB"].ConnectionString;
                    ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;
                }
                objSqlConnection = new SqlConnection(ConnectionString);
                objSqlConnection.Open();

                if (objSqlConnection.State == ConnectionState.Closed)
                {
                    //UserID = ConfigurationManager.AppSettings["OfflineDBUser"].ToString();
                    //Password = passwordDecrypt(ConfigurationManager.AppSettings["OfflineDBPassword"].ToString(), _EncryptKey);
                    //ConnectionString = ConfigurationManager.ConnectionStrings["offlineDB"].ConnectionString;
                    //ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;
                    //objSqlConnection = new SqlConnection(ConnectionString);
                    //_OfflineDB = true;

                    UserID = ConfigurationManager.AppSettings["DBUser"].ToString();
                    Password = passwordDecrypt(ConfigurationManager.AppSettings["DBPassword"].ToString(), _EncryptKey);
                    ConnectionString = ConfigurationManager.ConnectionStrings["GymMembershipDB"].ConnectionString;
                    ConnectionString = ConnectionString + "; User ID=" + UserID + ";Password=" + Password;

                    objSqlConnection = new SqlConnection(ConnectionString);
                    objSqlConnection.Open();

                    if (objSqlConnection.State == ConnectionState.Closed)
                    {
                        _ConnOpen = false;
                    }
                    else
                    {
                        _ConnOpen = true;
                    }
                }
                else
                {
                    _ConnOpen = true;
                }
            }
            catch(Exception)
            {
                _ConnOpen = false;
            }
        }
        public bool GetConnOpen()
        {
            return _ConnOpen;
        }
        public static DataManager GetInstance(string connString)
        {
            if (objSqlServerDAL == null)
            {
                objSqlServerDAL = new DataManager();
            }

            return objSqlServerDAL;
        }

        ///<summary>  
        /// Open method will open the current instance of the connection object if the object is in closed state  
        ///</summary>  
        public void Open()
        {
            try
            {
                if (objSqlConnection.State == ConnectionState.Closed)
                {
                    objSqlConnection.Open();
                }
            }
            catch (Exception e)
            {
                //throw e;
                string s = e.Message;
            }
        }

        ///<summary>  
        /// Dispose method will close and dispose the connection object that was created earlier.  
        ///</summary>  
        public void Dispose()
        {
            try
            {
                if (objSqlConnection.State != ConnectionState.Closed)
                {
                    this.objSqlConnection.Close();
                    //this.objSqlConnection.Dispose();  
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        ///<summary>  
        /// ExecuteProcedureReader is the method by which a Stored Procedure is called with the list of  
        /// parameters and its values. This method will execute the stored proc and returns the resultset  
        /// on successfull completion, else throws the error.  
        ///</summary>  
        ///<param name="StoredProcName">Name of the Stored Proc that to be executed  
        ///<param name="parameterList">All required parameters in the Hashtable format with the format  
        ///                                 - @OUT_ prefix for output parameters  
        ///                                 - @IN_ prefix for input parameters  
        ///  
        ///<returns>The resultant dataset</returns>  
        public DataSet ExecuteProcedureReader(string StoredProcName, Hashtable parameterList)
        {
            //string connectionstring = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
            DataSet objresultSet = new DataSet();
            SqlDataAdapter objSqlDataAdapter = null;
            int counter = 0;

            try
            {
                SqlCommand objSqlCommand = new SqlCommand(StoredProcName, objSqlConnection);
                objSqlCommand.CommandType = CommandType.StoredProcedure;

                if (parameterList != null)
                {
                    foreach (string parametername in parameterList.Keys)
                    {
                        objSqlCommand.Parameters.AddWithValue(parametername, parameterList[parametername]);

                        if (parametername.StartsWith("@OUT_"))
                        {
                            objSqlCommand.Parameters[counter].Direction = ParameterDirection.Output;
                        }
                        else
                        {
                            objSqlCommand.Parameters[counter].Direction = ParameterDirection.Input;
                        }

                        counter++;
                    }
                }

                Open();
                objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);

                objSqlDataAdapter.Fill(objresultSet);
                Dispose();
                return objresultSet;
            }
            catch (Exception e)
            {
                //throw e;
                return null;
            }


        }

        ///<summary>  
        /// ExecuteNonQueryProcedure is the method by which a Stored Procedure is called with the list of  
        /// parameters and its values. This method will execute the stored proc and returns the status of  
        /// on completion. Any exceptions during the process will throws the error.  
        ///</summary>  
        ///<param name="StoredProcName">Name of the Stored Proc that to be executed  
        ///<param name="parameterList">All required parameters in the Hashtable format with the format  
        ///                                 - @OUT_ prefix for output parameters  
        ///                                 - @IN_ prefix for input parameters  
        ///  
        ///<returns>The result of the Stored Proc execution</returns>  
        public int ExecuteNonQueryProcedure(string StoredProcName, Hashtable parameterList)
        {
            int returnValue = 0;
            int counter = 0;

            try
            {
                SqlCommand objSqlCommand = new SqlCommand(StoredProcName, objSqlConnection);
                objSqlCommand.CommandType = CommandType.StoredProcedure;

                if (parameterList != null)
                {
                    foreach (string parametername in parameterList.Keys)
                    {
                        objSqlCommand.Parameters.AddWithValue(parametername, parameterList[parametername]);

                        if (parametername.StartsWith("@OUT_"))
                        {
                            objSqlCommand.Parameters[counter].Direction = ParameterDirection.Output;
                        }
                        else
                        {
                            objSqlCommand.Parameters[counter].Direction = ParameterDirection.Input;
                        }

                        counter++;
                    }
                }
                Open();
                returnValue = objSqlCommand.ExecuteNonQuery();
                Dispose();

            }
            catch (Exception e)
            {
                throw e;
            }
            return returnValue;
        }
        public static string passwordDecrypt(string cryptTxt, string key)
        {
            //Decrypting a string
            cryptTxt = cryptTxt.Replace(" ", "+");
            byte[] bytesBuff = Convert.FromBase64String(cryptTxt);
            using (Aes aes = Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                aes.Key = crypto.GetBytes(32);
                aes.IV = crypto.GetBytes(16);
                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }
                    cryptTxt = Encoding.Unicode.GetString(mStream.ToArray());
                }
            }
            return cryptTxt;
        }
    }
}
