﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Paychoice_Payment
{
    public class Account_Created
    {
        [JsonProperty("bank_token")]
        public string bank_token { get; set; }
    }
}
