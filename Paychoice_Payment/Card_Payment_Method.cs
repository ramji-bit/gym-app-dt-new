﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Paychoice_Payment
{
    public class Card_Payment_Method
    {
        [JsonProperty("token")]
        public string token { get; set; }
        [JsonProperty("customer_id")]
        public string customer_id { get; set; }

    }
}
