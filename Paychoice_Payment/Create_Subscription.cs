﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paychoice_Payment
{
    public class Create_Subscription
    {
        public string subscription_name { get; set; }
        public string customer_id { get; set; }
        public string memebership_id { get; set; }
        public string start_date { get; set; }
        public string currency { get; set; }
        public double setup_fee { get; set; }
        public double trial_period_amount { get; set; }
        public int trial_period_length { get; set; }
        public string trial_period_unit { get; set; }
        public double billing_cycle_amount { get; set; }
        public int billing_cycle_length { get; set; }
        public string billing_cycle_unit { get; set; }
        public int billing_cycle_min_length { get; set; }
        public bool fixed_term { get; set; }
        public double cacellation_fee { get; set; }
        public bool charge_remain { get; set; }
    }
}
