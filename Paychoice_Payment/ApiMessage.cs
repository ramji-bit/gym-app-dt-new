﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Paychoice_Payment
{
    public class ApiMessage
    {
        [JsonProperty("default_payment_method")]
        public Card_Payment_Method default_payment_method { get; set; }
        [JsonProperty("customer")]
        public User_Created customer { get; set; }
        [JsonProperty("bank")]
        public Account_Created bank { get; set; }
        [JsonProperty("subscription")]
        public Subscription  subscription { get; set; }
        [JsonProperty("card")]
        public Card_Created card { get; set; }
        [JsonProperty("payment_schedule_list")]
        public List<PaymentScheduleList> payment_schedule_list { get; set; }
        public bool HasError
        {
            get
            {
                return Error != null;
            }
        }
        [JsonProperty("error")]
        public Error Error { get; set; }
        [JsonProperty("Invoice")]
        public Invoice invoice { get; set; }
        public List<SubscriptionList> subscription_list { get; set; }
        public PaymentSchedule payment_schedule { get; set; }
    }
}
