﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paychoice_Payment
{
    public class Create_Bank_Account
    {
        public string bank_name { get; set; }
        public string bank_bsb { get; set; }
        public string bank_number { get; set; }
    }
}
