﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Paychoice_Payment
{
    public class Link
    {
        [JsonProperty("href")]
        public string href { get; set; }
        [JsonProperty("rel")]
        public string rel { get; set; }
    }
}
