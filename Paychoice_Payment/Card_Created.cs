﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Paychoice_Payment
{
    public class Card_Created
    {
        [JsonProperty("token")]
        public string token { get; set; }
        [JsonProperty("masked_number")]
        public string masked_number { get; set; }
        [JsonProperty("card_type")]
        public string card_type { get; set; }
    }
}
