﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Paychoice_Payment
{
    public class LineItem
    {
        [JsonProperty("amount")]
        public double amount { get; set; }
        [JsonProperty("description")]
        public string description { get; set; }
    }
}
