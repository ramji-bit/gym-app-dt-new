﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Paychoice_Payment
{
    public class Transaction
    {
        [JsonProperty("amount")]
        public double amount { get; set; }
        [JsonProperty("approved")]
        public bool approved { get; set; }
        [JsonProperty("completed")]
        public bool completed { get; set; }
        [JsonProperty("error_code")]
        public string error_code { get; set; }
        [JsonProperty("error_description")]
        public string error_description { get; set; }
        [JsonProperty("has_error")]
        public bool has_error { get; set; }
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("payment_method")]
        public string payment_method { get; set; }
        [JsonProperty("status")]
        public string status { get; set; }
    }
}
