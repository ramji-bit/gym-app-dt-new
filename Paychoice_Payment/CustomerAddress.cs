﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Paychoice_Payment
{
    public class CustomerAddress
    {
        [JsonProperty("country")]
        public string country { get; set; }
        [JsonProperty("line1")]
        public string line1 { get; set; }
        [JsonProperty("line2")]
        public string line2 { get; set; }
        [JsonProperty("postcode")]
        public string postcode { get; set; }
        [JsonProperty("state")]
        public string state { get; set; }
        [JsonProperty("suburb")]
        public string suburb { get; set; }
    }
}
