﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Paychoice_Payment
{
    public class SubscriptionList
    {
        [JsonProperty("id")]
        public string id { get; set; }
    }
}
