﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Paychoice_Payment
{
    public class Invoice
    {
        [JsonProperty("amount_due")]
        public double amount_due { get; set; }

        [JsonProperty("amount_pending")]
        public double amount_pending { get; set; }

        [JsonProperty("invoice_date")]
        public string invoice_date { get; set; }

        [JsonProperty("customer_address")]
        public CustomerAddress customer_address { get; set; }

        [JsonProperty("business_name")]
        public string business_name { get; set; }

        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("invoice_amount")]
        public double invoice_amount { get; set; }

        [JsonProperty("line_items")]
        public List<LineItem> line_items { get; set; }

        [JsonProperty("invoice_number")]
        public string invoice_number { get; set; }

        [JsonProperty("link")]
        public Link link { get; set; }

        [JsonProperty("pending")]
        public bool pending { get; set; }

        [JsonProperty("scheduled_date")]
        public string scheduled_date { get; set; }

        [JsonProperty("subscription_id")]
        public string subscription_id { get; set; }

        [JsonProperty("status")]
        public string status { get; set; }

        [JsonProperty("payments_list")]
        public List<PaymentsList> payments_list { get; set; }
    }
}
