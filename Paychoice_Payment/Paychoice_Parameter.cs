﻿using System;
using System.Windows;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;
using System.Configuration;

namespace Paychoice_Payment
{
    public class Paychoice_Parameter
    {
        Paychoice_Request_Response gateway = new Paychoice_Request_Response();
        //string url = "https://sandbox.paychoice.com.au/api/v3/";
        //string url = "https://secure.paychoice.com.au/api/v3/";
        List<string> subscriptionId = new List<string>();

        public class InvStatus
        {
            public string _paymentStatus { get; set; }
            public string _InvStatus { get; set; }
            public double _InvAmount { get; set; }
        };

        string url = null;
        string Reason_For_Dishonour = "";
        string invoice_main_status="";

        public Paychoice_Parameter()
        {
            try
            {
                string paymentactiveportal = ConfigurationManager.AppSettings["PaychoiceUsagePortal"];
                if ((paymentactiveportal != null) && (paymentactiveportal != ""))
                {
                    if (paymentactiveportal == "Sandbox")
                    {
                        url = ConfigurationManager.AppSettings["PaychoiceSandboxURL"];
                    }
                    else
                    {
                        url = ConfigurationManager.AppSettings["PaychoiceSecuredURL"];
                    }
                }
                else
                {
                    url = "https://sandbox.paychoice.com.au/api/v3/";
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

        }
        public User_Created create_user(Create_User user)
        {
            string endpoint = "customer/";
            var endpointParams = new Dictionary<string, string>();
            endpointParams.Add("business_name", user.business_name);
            endpointParams.Add("account_number", user.account_number);
            endpointParams.Add("first_name", user.first_name);
            endpointParams.Add("last_name", user.last_name);
            endpointParams.Add("email", user.email);
            endpointParams.Add("phone", user.phone);
            endpointParams.Add("mobile", user.mobile);
            endpointParams.Add("address[line1]", user.address_line1);
            endpointParams.Add("address[suburb]", user.address_suburb);
            endpointParams.Add("address[state]", user.address_state);
            endpointParams.Add("address[postcode]", user.address_postcode);
            endpointParams.Add("address[country]", user.address_country);
            string response = gateway.Post(url + endpoint, endpointParams);
            var message = JsonConvert.DeserializeObject<ApiMessage>(response);
            if (message.HasError)
            {
                throw new Exception(message.Error.Message.ToString());

            }
            return message.customer;

        }
        public Account_Created create_bank_account(Create_Bank_Account bankaccount, string customerid)
        {
            string endpoint = "customer/" + customerid + "/bank";
            var endpointParams = new Dictionary<string, string>();
            endpointParams.Add("bank[name]", bankaccount.bank_name);
            endpointParams.Add("bank[bsb]", bankaccount.bank_bsb);
            endpointParams.Add("bank[number]", bankaccount.bank_number);
            string response = gateway.Post(url + endpoint, endpointParams);
            var message = JsonConvert.DeserializeObject<ApiMessage>(response);
            if (message.HasError)
            {
                throw new Exception(message.Error.Message.ToString());
            }
            return message.bank;
        }
        
        public Card_Payment_Method set_payment_method(string customerid, string banktoken)
        {

            string endpoint = "customer/" + customerid + "/bank/" + banktoken + "";
            string response = gateway.Post(url + endpoint, null);
            var message = JsonConvert.DeserializeObject<ApiMessage>(response);
            if (message.HasError)
            {
                throw new Exception(message.Error.Message.ToString());
            }
            return message.default_payment_method;
        }
        public Subscription create_subscription(Create_Subscription subscription)
        {
            string response = "";
            try
            {
                string endpoint = "subscription/";
                var endpointParams = new Dictionary<string, string>();
                endpointParams.Add("subscription_name", subscription.subscription_name);
                endpointParams.Add("customer_id", subscription.customer_id);
                endpointParams.Add("membership_id", subscription.memebership_id);
                endpointParams.Add("start_date", subscription.start_date);
                endpointParams.Add("currency", subscription.currency);
                endpointParams.Add("setup_fee", subscription.setup_fee.ToString());
                endpointParams.Add("billing_cycle[amount]", subscription.billing_cycle_amount.ToString());
                endpointParams.Add("billing_cycle[length]", subscription.billing_cycle_length.ToString());
                endpointParams.Add("billing_cycle[unit]", subscription.billing_cycle_unit);
                endpointParams.Add("billing_cycle[min_length]", subscription.billing_cycle_min_length.ToString());
                
                ////CANCELATION FEE DEFAULTED TO ZERO; WILL BE HANDLED IN EXPIRE MEMBERSHIP SCREEN - EMF-REDA; AS PER CLIENT C-R
                //endpointParams.Add("cancellation_fee", subscription.cacellation_fee.ToString());
                endpointParams.Add("cancellation_fee", "0");
                ////CANCELATION FEE DEFAULTED TO ZERO; WILL BE HANDLED IN EXPIRE MEMBERSHIP SCREEN - EMF-REDA; AS PER CLIENT C-R

                endpointParams.Add("charge_remain", subscription.charge_remain.ToString());
                response = gateway.Post(url + endpoint, endpointParams);
                var message = JsonConvert.DeserializeObject<ApiMessage>(response);

                if (message.HasError)
                {
                    throw new Exception(message.Error.Message.ToString());
                }
                return message.subscription;
            }
            catch (Exception)
            {
                if (response.ToUpper().StartsWith("YOU DO"))
                {
                    MessageBox.Show("Error! : Check Paychoice API user Credentials!", "Payment Details", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Error! : " + response, "Payment Details", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                return null;
            }
        }
        public string add_payment_schedule(Add_Payment_Schedule payschedule)
        {
            string endpoint = "subscription/e8265acd-e074-4497-8fd6-da77721916b3/paymentschedule/";
            var endpointParams = new Dictionary<string, string>();
            endpointParams.Add("amount", payschedule.amount.ToString());
            endpointParams.Add("schedule_date", payschedule.schedule_date);
            string response = gateway.Post(url + endpoint, endpointParams);
            //var message = JsonConvert.DeserializeObject<Paychoice_Parameter>(response.ToString());
            return response;
        }
        public Card_Created create_credit_card(Create_Credit_Card card, string customerid)
        {
            string endpoint = "customer/" + customerid + "/card";
            var endpointParams = new Dictionary<string, string>();
            endpointParams.Add("card[name]", card.card_name);
            endpointParams.Add("card[number]", card.card_number);
            endpointParams.Add("card[expiry_month]", card.card_expiry_month);
            endpointParams.Add("card[expiry_year]", card.card_expiry_year);
            endpointParams.Add("card[cvv]", card.card_cvv);
            string response = gateway.Post(url + endpoint, endpointParams);
            var message = JsonConvert.DeserializeObject<ApiMessage>(response);
            if (message.HasError)
            {
                throw new Exception(message.Error.Message.ToString());
            }
            return message.card;
        }
        //cc-credit card
        public Card_Payment_Method set_cc_payment_method(string customerid, string token)
        {
            string endpoint = "customer/" + customerid + "/card/" + token + "";
            string response = gateway.Post(url + endpoint, null);
            var message = JsonConvert.DeserializeObject<ApiMessage>(response);
            if (message.HasError)
            {
                throw new Exception(message.Error.Message.ToString());
            }
            return message.default_payment_method;
        }

        public string Retreive_Payment_Schedule_Items(string subscriptionguid, string customerguid)
        {
            try
            {

                DataManager dal = new DataManager();
                //while (!dal._ConnOpen)
                //{
                //    dal = new DataManager();
                //}

                Hashtable htschedule = new Hashtable();
                string endpoint = "subscription/" + subscriptionguid + "/paymentschedule/";
                string response = gateway.Get(url + endpoint, null);
                var message = JsonConvert.DeserializeObject<ApiMessage>(response);
                foreach (PaymentScheduleList i in message.payment_schedule_list)
                {
                    htschedule.Add("@CustomerGUID", customerguid);
                    htschedule.Add("@SubscriptionGUID", subscriptionguid);
                    htschedule.Add("@PaymentScheduleItemGUID", i.id);
                    htschedule.Add("@Status", i.status);
                    htschedule.Add("@amount", i.amount);
                    htschedule.Add("@date_scheduled", i.date_scheduled);
                    htschedule.Add("@ItemDesc", i.type);

                    int insertschedulitems = dal.ExecuteNonQueryProcedure("InsertPaymentScheduleItems", htschedule);
                    htschedule.Clear();
                }
                return "Success";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Database Problem " + ex.Message);
                return "Fail";
            }
        }
        public string Cancel_Payment_Schedule(string subscriptionguid, DateTime fromdate)
        {
            try
            {
                string endpoint = "subscription/" + subscriptionguid + "/paymentschedule/";
                string response = gateway.Get(url + endpoint, null);
                var message = JsonConvert.DeserializeObject<ApiMessage>(response);
                Hashtable htdeletepaymentschedule = new Hashtable();

                DataManager dal = new DataManager();
                //while (!dal._ConnOpen)
                //{
                //    dal = new DataManager();
                //}

                string data_del_reponse = "";
                foreach (PaymentScheduleList i in message.payment_schedule_list)
                {
                    DateTime dtreqdate = i.date_scheduled;
                    DateTime dtfiltefrom = fromdate;
                    if ((dtreqdate.Date > dtfiltefrom.Date))
                    {
                        if (i.status.ToUpper() != "VOIDED")
                        {
                            string payment_schedule_id = i.id;
                            endpoint = "subscription/" + subscriptionguid + "/paymentschedule/" + payment_schedule_id + "";
                            response = gateway.Delete(url + endpoint, null);
                            message = null;

                            if (response != null)
                            {
                                message = JsonConvert.DeserializeObject<ApiMessage>(response);
                            }

                            if (message != null)
                            {
                                if (message.Error == null)
                                {
                                    htdeletepaymentschedule.Clear();
                                }
                            }
                            else
                            {
                                //TIMED OUT OR RETURNED NULL
                                return "Fail";
                            }
                        }
                    }
                }
                data_del_reponse = "Success";
                if (message.HasError)
                {
                    throw new Exception(message.Error.ToString());
                }
                return data_del_reponse;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Cannot change now Try after sometime\n" + ex.Message);
                return "Fail";
            }
        }
        public string Skip_Delete_Single_Payment_Schedule(string subscriptionguid, string payment_schedule_id)
        {
            try
            {
                string endpoint = "";
                string response = "";

                Hashtable htdeletepaymentschedule = new Hashtable();

                DataManager dal = new DataManager();

                //while (!dal._ConnOpen)
                //{
                //    dal = new DataManager();
                //}

                string data_del_reponse = "Fail";
                
                endpoint = "subscription/" + subscriptionguid + "/paymentschedule/" + payment_schedule_id + "";
                response = gateway.Delete(url + endpoint, null);
                
                if (response != null)
                {
                    var message = JsonConvert.DeserializeObject<ApiMessage>(response);
                    data_del_reponse = "Success";
                    if (message.HasError)
                    {
                        throw new Exception(message.Error.ToString());
                    }
                    return data_del_reponse;
                }
                else
                {
                    return "Fail";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Cannot change now Try after sometime\n" + ex.Message);
                return "Fail" + ex.Message;
            }
        }
        public string Modify_Payment_Schedule(string subscriptionguid, DateTime fromdate, DateTime todate, double amount)
        {
            try
            {
                Hashtable htupdatpaymentschedule = new Hashtable();

                DataManager dal = new DataManager();
                //while (!dal._ConnOpen)
                //{
                //    dal = new DataManager();
                //}

                string dataresponse = "";
                int update_payment_schedule;
                string endpoint = "subscription/" + subscriptionguid + "/paymentschedule/";
                string response = gateway.Get(url + endpoint, null);
                var message = JsonConvert.DeserializeObject<ApiMessage>(response);
                foreach (PaymentScheduleList i in message.payment_schedule_list)
                {
                    DateTime dtreqdate = i.date_scheduled;
                    DateTime dtfiltefrom = fromdate;
                    DateTime dtfilterto = todate;
                    if ((dtreqdate.Date >= dtfiltefrom.Date) && (dtreqdate.Date <= dtfilterto.Date)) //>= AND <=
                    {
                        string payment_schedule_id = i.id;
                        string date = dtreqdate.ToString("yyyy-MM-dd");

                        endpoint = "subscription/" + subscriptionguid + "/paymentschedule/" + payment_schedule_id + "";
                        var endpointParams = new Dictionary<string, string>();
                        endpointParams.Add("amount", amount.ToString());
                        response = gateway.Post(url + endpoint, endpointParams);
                        message = JsonConvert.DeserializeObject<ApiMessage>(response);
                        if (message.Error == null)
                        {
                            htupdatpaymentschedule.Add("@PaymentScheduleItemGUID", i.id);
                            htupdatpaymentschedule.Add("@amount", amount);
                            update_payment_schedule = dal.ExecuteNonQueryProcedure("UpdatePaymentScheduleItems", htupdatpaymentschedule);
                            htupdatpaymentschedule.Clear();
                        }
                    }

                }
                dataresponse = "Success";
                if (message.HasError)
                {
                    throw new Exception(message.Error.ToString());
                }
                return dataresponse;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Cannot change now Try after sometime\n" + ex.Message);
                return "Fail " + ex.Message;
            }
        }
        public string Amend_Single_Payment_Schedule(string subscriptionguid, string payment_schedule_id, double amount)
        {
            try
            {
                Hashtable htupdatpaymentschedule = new Hashtable();

                DataManager dal = new DataManager();
                //while (!dal._ConnOpen)
                //{
                //    dal = new DataManager();
                //}

                string dataresponse = "";
                int update_payment_schedule;
                string endpoint = "";
                string response = "";
                
                endpoint = "subscription/" + subscriptionguid + "/paymentschedule/" + payment_schedule_id + "";
                var endpointParams = new Dictionary<string, string>();
                endpointParams.Add("amount", amount.ToString());
                response = gateway.Post(url + endpoint, endpointParams);
                var message = JsonConvert.DeserializeObject<ApiMessage>(response);
                if (message.Error == null)
                {
                    htupdatpaymentschedule.Add("@PaymentScheduleItemGUID", payment_schedule_id);
                    htupdatpaymentschedule.Add("@amount", amount);
                    update_payment_schedule = dal.ExecuteNonQueryProcedure("UpdatePaymentScheduleItems", htupdatpaymentschedule);
                    htupdatpaymentschedule.Clear();
                }

                dataresponse = "Success";
                if (message.HasError)
                {
                    throw new Exception(message.Error.ToString());
                }
                return dataresponse;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! Cannot change now Try after sometime\n" + ex.Message);
                return "Fail " + ex.Message;
            }
        }
        public string Archive_Subscription(string subscriptionGUID)
        {
            try
            {
                string endpoint = "subscription/" + subscriptionGUID + "";
                string response = gateway.Delete(url + endpoint, null);
                var message = JsonConvert.DeserializeObject<ApiMessage>(response);

                if (message.HasError)
                {
                    throw new Exception(message.Error.Message.ToString());
                }
                else
                {
                    if (message.subscription.archived == "false")
                    {
                        //MessageBox.Show("Not able to delete! Try after some time");
                        return message.subscription.archived;
                    }
                    else if (message.subscription.archived == null)
                    {
                        //MessageBox.Show("Subscription Deleted Successfully");
                        return "true";
                    }
                    else
                    {
                        //MessageBox.Show("Subscription Deleted Successfully");
                        return message.subscription.archived;
                    }
                }
            }
            catch(Exception ex)
            {
                //MessageBox.Show("Error! Cannot change now Try after sometime\n" + ex.Message);
                return "Fail " + ex.Message;
            }
        }

        public string Update_Payment_Schedule_Items(string subscriptionguid, string _customerGUID)
        {
            DataManager dal = new DataManager();
            //while (!dal._ConnOpen)
            //{
            //    dal = new DataManager();
            //}

            try
            {
                Hashtable htstatus_update = new Hashtable();
                string endpoint = "subscription/" + subscriptionguid + "/paymentschedule/";
                string response = gateway.Get(url + endpoint, null);
                ApiMessage message;

                if (response != null)
                {
                    message = JsonConvert.DeserializeObject<ApiMessage>(response);
                }
                else
                {
                    ////LOG ERROR
                    Hashtable ht = new Hashtable();
                    int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                    string _StaffUser = ConfigurationManager.AppSettings["LoggedInUser"].ToString();

                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@SiteID", _SiteID);
                    ht.Add("@functionName", "Update_Payment_Schedule_Items");
                    ht.Add("@errorMsg", "Paychoice response is NULL");
                    ht.Add("@AdditionalDetails", "CustomerGUID : " + _customerGUID + " SubscriptionGUID : " + subscriptionguid);
                    ht.Add("@userName", _StaffUser);
                    ht.Add("@localTimeStamp", DateTime.Now);

                    dal.ExecuteNonQueryProcedure("LogError", ht);
                    ////LOG ERROR

                    MessageBox.Show("UNABLE to get updated Status!\n\n\nPossible causes: \n\n(1) Invalid API Credentails!\n(2)Paychoice response Error!\n(3)Internet Connectivity not available (or) Disrupted!", "Payment Status", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return "Fail";
                }
                foreach (PaymentScheduleList i in message.payment_schedule_list)
                {
                    //if (i.date_scheduled <= DateTime.Now)
                    //{
                        string _status = "";
                        string _InvStatus = "";
                        double _InvoiceAmt = 0;

                        InvStatus _invPaymentStatus = new InvStatus();
                        if (i.invoiced)
                        {
                            _invPaymentStatus = Payment_Schedule_Item_Invoiced_Status(_customerGUID, i.invoice_id);
                            _status = _invPaymentStatus._paymentStatus;
                            _InvStatus = _invPaymentStatus._InvStatus;
                            _InvoiceAmt = _invPaymentStatus._InvAmount;
                        }
                        else
                        {
                            _status = i.status;
                            _InvStatus = "";
                        }
                        DateTime _date_Scheduled = i.date_scheduled;

                        htstatus_update.Add("@PaymentScheduleItemGUID", i.id);
                        htstatus_update.Add("@Status", _status);
                        htstatus_update.Add("@Reason", Reason_For_Dishonour);
                        htstatus_update.Add("@Amount", i.amount);
                        htstatus_update.Add("@ItemDesc", i.type);
                        htstatus_update.Add("@CustomerGUID", _customerGUID);
                        htstatus_update.Add("@SubscriptionGUID", subscriptionguid);
                        htstatus_update.Add("@Date_Scheduled", _date_Scheduled);
                        htstatus_update.Add("@InvMainStatus", _InvStatus);
                        htstatus_update.Add("InvoiceAmt", _InvoiceAmt);

                        int insertschedulitems = dal.ExecuteNonQueryProcedure("UpdatePaymentScheduleItems_Status", htstatus_update);
                        htstatus_update.Clear();
                    //}
                }
                    return "Success";
            }
            catch (Exception ex)
            {
                ////LOG ERROR
                Hashtable ht = new Hashtable();
                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                string _StaffUser = ConfigurationManager.AppSettings["LoggedInUser"].ToString();

                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                ht.Add("@functionName", "Update_Payment_Schedule_Items");
                ht.Add("@errorMsg",  "Error: " + ex.Message);
                ht.Add("@AdditionalDetails", "CustomerGUID : " + _customerGUID + " SubscriptionGUID : " + subscriptionguid);
                ht.Add("@userName", _StaffUser);
                ht.Add("@localTimeStamp", DateTime.Now);

                dal.ExecuteNonQueryProcedure("LogError", ht);
                ////LOG ERROR

                MessageBox.Show("UNABLE to get updated Status!\n\n\nPossible causes: \n\n(1) Invalid API Credentails!\n(2)Paychoice response Error!\n(3)Internet Connectivity not available (or) Disrupted!", "Payment Status", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "Fail";
            }
        }
        public string Update_Payment_Schedule_Items_EXPIRED(string subscriptionguid, string _customerGUID, DateTime _prgEndDate)
        {
            DataManager dal = new DataManager();
            //while (!dal._ConnOpen)
            //{
            //    dal = new DataManager();
            //}

            try
            {
                Hashtable htstatus_update = new Hashtable();
                string endpoint = "subscription/" + subscriptionguid + "/paymentschedule/";
                string response = gateway.Get(url + endpoint, null);
                ApiMessage message;

                if (response != null)
                {
                    message = JsonConvert.DeserializeObject<ApiMessage>(response);
                }
                else
                {
                    ////LOG ERROR
                    Hashtable ht = new Hashtable();
                    int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                    string _StaffUser = ConfigurationManager.AppSettings["LoggedInUser"].ToString();

                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@SiteID", _SiteID);
                    ht.Add("@functionName", "Update_Payment_Schedule_Items_EXPIRED");
                    ht.Add("@errorMsg", "Paychoice response is NULL");
                    ht.Add("@AdditionalDetails", "CustomerGUID : " + _customerGUID + " SubscriptionGUID : " + subscriptionguid);
                    ht.Add("@userName", _StaffUser);
                    ht.Add("@localTimeStamp", DateTime.Now);

                    dal.ExecuteNonQueryProcedure("LogError", ht);
                    ////LOG ERROR

                    MessageBox.Show("UNABLE to get updated Status!\n\n\nPossible causes: \n\n(1) Invalid API Credentails!\n(2)Paychoice response Error!\n(3)Internet Connectivity not available (or) Disrupted!", "Payment Status", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return "Fail";
                }
                foreach (PaymentScheduleList i in message.payment_schedule_list)
                {
                    string _status = "";
                    string _InvStatus = "";
                    double _InvoiceAmt = 0;

                    InvStatus _invPaymentStatus = new InvStatus();
                    if (i.invoiced)
                    {
                        _invPaymentStatus = Payment_Schedule_Item_Invoiced_Status(_customerGUID, i.invoice_id);
                        _status = _invPaymentStatus._paymentStatus;
                        _InvStatus = _invPaymentStatus._InvStatus;
                        _InvoiceAmt = _invPaymentStatus._InvAmount;
                    }
                    else
                    {
                        _status = i.status;
                        _InvStatus = "";
                    }
                    DateTime _date_Scheduled = i.date_scheduled;

                    if (_date_Scheduled > _prgEndDate && i.status == "Scheduled")
                    {
                        //VOID THE NEW ITEM in P.C
                        Skip_Delete_Single_Payment_Schedule(subscriptionguid, i.id);
                        //VOID THE NEW ITEM in P.C
                        htstatus_update.Clear();

                        htstatus_update.Add("@PaymentScheduleItemGUID", i.id);
                        htstatus_update.Add("@Status", "Voided");
                        htstatus_update.Add("@Reason", Reason_For_Dishonour);
                        htstatus_update.Add("@Amount", i.amount);
                        htstatus_update.Add("@ItemDesc", i.type);
                        htstatus_update.Add("@CustomerGUID", _customerGUID);
                        htstatus_update.Add("@SubscriptionGUID", subscriptionguid);
                        htstatus_update.Add("@Date_Scheduled", _date_Scheduled);
                        htstatus_update.Add("@InvMainStatus", _InvStatus);
                        htstatus_update.Add("InvoiceAmt", _InvoiceAmt);
                    }
                    else
                    {
                        htstatus_update.Clear();

                        htstatus_update.Add("@PaymentScheduleItemGUID", i.id);
                        htstatus_update.Add("@Status", _status);
                        htstatus_update.Add("@Reason", Reason_For_Dishonour);
                        htstatus_update.Add("@Amount", i.amount);
                        htstatus_update.Add("@ItemDesc", i.type);
                        htstatus_update.Add("@CustomerGUID", _customerGUID);
                        htstatus_update.Add("@SubscriptionGUID", subscriptionguid);
                        htstatus_update.Add("@Date_Scheduled", _date_Scheduled);
                        htstatus_update.Add("@InvMainStatus", _InvStatus);
                        htstatus_update.Add("InvoiceAmt", _InvoiceAmt);
                    }
                    int insertschedulitems = dal.ExecuteNonQueryProcedure("UpdatePaymentScheduleItems_Status", htstatus_update);
                    htstatus_update.Clear();
                }

                htstatus_update.Clear();
                htstatus_update.Add("@SubscriptionGUID", subscriptionguid);
                dal.ExecuteNonQueryProcedure("Update_expiredMembershipPaychoiceStatus", htstatus_update);

                return "Success";
            }
            catch (Exception ex)
            {
                ////LOG ERROR
                Hashtable ht = new Hashtable();
                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                string _StaffUser = ConfigurationManager.AppSettings["LoggedInUser"].ToString();

                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                ht.Add("@functionName", "Update_Payment_Schedule_Items");
                ht.Add("@errorMsg", "Error: " + ex.Message);
                ht.Add("@AdditionalDetails", "CustomerGUID : " + _customerGUID + " SubscriptionGUID : " + subscriptionguid);
                ht.Add("@userName", _StaffUser);
                ht.Add("@localTimeStamp", DateTime.Now);

                dal.ExecuteNonQueryProcedure("LogError", ht);
                ////LOG ERROR

                MessageBox.Show("UNABLE to get updated Status!\n\n\nPossible causes: \n\n(1) Invalid API Credentails!\n(2)Paychoice response Error!\n(3)Internet Connectivity not available (or) Disrupted!", "Payment Status", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "Fail";
            }
        }
        public string Update_Payment_Schedule_Items_EXPIRED_PREVSTATUS(string subscriptionguid, string _customerGUID, DateTime _prgEndDate)
        {
            DataManager dal = new DataManager();
            //while (!dal._ConnOpen)
            //{
            //    dal = new DataManager();
            //}

            try
            {
                Hashtable htstatus_update = new Hashtable();
                string endpoint = "subscription/" + subscriptionguid + "/paymentschedule/";
                string response = gateway.Get(url + endpoint, null);
                ApiMessage message;

                if (response != null)
                {
                    message = JsonConvert.DeserializeObject<ApiMessage>(response);
                }
                else
                {
                    ////LOG ERROR
                    Hashtable ht = new Hashtable();
                    int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                    string _StaffUser = ConfigurationManager.AppSettings["LoggedInUser"].ToString();

                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@SiteID", _SiteID);
                    ht.Add("@functionName", "Update_Payment_Schedule_Items_EXPIRED_PREVSTATUS");
                    ht.Add("@errorMsg", "Paychoice response is NULL");
                    ht.Add("@AdditionalDetails", "CustomerGUID : " + _customerGUID + " SubscriptionGUID : " + subscriptionguid);
                    ht.Add("@userName", _StaffUser);
                    ht.Add("@localTimeStamp", DateTime.Now);

                    dal.ExecuteNonQueryProcedure("LogError", ht);
                    ////LOG ERROR

                    MessageBox.Show("UNABLE to get updated Status!\n\n\nPossible causes: \n\n(1) Invalid API Credentails!\n(2)Paychoice response Error!\n(3)Internet Connectivity not available (or) Disrupted!", "Payment Status", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return "Fail";
                }
                foreach (PaymentScheduleList i in message.payment_schedule_list)
                {
                    if (i.date_scheduled.Date <= _prgEndDate.Date)
                    {
                        string _status = "";
                        string _InvStatus = "";
                        double _InvoiceAmt = 0;

                        InvStatus _invPaymentStatus = new InvStatus();
                        if (i.invoiced)
                        {
                            _invPaymentStatus = Payment_Schedule_Item_Invoiced_Status(_customerGUID, i.invoice_id);
                            _status = _invPaymentStatus._paymentStatus;
                            _InvStatus = _invPaymentStatus._InvStatus;
                            _InvoiceAmt = _invPaymentStatus._InvAmount;
                        }
                        else
                        {
                            _status = i.status;
                            _InvStatus = "";
                        }
                        DateTime _date_Scheduled = i.date_scheduled;

                        htstatus_update.Add("@PaymentScheduleItemGUID", i.id);
                        htstatus_update.Add("@Status", _status);
                        htstatus_update.Add("@Reason", Reason_For_Dishonour);
                        htstatus_update.Add("@Amount", i.amount);
                        htstatus_update.Add("@ItemDesc", i.type);
                        htstatus_update.Add("@CustomerGUID", _customerGUID);
                        htstatus_update.Add("@SubscriptionGUID", subscriptionguid);
                        htstatus_update.Add("@Date_Scheduled", _date_Scheduled);
                        htstatus_update.Add("@InvMainStatus", _InvStatus);
                        htstatus_update.Add("InvoiceAmt", _InvoiceAmt);

                        int insertschedulitems = dal.ExecuteNonQueryProcedure("UpdatePaymentScheduleItems_Status", htstatus_update);
                        htstatus_update.Clear();
                    }
                }
                return "Success";
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Invalid API Credentails! OR Paychoice Error!\n\nUNABLE to get updated Status!\n" + ex.Message, "Paychoice", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "Fail";
            }
        }
        public string Update_Single_Payment_Schedule_Item_Status(string PaymentScheduleItemGUID, string subscriptionguid, string _customerGUID)
        {

            DataManager dal = new DataManager();
            //while (!dal._ConnOpen)
            //{
            //    dal = new DataManager();
            //}

            string _addlDetails = "Customer TOKEN ID: " + _customerGUID + " SubscriptionGUID : " + subscriptionguid + " Payment Item GUID : " + PaymentScheduleItemGUID;

            try
            {
                Hashtable htstatus_update = new Hashtable();
                string endpoint = "subscription/" + subscriptionguid + "/paymentschedule/" + PaymentScheduleItemGUID;
                string response = gateway.Get(url + endpoint, null);

                if (response.Trim().ToUpper().StartsWith("YOU DO"))
                {
                    ////LOG ERROR
                    Hashtable ht = new Hashtable();
                    int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                    int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                    string _StaffUser = ConfigurationManager.AppSettings["LoggedInUser"].ToString();

                    ht.Add("@CompanyID", _CompanyID);
                    ht.Add("@SiteID", _SiteID);
                    ht.Add("@functionName", "Update_Single_Payment_Schedule_Item_Status");
                    ht.Add("@errorMsg", "Invalid PayChoice Credentials! User : " + gateway.username + " Password: " + gateway.password + " " + response);
                    ht.Add("@AdditionalDetails", _addlDetails);
                    ht.Add("@userName", _StaffUser);
                    ht.Add("@localTimeStamp", DateTime.Now);

                    dal.ExecuteNonQueryProcedure("LogError", ht);
                    ////LOG ERROR

                    return "Fail";
                }

                var message = JsonConvert.DeserializeObject<ApiMessage>(response);

                string _status = "";
                string _InvStatus = "";
                double _InvoiceAmt = 0;
                InvStatus _invPaymentStatus = new InvStatus();

                if (message != null)
                {
                    if (!message.HasError)
                    {
                        if (message.payment_schedule.invoiced)
                        {
                            _invPaymentStatus = Payment_Schedule_Item_Invoiced_Status(_customerGUID, message.payment_schedule.invoice_id);
                            if ((message.payment_schedule.status.ToUpper() == "PROCESSED") && (_invPaymentStatus._paymentStatus.ToUpper() == "PROCESSING") && ((_invPaymentStatus._InvStatus + "").ToUpper() != "OPEN"))
                            {
                                //_status = message.payment_schedule.status;
                                _status = _invPaymentStatus._paymentStatus;
                            }
                            else
                            {
                                _status = _invPaymentStatus._paymentStatus;
                            }
                            _InvStatus = _invPaymentStatus._InvStatus;
                            _InvoiceAmt = _invPaymentStatus._InvAmount;
                        }
                        else
                        {
                            _status = message.payment_schedule.status;
                            _InvStatus = "";
                        }

                        DateTime _date_Scheduled = message.payment_schedule.date_scheduled;

                        htstatus_update.Add("@PaymentScheduleItemGUID", message.payment_schedule.id);
                        htstatus_update.Add("@Status", _status);
                        htstatus_update.Add("@Reason", Reason_For_Dishonour);
                        htstatus_update.Add("@Amount", message.payment_schedule.amount);
                        htstatus_update.Add("@ItemDesc", message.payment_schedule.type);
                        htstatus_update.Add("@CustomerGUID", _customerGUID);
                        htstatus_update.Add("@SubscriptionGUID", subscriptionguid);
                        htstatus_update.Add("@Date_Scheduled", _date_Scheduled);
                        htstatus_update.Add("@InvMainStatus", _InvStatus);
                        htstatus_update.Add("@InvoiceAmt", _InvoiceAmt);

                        int insertschedulitems = dal.ExecuteNonQueryProcedure("UpdatePaymentScheduleItems_Status", htstatus_update);
                        htstatus_update.Clear();

                        return "Success";
                    }
                    else
                    {
                        ////LOG ERROR
                        Hashtable ht = new Hashtable();
                        int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                        int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                        string _StaffUser = ConfigurationManager.AppSettings["LoggedInUser"].ToString();

                        ht.Add("@CompanyID", _CompanyID);
                        ht.Add("@SiteID", _SiteID);
                        ht.Add("@functionName", "Update_Single_Payment_Schedule_Item_Status");
                        //ht.Add("@errorMsg", "Error Response : " + response);
                        ht.Add("@errorMsg", "Error Response : " + message.Error.Message);
                        ht.Add("@AdditionalDetails", _addlDetails);
                        ht.Add("@userName", _StaffUser);
                        ht.Add("@localTimeStamp", DateTime.Now);

                        dal.ExecuteNonQueryProcedure("LogError", ht);
                        ////LOG ERROR

                        return "NOT FOUND";
                    }
                }
                else
                {
                    if (response.Trim() != "")
                    {
                        ////LOG ERROR
                        Hashtable ht = new Hashtable();
                        int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                        int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                        string _StaffUser = ConfigurationManager.AppSettings["LoggedInUser"].ToString();

                        ht.Add("@CompanyID", _CompanyID);
                        ht.Add("@SiteID", _SiteID);
                        ht.Add("@functionName", "Update_Single_Payment_Schedule_Item_Status");
                        ht.Add("@errorMsg", response);
                        ht.Add("@AdditionalDetails", _addlDetails);
                        ht.Add("@userName", _StaffUser);
                        ht.Add("@localTimeStamp", DateTime.Now);

                        dal.ExecuteNonQueryProcedure("LogError", ht);
                        ////LOG ERROR
                    }
                    return "NOT FOUND";
                }
            }
            catch (Exception ex)
            {
                ////LOG ERROR
                Hashtable ht = new Hashtable();
                int _CompanyID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInOrgID"].ToString());
                int _SiteID = Convert.ToInt32(ConfigurationManager.AppSettings["LoggedInSiteID"].ToString());
                string _StaffUser = ConfigurationManager.AppSettings["LoggedInUser"].ToString();

                ht.Add("@CompanyID", _CompanyID);
                ht.Add("@SiteID", _SiteID);
                ht.Add("@functionName", "Update_Single_Payment_Schedule_Item_Status");
                ht.Add("@errorMsg", ex.Message);
                ht.Add("@AdditionalDetails", _addlDetails);
                ht.Add("@userName", _StaffUser);
                ht.Add("@localTimeStamp", DateTime.Now);

                dal.ExecuteNonQueryProcedure("LogError", ht);
                ////LOG ERROR

                return "Fail";
            }
        }

        //public string Payment_Schedule_Item_Invoiced_Status(string customerid, string invoiceid)
        //{
        //    ////// till V1.0.5
        //    //string status_on_processed = "";
        //    //string endpoint = "customer/" + customerid + "/invoice/" + invoiceid + "";
        //    //string response = gateway.Get(url + endpoint, null);
        //    //var message = JsonConvert.DeserializeObject<ApiMessage>(response);
        //    //foreach (PaymentsList i in message.invoice.payments_list)
        //    //{
        //    //    status_on_processed = i.transaction.status.ToString();
        //    //    Reason_For_Dishonour = i.transaction.error_description.ToString();
        //    //}
        //    //return status_on_processed;
        //    ////// till V1.0.5
        //}

        public InvStatus Payment_Schedule_Item_Invoiced_Status(string customerid, string invoiceid)
        { 
            string status_on_processed = "";
            int payment_list_count;
            string endpoint = "customer/" + customerid + "/invoice/" + invoiceid + "";
            string response = gateway.Get(url + endpoint, null);
            var message = JsonConvert.DeserializeObject<ApiMessage>(response);
            
            //Invoice Main Status
            InvStatus _InvStatus = new InvStatus();
            invoice_main_status = message.invoice.status;
            _InvStatus._InvStatus = invoice_main_status;
            _InvStatus._InvAmount = message.invoice.invoice_amount;

            Reason_For_Dishonour = "";

            payment_list_count = message.invoice.payments_list.Count;
            int looping_past_recipt_number = 0;
            int looping_present_receipt_number = 0;
            int j = 0;
            
            if (payment_list_count > 1)
            {
                for (int i = 0; i <= payment_list_count - 1; i++)
                {
                    if (i == 0)
                    {
                        looping_past_recipt_number = message.invoice.payments_list[i].receipt_number;
                        j = i;
                    }
                    else
                    {
                        looping_present_receipt_number = message.invoice.payments_list[i].receipt_number;
                        if (looping_present_receipt_number > looping_past_recipt_number)
                        {
                            status_on_processed = message.invoice.payments_list[i].transaction.status.ToString();
                            Reason_For_Dishonour = message.invoice.payments_list[i].transaction.error_description.ToString();
                            looping_past_recipt_number = looping_present_receipt_number;
                            j = i;
                            _InvStatus._paymentStatus = status_on_processed;
                        }
                        else
                        {
                            status_on_processed = message.invoice.payments_list[j].transaction.status.ToString();
                            Reason_For_Dishonour = message.invoice.payments_list[j].transaction.error_description.ToString();
                            _InvStatus._paymentStatus = status_on_processed;
                        }
                    }
                }
            }
            else
            {
                if (payment_list_count > 0)
                {
                    foreach (PaymentsList i in message.invoice.payments_list)
                    {
                        status_on_processed = i.transaction.status.ToString();
                        Reason_For_Dishonour = i.transaction.error_description.ToString();
                        _InvStatus._paymentStatus = status_on_processed;
                    }
                }
                else
                {
                    _InvStatus._paymentStatus = message.invoice.status;
                }
            }
            return _InvStatus;
        }

        public string List_Subscriptions_All(string CustomerGUID)
        {
            try
            {
                string endpoint = "subscription/";
                var endpointParams = new Dictionary<string, string>();
                endpointParams.Add("customer_id", CustomerGUID);
                endpointParams.Add("archived", "true");
                string response = gateway.Get(url + endpoint, endpointParams);
                var message = JsonConvert.DeserializeObject<ApiMessage>(response);
                subscriptionId.Clear();

                if (message.HasError)
                {
                    throw new Exception(message.Error.Message.ToString());
                }
                foreach (SubscriptionList i in message.subscription_list)
                {
                    subscriptionId.Add(i.id);
                }
                return "Success";
            }
            catch (Exception e)
            {
                return "Fail";
            }
        }
        public List<string> List_Subscriptions_Active(string CustomerGUID)
        {
            try
            {
                string endpoint = "subscription/";
                var endpointParams = new Dictionary<string, string>();
                endpointParams.Add("customer_id", CustomerGUID);
                endpointParams.Add("archived", "false");
                string response = gateway.Get(url + endpoint, endpointParams);
                var message = JsonConvert.DeserializeObject<ApiMessage>(response);
                subscriptionId.Clear();

                if (message.HasError)
                {
                    throw new Exception(message.Error.Message.ToString());
                }
                foreach (SubscriptionList i in message.subscription_list)
                {
                    subscriptionId.Add(i.id);
                }
                return subscriptionId;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public string Edit_Existing_Customer(String customerGUID, Hashtable hteditcustomer)
        {
            try
            {
                string endpoint = "customer/" + customerGUID;
                var endpointParams = new Dictionary<string, string>();
                string business_name = hteditcustomer["business_name"].ToString();
                string account_number = hteditcustomer["account_number"].ToString();
                string first_name = hteditcustomer["first_name"].ToString();
                string last_name = hteditcustomer["last_name"].ToString();
                string email = hteditcustomer["email"].ToString();
                string phone = hteditcustomer["phone"].ToString();
                string mobile = hteditcustomer["mobile"].ToString();
                string address_line1 = hteditcustomer["address_line1"].ToString();
                string address_line2 = hteditcustomer["address_line2"].ToString();
                string address_suburb = hteditcustomer["address_suburb"].ToString();
                string address_state = hteditcustomer["address_state"].ToString();
                string address_postcode = hteditcustomer["address_postcode"].ToString();
                string address_country = hteditcustomer["address_country"].ToString();

                if (string.IsNullOrEmpty(business_name) == false)
                {
                    endpointParams.Add("business_name", business_name);
                }
                if (string.IsNullOrEmpty(account_number) == false)
                {
                    endpointParams.Add("account_number", account_number);
                }
                if (string.IsNullOrEmpty(first_name) == false)
                {
                    endpointParams.Add("first_name", first_name);
                }
                if (string.IsNullOrEmpty(last_name) == false)
                {
                    endpointParams.Add("last_name", last_name);
                }
                if (string.IsNullOrEmpty(email) == false)
                {
                    endpointParams.Add("email", email);
                }
                if (string.IsNullOrEmpty(phone) == false)
                {
                    endpointParams.Add("phone", phone);
                }
                if (string.IsNullOrEmpty(mobile) == false)
                {
                    endpointParams.Add("mobile", mobile);
                }
                if (string.IsNullOrEmpty(address_line1) == false)
                {
                    endpointParams.Add("address[line1]", address_line1);
                }
                if (string.IsNullOrEmpty(address_line2) == false)
                {
                    endpointParams.Add("address[line2]", address_line2);
                }
                if (string.IsNullOrEmpty(address_suburb) == false)
                {
                    endpointParams.Add("address[suburb]", address_suburb);
                }
                if (string.IsNullOrEmpty(address_state) == false)
                {
                    endpointParams.Add("address[state]", address_state);
                }
                if (string.IsNullOrEmpty(address_postcode) == false)
                {
                    endpointParams.Add("address[postcode]", address_postcode);
                }
                if (string.IsNullOrEmpty(address_country) == false)
                {
                    endpointParams.Add("address[country]", address_country);
                }

                string response = gateway.Post(url + endpoint, endpointParams);
                var message = JsonConvert.DeserializeObject<ApiMessage>(response);
                if (message.HasError)
                {
                    throw new Exception(message.Error.Message.ToString());
                }
                else
                {
                    return "Success";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Remove_Credit_Card(string customerguid, string tokenguid)
        {
            string endpoint = "customer/" + customerguid + "/card/" + tokenguid + "";
            string response = gateway.Delete(url + endpoint, null);
            var message = JsonConvert.DeserializeObject<ApiMessage>(response);

            if (message.HasError)
            {
                //MessageBox.Show(message.Error.ToString());
                return "Fail";
            }
            else
            {
                //MessageBox.Show("Credit Card Deleted Successfully");
                return "Success";
            }

        }

        public string Pay_Customer_Invoice(string customerid, string invoiceid, double Amount)
        {
            string endpoint = "customer/" + customerid + "/invoice/" + invoiceid + "";
            var endpointParams = new Dictionary<string, string>();
            endpointParams.Add("amount", Amount.ToString());
            string response = gateway.Post(url + endpoint, endpointParams);
            var message = JsonConvert.DeserializeObject<ApiMessage>(response);
            if (message.HasError)
            {
                //MessageBox.Show(message.Error.ToString());
                return "Fail";
            }
            else
            {
                //MessageBox.Show("Invoice Paid Successfully");
                return "Success";
            }
        }

        public string Cancel_Invoices(string customerid, string invoiceid)
        {
            string endpoint = "customer/" + customerid + "/invoice/" + invoiceid + "";
            string response = gateway.Delete(url + endpoint, null);
            var message = JsonConvert.DeserializeObject<ApiMessage>(response);
            if (message.HasError)
            {
                //MessageBox.Show(message.Error.Message.ToString());
                return "Fail";
            }
            else
            {
                //MessageBox.Show("The Invoice Voided successfully!");
                return "Success";
            }
        }

        public string Void_Dishonoured_Invoice_with_Alternate_Payment(string PaymentScheduleItemGUID, string subscriptionguid, string _customerGUID)
        {
            try
            {

                DataManager dal = new DataManager();
                //while (!dal._ConnOpen)
                //{
                //    dal = new DataManager();
                //}

                Hashtable htstatus_update = new Hashtable();
                string endpoint = "subscription/" + subscriptionguid + "/paymentschedule/" + PaymentScheduleItemGUID;
                string response = gateway.Get(url + endpoint, null);
                var message = JsonConvert.DeserializeObject<ApiMessage>(response);

                string _status = "";

                if (message.payment_schedule.invoiced)
                {
                    _status = Cancel_Invoices(_customerGUID, message.payment_schedule.invoice_id);
                }
                else
                {
                    _status = message.payment_schedule.status;
                }

                if (_status.ToString().ToUpper() == "SUCCESS")
                {
                    //DateTime _date_Scheduled = message.payment_schedule.date_scheduled;

                    //htstatus_update.Add("@PaymentScheduleItemGUID", message.payment_schedule.id);
                    //htstatus_update.Add("@Status", _status);
                    //htstatus_update.Add("@Reason", Reason_For_Dishonour);
                    //htstatus_update.Add("@Amount", message.payment_schedule.amount);
                    //htstatus_update.Add("@ItemDesc", message.payment_schedule.type);
                    //htstatus_update.Add("@CustomerGUID", _customerGUID);
                    //htstatus_update.Add("@SubscriptionGUID", subscriptionguid);
                    //htstatus_update.Add("@Date_Scheduled", _date_Scheduled);
                    //htstatus_update.Add("@InvMainStatus", _InvStatus);

                    //int insertschedulitems = dal.ExecuteNonQueryProcedure("UpdatePaymentScheduleItems_Status", htstatus_update);
                    //htstatus_update.Clear();
                }
                return _status;
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Database Problem - " + ex.Message);
                return "Fail";
            }
        }

        public double Retrieve_Customer_Balance_pc_param (string _customerGUID)
        {
            try
            {

                DataManager dal = new DataManager();
                //while (!dal._ConnOpen)
                //{
                //    dal = new DataManager();
                //}

                Hashtable htstatus_update = new Hashtable();
                string endpoint = "customer/" + _customerGUID;
                string response = gateway.Get(url + endpoint, null);
                var message = JsonConvert.DeserializeObject<ApiMessage>(response);

                if (message != null)
                {
                    if (message.HasError)
                    {
                        return 0;
                    }
                    else
                    {
                        return message.customer.account_balance;
                    }
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public string Pay_Dishonoured_Invoice_with_Alternate_Payment(string PaymentScheduleItemGUID, string subscriptionguid, string _customerGUID, double _amount)
        {
            try
            {

                DataManager dal = new DataManager();
                //while (!dal._ConnOpen)
                //{
                //    dal = new DataManager();
                //}

                Hashtable htstatus_update = new Hashtable();
                string endpoint = "subscription/" + subscriptionguid + "/paymentschedule/" + PaymentScheduleItemGUID;
                string response = gateway.Get(url + endpoint, null);
                var message = JsonConvert.DeserializeObject<ApiMessage>(response);

                string _status = "";

                if (message.payment_schedule.invoiced)
                {
                    _status = Pay_Customer_Invoice(_customerGUID, message.payment_schedule.invoice_id, _amount);
                }
                else
                {
                    _status = message.payment_schedule.status;
                }

                if (_status.ToString().ToUpper() == "SUCCESS")
                {
                    //DateTime _date_Scheduled = message.payment_schedule.date_scheduled;

                    //htstatus_update.Add("@PaymentScheduleItemGUID", message.payment_schedule.id);
                    //htstatus_update.Add("@Status", _status);
                    //htstatus_update.Add("@Reason", Reason_For_Dishonour);
                    //htstatus_update.Add("@Amount", message.payment_schedule.amount);
                    //htstatus_update.Add("@ItemDesc", message.payment_schedule.type);
                    //htstatus_update.Add("@CustomerGUID", _customerGUID);
                    //htstatus_update.Add("@SubscriptionGUID", subscriptionguid);
                    //htstatus_update.Add("@Date_Scheduled", _date_Scheduled);
                    //htstatus_update.Add("@InvMainStatus", _InvStatus);

                    //int insertschedulitems = dal.ExecuteNonQueryProcedure("UpdatePaymentScheduleItems_Status", htstatus_update);
                    //htstatus_update.Clear();
                }
                return _status;
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Database Problem - " + ex.Message);
                return "Fail";
            }
        }
        public string Delete_Customer(string customerguid)
        {
            string endpoint = "customer/" + customerguid + "";
            string response = gateway.Delete(url + endpoint, null);
            var message = JsonConvert.DeserializeObject<ApiMessage>(response);

            if (message.HasError)
            {
                //MessageBox.Show("Error! Cannot change now Try after sometime\n" + message.Error);
                return "Fail";
            }
            else
            {
                //MessageBox.Show("Customer Archived!");
                return "Success";
            }
        }
    }
}
