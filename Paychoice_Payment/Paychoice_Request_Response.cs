﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Security;
using System.Web;
using System.IO;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Configuration;
using System.Windows.Forms;


namespace Paychoice_Payment
{
    public class Paychoice_Request_Response
    {
        ////*SANDBOX - HIKMA
        //string username = "bf869fc2-5536-4d34-9c07-1453e089e845";
        //string password = "J&MdT(f+87*5";
        ////*SANDBOX - HIKMA

        ////*LIVE - HIKMA
        //string username = "22630f9b-1d1d-4707-83fd-b238e4274fdb";
        //string password = "/iu1qf5#.Oc";
        ////*LIVE - HIKMA

        //*LIVE - EMF 

        //*(BUNDALL)
        //string username = "baee0b4e-d631-4a10-b8f2-0e69a530a87a";
        //string password = "]w5L&p}]M$";
        //*(BUNDALL)

        //*(PACFAIR)
        //string username = "c560f91e-5bfe-4c58-8abd-5326761e0016";
        //string password = "Y2u>CuDMo]m1";
        //*(PACFAIR)

        //*LIVE - EMF

        public string username = null;
        public string password = null;
        public Paychoice_Request_Response()
        {
            try
            {
                string payment_portal = ConfigurationManager.AppSettings["PaychoiceUsagePortal"];
                if ((payment_portal != null) && (payment_portal != ""))
                {
                    if (payment_portal == "Sandbox")
                    {
                        username = ConfigurationManager.AppSettings["PaychoiceSandboxUsername"];
                        password = ConfigurationManager.AppSettings["PaychoiceSandboxPassword"];
                    }
                    else
                    {
                        username = ConfigurationManager.AppSettings["PaychoiceSecuredUsername"];
                        password = ConfigurationManager.AppSettings["PaychoiceSecuredPassword"];
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public string Post(string url, IDictionary<string, string> paramValues = null)
        {
            var body = paramValues != null ? GetFormValues(paramValues) : null;
            var request = GetWebRequest(url, "POST", body);
            return Execute(request);
        }
        private static string GetFormValues(IDictionary<string, string> paramValues)
        {
            StringBuilder formString = new StringBuilder();
            bool firstValue = true;

            foreach (var param in paramValues)
            {
                if (firstValue)
                {
                    firstValue = false;
                }
                else
                {
                    formString.Append("&");
                }

                formString.AppendFormat("{0}={1}", param.Key, param.Value);
            }

            return formString.ToString();
        }

        private WebRequest GetWebRequest(string url, string method, string body = null)
        {
            string authHeader = GetAuthorizationHeader();

            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = method;
            request.ContentType = "application/x-www-form-urlencoded";
            request.UserAgent = "Paychoice.Net Client";

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            if (!string.IsNullOrEmpty(authHeader))
            {
                request.Headers.Add("Authorization", authHeader);
            }

            if (!string.IsNullOrEmpty(body))
            {
                TryLog(string.Format("{0} {1} {2}", request.Method, request.RequestUri.ToString(), body));
                byte[] bodyData = Encoding.UTF8.GetBytes(body);
                request.ContentLength = bodyData.Length;

                request.GetRequestStream().Write(bodyData, 0, bodyData.Length);
                request.GetRequestStream().Flush();
                request.GetRequestStream().Close();
            }
            else
            {
                if(method!="GET")
                {
                    request.GetRequestStream().Flush();
                    request.GetRequestStream().Close();
                }
            }
            return request;
        }
        private string Execute(WebRequest webRequest)
        {
            var apiResponse = new PaychoiceResponse();

            try
            {
                using (var response = webRequest.GetResponse())
                {
                    apiResponse.RawResponse = ReadStream(response.GetResponseStream());
                    apiResponse.StatusCode = ((HttpWebResponse)response).StatusCode;
                    var jsonstring = new JavaScriptSerializer().Serialize(apiResponse);
                }
            }
            catch (WebException webException)
            {
                //MessageBox.Show(webException.Message,"Paychoice",MessageBoxButtons.OK,MessageBoxIcon.Information);
                if (webException.Response != null)
                {
                    var response = webException.Response;
                    apiResponse.RawResponse = ReadStream(response.GetResponseStream());
                    apiResponse.StatusCode = ((HttpWebResponse)response).StatusCode;
                }
            }

            TryLog(string.Format("{0} {1} {2}", (int)apiResponse.StatusCode, apiResponse.StatusCode, apiResponse.RawResponse));

            return apiResponse.RawResponse;
        }
        public string GetAuthorizationHeader()
        {
            var token = Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Format("{0}:{1}", username, password)));
            return string.Format("Basic {0}", token);
        }
        private void TryLog(string message)
        {
            if (message != null)
            {
                Log(message);
            }
        }
        public void Log(string message)
        {
            string message_authorize = message ;
        }
        private static string ReadStream(Stream stream)
        {
            using (var reader = new StreamReader(stream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }

        public string Delete(string url, IDictionary<string, string> paramValues = null)
        {
            var body = paramValues != null ? GetFormValues(paramValues) : null;
            var request = GetWebRequest(url, "DELETE", body);
            return Execute(request);
        }
        public string Get(string url, IDictionary<string, string> paramValues = null)
        {
            var query = paramValues != null ? GetQuerystring(paramValues) : null;
            var request = GetWebRequest(url + query, "GET");
            return Execute(request);
        }

        private static string GetQuerystring(IDictionary<string, string> paramValues)
        {
            StringBuilder queryString = new StringBuilder();
            bool firstValue = true;

            foreach (var param in paramValues)
            {
                if (firstValue)
                {
                    queryString.Append("?");
                    firstValue = false;
                }
                else
                {
                    queryString.Append("&");
                }

                queryString.AppendFormat("{0}={1}", HttpUtility.UrlEncode(param.Key), HttpUtility.UrlEncode(param.Value));
            }
            return queryString.ToString();
        }
    }
}
