﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Paychoice_Payment
{
    public class PaymentScheduleList
    {
        [JsonProperty("amount")]
        public double amount { get; set; }
        [JsonProperty("date_scheduled")]
        public DateTime date_scheduled { get; set; }
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("invoiced")]
        public bool invoiced { get; set; }
        [JsonProperty("link")]
        public Link link { get; set; }
        [JsonProperty("locked")]
        public bool locked { get; set; }
        [JsonProperty("status")]
        public string status { get; set; }
        [JsonProperty("subscription_id")]
        public string subscription_id { get; set; }
        [JsonProperty("type")]
        public string type { get; set; }
        [JsonProperty("invoice_id")]
        public string invoice_id { get; set; }
    }
}
