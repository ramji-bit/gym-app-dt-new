﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Paychoice_Payment
{
    public class PaymentsList
    {
        [JsonProperty("amount")]
        public double amount { get; set; }
        [JsonProperty("amount_applied")]
        public double amount_applied { get; set; }
        [JsonProperty("amount_pending")]
        public double amount_pending { get; set; }
        [JsonProperty("date")]
        public string date { get; set; }
        [JsonProperty("receipt_number")]
        //public string receipt_number { get; set; }
        public int receipt_number { get; set; }
        [JsonProperty("type")]
        public string type { get; set; }
        [JsonProperty("transaction")]
        public Transaction transaction { get; set; }
    }
}
