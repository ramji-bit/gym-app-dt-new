﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Windows;
using System.Windows.Forms;

namespace WebServicesApps
{
    public class SqlHelper
    {

        string ConnectionString = string.Empty;
        static SqlConnection con;
        public SqlHelper()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["GymMembershipDB"].ConnectionString;
            con = new SqlConnection(ConnectionString);
        }
        public void SetConnection()
        {
            if (ConnectionString == string.Empty)
            {
                ConnectionString = ConfigurationManager.ConnectionStrings["GymMembershipDB"].ConnectionString;
            }
            con = new SqlConnection(ConnectionString);
        }
        public DataSet ExecuteProcudere(string procName, Hashtable parms)
        {

            try
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter da = new SqlDataAdapter();
                DataSet ds = new DataSet();
                cmd.CommandText = procName;
                cmd.CommandType = CommandType.StoredProcedure;
                if (con == null)
                {
                    SetConnection();
                }
                cmd.Connection = con;
                if (parms.Count > 0)
                {
                    foreach (DictionaryEntry de in parms)
                    {
                        cmd.Parameters.AddWithValue(de.Key.ToString(), de.Value);
                    }
                }
                da.SelectCommand = cmd;
                da.Fill(ds);
                return ds;
            }
            catch (SqlException Exception)
            {
                MessageBox.Show(Exception.Message.ToString());
                return null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                return null;
            }
        }
        public int ExecuteQuery(string procName, Hashtable parms)
        {

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = procName;
                if (parms.Count > 0)
                {
                    foreach (DictionaryEntry de in parms)
                    {
                        cmd.Parameters.AddWithValue(de.Key.ToString(), de.Value);
                    }
                }
                if (con == null)
                {
                    SetConnection();
                }
                cmd.Connection = con;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                int result = cmd.ExecuteNonQuery();
                return result;
            }
            catch (SqlException Exception)
            {
                MessageBox.Show(Exception.Message.ToString());
                return 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                return 0;
            }
        }
        public int ExecuteQuerywithOutputparams(SqlCommand cmd)
        {
            try
            {
                if (con == null)
                {
                    SetConnection();
                }
                cmd.Connection = con;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                int result = cmd.ExecuteNonQuery();
                return result;
            }
            catch (SqlException Exception)
            {
                MessageBox.Show(Exception.Message.ToString());
                return 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                return 0;
            }
        }
        public int ExecuteQueryWithOutParam(string procName, Hashtable parms)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                SqlParameter sqlparam = new SqlParameter();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = procName;
                if (parms.Count > 0)
                {
                    foreach (DictionaryEntry de in parms)
                    {
                        if (de.Key.ToString().Contains("_out"))
                        {
                            sqlparam = new SqlParameter(de.Key.ToString(), de.Value);
                            sqlparam.DbType = DbType.Int32;
                            sqlparam.Direction = ParameterDirection.Output;
                            cmd.Parameters.Add(sqlparam);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue(de.Key.ToString(), de.Value);
                        }
                    }
                }
                if (con == null)
                {
                    SetConnection();
                }
                cmd.Connection = con;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                int result = cmd.ExecuteNonQuery();
                if (sqlparam != null)
                    result = Convert.ToInt32(sqlparam.SqlValue.ToString());
                return result;
            }
            catch (SqlException Exception)
            {
                MessageBox.Show(Exception.Message.ToString());
                return 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                return 0;
            }
        }
    }
}