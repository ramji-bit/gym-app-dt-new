﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections;
using System.Windows.Forms;
using System.Numerics;

namespace WebServicesApps
{
    /// <summary>
    /// Summary description for GymAppWebServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class GymAppWebServices : System.Web.Services.WebService
    {
        SqlHelper sql = new SqlHelper();

       

        //[WebMethod]
        //public string HelloWorld()
        //{
        //    return "Hello World";
        //}

            //[WebMethod]
        //public int add_MemberDetails(MemberDetailsDTO memberDetailsDTO)
        //{
        //    try
        //    {
        //        Hashtable ht = new Hashtable();

        //        ht.Add("@Fname", memberDetailsDTO.FirstName);
        //        ht.Add("@Lname", memberDetailsDTO.LastName);
        //        ht.Add("@dob", memberDetailsDTO.DateofBirth);
        //        ht.Add("@Gender", memberDetailsDTO.Gender);
        //        ht.Add("@Street", memberDetailsDTO.Street);
        //        ht.Add("@Suburb", memberDetailsDTO.Subrub);
        //        ht.Add("@City", memberDetailsDTO.City);
        //        ht.Add("@PostalCode", memberDetailsDTO.PostalCode);
        //        ht.Add("@HomePhone", memberDetailsDTO.HomePhone);
        //        ht.Add("@MobilePhone", memberDetailsDTO.MobilePhone);
        //        ht.Add("@Notes", memberDetailsDTO.Notes);
        //        ht.Add("@CardNumber", memberDetailsDTO.CardNo);
        //        ht.Add("@WorkPhone", memberDetailsDTO.WorkPhone);
        //        ht.Add("@Occupation", memberDetailsDTO.Occupation);
        //        ht.Add("@Organisation", memberDetailsDTO.Organisation);
        //        ht.Add("@InvolvementType", memberDetailsDTO.InvolvementType);
        //        ht.Add("@PersonalTrainer", memberDetailsDTO.PersonalTrainer);
        //        ht.Add("@EamilId", memberDetailsDTO.EamilId);
        //        ht.Add("@MemberNo", memberDetailsDTO.MemberId);

        //        int result = sql.ExecuteQuery("SaveMemberDetails", ht);
        //        return result;
        //    }
        //    catch (Exception)
        //    {
        //        return 0;
        //    }

        //}

        [WebMethod]
        public DataSet get_PersonalTrainer()
        {
            try
            {
                Hashtable ht = new Hashtable();
                DataSet ds = sql.ExecuteProcudere("GetPersonalTrainer", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        [WebMethod]
        public DataSet get_InvolvementType()
        {
            try
            {
                Hashtable ht = new Hashtable();
                DataSet ds = sql.ExecuteProcudere("GetInvolvementType", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        [WebMethod]
        public DataSet get_ProgramGroup()
        {
            try
            {
                Hashtable ht = new Hashtable();
                DataSet ds = sql.ExecuteProcudere("GetProgrammeGroup", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        [WebMethod]
        public DataSet get_Program()
        {
            try
            {
                Hashtable ht = new Hashtable();
                DataSet ds = sql.ExecuteProcudere("GetProgramme", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        [WebMethod]
        public DataSet get_ProgramByGroup(int ProgramGroupId)
        {
            try
            {
                Hashtable ht = new Hashtable();
                ht.Add("@ProgramGroupId", ProgramGroupId);
                DataSet ds = sql.ExecuteProcudere("GetProgrammebyGroup", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        [WebMethod]
        public DataSet get_ProgrammeDetails(int ProgramId)
        {
            try
            {
                Hashtable ht = new Hashtable();
                ht.Add("@ProgramId", ProgramId);
                DataSet ds = sql.ExecuteProcudere("GetProgrammeDetails", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        [WebMethod]
        public DataSet get_CardNo()
        {
            try
            {
                Hashtable ht = new Hashtable();
                DataSet ds = sql.ExecuteProcudere("GetMemberCardNo", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        [WebMethod]
        public DataSet get_MemberNo()
        {
            try
            {
                Hashtable ht = new Hashtable();
                DataSet ds = sql.ExecuteProcudere("GetMemberNo", ht);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return null;
            }
        }

        [WebMethod]
        //public int add_Membership(DataTable addMembership, decimal FullCost, string Condition, int CardNo, int MemberNo)
        //{
        //    try
        //    {
        //        Hashtable ht = new Hashtable();

        //        ht.Add("@memberID", Convert.ToInt32(MemberNo.ToString().Trim()));
        //        ht.Add("@cardnumber", Convert.ToInt32(CardNo.ToString().Trim()));
        //        ht.Add("@programmegroupId", addMembership.Rows[0]["ProgramGroupValue"].ToString().Trim());
        //        ht.Add("@programmeId", addMembership.Rows[0]["ProgramValue"].ToString().Trim());
        //        ht.Add("@state", addMembership.Rows[0]["State"].ToString().Trim());
        //        ht.Add("@fullcost", Convert.ToDecimal(FullCost.ToString().Trim()));
        //        ht.Add("@conditions", Condition.ToString().Trim());

        //        //ht.Add("@programStartDate", Convert.ToDateTime(addMembership.Rows[0]["Start Date"]).ToString().Trim());
        //        //ht.Add("@programEndDate", Convert.ToDateTime(addMembership.Rows[0]["End Date"]).ToString().Trim());

        //        int Membershipresult = sql.ExecuteQuery("SaveMembership", ht);
        //        return Membershipresult;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message).ToString();
        //        return 0;
        //    }

        //    }

        public int add_Membership(DataTable addMembership)//,decimal FullCost, string Condition, int CardNo, int MemberNo) 
        {
            try
            {
                int Membershipresult = 0;
                for (int i = 0; i <= addMembership.Rows.Count - 1; i++)
                {
                    Hashtable ht = new Hashtable();


                    //ht.Add("@fullcost", FullCost);
                    //ht.Add("@conditions", Condition);
                    ht.Add("@programmegroupId", Convert.ToInt32(addMembership.Rows[i]["ProgramGroupValue"].ToString().Trim()));
                    ht.Add("@programmeId", Convert.ToInt32(addMembership.Rows[i]["ProgramValue"].ToString().Trim()));
                    ht.Add("@state", addMembership.Rows[i]["State"].ToString().Trim());
                    ht.Add("@programStartDate", (addMembership.Rows[i]["Start Date"]).ToString().Trim());
                    ht.Add("@programEndDate", (addMembership.Rows[i]["End Date"]).ToString().Trim());
                    ht.Add("@memberID", Convert.ToInt32(addMembership.Rows[i]["MemberId"]).ToString().Trim());
                    ht.Add("@cardnumber", Convert.ToInt32(addMembership.Rows[i]["CardNo"]).ToString().Trim());

                    Membershipresult = sql.ExecuteQuery("SaveMembership", ht);
                }
                return Membershipresult;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message).ToString();
                return 0;
            }



        }

       
    }
}
