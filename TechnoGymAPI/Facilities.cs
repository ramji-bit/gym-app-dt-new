﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TechnoGymAPI
{
    public class Facilities
    {
        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("url")]
        public string url { get; set; }

        [JsonProperty("name")]
        public string name { get; set; }


        [JsonProperty("companyName")]
        public string companyName { get; set; }

        [JsonProperty("isChain")]
        public bool isChain { get; set; }
    }
}
