﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoGymAPI
{
    public class Create_User
    {
        public string business_name { get; set; }
        public string account_number { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string mobile { get; set; }
        public string address_line1 { get; set; }
        public string address_line2 { get; set; }
        public string address_suburb { get; set; }
        public string address_state { get; set; }
        public string address_postcode { get; set; }
        public string address_country { get; set; }
    }
}
