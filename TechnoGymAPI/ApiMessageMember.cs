﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TechnoGymAPI
{
    public class ApiMessageMember
    {
        [JsonProperty("token")]
        string _token { get; set; }
        
        [JsonProperty("data")]
        public DataMember data { get; set; }
        
        public string GetToken
        {
            get
            {
                return _token;
            }
        }

        [JsonProperty("errors")]
        public List<Errors> Errors { get; set; }
    }
}
