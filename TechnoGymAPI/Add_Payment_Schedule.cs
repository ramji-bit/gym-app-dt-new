﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoGymAPI
{
    public class Add_Payment_Schedule
    {
        public double amount { get; set; }
        public string schedule_date { get; set; }
    }
}
