﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Security;
using System.Web;
using System.IO;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Configuration;
using System.Windows.Forms;


namespace TechnoGymAPI
{
    public class TechnoGym_Request_Response
    {
        public string HdrAPIKEY = null;
        public string HdrCLIENT = "thirdParties";
        string username;
        string password;
        public TechnoGym_Request_Response()
        {
            try
            {
                string TechnoGym_Test_OR_Prod = ConfigurationManager.AppSettings["TechnoGym_Test_OR_Prod"];
                if ((TechnoGym_Test_OR_Prod != null) && (TechnoGym_Test_OR_Prod != ""))
                {
                    if (TechnoGym_Test_OR_Prod.ToUpper() == "TEST")
                    {
                        username = ConfigurationManager.AppSettings["test_technoGym_param_Username"];
                        password = ConfigurationManager.AppSettings["test_technoGym_param_Password"];
                        HdrAPIKEY = ConfigurationManager.AppSettings["test_technoGym_hdr_APIKEY"];
                    }
                    else
                    {
                        username = ConfigurationManager.AppSettings["PROD_technoGym_param_Username"];
                        password = ConfigurationManager.AppSettings["PROD_technoGym_param_Password"];
                        HdrAPIKEY = ConfigurationManager.AppSettings["PROD_technoGym_hdr_APIKEY"];
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public string Post(string url, IDictionary<string, string> paramValues = null, bool getToken = false)
        {
            var body = paramValues != null ? GetFormValues(paramValues) : null;
            var request = GetWebRequest(url, "POST", body, getToken);
            return Execute(request);
        }
        
        private static string GetFormValues(IDictionary<string, string> paramValues)
        {
            StringBuilder formString = new StringBuilder();
            bool firstValue = true;

            foreach (var param in paramValues)
            {
                if (firstValue)
                {
                    formString.Append("{");
                    firstValue = false;
                }
                else
                {
                    formString.Append(",");
                }

                formString.AppendFormat("\"{0}\":\"{1}\"", param.Key, param.Value);
            }
            formString.Append("}");

            return formString.ToString();
        }
        
        private WebRequest GetWebRequest(string url, string method, string body = null, bool getToken = false)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = method;
            request.ContentType = "application/json";

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            if (getToken)
            {
                request.Headers.Add("X-MWAPPS-APIKEY", HdrAPIKEY);
                request.Headers.Add("X-MWAPPS-CLIENT", HdrCLIENT);
            }

            if (!string.IsNullOrEmpty(body))
            {
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(body);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
            }
            return request;
        }
        private string Execute(WebRequest webRequest)
        {
            var apiResponse = new TechnoGymResponse();

            try
            {
                using (var response = webRequest.GetResponse())
                {
                    apiResponse.RawResponse = ReadStream(response.GetResponseStream());
                    apiResponse.StatusCode = ((HttpWebResponse)response).StatusCode;
                    var jsonstring = new JavaScriptSerializer().Serialize(apiResponse);
                }
            }
            catch (WebException webException)
            {
                if (webException.Response != null)
                {
                    var response = webException.Response;
                    apiResponse.RawResponse = ReadStream(response.GetResponseStream());
                    apiResponse.StatusCode = ((HttpWebResponse)response).StatusCode;
                }
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message, "TechnoGym API Integration", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            TryLog(string.Format("{0} {1} {2}", (int)apiResponse.StatusCode, apiResponse.StatusCode, apiResponse.RawResponse));

            return apiResponse.RawResponse;
        }
        public string GetAuthorizationHeader()
        {
            var token = Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Format("{0}:{1}", username, password)));
            return string.Format("Basic {0}", token);
        }
        private void TryLog(string message)
        {
            if (message != null)
            {
                Log(message);
            }
        }
        public void Log(string message)
        {
            string message_authorize = message;
        }
        private static string ReadStream(Stream stream)
        {
            using (var reader = new StreamReader(stream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }

        public string Delete(string url, IDictionary<string, string> paramValues = null)
        {
            var body = paramValues != null ? GetFormValues(paramValues) : null;
            var request = GetWebRequest(url, "DELETE", body);
            return Execute(request);
        }
        public string Get(string url, IDictionary<string, string> paramValues = null)
        {
            var query = paramValues != null ? GetQuerystring(paramValues) : null;
            var request = GetWebRequest(url + query, "GET");
            return Execute(request);
        }

        private static string GetQuerystring(IDictionary<string, string> paramValues)
        {
            StringBuilder queryString = new StringBuilder();
            bool firstValue = true;

            foreach (var param in paramValues)
            {
                if (firstValue)
                {
                    queryString.Append("?");
                    firstValue = false;
                }
                else
                {
                    queryString.Append("&");
                }

                queryString.AppendFormat("{0}={1}", HttpUtility.UrlEncode(param.Key), HttpUtility.UrlEncode(param.Value));
            }
            return queryString.ToString();
        }
    }
}
