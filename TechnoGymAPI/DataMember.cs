﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TechnoGymAPI
{
    public class DataMember
    {
        [JsonProperty("result")]
         public string result { get; set; }

        [JsonProperty("userId")]
        public string userId { get; set; }

        [JsonProperty("facilityUserId")]
        public string facilityUserId { get; set; }

        [JsonProperty("permanentToken")]
        public string permanentToken { get; set; }

        [JsonProperty("Errors")]
        public Errors errors { get; set; }
    }
}
