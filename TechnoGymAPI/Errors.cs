﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TechnoGymAPI
{
    [JsonObject]
    public class Errors
    {
        [JsonProperty("field")]
        public string field { get; set; }

        [JsonProperty("type")]
        public string type { get; set; }

        [JsonProperty("details")]
        public string details { get; set; }

        [JsonProperty("errorMessage")]
        public string errorMessage { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }
    }
}
