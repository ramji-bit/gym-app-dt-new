﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TechnoGymAPI
{
    public class DataAccess
    {
        [JsonProperty("facilities")]
        public List<Facilities> facilities { get; set; }

        [JsonProperty("result")]
        public string result { get; set; }

        [JsonProperty("accountConfirmed")]
        public bool accountConfirmed { get; set; }

        [JsonProperty("credentialId")]
        public string credentialId { get; set; }
    }
}
