﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;


namespace TechnoGymAPI
{
    internal class TechnoGymResponse
    {
        /// <summary>
        /// Gets or sets the raw response.
        /// </summary>
        /// <value>
        /// The raw response.
        /// </value>
        public string RawResponse { get; set; }

        /// <summary>
        /// Gets or sets the HTTP status code.
        /// </summary>
        /// <value>
        /// The status code.
        /// </value>
        public HttpStatusCode StatusCode { get; set; }

        public TMessage ConvertResponseTo<TMessage>()
        {
            try
            {
                if (!string.IsNullOrEmpty(RawResponse))
                {
                    return JsonConvert.DeserializeObject<TMessage>(RawResponse.ToString());
                }
            }
            catch (Exception ex) { }
            throw new Exception (StatusCode.ToString());
        }
    }

}
