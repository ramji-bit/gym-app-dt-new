﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TechnoGymAPI
{
    public class TechnoGym_Get_AuthToken
    {
        [JsonProperty("token")]
        public string token;
    }
}
