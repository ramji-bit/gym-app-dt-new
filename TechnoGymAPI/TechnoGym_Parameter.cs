﻿using System;
using System.Windows;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;
using System.Configuration;

namespace TechnoGymAPI
{
    public class TechnoGym_Parameter
    {
        TechnoGym_Request_Response gateway = new TechnoGym_Request_Response();
        //string url_dev = "https://apidev.mywellness.com/";
        //string url_prod = "https://api.mywellness.com/";
        
        string url = null;

        public TechnoGym_Parameter()
        {
            try
            {
                string TechnoGym_Test_OR_Prod = ConfigurationManager.AppSettings["TechnoGym_Test_OR_Prod"].ToString();
                if ((TechnoGym_Test_OR_Prod != null) && (TechnoGym_Test_OR_Prod != ""))
                {
                    if (TechnoGym_Test_OR_Prod.ToUpper() == "TEST")
                    {
                        url = ConfigurationManager.AppSettings["test_technoGym_URL"];
                    }
                    else
                    {
                        url = ConfigurationManager.AppSettings["PROD_technoGym_URL"];
                    }
                }
                else
                {
                    url = "https://apidev.mywellness.com/";
                }
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
                ConfigurationManager.AppSettings["globalErrorMessage"] = e.Message;
            }
        }
        public TechnoGymToken GetTechnoGym_Token
                        (
                            string technoGymURL, string technoGymFacility, string technoGymUniqueID,
                            string technoGymCLIENT, string technoGymAPIKEY, string technoGymUsername, string technoGymPassword
                        )
        {
            TechnoGymToken _TokenObj = new TechnoGymToken();

            try
            {

                string endpoint = "/" + technoGymFacility + "/Application/" + technoGymUniqueID + "/AccessIntegration";
                var endpointParams = new Dictionary<string, string>();

                endpointParams.Add("apiKey", technoGymAPIKEY);
                endpointParams.Add("Username", technoGymUsername);
                endpointParams.Add("Password", technoGymPassword);

                gateway.HdrAPIKEY = technoGymAPIKEY;
                gateway.HdrCLIENT = technoGymCLIENT;

                string response = gateway.Post(technoGymURL + endpoint, endpointParams, true);
                var message = JsonConvert.DeserializeObject<ApiMessage>(response);

                _TokenObj.StatCode = message.GetStatusCode;
                _TokenObj.Token = message.GetToken;
                _TokenObj.facilityID = message.data.facilities[0].id;

                ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"] = message.GetToken;

                return _TokenObj;
            }
            catch (Exception ex)
            {
                _TokenObj.StatCode = "400";
                _TokenObj.Token = ex.Message;
                return _TokenObj;
            }
        }
        public TechnoGymMemberParams AddMember_To_TechnoGym
                        (
                            string technoGymURL, string technoGymFacilityURL, string technoGymFacilityEntityID,
                            string technoGymCLIENT, string technoGymAPIKEY, string technoGymUsername, string technoGymPassword,
                            string technoGymAccessToken,
                            string memberFirstName,
                            string memberLastName,
                            string memberEmail,
                            string memberExternalID,
                            string memberGender,
                            string memberMobile
                        )
        {
            TechnoGymMemberParams _TokenObj = new TechnoGymMemberParams();

            try
            {
                //POST /{facilityUrl}/core/Facility/{entityId}/CreateFacilityUserFromThirdParty

                string endpoint = "/" + technoGymFacilityURL + "/Core/Facility/" + technoGymFacilityEntityID + "/CreateFacilityUserFromThirdParty";
                var endpointParams = new Dictionary<string, string>();

                endpointParams.Add("email", memberEmail);
                endpointParams.Add("externalId", memberExternalID);
                endpointParams.Add("firstName", memberFirstName);
                endpointParams.Add("lastName", memberLastName);
                endpointParams.Add("gender", memberGender);
                endpointParams.Add("token", technoGymAccessToken);

                if (memberMobile.Trim() != "")
                {
                    endpointParams.Add("mobilePhoneNumber", memberMobile); //RAMJI-26MAR2018
                }

                gateway.HdrAPIKEY = technoGymAPIKEY;
                gateway.HdrCLIENT = technoGymCLIENT;

                string response = gateway.Post(technoGymURL + endpoint, endpointParams, true);
                var message = JsonConvert.DeserializeObject<ApiMessageMember>(response);

                if (message.Errors != null)
                {
                    if (message.Errors[0].field.ToUpper().Trim() == "TOKENNOTVALID")
                    {
                        _TokenObj.ErrorMsg = "TOKENNOTVALID";
                        _TokenObj.PermanentToken = "TOKENNOTVALID";
                    }
                }
                else
                {
                    _TokenObj.PermanentToken = message.data.permanentToken;
                    _TokenObj.ErrorMsg = message.data.result;
                    ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"] = message.GetToken;
                }
                return _TokenObj;
            }
            catch (Exception ex)
            {
                _TokenObj.PermanentToken = "";
                return _TokenObj;
            }
        }
        public TechnoGymMemberDetailsParams Get_TechnoGym_User_FacilityID
                        (
                            string technoGymURL, string technoGymFacilityURL, string technoGymFacilityEntityID,
                            string technoGymCLIENT, string technoGymAPIKEY, string technoGymUsername, string technoGymPassword,
                            string technoGymAccessToken,
                            string technoGymPermanentToken
                        )
        {
            TechnoGymMemberDetailsParams _TokenObj = new TechnoGymMemberDetailsParams();

            try
            {
                //-> POST /{facilityUrl}/core/Facility/{entityId}/GetFacilityUserFromPermanentToken -> get Entity ID [User]

                string endpoint = "/" + technoGymFacilityURL + "/Core/Facility/" + technoGymFacilityEntityID + "/GetFacilityUserFromPermanentToken";
                var endpointParams = new Dictionary<string, string>();

                endpointParams.Add("permanentToken", technoGymPermanentToken);
                endpointParams.Add("token", technoGymAccessToken);

                gateway.HdrAPIKEY = technoGymAPIKEY;
                gateway.HdrCLIENT = technoGymCLIENT;

                string response = gateway.Post(technoGymURL + endpoint, endpointParams, true);
                var message = JsonConvert.DeserializeObject<ApiMessageMember>(response);

                if (message.Errors != null)
                {
                    if (message.Errors[0].field.ToUpper().Trim() == "TOKENNOTVALID")
                    {
                        _TokenObj.facilityUserId = "TOKENNOTVALID";
                    }
                }
                else
                {
                    _TokenObj.userId = message.data.userId;
                    _TokenObj.facilityUserId = message.data.facilityUserId;
                    ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"] = message.GetToken;
                }
                return _TokenObj;
            }
            catch (Exception ex)
            {
                _TokenObj.facilityUserId = "";
                return _TokenObj;
            }
        }
        public TechnoGymMemberParams updateMember_To_TechnoGym
                        (
                            string technoGymURL, string technoGymFacilityURL, string technoGym_User_FacilityID,
                            string technoGymCLIENT, string technoGymAPIKEY, string technoGymUsername, string technoGymPassword,
                            string technoGymAccessToken,
                            string memberFirstName,
                            string memberLastName,
                            string memberEmail,
                            string memberExternalID,
                            string memberGender,
                            string mobilePhoneNumber
                        )
        {
            TechnoGymMemberParams _TokenObj = new TechnoGymMemberParams();

            try
            {
                //POST /{facilityUrl}/core/FacilityUser/{entityId}/Update

                string endpoint = "/" + technoGymFacilityURL + "/Core/FacilityUser/" + technoGym_User_FacilityID + "/Update";
                var endpointParams = new Dictionary<string, string>();

                endpointParams.Add("email", memberEmail);
                endpointParams.Add("externalId", memberExternalID);
                endpointParams.Add("firstName", memberFirstName);
                endpointParams.Add("lastName", memberLastName);
                endpointParams.Add("gender", memberGender);
                endpointParams.Add("token", technoGymAccessToken);
                endpointParams.Add("mobilePhoneNumber", mobilePhoneNumber);

                gateway.HdrAPIKEY = technoGymAPIKEY;
                gateway.HdrCLIENT = technoGymCLIENT;

                string response = gateway.Post(technoGymURL + endpoint, endpointParams, true);
                var message = JsonConvert.DeserializeObject<ApiMessageMember>(response);

                if (message.Errors != null)
                {
                    if (message.Errors[0].field.ToUpper().Trim() == "TOKENNOTVALID")
                    {
                        _TokenObj.PermanentToken = "TOKENNOTVALID";
                    }
                }
                else
                {
                    _TokenObj.PermanentToken = "SUCCESS";
                    ConfigurationManager.AppSettings["TechnoGym_AccessIntegration_Token"] = message.GetToken;
                }
                return _TokenObj;
            }
            catch (Exception ex)
            {
                _TokenObj.PermanentToken = "FAILED";
                return _TokenObj;
            }
        }
    }
}
