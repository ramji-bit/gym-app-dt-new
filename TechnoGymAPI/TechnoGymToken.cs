﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoGymAPI
{
    public class TechnoGymToken
    {
        public string StatCode { get; set; }
        public string Token { get; set; }
        public string facilityID { get; set; }
    }
    public class TechnoGymMemberDetailsParams
    {
        public string userId { get; set; }
        public string facilityUserId { get; set; }
    }
    public class TechnoGymMemberParams
    {
        public string PermanentToken { get; set; }
        public string ErrorMsg { get; set; }
    }
}
