﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TechnoGymAPI
{
    public class ApiMessage
    {
        [JsonProperty("token")]
        string _token { get; set; }

        [JsonProperty("Statuscode")]
        string Statuscode { get; set; }

        [JsonProperty("data")]
        public DataAccess data { get; set; }
        
        public string GetToken
        {
            get
            {
                return _token;
            }
        }
        public string GetStatusCode
        {
            get
            {
                return Statuscode;
            }
        }
    }
}
